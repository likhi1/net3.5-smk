﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="suppport_02.aspx.cs" Inherits="suppport_02" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <h1 class="subject1">ร้องเรียน</h1>


    บริษัทสินมั่นคงประกันภัย จำกัด (มหาชน) มีความมุ่งมั่นที่จะพัฒนาบุคลากรที่ให้บริการและปรับปรุงการบริการ
    <br />
    ในทุกๆ ด้านอย่างต่อเนื่อง โปรดให้ข้อมูลความคิดเห็น ข้อเสนอแนะ หรือข้อร้องเรียนของท่านต่อบริษัทฯ 
    <br />
    เพื่อบริษัทฯจะได้นำมาพัฒนาการให้บริการต่อไป โดยผ่านแผนกลูกค้าสัมพันธ์ที่หมายเลขโทรศัพท์
 <br />
    <br />
    <ul>
        <li>1596 กด 5</li>
        <li>02 - 3793797</li>
        <li>02 - 3787000 ต่อ 7180 , 7177 , 7188 </li>
    </ul>
    <br />
    <strong>" ทุกความเห็นของท่าน มีค่ามากต่อการพัฒนาบริการของเรา "</strong>
    <br />

    ขอขอบคุณทุกความคิดเห็นที่มีให้กับบริษัทฯ  หรือ  
    <a href="mailto:customer@smk.co.th;info@smk.co.th;janya_s@smk.co.th">แจ้งผ่าน email คลิ๊กที่นี่</a>

</asp:Content>

