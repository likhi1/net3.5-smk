﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.Web.Services;
using System.Web.Script.Services;
using Newtonsoft.Json;

public partial class buyVoluntary_step1 : System.Web.UI.Page
{
    public string[] ColumnName = { "name", "car_od", "pre_grsservice", "web_id", "pre_grsgar", "campaign", "pol_typ", "car_cod", "car_size", "pre_vouchergar", "pre_voucherservice", "car_body" };

    DataView dv = new DataView();
    Label hplTmp = new Label();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initialDropdownList();
            ddlCarName.Attributes.Add("onchange", "PopulateCarMark()");
            ddlCarMark.Attributes.Add("onchange", "PopulateCarDetail()");
            txtDriver1BrithDate.maxdate = "31/12/" + Convert.ToString(DateTime.Now.Year + 543 - 18);
            txtDriver1BrithDate.LastYear = DateTime.Now.Year - 18;
            txtDriver2BrithDate.maxdate = "31/12/" + Convert.ToString(DateTime.Now.Year + 543 - 18);
            txtDriver2BrithDate.LastYear = DateTime.Now.Year - 18;
        }
        else
        {
            litScript.Text = "";
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string strCarCode = "";
        string strCarBody = "";
        string strOdMin = "0";
        string strOdMax = "0";
        string strScript = "";
        string strGroupCode = "";
        string strCarSize = "";
        DataSet dsPol1;
        DataSet dsPol2;
        DataSet dsPol3;
        OnlineVoluntaryManager cmOnline = new OnlineVoluntaryManager();
        RegisterManager cmRegister = new RegisterManager();
        strCarSize = txtCarCC.Text;
        if (rdoCarK.Checked)
        {
            strCarCode = "110";
            strCarBody = "A";
            strGroupCode = txtCarGroup.Text;
        }
        else if (rdoCarP2.Checked)
        {
            strCarCode = "210";
            strCarBody = "21";
            strGroupCode = "A";
        }
        else if (rdoCarP3.Checked)
        {
            strCarCode = "320";
            strCarBody = "N";
            strGroupCode = "A";
            strCarSize = "4";
        }
        else if (rdoCarV.Checked)
        {
            strCarCode = "210";
            strCarBody = "22";
            strGroupCode = "A";
        }
        strOdMin = txtOdMin.Text;
        strOdMax = txtOdMax.Text;
        for (int iYear = 1; iYear <= DateTime.Now.Year + 543 - Convert.ToInt32(ddlCarAge.SelectedValue); iYear++)
        {
            double dTmp;
            dTmp = Convert.ToDouble(strOdMin);
            strOdMin = Convert.ToString(dTmp - (dTmp * 5/100));
            dTmp = Convert.ToDouble(strOdMax);
            strOdMax = Convert.ToString(dTmp - (dTmp * 5/100));
        }
        dsPol1 = cmOnline.getDataPremium(strCarCode, ddlCarAge.SelectedValue, strGroupCode,
                                            ddlCarMark.SelectedValue, strCarSize, strCarBody,
                                            "A", (rdoDriverY.Checked ? "Y" : (rdoDriverN.Checked ? "N" : "")), txtDriver1BrithDate.Text,
                                            txtDriver2BrithDate.Text, "1", strOdMin, strOdMax,
                                            (rdoCompulY.Checked ? "Y" : (rdoCompulN.Checked ? "N" : "")), "",
                                            ddlDiscount.SelectedValue);
        dsPol2 = cmOnline.getDataPremium(strCarCode, ddlCarAge.SelectedValue, strGroupCode,
                                            ddlCarMark.SelectedValue, strCarSize, strCarBody,
                                            "A", (rdoDriverY.Checked ? "Y" : (rdoDriverN.Checked ? "N" : "")), txtDriver1BrithDate.Text,
                                            txtDriver2BrithDate.Text, "2", strOdMin, strOdMax,
                                            (rdoCompulY.Checked ? "Y" : (rdoCompulN.Checked ? "N" : "")), "",
                                            ddlDiscount.SelectedValue);
        dsPol3 = cmOnline.getDataPremium(strCarCode, ddlCarAge.SelectedValue, strGroupCode,
                                            ddlCarMark.SelectedValue, strCarSize, strCarBody,
                                            "A", (rdoDriverY.Checked ? "Y" : (rdoDriverN.Checked ? "N" : "")), txtDriver1BrithDate.Text,
                                            txtDriver2BrithDate.Text, "3", "0", "0",
                                            (rdoCompulY.Checked ? "Y" : (rdoCompulN.Checked ? "N" : "")), "",
                                            ddlDiscount.SelectedValue);
        ViewState.Remove("SortDirection");
        ViewState.Remove("SortString");
        ViewState.Remove("SortIndex");
        ViewState.Add("DSPol1Table", dsPol1);
        ViewState.Add("DSPol2Table", dsPol2);
        ViewState.Add("DSPol3Table", dsPol3);        
        ShowData();
        strScript = "<script>";
        strScript += " document.getElementById('divPol1').style.display = 'block';";
        strScript += " document.getElementById('divPol2').style.display = 'none';";
        strScript += " document.getElementById('divPol3').style.display = 'none';";
        strScript += " document.getElementById('tabOvl-onlineInsureCar').style.display = 'block';";
        strScript += " document.getElementById('divRemark').style.display = 'block';";
        string polType = "";
        if (GridView1.Rows.Count > 0)
        {
            polType = "1";
        }
        else if (GridView2.Rows.Count > 0)
        {
            polType = "2";
        }
        else if (GridView3.Rows.Count > 0)
        {
            polType = "3";
        }
        strScript += " poltype_click('"+polType+"');";
        strScript += " document.getElementById('divLoading').style.display = 'none';";
        strScript += " </script>";
        ScriptManager.RegisterClientScriptBlock(Literal1, typeof(Literal), "s", strScript, false);


        // insert tmpreg, tmpvolun, tmpmtveh, tmpcompul
        string[] ColumnName = { "tm_tmp_cd", "campaign",
                                    //register
                                    "rg_regis_no", "rg_member_cd", "rg_member_ref", "rg_main_class", "rg_pol_type", 
                                    "rg_effect_dt", "rg_expiry_dt", "rg_regis_dt", "rg_regis_time", "rg_regis_type", 
                                    "rg_ins_fname", "rg_ins_lname", "rg_ins_addr1", "rg_ins_addr2", "rg_ins_amphor",
                                    "rg_ins_changwat", "rg_ins_postcode", "rg_ins_tel", "rg_ins_email", "rg_sum_ins", 
                                    "rg_prmm", "rg_tax", "rg_stamp", "rg_pmt_cd", "rg_fleet_perc", 
                                    "rg_pay_type", "rg_payment_stat", "rg_ref_bank", "rg_approve", "rg_prmmgross",
                                    // register - new column
                                    "rg_ins_mobile", "rg_ins_idcard", "oth_distance", "oth_region", "oth_flagdeduct", 
                                    "oth_oldpolicy", "insc_id", "mott_id", "exp_percent",
                                    // voluntary
                                    "vl_regis_no", "vl_veh_cd", "vl_drv_flag", "vl_drv1", "vl_birth_drv1", 
                                    "vl_drv2", "vl_birth_drv2", "vl_tpbi_person", "vl_tpbi_time", "vl_tppd_time", 
                                    "vl_tppd_exc", "vl_od_time", "vl_od_exc", "vl_f_t", "vl_01_11", 
                                    "vl_01_121", "vl_01_122", "vl_01_21", "vl_01_211", "vl_01_212", 
                                    "vl_02_person", "vl_02", "vl_03", "vl_repair", "vl_accessory", 
                                    "vl_fleet_perc", "pre_grsvol", "vl_prmm", "vl_campaign",
                                    //mtveh
                                    "mv_regis_no", "mv_major_cd", "mv_minor_cd", "mv_veh_year", "mv_veh_cc", 
                                    "mv_veh_seat", "mv_veh_weight", "mv_license_no", "mv_license_area", "mv_engin_no", 
                                    "mv_chas_no", "mv_combine", "mv_no_claim", "mv_no_claim_perc",
                                    // compulsary
                                    "cp_regis_no", "cp_veh_cd", "cp_comp_prmm", "cp_comp_tax", "cp_comp_stamp",
                                    "cp_comp_grs",
                                    // senddoc
                                    "sed_regis_no", "sed_branch", "sed_name", "sed_ins_addr", "sed_amphor",
                                    "sed_changwat", "sed_postcode", "sed_tel", "sed_at"
                                    };
        string[] ColumnType = { "string", "string",
                                  //
                                    "string", "string", "string", "string", "string", 
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    //
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string",                                
                                    //
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string",
                                    //
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string",
                                    //
                                    "string", "string", "string", "string", "string",
                                    "string",
                                    //
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string"};
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        string strTmpCD = "";
        

        dt = TDS.Utility.MasterUtil.AddColumnToDataTable(dt, ColumnType, ColumnName);
        ds.Tables.Add(dt);
        ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
        // register
        ds.Tables[0].Rows[0]["rg_main_class"] = "M";        
        ds.Tables[0].Rows[0]["rg_ins_fname"] = txtRegName.Text;
        ds.Tables[0].Rows[0]["rg_ins_lname"] = txtRegSur.Text;
        ds.Tables[0].Rows[0]["rg_ins_mobile"] = txtRegMobile.Text;
        ds.Tables[0].Rows[0]["rg_ins_email"] = txtRegEmail.Text;
        // voluntary
        ds.Tables[0].Rows[0]["vl_drv_flag"] = (rdoDriverY.Checked ? "Y" : (rdoDriverN.Checked ? "N" : ""));
        ds.Tables[0].Rows[0]["vl_birth_drv1"] = txtDriver1BrithDate.Text;
        ds.Tables[0].Rows[0]["vl_birth_drv2"] = txtDriver2BrithDate.Text;
        // mtveh
        ds.Tables[0].Rows[0]["mv_major_cd"] = ddlCarName.SelectedValue;
        ds.Tables[0].Rows[0]["mv_minor_cd"] = Request.Form[ddlCarMark.UniqueID];
        ds.Tables[0].Rows[0]["mv_veh_year"] = Convert.ToString(Convert.ToInt32(ddlCarAge.SelectedValue) - 543);
        ds.Tables[0].Rows[0]["mv_veh_cc"] = strCarSize;//txtCarCC.Text;
        ds.Tables[0].Rows[0]["mv_veh_seat"] = "0";
        ds.Tables[0].Rows[0]["mv_veh_weight"] = "0";
        ds.Tables[0].Rows[0]["mv_combine"] = (rdoCompulY.Checked ? "Y" : (rdoCompulN.Checked ? "N" : ""));
        // new column
        ds.Tables[0].Rows[0]["oth_distance"] = ddlDriveLength.SelectedValue;
        ds.Tables[0].Rows[0]["oth_region"] = (rdoRegionDrive1.Checked ? "1" : (rdoRegionDrive2.Checked ? "2" : ""));
        ds.Tables[0].Rows[0]["oth_flagdeduct"] = "";
        ds.Tables[0].Rows[0]["oth_oldpolicy"] = rdoRegInsHave.Items[0].Selected ? "Y" : "N";
        ds.Tables[0].Rows[0]["insc_id"] = ddlOldInsurance.SelectedValue;
        ds.Tables[0].Rows[0]["mott_id"] = ddlOldPolType.SelectedValue;
        ds.Tables[0].Rows[0]["exp_percent"] = ddlDiscount.SelectedValue;        

        Session.Add("DSRegister", ds);
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
       // Label hplTmp = new Label();
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            hplTmp = (Label)(e.Row.FindControl("lblCampaign"));
            //TDS.Utility.MasterUtil.writeLog("lblCampaign", hplTmp.Text + " : " + txtTmpCampaign.Text + " : " + e.Row.RowIndex.ToString());
            if (hplTmp.Text == txtTmpCampaign.Text && e.Row.RowIndex > 0)
            {
                hplTmp.Text = "";
            }
            else
            {
                txtTmpCampaign.Text = hplTmp.Text;
            }
        }
    }
    protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //Label hplTmp = new Label();
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            hplTmp = (Label)(e.Row.FindControl("lblCampaign"));
            if (hplTmp.Text == txtTmpCampaign.Text)
            {
                hplTmp.Text = "";
            }
            else
            {
                txtTmpCampaign.Text = hplTmp.Text;
            }
        }
    }
    protected void GridView3_RowDataBound(object sender, GridViewRowEventArgs e)
    {
       // Label hplTmp = new Label();
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            hplTmp = (Label)(e.Row.FindControl("lblCampaign"));
            if (hplTmp.Text == txtTmpCampaign.Text)
            {
                hplTmp.Text = "";
            }
            else
            {
                txtTmpCampaign.Text = hplTmp.Text;
            }
        }
    }

    public void initialDropdownList()
    {
        DataSet ds;
        DataView dvTmp;
        OnlineVoluntaryManager cmOnline = new OnlineVoluntaryManager();
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        ds = cmDate.getDDLYear(DateTime.Now.Year + 543 - 20, DateTime.Now.Year + 543);
        dvTmp = ds.Tables[0].DefaultView;
        dvTmp.Sort = "value desc ";
        ddlCarAge.DataSource = dvTmp;
        ddlCarAge.DataTextField = "value";
        ddlCarAge.DataValueField = "value";
        ddlCarAge.DataBind();
        ddlCarAge.Items.Insert(0, new ListItem("-- กรุณาระบุ --", ""));

        ds = cmOnline.getAllDataCarName();
        ddlCarName.DataSource = ds;
        ddlCarName.DataTextField = "desc_t";
        ddlCarName.DataValueField = "carnamcod";
        ddlCarName.DataBind();
        ddlCarName.Items.Insert(0, new ListItem("-- กรุณาระบุ --", ""));

        ds = cmOnline.getDataCarMarkByCarnamcod(ddlCarName.SelectedValue, (rdoCarK.Checked ? "1" : (rdoCarP2.Checked ? "2" : (rdoCarP3.Checked ? "3" : (rdoCarV.Checked ? "4" : "")))));
        ddlCarMark.DataSource = ds;
        ddlCarMark.DataTextField = "desc_t";
        ddlCarMark.DataValueField = "carmakcod";
        ddlCarMark.DataBind();
        ddlCarMark.Items.Insert(0, new ListItem("-- กรุณาระบุ --", ""));

        ds = cmDate.getDDLYear(18, 100);
        dvTmp = ds.Tables[0].DefaultView;
        dvTmp.Sort = "value asc ";
        ddlRegAge.DataSource = dvTmp;
        ddlRegAge.DataTextField = "value";
        ddlRegAge.DataValueField = "value";
        ddlRegAge.DataBind();
        ddlRegAge.Items.Insert(0, new ListItem("-- กรุณาระบุ --", ""));

        ds = cmOnline.getActiveDataInsuranceCompany();
        ddlOldInsurance.DataSource = ds;
        ddlOldInsurance.DataTextField = "insc_name";
        ddlOldInsurance.DataValueField = "insc_id";
        ddlOldInsurance.DataBind();
        ddlOldInsurance.Items.Insert(0, new ListItem("-- กรุณาระบุ --", ""));

        ds = cmOnline.getActiveDataMotorType();
        ddlOldPolType.DataSource = ds;
        ddlOldPolType.DataTextField = "mott_name";
        ddlOldPolType.DataValueField = "mott_id";
        ddlOldPolType.DataBind();
        ddlOldPolType.Items.Insert(0, new ListItem("-- กรุณาระบุ --", ""));

        ds = cmOnline.getAllDriveDistance();
        ddlDriveLength.DataSource = ds;
        ddlDriveLength.DataTextField = "ddis_name";
        ddlDriveLength.DataValueField = "ddis_id";
        ddlDriveLength.DataBind();
        ddlDriveLength.Items.Insert(0, new ListItem("-- กรุณาระบุ --", ""));
    }
    protected void ShowData()
    {
        DataSet ds = new DataSet();
        if (ViewState["DSPol1Table"] != null)
            ds = (DataSet)ViewState["DSPol1Table"];
        if (ds.Tables.Count > 0)
        {
            dv.Table = ds.Tables[0];            
        }
        GridView1.DataSource = dv;
        GridView1.DataBind();

        if (ViewState["DSPol2Table"] != null)
            ds = (DataSet)ViewState["DSPol2Table"];
        if (ds.Tables.Count > 0)
        {
            dv.Table = ds.Tables[0];
        }
        GridView2.DataSource = dv;
        GridView2.DataBind();

        if (ViewState["DSPol3Table"] != null)
            ds = (DataSet)ViewState["DSPol3Table"];
        if (ds.Tables.Count > 0)
        {
            dv.Table = ds.Tables[0];
        }
        GridView3.DataSource = dv;
        GridView3.DataBind();
    }
    public string showPremium(string strPrem)
    {
        string strRet = "";
        if (strPrem == "0.00")
            strRet = "-";
        else
            strRet = strPrem;
        return strRet;
    }
    public bool isShowBuyIcon(object strPrem)
    {
        bool isRet = true;
        if (Convert.ToDouble(strPrem.ToString()) == 0.00)
            isRet = false;
        return isRet;
    }
    public string showCoverDetail(object campaign, object polType, 
                                    object carCode, object carSize,
                                    object carOD,object carBody )
    {
        string strRet = "buyInsureCarCover.aspx";
        strRet += "?cp=" + campaign.ToString();
        strRet += "&pl=" + polType.ToString();
        strRet += "&cc=" + carCode.ToString();
        strRet += "&cs=" + carSize.ToString();
        strRet += "&car_od=" + carOD.ToString();
        strRet += "&cb=" +carBody.ToString();
        return strRet;
    }
    public string showVoucherDetail(object strPremService, object strPremGar)
    {
        string strRet = "buyInsureCarVoucher.aspx";
        strRet += "?prem_gar=" + strPremGar.ToString();
        strRet += "&prem_service=" + strPremService.ToString();
        return strRet;
    }
    public string showBuyIcon(object WebID, object Type)
    {
        string strRet = "buyVoluntary_step2.aspx";
        strRet += "?web_id=" + WebID.ToString();
        strRet += "&type=" + Type.ToString();        
        return strRet;
    }
    
    [WebMethod]
    public static ArrayList GetCarMakData(string strCarName, string strCarType)
    {
        DataSet ds = new DataSet();
        ArrayList list = new ArrayList();
        OnlineVoluntaryManager cmOnline = new OnlineVoluntaryManager();
        //TDS.Utility.MasterUtil.writeLog("ddd", strCarMark + " : " + strCarType);
        ds = cmOnline.getDataCarMarkByCarnamcod(strCarName, strCarType);        
        ds.Tables[0].Rows.InsertAt(ds.Tables[0].NewRow(), 0);
        ds.Tables[0].Rows[0]["carmakcod"] = "";
        ds.Tables[0].Rows[0]["desc_t"] = "-- กรุณาระบุ --";
        for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
        {
            list.Add(new ListItem(
           ds.Tables[0].Rows[i]["desc_t"].ToString(),
           ds.Tables[0].Rows[i]["carmakcod"].ToString()
            ));            
        }
        return list;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)] 
    public static string GetCarDetailData(string strCarMark)
    {
        DataSet ds = new DataSet();
        ArrayList list = new ArrayList();
        OnlineVoluntaryManager cmOnline = new OnlineVoluntaryManager();
        ds = cmOnline.getDataCarMarkByCarMarkCode(strCarMark);
        for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
        {
           // list.Add(new ListItem(
           //ds.Tables[0].Rows[i]["desc_t"].ToString(),
           //ds.Tables[0].Rows[i]["carmakcod"].ToString()
           // ));

            //list.Add(new string[] { ds.Tables[0].Rows[i]["car_cc"].ToString(), ds.Tables[0].Rows[i]["car_group"].ToString(),
            //                        ds.Tables[0].Rows[i]["od_min"].ToString(), ds.Tables[0].Rows[i]["od_max"].ToString()});

            list.Add(ds.Tables[0].Rows[i]);
        }
        string json = JsonConvert.SerializeObject(list, Formatting.Indented);
        return json; 
        //return list;
    }
    

}
