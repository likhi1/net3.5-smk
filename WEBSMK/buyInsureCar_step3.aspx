﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="buyInsureCar_step3.aspx.cs" Inherits="buyInsureCar_step3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

      <div   id="page-online-insCar" >
<h1 class="subject1">ทำประกันออนไลน์</h1>

<div class="stepOnline">
<img src="images/app-step-3.jpg" />
</div>

<div class="prc-row"> 
   <h2 class="subject-detail">ยอดเบี้ยประกันที่ต้องชำระ</h2>
   <table   class="tb-online-insCar" >
   
        <tr>
            <td width="100">เบี้ยสมัครใจ</td>
            <td class="label"   >
                เบี้ยสุทธิ :
                </td>
            <td  >
                22,000 บาท</td>
            <td class="label" width="150">
                  อากร :</td>
            <td   >
                32 บาท</td>
        </tr>
        <tr>
            <td></td>
            <td class="label"   >
               <label>ภาษี :</label>
                </td>
            <td  >
               420 บาท</td>
            <td class="label"  >
                  เบี้ยรวม<label> :</label></td>
            <td   >
                22,452 บาท</td>
        </tr>
        <tr>
            <td width="100">เบี้ย พรบ.</td>            
                <td></td>
            <td  >
                22,000 บาท</td>
        </tr>
        <tr>
            <td width="100">เบี้ยรวมทั้งสิ้น</td>            
            <td></td>
            <td  >
                22,000 บาท</td>
        </tr>
        </table>
        
<table    class="tb-online-insCar" >
   
    <tr>
    <td>
    

  
    </td>
    </tr>
   
     
        </table>
        <h2 class="subject-detail">เลือกวิธีการชำระเงิน</h2>
<table    class="tb-online-insCar" >
   
    
   <tr>
      <td> 
      ท่านสามารถเลือกวิธีการชำระเบี้ยประกันภัยกับ บริษัท สินมั่นคงประกันภัย จำกัด (มหาชน) 
          <br />
          ด้วยวิธีใดวิธีหนึ่งจากตัวเลือกข้างล่างนี้
         </td>
   </tr>
   <tr>
      <td> 
          &nbsp;</td>
   </tr>
   <tr>
      <td> 
          <asp:RadioButtonList ID="RadioButtonList2" runat="server" 
               >
              <asp:ListItem Value="1">ชำระเงินด้วยเงินสด หรือเช็ค ที่สาขา<asp </asp:ListItem>
              <asp:ListItem Value="2">ชำระเงินโดยวิธีการโอนเข้าบัญชี</asp:ListItem>
              <asp:ListItem Value="3" Selected="True">ชำระเงินผ่านทาง www.scbeasy.com </asp:ListItem>
              <asp:ListItem Value="4">บริการจัดส่งและจัดเก็บเบี้ยประกันถึงบ้าน (เฉพาะภายในเขตกรุงเทพฯ)</asp:ListItem>
              <asp:ListItem Value="5">ชำระเงิน ด้วยบัตรเครดิต</asp:ListItem>
          </asp:RadioButtonList>
         </td>
   </tr>
         <tr>
         <td>&nbsp;</td>
         </tr>
        </table>
   <h2 class="subject-detail">ข้อมูลผู้เอาประกันภัย</h2>
<table    class="tb-online-insCar" >
    
        <tr>
            <td class="label"   >
                ชื่อ :
                </td>
            <td  >
                XXXXXXXXXX</td>
            <td class="label"  >
                  จังหวัด :</td>
            <td   >
                XXXXXXXXXX</td>
        </tr>
        <tr>
            <td class="label"   >
                นามกสกุล
                <label>:</label>
                </td>
            <td  >
                XXXXXXXXXX</td>
            <td class="label"  >
                  รหัสไปรษณีย์ :</td>
            <td   >
                XXXXXXXXXX</td>
        </tr>
        <tr>
            <td class="label"   >
                ที่อยู่ :</td>
            <td  >
                XXXXXXXXXX</td>
            <td class="label"  >
                  เบอร์โทรศัพท์มือถือ :</td>
            <td   >
                XXXXXXXXXX</td>
        </tr>
        <tr>
            <td class="label"   >
                ตำบล / แขวง :</td>
            <td  >
                XXXXXXXXXX</td>
            <td class="label"  >
                  เบอร์โทรศัพท์บ้าน :</td>
            <td   >
                XXXXXXXXXX</td>
        </tr>
        <tr>
            <td class="label"   >
                อำเภอ :</td>
            <td  >
                XXXXXXXXXX</td>
            <td class="label"  >
                  อีเมล์<label> :</label></td>
            <td   >
                XXXXXXXXXX</td>
        </tr>
         <tr>
         <td colspan="4"></td>
         </tr>
         </table> 
    <h2 class="subject-detail">ข้อมูลรถยนต์</h2>
<table    class="tb-online-insCar">
 

<tr>
<td class="label"  >
ยี่ห้อรถ :
</td>
<td  >
XXXXXXXXXX</td>
<td class="label"  >
ปีจดทะเบียน :</td>
<td   >
XXXXXXXXXX</td>
</tr>
<tr>
<td class="label"   >
รุ่นรถ
<label>:</label>
</td>
<td  >
XXXXXXXXXX</td>
<td class="label"  >
ขนาดซีซีเครื่องยนต์ :</td>
<td   >
XXXXXXXXXX</td>
</tr>
<tr>
<td class="label"   >
เลขทะเบียนรถ :</td>
<td  >
XXXXXXXXXX</td>
<td class="label"  >
เลขตัวถัง :</td>
<td   >
XXXXXXXXXX</td>
</tr>
<tr>
<td class="label"   >
จังหวัดที่จดทะเบียน :</td>
<td  >
XXXXXXXXXX</td>
<td class="label"  >
เลขเครื่องยนต์ :</td>
<td   >
XXXXXXXXXX</td>
</tr>
<tr>
<td class="label"   >
&nbsp;</td>
<td  >
&nbsp;</td>
<td class="label"  >
&nbsp;</td>
<td   >
&nbsp;</td>
</tr>
 
</table> 
   <h2 class="subject-detail">ข้อมูลความคุ้มครองหลัก</h2>
<table    class="tb-online-insCar" >

 

<tr>
<td class="label"  >
เลขที่อ้างอิง :
</td>
<td  >
90-05953</td>
<td    >
&nbsp;</td>
</tr>
<tr>
<td class="label"  >
วันที่เริ่มคุ้มครอง
<label>:</label>
</td>
<td  >
01/04/2012</td>
<td   >
1,000,000.00 บาท/ครั้ง</td>
</tr>
<tr>
<td class="label"  >
ความเสียหายต่อร่างกายหรืออนามัย :</td>
<td  >
300,000.00 บาท/คน</td>
<td   >
&nbsp;</td>
</tr>
<tr>
<td class="label"  >
ความเสียหายต่อทรัพย์สิน :</td>
<td  >
1,000,000.00 บาท/ครั้ง</td>
<td    >
&nbsp;</td>
</tr>
<tr>
<td class="label"  >
ความเสียหายต่อรถยนต์ :</td>
<td  >
0.00 บาท/ครั้ง</td>
<td   >
&nbsp;</td>
</tr>
<tr>
<td class="label"  >
รถยนต์สูญหาย / ไฟไหม้ :</td>
<td  >
0.00 บาท/ครั้ง</td>
<td   >
&nbsp;</td>
</tr>
<tr>
<td class="label"  >
ความเสียหายส่วนแรก :</td>
<td  >
0.00 บาท/ครั้ง</td>
<td   >
&nbsp;</td>
</tr>
<tr>
<td colspan="3"></td>
</tr>


</tr>
</table>    
   <h2 class="subject-detail">ข้อมูลความคุ้มครองเพิ่ม</h2>
<table    class="tb-online-insCar" >
 

<tr>
<td class="label"  >
เสียชีวิต สูญเสียอวัยวะ ทุพพลภาพ&nbsp; :
</td>
<td  >
ผู้ขับขี่ 1 คน</td>
<td   >
/  ผู้โดยสาร 5 คน</td>
<td   >
50,000.00 บาท</td>
</tr>

<tr>
<td class="label"  >
ทุพพลภาพชั่วคราว
<label>:</label>
</td>
<td  >
ผู้ขับขี่ 1 คน</td>
<td   >
/  ผู้โดยสาร 0 คน</td>
<td   >
0.00 บาท</td>
</tr>
<tr>
<td class="label"  >
ค่ารักษาพยาบาล :</td>
<td  >
6 คน</td>
<td   >
/  ประกันผู้ขับขี่</td>
<td   >
200,000.00 บาท</td>
</tr>
<tr>
<td colspan="4"></td> 
</tr>
</table>      
   <h2 class="subject-detail">สถานที่จัดส่งเอกสาร (ยกเว้นการเลือกชำระด้วยเงินสด หรือเช็ค)</h2>
<table   class="tb-online-insCar" >

 

<tr>
<td  colspan=4 >
<asp:RadioButtonList ID="RadioButtonList1" runat="server" Height="19px" 
RepeatDirection="Horizontal" Width="213px">
<asp:ListItem>ที่อยู่ตามกรมธรรม์</asp:ListItem>
<asp:ListItem>อื่นๆ</asp:ListItem>
</asp:RadioButtonList>
</td>
</tr>

<tr>
<td class="label"  >
ชื่อ :</td>
<td  >
<asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
</td>
<td class="label"   >
อำเภอ / เขต :</td>
<td   >
<asp:TextBox ID="TextBox9" runat="server"></asp:TextBox>
</td>
</tr>

<tr>
<td class="label"  >
นามสกุล :</td>
<td  >
<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
</td>
<td class="label"   >
รหัสไปรษณีย์ :&nbsp; </td>
<td   >
<asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
</td>
</tr>

<tr>
<td class="label"  >
ที่อยู่
<label>:</label>
</td>
<td  >
<asp:TextBox ID="TextBox2" runat="server" TextMode="MultiLine"></asp:TextBox>
</td>
<td class="label"  >
จังหวัด :</td>
<td   >
<asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
</td>
</tr>
 
</table>

<div style="margin:0px auto; width:190px;margin-top:15px;">
 

<asp:Button ID="Button1" runat="server"  class="btn-default" style="margin-right:10px;" Text="ยืนยัน"  OnClientClick="window.location='buyInsureCar_step4.aspx' ;return false;" />
<asp:Button ID="Button2" runat="server"  class="btn-default" Text="ย้อนกลับ"   OnClientClick="history.back();return false;"  />

    <div class=""></div>
</div>
 
</div><!-- End class="prc-row"-->

</div><!-- End  <div class="context page-app"> -->
</asp:Content>

