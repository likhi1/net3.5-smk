function isKeyUpperCase(objText) {
    key = window.event.keyCode;
    if ((key > 0x60) && (key < 0x7B))
    window.event.keyCode = key-0x20;

}

function isCommonKey() {
	var returnValue = false;
// Common Key are Tab, Caps Lock, Shift, Ctrl, Alt, Backspace, Insert, Delete, Home, End, Arrow left (<--), Arrow right (-->)
//alert(event.keyCode);
	if (event.keyCode== 9 || event.keyCode== 20 || event.keyCode== 16 || event.ctrlKey || event.altKey || event.keyCode==8 || event.keyCode==45 || event.keyCode==46 || event.keyCode==36 || event.keyCode==35 || event.keyCode==37 || event.keyCode==39) {
		returnValue =  true;
	}
	return returnValue;
}

function isKeyInteger() {
	var returnValue = false;
	returnValue = isCommonKey();
	if (!returnValue) {
// Only "-", "," and 0-9 Key-in Textfield
		if ((event.keyCode<=57 && event.keyCode>=48) || (event.keyCode<=105 && event.keyCode>=96) || event.keyCode== 109 || event.keyCode== 189 || event.keyCode== 188) {
			returnValue =  true;
		}
	}
	return returnValue;
}

function isKeyInteger2() {
	var returnValue = false;
	returnValue = isCommonKey();
	if (!returnValue) {
// Only 0-9 Key-in Textfield
		if ((event.keyCode<=57 && event.keyCode>=48) || (event.keyCode<=105 && event.keyCode>=96) ) {
			returnValue =  true;
		}
	}
	return returnValue;
}

function isKeyFloat() {
	var returnValue = false;
	returnValue = isCommonKey();
	if (!returnValue) {
// Only "-", ".", "," and 0-9 Key-in Textfield
		if ((event.keyCode<=57 && event.keyCode>=48) || (event.keyCode<=105 && event.keyCode>=96) || (event.keyCode<=110 && event.keyCode>=109) || (event.keyCode<=190 && event.keyCode>=188)) {
			returnValue =  true;
		}
	}
	return returnValue;
}

function isFloat (obj, fieldName, minFraction, maxFraction, haveComma, hiddenObj, round) {
// Set default value for run function
	defaultTitle = "�����żԴ��Ҵ"; // Default title of alert dialog
	defaultButtonLabel = "OK"; // Default title of alert dialog
	defaultMinFraction = 2; // Default fraction digits of float value
	defaultMaxFraction = 2; // Default fraction digits of float value
	defaultHaveComma = "no"; // Default comma in float value
	defaultRound = "normal"; // Default comma in float value
	var returnValue = true; // Variable for return value
	floatValue = ""; // Variable for keep float value in form of string
	numFraction = ""; // Variable for keep number of fraction digits of float value

// Check value of arguments and assign appropriate value
	minFraction = (minFraction == undefined || minFraction < 0) ? defaultMinFraction : minFraction ;
	maxFraction = (maxFraction == undefined || maxFraction < 0) ? defaultMaxFraction : maxFraction ;
	haveComma = (haveComma == undefined || haveComma == "") ? defaultHaveComma : haveComma ;
	round = (round == undefined || round == "") ? defaultRound : round ;
	hiddenObj = (hiddenObj == undefined || hiddenObj == "") ? obj : hiddenObj ;
	alertMessage = "������㹪�ͧ '"+fieldName+"' �����ӹǹ��ԧ  �ô��䢢�����";

// Check user key any charactor and check value is float
	if (obj.value != "") {
		floatValue = ""+toFloat(obj.value);
		if (floatValue == "false") {
			returnValue=false;
		}
	}

// Format data for show in text box
	if (returnValue && obj.value != "") {
// Show float value
		obj.value = formatFloat(floatValue, minFraction, maxFraction, haveComma, round);
	}

// Show alert if check value is not float
	if (!returnValue) {
		//showOWarningDialog(defaultTitle, alertMessage, defaultButtonLabel) ;
		alert(alertMessage);
		if (!(obj.type == 'hidden')) {
			obj.focus();
		}
	} else {
		if (haveComma.toLowerCase() == "yes") {
			hiddenObj.value = floatValue;
		}
	}
	return returnValue;	
}

function toFloat (checkValue) {
	var returnValue = true; // Variable for return value
	var floatValue = ""; // Variable for keep float value in form of string

	deci = checkValue.split(".");
	if (deci.length <= 2) {
		intArray = deci[0].split(",");
		floatValue = "";
		for (i=0; i<intArray.length; i++) {
			floatValue += intArray[i];
		}
		if (floatValue == parseFloat(floatValue)) {
			if (deci.length == 2) {
				if (deci[1] == parseInt(parseFloat(deci[1]))) {
					floatValue += "."+deci[1];
				} else returnValue=false;
			}
		} else returnValue=false;
	} else returnValue=false;

	if (returnValue) {
		returnValue = parseFloat(floatValue);
	}

	return returnValue;
}

function formatFloat (floatValue, minFraction, maxFraction, haveComma, round) {
// Set default value for run function
	var returnValue = ""; // Variable for return value
	var defaultMinFraction = 2; // Default fraction digits of float value
	var defaultMaxFraction = 2; // Default fraction digits of float value
	var defaultHaveComma = "no"; // Default comma in float value
	var defaultRound = "normal"; // Default comma in float value

// Check value of arguments and assign appropriate value
	minFraction = (minFraction == undefined || minFraction < 0) ? defaultMinFraction : minFraction ;
	maxFraction = (maxFraction == undefined || maxFraction < 0) ? defaultMaxFraction : maxFraction ;
	haveComma = (haveComma == undefined || haveComma == "") ? defaultHaveComma : haveComma ;
	round = (round == undefined || round == "") ? defaultRound : round ;

	floatArray = floatValue.split(".");
	isNegative = floatArray[0].charAt(0) == "-" ? true : false;
// Calculate round up, round down, normal value
	if (floatArray.length == 2) {
		if (floatArray[1].length > maxFraction) {
			tempValue = parseFloat(floatValue);
			addValue = "0.";
			for (i=0; i<maxFraction-1; i++){
				addValue += "0";
			}
			addValue += "1";
			if (round.toLowerCase() == "normal") {
				if (isNegative) {
					if (parseInt(floatArray[1].charAt(maxFraction)) >= 5) {
						floatValue = ""+(tempValue-parseFloat(addValue));
						floatArray = floatValue.split(".");
					}
				} else {
					if (parseInt(floatArray[1].charAt(maxFraction)) >= 5) {
						floatValue = ""+(tempValue+parseFloat(addValue));
						floatArray = floatValue.split(".");
					}
				}
			} else if (round.toLowerCase() == "up") {
				if (!isNegative) {
					floatValue = ""+(tempValue+parseFloat(addValue));
					floatArray = floatValue.split(".");
				}
			} else if (round.toLowerCase() == "down") {
				if (isNegative) {
					floatValue = ""+(tempValue-parseFloat(addValue));
					floatArray = floatValue.split(".");
				}
			}
		}
	}
// Find integer with comma    
	commaValue = floatArray[0];
	if (haveComma.toLowerCase() == "yes") {
		commaValue = "";
		floatArray[0] = isNegative ? floatArray[0].substring(1) : floatArray[0];
		for (i=floatArray[0].length-1; i>=0; i--){
			commaValue = floatArray[0].charAt(i)+commaValue;
			if (i%3 == floatArray[0].length%3 && i != 0) {
				commaValue = ","+commaValue;
			}
		}
		commaValue = (isNegative ? "-" : "")+commaValue;
	}
    
// Find fraction digits
	fractionValue = "";
	if (floatArray.length == 2) {
		if (floatArray[1].length > maxFraction) {
			numFraction = maxFraction;
			for (i=0; i<maxFraction; i++){
				fractionValue += floatArray[1].charAt(i);
			}
		} else if (floatArray[1].length < minFraction) {
			numFraction = minFraction;
			fractionValue = floatArray[1];
			for (i=0; i<(minFraction - floatArray[1].length); i++){
				fractionValue += "0";
			}
		} else {
			numFraction = floatArray[1].length;
			fractionValue = floatArray[1];
		}
	} else {
		numFraction = minFraction;
		for (i=0; i<numFraction; i++) {
			fractionValue += "0";
		}
	}

// Find value for show
	if (numFraction > 0) {
		returnValue = commaValue + "." + fractionValue;
	} else {
		returnValue = commaValue;
	}

	return returnValue ;
}

function isInteger (obj, fieldName, haveComma, hiddenObj) {
// Set default value for run function
	defaultTitle = "Data not valid"; // Default title of alert dialog
	defaultButtonLabel = "OK"; // Default title of alert dialog
	defaultHaveComma = "no"; // Default comma in float value
	var returnValue = true; // Variable for return value
	intValue = ""; // Variable for keep float value in form of string

// Check value of arguments and assign appropriate value
	haveComma = (haveComma == undefined || haveComma == "") ? defaultHaveComma : haveComma ;
	hiddenObj = (hiddenObj == undefined || hiddenObj == "") ? obj : hiddenObj ;
	alertMessage = "������㹪�ͧ '"+fieldName+"' �����ӹǹ���  �ô��䢢�����";

// Check user key any charactor and check value is integer
	if (obj.value != "") {
		intArray = obj.value.split(",");
		intValue = "";
		for (i=0; i<intArray.length; i++) {
			intValue += intArray[i];
		}
		if (intValue != parseInt(parseFloat(intValue))) {
			returnValue=false;
		}
	}

// Format data for show in text box
	if (returnValue && obj.value != "") {
		intArray = intValue.split(".");
		isNegative = intArray[0].charAt(0) == "-" ? true : false;
// Find integer with comma
		commaValue = intArray[0];
		if (haveComma.toLowerCase() == "yes") {
			commaValue = "";
			intArray[0] = isNegative ? intArray[0].substring(1) : intArray[0];
			for (i=intArray[0].length-1; i>=0; i--){
				commaValue = intArray[0].charAt(i)+commaValue;
				if (i%3 == intArray[0].length%3 && i != 0) {
					commaValue = ","+commaValue;
				}
			}
			commaValue = (isNegative ? "-" : "")+commaValue;
		}
// Show integer value
		obj.value = commaValue;
	}

// Show alert if check value is not integer
	if (!returnValue) {
		//showOWarningDialog(defaultTitle, alertMessage, defaultButtonLabel) ;
		alert(alertMessage);
		if (!(obj.type == 'hidden')) {
			obj.focus();
		}
	} else {
		if (haveComma.toLowerCase() == "yes") {
			hiddenObj.value = intValue;
		}
	}
	return returnValue;	
}

function isKeyPercent() {
	var returnValue = false;
	returnValue = isCommonKey();
	if (!returnValue) {
// Only "." and 0-9 Key-in Textfield
		if ((event.keyCode<=57 && event.keyCode>=48) || (event.keyCode<=105 && event.keyCode>=96) || (event.keyCode<=110 && event.keyCode>=109) || (event.keyCode==190) ) {
			returnValue =  true;
		}
	}
	return returnValue;
}
function isPercent (obj, fieldName, minFraction, maxFraction, haveComma, hiddenObj, round) {
// Set default value for run function
	defaultTitle = "�����żԴ��Ҵ"; // Default title of alert dialog
	defaultButtonLabel = "OK"; // Default title of alert dialog
	defaultMinFraction = 2; // Default fraction digits of float value
	defaultMaxFraction = 2; // Default fraction digits of float value
	defaultHaveComma = "no"; // Default comma in float value
	defaultRound = "normal"; // Default comma in float value
	var returnValue = true; // Variable for return value
	floatValue = ""; // Variable for keep float value in form of string
	numFraction = ""; // Variable for keep number of fraction digits of float value

// Check value of arguments and assign appropriate value
	minFraction = (minFraction == undefined || minFraction < 0) ? defaultMinFraction : minFraction ;
	maxFraction = (maxFraction == undefined || maxFraction < 0) ? defaultMaxFraction : maxFraction ;
	haveComma = (haveComma == undefined || haveComma == "") ? defaultHaveComma : haveComma ;
	round = (round == undefined || round == "") ? defaultRound : round ;
	hiddenObj = (hiddenObj == undefined || hiddenObj == "") ? obj : hiddenObj ;
	alertMessage = "������㹪�ͧ '"+fieldName+"' �����ӹǹ�����繵�  �ô��䢢�����";

// Check user key any charactor and check value is float
	if (obj.value != "") {
		floatValue = ""+toPercent(obj.value);
		if (floatValue == "false") {
			returnValue=false;
		}
	}

// Format data for show in text box
	if (returnValue && obj.value != "") {
// Show float value
		obj.value = formatFloat(floatValue, minFraction, maxFraction, haveComma, round);
	}

// Show alert if check value is not float
	if (!returnValue) {
		//showOWarningDialog(defaultTitle, alertMessage, defaultButtonLabel) ;
		alert(alertMessage);
		if (!(obj.type == 'hidden')) {
			obj.focus();
		}
	} else {
		if (haveComma.toLowerCase() == "yes") {
			hiddenObj.value = floatValue;
		}
	}
	return returnValue;	
}

function toPercent (checkValue) {
	var returnValue = true; // Variable for return value
	var floatValue = ""; // Variable for keep float value in form of string

	deci = checkValue.split(".");
	if (deci.length <= 2) {
		intArray = deci[0].split(",");
		floatValue = "";
		for (i=0; i<intArray.length; i++) {
			floatValue += intArray[i];
		}
		if (parseFloat(checkValue) > 100)
		{
			returnValue = false;
		}
		if (floatValue == parseFloat(floatValue)) {
			if (deci.length == 2) {
				if (deci[1] == parseInt(parseFloat(deci[1]))) {
					floatValue += "."+deci[1];
				} else returnValue=false;
			}
		} else returnValue=false;
	} else returnValue=false;

	if (returnValue) {
		returnValue = parseFloat(floatValue);
	}

	return returnValue;
}


// ******************************************************************
// This function accepts a string variable and verifies if it is a
// proper date or not. It validates format matching either
// mm-dd-yyyy or mm/dd/yyyy. Then it checks to make sure the month
// has the proper number of days, based on which month it is.

// The function returns true if a valid date, false if not.
// ******************************************************************

function isDate(objDate) {
    
    var dateStr = objDate.value;
    var isRet = true;
    var yearEng ;
    if (dateStr != "__/__/____"){
        dateStr = dateStr.replace("_","0").replace("_","0").replace("_","0").replace("_","0").replace("_","0").replace("_","0").replace("_","0").replace("_","0");
        objDate.value = dateStr;
        var datePat = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
        var matchArray = dateStr.match(datePat); // is the format ok?

        if (matchArray == null) {
        alert("Please enter date as either dd/mm/yyyy .");
        isRet = false;
        }
        
        month = matchArray[3]; // p@rse date into variables
        day = matchArray[1];
        year = matchArray[5];
        

        if (month < 1 || month > 12) { // check month range
        alert("Month must be between 1 and 12.");
        isRet = false;
        }

        if (day < 1 || day > 31) {
        alert("Day must be between 1 and 31.");
        isRet = false;
        }

        if ((month==4 || month==6 || month==9 || month==11) && day==31) {
        alert("Month "+month+" doesn`t have 31 days!")
        isRet = false;
        }

        yearEnt = year;
        if (year > 2500)
        {
            yearEng = year - 543;
        }
        if (month == 2) { // check for february 29th
        var isleap = (yearEng % 4 == 0 && (yearEng % 100 != 0 || yearEng % 400 == 0));
        if (day > 29 || (day==29 && !isleap)) {
        alert("February " + year + " doesn`t have " + day + " days!");
        isRet = false; 
        }
        }
        //isRet = true; // date is valid
        // ��Ǩ�ͺ mindate , maxdate
//        alert("dateStr : " + dateStr);
//        alert("day : " + day); 
//        alert(parseFloat((year*10000)+(month*100)+parseFloat(day)));
//        alert(objDate.mindate);
//        alert(parseFloat( parseInt(objDate.mindate.substring(6,10)*10000) + parseInt(objDate.mindate.substring(3,5)*100) + parseInt(objDate.mindate.substring(0,2)) ));
//        alert ( parseFloat((year*10000)+(month*100)+parseFloat(day)) <  parseFloat( parseInt(objDate.mindate.substring(6,10)*10000) + parseInt(objDate.mindate.substring(3,5)*100) + parseInt(objDate.mindate.substring(0,2)) ) );
        if (isRet)
        {
            if (objDate.mindate != "")
            {
                if ( parseFloat((year*10000)+(month*100)+parseFloat(day)) <  parseFloat( parseInt(objDate.mindate.substring(6,10)*10000) + parseFloat(objDate.mindate.substring(3,5)*100) + parseFloat(objDate.mindate.substring(0,2)) ) )
                {
                    alert("�ѹ������кص�ͧ�����¡��� " + objDate.mindate);
                    isRet = false;
                }
            }
            if (objDate.maxdate != "")
            {
                if ( parseFloat((year*10000)+(month*100)+parseFloat(day)) >  parseFloat( parseInt(objDate.maxdate.substring(6,10)*10000) + parseFloat(objDate.maxdate.substring(3,5)*100) + parseFloat(objDate.maxdate.substring(0,2)) ) )
                {
                    //alert("txtDate" + parseFloat((year*10000)+(month*100)+parseFloat(day)));
                    //alert("maxDate" +  parseFloat( parseInt(objDate.maxdate.substring(6,10)*10000) + parseInt(objDate.maxdate.substring(3,5)*100) + parseInt(objDate.maxdate.substring(0,2)) ));
                    alert("�ѹ������кص�ͧ����ҡ���� " + objDate.maxdate);
                    isRet = false;
                }
            }
        }
    }
    return isRet; // date is valid
}

function CommaFormatted(amount)
{
    amount = amount + ".00";
	var delimiter = ","; // replace comma if desired
	var floatArray = amount.split('.')
	var commaValue = "";	  	
	isNegative = floatArray[0].charAt(0) == "-" ? true : false;
	floatArray[0] = isNegative ? floatArray[0].substring(1) : floatArray[0];
//	alert("isNegative : " + isNegative); 
//	alert("1. floatArray[0] : " + floatArray[0] );	
	for (i=floatArray[0].length-1; i>=0; i--){
		commaValue = floatArray[0].charAt(i)+commaValue;
		if (i%3 == floatArray[0].length%3 && i != 0) {
			commaValue = ","+commaValue;
		}
	}
	commaValue = (isNegative ? "-" : "")+commaValue;
//	alert("2. commaValue : " + commaValue );
	return commaValue;
}
function CommaFormatted2(amount) {
    amount = amount + ".00";
    var delimiter = ","; // replace comma if desired
    var floatArray = amount.split('.')
    var commaValue = "";
    isNegative = floatArray[0].charAt(0) == "-" ? true : false;
    floatArray[0] = isNegative ? floatArray[0].substring(1) : floatArray[0];
    //	alert("isNegative : " + isNegative); 
    //	alert("1. floatArray[0] : " + floatArray[0] );	
    for (i = floatArray[0].length - 1; i >= 0; i--) {
        commaValue = floatArray[0].charAt(i) + commaValue;
        if (i % 3 == floatArray[0].length % 3 && i != 0) {
            commaValue = "," + commaValue;
        }
    }
    if (floatArray[1].length == 1)
        floatArray[1] = floatArray[1] + "0";
    else if (floatArray[1].length > 2)
        floatArray[1] = floatArray[1].substring(0, 2);
    commaValue += "." + floatArray[1];

    commaValue = (isNegative ? "-" : "") + commaValue;
    //	alert("2. commaValue : " + commaValue );
    return commaValue;
}
function CommaFormatted5Digit(amount)
{
    amount = amount + ".00";
	var delimiter = ","; // replace comma if desired
	var floatArray = amount.split('.')
	var commaValue = "";
	isNegative = floatArray[0].charAt(0) == "-" ? true : false;
	floatArray[0] = isNegative ? floatArray[0].substring(1) : floatArray[0];
//	alert("isNegative : " + isNegative); 
//	alert("1. floatArray[0] : " + floatArray[0] );	
	for (i=floatArray[0].length-1; i>=0; i--){
		commaValue = floatArray[0].charAt(i)+commaValue;
		if (i%3 == floatArray[0].length%3 && i != 0) {
			commaValue = ","+commaValue;
		}
	}
	if (floatArray[1].length == 1)
	    floatArray[1] = floatArray[1] + "0000";
	else if (floatArray[1].length == 2)
	    floatArray[1] = floatArray[1] + "000";
	else if (floatArray[1].length == 3)
	    floatArray[1] = floatArray[1] + "00";
	else if (floatArray[1].length == 4)
	    floatArray[1] = floatArray[1] + "0";
	else if (floatArray[1].length > 5) {
	    if (parseFloat(floatArray[1].charAt(5)) >= 5) {
	        floatArray[1] = floatArray[1].substring(0, 4) + (parseFloat(floatArray[1].charAt(4))+1);
	    }
	    else
	        floatArray[1] = floatArray[1].substring(0, 5);
	}
	commaValue += "." + floatArray[1];
	
	commaValue = (isNegative ? "-" : "")+commaValue;
//	alert("2. commaValue : " + commaValue );
	return commaValue;
}