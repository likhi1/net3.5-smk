﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using TDSLib.Libs.net.mail;
using System.Net.Mail;

public partial class buyInsureTravel_step3 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        RegisterManager cmRegister = new RegisterManager();
        if (Request.QueryString["regis_no"] != "")
        {
            ds = cmRegister.getDataTravelAccident(Request.QueryString["regis_no"]);
        }
        else
        {
            Response.Redirect("buyInsureTravel_step1.aspx", true);
        }

        if (!IsPostBack)
        {
            docToUI(ds);
        }
    }
    protected void btnPrint_Click(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        string strPrintURL = "";
        DataSet dsTmp = new DataSet();
        OnlineVoluntaryManager cmOnline = new OnlineVoluntaryManager();
        OnlineCompulsaryManager cmOnlineComp = new OnlineCompulsaryManager();
        RegisterManager cmRegister = new RegisterManager();
        NonMotorManager cmNon = new NonMotorManager();

        ds = cmRegister.getDataTravelAccident(Request.QueryString["regis_no"]);
        //if (Session["DSRegister"] != null)
        if (ds.Tables[0].Rows.Count > 0)            
        {
            //ds = (DataSet)Session["DSRegister"];
            ds.Tables[0].Columns.Add("cardtype_desc");
            ds.Tables[0].Columns.Add("occup_desc");
            ds.Tables[0].Columns.Add("relation_desc");
            
            dsTmp = cmOnline.getDataProvinceByCode(ds.Tables[0].Rows[0]["rg_ins_changwat"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                ds.Tables[0].Rows[0]["rg_ins_changwat"] = dsTmp.Tables[0].Rows[0]["ds_desc"].ToString();

			dsTmp = cmNon.getDataCardTypeByCode(ds.Tables[0].Rows[0]["pe_card_type"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                ds.Tables[0].Rows[0]["cardtype_desc"] = dsTmp.Tables[0].Rows[0]["ds_desc"].ToString();
            
            dsTmp = cmNon.getDataOccupationByCode(ds.Tables[0].Rows[0]["pe_occup"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                ds.Tables[0].Rows[0]["occup_desc"] = dsTmp.Tables[0].Rows[0]["ds_desc"].ToString();
                
			dsTmp = cmNon.getDataRelationByCode(ds.Tables[0].Rows[0]["pe_relation"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                ds.Tables[0].Rows[0]["relation_desc"] = dsTmp.Tables[0].Rows[0]["ds_desc"].ToString();
                
			dsTmp = cmOnline.getDataProvinceByCode(ds.Tables[0].Rows[0]["pe_ben_changwat"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                ds.Tables[0].Rows[0]["pe_ben_changwat"] = dsTmp.Tables[0].Rows[0]["ds_desc"].ToString();
                
			dsTmp = cmOnline.getDataProvinceByCode(ds.Tables[0].Rows[0]["sed_changwat"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                ds.Tables[0].Rows[0]["sed_changwat"] = dsTmp.Tables[0].Rows[0]["ds_desc"].ToString();
        }
        Session["DsReport"] = ds;

        strPrintURL = "CommonReportForm.aspx?rptName=TADocument.rpt&sid=DsReport";
        litScript.Text += "<script>window.open('" + strPrintURL + "','Document');</script>";
    }
    
    public void docToUI(DataSet ds)
    {
        string strMotGar = "";
        DataSet dsPrem = new DataSet();
        DataSet dsTmp = new DataSet();
        OnlineVoluntaryManager cmOnline = new OnlineVoluntaryManager();
        NonMotorManager cmNon = new NonMotorManager();

        sendMail(ds);
        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            lblRefNo.Text = ds.Tables[0].Rows[0]["rg_regis_no"].ToString();
            // รายละเอียดผู้เอาประกัน
            lblFName.Text = ds.Tables[0].Rows[0]["rg_ins_fname"].ToString();
            lblLName.Text = ds.Tables[0].Rows[0]["rg_ins_lname"].ToString();
            lblAddress.Text = ds.Tables[0].Rows[0]["rg_ins_addr1"].ToString();
            lblSubDistrict.Text = ds.Tables[0].Rows[0]["rg_ins_addr2"].ToString();
            lblDistrict.Text = ds.Tables[0].Rows[0]["rg_ins_amphor"].ToString();
            dsTmp = cmOnline.getDataProvinceByCode(ds.Tables[0].Rows[0]["rg_ins_changwat"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                lblProvince.Text = dsTmp.Tables[0].Rows[0]["ds_desc"].ToString();
            lblPostCode.Text = ds.Tables[0].Rows[0]["rg_ins_postcode"].ToString();
            lblIDCard.Text = ds.Tables[0].Rows[0]["pe_card_no"].ToString();
            lblMobile.Text = ds.Tables[0].Rows[0]["rg_ins_mobile"].ToString();
            lblTelephone.Text = ds.Tables[0].Rows[0]["rg_ins_tel"].ToString();
            lblEmail.Text = ds.Tables[0].Rows[0]["rg_ins_email"].ToString();

            // รายละเอียดผู้รับประโยชน์
            lblBefFName.Text = ds.Tables[0].Rows[0]["pe_ben_fname"].ToString();
            lblBefLName.Text = ds.Tables[0].Rows[0]["pe_ben_lname"].ToString();
            dsTmp = cmNon.getDataRelationByCode(ds.Tables[0].Rows[0]["pe_relation"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                lblRelationship.Text = dsTmp.Tables[0].Rows[0]["ds_desc"].ToString();
            lblBefAddress1.Text = ds.Tables[0].Rows[0]["pe_ben_addr1"].ToString();
            lblBefAddress2.Text = ds.Tables[0].Rows[0]["pe_ben_addr2"].ToString();
            lblBefDistrict.Text = ds.Tables[0].Rows[0]["pe_ben_amphor"].ToString();
            dsTmp = cmOnline.getDataProvinceByCode(ds.Tables[0].Rows[0]["pe_ben_changwat"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                lblBefProvince.Text = dsTmp.Tables[0].Rows[0]["ds_desc"].ToString();
            lblBefPostCode.Text = ds.Tables[0].Rows[0]["pe_ben_postcode"].ToString();
            lblBefTelephone.Text = ds.Tables[0].Rows[0]["pe_ben_tel"].ToString();
            lblBefEmail.Text = ds.Tables[0].Rows[0]["pe_ben_email"].ToString();

			lblStartDate.Text = FormatStringApp.FormatDate(ds.Tables[0].Rows[0]["rg_effect_dt"].ToString());
            // เบี้ย 
            double dPrem = 0;
            double dStamp = 0;
            double dTax = 0;
            double dGross = 0;
            double dPaid = 0;
            dPrem = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["rg_prmm"]);
            dStamp = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["rg_stamp"]);
            dTax = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["rg_tax"]);
            dGross = dPrem + dStamp + dTax;
            dPaid = dGross;
            lblPremium.Text = FormatStringApp.FormatNDigit(ds.Tables[0].Rows[0]["rg_prmm"], 2) + " บาท";
            lblStamp.Text = FormatStringApp.FormatNDigit(ds.Tables[0].Rows[0]["rg_stamp"], 2) + " บาท";
            lblVat.Text = FormatStringApp.FormatNDigit(ds.Tables[0].Rows[0]["rg_tax"], 2) + " บาท";
            lblGrossPremium.Text = FormatStringApp.FormatNDigit(dGross, 2) + " บาท";
            dPrem = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["rg_prmm"]);
            dStamp = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["rg_stamp"]);
            dTax = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["rg_tax"]);
            dGross = dPrem + dStamp + dTax;
            //lblComPremium.Text = FormatStringApp.FormatNDigit(dGross, 2) + " บาท";
            lblPaid.Text = FormatStringApp.FormatNDigit(dPaid, 2) + " บาท";

            // การชำระเงิน
            if (ds.Tables[0].Rows[0]["rg_pay_type"].ToString() == "1")
            {
                lblPaidMethod.Text = "ชำระเงินด้วยเงินสดหรือเช็ค ที่บริษัทสินมั่นคงประกันภัยจำกัด (มหาชน) ที่</br>";
                if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "100")
                    lblPaidMethod.Text += "สำนักงานใหญ่ (ถนนศรีนครินทร์)";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "100")
                    lblPaidMethod.Text += "สำนักงานใหญ่ (ถนนศรีนครินทร์)";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "101")
                    lblPaidMethod.Text += "สาขาสวนมะลิ (ยสเส)";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "102")
                    lblPaidMethod.Text += "สาขาดอนเมือง";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "102")
                    lblPaidMethod.Text += "สาขาบางแค";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "130")
                    lblPaidMethod.Text += "สาขารัตนาธิเบศร์";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "500")
                    lblPaidMethod.Text += "สาขาชลบุรี";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "201")
                    lblPaidMethod.Text += "สาขาพิษณุโลก";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "200")
                    lblPaidMethod.Text += "สาขาเชียงใหม่";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "601")
                    lblPaidMethod.Text += "สาขาหาดใหญ่";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "600")
                    lblPaidMethod.Text += "สาขาสุราษฎร์ธานี";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "300")
                    lblPaidMethod.Text += "สาขานครปฐม";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "401")
                    lblPaidMethod.Text += "สาขาขอนแก่น";
            }
            else if (ds.Tables[0].Rows[0]["rg_pay_type"].ToString() == "2")
            {
                lblPaidMethod.Text = "ชำระเงิน ด้วยวิธีการโอนเข้าบัญชีของ บริษัท สินมั่นคงประกันภัย จำกัด (มหาชน)";
            }
            else if (ds.Tables[0].Rows[0]["rg_pay_type"].ToString() == "3")
            {
                lblPaidMethod.Text = "ชำระเงิน ด้วยวิธีการโอนเข้าบัญชีของ บริษัท สินมั่นคงประกันภัย จำกัด(มหาชน) ผ่าน www.scbeasy.com";
            }
            else if (ds.Tables[0].Rows[0]["rg_pay_type"].ToString() == "4")
            {
                lblPaidMethod.Text = "บริการจัดส่งและจัดเก็บเบี้ยประกันถึงบ้าน (เฉพาะภายในเขตกรุงเทพฯ)";
            }
            else if (ds.Tables[0].Rows[0]["rg_pay_type"].ToString() == "5")
            {
                lblPaidMethod.Text = "ชำระเงิน ด้วยบัตรเครดิต";
            }
            // จัดส่งเอกสาร
            if (ds.Tables[0].Rows[0]["rg_ref_bank"].ToString() == "1")
                lblSendDoc.Text = "ที่อยู่ตามกรมธรรม์";
            else
            {
            }

        }
    }

    private void sendMail(DataSet dsContact)
    {
        string strSMTPDomainName = "";
        TDSMail mail;
        string strBody = "";
        string strType = "";
        string strEmail = "";
        MailAddressCollection mAddrs = new MailAddressCollection();
        MailManager cmMail = new MailManager();
        cmMail.setXmlFilePath(System.Configuration.ConfigurationManager.AppSettings["appPath"] + "/Adminweb/xml/ContactUs.xml");
        // ส่งเมล์ไปให้บุคคลที่มีหน้าทีรับผิดชอบ
        strSMTPDomainName = cmMail.getDataByKey("SMTPMail");
        mail = new TDSMail(strSMTPDomainName, 25);
        mail.TO = new MailAddressCollection();
        mail.Subject = "ลูกค้าจาก Internet สั่งซื้อประกันภัยออนไลน์ตามเลขอ้างอิง " + dsContact.Tables[0].Rows[0]["rg_regis_no"].ToString();
        mail.IsBodyHtml = true;
        strBody = " Dear เจ้าหน้าที่รับแจ้งประกัน ," + "<br>";
        strBody += " ลูกค้าได้ทำการบันทึกข้อมูลสั่งซื้อประกันภัยออนไลน์ตามเลขอ้างอิง " + dsContact.Tables[0].Rows[0]["rg_regis_no"].ToString() + "<br>";
        //mAddrs.Add(new MailAddress(cmMail.getDataByKey("EmailBuyOnline")));
        strBody += " Best Regards," + "<br>";
        strBody += " &nbsp;&nbsp;&nbsp;บริษัท สินมั่นคงประกันภัย จำกัด(มหาชน)." + "<br>";
        mail.Body = strBody;

        string[] arrEmailTo;
        strEmail = cmMail.getDataByKey("EmailBuyOnline");
        arrEmailTo = strEmail.Split(',');
        for (int i = 0; i <= arrEmailTo.Length - 1; i++)
        {
            mAddrs.Add(new MailAddress(arrEmailTo[i]));
        }
        mail.TO = mAddrs;
        try
        {
            string strFrom = cmMail.getDataByKey("EmailAdmin");
            string strPassword = cmMail.getDataByKey("EmailAdminPassword");
            mail.Send(new MailAddress(strFrom), strPassword, false);
        }
        catch (Exception ex)
        {
            TDS.Utility.MasterUtil.writeError("buyCompulsary_step3.sendMail() ", ex.ToString());
        }
    }
}
