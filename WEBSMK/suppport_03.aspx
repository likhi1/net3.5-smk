﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="suppport_03.aspx.cs" Inherits="suppport_03" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

<h1 class="subject1">การจ่ายสินไหม</h1>
    
<ol>
    <li>ค่าซ่อมรถยนต์
    <ul>
<li>กรณีเจ้าของติดต่อเอง ค่าแรง 1 - 50,000 บาท จ่ายทันที (วันทำการ)</li>
<li>กรณีเจ้าของติดต่อเอง ค่าแรง 50,001 - 100,000 บาท จ่ายภายใน 7 วัน (วันทำการ)</li>
<li>กรณีเจ้าของติดต่อเอง ค่าแรง 100,000 บาทขึ้นไป จ่ายภายใน 14 วัน (วันทำการ)</li>
        </ul>
        </li>
<li>ค่าบาดเจ็บและเสียชิวิต 
    <ul>
<li>1 - 100,000 บาท จ่ายภายใน 3 วัน (วันทำการ)</li>
<li>100,000 บาทขึ้นไป จ่ายภายใน 7 วัน (วันทำการ)</li>
    </ul>
    </li>
<li>อัคคีภัย (นับแต่วันที่ตกลงค่าสินไหมเรียบร้อยแล้ว) 
       <ul> 
<li>1 - 1,000,000 บาท จ่ายภายใน 5 วัน (วันทำการ)</li>
<li>1,000,001 - 2,000,000 บาท จ่ายภายใน 7 วัน (วันทำการ)</li>
<li>2,000,001 ขึ้น จ่ายภายใน 10 (วันทำการ)</li>
     </ul>
    </li>
<li>อุบัติเหตุส่วนบุคคล
<ul> 
<li>กรณีบาดเจ็บ และ เสียชีวิต ทุกวงเงิน จ่ายภายใน 7 วัน
    </li>
     </ul>
    </li>

</li>

</ol>


</asp:Content>

