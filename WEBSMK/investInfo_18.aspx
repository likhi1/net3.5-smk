﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="investInfo_18.aspx.cs" Inherits="investInfo_18" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
 

<div class="subject1">ฐานะทางการเงินและผลการดำเนินงาน (ปผว.1)</div > 
เปิดเผย ณ วันที่ 14 พฤศจิกายน พ.ศ. 2555
<div id="page-invest">

<table   class="tb-ver-center" >
<tr> 
<td class="no-border" colspan="5" > </td> 
<td class="no-border align-right"  colspan="2" >(หน่วย : ล้านบาท)</td>
</tr>
<tr>
<th  rowspan="2" class="align-left" >รายการ</th>
<th   colspan="2"   >ไตรมาสที่ 1</th>
<th   colspan="2"   >ไตรมาสที่ 2</th>
<th   colspan="2"    >ไตรมาสที่ 3</th>
</tr>
<tr>
<th width="5%"   >2555</th>
<th width="5%"  >2554</th>
<th width="5%"   >2555</th>
<th width="5%"  >2554</th>
<th width="5%"   >2555</th>
<th width="5%"  >2554</th>
</tr>
<tr>
<td class="align-left"  >สินทรัพย์</td>
<td class="align-right"   >10,447.69</td>
<td class="align-right"   >9,717.28</td>
<td class="align-right"   >10,501.53</td>
<td class="align-right"   >9,696.07</td>
<td class="align-right"   >10,843.71</td>
<td class="align-right"   >9,781.75</td>
</tr>
<tr>
<td class="align-left"  >หนี้สิน</td>
<td class="align-right"   >7,936.01</td>
<td class="align-right"   >7,527.33</td>
<td class="align-right"   >7,993.64</td>
<td class="align-right"   >7,440.94</td>
<td class="align-right"   >8,144.89</td>
<td class="align-right"   >7,492.31</td>
</tr>
<tr>
<td class="align-left"  >ส่วนของผู้ถือหุ้น</td>
<td class="align-right"   >2,511.68</td>
<td class="align-right"   >2,189.95</td>
<td class="align-right"   >2,507.89</td>
<td class="align-right"   >2,255.12</td>
<td class="align-right"   >2,698.82</td>
<td class="align-right"   >2,289.44</td>
</tr>
<tr>
<td class="align-left"  >เงินกองทุน</td>
<td class="align-right"   >3,616.78</td>
<td class="align-right"   >1,601.23</td>
<td class="align-right"   >3,092.05</td>
<td class="align-right"   >1,770.68</td>
<td class="align-right"   >3,291.71</td>
<td class="align-right"   >3,507.69</td>
</tr>
<tr>
<td class="align-left"  >เงินกองทุนที่ต้องดำรงตามกฎหมาย</td>
<td class="align-right"   >683.25</td>
<td class="align-right"   >589.14</td>
<td class="align-right"   >731.95</td>
<td class="align-right"   >589.14</td>
<td class="align-right"   >794.56</td>
<td class="align-right"   >577.63</td>
</tr>
<tr>
<td class="align-left"  >อัตราส่วนเงินกองทุนต่อเงินกองทุนที่ต้องดำรงตามกฎหมาย (ร้อยละ)</td>
<td class="align-right"   >529.35%</td>
<td class="align-right"   >271.79%</td>
<td class="align-right"   >422.44%</td>
<td class="align-right"   >300.56%</td>
<td class="align-right"   >414.28%</td>
<td class="align-right"   >607.26%</td>
</tr>
<tr>
<td class="align-left"  >รายได้</td>
<td class="align-right"   >1,781.67</td>
<td class="align-right"   >1,589.54</td>
<td class="align-right"   >3,653.31</td>
<td class="align-right"   >3,258.38</td>
<td class="align-right"   >5,645.77</td>
<td class="align-right"   >4,940.08</td>
</tr>
<tr>
<td class="align-left"  >รายจ่าย</td>
<td class="align-right"   >1,618.02</td>
<td class="align-right"   >1,437.68</td>
<td class="align-right"   >3,291.74</td>
<td class="align-right"   >2,862.13</td>
<td class="align-right"   >5,133.31</td>
<td class="align-right"   >4,415.73</td>
</tr>
<tr>
<td class="align-left"  >กำไร (ขาดทุน) สุทธิ</td>
<td class="align-right"   >163.65</td>
<td class="align-right"   >151.86</td>
<td class="align-right"   >361.56</td>
<td class="align-right"   >396.26</td>
<td class="align-right"   >512.46</td>
<td class="align-right"   >524.35</td>
</tr>
<tr>
<td class="align-left"  >กระแสเงินสดได้มา (ใช้ไป)<br />จากกิจกรรมดำเนินงาน</td>
<td class="align-right"   >191.82</td>
<td class="align-right"   >521.00</td>
<td class="align-right"   >446.20</td>
<td class="align-right"   >627.39</td>
<td class="align-right"   >705.37</td>
<td class="align-right"   >822.97</td>
</tr>
<tr>
<td class="align-left"  >กระแสเงินสดได้มา (ใช้ไป)<br />จากกิจกรรมลงทุน</td>
<td class="align-right"   >92.72</td>
<td class="align-right"   >(331.90)</td>
<td class="align-right"   >(241.05)</td>
<td class="align-right"   >(346.10)</td>
<td class="align-right"   >(432.86)</td>
<td class="align-right"   >(599.84)</td>
</tr>
<tr>
<td class="align-left"  >กระแสเงินสดได้มา (ใช้ไป)<br />จากกิจกรรมจัดหาเงิน</td>
<td class="align-right"   > - </td>
<td class="align-right"   > - </td>
<td class="align-right"   >(180.00)</td>
<td class="align-right"   >(160.00)</td>
<td class="align-right"   >(180.00)</td>
<td class="align-right"   >(160.00)</td>
</tr>
<tr>
<td class="align-left"  >เงินสดเพิ่มขึ้น (ลดลง) สุทธิ</td>
<td class="align-right"   >284.54</td>
<td class="align-right"   >189.10</td>
<td class="align-right"   >25.15</td>
<td class="align-right"   >121.29</td>
<td class="align-right"   >92.51</td>
<td class="align-right"   >63.13</td>
</tr> 
</table>
 
<div class="bold ">หมายเหตุ : </div>
<ol class="fontSize12">
<li>กฎหมายกำหนดให้สัดส่วนเงินกองทุนต่อเงินกองทุนที่ต้องดำรงตามกฎหมาย ต้องไม่ต่ำกว่าร้อยละ 100 </li>
<li>เงินกองทุน เป็นเงินกองทุนตามราคาประเมิน ตามประกาศว่าด้วยการประเมินราคาทรัพย์สินและหนี้สินของริษัท ซึ่งไม่อยู่ในขอบเขตการสอบทานของผู้สอบบัญชี</li>
<li>ไตรมาสที่ 2 หมายถึง ผลการดำเนินงานสะสม 6 เดือน และไตรมาสที่ 3 หมายถึงผลการดำเนินงานสะสม 9 เดือน</li>
<li>รายการกระแสเงินสดจากกิจกรรมแต่ละประเภท ให้หมายถึงกระแสเงินสดได้มา(ใช้ไป) ที่จัดทำโดยวิธีทางตรงหรือทางอ้อม</li>
<li>งบการเงินรายไตรมาสนี้ผ่านการสอบทานจากผู้สอบบัญชีแล้ว</li>
<li>ข้อมูลรายการในไตรมาสที่สามของปี พ.ศ.2554 (เดือนกันยายน) นำเสนอตามประกาศนายทะเบียน เรื่องกำหนดหลักเกณฑ์ วิธีการและเงื่อนไขในการจัดทำรายงานการดำรงเงินกองทุน ของบริษัทประกันวินาศภัย พ.ศ.2554</li>
</ol>

 
 
 <table   class="tb-ver-center" >
<tr>
<td class="no-border" > </td>
<td class="no-border align-right" colspan="2" > (หน่วย : ล้านบาท)</td>
</tr>
<tr>
  <th class="align-left" >รายการ</th>
  <th  >31 ธ.ค. 2554</th>
  <th   >31 ธ.ค. 2553</th>
</tr>
<tr>
<td class="align-left"  >สินทรัพย์</td>
<td class="align-right"   >10,013.89</td>
<td class="align-right"   >8,886.15</td>
</tr>
<tr>
<td class="align-left"  >หนี้สิน</td>
<td class="align-right"   >7,778.96</td>
<td class="align-right"   >6,871.46</td>
</tr>
<tr>
<td class="align-left"  >ส่วนของผู้ถือหุ้น</td>
<td class="align-right"   >2,234.93</td>
<td class="align-right"   >2,014.69</td>
</tr>
<tr>
<td class="align-left"  >เงินกองทุน</td>
<td class="align-right"   >3,295.23</td>
<td class="align-right"   >1,512.60</td>
</tr>
<tr>
<td class="align-left"  >เงินกองทุนที่ต้องดำรงตามกฎหมาย</td>
<td class="align-right"   >642.22</td>
<td class="align-right"   >486.46</td>
</tr>
<tr>
<td class="align-left"  >อัตราส่วนเงินกองทุนต่อเงินกองทุนที่ต้องดำรงตามกฎหมาย (ร้อยละ)</td>
<td class="align-right"   >513.10%</td>
<td class="align-right"   >310.94%</td>
</tr>
<tr>
<td class="align-left"  >รายได้</td>
<td class="align-right"   >6,622.25</td>
<td class="align-right"   >5,652.33</td>
</tr>
<tr>
<td class="align-left"  >รายจ่าย</td>
<td class="align-right"   >6,166.49</td>
<td class="align-right"   >5,170.73</td>
</tr>
<tr>
<td class="align-left"  >กำไร (ขาดทุน)</td>
<td class="align-right"   >455.75</td>
<td class="align-right"   >481.60</td>
</tr>
<tr>
<td class="align-left"  >กระแสเงินสดได้มา (ใช้ไป) จากกิจกรรมดำเนินงาน</td>
<td class="align-right"   >1,019.99</td>
<td class="align-right"   >1,296.07</td>
</tr>
<tr>
<td class="align-left"  >กระแสเงินสดได้มา (ใช้ไป) จากกิจกรรมลงทุน</td>
<td class="align-right"   >(1,086.05)</td>
<td class="align-right"   >(1,185.27)</td>
</tr>
<tr>
<td class="align-left"  >กระแสเงินสดได้มา (ใช้ไป) จากกิจกรรมจัดหาเงิน</td>
<td class="align-right"   >(160.32)</td>
<td class="align-right"   >(89.91)</td>
</tr>
<tr>
<td class="align-left"  >เงินสดเพิ่มขึ้น (ลดลง) สุทธิ</td>
<td class="align-right"   >(226.37)</td>
<td class="align-right"   >20.89</td>
</tr>
 
</table>

 
<div class="bold">หมายเหตุ : </div>
<ol class="fontSize12">
<li>กฎหมายกำหนดให้สัดส่วนเงินกองทุนต่อเงินกองทุนที่ต้องดำรงตามกฎหมาย ต้องไม่ต่ำกว่าร้อยละ 100</li>
<li>เงินกองทุน เป็นเงินกองทุนตามราคาประเมิน ตามประกาศว่าด้วยการประเมินราคาทรัพย์สินและหนี้สินของ บริษัท ซึ่งไม่อยู่ในขอบเขตการตรวจสอบของผู้สอบบัญชี</li>
<li>ข้อมูลรายงานในปี พ.ศ.2554 นำเสนอตามประกาศนายทะเบียน เรื่องกำหนดหลักเกณฑ์ วิธีการและเงื่อนไข ในการจัดทำรายงานการดำรงเงินกองทุน ของบริษัทประกันวินาศภัย พ.ศ.2554</li>
</ol>
 

<br />
<div class="subject2"> อัตราส่วนทางการเงินที่สำคัญ (ร้อยละ)</div>
<table  class="tb-ver-center"  >
 
<tr>
<th class="align-left" >อัตราส่วน</th>
<th >ค่ามาตรฐาน</th>
<th  >ปี 2554</th>
<th   >ปี 2553</th>
</tr>
<tr>
<td class="align-left"  >อัตราส่วนสภาพคล่อง</td>
<td   >100</td>
<td   >296.08</td> 
<td    >243.34</td>
</tr>
 
</table>

 
<br />
<div class="subject2">ตารางสัดส่วนร้อยละของเบี้ยประกันภัยแยกตามประเภทของการรับประกันภัยประจำปี 2554</div>

<table   class="tb-padding-zero" >
 
<tr>
<td class="no-border"> </td>
<td class="no-border"> </td>
<td class="no-border"> </td>
<td class="no-border"> </td>
<td class="no-border"> </td>
<td class="no-border"> </td>
<td colspan="8" class="no-border align-right" >(หน่วย : ล้านบาท)</td>
</tr>

<tr>
<th rowspan="2" >รายการ</th>
<th rowspan="2"   >การประกัน<br>
อัคคีภัย</th>
<th colspan="2"  >การประกันภัยทาง<br>
ทะเลและขนส่ง</th>
<th colspan="2"   >การประกันภัย<br>
รถยนต์</th>
<th colspan="7"  >การประกันภัย<br>
เบ็ดเตล็ด</th>
<th rowspan="2"   >รวม</th>
</tr>



<tr>
<th>ตัวเรือ</th>
<th>สินค้า</th>
<th>โดยข้อบังคับ<br>
ของ<br>กฎหมาย</th>
<th>โดยความ<br>
สมัครใจ</th>
<th>ความ<br>
เสี่ยงภัย<br>ทรัพย์สิน</th>
<th>ความรับผิด<br>
ต่อบุคคล<br>ภายนอก</th>
<th>วิศวกรรม</th>
<th>อุบัติเหตุ<br>
ส่วน<br>บุคคล</th>
<th>สุขภาพ</th>
<th>พืชผล</th>
<th>อื่นๆ</th>
</tr>
<tr>
<td   >จำนวนเบี้ย<br>
ประกันภัย</td>
<td class="align-right"   >106.20</td>
<td class="align-center"  >-</td> 
<td class="align-right"  >6.46</td>
<td class="align-right"   >625.93</td> 
<td class="align-right"   >5,555.10</td>
<td class="align-right"  >38.77</td> 
<td class="align-right"  >0.85</td>
<td class="align-right"  >0.90</td> 
<td class="align-right"  >362.04</td>
<td class="align-right"  >9.51</td> 
<td class="align-right"  >0.04</td>
<td class="align-right"  >98.36</td>
<td class="align-right"   >6,804.15</td> 
</tr>
<tr>
<td  >สัดส่วน<br>
ของเบี้ย<br>ประกันภัย</td>
<td class="align-right"   >1.6%</td>
<td class="align-center"  >-</td> 
<td class="align-right"  >0.1%</td>
<td class="align-right"   >9.2%</td> 
<td class="align-right"   >81.6%</td>
<td class="align-right"  >0.6%</td> 
<td class="align-right"  >0.01%</td>
<td class="align-right"  >0.01%</td> 
<td class="align-right"  >5.3%</td>
<td class="align-right"  >0.1%</td> 
<td class="align-right"  >0.0%</td> 
<td class="align-right"  >1.4%</td>
<td class="align-right"   >100.0%</td> 
</tr>
 
 
</table>

 
<div class="bold">หมายเหตุ : ข้อมูลมาจากรายงานประจำปี</div> 
<ol class="fontSize12"> 
<li><a href="downloads/investment/SMK_finance_file1.pdf">ขั้นตอน ระยะเวลา เอกสารและวิธีการในการขอเอาประกันภัย</a></li>
<li><a href="downloads/investment/upload/SMK_finance_file1.pdf">ขั้นตอน ระยะเวลา เอกสารและวิธีการในการขอรับค่าสินไหมทดแทน หรือผลประโยชน์ตามกรมธรรม์ประกันภัย</a></li>
<li><a href="downloads/investment/upload/SMK_finance_file1.pdf">การติดต่อบริษัทประกันวินาศภัย และหน่วยงานที่เกี่ยวข้อง กรณีมีข้อพิพาทหรือเรื่องร้องเรียน</a></li>
</ol> 
 

<br />
<p>ข้าพเจ้า นายเรืองเดช  ดุษฎีสุรพจน์  และ นางสุวิมล  ชยวรประภา ขอรับรองความถูกต้องของรายงานฐานะการเงิน <br />และผลการดำเนินงานทั้งหมด 5 หน้า</p>
<div style="margin:0px auto; width:357px;height:221px;display:block; "  ><img src="images/signature1.jpg" /></div>
</div><!-- end id="page-invest"  -->

</asp:Content>

