﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//--- connect
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
//--- arrayList
using System.Collections;


public partial class networkMap : System.Web.UI.Page
{
    public string NtwId  { get{ return Request.QueryString["id"]; } } 

    protected void Page_Load(object sender, EventArgs e) {
        BindData();
    } 

    protected void BindData() { 

      DataSet ds  = SelectData(NtwId);
      DetailsView1.DataSource = ds;
      DetailsView1.DataBind(); 


      DataTable dt = ds.Tables[0];  
              if (dt.Rows.Count > 0) { 
                   

                    string pic1 = dt.Rows[0]["NtwPic1"].ToString();
                    string pic2 = dt.Rows[0]["NtwPic2"].ToString();
                    string picDefault = "images/blank_photo_mediuem.jpg";  
                    pic1 = (pic1 == "") ? picDefault : pic1;
                    pic2 = (pic2 == "") ? picDefault : pic2; 

                    imgPic1.Attributes.Add("src", pic1);
                    imgPic2.Attributes.Add("src", pic2);  

                    //////// Add HTML Code To <div>
                    areaMap.InnerHtml =  dt.Rows[0]["NtwMap"].ToString();



                    
                    HyperLink hplMapLink = (HyperLink)( Page.FindControl("hplMapLink"));
                    if (hplMapLink != null) {
                        hplMapLink.Text = "เปิดแผนที่ขนาดใหญ่่";
                        string strNtwMapLink = dt.Rows[0]["NtwMapLink"].ToString();
                        if (strNtwMapLink != null && strNtwMapLink != "") {
                            hplMapLink.NavigateUrl = strNtwMapLink;
                            hplMapLink.Target = "_blank";
                        } else {
                            hplMapLink.NavigateUrl = "#";
                        }

                    }
 


                    //areaMap.Attributes["style"] =  "width:620px; height:260px;margin-bottom:50px;clear:both;overflow:hidden;" ; 

                    ///////// Add  Attributes src(Url)  to iframe   
                    //ifmMap.Visible = true;
                    //ifmMap.Attributes.Add ( "src" , dt.Rows[0]["NtwMap"].ToString()  ) ;
              } 
   }


    protected DataSet SelectData( string _NtwId  ) { 
        string strSql = "SELECT *  FROM  tbNtw "
            + "WHERE  NtwId='" + NtwId + "'  "; 
        DBClass obj = new DBClass ();
        DataSet ds = obj.SqlGet(strSql ,"tb");

        return ds; 

    }




    protected void DetailsView1_DataBound(object sender, EventArgs e) { 

           
                Label lblName = (Label)(DetailsView1.FindControl("lblName"));
                object NtwName = DataBinder.Eval(DetailsView1.DataItem, "NtwName");
                if (lblName != null && !Convert.IsDBNull(NtwName) && NtwName.ToString() != "")
                    lblName.Text = NtwName.ToString() ;
                else
                    DetailsView1.Fields[0].Visible = false;


                Label lblAddress = (Label)(DetailsView1.FindControl("lblAddress"));
                object NtwAddress = DataBinder.Eval(DetailsView1.DataItem, "NtwAddress");
                if (lblAddress != null && !Convert.IsDBNull(NtwAddress) && NtwAddress.ToString() != "")
                    lblAddress.Text = NtwAddress.ToString();
                else
                    DetailsView1.Fields[1].Visible = false;
              
                Label lblProvince = (Label)(DetailsView1.FindControl("lblProvince"));
                object NtwProvince = DataBinder.Eval(DetailsView1.DataItem, "NtwProvince") ;
                if (lblProvince != null && !Convert.IsDBNull(NtwProvince) && NtwProvince.ToString() != "")
                    lblProvince.Text = NtwProvince.ToString() ;
                else
                    DetailsView1.Fields[2].Visible = false;

                Label lblAmphoe = (Label)(DetailsView1.FindControl("lblAmphoe"));
                object NtwAmphoe = DataBinder.Eval(DetailsView1.DataItem, "NtwAmphoe") ;
                if (lblAmphoe != null && !Convert.IsDBNull(NtwAmphoe) && NtwAmphoe.ToString() != "" )
                    lblAmphoe.Text = "(" + NtwAmphoe + ")";
                // Display same line lblProvince -  No Check Value For Show Row & Hide Row 
                   

                Label lblPhone = (Label)(DetailsView1.FindControl("lblPhone"));
                object NtwTelPhone = DataBinder.Eval(DetailsView1.DataItem, "NtwTelPhone");
                object NtwMoTelbile = DataBinder.Eval(DetailsView1.DataItem, "NtwTelMobile");
                if (!Convert.IsDBNull(NtwTelPhone) && NtwTelPhone.ToString() != "") {
                    lblPhone.Text =   NtwTelPhone.ToString();

                    if (!Convert.IsDBNull(NtwMoTelbile) && NtwMoTelbile.ToString() != "")
                        lblPhone.Text += ",  " + NtwMoTelbile.ToString();

                } else {
                    if (!Convert.IsDBNull(NtwMoTelbile) && NtwMoTelbile.ToString() != "") {
                        lblPhone.Text += NtwMoTelbile.ToString();
                    } else {
                        DetailsView1.Fields[3].Visible = false;
                    }
                }
         

                Label lblFax = (Label)(DetailsView1.FindControl("lblFax"));
                object NtwFax = DataBinder.Eval(DetailsView1.DataItem, "NtwFax");
                if (lblFax != null && !Convert.IsDBNull(NtwFax) && NtwFax.ToString() != "" )
                    lblFax.Text = NtwFax.ToString();
                else
                    DetailsView1.Fields[4].Visible = false;
                   

                Label lblEmail = (Label)(DetailsView1.FindControl("lblEmail"));
                object NtwEmail = DataBinder.Eval(DetailsView1.DataItem, "NtwEmail") ;
                if (lblEmail != null && !Convert.IsDBNull(NtwEmail) && NtwEmail.ToString() != ""  )
                    lblEmail.Text = NtwEmail.ToString();
                else
                    DetailsView1.Fields[5].Visible = false;


                HyperLink hplWebsite = (HyperLink)(DetailsView1.FindControl("hplWebsite"));
                object NtwWebsite = DataBinder.Eval(DetailsView1.DataItem, "NtwWebsite") ;
                if (hplWebsite != null && !Convert.IsDBNull(NtwWebsite) && NtwWebsite.ToString() != "" ) {
                    string strNtwWebsite = NtwWebsite.ToString().Replace("http://", "");
                    hplWebsite.NavigateUrl = "http://" +   NtwWebsite.ToString();

                    hplWebsite.Text = NtwWebsite.ToString();
                    hplWebsite.Target = "blank";
                } else {
                    DetailsView1.Fields[6].Visible = false;
                }


                Label lblNotation = (Label)(DetailsView1.FindControl("lblNotation"));
                object NtwNotation = DataBinder.Eval(DetailsView1.DataItem, "NtwNotation").ToString();
                if (lblNotation != null && !Convert.IsDBNull(NtwNotation) && NtwNotation.ToString() != "")
                    lblNotation.Text = NtwNotation.ToString();
                else
                    DetailsView1.Fields[7].Visible = false;

         
   }


         



}