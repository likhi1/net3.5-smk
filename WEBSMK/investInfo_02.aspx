﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="investInfo_02.aspx.cs" Inherits="investInfo_02" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
 <div class="subject1">คณะกรรมการและผู้บริหาร</div>  
<div class="subject2">คณะกรรมการ</div>  
<table class="tb-ver-center" >
                    <tr  >
                      <th width="169"  >รายนาม</th>
                      <th width="431" >ตำแหน่ง</th>
                    </tr>
                    <tr >
                      <td class="align-left" >นายเรืองวิทย์ ดุษฎีสุรพจน์</td>
                      <td class="align-left" >กรรมการและประธานกรรมการ</td>
                    </tr>
                    <tr >
                      <td class="align-left" >นายเรืองเดช ดุษฎีสุรพจน์</td>
                      <td class="align-left" >กรรมการและประธานกรรมการบริหารกรรมการผู้จัดการ และประธานกรรมการลงทุน</td>
                    </tr>
                    <tr >
                      <td class="align-left" >นางสุวิมล ชยวรประภา</td>
                      <td class="align-left" >กรรมการ  กรรมการบริหาร  และกรรมการลงทุน</td>
                    </tr>
                    <tr >
                      <td class="align-left" >นายพอล  จอห์น  ฮุสตัน</td>
                      <td class="align-left" >กรรมการ  (เป็นตัวแทนผู้ถือหุ้นกลุ่ม Royal & SunAlliance Insurance Group)</td>
                    </tr>
                    <tr >
                      <td class="align-left" >นายประดิษฐ รอดลอยทุกข์</td>
                      <td class="align-left" >กรรมการ กรรมการบริหาร กรรมการลงทุน และประธานกรรมการบริหารความเสี่ยง</td>
                    </tr>
                    <tr >
                      <td class="align-left" >นางอัญชุลี คุณวิบูลย์</td>
                      <td class="align-left" >กรรมการ</td>
                    </tr>
                    <tr >
                      <td class="align-left" >นายอนุชาต ชัยประภา</td>
                      <td class="align-left" >กรรมการ กรรมการอิสระ ประธานกรรมการตรวจสอบและกรรมการบริหารความเสี่ยง</td>
                    </tr>
                    <tr >
                      <td class="align-left" >นายวิเศษ ภานุทัต</td>
                      <td class="align-left" >กรรมการ กรรมการอิสระ และกรรมการตรวจสอบ</td>
                    </tr>
                    <tr >
                      <td class="align-left" >นายสุวิชากร ชินะผา</td>
                      <td class="align-left" >กรรมการ กรรมการอิสระ กรรมการตรวจสอบและกรรมการบริหารความเสี่ยง </td>
                    </tr>
                    <tr >
                      <td class="align-left" >นางสาวถนอมศรี สินสุขเพิ่มพูน</td>
                      <td class="align-left" >กรรมการ และเลขานุการบริษัท</td>
                    </tr>
                    </table>
                  
       




<div class="subject2">เจ้าหน้าที่บริหาร</div>
                    <table class="tb-ver-center"  >
                    <tr   >
                      <th width="30%"  >รายนาม</th>
                      <th   >ตำแหน่ง</th>
                     </tr>
             
                    <tr >
                      <td class="align-left"  >นายสุทิพย์ รัตนรัตน์</td>
                      <td class="align-left" >SEVP ผู้ช่วยกรรมการผู้จัดการสินไหมรถยนต์</td>
   </tr>
                    <tr >
                      <td class="align-left" >นายประหยัด ฐิตะธรรมกุล</td>
                      <td class="align-left" >EVP ผู้ช่วยกรรมการผู้จัดการบริหารภาค</td>
                     </tr>
                    <tr >
                      <td class="align-left" >นางสาวพรรณี ปิติกุลตัง</td>
                      <td class="align-left" >EVP ผู้ช่วยกรรมการผู้จัดการพัฒนาระบบ บริการ ศูนย์ข้อมูล และ Tele-Marketing</td>
                     </tr>
                    <tr >
                      <td class="align-left" >นางสาวถนอมศรี สินสุขเพิ่มพูน</td>
                      <td class="align-left" >EVP ผู้ช่วยกรรมการผู้จัดการการเงิน</td>
                     </tr>
                    <tr >
                      <td class="align-left" >นางวีณา นิรมานสกุล</td>
                      <td class="align-left" >EVP ผู้ช่วยกรรมการผู้จัดการบัญชี และบริหารลูกหนี้</td>
   </tr>
                    <tr >
                      <td class="align-left" >นายนคร ต่อเจริญ</td>
                      <td class="align-left" >EVP ผู้จัดการฝ่ายบริหารงานตัวแทน</td>
   </tr>
    
                    <tr >
                      <td class="align-left" >นายนุสนธิ นิลวงศ์</td>
                      <td class="align-left" >SVP ผู้จัดการฝ่ายสินไหมรถยนต์กรุงเทพ</td>
   </tr>
					<tr >
                      <td class="align-left" >นายอัครนันท์  จินดาวัฒน์ธนโชค</td>
                      <td class="align-left" >SVP ผู้จัดการฝ่ายอัคคีภัย</td>
   </tr>
                    <tr >
                      <td class="align-left" >นายธารา วนลาภพัฒนา</td>
                      <td class="align-left" >SVP ผู้จัดการฝ่ายการลงทุน</td>
   </tr>
                    <tr >
                      <td class="align-left" >นายประภากร ปิณฑะดิษ</td>
                      <td class="align-left" >VP ผู้จัดการฝ่ายกฎหมาย</td>
   </tr>
                    <tr >
                      <td class="align-left" >นายวิชิระ ลอยชัยภูมิ</td>
                      <td class="align-left" >VP ผู้จัดการฝ่ายเทคโนโลยีสารสนเทศ</td>
   </tr>
                    <tr >
                      <td class="align-left" >นางสาวอรพิน อนันตรกิตติ</td>
                      <td class="align-left" >VP ผู้จัดการฝ่าย
                      บริหารทรัพยากรบุคคล</td>
   </tr>
                    <tr >
                      <td class="align-left" >นายชาติชาย วงศ์ตรีรัตนชัย</td>
                      <td class="align-left" >VP ผู้จัดการภาคภาคเหนือ</td>
   </tr>
                    <tr >
                      <td class="align-left" >นางสาววลีรัตน์ กุลวิโรจน์</td>
                      <td class="align-left" >VP ผู้จัดการฝ่ายจัดเก็บเบี้ยประกัน</td>
   </tr>
                    <tr >
                      <td class="align-left" >นายอมรศักดิ์ ศรีมงคลชัย</td>
                      <td class="align-left" >VP ผู้จัดการฝ่ายตรวจสอบภายใน</td>
   </tr>
                    <tr >
                      <td class="align-left" >นางสาวมนทรัตน์  นาวารัตน์</td>
                      <td class="align-left" >VP ผู้จัดการศูนย์Call Center</td>
   </tr>
                    <tr >
                      <td class="align-left" >นายประสิทธิ์ ดุษฎีสุรพจน์</td>
                      <td class="align-left" >VP ผู้จัดการฝ่าย
                        การตลาดสายสถาบันการเงิน</td>
   </tr>
                    <tr >
                      <td class="align-left" >ดร.วชิระ ชนะบุตร</td>
                      <td class="align-left" >VP ผู้จัดการศูนย์
                      พัฒนาทรัพยากรบุคคล</td>
   </tr> 
                    <tr >
                      <td class="align-left" >นายสมบูรณ์ เจริญศรี</td>
                      <td class="align-left" >VP ผู้จัดการฝ่าย
                        อะไหล่</td>
   </tr>
                    <tr >
                      <td class="align-left" >นายคงศักดิ์ ลีละศรชัย</td>
                      <td class="align-left" >VP ผู้จัดการฝ่าย
                        สินไหมรถยนต์กลาง ( ประเมินราคา )</td>
   </tr>
                    <tr >
                      <td class="align-left" >นางสาวศรันยา สอนดี</td>
                      <td class="align-left" >VP ผู้จัดการฝ่าย
                        ธุรการ</td>
   </tr>

                    <tr >
                      <td class="align-left" >นายวิวัฒน์ สุนทรชัยนุกูล</td>
                      <td class="align-left" >VP รองผู้จัดการฝ่าย
                        สินไหมรถยนต์ภูมิภาค</td>
   </tr>
                    <tr >
                      <td class="align-left" >นายมงคล บุญชูวงศ์</td>
                      <td class="align-left" >AVP ผู้ช่วยผู้จัดการ
                      ภาคกลาง และภาคตะวันออก</td>
   </tr>
                    <tr >
                      <td class="align-left" >นายดำหริ เถื่อนชื่น</td>
                      <td class="align-left" >AVP ผู้ช่วยผู้จัดการ
                      ภาคเหนือ</td>
   </tr>
                    <tr >
                      <td class="align-left" >นายวิเชียร ประทักษากุล</td>
                      <td class="align-left" >AVP ผู้ช่วยผู้จัดการ
                      ภาคใต้</td>
   </tr>
                  </table>
                  
                  
                
</asp:Content>

