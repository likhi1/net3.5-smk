﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//--- connect
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
//--- arrayList
using System.Collections;

public partial class networkSearch : System.Web.UI.Page {

    protected int CatId {
        get {
            int catId;
            if (string.IsNullOrEmpty(Request.QueryString["cat"])) {
                catId = 1;
            } else {
                catId = Convert.ToInt32(Request.QueryString["cat"]);
            }
            return catId;
        }
    }
    protected int ZoneId {
        get {
            int zoneId;
            if (string.IsNullOrEmpty(Request.QueryString["zone"])) {
                zoneId = 1;
            } else {
                zoneId = Convert.ToInt32(Request.QueryString["zone"]);
            }
            return zoneId;
        }
    }

    protected string PageName;
    protected DataTable DtCat;
     
    //==============  Set Create varible ;
    protected string Type;
    protected string Pro;
    protected string Amp;
    protected string Key;

    protected void Page_Load(object sender, EventArgs e) { 
   
        ///// BindDropDown 
        SelectCat();
        ddlProvinceBindData();
        ddlAmphoeBindData();
        ddlBranchTypeBindData(CatId);

        /////// Set Pagename
        GetNamePage();
        lblPageName.Text = PageName;

        if (!Page.IsPostBack) {
            ///// Set Varible from QueryString  => from Page network.aspx
            Type = Request.QueryString["type"];
            Pro = Server.HtmlDecode(Request.QueryString["pro"]);
            Amp = Server.HtmlDecode(Request.QueryString["amp"]);
            Key = Server.HtmlDecode(Request.QueryString["key"]);  
               
            BindData();
        } else {
            ///// Set Varible from => Page networkSearch.aspx
            Type = ddlNetworkType.SelectedValue ;
            Pro = ddlProvince.SelectedValue  ;
            Amp = ddlAmphoe.SelectedValue  ;
            Key = tbxKeyWord.Text  ;

        }

        ///// Set select & Show Data
        ddlNetworkType.SelectedValue = Type;
        ddlProvince.SelectedValue = Pro;
        ddlAmphoe.SelectedValue = Amp;
        tbxKeyWord.Text = Key;

    }
    

    protected void BindData() {
        DataSet ds = SelectData( );
        DataTable dt1 = ds.Tables[0];

        int RowCount = dt1.Rows.Count;

        //=== Set Pager From code Behind
        GridView1.AllowPaging = true;
        GridView1.PageSize = 20;
        GridView1.PagerStyle.CssClass = "cssPager";

        //===== Bind  Data
        GridView1.DataSource = dt1;
        GridView1.DataBind();

    }

    protected DataSet SelectData() {

        string sql1 = "SELECT tbNtw.* , tbNtwCat.NtwCatId ,   tbNtwZone.NtwZoneId ";
        if (Type != "") {
            sql1 += " , tbNtwType.NtwTypeId  ";
        }

        sql1 += "FROM  tbNtw "
        + "LEFT JOIN tbNtwCat ON tbNtw.NtwCatId  = tbNtwCat.NtwCatId "
        + "LEFT JOIN tbNtwZone ON tbNtw.NtwZoneId  = tbNtwZone.NtwZoneId  ";


        if (Type != "") {
            sql1 += "LEFT JOIN tbNtwType ON tbNtw.NtwTypeId  = tbNtwType.NtwTypeId  ";
        }

        sql1 += "WHERE  tbNtw.NtwStatus ='1'   "
        + "AND  tbNtw.NtwCatId  ='" + CatId + "'  "
        + "AND lang = '0' ";

        if (Type != "") {
            sql1 += "AND   tbNtw.NtwTypeId  = " + Type + " ";
        }

        if (Pro != "") {
            //sql1 += "AND   tbNtw.NtwProvince  = '" + Pro + "'  ";
            //sql1 += "AND (NtwName   LIKE  '%" + Pro + "%'  OR   "
            //    + "NtwProvince   LIKE  '%" + Pro + "%'   "
            //     + " )   ";
            // KKN 2014-06-13
            sql1 += "AND  NtwProvince  like  '%" + Pro + "%'  ";
        }

        if (Amp != "") {
            // sql1 += "AND   tbNtw.NtwAmphoe  = '" + Amp + "'  ";
            sql1 += "AND (NtwName   LIKE  '%" + Amp + "%'  OR   "
                  + "NtwProvince   LIKE  '%" + Amp + "%'  OR   "
                  + "NtwAmphoe    LIKE  '%" + Amp + "%'  OR   "
                  + "NtwAddress   LIKE  '%" + Amp + "%'  OR   "
                  + "NtwService   LIKE  '%" + Amp + "%' )   ";
        }

        if (Key != "") {
            sql1 += "AND (NtwName   LIKE  '%" + Key + "%'  OR   "
                    + "NtwProvince   LIKE  '%" + Key + "%'  OR   "
                    + "NtwAmphoe    LIKE  '%" + Key + "%'  OR   "
                    + "NtwAddress   LIKE  '%" + Key + "%'  OR   "
                    + "NtwService   LIKE  '%" + Key + "%' )   ";
        }



        sql1 += "ORDER BY tbNtw.NtwSort ASC , tbNtw.NtwName ASC ,   tbNtw.NtwId DESC   ";    // Sort By NtwSort  &   NtwName  &  NtwId  


        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql1, "tbNtwData");

        return ds;

    }

    //======================  Set DropDown  
    protected DataTable SelectCat() {
        string sql = "SELECT  NtwCatId  ,  NtwCatName  FROM tbNtwCat  ORDER BY NtwCatId ";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "_tb");
        DtCat = ds.Tables[0];
        return DtCat;
    }

    protected void ddlBranchTypeBindData(int CatId) {
        string sql = "SELECT * FROM tbNtwType WHERE NtwTypeCat='" + CatId + "'  ORDER BY NtwTypeId";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "_tb");

        ddlNetworkType.DataSource = ds;
        //**** Add frist Item of dropdownlist
        ddlNetworkType.AppendDataBoundItems = true;
        ddlNetworkType.DataTextField = "NtwTypeNameTh";
        ddlNetworkType.DataValueField = "NtwTypeId";
        ddlNetworkType.DataBind();
    }

    protected void ddlProvinceBindData() {
        string sql = "SELECT ds_major_cd , RTRIM(ds_desc_t) AS ds_desc_t  FROM setcode  WHERE (ds_major_cd = 'CH')  ORDER BY  ds_desc_t  ";

        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "tb");

        ddlProvince.DataSource = ds;
        //**** Add frist Item of dropdownlist
        ddlProvince.AppendDataBoundItems = true;
        ddlProvince.DataTextField = "ds_desc_t";
        //ddlProvince.DataValueField = "ds_desc_t";
        ddlProvince.DataBind();

    }
     
    protected void ddlAmphoeBindData() {
        string sql = "SELECT ds_major_cd , ds_desc , RTRIM(ds_desc_t) AS ds_desc_t  FROM setcode  WHERE (ds_major_cd = 'AM') AND (ds_desc = '0') ORDER BY  ds_desc_t  ";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "tb");

        ddlAmphoe.DataSource = ds;
        //**** Add frist Item of dropdownlist
        ddlAmphoe.AppendDataBoundItems = true;
        ddlAmphoe.DataTextField = "ds_desc_t";
        // ddlAmphoe.DataValueField = "ds_desc_t";
        ddlAmphoe.DataBind();
    }

    //=======================  Set PageName
    protected string GetNamePage() {
        foreach (DataRow r in DtCat.Rows) {
            if ((int)r[0] == CatId) {
                PageName = r[1].ToString();
            }
        }
        return PageName;
    }

    //======================= ?????
    protected ArrayList ddlConSortAddList() {
        ArrayList arl = new ArrayList();
        for (int i = 1; i <= 10; i++) {
            arl.Add(i);
        }
        return arl;
    }

    protected DataSet SelectZone() {
        string sql = "SELECT * From tbNtwZone";
        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql, "_tbNtwZone");
        return ds;
    }
     
    //======================== Set  Event 
    protected void BtnSearch_Click(object sender, EventArgs e) {

        //=========  Set  Renew  Seession 
        //Session["pro"] = ddlProvince.SelectedValue;
        //Session["amp"] = ddlAmphoe.SelectedValue;
        //Session["key"] = tbxKeyWord.Text;
        //Session["type"] = ddlNetworkType.SelectedValue;

        //======= Serach By QueryString 
        Type = ddlNetworkType.SelectedValue;
        Pro = Server.HtmlEncode(ddlProvince.SelectedValue);
        Amp = Server.HtmlEncode(ddlAmphoe.SelectedValue);
        Key = Server.HtmlEncode(tbxKeyWord.Text);

        Response.Redirect("networkSearch.aspx?cat=" + CatId + "&zone=" + ZoneId + "&type=" + Type + "&pro=" + Pro + "&amp=" + Amp + "&key=" + Key);

    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e) {
        if (e.Row.RowType == DataControlRowType.DataRow) {

            Label lblName = (Label)(e.Row.FindControl("lblName"));
            if (lblName != null) {
                lblName.Text = DataBinder.Eval(e.Row.DataItem, "NtwName").ToString();
                lblName.Style.Add("font-weight", "bold"); 
            }

            Label lblAddress = (Label)(e.Row.FindControl("lblAddress"));
            if (lblAddress != null) {
                lblAddress.Text = DataBinder.Eval(e.Row.DataItem, "NtwAddress").ToString();
            }

            Label lblMobile = (Label)(e.Row.FindControl("lblMobile"));
            if (lblMobile != null) {
                string strMobile = DataBinder.Eval(e.Row.DataItem, "NtwTelMobile").ToString();
                lblMobile.Text = (strMobile != "" ? "มือถือ : " : "") + strMobile;
            }

            Label lblPhone = (Label)(e.Row.FindControl("lblPhone"));
            if (lblPhone != null) {
                string strPhone = DataBinder.Eval(e.Row.DataItem, "NtwTelPhone").ToString();
                lblPhone.Text = (strPhone != "" ? "โทรศัพท์ : " : "") + strPhone;
            }

            Label lblProvince = (Label)(e.Row.FindControl("lblProvince"));
            if (lblProvince != null) {
                lblProvince.Text = DataBinder.Eval(e.Row.DataItem, "NtwProvince").ToString();
                lblProvince.Attributes.Add("style", "text-align:center");
            }

            HyperLink hplMap = (HyperLink)(e.Row.FindControl("hplMap"));
            if (hplMap != null) {
                string strNtwId = DataBinder.Eval(e.Row.DataItem, "NtwId").ToString();
                hplMap.CssClass = "openerCover";
                hplMap.Text = "ดูแผนที่";
                hplMap.NavigateUrl = "networkMap.aspx?id=" + strNtwId;
                hplMap.Attributes.Add("OnClick", "clickModal();return false ;");
            }

            HyperLink hplWebsite = (HyperLink)(e.Row.FindControl("hplWebsite"));
            object NtwWebsite = DataBinder.Eval(e.Row.DataItem, "NtwWebsite");
            if (NtwWebsite.ToString() != "") {
                if (hplWebsite != null && !Convert.IsDBNull(NtwWebsite)) {
                    string strNtwWebsite = NtwWebsite.ToString().Replace("http://", "");
                    hplWebsite.NavigateUrl = "http://" + NtwWebsite.ToString();
                    hplWebsite.Target = "blank";
                    hplWebsite.Text = NtwWebsite.ToString();
                }
            } else {
                hplWebsite.Visible = false;
            }
             

            Label lblService = (Label)(e.Row.FindControl("lblService"));
            string NtwService = DataBinder.Eval(e.Row.DataItem, "NtwService").ToString();
            if (!String.IsNullOrEmpty(NtwService)) {
                lblService.Text = "(" + NtwService + ")";
                lblService.Style.Add("color", "#F17503");
            }


            Label lblNotation = (Label)(e.Row.FindControl("lblNotation"));
            if (lblNotation != null) {
                string strNotation = DataBinder.Eval(e.Row.DataItem, "NtwNotation").ToString();
                lblNotation.Text = (strNotation != "" ? "หมายเหตุ : " : "") + strNotation;
            }

 

            Label lblAmphoe = (Label)(e.Row.FindControl("lblAmphoe"));
            if (lblAmphoe != null) {
                string strAmphoe = DataBinder.Eval(e.Row.DataItem, "NtwAmphoe").ToString();
                if (!String.IsNullOrEmpty(strAmphoe)) {
                    lblAmphoe.Text = "<br>"; 
                    lblAmphoe.Text += "(" + strAmphoe + ")";
                    lblAmphoe.Attributes.Add("style", "text-align:center");
                    //  lblAmphoe.Style.Add("color", "#038DFA");
                }
            }

        }
    }

    protected void GridView1_Deleting(object s, GridViewDeleteEventArgs e) {
        int a = e.RowIndex;
        string sql2 = "DELETE  FROM  tbNtw  WHERE NtwId = '" + GridView1.DataKeys[e.RowIndex].Value + "'";
        int i = new DBClass().SqlExecute(sql2);
        GridView1.EditIndex = -1;
        BindData();
    }

    //====================== Set  Pager
    protected void GridView1_IndexChanging(object sender, GridViewPageEventArgs e) {
        GridView1.PageIndex = e.NewPageIndex;
        BindData();
    }

    //======================  Set Gridview
    protected void modEditCommand(object sender, GridViewEditEventArgs e) {
        GridView1.EditIndex = e.NewEditIndex;
        BindData();
    }

    protected void modCancelCommand(object sender, GridViewCancelEditEventArgs e) {
        GridView1.EditIndex = -1;
        BindData();
    }

    protected void modUpdateCommand(object sender, GridViewUpdateEventArgs e) {


        DropDownList ddlStatus = (DropDownList)GridView1.Rows[e.RowIndex].FindControl("ddlStatus");
        TextBox tbxSort = (TextBox)GridView1.Rows[e.RowIndex].FindControl("tbxSort");

        string strSQL = "UPDATE tbNtw SET " +

             "NtwStatus = '" + ddlStatus.Text + "', " +
             "NtwSort = '" + tbxSort.Text + "' " +
             " WHERE  NtwId = '" + GridView1.DataKeys[e.RowIndex].Value + "'";

        DBClass obj = new DBClass();
        int i = obj.SqlExecute(strSQL);

        GridView1.EditIndex = -1;
        BindData();
    }






  
}