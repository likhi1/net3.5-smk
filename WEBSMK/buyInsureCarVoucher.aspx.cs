﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class buyOnlineInsCarVoucher : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string strPremGar;
        string strPremService;
        DataSet ds;
        OnlineVoluntaryManager cmOnline = new OnlineVoluntaryManager();
        
        strPremGar = Request.QueryString["prem_gar"];
        strPremService = Request.QueryString["prem_service"];
        
        ds = cmOnline.getDataVoucherByPrem(strPremGar);
        if (ds.Tables[0].Rows.Count > 0)
        {
		    lblVoucherGar.Text =  "<div class='nameVoucher'>ซ่อมอู่รับ Gift Voucher มูลค่า </div>";
            lblVoucherGar.Text +=  "<div class='priceVoucher'>" +FormatStringApp.FormatInt(ds.Tables[0].Rows[0]["gvou_value"]) + " บาท </div> ";
        }
        ds.Clear();
        ds = cmOnline.getDataVoucherByPrem(strPremService);
        if (ds.Tables[0].Rows.Count > 0)
        {
		 lblVoucherService.Text = "<div  class='nameVoucher'>ซ่อมศูนย์รับ Gift Voucher มูลค่า </div>";
            lblVoucherService.Text += "<div class='priceVoucher'>" +FormatStringApp.FormatInt(ds.Tables[0].Rows[0]["gvou_value"]) + " บาท </div>";
        }

    }
}
