﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPageFront2_th.master" CodeFile="network.aspx.cs" Inherits="network" %>

<%@ Register Src="~/adminWeb/uc/ucZone.ascx" TagPrefix="myUc" TagName="ucZone" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server"> 
<!-- ===============  Modal  --> 
  
<script>
    $(function () { 

    $("[id*=dialog]").dialog({
        autoOpen: false,
        show: "fade",
        hide: "fade",
        modal: true,
        //open: function (ev, ui) {
        //    $('#myIframe').src = 'http://www.google.com';
        //},
        //resizable: true,  
        //title: 'Title Topic',
        height: '550',
        width: '985',
        dialogClass: 'yourclassname',
        headerVisible: false ,// or true
        draggable: false, 
        buttons: { CLOSE: function () { $(this).dialog("close"); } }
                
    }) 

    ////  Set  Attr  Link For Iframe
    areaIframe1 = $("#areaIframe1");
    $(".openerCover").click(function (e) {
        e.preventDefault();

        link = $(this).attr('href');
        areaIframe1.attr('src', link);

        $("#dialog1").dialog("open");
        //   return false;
    })

    ///  Hide Title Bar
    $(".ui-dialog-titlebar").hide();
    $("#dialog1").css("padding", 10); 
});
</script> 

 
    
<!-- =============== Start Overlapping Tabs  --> 
<script>
$(function () {
    ///////////////// Control Overlab Tabs 
    $('.obtabs li ').click(function () {
        // set active
        //$(this).parent('li').removeClass('current');
            $('.obtabs li').removeAttr("id");
                  
        // $(this).addClass('current');
            $(this).attr("id", "current");
        // Set Hide
        $("#selectData div[id^=tabMenu]").hide();
        //Set Show 
        elmSelect = $(this).children().children().attr("rel");
        console.log(elmSelect);
        $("#selectData #" + elmSelect).show();
    })
});
</script>

<!-- =============== Set Show/Hide Map  -->   
<script>
$(function () {  
$('#areaSmkMap2').hide();
$('#btnSmkMap1').click(function () {
    $('#areaSmkMap2').fadeOut();
    $('#areaSmkMap1').fadeIn();
})
$('#btnSmkMap2').click(function () {
    $('#areaSmkMap1').fadeOut();
    $('#areaSmkMap2').fadeIn();
}) 
});
</script>

               <!-- ===============  Limit  Select DropDownList  -->
     <script>
         $(function () {
             ddlProvince = $("#<%=ddlProvince.ClientID%>");
             ddlAmphoe = $("#<%=ddlAmphoe.ClientID%>");
             areaAmphoe = $("#areaAmphoe");
              

             ddlProvince.change(function () {
                 if (ddlProvince.val() != "กรุงเทพมหานคร") {
                     ddlAmphoe.val('');
                     areaAmphoe.hide();  
                     // ddlAmphoe.attr("disabled", "disabled");
                 } else {
                     areaAmphoe.show();
                     //ddlAmphoe.removeAttr("disabled");
                 }
             })

         });
   </script> 


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div id="page-network">
        <h1 class="subject1">เครือข่าย 
            <asp:Label ID="lblPageName" runat="server"  ></asp:Label></h1>

      <%-- <asp:Label ID="Label1" runat="server" Text="Label" ></asp:Label>--%>



 <div id="search-area"> 

<div style="float:left;margin-right:5px;">
เลือกจังหวัด : </span><asp:DropDownList ID="ddlProvince" runat="server">  <asp:ListItem Value="">-- โปรดระบุ  --</asp:ListItem></asp:DropDownList>  
</div> 
<div style="float:left;margin-right:5px; display:none" id="areaAmphoe">
เลือกเขต : <asp:DropDownList ID="ddlAmphoe" runat="server">  <asp:ListItem Value="">-- โปรดระบุ  --</asp:ListItem></asp:DropDownList>  
</div> 
<div style="float:left;margin-right:5px;">
เลือกคำค้น :  <asp:TextBox ID="tbxKeyWord" runat="server" Width="100px"  ></asp:TextBox>   
</div> 
<div style="float:left;margin-right:5px;">  
<asp:Button ID="Button1" runat="server" Text="ค้นหา" class="btn-default" style="height:22px;"  OnClick="BtnSearch_Click"  /> 
</div> 

<%--
<div style="float:left;margin-right:10px;">
เลือกภาค : <myUc:ucZone runat="server" ID="ucZone" />
&nbsp;&nbsp;    เลือกคำค้นหา :
<asp:TextBox ID="tbxKeyWord" runat="server"></asp:TextBox>
</div> 
--%>
              
        </div>
        <!--  End id="search-area" -->

        <div class="clear"></div>

          <!--=============  Start  Overlapping Tabs ===========--> 
<a name="grd"></a> 
        <%if (Convert.ToUInt32(CatId) == 4) { %> 
        <script>
            $(function () {
                $("#tabMenu0").show();
            });
        </script>
       
        <div id="tabOvl-network1"> 
            <ul class="obtabs"> 
                <li id="current"><span><a href="#" rel="tabMenu0" name="grd">สำนักงาน<br />ใหญ่</a></span></li> 
                <li><span><a href="#" rel="tabMenu1" name="grd">กรุงเทพฯ<br />& ปริมณฑล</a></span></li>
                <li><span><a href="#" rel="tabMenu2" name="grd">กลาง</a></span></li>
                <li><span><a href="#" rel="tabMenu3" name="grd">ตะวันออก</a></span></li>
                <li><span><a href="#" rel="tabMenu4" name="grd">ตะวันออกเฉียงเหนือ</a></span></li>
                <li><span><a href="#" rel="tabMenu5" name="grd">เหนือ</a></span></li>
                <li><span><a href="#" rel="tabMenu6" name="grd">ใต้</a></span></li>
                <li><span><a href="#" rel="tabMenu7" name="grd">ตะวันตก</a></span></li>
            </ul>
        </div> 
        
        <% }else{ %>
            <script>
            $(function () {
            $("#tabMenu1").show();
            });
            </script>

            <div id="tabOvl-network2"> 
            <ul class="obtabs">  
                <li id="current"><span><a href="#" rel="tabMenu1" name="grd">กรุงเทพฯ<br />& ปริมณฑล</a></span></li>
                <li><span><a href="#" rel="tabMenu2" name="grd">กลาง</a></span></li>
                <li><span><a href="#" rel="tabMenu3" name="grd">ตะวันออก</a></span></li>
                <li><span><a href="#" rel="tabMenu4" name="grd">ตะวันออก<br />เฉียงเหนือ</a></span></li>
                <li><span><a href="#" rel="tabMenu5" name="grd">เหนือ</a></span></li>
                <li><span><a href="#" rel="tabMenu6" name="grd">ใต้</a></span></li>
                <li><span><a href="#" rel="tabMenu7" name="grd">ตะวันตก</a></span></li>
            </ul>
        </div>  
         <% }  %>
       <!--============= End  Overlapping Tabs ===========--> 


        <style>
        /*===== Control  GridView  Width (support IE9 & Over) =====*/
        [id*=GridView] { width:100% ;  /*border: 1px solid red;*/  }
        [id*=GridView] th {height:20px;   color:#fff ;background-color:#485ed1; padding:5px 5px 7px 5px; font-size:13px;  vertical-align:top ; border : 1px solid #fff;  } 
        [id*=GridView] th:nth-child(1) {   }
        [id*=GridView] th:nth-child(2)  { width: 15%;  text-align:center ;  }
        [id*=GridView] th:nth-child(3)  { width: 10%;  text-align:center ;  } 
        [id*=GridView] td {padding:2px 5px;height:20px; line-height: 18px; 
                           vertical-align:top; border : 1px dotted #ddd;   }
        [id*=GridView] td .name{  }
        [id*=GridView] td:nth-child(2) {text-align:center ; }
        [id*=GridView] td:nth-child(3) {text-align:center ; }
         
        </style>



            <div id="selectData"> 
                   <%if (Convert.ToUInt32(CatId) == 4) { %>
                <div id="tabMenu0" style="display:none">
                    <%--<h1>Content 0</h1> --%>
                    <table class="tb-network" width="100%">

                        <tr>
                            <td valign="top" class="align-left" colspan="3">
                                
                                <div class="network-name">สำนักงานใหญ่ บริษัท สินมั่นคงประกันภัย จำกัด (มหาชน)</div>
                                <div class="network-address">ลำสาลี 1 เขต บางกะปิ บางกะปิ กรุงเทพมหานคร 10240</div>
                                <div class="network-tel">โทรศัพท์ : 02 378 7000 แฟกต์ : 02 378 7000  </div>

                                <br />

                                <input type="button" id="btnSmkMap1" value="แผนที่ ขนาดย่อ" class="btn-default" style="margin-right: 10px;" />
                                <input type="button" id="btnSmkMap2" value="แผนที่ Google Map" class="btn-default" />
                                <br />

                                <br />
                                <div id="areaSmkMap1" style="border : solid 1px #ddd;">
                                    <img src="images/smkCenterMap2.jpg"  width="630px"  />
                                </div> 


                                <div id="areaSmkMap2">
                                    <iframe width="630" height="380" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/ms?msa=0&amp;msid=205759285013772248066.0004dbc87988f8deeea5f&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;ll=13.74965,100.644754&amp;spn=0,0&amp;output=embed"></iframe><br /><small>View <a href="https://www.google.com/maps/ms?msa=0&amp;msid=205759285013772248066.0004dbc87988f8deeea5f&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;ll=13.74965,100.644754&amp;spn=0,0&amp;source=embed" style="color:#0000FF;text-align:left">สินมั่นคง</a> in a larger map</small>
                                </div>


                                <br />
                                <br />
                                <br />
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- End  id="tabMenu0"-->
                <%} %>

                <div id="tabMenu1" style="display:none" >
                   <%-- <h1>Content 1</h1>--%>
                    <asp:GridView ID="GridView1" runat="server" EnableModelValidation="True"
                        DataKeyNames="NtwId"
                        OnRowDataBound="GridView_RowDataBound" 
                        AutoGenerateColumns="false" 
                        OnPageIndexChanging="GridView1_IndexChanging"  >
                        <Columns>

                            <asp:TemplateField   HeaderText="ชื่อ-ที่อยู่" >
                                <ItemTemplate>
<div class="rowAddress">

<div  class="name">
<asp:Label runat="server" ID="lblName"></asp:Label>
<asp:Label runat="server" ID="lblService"></asp:Label> 
</div>  
      
<div  ><asp:Label runat="server" ID="lblAddress"></asp:Label></div>
<div ><asp:Label runat="server" ID="lblMobile"></asp:Label></div>
<div ><asp:Label runat="server" ID="lblPhone"></asp:Label></div>
 <div  ><asp:Label runat="server" ID="lblNotation"></asp:Label></div>

</div>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField   HeaderText="จังหวัด/อำเภอ" >
                                <ItemTemplate>

<asp:Label runat="server" ID="lblProvince"></asp:Label>
<asp:Label runat="server" ID="lblAmphoe"></asp:Label>


                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField    HeaderText="แผนที่" >
                                <ItemTemplate>
                                    <asp:HyperLink runat="server" ID="hplMap"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField> 

                        </Columns>
                    </asp:GridView>
                </div>
                <!-- End  id="tabMenu1"-->
                <div id="tabMenu2" style="display:none" >
                  <%--  <h1>Content 2</h1>--%>
                     <asp:GridView ID="GridView2" runat="server" EnableModelValidation="True"
                        DataKeyNames="NtwId"
                        OnRowDataBound="GridView_RowDataBound"
                        
                        AutoGenerateColumns="false" 
                        >
                        <Columns>

                            <asp:TemplateField   HeaderText="ชื่อ-ที่อยู่" >
                                <ItemTemplate>
<div class="rowAddress"> 
<div  class="name">
    <asp:Label runat="server" ID="lblName"></asp:Label>
    <asp:Label runat="server" ID="lblService"></asp:Label> 
</div> 
                                     
<div ><asp:Label runat="server" ID="lblAddress"></asp:Label></div>
<div  ><asp:Label runat="server" ID="lblMobile"></asp:Label></div>
<div ><asp:Label runat="server" ID="lblPhone"></asp:Label></div>
     <div  ><asp:Label runat="server" ID="lblNotation"></asp:Label></div>
</div>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField   HeaderText="จังหวัด" >
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblProvince"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField    HeaderText="แผนที่" >
                                <ItemTemplate>
                                    <asp:HyperLink runat="server" ID="hplMap"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField> 

                        </Columns>
                    </asp:GridView> 
                </div>
                <!-- End  id="tabMenu2"-->
                <div id="tabMenu3" style="display:none" >
                   <%-- <h1>Content 3</h1>--%>
                      <asp:GridView ID="GridView3" runat="server" EnableModelValidation="True"
                        DataKeyNames="NtwId"
                        OnRowDataBound="GridView_RowDataBound"
                        
                        AutoGenerateColumns="false" 
                        >
                        <Columns>

                            <asp:TemplateField   HeaderText="ชื่อ-ที่อยู่" >
                                <ItemTemplate>
                                    <div class="rowAddress">
<div  class="name">
<asp:Label runat="server" ID="lblName"></asp:Label>
<asp:Label runat="server" ID="lblService"></asp:Label> 
</div>                                
                                    <div  ><asp:Label runat="server" ID="lblAddress"></asp:Label></div>
                                    <div ><asp:Label runat="server" ID="lblMobile"></asp:Label></div>
                                    <div ><asp:Label runat="server" ID="lblPhone"></asp:Label></div>
                                         <div ><asp:Label runat="server" ID="lblNotation"></asp:Label></div>

</div>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField   HeaderText="จังหวัด" >
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblProvince"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField    HeaderText="แผนที่" >
                                <ItemTemplate>
                                    <asp:HyperLink runat="server" ID="hplMap"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField> 

                        </Columns>
                    </asp:GridView> 
                </div>
                <!-- End  id="tabMenu3"-->

                <div id="tabMenu4"  style="display:none" >
                    <%--<h1>Content 4</h1>--%>
                      
                      <asp:GridView ID="GridView4" runat="server" EnableModelValidation="True"
                        DataKeyNames="NtwId"
                        OnRowDataBound="GridView_RowDataBound"
                        
                        AutoGenerateColumns="false" 
                        >
                        <Columns>

                            <asp:TemplateField   HeaderText="ชื่อ-ที่อยู่" >
                                <ItemTemplate>
                                    <div class="rowAddress">
<div  class="name">
<asp:Label runat="server" ID="lblName"></asp:Label>
<asp:Label runat="server" ID="lblService"></asp:Label> 
</div>    
                                     
                                    <div  ><asp:Label runat="server" ID="lblAddress"></asp:Label></div>
                                    <div  ><asp:Label runat="server" ID="lblMobile"></asp:Label></div>
                                    <div  ><asp:Label runat="server" ID="lblPhone"></asp:Label></div>
                                         <div  ><asp:Label runat="server" ID="lblNotation"></asp:Label></div>

</div>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField   HeaderText="จังหวัด" >
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblProvince"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField    HeaderText="แผนที่" >
                                <ItemTemplate>
                                    <asp:HyperLink runat="server" ID="hplMap"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField> 

                        </Columns>
                    </asp:GridView> 
                </div>
                <!-- End  id="tabMenu4"-->

                <div id="tabMenu5" style="display:none" >
                   <%-- <h1>Content 5</h1>--%>
                  <asp:GridView ID="GridView5" runat="server" EnableModelValidation="True"
                        DataKeyNames="NtwId"
                        OnRowDataBound="GridView_RowDataBound"
                        
                        AutoGenerateColumns="false" 
                        >
                        <Columns>

                            <asp:TemplateField   HeaderText="ชื่อ-ที่อยู่" >
                                <ItemTemplate>
                                    <div class="rowAddress">
                      <div  class="name">
<asp:Label runat="server" ID="lblName"></asp:Label>
<asp:Label runat="server" ID="lblService"></asp:Label> 
</div>    
                                     
                                    <div ><asp:Label runat="server" ID="lblAddress"></asp:Label></div>
                                    <div ><asp:Label runat="server" ID="lblMobile"></asp:Label></div>
                                    <div  ><asp:Label runat="server" ID="lblPhone"></asp:Label></div>
                                         <div ><asp:Label runat="server" ID="lblNotation"></asp:Label></div>
</div>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField   HeaderText="จังหวัด" >
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblProvince"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField    HeaderText="แผนที่" >
                                <ItemTemplate>
                                    <asp:HyperLink runat="server" ID="hplMap"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField> 

                        </Columns>
                    </asp:GridView> 
                </div>
                <!-- End  id="tabMenu5"-->

                <div id="tabMenu6" style="display:none" >
                    <%--<h1>Content 6</h1>--%>
                       <asp:GridView ID="GridView6" runat="server" EnableModelValidation="True"
                        DataKeyNames="NtwId"
                        OnRowDataBound="GridView_RowDataBound"
                        
                        AutoGenerateColumns="false" 
                        >
                        <Columns>

                            <asp:TemplateField   HeaderText="ชื่อ-ที่อยู่" >
                                <ItemTemplate>
                                    <div class="rowAddress">
               <div  class="name">
<asp:Label runat="server" ID="lblName"></asp:Label>
<asp:Label runat="server" ID="lblService"></asp:Label> 
</div>    
                                     
                                    <div class="address"><asp:Label runat="server" ID="lblAddress"></asp:Label></div>
                                    <div class="mobile"><asp:Label runat="server" ID="lblMobile"></asp:Label></div>
                                    <div class="phone"><asp:Label runat="server" ID="lblPhone"></asp:Label></div>
                                         <div  ><asp:Label runat="server" ID="lblNotation"></asp:Label></div>
</div>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField   HeaderText="จังหวัด" >
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblProvince"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField    HeaderText="แผนที่" >
                                <ItemTemplate>
                                    <asp:HyperLink runat="server" ID="hplMap"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField> 

                        </Columns>
                    </asp:GridView> 
                </div>
                <!-- End  id="tabMenu6"-->
                  <div id="tabMenu7" style="display:none" >
                    <%--<h1>Content 7</h1>--%>
                         <asp:GridView ID="GridView7" runat="server" EnableModelValidation="True"
                        DataKeyNames="NtwId"
                        OnRowDataBound="GridView_RowDataBound"
                        
                        AutoGenerateColumns="false" 
                        >
                        <Columns>

                            <asp:TemplateField   HeaderText="ชื่อ-ที่อยู่" >
                                <ItemTemplate>
                                    <div class="rowAddress">
                          <div  class="name">
<asp:Label runat="server" ID="lblName"></asp:Label>
<asp:Label runat="server" ID="lblService"></asp:Label> 
</div>    
                                     
                                    <div  ><asp:Label runat="server" ID="lblAddress"></asp:Label></div>
                                    <div  ><asp:Label runat="server" ID="lblMobile"></asp:Label></div>
                                    <div ><asp:Label runat="server" ID="lblPhone"></asp:Label></div>
                                    <div  ><asp:Label runat="server" ID="lblNotation"></asp:Label></div>
</div>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField   HeaderText="จังหวัด" >
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblProvince"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField    HeaderText="แผนที่" >
                                <ItemTemplate>
                                    <asp:HyperLink runat="server" ID="hplMap"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField> 

                        </Columns>
                    </asp:GridView> 
                </div>
                <!-- End  id="tabMenu7"-->
            </div>
            <!-- End  id="selectData"-->

        

<div id="dialog1" style="display:none; z-index:1111; overflow:hidden"     >
  <iframe id="areaIframe1"   frameborder="0"  scrolling="no" width="1000px"   height="550px"; ></iframe>
</div>
    </div>
    <!-- End  id="page-network"-->

</asp:Content>

