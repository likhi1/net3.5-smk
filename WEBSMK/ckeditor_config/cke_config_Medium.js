﻿// JavaScript Document
var varToolbarMedium  = {
	
	//==========================  Set Config Gernaral  
	//skin:'v2', // kama | office2003 | v2
    uiColor: '#ddedf4', // ใช้กับ  skin kama
	language:'th', // th / en and more..... 
	enterMode: 2,// กดปุ่ม Shift กับ Enter พร้อมกัน 1=แทรกแท็ก <p> 2=แทรก <br> 3=แทรก <div> 
	shiftEnterMode	:1,// กดปุ่ม Enter -- 1=แทรกแท็ก <p> 2=แทรก <br> 3=แทรก <div>	
	forcePasteAsPlainText: true,   // Paste Pain Text

	height: 300, // กำหนดความสูง
	width :730, // กำ//หนดความกว้าง * การกำหนดความกว้างต้องให้เหมาะสมกับจำนวนของ Toolbar
	removePlugins : 'elementspath' , //remove show path
	toolbarCanCollapse: false,   //not allow toolbar collapse
	resize_enabled: false, //remove resize

	//=========================== Set Plugin   
	 //extraPlugins : 'autogrow', // เรียกใช้ plugin ให้สามารถขยายขนาดความสูงตามเนื้อหาข้อมูล
     //extraPlugins : 'pastetext',
	 //autoGrow_maxHeight : 600,	 // กำหนดความสูงตามเนื้อหาสูงสุด ถ้าเนื้อหาสูงกว่า จะแสดง scrollbar
	 //forcePasteAsPlainText : true,
	
	//============================ Set Config Advance  
	//entities : false,
	// forceEnterMode : true,
	// autoParagraph :2,
	//  FillEmptyBlocks : true,
	// stylesSet : '',
	// protectedSource : ( /<\?[\s\S]*?\?>/g ),   // PHP code
	// FormatSource	 : true,
	// FormatOutput : true,
	//extraPlugins : 'bbcode',
	//fullPage : true,   // กำหนดให้สามารถแก้ไขแบบเโค้ดเต็ม คือมีแท็กตั้งแต่ <html> ถึง </html> 
	
	//============================= Set Tool   	
	toolbar: [   // Start  Set Toolbar   


['Maximize', 'Source', 'ShowBlocks', 'SelectAll', 'RemoveFormat'], ['Undo', 'Redo', 'Cut', 'Copy', 'Paste'], ['NewPage', 'Templates', 'Preview', 'Save', 'Print'],
['Find', 'Replace'], ['SpecialChar', 'Subscript', 'Superscript', 'HorizontalRule'],
'/',
['Image', 'Link', 'Unlink', 'Table', ],
['Bold', 'Italic', 'Underline', 'Strike', 'Outdent', 'Indent'], ['TextColor', 'BGColor'],
['FontSize', 'Styles', ],
['BulletedList', 'NumberedList', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],

'/',

// [ 'Font', 'Format' ,'Smiley'],
// ['PasteText', 'PasteFromWord'],
// ['SpellChecker', 'Scayt','Blockquote'],       
// ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField']	    
				],// End  Set Toolbar  
				 

//--------- Add ckfinder -------------------------------------------------------//				
filebrowserBrowseUrl : '/ckfinder/ckfinder.html',
filebrowserImageBrowseUrl : '/ckfinder/ckfinder.html?type=Images',
filebrowserFlashBrowseUrl : '/ckfinder/ckfinder.html?type=Flash',
filebrowserUploadUrl : '/ckfinder/core/connector/asp/connector.asp?command=QuickUpload&type=Files',
filebrowserImageUploadUrl : '/ckfinder/core/connector/asp/connector.asp?command=QuickUpload&type=Images',
filebrowserFlashUploadUrl : '/ckfinder/core/connector/asp/connector.asp?command=QuickUpload&type=Flash',
filebrowserWindowWidth : '1000',
filebrowserWindowHeight : '700'
				
		 

} // End  var cke_config



//===================  Set Custom Function 
//ฟังก์ฃัน InsertHTML สามารถประยุกต์ใช้งานกรณีใช้กับเว็บบอร์ด และมีการอ้างอิง ความคิดเห็น
//ฟังก์ชัน SetContents สามารถประยุกต์ใช้ Reset ค่าใน CKEditor ให้ว่างหรือล้างค่าได้
//ฟังก์ชัน GetContents สามารถประยุกต์ไว้สำหรับเช็คว่า CKEditor มีค่าว่าหรือไม่ก่อน Submit
//ฟังก์ชัน ExecuteCommand สามารถไว้ใช้งานเพิ่มเรียกใช้คำสั่ง ของ CKEditor จากภายนอก เช่น
//ExecuteCommand('link',editorObj) เป็นต้น

function InsertHTML(htmlValue,editorObj){// ฟังก์ขันสำหรับไว้แทรก HTML Code
	if(editorObj.mode=='wysiwyg'){
		editorObj.insertHtml(htmlValue);
	}else{
		alert( 'You must be on WYSIWYG mode!');
	}	
}
function SetContents(htmlValue,editorObj){ // ฟังก์ชันสำหรับแทนที่ข้อความทั้งหมด
	editorObj.setData(htmlValue);
}
function GetContents(editorObj){// ฟังก์ชันสำหรับดึงค่ามาใช้งาน
	return editorObj.getData();
}
function ExecuteCommand(commandName,editorObj){// ฟังก์ชันสำหรับเรียกใช้ คำสั่งใน CKEditor
	if(editorObj.mode=='wysiwyg'){
		editorObj.execCommand(commandName);
	}else{
		alert( 'You must be on WYSIWYG mode!');
	}
}
