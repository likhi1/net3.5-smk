﻿/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example: 
	
    //======================= Basic SetUp
     config.uiColor = '#ddedf4';    // ใช้กับ  skin kama 
    // config.language: 'th', // th / en and more..... 
     config.height = 300; // กำหนดความสูง
     config.width = 730; // กำ//หนดความกว้าง * การกำหนดความกว้างต้องให้เหมาะสมกับจำนวนของ Toolbar
     config.enterMode = 2;// กดปุ่ม Shift กับ Enter พร้อมกัน 1=แทรกแท็ก <p> 2=แทรก <br> 3=แทรก <div> 
    config.shiftEnterMode = 1 ;// กดปุ่ม Enter -- 1=แทรกแท็ก <p> 2=แทรก <br> 3=แทรก <div>	

    config.forcePasteAsPlainText = true;   // Paste Pain Text
    config.toolbarCanCollapse = false ;  //not allow toolbar collapse
    config.resize_enabled =false ; //remove resize
   
     
    //====================== Advance SetUp
    config.removePlugins = 'magicline'; 
    //config.removePlugins ='elementspath'; //remove show path
    // config.stylesSet = 'ckeditor_config.css' ;
    // config.protectedSource  = ( /<\?[\s\S]*?\?>/g ),   // PHP code
    // config.FormatSource	= true ;
    // config.FormatOutput = true  ;
	

    config.toolbar = [
    ['Maximize', 'Source', 'ShowBlocks', 'SelectAll', 'RemoveFormat'], ['Undo', 'Redo', 'Cut', 'Copy', 'Paste'], ['NewPage', 'Templates', 'Preview', 'Save', 'Print'],
    ['Find', 'Replace'], ['SpecialChar', 'Subscript', 'Superscript', 'HorizontalRule'], 
    '/',
    ['Image', 'Link', 'Unlink', 'Table', ],
    ['Bold', 'Italic', 'Underline', 'Strike', 'Outdent', 'Indent'], ['TextColor', 'BGColor'],
    ['FontSize', 'Styles', ],
    ['BulletedList', 'NumberedList', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']

    // ['About'],
    // [ 'Font', 'Format' ,'Smiley', 'ShowBlocks',],
    // ['PasteText', 'PasteFromWord'],
    // ['SpellChecker', 'Scayt','Blockquote'],       
    // ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField']  

    ]; 

	
};
