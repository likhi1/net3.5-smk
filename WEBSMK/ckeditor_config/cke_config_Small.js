﻿// JavaScript Document
var varToolbarSmall = {
	
	//=========================== Set Config Gernaral 
	//skin:'v2', // kama | office2003 | v2
    uiColor: '#ddedf4', // กำหนดสีของ ckeditor
	language:'th', // th / en and more..... 
	enterMode : 2,// กดปุ่ม Shift กับ Enter     1= แทรกแท็ก <p>       2 = แทรก <br>       3 = แทรก <div> 
	shiftEnterMode : 2, // กดปุ่ม Enter          1= แทรกแท็ก <p>       2 = แทรก <br>       3 = แทรก <div>
	forcePasteAsPlainText : true,   // Paste Pain Text
	
	height :100, // กำหนดความสูง
	width :730, // กำหนดความกว้าง * การกำหนดความกว้างต้องให้เหมาะสมกับจำนวนของ Toolbar
    removePlugins : 'elementspath' , //remove show path
	toolbarCanCollapse : false ,   //not allow toolbar collapse
	resize_enabled : false, //remove resize
	
	 
	//============================= Set Plugin   
    //  bbcode  =  อนุญาติให้ใช้ bb tag
    //  autogrow  = Text Area  ยืดตามข้อมูล และความสูงที่กำหนด
    //  pastetext  =  
    // extraPlugins : 'autogrow',  
    // autoGrow_maxHeight : 300,	 // property  of plugin  autogrow  ( กำหนดความสูงตามเนื้อหาสูงสุด ถ้าเนื้อหาสูงกว่า จะแสดง scrollbar)
	
	 
   
	//============================== Set Toolbar
	toolbar : [ // Start  Set Toolbar  
    ['Maximize', 'Source'],
    ['Bold', 'Italic', 'Underline', 'Strike', 'Outdent', 'Indent'], ['TextColor', 'BGColor'],
    ['FontSize', 'Styles', ],
					
				]// End  Set Toolbar 
	
}


 