﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//--- connect
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
//--- arrayList
using System.Collections;

/// <summary>
/// 
/// http://stackoverflow.com/questions/8049341/how-pass-session-variables-between-two-or-more-aspx-pages
/// </summary>

public partial class network : System.Web.UI.Page
{
    //public string CatId { get { string x; if (Request.QueryString["cat"] == null) { x = "4"; } else { x = Request.QueryString["cat"]; } return x; } }
    public string CatId { get { return Request.QueryString["cat"]; }   }
    public string Pro { get { Session["pro"] = ddlProvince.SelectedValue;  return Convert.ToString(Session["pro"]); } }
    public string Amp { get { Session["amp"] = ddlAmphoe.SelectedValue;  return Convert.ToString(Session["amp"]); } }
    public string Key { get { Session["key"] = tbxKeyWord.Text ;  return Convert.ToString(Session["key"]); } }

      
    protected void Page_Load(object sender, EventArgs e){ 

    //    Label1.Text = Pro + " / " + Amp + " / " + Key + " / ";    // Check Session 


        if (CatId== null) {
            Response.Redirect("network.aspx?cat=1");
        }
        ddlProvinceBindData();
        ddlAmphoeBindData();
        BindData(); 

        ////// Set PageName  //////
        Utility obj = new Utility();
        string strPageName = obj.SetPageName(CatId);
        lblPageName.Text = strPageName ; 
       
    }
     

    protected void BindData(){ 
        ///// Set Name  GridView  All Zone ///////
        GridView[] arr = new GridView[7] { GridView1, GridView2, GridView3, GridView4, GridView5, GridView6, GridView7};
        for (int i = 0; i <= (arr.Length - 1); i++) {
            DataSet ds = SelectData(CatId, (i + 1));

            arr[i].AllowPaging = true;
            arr[i].PageSize = 20;
            arr[i].PagerStyle.CssClass = "cssPager";


            arr[i].DataSource = ds;
            arr[i].DataBind();

           

        }
        
        //ArrayList arl = new ArrayList() ;
        //for (int i = 1; i <= 6; i++) {
        //    arl.Add(GridView1+i); 
        //}  
        //for (int i =  1 ; i <= arl.Count; i++) {
        //   DataSet ds = SelectData(CatId, i) ; 
        //   arl[i].DataSource = ds;
        //   arl[i].DataBind();

        //}


      
 
    }

    protected void ddlProvinceBindData() {
        string sql = "SELECT ds_major_cd , RTRIM(ds_desc_t) AS ds_desc_t  FROM setcode  WHERE (ds_major_cd = 'CH') ";

        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "tb");

        ddlProvince.DataSource = ds;
        //**** Add frist Item of dropdownlist
        ddlProvince.AppendDataBoundItems = true;
        ddlProvince.DataTextField = "ds_desc_t";
        //ddlProvince.DataValueField = "ds_desc_t";
        ddlProvince.DataBind();

    }


    protected void ddlAmphoeBindData() {
        string sql = "SELECT ds_major_cd , ds_desc , RTRIM(ds_desc_t) AS ds_desc_t  FROM setcode  WHERE (ds_major_cd = 'AM') AND (ds_desc = '0') ";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "tb");

        ddlAmphoe.DataSource = ds;
        //**** Add frist Item of dropdownlist
        ddlAmphoe.AppendDataBoundItems = true;
        ddlAmphoe.DataTextField = "ds_desc_t";
        // ddlAmphoe.DataValueField = "ds_desc_t";
        ddlAmphoe.DataBind();

    }
     
    protected   DataSet  SelectData(string _CatId  , int  _EachZoneId ){ 
      string   sql1 = "SELECT * FROM tbNtw "
                    + "WHERE NtwCatId='"+_CatId+"' AND "
                    + "NtwZoneId='" + _EachZoneId + "'  AND "
                    + "NtwStatus= '1' " 
                    + "ORDER BY NtwSort ASC , NtwId DESC   "; 
      DBClass obj = new DBClass();
      DataSet ds = obj.SqlGet(sql1, "tbNtw");
      return   ds;
    }
      

    protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e) {
        if (e.Row.RowType == DataControlRowType.DataRow) { 
         
             Label lblName = (Label)(e.Row.FindControl("lblName")) ;
             if(lblName  != null){
                 lblName.Text =  DataBinder.Eval(e.Row.DataItem, "NtwName").ToString()   ;
                 lblName.Style.Add("font-weight", "bold");
             }

            
             

             Label lblAddress = (Label)(e.Row.FindControl("lblAddress")) ;
             if(lblAddress  != null){
                 lblAddress.Text = DataBinder.Eval(e.Row.DataItem, "NtwAddress" ).ToString();
             } 

             Label lblMobile = (Label)(e.Row.FindControl("lblMobile")) ;
             if(lblMobile != null){
                string  strMobile = DataBinder.Eval(e.Row.DataItem, "NtwTelMobile").ToString();
                 lblMobile.Text = ( strMobile!=""?"มือถือ : ": "" ) + strMobile ;
             }

             Label lblPhone = (Label)(e.Row.FindControl("lblPhone")) ;
             if(lblPhone != null){
                 string  strPhone  =  DataBinder.Eval(e.Row.DataItem, "NtwTelPhone" ).ToString();
                lblPhone.Text = ( strPhone !=""?"โทรศัพท์ : ": "" ) + strPhone  ; 
             }

           
               Label lblProvince= (Label)(e.Row.FindControl("lblProvince")) ;
             if( lblProvince  != null){
                 lblProvince.Text = DataBinder.Eval(e.Row.DataItem, "NtwProvince" ).ToString();
                 lblProvince.Attributes.Add("style", "text-align:center"); 
             }

            

             Label lblService = (Label)(e.Row.FindControl("lblService"));
             string NtwService = DataBinder.Eval(e.Row.DataItem, "NtwService").ToString();
             if (!String.IsNullOrEmpty(NtwService)) {
                 lblService.Text = "(" + NtwService + ")";
                 lblService.Style.Add("color", "#F17503");
             }


             Label lblNotation = (Label)(e.Row.FindControl("lblNotation"));
             if (lblNotation != null) {
                 string strNotation = DataBinder.Eval(e.Row.DataItem, "NtwNotation").ToString();
                 lblNotation.Text = (strNotation != "" ? "หมายเหตุ : " : "") + strNotation;
             }

             Label lblAmphoe = (Label)(e.Row.FindControl("lblAmphoe"));
             // if( lblAmphoe  != null){
             string strAmphoe = DataBinder.Eval(e.Row.DataItem, "NtwAmphoe").ToString();
             if (!string.IsNullOrEmpty(strAmphoe)) {
                 lblAmphoe.Text = "(" + DataBinder.Eval(e.Row.DataItem, "NtwAmphoe").ToString() + ")";
                 lblAmphoe.Attributes.Add("style", "text-align:center");
               //  lblAmphoe.Style.Add("color", "#038DFA");

             }

             HyperLink hplMap = (HyperLink)(e.Row.FindControl("hplMap")) ;
             if( hplMap  != null){
                 hplMap.Text = "ดูแผนที่";
                 string  strNtwId = DataBinder.Eval(e.Row.DataItem, "NtwId").ToString();
                 hplMap.NavigateUrl = "networkMap.aspx?id=" + strNtwId;
                 hplMap.CssClass = "openerCover";

             }
             
            
        }
    }


    protected void BtnSearch_Click(object sender, EventArgs e) {
        
         
        Response.Redirect("networkSearch.aspx?cat=" + CatId  );

    }


    //======================  Pager
    protected void  GridView1_IndexChanging(object sender, GridViewPageEventArgs e) {
        GridView1.PageIndex = e.NewPageIndex;
        BindData();
    }


}