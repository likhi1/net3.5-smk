﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="buyInsureAccident.aspx.cs" Inherits="onlineInsAccident" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">


    <div id="page-buy-insure">
        <h1 class="subject1">ซื้อประกัน-อุบัติเหตุส่วนบุคคล </h1>
   <div class="stepOnline"> <img src="images/step-buy-insure1.jpg" /></div>
 
 
        <h2 class="subject-detail">รายละเอียดผู้เอาประกัน  </h2>

        <div>กรณีมีผู้เอาประกันมากกว่า 1 คน กรุณาแฟกซ์ชื่อผู้เอาประกันไปที่ 02-3772097 หรือ Email ไปที่ reg_misc@smk.co.th</div>


        <table class="tb-buy-insure ">
            <tr>
                <td width="90" class="label">ชื่อ :</td>
                <td width="100">
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox><span class="star">* </span>

                </td>

                <td class="label" width="130">จังหวัด:</td>
                <td width="180">
                    <asp:TextBox ID="TextBox10" runat="server"></asp:TextBox><span class="star">* </span>&nbsp;</td>


            </tr>
            <tr>
                <td class="label">นามสกุล :</td>
                <td>
                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox><span class="star">*</span></td>
                <td class="label">อำเภอ/เขต :</td>
                <td>
                    <asp:TextBox ID="TextBox11" runat="server"></asp:TextBox></td>

            </tr>
            <tr>
                <td width="114" class="label">วันที่เกิด :</td>
                <td width="252">
                    <asp:DropDownList ID="DropDownList1" runat="server">
                    </asp:DropDownList>
                </td>
                <td class="label">รหัสไปรษณีย์ :</td>
                <td>
                    <asp:TextBox ID="TextBox9" runat="server"></asp:TextBox><span class="star">*</span>
                </td>

            </tr>
            <tr>
                <td class="label">เลขที่บัตร :</td>
                <td>
                    <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox><span class="star">*</span>
                </td>

                <td class="label" width="130">อีเมล์ :</td>
                <td>
                    <asp:TextBox ID="TextBox12" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label">เบอร์โทรศัพท์ :</td>
                <td>
                    <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                </td>

                <td>&nbsp;</td>
            </tr>
            <tr>
                กรมธรรม์คุ้มครอง 1 ปี ตั้งแต่ 12.00น.
    <td class="label">วันที่คุ้มครอง :</td>
                <td colspan="3">
                    <asp:TextBox ID="TextBox14" runat="server"></asp:TextBox>
                    กรมธรรม์คุ้มครอง 1 ปี (เวลา 12.00น.)</td>

            </tr>
            <tr>
                <td class="label">ที่อยู่ :</td>
                <td colspan="3">
                    <asp:TextBox ID="TextBox7" runat="server" TextMode="MultiLine"></asp:TextBox><span class="star">* </span>ใส่ บ้านเลขที่, ซอย, หมู่, หมู่บ้าน, ถนน
                </td>


            </tr>


            <tr>
                <td class="label">&nbsp;</td>
                <td colspan="3">
                    <asp:TextBox ID="TextBox4" runat="server" TextMode="MultiLine"></asp:TextBox>
                    ใส่ ตำบล, แขวง

        
                </td>

            </tr>
        </table>


        <h2 class="subject-detail">รายละเอียดผู้รับประโยชน์</h2>
        <div>กรณีมีผู้เอาประกันมากกว่า 1 คน กรุณาแฟกซ์ชื่อผู้เอาประกันไปที่ 02-3772097 หรือ Email ไปที่ reg_misc@smk.co.th</div>


        <table class="tb-buy-insure">
            <tr>
                <td class="label" width="90">ชื่อ :</td>
                <td width="100">
                    <asp:TextBox ID="TextBox13" runat="server"></asp:TextBox><span class="star">*</span>
                </td>
                <td class="label" width="130">จังหวัด :</td>
                <td width="180">
                    <asp:DropDownList ID="DropDownList11" runat="server">
                    </asp:DropDownList> 

                </td> 
            </tr>

            <tr>
                <td class="label">นามสกุล :</td>
                <td>
                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox><span class="star">*</span>
                </td>
                <td class="label">อำเภอ/เขต :</td>
                <td>
                    <asp:TextBox ID="TextBox24" runat="server"></asp:TextBox>
                </td> 
            </tr>

            <tr>
                <td width="114" class="label">ความสัมพันธ์	 :</td>
                <td width="252">
                    <asp:DropDownList ID="DropDownList9" runat="server">
                    </asp:DropDownList><span class="star">*</span>
                </td>
                <td class="label">รหัสไปรษณีย์ :</td>
                <td>
                    <asp:TextBox ID="TextBox25" runat="server"></asp:TextBox>
                </td>

            </tr>
            <tr>
                <td width="114" class="label">เบอร์โทรศัพท์ :</td>
                <td width="252">
                    <asp:DropDownList ID="DropDownList6" runat="server">
                    </asp:DropDownList>
                </td>
                <td class="label">อีเมล์&nbsp; :</td>
                <td width="252">
                    <asp:TextBox ID="TextBox26" runat="server"></asp:TextBox>
                </td>

            </tr>
            <tr>
                <td class="label">ที่อยู่ :</td>
                <td colspan="3">
                    <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                        <asp:ListItem>ที่อยู่เดียวกับผู้เอาประกัน</asp:ListItem>
                        <asp:ListItem>ที่อยู่อื่นๆ</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td class="label">&nbsp;</td>
                <td colspan="3">
                    <asp:TextBox ID="TextBox19" runat="server" TextMode="MultiLine"></asp:TextBox>
                    ใส่ บ้านเลขที่, ซอย, หมู่, หมู่บ้าน, ถนน
                </td>


            </tr>


            <tr>
                <td class="label">&nbsp;</td>
                <td colspan="3">
                    <asp:TextBox ID="TextBox20" runat="server" TextMode="MultiLine"></asp:TextBox>
                    ใส่ ตำบล, แขวง

        
                </td>

            </tr>
        </table>


        <h2 class="subject-detail">รายละเอียดความคุ้มครองหลัก</h2>
        

        <table  width="100%" class="tb-buy-Info" >
 
    <tr>
      <th width="409" > ความคุ้มครอง </th>
      <th width="123"> จำนวนเงินเอาประกัน </th>
    </tr>
    <tr >
      <td class="align-left" > เสียชีวิต สูญเสียอวัยวะ สายตา หรือทุพลภาพถาวรสิ้นเชิง </td>
      <td  class="align-center" > <input   type="text"  style="width:90px;"></td>
    </tr>
    <tr >
      <td class="align-left"> การสูญเสียชีวิต สูญเสียอวัยวะ สายตา การรับฟัง
        
        การพูดออกเสียงหรือทุพพลภาพถาวร </td>
      <td  class="align-center" > ไม่คุ้มครอง </td>
    </tr>
    <tr >
      <td class="align-left"> ทุพพลภาพชั่วคราวสิ้นเชิง และไม่เกิน
          <input   type="text"  style="width:25px;">
          สัปดาห์  </td>
      <td  class="align-center" > ไม่คุ้มครอง </td>
    </tr>
    <tr >
      <td class="align-left">  ทุพพลภาพชั่วคราวบางส่วน และไม่เกิน
          <input  type="text"  style="width:25px;">
          สัปดาห์  </td>
      <td  class="align-center" > ไม่คุ้มครอง </td>
    </tr>
         
    
 
</table>

<br />
        <h2 class="subject-detail">รายละเอียดความคุ้มครองเพิ่ม</h2>

        <div>การขับขี่หรือโดยสารรถจักรยานยนต์ (กรณีที่ท่านมีการขับขี่หรือโดยสารรถจักรยานยนต์)</div>

        <br />
        <div class="notation" style="text-align: center;">
            บริษัทสินมั่นคงประกันภัย จำกัด (มหาชน)
            <br />
            ขอสงวนสิทธิในการให้ความคุ้มครองและเปลี่ยนแปลงเบี้ยประกันภัย ในกรณีข้อมูลของท่านไม่ตรงกับความเป็นจริง
        </div>

        </div>
        <!-- End id="page-buy-insure" --->
</asp:Content>

