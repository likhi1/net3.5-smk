﻿/// <reference name="MicrosoftAjax.js"/>

Type.registerNamespace("SMK");

SMK.ClientBehavior = function(element) {
    SMK.ClientBehavior.initializeBase(this, [element]);
}

SMK.ClientBehavior.prototype = {
    initialize: function() {
        SMK.ClientBehavior.callBaseMethod(this, 'initialize');
        
        // Add custom initialization here
    },
    dispose: function() {        
        //Add custom dispose actions here
        SMK.ClientBehavior.callBaseMethod(this, 'dispose');
    }
}
SMK.ClientBehavior.registerClass('SMK.ClientBehavior', Sys.UI.Behavior);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
