﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="buyInsurePA_step2.aspx.cs" Inherits="buyInsurePA_step2" EnableEventValidation="false"%>
<%@ Register src="~/uc/ucTxtDate.ascx" tagname="ucTxtDate" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script>
        function rdoPA_Click(paType) {
            if (paType == "PA1") {
                document.getElementById("<%=ddlPa1.ClientID %>").disabled = false;
                document.getElementById("<%=ddlPa2.ClientID %>").disabled = true;
            }
            else if (paType == "PA2") {
                document.getElementById("<%=ddlPa1.ClientID %>").disabled = true;
                document.getElementById("<%=ddlPa2.ClientID %>").disabled = false;
            }
        }
        function PopulateMedical() {
            var pageUrl = '<%=ResolveUrl("~/buyInsurePA_step2.aspx")%>';
            var strSumIns;
            var ddlChild;            

            //$(ddlChild).attr("disabled", "disabled");
            if (document.getElementById("<%=ddlPa1.ClientID %>").length > 0 && 
                document.getElementById("<%=ddlPa1.ClientID %>").disabled == false) {
                strSumIns = document.getElementById("<%=ddlPa1.ClientID %>").value;
            }
            if (document.getElementById("<%=ddlPa2.ClientID %>").length > 0 &&
                document.getElementById("<%=ddlPa2.ClientID %>").disabled == false) {
                strSumIns = document.getElementById("<%=ddlPa2.ClientID %>").value;
            }
            ddlChild = document.getElementById("<%=ddlMedicine.ClientID %>");
            $.ajax({
                async: false,
                type: "POST",
                url: pageUrl + '/GetCoverMedicine',
                data: '{strSumIns:"' + strSumIns + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(response) {
                    PopulateDropdownControl(response.d, $(ddlChild))
                },
                error: function(response) {
                    alert("error " + response.d);
                }
            });
        }
        function PopulateDropdownControl(list, control) {
            //control.removeAttr("disabled");
            if (list.length > 0) {
                //control.empty().append('<option selected="selected" value="0">Please select</option>');
                control.empty();
                $.each(list, function() {
                    control.append($("<option></option>").val(this['Value']).html(this['Text']));
                });
                control.removeAttr("disabled");
            }
            else {
                control.empty().append('<option selected="selected" value=""> - เลือก - <option>');
                control.attr("disabled", "disabled");
            }
        }     
        function validateForm() {
            var isRet = true;
            var isRequire = true;
            var isLength = true;
            var isDate = true;
            var strMsg = "";
            var strMsgRequire = "";
            var strMsgLength = "";
            var strMsgDate = "";
            if (document.getElementById("<%=txtFName.ClientID %>").value == "") {
                strMsg += "    - ชื่อ\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtLName.ClientID %>").value == "") {
                strMsg += "    - สกุล\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtAddress.ClientID %>").value == "") {
                strMsg += "    - ที่อยู่\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtSubDistrict.ClientID %>").value == "") {
                strMsg += "    - ตำบล / แขวง\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtDistrict.ClientID %>").value == "") {
                strMsg += "    - อำเภอ / เขต\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=ddlProvince.ClientID %>").value == "") {
                strMsg += "    - จังหวัด\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtPostCode.ClientID %>").value == "") {
                strMsg += "    - รหัสไปรษณีย์\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtMobile.ClientID %>").value == "") {
                strMsg += "    - เบอร์โทรศัพท์มือถือ\n";
                isRet = false;
                isRequire = false;
            }
            // ผู้รับผลประโยชน์
            if (document.getElementById("<%=txtBefFName.ClientID %>").value == "") {
                strMsg += "    - ชื่อ\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtBefLName.ClientID %>").value == "") {
                strMsg += "    - นามสกุล\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=ddlBefRelationship.ClientID %>").value == "") {
                strMsg += "    - ความสัมพันธ์\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=rdoInsAddress.ClientID %>").checked == false &&
                document.getElementById("<%=rdoOtherAddress.ClientID %>").checked == false) {
                if (document.getElementById("<%=txtBefAddress.ClientID %>").value == "") {
                    strMsg += "    - ที่อยู่\n";
                    isRet = false;
                    isRequire = false;
                }
                if (document.getElementById("<%=txtBefSubDistrict.ClientID %>").value == "") {
                    strMsg += "    - ตำบล / แขวง\n";
                    isRet = false;
                    isRequire = false;
                }
                if (document.getElementById("<%=txtBefDistrict.ClientID %>").value == "") {
                    strMsg += "    - อำเภอ / เขต\n";
                    isRet = false;
                    isRequire = false;
                }
                if (document.getElementById("<%=ddlBefProvince.ClientID %>").value == "") {
                    strMsg += "    - จังหวัด\n";
                    isRet = false;
                    isRequire = false;
                }
                if (document.getElementById("<%=txtBefPostcode.ClientID %>").value == "") {
                    strMsg += "    - รหัสไปรษณีย์\n";
                    isRet = false;
                    isRequire = false;
                }
                if (document.getElementById("<%=txtBefTelephone.ClientID %>").value == "") {
                    strMsg += "    - เบอร์โทรศัพท์\n";
                    isRet = false;
                    isRequire = false;
                }
            }
            
            // ความคุ้มครอง
            

            if (isRequire == false) {
                strMsg = "!กรุณาระบุ\n" + strMsg;
            }

            if (isRet == false) {
                //alert(strMsg + strMsgLength + strMsgDate);
                showMessagePage(strMsg + strMsgLength + strMsgDate);
            }
            else {
                document.getElementById("divLoading").style.display = "block";
                document.getElementById('divPremium').style.display = 'none';
            }
            return isRet;
        }
        function showMessagePage(strMsg) {
            document.getElementById("MsgIframe").setAttribute("src", "ShowMessageBox.aspx?msg=" + strMsg);
            //alert("url : " + link);
            $("[id*=dialog]").dialog({
                autoOpen: false,
                show: "fade",
                hide: "fade",
                modal: true,
                //open: function (ev, ui) {
                //    $('#myIframe').src = 'http://www.google.com';
                //},
                maxHeight: '400',
                width: '400',
                resizable: true
                //title: 'Title Topic'
            });
            //            $("#dialog1").dialog();

            $("#dialogMsg").dialog("open");
        }
        function closeMessagePage() {
            $('#dialogMsg').dialog('close');
            return false;

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div id="page-online-insCar"> 
        <h1 class="subject1">ซื้อประกัน-อุบัติเหตุส่วนบุคคล</h1>
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="True">
        </asp:ScriptManager>
        <div class="stepOnline"> <img src="images/navi3step_s-2.jpg" /></div>
        <div class="prc-row">    
            <h2 class="subject-detail">
                รายละเอียดผู้เอาประกัน  
                <div class="label">กรณีมีผู้เอาประกันมากกว่า 1 คน กรุณาแฟกซ์ชื่อผู้เอาประกันไปที่ 02-3772097 หรือ Email ไปที่ reg_misc@smk.co.th</div>
            </h2>
            <table width="630px">
                <tr>
                    <td valign="top">
                        <table class="tb-online-insCar">             
                            <tr>
                                <td class="label" >
                                    ชื่อ :
                                </td>
                                <td >
                                    <asp:TextBox ID="txtFName" runat="server"></asp:TextBox>
                                    <span class="star">*</span>
                                </td>    
                                <td class="label">
                                    นามสกุล :
                                </td>
                                <td>
                                    <asp:TextBox ID="txtLName" runat="server"></asp:TextBox>
                                    <span class="star">*</span>
                                </td>                            
                            </tr>
                            <tr>
                                <td class="label">
                                    ที่อยู่ :
                                </td>
                                <td colspan="3" nowrap>
                                    <asp:TextBox ID="txtAddress" runat="server" Width="250" MaxLength="50"></asp:TextBox>
                                    <span class="star">*</span>(ใส่ บ้านเลขที่, หมู่, ถนน)  
                                </td>
                            </tr>
                            <tr>                    
                                <td class="label"  >
                                    
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtSubDistrict" runat="server" Width="250" MaxLength="50"></asp:TextBox>
                                    <span class="star">*</span>(ใส่ ตำบล, แขวง)
                                </td>
                            </tr>
                            <tr>
                                <td class="label" >
                                    อำเภอ / เขต :
                                </td>
                                <td width="150px">
                                    <asp:TextBox ID="txtDistrict" runat="server"></asp:TextBox>
                                    <span class="star">*</span>
                                </td>      
                                <td class="label">
                                    จังหวัด :
                                </td>
                                <td  >
                                    <asp:DropDownList ID="ddlProvince" runat="server">
                                    </asp:DropDownList>
                                    <span class="star">*</span>
                                </td>                          
                            </tr>
                            <tr>
                                <td class="label">
                                    รหัสไปรษณีย์ :
                                </td>
                                <td width="130px">
                                    <asp:TextBox ID="txtPostCode" runat="server" MaxLength="5"></asp:TextBox>
                                    <span class="star">*</span>
                                </td>                            
                            </tr>
                            <tr>
                                <td class="label">
                                    เบอร์โทรศัพท์มือถือ :
                                </td>
                                <td>
                                    <asp:TextBox ID="txtMobile" runat="server"></asp:TextBox>
                                    <span class="star">*</span>
                                </td>  
                                <td class="label">
                                    เบอร์โทรศัพท์บ้าน :
                                </td>
                                <td>
                                    <asp:TextBox ID="txtTelephone" runat="server"></asp:TextBox>                        
                                </td>                                                  
                            </tr>
                            <tr>
                                <td class="label">
                                    อีเมล์ :
                                </td>
                                <td>
                                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>                                    
                                </td>                    
                            </tr>
                            <tr>
                                <td class="label">
                                    ประเภทบัตร :
                                </td>
                                <td  >
                                    <asp:DropDownList ID="ddlCardType" runat="server">
                                    </asp:DropDownList>
                                </td>     
                                <td class="label" >
                                    เลขที่บัตร :
                                </td>
                                <td width="150px">
                                    <asp:TextBox ID="txtCardNo" runat="server"></asp:TextBox>
                                    <span class="star">*</span>
                                </td>                                                           
                            </tr>
                            <tr>
                                <td class="label">
                                    อาชีพ :
                                </td>
                                <td  >
                                    <asp:DropDownList ID="ddlOccupy" runat="server">
                                    </asp:DropDownList>
                                </td>  
                                <td class="label">
                                    วันเกิด :
                                </td>
                                <td>
                                    <uc1:ucTxtDate ID="txtBirthDate" runat="server" />                                    
                                </td> 
                            </tr>
                            <tr>
                                <td class="label">
                                    วันที่เริ่มคุ้มครอง :
                                </td>
                                <td colspan="3">
                                    <uc1:ucTxtDate ID="txtStartDate" runat="server" />
                                    <span class="star">*</span>
                                    เวลา(12.00น.)(กรมธรรม์คุ้มครอง1ปี)
                                </td>                                                    
                            </tr>                            
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>               
            <h2 class="subject-detail">
                รายละเอียดผู้รับประโยชน์  
                <div class="label">
                    ไม่ต้องระบุ ในกรณีมีผู้รับประโยชน์มากกว่า 1 คน กรุณา FAX ชื่อผู้รับประโยชน์ไปที่ 02-3772097 หรือ Email ไปที่ reg_misc@smk.co.th
                </div>
            </h2>            
            <table class="tb-online-insCar">              
                <tr>
                    <td class="label" >
                        ชื่อ :
                    </td>
                    <td >
                        <asp:TextBox ID="txtBefFName" runat="server"></asp:TextBox>
                        <span class="star">*</span>
                    </td>    
                    <td class="label">
                        นามสกุล :
                    </td>
                    <td>
                        <asp:TextBox ID="txtBefLName" runat="server"></asp:TextBox>
                        <span class="star">*</span>
                    </td>                            
                </tr>
                <tr>
                    <td class="label">ความสัมพันธ์	 :</td>
                    <td>
                        <asp:DropDownList ID="ddlBefRelationship" runat="server">
                        </asp:DropDownList><span class="star">*</span>
                    </td>
                </tr>
                <tr>
                    <td class="label" valign="top">ที่อยู่ :</td>
                    <td colspan="3">
                        <asp:RadioButton ID="rdoInsAddress" runat="server" text="ที่อยู่เดียวกับผู้เอาประกัน" GroupName="BefAddress" Checked="true"/>
                        <asp:RadioButton ID="rdoOtherAddress" runat="server" text="ที่อยู่อื่นๆ" GroupName="BefAddress"/>                    
                    </td>
                </tr>
                <tr>
                    <td class="label">                    
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="txtBefAddress" runat="server" Width="365"></asp:TextBox>                    
                    </td>
                </tr>
                <tr>                    
                    <td class="label"  >
                        ตำบล / แขวง :
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="txtBefSubDistrict" runat="server" Width="365"></asp:TextBox>                    
                    </td>
                </tr>
                <tr>
                    <td class="label" >
                        อำเภอ / เขต :
                    </td>
                    <td width="150px">
                        <asp:TextBox ID="txtBefDistrict" runat="server"></asp:TextBox>                    
                    </td>      
                    <td class="label">
                        จังหวัด :
                    </td>
                    <td  >
                        <asp:DropDownList ID="ddlBefProvince" runat="server">
                        </asp:DropDownList>                    
                    </td>                          
                </tr>
                <tr>
                    <td class="label">
                        รหัสไปรษณีย์ :
                    </td>
                    <td width="130px">
                        <asp:TextBox ID="txtBefPostcode" runat="server" MaxLength="5"></asp:TextBox>                    
                    </td>  
                    <td class="label">
                        เบอร์โทรศัพท์ :
                    </td>
                    <td>
                        <asp:TextBox ID="txtBefTelephone" runat="server"></asp:TextBox>                    
                    </td>                             
                </tr>
                <tr>
                    <td class="label">
                        อีเมล์ :
                    </td>
                    <td>
                        <asp:TextBox ID="txtBefEmail" runat="server"></asp:TextBox>                                    
                    </td>                    
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>
            <h2 class="subject-detail">รายละเอียดความคุ้มครองหลัก</h2>
            <div id="divFixCover" style="display:none">
            <table class="tb-buy-Info">
                <tr>
                    <th>
                        ความคุ้มครอง
                    </th>
                    <th>
                        จำนวนเงินเอาประกัน
                    </th>
                </tr>
                <tr>
                    <td class="align-left">
                        เสียชีวิต สูญเสียอวัยวะ สายตา หรือทุพลภาพถาวรสิ้นเชิง
                    </td>
                    <td>
                        <asp:TextBox ID="txtPa1" runat="server" CssClass="TEXTBOXMONEYDIS"
                            MaxLength="10" ReadOnly="True" Width="80px"></asp:TextBox>
                        <asp:Label ID="lblPa1" runat="server" Text="ไม่คุ้มครอง"></asp:Label>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="align-left">
                        การสูญเสียชีวิต สูญเสียอวัยวะ สายตา การรับฟัง การพูดออกเสียงหรือทุพพลภาพถาวร
                    </td>
                    <td>
                        <asp:TextBox ID="txtPa2" runat="server" CssClass="TEXTBOXMONEYDIS" MaxLength="10"
                            ReadOnly="True" Width="80px"></asp:TextBox>
                        <asp:Label ID="lblPa2" runat="server" Text="ไม่คุ้มครอง"></asp:Label>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="align-left">            
                        <div id="divTtd" runat="server">
                            ทุพพลภาพชั่วคราวสิ้นเชิง และไม่เกิน
                            <asp:TextBox ID="txtTtdWeek" runat="server" CssClass="TEXTBOXMONEYDIS" MaxLength="3"
                                ReadOnly="True" Width="25px"></asp:TextBox>
                            <asp:Label ID="Label7" runat="server" Text="สัปดาห์"></asp:Label>
                        </div>&nbsp;
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlTtd" runat="server" CssClass="TEXTBOX" Visible="False">
                        </asp:DropDownList>
                        <asp:TextBox ID="txtTtd" runat="server" CssClass="TEXTBOXMONEYDIS" MaxLength="10"
                            ReadOnly="True" Width="80px"></asp:TextBox>
                        <asp:Label ID="lblTtd" runat="server" Text="ไม่คุ้มครอง"></asp:Label>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="divPtd" runat="server" class="align-left">
                            ทุพพลภาพชั่วคราวบางส่วน และไม่เกิน
                            <asp:TextBox ID="txtPtdWeek" runat="server" CssClass="TEXTBOXMONEYDIS" MaxLength="3"
                                ReadOnly="True" Width="25px"></asp:TextBox>
                            <asp:Label ID="Label9" runat="server" Text="สัปดาห์"></asp:Label>
                        </div>
                        </td>
                    <td>
                        <asp:TextBox ID="txtPtd" runat="server" CssClass="TEXTBOXMONEYDIS" MaxLength="10"
                            ReadOnly="True" Width="80px"></asp:TextBox>
                        <asp:Label ID="lblPtd" runat="server" Text="ไม่คุ้มครอง"></asp:Label>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="align-left">
                        ค่ารักษาพยาบาล
                    </td>
                    <td>          
                        <asp:TextBox ID="txtMedicine" runat="server" CssClass="TEXTBOXMONEYDIS" MaxLength="10"
                            ReadOnly="True" Width="80px"></asp:TextBox>
                        <asp:Label ID="lblMedicine" runat="server" Text="ไม่คุ้มครอง"></asp:Label>&nbsp;                       
                    </td>
                </tr>
            </table>
            <br />
            <h2 class="subject-detail">รายละเอียดความคุ้มครองเพิ่ม</h2>
            <table>                
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Label ID="lblWar" runat="server" Text="การสงคราม"></asp:Label></td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Label ID="lblStr" runat="server" Text="การนัดหยุดงาน การจลาจล และการที่ประชนก่อความวุนวายถึงขนาดลุกฮือต่อต้านรัฐบาล"></asp:Label></td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Label ID="lblSpt" runat="server" Text="การเล่นหรือการแข่งขันกีฬาอันตราย"></asp:Label></td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Label ID="lblMtr" runat="server" Text="การขับขี่หรือโดยสารรถจักรยานยนต์(กรณีที่ท่านมีการขับขี่หรือโดยสารรถจักรยานยนต์)"></asp:Label></td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Label ID="lblAir" runat="server" Text="การโดยสารในฐานะผู้โดยสารอากาศยานที่มิได้ประกอบการโดยสารการบินพาณิชย์"></asp:Label></td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Label ID="lblMurder" runat="server" Text="คุ้มครองการถูกฆ่าหรือถูกทำร้ายร่างกาย"></asp:Label></td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
            </div>
            <div id="divChooseCover" style="display:none">
                <table class="tb-buy-Info">
                    <tr>
                        <th>
                            ความคุ้มครอง
                        </th>
                        <th>
                            จำนวนเงินเอาประกัน
                        </th>
                    </tr>
                    <tr>
                        <td class="align-left">
                            <asp:RadioButton ID="rdoPa1" runat="server" GroupName="PA" Text="เสียชีวิต สูญเสียอวัยวะ สายตา หรือทุพลภาพถาวรสิ้นเชิง" onclick="rdoPA_Click('PA1');" /></td>
                        <td>
                            <asp:DropDownList ID="ddlPa1" runat="server" CssClass="inputText" AutoPostBack="false">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="align-left">
                            <asp:RadioButton ID="rdoPa2" runat="server" GroupName="PA" Text="การสูญเสียชีวิต สูญเสียอวัยวะ สายตา การรับฟัง การพูดออกเสียงหรือทุพพลภาพถาวร" onclick="rdoPA_Click('PA2');"/></td>
                        <td>
                            <asp:DropDownList ID="ddlPa2" runat="server" CssClass="inputText" AutoPostBack="false">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="align-left">            
                            <div id="div1" runat="server">
                                <asp:Label ID="Label4" runat="server" Text="ทุพพลภาพชั่วคราวสิ้นเชิง และไม่เกิน"></asp:Label>
                                <asp:DropDownList ID="ddlTtdWeek" runat="server" CssClass="inputText">
                                </asp:DropDownList>
                                <asp:Label ID="Label5" runat="server" Text="สัปดาห์"></asp:Label>
                            </div>
                            </td>
                        <td><asp:DropDownList ID="ddlTtdSI" runat="server" CssClass="inputText">
                        </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td class="align-left">
                            <div id="div2" runat="server">
                                <asp:Label ID="Label6" runat="server" Text="ทุพพลภาพชั่วคราวบางส่วน และไม่เกิน"></asp:Label>
                                <asp:DropDownList ID="ddlPtdWeek" runat="server" CssClass="inputText">
                                </asp:DropDownList>
                                <asp:Label ID="Label1" runat="server" Text="สัปดาห์"></asp:Label>
                            </div>
                            </td>
                        <td>
                            <asp:DropDownList ID="ddlPtdSI" runat="server" CssClass="inputText">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td class="align-left">
                            <asp:Label ID="Label8" runat="server" Text="ค่ารักษาพยาบาล"></asp:Label>
                        </td>
                        <td><asp:DropDownList ID="ddlMedicine" runat="server" CssClass="inputText">
                        </asp:DropDownList></td>
                    </tr>
                </table>
                <br />
                <h2 class="subject-detail">รายละเอียดความคุ้มครองเพิ่ม</h2>
                <table class="tb-online-insCar">
                    <tr>
                        <td>
                            <asp:CheckBox ID="chkWar" runat="server" Text="การสงคราม" /></td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="chkStr" runat="server" Text="การนัดหยุดงาน การจลาจล และการที่ประชนก่อความวุนวายถึงขนาดลุกฮือต่อต้านรัฐบาล" /></td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="chkSpt" runat="server" Text="การเล่นหรือการแข่งขันกีฬาอันตราย" /></td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="chkMtr" runat="server" Text="การขับขี่หรือโดยสารรถจักรยานยนต์(กรณีที่ท่านมีการขับขี่หรือโดยสารรถจักรยานยนต์)" /></td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="chkAir" runat="server" Text="การโดยสารในฐานะผู้โดยสารอากาศยานที่มิได้ประกอบการโดยสารการบินพาณิชย์" /></td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="chkMurder" runat="server" Text="คุ้มครองการถูกฆ่าหรือถูกทำร้ายร่างกาย" /></td>
                        <td>
                        </td>
                    </tr>
                </table>
            </div>
            <table class="tb-buy-insure">
                <tr>
                    <td colspan="4">
                        <div style="margin: 0px auto; width: 250px; margin-top: 15px;">                        
                            <asp:Button ID="btnCalculate" runat="server" Text="คำนวณเบี้ย" class="btn-default" Style="margin-right: 10px;" OnClick="btnCalculate_Click" OnClientClick="return validateForm();"/>
                            <input id="btnClear" type="reset" class="btn-default " value="เคียร์ข้อมูล" />
                        </div>
                    </td>
                    <a name="grd"></a>
                </tr>
            </table>    
            <div id="divLoading" style="text-align:center;display:none;">
                <img src="./images/loading.gif" />
            </div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divPremium" style="display:none">
                        <h2 class="subject-detail">ยอดเบี้ยประกันที่ต้องชำระ</h2>
                        <table   class="tb-online-insCar">           
                            <tr>
                                <td class="label">เบี้ยประกันภัยปกติรวม :</td>
                                <td>
                                    <asp:Label ID="lblPremium" runat="server" Text=""></asp:Label>
                                    บาท
                                </td>
                            </tr>
                            <tr>
                                <td class="label">เบี้ยประกันภัยพิเศษผ่านอินเทอร์เน็ต :</td>
                                <td>
                                    <asp:Label ID="lblInternetPremium" runat="server" Text=""></asp:Label>
                                    บาท
                                </td>
                            </tr>                
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div style="margin: 0px auto; width: 250px; margin-top: 15px;">                        
                                        <asp:Button ID="btnSave" runat="server" Text="ถัดไป >>" class="btn-default" Style="margin-right: 10px;" OnClick="btnSave_Click"/>
                                        <input id="Reset1" type="reset" class="btn-default " value="ยกเลิก" />
                                    </div>
                                </td>
                                <a name="grd"></a>
                            </tr>
                        </table>   
                    </div>
                    <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnCalculate" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel> 
        </div><!-- End class="prc-row"-->
        <div id="dialogMsg" style="display:none" title="การตรวจสอบข้อมูล"   >
            <iframe id="MsgIframe"   frameborder="0" style="max-height:360" width="370" scrolling="auto"></iframe>
        </div>
    </div><!-- End  class="page-online-insCar" -->
    <div style="display:none">
        <asp:TextBox ID="txtTmpCD" runat="server"></asp:TextBox>        
        <asp:TextBox ID="txtPromotionMajor" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtPromotionMinor" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtPAType" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtPremium" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtStamp" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtTax" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtDiscountRate" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtDiscountAmt" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtPremiumDiscount" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtGrossPremiumDiscount" runat="server"></asp:TextBox>        
        <asp:TextBox ID="txtDeathPrmm" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtTtdPrmm" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtPtdPrmm" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtMedPrmm" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtAddPrmm" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtDiscPrmm" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtNetPrmm" runat="server"></asp:TextBox>
    </div>
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

