﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="investInfo_15.aspx.cs" Inherits="investInfo_15" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    
     <div class="subject1">รายงานการประชุมสามัญผู้ถือหุ้น</div>
     
     
      
     <table class="tb-quarter"  width="80%">
        <tr>
            <th class="no-borderleft" width="30%"> ปี</th>
            <th colspan="2">
              รายงานการประชุมสามัญผู้ถือหุ้น</th>
        </tr>
        <tr>
            <td width="30%" class="no-borderleft">
                2555</td>
            <td width="64"  class="no-border-leftright" ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"  /> 
            </td>
            <td width="341" class="no-border-leftright   align-left" > 
             <a href="downloads/investment/minute-8-2555.pdf">รายงานการประชุมสามัญผู้ถือหุ้น 2555</a> </td>
        </tr>
        <tr>
          <td class="no-borderleft"> 2554</td>
          <td  class="no-border-leftright" ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"  /></td>
          <td class="no-border-leftright   align-left" ><a href="downloads/investment/minute-7-2554.pdf">รายงานการประชุมสามัญผู้ถือหุ้น 2554</a></td>
        </tr>
        <tr>
          <td class="no-borderleft"> 2553</td>
          <td  class="no-border-leftright" ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"  /></td>
          <td class="no-border-leftright   align-left" ><a href="downloads/investment/minute-6-2553.pdf">รายงานการประชุมสามัญผู้ถือหุ้น 2553</a></td>
        </tr>
        <tr>
          <td class="no-borderleft"> 2552</td>
          <td  class="no-border-leftright" ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"  /></td>
          <td class="no-border-leftright   align-left" ><a href="downloads/investment/minute-5-2552.pdf">รายงานการประชุมสามัญผู้ถือหุ้น 2552</a></td>
        </tr>
     </table>
   
  
     
     
<div class="notation2">
เอกสารเป็นไฟล์ PDF การเปิดดูจำเป็นต้องมีโปรแกรม  Adobe Reader คลิ๊กที่ไอคอนโปรแกรมเพื่อติดตั้ง   
<a href="http://www.adobe.com/products/acrobat/readstep2.html"
target="_blank"><img src="<%=Config.SiteUrl%>images/icon-pdf-download.gif"  />
</a>
</div>


     
</asp:Content>

