﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="buyInsureTravel_step1.aspx.cs" Inherits="buyInsureTravel_step1" EnableEventValidation="false"%>
<%@ Register src="~/uc/ucTxtDate.ascx" tagname="ucTxtDate" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script>
        function PopulateMedical() {
            var pageUrl = '<%=ResolveUrl("~/buyInsureTravel_step1.aspx")%>';
            var strCarName;
            var strCarCategory;
            var strCarUse;
            var ddlChild;

            //$(ddlChild).attr("disabled", "disabled");
            strSumIns = document.getElementById("<%=ddlSumIns.ClientID %>").value;
            ddlChild = document.getElementById("<%=ddlMedical.ClientID %>");
            $.ajax({
                async: false,
                type: "POST",
                url: pageUrl + '/GetCoverMedical',
                data: '{strSumIns:"' + strSumIns + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(response) {
                    PopulateDropdownControl(response.d, $(ddlChild))
                },
                error: function(response) {
                    alert("error " + response.d);
                }
            });
        }
        function PopulateDropdownControl(list, control) {
            //control.removeAttr("disabled");
            if (list.length > 0) {
                //control.empty().append('<option selected="selected" value="0">Please select</option>');
                control.empty();
                $.each(list, function() {
                    control.append($("<option></option>").val(this['Value']).html(this['Text']));
                });
                control.removeAttr("disabled");
            }
            else {
                control.empty().append('<option selected="selected" value=""> - เลือก - <option>');
                control.attr("disabled", "disabled");
            }
        }
        function validateForm() {
            var isRet = true;
            var isRequire = true;
            var isLength = true;
            var isDate = true;
            var strMsg = "";
            var strMsgRequire = "";
            var strMsgLength = "";
            var strMsgDate = "";
            if (document.getElementById("<%=txtNumIns.ClientID %>").value == "") {
                strMsg += "    - กลุ่มบุคคล\n";
                isRet = false;
                isRequire = false;
            }       
            if (document.getElementById("<%=txtFName.ClientID %>").value == "") {
                strMsg += "    - ชื่อ\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtLName.ClientID %>").value == "") {
                strMsg += "    - สกุล\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtAddress.ClientID %>").value == "") {
                strMsg += "    - ที่อยู่\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtSubDistrict.ClientID %>").value == "") {
                strMsg += "    - ตำบล / แขวง\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtDistrict.ClientID %>").value == "") {
                strMsg += "    - อำเภอ / เขต\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=ddlProvince.ClientID %>").value == "") {
                strMsg += "    - จังหวัด\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtPostCode.ClientID %>").value == "") {
                strMsg += "    - รหัสไปรษณีย์\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtMobile.ClientID %>").value == "") {
                strMsg += "    - เบอร์โทรศัพท์มือถือ\n";
                isRet = false;
                isRequire = false;
            }
            // ผู้รับผลประโยชน์
            if (document.getElementById("<%=txtBefFName.ClientID %>").value == "") {
                strMsg += "    - ชื่อ\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtBefLName.ClientID %>").value == "") {
                strMsg += "    - นามสกุล\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=ddlBefRelationship.ClientID %>").value == "") {
                strMsg += "    - ความสัมพันธ์\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=rdoInsAddress.ClientID %>").checked == false &&
                document.getElementById("<%=rdoOtherAddress.ClientID %>").checked == false) {
                if (document.getElementById("<%=txtBefAddress.ClientID %>").value == "") {
                    strMsg += "    - ที่อยู่\n";
                    isRet = false;
                    isRequire = false;
                }
                if (document.getElementById("<%=txtBefSubDistrict.ClientID %>").value == "") {
                    strMsg += "    - ตำบล / แขวง\n";
                    isRet = false;
                    isRequire = false;
                }
                if (document.getElementById("<%=txtBefDistrict.ClientID %>").value == "") {
                    strMsg += "    - อำเภอ / เขต\n";
                    isRet = false;
                    isRequire = false;
                }
                if (document.getElementById("<%=ddlBefProvince.ClientID %>").value == "") {
                    strMsg += "    - จังหวัด\n";
                    isRet = false;
                    isRequire = false;
                }
                if (document.getElementById("<%=txtBefPostcode.ClientID %>").value == "") {
                    strMsg += "    - รหัสไปรษณีย์\n";
                    isRet = false;
                    isRequire = false;
                }
                if (document.getElementById("<%=txtBefTelephone.ClientID %>").value == "") {
                    strMsg += "    - เบอร์โทรศัพท์\n";
                    isRet = false;
                    isRequire = false;
                }
            }
            
            // ความคุ้มครอง
            if (document.getElementById("<%=ddlMedical.ClientID %>").value == "") {
                strMsg += "    - ค่ารักษาพยาบาล\n";
                isRet = false;
                isRequire = false;
            }

            if (isRequire == false) {
                strMsg = "!กรุณาระบุ\n" + strMsg;
            }

            if (isLength == false) {
                strMsgLength = "!ขนาดข้อมูลที่ระบุไม่ถูกต้อง\n" + strMsg;
            }

            if (isRet == false) {
                //alert(strMsg + strMsgLength + strMsgDate);
                showMessagePage(strMsg + strMsgLength + strMsgDate);
            }
            else {
                document.getElementById("divLoading").style.display = "block";
                document.getElementById('divPremium').style.display = 'none';
            }
            return isRet;
        }
        function showMessagePage(strMsg) {
            document.getElementById("MsgIframe").setAttribute("src", "ShowMessageBox.aspx?msg=" + strMsg);
            //alert("url : " + link);
            $("[id*=dialog]").dialog({
                autoOpen: false,
                show: "fade",
                hide: "fade",
                modal: true,
                //open: function (ev, ui) {
                //    $('#myIframe').src = 'http://www.google.com';
                //},
                maxHeight: '400',
                width: '400',
                resizable: true
                //title: 'Title Topic'
            });
            //            $("#dialog1").dialog();

            $("#dialogMsg").dialog("open");
        }
        function closeMessagePage() {
            $('#dialogMsg').dialog('close');
            return false;

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div id="page-online-insCar"> 
        <h1 class="subject1">ซื้อประกัน-ประกันเดินทาง  </h1>
        <%--<div class="btnCallBack"><a href="./buyInsure.aspx?main_class=P" title="ซื้อประกันภัยอุบัติเหตุส่วนบุคคล และการเดินทาง">ซื้อประกัน</a></div>--%>
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="True">
        </asp:ScriptManager>
        <div class="stepOnline"> <img src="images/navi2step_s-1.jpg" /></div>
        <div class="prc-row">    
            <h2 class="subject-detail">
                รายละเอียดผู้เอาประกัน  
                <div class="label">กรณีมีผู้เอาประกันมากกว่า 1 คน กรุณาแฟกซ์ชื่อผู้เอาประกันไปที่ 02-3772097 หรือ Email ไปที่ reg_misc@smk.co.th</div>
            </h2>
            <table width="630px">
                <tr>
                    <td valign="top">
                        <table class="tb-online-insCar">    
                            <tr>
                                <td class="label" >
                                    กลุ่มบุคคล :
                                </td>
                                <td >
                                    <asp:TextBox ID="txtNumIns" runat="server" MaxLength="2" Width="20px" onkeypress="return isKeyInteger()"></asp:TextBox>
                                    <span class="star">*</span>
                                </td>
                            </tr>          
                            <tr>
                                <td class="label" >
                                    ชื่อ :
                                </td>
                                <td >
                                    <asp:TextBox ID="txtFName" runat="server"></asp:TextBox>
                                    <span class="star">*</span>
                                </td>    
                                <td class="label">
                                    นามสกุล :
                                </td>
                                <td>
                                    <asp:TextBox ID="txtLName" runat="server"></asp:TextBox>
                                    <span class="star">*</span>
                                </td>                            
                            </tr>
                            <tr>
                                <td class="label">
                                    ที่อยู่ :
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtAddress" runat="server" Width="250" MaxLength="50"></asp:TextBox>
                                    <span class="star">*</span>(ใส่ บ้านเลขที่, หมู่, ถนน)
                                </td>
                            </tr>
                            <tr>                    
                                <td class="label"  >
                                    
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtSubDistrict" runat="server" Width="250" MaxLength="50"></asp:TextBox>
                                    <span class="star">*</span>(ใส่ ตำบล, แขวง)
                                </td>
                            </tr>
                            <tr>
                                <td class="label" >
                                    อำเภอ / เขต :
                                </td>
                                <td width="150px">
                                    <asp:TextBox ID="txtDistrict" runat="server"></asp:TextBox>
                                    <span class="star">*</span>
                                </td>      
                                <td class="label">
                                    จังหวัด :
                                </td>
                                <td  >
                                    <asp:DropDownList ID="ddlProvince" runat="server">
                                    </asp:DropDownList>
                                    <span class="star">*</span>
                                </td>                          
                            </tr>
                            <tr>
                                <td class="label">
                                    รหัสไปรษณีย์ :
                                </td>
                                <td width="130px">
                                    <asp:TextBox ID="txtPostCode" runat="server" MaxLength="5"></asp:TextBox>
                                    <span class="star">*</span>
                                </td>      
                                <td class="label">
                                    เลขที่บัตรประชาชน :
                                </td>
                                <td  >
                                    <asp:TextBox ID="txtIDCard" runat="server"></asp:TextBox>                        
                                </td>                         
                            </tr>
                            <tr>
                                <td class="label">
                                    เบอร์โทรศัพท์มือถือ :
                                </td>
                                <td>
                                    <asp:TextBox ID="txtMobile" runat="server"></asp:TextBox>
                                    <span class="star">*</span>
                                </td>  
                                <td class="label">
                                    เบอร์โทรศัพท์บ้าน :
                                </td>
                                <td>
                                    <asp:TextBox ID="txtTelephone" runat="server"></asp:TextBox>                        
                                </td>                                                  
                            </tr>
                            <tr>
                                <td class="label">
                                    อีเมล์ :
                                </td>
                                <td>
                                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>                                    
                                </td>                    
                            </tr>
                            <tr>
                                <td class="label">
                                    เริ่มเดินทางวันที่ :
                                </td>
                                <td>
                                    <uc1:ucTxtDate ID="txtStartDate" runat="server" />
                                    <span class="star">*</span>
                                </td>  
                                <td class="label">
                                    เวลา :
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlHour" runat="server" Width="40" style="margin-right:0px"></asp:DropDownList>
                                    :
                                    <asp:DropDownList ID="ddlMinute" runat="server" Width="40"></asp:DropDownList>
                                </td>                                                  
                            </tr>
                            <tr>
                                <td class="label">
                                    จำนวนวันเดินทาง :
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlDays" runat="server" Width="40"></asp:DropDownList>
                                </td>                    
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>               
            <h2 class="subject-detail">
                รายละเอียดผู้รับประโยชน์  
                <div class="label">
                    ไม่ต้องระบุ ในกรณีมีผู้รับประโยชน์มากกว่า 1 คน กรุณา FAX ชื่อผู้รับประโยชน์ไปที่ 02-3772097 หรือ Email ไปที่ reg_misc@smk.co.th
                </div>
            </h2>            
            <table class="tb-online-insCar">              
                <tr>
                    <td class="label" >
                        ชื่อ :
                    </td>
                    <td >
                        <asp:TextBox ID="txtBefFName" runat="server"></asp:TextBox>
                        <span class="star">*</span>
                    </td>    
                    <td class="label">
                        นามสกุล :
                    </td>
                    <td>
                        <asp:TextBox ID="txtBefLName" runat="server"></asp:TextBox>
                        <span class="star">*</span>
                    </td>                            
                </tr>
                <tr>
                    <td class="label">ความสัมพันธ์	 :</td>
                    <td>
                        <asp:DropDownList ID="ddlBefRelationship" runat="server">
                        </asp:DropDownList><span class="star">*</span>
                    </td>
                </tr>
                <tr>
                    <td class="label" valign="top">ที่อยู่ :</td>
                    <td colspan="3">
                        <asp:RadioButton ID="rdoInsAddress" runat="server" text="ที่อยู่เดียวกับผู้เอาประกัน" GroupName="BefAddress" Checked="true"/>
                        <asp:RadioButton ID="rdoOtherAddress" runat="server" text="ที่อยู่อื่นๆ" GroupName="BefAddress"/>                    
                    </td>
                </tr>
                <tr>
                    <td class="label">                    
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="txtBefAddress" runat="server" Width="365"></asp:TextBox>                    
                    </td>
                </tr>
                <tr>                    
                    <td class="label"  >
                        ตำบล / แขวง :
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="txtBefSubDistrict" runat="server" Width="365"></asp:TextBox>                    
                    </td>
                </tr>
                <tr>
                    <td class="label" >
                        อำเภอ / เขต :
                    </td>
                    <td width="150px">
                        <asp:TextBox ID="txtBefDistrict" runat="server"></asp:TextBox>                    
                    </td>      
                    <td class="label">
                        จังหวัด :
                    </td>
                    <td  >
                        <asp:DropDownList ID="ddlBefProvince" runat="server">
                        </asp:DropDownList>                    
                    </td>                          
                </tr>
                <tr>
                    <td class="label">
                        รหัสไปรษณีย์ :
                    </td>
                    <td width="130px">
                        <asp:TextBox ID="txtBefPostcode" runat="server"></asp:TextBox>                    
                    </td>  
                    <td class="label">
                        เบอร์โทรศัพท์ :
                    </td>
                    <td>
                        <asp:TextBox ID="txtBefTelephone" runat="server"></asp:TextBox>                    
                    </td>                             
                </tr>
                <tr>
                    <td class="label">
                        อีเมล์ :
                    </td>
                    <td>
                        <asp:TextBox ID="txtBefEmail" runat="server"></asp:TextBox>                                    
                    </td>                    
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>
            <h2 class="subject-detail">รายละเอียดความคุ้มครอง </h2>
            <table class="tb-buy-insure">
                <tr>
                    <td class="label" width="180" >การเสียชีวิต การสูญเสียอวัยวะ	 :</td>
                    <td >
                        <asp:DropDownList ID="ddlSumIns" runat="server">
                        </asp:DropDownList> บาท
            
                    </td>
                    <td class="label">ค่ารักษาพยาบาล :</td>
                    <td>
                        <asp:DropDownList ID="ddlMedical" runat="server"></asp:DropDownList><span class="star">*</span>  บาท
                    </td>   
                </tr>
                <tr>
                    <td colspan="4">
                        <br />
                        <div class="notation" style="text-align:center;">       
                            บริษัทสินมั่นคงประกันภัย จำกัด (มหาชน) <br />ขอสงวนสิทธิในการให้ความคุ้มครองและเปลี่ยนแปลงเบี้ยประกันภัย ในกรณีข้อมูลของท่านไม่ตรงกับความเป็นจริง
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <div style="margin: 0px auto; width: 250px; margin-top: 15px;">                        
                            <asp:Button ID="btnCalculate" runat="server" Text="คำนวณเบี้ย" class="btn-default" Style="margin-right: 10px;" OnClick="btnCalculate_Click" OnClientClick="return validateForm();"/>
                            <input id="btnClear" type="reset" class="btn-default " value="เคียร์ข้อมูล" />
                        </div>
                    </td>
                    <a name="grd"></a>
                </tr>
            </table>            
            <div id="divLoading" style="text-align:center;display:none;">
                <img src="./images/loading.gif" />
            </div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divPremium" style="display:none">
            <h2 class="subject-detail">ยอดเบี้ยประกันที่ต้องชำระ</h2>
            <table   class="tb-online-insCar">           
                <tr>
                    <td class="label">เบี้ยประกันภัยปกติรวม :</td>
                    <td>
                        <asp:Label ID="lblPremium" runat="server" Text=""></asp:Label>
                        บาท
                    </td>
                </tr>
                <tr>
                    <td class="label">เบี้ยประกันภัยพิเศษผ่านอินเทอร์เน็ต :</td>
                    <td>
                        <asp:Label ID="lblInternetPremium" runat="server" Text=""></asp:Label>
                        บาท
                    </td>
                </tr>                
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="4">
                        <div style="margin: 0px auto; width: 250px; margin-top: 15px;">                        
                            <asp:Button ID="btnSave" runat="server" Text="ถัดไป >>" class="btn-default" Style="margin-right: 10px;" OnClick="btnSave_Click"/>
                            <input id="Reset1" type="reset" class="btn-default " value="ยกเลิก" />
                        </div>
                    </td>
                    <a name="grd"></a>
                </tr>
            </table>   
            </div>
                    <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnCalculate" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel> 
        </div><!-- End class="prc-row"-->
        <div id="dialogMsg" style="display:none" title="การตรวจสอบข้อมูล"   >
            <iframe id="MsgIframe"   frameborder="0" style="max-height:360" width="370" scrolling="auto"></iframe>
        </div>
    </div><!-- End  class="page-online-insCar" -->
    <div style="display:none">
        <asp:TextBox ID="txtTmpCD" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtPremium" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtStamp" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtTax" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtDiscountRate" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtDiscountAmt" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtPremiumDiscount" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtGrossPremiumDiscount" runat="server"></asp:TextBox>
        
    </div>
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

