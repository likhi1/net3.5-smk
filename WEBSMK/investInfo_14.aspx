﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="investInfo_14.aspx.cs" Inherits="investInfo_14" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

   <div class="subject1">หนังสือเชิญประชุมผู้ถือหุ้น</div>

<table class="tb-quarter"  width="80%">
        <tr>
            <th class="no-borderleft" width="30%"> ปี</th>
            <th colspan="2">
              หนังสือเชิญประชุมผู้ถือหุ้น</th>
        </tr>
        <tr>
            <td width="30%" class="no-borderleft">
                2556</td>
            <td width="64"  class="no-border-leftright" ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"  /> 
            </td>
            <td width="341" class="no-border-leftright   align-left" > 
             <a href="downloads/investment/meetinginvi-11-2556.pdf">หนังสือเชิญประชุมผู้ถือหุ้นประจำปี 2556</a> </td>
        </tr>
        <tr>
          <td class="no-borderleft"> 2555</td>
          <td  class="no-border-leftright" ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"  /></td>
          <td class="no-border-leftright   align-left" ><a href="downloads/investment/meetinginvi-10-2555.pdf">หนังสือเชิญประชุมผู้ถือหุ้นประจำปี 2555</a></td>
        </tr>
        <tr>
          <td class="no-borderleft"> 2554</td>
          <td  class="no-border-leftright" ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"  /></td>
          <td class="no-border-leftright   align-left" ><a href="downloads/investment/meetinginvi-9-2554.pdf">หนังสือเชิญประชุมผู้ถือหุ้นประจำปี 2554</a></td>
        </tr>
        <tr>
          <td class="no-borderleft"> 2553</td>
          <td  class="no-border-leftright" ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"  /></td>
          <td class="no-border-leftright   align-left" ><a href="downloads/investment/meetinginvi-8-2553.pdf">หนังสือเชิญประชุมผู้ถือหุ้นประจำปี 2553</a></td>
        </tr>
     </table>
   
     
          <div class="notation2">
เอกสารเป็นไฟล์ PDF การเปิดดูจำเป็นต้องมีโปรแกรม  Adobe Reader คลิ๊กที่ไอคอนโปรแกรมเพื่อติดตั้ง   
<a href="http://www.adobe.com/products/acrobat/readstep2.html"
target="_blank"><img src="<%=Config.SiteUrl%>images/icon-pdf-download.gif"  />
</a>
</div>
     
     
</asp:Content>

