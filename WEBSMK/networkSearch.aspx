﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="networkSearch.aspx.cs" Inherits="networkSearch"    %>
 
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  
    <script>
        //===== Set Global var
        cat = parseInt( <%=CatId%>);
        zone = parseInt( <%=ZoneId%>);

        $(function () { 
            //===== Run Function
            manageAllOverlappingTab();
            limitControler();
        });

        //======== Open  Modal
        function clickModal() {
            $(".openerCover").click(function (e) {
                e.preventDefault();
                setModal();
                console.log("clicked");
                $("#areaIframe1").attr('src', $(this).attr('href'));
                $("#dialog1").dialog("open"); 
            }) 
           // $("#dialog1").css("padding", 10);
        }

        //======== Set  Jquery UI - Modal 
        function setModal() {
            $("[id*=dialog]").dialog({
                autoOpen: false,
                show: "fade",
                hide: "fade",
                modal: true,
                //open: function (ev, ui) {
                //    $('#myIframe').src = 'http://www.google.com';
                //},
                resizable: false,  
                //title: 'Title Topic',
                height: '580',
                width: '985',
                dialogClass: 'yourclassname',
                headerVisible: false,// or true
                draggable: false,
                buttons: { CLOSE: function () { $(this).dialog("close"); } }
            });  
            //////// Config - Jquery UI Modal
            $(".ui-dialog-titlebar").hide();
        }

        //====== Limit  Select DropDownList
        function limitControler() {
            var ddlProvince = $("#<%=ddlProvince.ClientID%>");
            var ddlAmphoe = $("#<%=ddlAmphoe.ClientID%>");
            var ddlNetworkType = $("#<%=ddlNetworkType.ClientID%>");

            ddlNetworkType.attr("disabled", "disabled");
            ddlAmphoe.attr("disabled", "disabled");
            
            ///// Check for show NetworkType 
            if (ddlProvince.val() == "กรุงเทพมหานคร") {
                ddlAmphoe.removeAttr("disabled");
            }

            ///// Check for show NetworkType 
            if (cat == 2 || cat == 4) {
                ddlNetworkType.removeAttr("disabled");
            } else {
                ddlNetworkType.attr("disabled", "disabled");
            }

            //// When Change Dropdown
            ddlProvince.change(function () {
                if (ddlProvince.val() == "กรุงเทพมหานคร")
                    ddlAmphoe.removeAttr("disabled");
                else
                    ddlAmphoe.attr("disabled", "disabled");
                    ddlAmphoe.val('');
            }); 
         
        }

        //============== Manage All Overlapping Tabs  
        function manageAllOverlappingTab() {
            //////// Set Active Tabs 
            $now = $('.obtabs li.item<%=ZoneId%>').attr("id", "current");
            /////// Set Show/Hide  Center Branch For Category 4   
            if (cat == 4 && zone == 0) {
                $("#smkCenter").show();
            } else {
                $("#smkCenter").hide();
            }
            /////// Set Switch Show/Hide Google Map
            $("[id^=btnMap]").click(function () {
                elm = ($(this).attr("id"));
                elmId = elm.substring(6);
                $("[id^=mapSmk]").hide();
                $("#mapSmk" + elmId).show();
            });
        }
         

    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
<div id="page-network">
<h1 class="subject1">ผลการค้นหา 

   เครือข่าย <asp:Label ID="lblPageName" runat="server"></asp:Label>
</h1> 

<%--<asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>--%>
 
 <!-- =========================================== Search Area  -->
        <div id="search-area">
        <table style="width: 100%;">
            <tr>
                <td>  ประเภท : </span><asp:DropDownList ID="ddlNetworkType" runat="server">
                    <asp:ListItem Value="">-- โปรดระบุ  --</asp:ListItem>
                </asp:DropDownList></td>
                <td> เลือกจังหวัด : </span><asp:DropDownList ID="ddlProvince" runat="server">
                    <asp:ListItem Value="">-- โปรดระบุ  --</asp:ListItem>
                </asp:DropDownList></td>
                <td>เลือกเขต :
                    <asp:DropDownList ID="ddlAmphoe" runat="server">
                        <asp:ListItem Value="">-- โปรดระบุ  --</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td colspan="3">
                    <div style="margin-left:100px;"> 
                    <span  style="float:left;margin-right:5px;"  > 
                    เลือกคำค้น :   <asp:TextBox ID="tbxKeyWord" runat="server" Width="300px"></asp:TextBox>
                    </span> 
  <span >
                    <asp:Button ID="BtnSearch" runat="server" Text="ค้นหา" class="btn-default" Style="height: 22px;" OnClick="BtnSearch_Click" />
 </span>
                    </div>   
</td>
               </div>  
            </tr>
        </table>

     </div>

        <div class="clear"></div>
 


        <style>
    /*===== Control  GridView  Width (support IE9 & Over) =====*/
    [id*=GridView] { width: 100%; /*border: 1px solid red;*/ }
    [id*=GridView] th { height: 20px; color: #fff; background-color: #485ed1; padding: 5px 5px 7px 5px; font-size: 13px; vertical-align: top; border: 1px solid #fff; }
    [id*=GridView] th:nth-child(1) { }
    [id*=GridView] th:nth-child(2) { width: 15%; text-align: center; }
    [id*=GridView] th:nth-child(3) { width: 10%; text-align: center; }
    [id*=GridView] td { padding: 2px 5px; height: 20px; line-height: 18px; vertical-align: top; border: 1px dotted #ddd; }
    [id*=GridView] td .name { }
    [id*=GridView] td:nth-child(2) { text-align: center; }
    [id*=GridView] td:nth-child(3) { text-align: center; }
         
        </style>

            <asp:GridView ID="GridView1" runat="server" EnableModelValidation="True"
                 AutoGenerateColumns="false" 
            DataKeyNames="NtwId"
            OnRowDataBound="GridView1_RowDataBound" 
            OnRowDeleting="GridView1_Deleting"
            OnPageIndexChanging="GridView1_IndexChanging"
            OnRowEditing="modEditCommand"
            OnRowUpdating="modUpdateCommand"
            OnRowCancelingEdit="modCancelCommand"  >

                        <Columns>

                            <asp:TemplateField   HeaderText="ชื่อ-ที่อยู่" >
<ItemTemplate>
<div class="rowAddress">
<div  class="name"><asp:Label runat="server" ID="lblName"></asp:Label>
<asp:Label runat="server" ID="lblService"></asp:Label> 
</div> 
                                     
<div  ><asp:Label runat="server" ID="lblAddress"></asp:Label></div>
<div  ><asp:Label runat="server" ID="lblMobile"></asp:Label></div>
<div  ><asp:Label runat="server" ID="lblPhone"></asp:Label></div>
<div  ><asp:Label runat="server" ID="lblNotation"></asp:Label></div>
<div><asp:HyperLink ID="hplWebsite" runat="server">HyperLink</asp:HyperLink> </div>

</div>
</ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField   HeaderText="จังหวัด/อำเภอ" >
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblProvince"></asp:Label> 

                                    <asp:Label runat="server" ID="lblAmphoe"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField    HeaderText="แผนที่" >
                                <ItemTemplate>
                                    <asp:HyperLink runat="server" ID="hplMap"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField> 

                        </Columns>
                    </asp:GridView>


<div id="dialog1" style="display:none; z-index:1111; overflow:hidden"     >
  <iframe id="areaIframe1"   frameborder="0"  scrolling="no" width="1000px"   height="550px"  ></iframe>
</div>

    </div>
    <!-- End  id="page-network"-->

</asp:Content>

