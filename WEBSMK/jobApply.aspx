﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="jobApply.aspx.cs" Inherits="jobApply" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
     <!-- ===============  Date  Picker - Config -->
    <script>
        $(function () {
            $("[id*=tbxDate]").attr('readOnly', 'true');
            $("[id*=tbxDate]").datepicker({
                 changeMonth: true,
                 changeYear: true,
                  yearRange: "-70:+0",
                isBE: true,
                autoConversionField: false
            })

        });
    </script>

       <!-- ===============  Validate  -->

     <script>
         $(function () {
             //// Set Varbles
             ddlSelectPosi1 = $("#<%=ddlSelectPosi1.ClientID%>");
             tbxSalary = $("#<%=tbxSalary.ClientID%>"); 
             ddlNamePrefix = $("#<%=ddlNamePrefix.ClientID%>");
             tbxName1 = $("#<%=tbxName1.ClientID%>");
             tbxName2 = $("#<%=tbxName2.ClientID%>");
             tbxDateBrith = $("#<%=tbxDateBrith.ClientID%>");  
             ddlAge = $("#<%=ddlAge.ClientID%>");
             tbxIdCard = $("#<%=tbxIdCard.ClientID%>"); 
             tbxTel1 = $("#<%=tbxTel1.ClientID%>");
             tbxEmail = $("#<%=tbxEmail.ClientID%>"); 
             tarAddress1 = $("#<%=tarAddress1.ClientID%>");
             tarAddress2 = $("#<%=tarAddress2.ClientID%>");
             ddlEduLevel1 = $("#<%=ddlEduLevel1.ClientID%>");
             tbxEduName1 = $("#<%=tbxEduName1.ClientID%>"); 
             tbxEduBg1 = $("#<%=tbxEduBg1.ClientID%>"); 
             tbxEduMajor1 = $("#<%=tbxEduMajor1.ClientID%>"); 
             tbxEduAvg1 = $("#<%=tbxEduAvg1.ClientID%>"); 
             tbxEduYear1 = $("#<%=tbxEduYear1.ClientID%>");  
             tarSklCom = $("#<%=tarSklCom.ClientID%>");
               
        btnSubmit = $("#<%=btnSave.ClientID%>");

        btnSubmit.click(function () {
            return ChkNull();
        })

    });

    function ChkNull() {
        if (ddlSelectPosi1.val() == "" || ddlSelectPosi1.val() == null) {
            alert("กรุณาระบุตำแหน่งงานด้วย");
            return false; 
        } else if (tbxSalary.val() == "" || tbxSalary.val() == null) {
            alert("กรุณาระบุเงินเดือนด้วย");
            return false;
        } else if (ddlNamePrefix.val() == "" || ddlNamePrefix.val() == null) {
            alert("กรุณาระบุคำนำหน้า");
            return false;  
        } else if (tbxName1.val() == "" || tbxName1.val() == null) {
            alert("กรุณาระบุชื่อ");
            return false;
        } else if (tbxName2.val() == "" || tbxName2.val() == null) {
            alert("กรุณาระบุนามสกุล");
            return false; 
        } else if (tbxDateBrith.val() == "" || tbxDateBrith.val() == null) {
            alert("กรุณาระบุวันเกิด");
            return false;
        } else if (ddlAge.val() == "" || ddlAge.val() == null) {
            alert("กรุณาระบุอายุ");
            return false;
        } else if (tbxIdCard.val() == "" || tbxIdCard.val() == null) {
            alert("กรุณาระบุเลขที่บัตรประชาชน");
            return false;
        } else if (tbxTel1.val() == "" || tbxTel1.val() == null) {
            alert("กรุณาระบุเบอรโทรศัพท์");
            return false;
        } else if (tbxEmail.val() == "" || tbxEmail.val() == null) {
            alert("กรุณาระบุอีเมล");
            return false;
        } else if (tarAddress1.val() == "" || tarAddress1.val() == null) {
            alert("กรุณาระบุที่อยู่ปัจจุบันที่ติดต่อได้");
            return false;
        }

        return true;
    }

    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">


    <div id="page-jobProfile">
        <div class="subject1">สมัครงานออนไลน์</div>


<style>
#page-jobProfile { }
#page-jobProfile table td { vertical-align: top; }
#page-jobProfile .label { text-align: right; padding-right: 5px; vertical-align: top; }
#page-jobProfile .tbPastWork td:nth-child(2) { border-right: 1px dotted #ccc; }

/*
[id*=TbProfile] td:nth-child(1) { width: 5%; text-align: right; width: 120px; } 
[id*=TbProfile] textarea { } 
[id*=TbProfile] select { }
*/
.auto-style1 { height: 24px; }
</style>


        <h2>ประวัติส่วนตัว</h2>


        <table style="width: 100%;">
            <tr>
                <td class="label" width="130px">ตำแหน่งลำดับที่ 1 : </td>
                <td><asp:DropDownList ID="ddlSelectPosi1" runat="server"  ></asp:DropDownList> <span class="star">*</span>
                </td>
            </tr>

            <tr>
                <td class="label" width="130px">ตำแหน่งลำดับที่ 2 :</td>
                <td>
                  <asp:DropDownList ID="ddlSelectPosi2" runat="server"  ></asp:DropDownList>

                </td>
            </tr>

            <tr>
                <td class="label" width="130px">ตำแหน่งลำดับที่ 3 : </td>
                <td> 
                  <asp:DropDownList ID="ddlSelectPosi3" runat="server"  ></asp:DropDownList>

                </td>
            </tr>

            <tr>
                <td class="label" width="130px">เงินเดือนที่ต้องการ : </td>
                <td>
                    <asp:TextBox ID="tbxSalary" runat="server"></asp:TextBox>
                    บาท <span class="star">*</span></td>
            </tr>

            <tr>
                <td class="label" width="130px">เคยสมัครงานกับสินมั่นคง :</td>
                <td>


                    <asp:RadioButtonList ID="rblFirstApply" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Selected="True" Value="0">ไม่เคย</asp:ListItem>
                        <asp:ListItem Value="1">เคย</asp:ListItem>
                    </asp:RadioButtonList>






                </td>
            </tr>

        </table>
        <table style="width: 100%;">
            <tr>
                <td>
                    <table width="100%" id="Table2">
                        <tr>
                            <td class="label" width="125px">คำนำหน้า :</td>
                            <td>

                                <asp:DropDownList ID="ddlNamePrefix" runat="server">
                                   <asp:ListItem Selected="True" Value="">-- กรุณาระบุ --</asp:ListItem>
                                      <asp:ListItem>นาย</asp:ListItem>
                                    <asp:ListItem>นาง</asp:ListItem>
                                    <asp:ListItem>นางสาว</asp:ListItem>
                                </asp:DropDownList> <span class="star">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">ชื่อ
        :</td>
                            <td>


                                <asp:TextBox ID="tbxName1" runat="server"></asp:TextBox> <span class="star">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">นามสกุล
        :
                            </td>
                            <td>


                                <asp:TextBox ID="tbxName2" runat="server"></asp:TextBox> <span class="star">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">วันเกิด :</td>
                            <td>
                                <asp:TextBox ID="tbxDateBrith" runat="server"></asp:TextBox> <span class="star">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">อายุ :</td>
                            <td>

                                <asp:DropDownList ID="ddlAge" runat="server">
                                </asp:DropDownList> <span class="star">*</span>

                                ปี </td>
                        </tr>

                        <tr>
                            <td class="label" style="height: 24px">สัญชาติ :</td>
                            <td class="auto-style1">
                                <asp:TextBox ID="tbxNation" runat="server"></asp:TextBox></td>
                        </tr>

                        <tr>
                            <td class="label">ศาสนา :</td>
                            <td>
                                <asp:TextBox ID="tbxReligion" runat="server"></asp:TextBox></td>
                        </tr>

                    </table>
                </td>
                <td>
                    <table width="100%" id="Table3">
                        <tr>
                            <td class="label">น้ำหนัก :</td>
                            <td>
                                <asp:TextBox ID="tbxWeight" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td class="label">ส่วนสูง
        :
                            </td>
                            <td>
                                <asp:TextBox ID="tbxHeight" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td class="label">เลขที่บัตรประชาชน :</td>
                            <td>
                                <asp:TextBox ID="tbxIdCard" runat="server"></asp:TextBox> <span class="star">*</span></td>
                        </tr>
                        <tr>
                            <td class="label">เบอร์มือถือ :</td>
                            <td>
                                <asp:TextBox ID="tbxTel1" runat="server"></asp:TextBox> <span class="star">*</span></td>
                        </tr>
                        <tr>
                            <td class="label">เบอร์บ้าน :</td>
                            <td>
                                <asp:TextBox ID="tbxTel2" runat="server"></asp:TextBox></td>
                        </tr>

                        <tr>
                            <td class="label">อีเมลล์ :</td>
                            <td>
                                <asp:TextBox ID="tbxEmail" runat="server"></asp:TextBox> <span class="star">*</span></td>
                        </tr>

                        <tr>
                            <td class="label">พันธะทางทหาร :</td>
                            <td>

                                <asp:RadioButtonList ID="rblMilitary" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True" Value="1">พ้น</asp:ListItem>
                                    <asp:ListItem Value="0">ไม่พ้น</asp:ListItem>
                                </asp:RadioButtonList>



                            </td>
                        </tr>

                    </table>
                </td>

            </tr>
        </table>

        <table width="100%" id="TbProfile">

            <tr>
                <td class="label" width="130px">ที่อยู่ปัจจุบันที่ติดต่อได้ :</td>
                <td>

                    <asp:TextBox ID="tarAddress1" runat="server" TextMode="MultiLine" Style="width: 465px"></asp:TextBox> <span class="star">*</span>

                </td>
            </tr>
            <tr>
                <td class="label">ที่อยู่ตามบัตรประชาชน :</td>
                <td>
                    <asp:TextBox ID="tarAddress2" runat="server" TextMode="MultiLine" Style="width: 465px"></asp:TextBox> 
                </td>
            </tr>
        </table>
        <h2>ประวัติการศึกษา</h2>
        <table width="100%">
            <tr>
                <td align="center">ระดับการศึกษา</td>
                <td align="center">สถาบัน</td>
                <td align="center">วุฒิ</td>
                <td align="center">วิชาเอก</td>
                <td align="center">เกรดเฉลี่ย</td>
                <td align="center">ปีที่จบ (พ.ศ.)</td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DropDownList ID="ddlEduLevel1" runat="server">
                    </asp:DropDownList>
                </td>
                <td align="center">
                    <asp:TextBox ID="tbxEduName1" runat="server"></asp:TextBox></td>
                <td align="center">
                    <asp:TextBox ID="tbxEduBg1" runat="server" style="width:100px"></asp:TextBox></td>
                <td align="center">
                    <asp:TextBox ID="tbxEduMajor1" runat="server" style="width:100px"></asp:TextBox></td>
                <td align="center">
                    <asp:TextBox ID="tbxEduAvg1" runat="server" Width="50px"></asp:TextBox></td>
                <td align="center">
                    <asp:TextBox ID="tbxEduYear1" runat="server" Width="50px"  ></asp:TextBox></td>
                <td> <span class="star">*</span></td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DropDownList ID="ddlEduLevel2" runat="server">
                    </asp:DropDownList>
                </td>
                <td align="center">
                    <asp:TextBox ID="tbxEduName2" runat="server"></asp:TextBox></td>
                <td align="center">
                    <asp:TextBox ID="tbxEduBg2" runat="server" style="width:100px"></asp:TextBox></td>
                <td align="center">
                    <asp:TextBox ID="tbxEduMajor2" runat="server"  style="width:100px"></asp:TextBox></td>
                <td align="center">
                    <asp:TextBox ID="tbxEduAvg2" runat="server" Width="50px"></asp:TextBox></td>
                <td align="center">
                    <asp:TextBox ID="tbxEduYear2" runat="server" Width="50px"></asp:TextBox></td>
            </tr>
            <tr>

                <td align="center">
                    <asp:DropDownList ID="ddlEduLevel3" runat="server">
                    </asp:DropDownList>
                </td>
                <td align="center">
                    <asp:TextBox ID="tbxEduName3" runat="server"  ></asp:TextBox></td>
                <td align="center">
                    <asp:TextBox ID="tbxEduBg3" runat="server" style="width:100px"></asp:TextBox></td>
                <td align="center">
                    <asp:TextBox ID="tbxEduMajor3" runat="server"  style="width:100px"></asp:TextBox></td>
                <td align="center">
                    <asp:TextBox ID="tbxEduAvg3" runat="server" Width="50px"></asp:TextBox></td>
                <td align="center">
                    <asp:TextBox ID="tbxEduYear3" runat="server" Width="50px"></asp:TextBox></td>
            </tr>

        </table>
        <table style="width: 100%;">
            <tr>
                <td class="label" width="130px">กิจกรรมระหว่างศึกษา :</td>
                <td>

                    <asp:TextBox ID="tarEduActivity" runat="server" TextMode="MultiLine" Style="width: 465px"></asp:TextBox>
                </td>
            </tr>
        </table>


        <h2>ประวัติการทำงาน&nbsp; </h2>
        <table width="100%" class="tbPastWork">
            <tr>
                <td class="label">1. บริษัท :</td>
                <td>
                    <asp:TextBox ID="tbxPsjName1" runat="server"></asp:TextBox></td>
                <td class="label">2. บริษัท :</td>
                <td>
                    <asp:TextBox ID="tbxPsjName2" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="label">ที่อยู่ :</td>
                <td>
                    <asp:TextBox ID="tbxPsjAddress1" runat="server"></asp:TextBox></td>
                <td class="label">ที่อยู่ :</td>
                <td>
                    <asp:TextBox ID="tbxPsjAddress2" runat="server"></asp:TextBox></td>
            </tr>
    <tr>
    <td class="label">ตำแหน่ง :</td>
    <td>
    <asp:TextBox ID="tbxPsjPosi1" runat="server"></asp:TextBox></td>
    <td class="label">ตำแหน่ง :</td>
    <td>
    <asp:TextBox ID="tbxPsjPosi2" runat="server"></asp:TextBox></td>
    </tr>
            <tr>
                <td class="label">ระยะเวลา :</td>
                <td>
                    <asp:TextBox ID="tbxPsjAge1" runat="server"></asp:TextBox></td>
                <td class="label">ระยะเวลา :</td>
                <td>
                    <asp:TextBox ID="tbxPsjAge2" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="label">เงินเดือน :</td>
                <td>
                    <asp:TextBox ID="tbxPsjSalary1" runat="server"></asp:TextBox></td>
                <td class="label">เงินเดือน :</td>
                <td>
                    <asp:TextBox ID="tbxPsjSalary2" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="label">หน้าที่ :</td>
                <td>
                    <asp:TextBox ID="tbxPsjDuty1" runat="server"></asp:TextBox></td>
                <td class="label">หน้าที่ :</td>
                <td>
                    <asp:TextBox ID="tbxPsjDuty2" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="label">สาเหตุที่ออก :</td>
                <td>
                    <asp:TextBox ID="tbxPsjReason1" runat="server"></asp:TextBox></td>
                <td class="label">สาเหตุที่ออก :</td>
                <td>
                    <asp:TextBox ID="tbxPsjReason2" runat="server"></asp:TextBox></td>
            </tr>
        </table>
        <h2>ทักษะและความสามารถพิเศษ</h2>
        <table width="95%">
            <tr>
                <td align="center" width="120px">ด้านภาษา</td>
                <td align="center">การฟัง</td>
                <td align="center">การพูด</td>
                <td align="center">การอ่าน</td>
                <td align="center">การเขียน</td>
            </tr>
            <tr>
                <td align="center">
                    <asp:TextBox ID="tbxSklLangName1" runat="server"></asp:TextBox></td>
                <td align="center">
                    <asp:DropDownList ID="ddlSklLangListen1" runat="server">
                    </asp:DropDownList>
                </td>
                <td align="center">
                    <asp:DropDownList ID="ddlSklLangSpeak1" runat="server">
                    </asp:DropDownList>
                </td>
                <td align="center">
                    <asp:DropDownList ID="ddlSklLangRead1" runat="server">
                    </asp:DropDownList>
                </td>
                <td align="center">
                    <asp:DropDownList ID="ddlSklLangWrite1" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:TextBox ID="tbxSklLangName2" runat="server"></asp:TextBox></td>
                <td align="center">
                    <asp:DropDownList ID="ddlSklLangListen2" runat="server">
                    </asp:DropDownList>
                </td>
                <td align="center">
                    <asp:DropDownList ID="ddlSklLangSpeak2" runat="server">
                    </asp:DropDownList>
                </td>
                <td align="center">
                    <asp:DropDownList ID="ddlSklLangRead2" runat="server">
                    </asp:DropDownList>
                </td>
                <td align="center">
                    <asp:DropDownList ID="ddlSklLangWrite2" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <table width="100%">
            <tr>
                <td class="label">ความสามารถทาง :
                    <br />คอมพิวเตอร์  &nbsp; </td>
                <td>
                    <asp:TextBox ID="tarSklCom" runat="server" TextMode="MultiLine" Style="width: 490px"></asp:TextBox> <span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td class="label">ความสามารถอื่นๆ :</td>
                <td>
                    <asp:TextBox ID="tarSklOther" runat="server" TextMode="MultiLine" Style="width: 490px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label">ข้อมูลอื่นๆเพิ่มเติม :<br />
                    (เพื่อแนะนำตัวท่าน)</td>
                <td>
                    <asp:TextBox ID="tarMoreInfo" runat="server" TextMode="MultiLine" Style="width: 490px"></asp:TextBox>
                </td>
            </tr>
        </table>
        <div style="margin: 0px auto; width: 170px; height:50px; ">

            <asp:Button ID="btnSave" runat="server" Text="ส่งใบสมัคร" class="btn-default btn-small " Style="margin-right: 5px;" OnClick="btnSave_Click" />
            <asp:Button ID="Button2" runat="server" Text="ยกเลิก" class="btn-default btn-small " OnClientClick="history.back();return false;" />


        </div>



    </div>
    <!-- End id ="page-jobProfile"  -->


</asp:Content>

