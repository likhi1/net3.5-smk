﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="buyInsureTravel_step2.aspx.cs" Inherits="buyInsureTravel_step2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script>
        function validateForm() {
            var isRet = true;
            var isRequire = true;
            var isLength = true;
            var isDate = true;
            var strMsg = "";
            var strMsgRequire = "";
            var strMsgLength = "";
            var strMsgDate = "";
            if (document.getElementById("<%=rdoCheck.ClientID%>").checked == false &&
                document.getElementById("<%=rdoTransfer.ClientID%>").checked == false &&
                document.getElementById("<%=rdoSCB.ClientID%>").checked == false &&
                document.getElementById("<%=rdoDelivery.ClientID%>").checked == false &&
                document.getElementById("<%=rdoCredit.ClientID%>").checked == false) {
                strMsg += "    - วิธีการชำระเบี้ยประกันภัย\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=rdoCheck.ClientID %>").checked == true &&
                document.getElementById("<%=ddlBranch.ClientID %>").value == "") {
                strMsg += "    - สาขาที่ต้องการชำระเงินด้วยเงินสดหรือเช็ค\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=rdoChange.ClientID %>").checked == true &&
                document.getElementById("<%=txtFName.ClientID %>").value == "") {
                strMsg += "    - ชื่อ\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=rdoChange.ClientID %>").checked == true &&
                document.getElementById("<%=txtLName.ClientID %>").value == "") {
                strMsg += "    - สกุล\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=rdoChange.ClientID %>").checked == true &&
                document.getElementById("<%=txtAddress.ClientID %>").value == "") {
                strMsg += "    - ที่อยู่\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=rdoChange.ClientID %>").checked == true &&
                document.getElementById("<%=txtSubDistrict.ClientID %>").value == "") {
                strMsg += "    - ตำบล / แขวง\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=rdoChange.ClientID %>").checked == true &&
                document.getElementById("<%=txtDistrict.ClientID %>").value == "") {
                strMsg += "    - อำเภอ / เขต\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=rdoChange.ClientID %>").checked == true &&
                document.getElementById("<%=ddlProvince.ClientID %>").value == "") {
                strMsg += "    - จังหวัด\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=rdoChange.ClientID %>").checked == true &&
                document.getElementById("<%=txtPostCode.ClientID %>").value == "") {
                strMsg += "    - รหัสไปรษณีย์\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=rdoChange.ClientID %>").checked == true &&
                document.getElementById("<%=txtTelephone.ClientID %>").value == "") {
                strMsg += "    - เบอร์โทรศัพท์\n";
                isRet = false;
                isRequire = false;
            }


            if (isRequire == false) {
                strMsg = "!กรุณาระบุ\n" + strMsg;
            }

            if (isLength == false) {
                strMsgLength = "!ขนาดข้อมูลที่ระบุไม่ถูกต้อง\n" + strMsg;
            }

            if (isRet == false) {
                //alert(strMsg + strMsgLength + strMsgDate);
                showMessagePage(strMsg + strMsgLength + strMsgDate);
            }
            return isRet;
        }
        function showMessagePage(strMsg) {
            document.getElementById("MsgIframe").setAttribute("src", "ShowMessageBox.aspx?msg=" + strMsg);
            //alert("url : " + link);
            $("[id*=dialog]").dialog({
                autoOpen: false,
                show: "fade",
                hide: "fade",
                modal: true,
                //open: function (ev, ui) {
                //    $('#myIframe').src = 'http://www.google.com';
                //},
                maxHeight: '400',
                width: '400',
                resizable: true
                //title: 'Title Topic'
            });
            //            $("#dialog1").dialog();

            $("#dialogMsg").dialog("open");
        }
        function closeMessagePage() {
            $('#dialogMsg').dialog('close');
            return false;

        }
        function openPayInSlip() {
            window.open('PayInSlip.aspx?tmpCd=' + document.getElementById("<%=txtTmpCd.ClientID %>").value  , 'payinslip', 'location=no,menu=no,height=530,width=800');
        }
        function copydata() {
            if (document.getElementById("<%=rdoOld.ClientID %>").checked == true) {
                document.getElementById("<%=txtFName.ClientID %>").value = "<%=dsUI.Tables[0].Rows[0]["rg_ins_fname"]%>";
                document.getElementById("<%=txtLName.ClientID %>").value = "<%=dsUI.Tables[0].Rows[0]["rg_ins_lname"]%>";
                document.getElementById("<%=txtAddress.ClientID %>").value = "<%=dsUI.Tables[0].Rows[0]["rg_ins_addr1"]%>";
                document.getElementById("<%=txtSubDistrict.ClientID %>").value = "<%=dsUI.Tables[0].Rows[0]["rg_ins_addr2"]%>";
                document.getElementById("<%=txtDistrict.ClientID %>").value = "<%=dsUI.Tables[0].Rows[0]["rg_ins_amphor"]%>";
                document.getElementById("<%=ddlProvince.ClientID %>").value = "<%=dsUI.Tables[0].Rows[0]["rg_ins_changwat"]%>";
                document.getElementById("<%=txtPostCode.ClientID %>").value = "<%=dsUI.Tables[0].Rows[0]["rg_ins_postcode"]%>";
                document.getElementById("<%=txtTelephone.ClientID %>").value = "<%=dsUI.Tables[0].Rows[0]["rg_ins_tel"]%>";
            }
        }
        function cleardata() {
            if (document.getElementById("<%=rdoChange.ClientID %>").checked == true) {
                document.getElementById("<%=txtFName.ClientID %>").value = "";
                document.getElementById("<%=txtLName.ClientID %>").value = "";
                document.getElementById("<%=txtAddress.ClientID %>").value = "";
                document.getElementById("<%=txtSubDistrict.ClientID %>").value = "";
                document.getElementById("<%=txtDistrict.ClientID %>").value = "";
                document.getElementById("<%=ddlProvince.ClientID %>").value = "";
                document.getElementById("<%=txtPostCode.ClientID %>").value = "";
                document.getElementById("<%=txtTelephone.ClientID %>").value = "";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div   id="page-online-insCar" >
        <h1 class="subject1">ซื้อประกัน-ประกันเดินทาง</h1>
        <div class="stepOnline">
            <img src="images/navi2step_s-2.jpg" />
        </div>
        <div class="prc-row"> 
           <h2 class="subject-detail">ยอดเบี้ยประกันที่ต้องชำระ</h2>
           <table   class="tb-online-insCar" >           
                <tr>
                    <td class="label"   >เบี้ยสุทธิ :</td>
                    <td>
                        <asp:Label ID="lblPremium" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="label" width="150">อากร :</td>
                    <td>
                        <asp:Label ID="lblStamp" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="label"   >ภาษี :</td>
                    <td  >
                        <asp:Label ID="lblVat" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="label"  >เบี้ยรวม :</td>
                    <td   >
                        <asp:Label ID="lblGrossPremium" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>   
            <h2 class="subject-detail">เลือกวิธีการชำระเงิน</h2>
            <table class="tb-online-insCar" >                      
                <tr>
                    <td> 
                        ท่านสามารถเลือกวิธีการชำระเบี้ยประกันภัยกับ บริษัท สินมั่นคงประกันภัย จำกัด (มหาชน) 
                        <br />
                        ด้วยวิธีใดวิธีหนึ่งจากตัวเลือกข้างล่างนี้
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td> 
                        <table>
                            <tr>
                                <td>
                                    <asp:RadioButton ID="rdoCheck" runat="server" GroupName="pay_type" Text="ชำระเงินด้วยเงินสดหรือเช็ค ที่บริษัทสินมั่นคงประกันภัยจำกัด (มหาชน) ได้ที่" />
                                    <asp:DropDownList ID="ddlBranch" runat="server" Width="350">
                                        <asp:ListItem Value="">--- โปรดระบุสาขา ---</asp:ListItem>
                                        <asp:ListItem VALUE="100">สำนักงานใหญ่ (ถนนศรีนครินทร์) โทร. 379-3140</asp:ListItem>
                                        <asp:ListItem Value="101">สาขาสวนมะลิ (ยสเส) โทร 2261646-54, 2250076</asp:ListItem>
                                        <asp:ListItem VALUE="102">สาขาดอนเมือง โทร. 5334053, 5334050, 5334059</asp:ListItem>
                                        <asp:ListItem VALUE="103">สาขาบางแค โทร. 4543959-66 FAX:5453967</asp:ListItem>
                                        <asp:ListItem VALUE="130">สาขารัตนาธิเบศร์ โทร. 9217582-9 FAX:9217581</asp:ListItem>
                                        <asp:ListItem VALUE="500">สาขาชลบุรี โทร. (038) 791672-9 FAX : (038) 270363</asp:ListItem>
                                        <asp:ListItem VALUE="201">สาขาพิษณุโลก โทร. (055) 216978-82 FAX : (055) 242239</asp:ListItem>
                                        <asp:ListItem VALUE="200">สาขาเชียงใหม่ โทร. (053) 409410-8 FAX : (055) 409419</asp:ListItem>
                                        <asp:ListItem VALUE="601">สาขาหาดใหญ่ โทร. (074) 343650-8 FAX : (074) 343657 
                                      (074) 343659</asp:ListItem>
                                        <asp:ListItem VALUE="600">สาขาสุราษฎร์ธานี โทร. (044) 261795-803 FAX : (044) 
                                      261804</asp:ListItem>
                                        <asp:ListItem VALUE="300">สาขานครปฐม โทร. (034) 284754-6 FAX : (034) 284758</asp:ListItem>
                                        <asp:ListItem VALUE="401">สาขาขอนแก่น โทร. (043) 324720-7 FAX : (043) 324728</asp:ListItem>
                                    </asp:DropDownList>
                                    <br />
                                    <div>ท่านสามารถชำระเงินด้วยบัตรเครดิต VISA , Master Card ได้ที่สำนักงานใหญ่</div>     
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton ID="rdoTransfer" runat="server" GroupName="pay_type" Text="ชำระเงิน ด้วยวิธีการโอนเข้าบัญชีของบริษัทสินมั่นคงประกันภัย จำกัด (มหาชน)" />
                                    <asp:Button ID="btnPrintDoc" runat="server" Text="พิมพ์เอกสาร" CssClass="btn" OnClientClick="openPayInSlip();return false;" />
                                    <table>
                                        <td width="30px"></td>
                                        <td>
                                            <ul>
                                                <li>
                                                    <IMG BORDER="0" SRC="./Images/paid.h4.gif" WIDTH="25" HEIGHT="23">
                                                    <div style="display:inline">ธนาคารกรุงเทพฯ จำกัด (มหาชน) สาขาถนนพัฒนาการ (Br. No. 198) บัญชีกระแสรายวัน เลขที่ 198-3-04200-1</div>
                                                </li>
                                                <li>
                                                    <IMG BORDER="0" SRC="./Images/paid.h5.gif" WIDTH="17" HEIGHT="16">
                                                    <div style="display:inline">ธนาคารไทยพาณิชย์ จำกัด (มหาชน) สาขาศรีนครินทร์ บัญชีกระแสรายวัน เลขที่ 080-3-00250-2 (Tr.code :3650)</div>
                                                </li>
                                                <li>
                                                    <IMG BORDER="0" SRC="./Images/bay2.gif" WIDTH="17" HEIGHT="16" alt=""> 
                                                    <div style="display:inline">ธนาคารกรุงศรีอยุธยา จำกัด (มหาชน) สาขาถนนศรีนครินทร์ กรุงเทพๆกรีฑา บัญชีกระแสรายวัน เลขที่ 307-0-00444-4</div>
                                                </li>
                                            </ul>
                                        </td>
                                    </table>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton ID="rdoSCB" runat="server" GroupName="pay_type" Text="ชำระเงิน ด้วยวิธีการโอนเข้าบัญชีของบริษัทสินมั่นคงประกันภัย จำกัด(มหาชน) ผ่าน" />                                    
                                    <asp:HyperLink ID="lnkSCB" runat="server" NavigateUrl="http://www.scbeasy.com" Target="_blank">www.scbeasy.com</asp:HyperLink>
                                    <div style="display:inline">(ต้องสมัครเป็นสมาชิกของ scbeasy ก่อน)</div>
                                    <table>
                                        <tr>
                                            <td width="30px"></td>
                                            <td>
                                                <ul>
                                                    <li>เลขที่บัญชีของ บมจ.สินมั่นคงประกันภัย(080-3-00250-2) ,ชื่อลูกค้า (เป็นภาษาอังกฤษ) และ CUST_NO (จากเลขที่อ้างอิง ที่จะได้รับในหน้าถัดไป)</li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>                                    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton ID="rdoDelivery" runat="server" GroupName="pay_type" Text="บริการจัดส่งและจัดเก็บเบี้ยประกันถึงบ้าน (เฉพาะภายในเขตกรุงเทพฯ)" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton ID="rdoCredit" runat="server" GroupName="pay_type" Text="ชำระเงิน ด้วยบัตรเครดิต" />
                                </td>
                            </tr>
                        </table>                
                     </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>  
            <h2 class="subject-detail">สถานที่จัดส่งเอกสาร (ยกเว้นการเลือกชำระด้วยเงินสด หรือเช็ค)</h2>
            <table   class="tb-online-insCar" >
                <tr>
                    <td  colspan=4 >
                        <asp:RadioButton ID="rdoOld" runat="server" GroupName="sendAt" text="ที่อยู่ตามกรมธรรม์" Checked="true" onclick="copydata();"/>
                        <asp:RadioButton ID="rdoChange" runat="server" GroupName="sendAt" text="อื่นๆ" onclick="cleardata();" />
                    </td>
                </tr>
                <tr>
                    <td class="label"  >
                        ชื่อ :
                    </td>
                    <td  >
                        <asp:TextBox ID="txtFName" runat="server"></asp:TextBox>
                        <span class="star">*</span>
                    </td>
                    <td class="label"  >
                        นามสกุล :
                    </td>
                    <td  >
                        <asp:TextBox ID="txtLName" runat="server"></asp:TextBox>
                        <span class="star">*</span>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        ที่อยู่ :
                    </td>
                    <td colspan="3" nowrap>
                        <asp:TextBox ID="txtAddress" runat="server" Width="250" MaxLength="50"></asp:TextBox>
                        <span class="star">*</span>(ใส่ บ้านเลขที่, หมู่, ถนน)  
                    </td>
                </tr>
                <tr>                    
                    <td class="label"  >
                        
                    </td>
                    <td  colspan="3">
                        <asp:TextBox ID="txtSubDistrict" runat="server" Width="250" MaxLength="50"></asp:TextBox>
                        <span class="star">*</span>(ใส่ ตำบล, แขวง)
                    </td>
                </tr>
                <tr>
                    <td class="label"  >
                        อำเภอ / เขต :
                    </td>
                    <td  >
                        <asp:TextBox ID="txtDistrict" runat="server" MaxLength="30"></asp:TextBox>
                        <span class="star">*</span>
                    </td> 
                    <td class="label">
                        จังหวัด :
                    </td>
                    <td  >
                        <asp:DropDownList ID="ddlProvince" runat="server">
                        </asp:DropDownList>
                        <span class="star">*</span>
                    </td>                               
                </tr>
                <tr>
                    <td class="label">
                        รหัสไปรษณีย์ :
                    </td>
                    <td  >
                        <asp:TextBox ID="txtPostCode" runat="server"></asp:TextBox>
                        <span class="star">*</span>
                    </td>                              
                    <td class="label">
                        เบอร์โทรศัพท์ :
                    </td>
                    <td>
                        <asp:TextBox ID="txtTelephone" runat="server"></asp:TextBox>                        
                    </td> 
                </tr>
            </table>

        <div style="margin:0px auto; width:190px;margin-top:15px;">       
            <asp:Button ID="btnSave" runat="server"  class="btn-default" style="margin-right:10px;" Text="ยืนยัน"  OnClientClick="return validateForm();" OnClick="btnSave_Click" />
            <asp:Button ID="btnBack" runat="server"  class="btn-default" Text="ย้อนกลับ"   OnClientClick="history.back();return false;"  />
        </div>
         
        <div id="dialogMsg" style="display:none" title="การตรวจสอบข้อมูล"   >
            <iframe id="MsgIframe"   frameborder="0" style="max-height:360" width="370" scrolling="auto"></iframe>
        </div>
        </div><!-- End class="prc-row"-->
        <div style="display:none">
            <asp:TextBox ID="txtTmpCd" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtPaid" runat="server"></asp:TextBox>
        </div>
    </div><!-- End  <div class="context page-app"> -->
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

