﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="buyInsure.aspx.cs" Inherits="onlineIns" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<script>
    function validateForm() {
        var isRet = true;
        var isRequire = true;
        var isLength = true;
        var isDate = true;
        var strMsg = "";
        var strMsgRequire = "";
        var strMsgLength = "";
        var strMsgDate = "";
        if (document.getElementById("<%=chkMotor.ClientID %>").checked == false &&
            document.getElementById("<%=chkSea.ClientID %>").checked == false &&
            document.getElementById("<%=chkFire.ClientID %>").checked == false &&
            document.getElementById("<%=chkPersonal.ClientID %>").checked == false &&
            document.getElementById("<%=chkHealth.ClientID %>").checked == false &&
            document.getElementById("<%=chkCancer.ClientID %>").checked == false &&
            document.getElementById("<%=chkOther.ClientID %>").checked == false) {
            strMsg += "    - การประกันภัยที่ท่านสนใจ\n";
            isRet = false;
            isRequire = false;
        }
        if (document.getElementById("<%=txtName.ClientID %>").value == "") {
            strMsg += "    - ชื่อ\n";
            isRet = false;
            isRequire = false;
        }
        if (document.getElementById("<%=txtTelephone.ClientID %>").value == "") {
            strMsg += "    - เบอร์โทรศัพท์\n";
            isRet = false;
            isRequire = false;
        }
        if (document.getElementById("<%=txtEmail.ClientID %>").value == "") {
            strMsg += "    - อีเมล์\n";
            isRet = false;
            isRequire = false;
        }
        if (document.getElementById("<%=txtRemark.ClientID %>").value == "") {
            strMsg += "    - ข้อมูลเพิ่มเติม\n";
            isRet = false;
            isRequire = false;
        }
        
        if (isRequire == false) {
            strMsg = "!กรุณาระบุ\n" + strMsg;
        }

        if (isLength == false) {
            strMsgLength = "!ขนาดข้อมูลที่ระบุไม่ถูกต้อง\n" + strMsg;
        }

        if (isRet == false) {
            //alert(strMsg + strMsgLength + strMsgDate);
            showMessagePage(strMsg + strMsgLength + strMsgDate);
        }
        else {
            document.getElementById("divLoading").style.display = "block";
            document.getElementById('divPremium').style.display = 'none';
        }
        return isRet;
    }
    function showMessagePage(strMsg) {
        document.getElementById("MsgIframe").setAttribute("src", "ShowMessageBox.aspx?msg=" + strMsg);
        //alert("url : " + link);
        $("[id*=dialog]").dialog({
            autoOpen: false,
            show: "fade",
            hide: "fade",
            modal: true,
            //open: function (ev, ui) {
            //    $('#myIframe').src = 'http://www.google.com';
            //},
            maxHeight: '400',
            width: '400',
            resizable: true
            //title: 'Title Topic'
        });
        //            $("#dialog1").dialog();

        $("#dialogMsg").dialog("open");
    }
    function closeMessagePage() {
        $('#dialogMsg').dialog('close');
        return false;

    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div id="page-online-insCar">
        <h1 class="subject1">ซื้อประกัน</h1>
        หากท่านประสงค์ให้บริษัทติดต่อกลับ
        โปรดกรอกรายละเอียดและหมายเลขโทรศัพท์(อนึ่งบริษัทฯจะติดต่อกลับท่านในวันและเวลาทำการ)         
 <br />
        <br />
        <table class="tb-buy-insure">
            <tr>
                <td class="label" valign="top" rowspan="4" nowrap>ท่านสนใจการประกันภัย :</br>(สามารถเลือกได้มากกว่า 1 ข้อ)</td>
                <td nowrap>
                    <asp:CheckBox ID="chkMotor" runat="server" Text="ประกันภัยรถยนต์"/>
                </td>
                <td nowrap>
                    <asp:CheckBox ID="chkSea" runat="server" Text="ประกันภัยทางทะเลและขนส่ง"/>
                </td>
            </tr>
            <tr>
                <td nowrap>
                    <asp:CheckBox ID="chkFire" runat="server" Text="ประกันอัคคีภัย"/>
                </td>
                <td nowrap>
                    <asp:CheckBox ID="chkPersonal" runat="server" Text="ประกันภัยอุบัติเหตุส่วนบุคคล และการเดินทาง"/>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox ID="chkHealth" runat="server" Text="ประกันภัยสุขภาพ"/>
                </td>
                <td>
                    <asp:CheckBox ID="chkCancer" runat="server" Text="ประกันภัยมะเร็ง"/>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox ID="chkOther" runat="server" Text="อื่นๆ"/>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td class="label">ชื่อ : 
                </td>
                <td>
                    <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td class="label">เบอร์โทรศัพท์ : 
                </td>
                <td>
                    <asp:TextBox ID="txtTelephone" runat="server"></asp:TextBox>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td class="label">อีเมล์ :</td>
                <td>
                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                    <span class="star">*</span>
                </td>
            </tr>
            <tr>
                <td class="label">เวลาสะดวกติดต่อกลับ :</td>
                <td>
                    <asp:DropDownList ID="ddlHour" runat="server" Width="40" style="margin-right:0px"></asp:DropDownList>
                    :
                    <asp:DropDownList ID="ddlMinute" runat="server" Width="40"></asp:DropDownList>                
                </td>
            </tr>            
            <tr>
                <td class="label" valign="top">ข้อมูลเพิ่มเติม :</td>
                <td colspan="3">
                    <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine"></asp:TextBox>
                    <span class="star">*</span>
                </td>
            </tr>

        </table>
        <div style="margin: 0px auto; width: 250px; margin-top: 15px;">
            <asp:Button ID="btnSave" runat="server" Text="ถัดไป >>" class="btn-default" Style="margin-right: 10px;" OnClick="btnSave_Click" OnClientClick="return validateForm();"/>
            <input id="Reset1" type="reset" class="btn-default " value="ยกเลิก" />
        </div>
        <div id="dialogMsg" style="display:none" title="การตรวจสอบข้อมูล"   >
            <iframe id="MsgIframe"   frameborder="0" style="max-height:360" width="370" scrolling="auto"></iframe>
        </div>
        

    </div>
    <!-- End   id="page-buy-insure" -->
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

