﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Module_Paid_PayCreditCard : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string strRegisNo = "";
        if (!IsPostBack)
        {
            strRegisNo = Request.QueryString["RegisNo"].ToString();
            if (strRegisNo != "")
            {
                //mid.Value = Session["mid" + strRegisNo].ToString();
                //terminal.Value = Session["terminal" + strRegisNo].ToString();
                //version.Value = Session["version" + strRegisNo].ToString();
                //command.Value = Session["command" + strRegisNo].ToString();
                //settlement_flag.Value = Session["settlement_flag" + strRegisNo].ToString();
                //ref_no.Value = Session["ref_no" + strRegisNo].ToString();
                //ref_date.Value = Session["ref_date" + strRegisNo].ToString();
                //service_id.Value = Session["service_id" + strRegisNo].ToString();
                //cur_abbr.Value = Session["cur_abbr" + strRegisNo].ToString();
                //cust_id.Value = Session["cust_id" + strRegisNo].ToString();
                //amount.Value = Session["amount" + strRegisNo].ToString();
                //cust_lname.Value = Session["cust_lname" + strRegisNo].ToString();
                //cust_email.Value = Session["cust_email" + strRegisNo].ToString();
                //cust_fname.Value = Session["cust_fname" + strRegisNo].ToString();
                //cust_country.Value = Session["cust_country" + strRegisNo].ToString();
                //cust_address1.Value = Session["cust_address1" + strRegisNo].ToString();
                //cust_city.Value = Session["cust_city" + strRegisNo].ToString();
                //cust_address2.Value = Session["cust_address2" + strRegisNo].ToString();
                //cust_province.Value = Session["cust_province" + strRegisNo].ToString();
                //cust_zip.Value = Session["cust_zip" + strRegisNo].ToString();
                //cust_phone.Value = Session["cust_phone" + strRegisNo].ToString();
                //backURL.Value = System.Configuration.ConfigurationManager.AppSettings["appUrl"].ToString() + "?regisNo=" + strRegisNo + "";
                //description.Value = Session["description" + strRegisNo].ToString();

                string pv_amount = Session["amount" + strRegisNo].ToString();
                pv_amount = pv_amount + ".00";
                string[] fmtnum1 = pv_amount.Split('.');
                int int_num1 = Convert.ToInt32(fmtnum1[0].ToString());
                string stnum1 = String.Format("{0:D10}", int_num1);
                string stnum2 = fmtnum1[1].ToString().Trim();
                if (stnum2.Length == 1)
                    stnum2 = stnum2 + "0";
                else
                    stnum2 = stnum2.Substring(0, 2);
                pv_amount = stnum1 + stnum2;

                string pv_refno = Session["ref_no" + strRegisNo].ToString(); // ex 90-02994
                pv_refno = "00000"+pv_refno.Replace("-", "");
                MERCHANT2.Value = "401001666477001";
                TERM2.Value = "74400101";
                AMOUNT2.Value = pv_amount; 
                URL2.Value = System.Configuration.ConfigurationManager.AppSettings["appUrl"].ToString() + "?regisNo=" + strRegisNo + "";
                RESPURL.Value = "";
                IPCUST2.Value  = "";
                //DETAIL2.Value = Session["description" + strRegisNo].ToString();
                DETAIL2.Value = "SMK Insurance";
                INVMERCHANT.Value =pv_refno;
            }
        }
    }
}
