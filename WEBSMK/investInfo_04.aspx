﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="investInfo_04.aspx.cs" Inherits="investInfo_04" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

 <div class="subject1">จุดเด่นในรอบปี</div >  
 
<table  class="tb-border-bottom"  >
                <tr>
                  <td   class="no-border" ></td>
                  <td   class="no-border"  >&nbsp;</td>
                  <td   class="no-border"  >&nbsp;</td>
                  <td   class="no-border"  >(หน่วย : บาท)</td>
                  </tr>
                <tr>
                  <th class="align-left">ผลประกอบการรอบปี</th>
                  <th width="17%">2555</th> 
                  <th width="17%" >2554</th>
                  <th width="17%">2553</th>
                </tr>
                <tr>
                  <td colspan="4" class="align-left  hilight1" >ผลการรับประกันภัย</td>
                </tr>
                <tr>
                  <td class="align-left " >เบี้ยประกันภัยรับรวม</td>
                  <td class="align-right"  >8,072,298</td>
                  <td class="align-right"  >6,927,634</td>
                  <td class="align-right"  >6,145,094</td>
  </tr>
                <tr>
                  <td class="align-left " >เบี้ยประกันภัยรับสุทธิ</td>
                  <td class="align-right"  >7,774,831</td>
                  <td class="align-right"  >6,658,884</td>
                  <td class="align-right"  >5,891,350</td>
  </tr>
                <tr>
                  <td class="align-left" >ค่าสินไหมทดแทน</td>
                  <td class="align-right"  >N/A</td>
                  <td class="align-right"  >N/A</td>
                  <td class="align-right"  >N/A</td>
                </tr>
                <tr>
                  <td class="align-left" >ค่าสินไหมทดแทนและค่าใช้จ่ายในการจัดการค่าสินไหมทดแทน</td>
                  <td class="align-right"  >4,321,614</td>
                  <td class="align-right"  >3,685,697</td>
                  <td class="align-right"  >3,136,996</td>
                </tr>
                <tr>
                  <td class="align-left" >กำไรจากการรับประกันภัย</td>
                  <td class="align-right"  >401,868</td>
                  <td class="align-right"  >495,755</td>
                  <td class="align-right"  >368,100</td>
                </tr>
                <tr class="no-border align-left">
                  <td  class="no-border align-left" >กำไรจากการลงทุน</td>
                  <td  class="no-border align-right"  >428,508</td>
                  <td  class="no-border align-right"  >307,302</td>
                  <td  class="no-border align-right" >239,698</td>
                </tr>
                <tr>
                  <td colspan="4" class="align-left   hilight1" >ฐานะทางการเงิน</td>
                </tr>
                <tr>
                  <td class="align-left" >สินทรัพย์รวม</td>
                  <td class="align-right"  >11,348,905</td>
                  <td class="align-right"  >10,013,894</td>
                  <td class="align-right"  >8,886,148</td>
                </tr>
                <tr>
                  <td class="align-left" >หนี้สินรวม</td>
                  <td class="align-right"  >8,450,139</td>
                  <td class="align-right"  >7,778,960</td>
                  <td class="align-right"  >6,871,462</td>
                </tr>
                <tr>
                  <td class="align-left" >ส่วนของผู้ถือหุ้น</td>
                  <td class="align-right"  >2,898,765</td>
                  <td class="align-right"  >2,234,934</td>
                  <td class="align-right"  >2,014.686</td>
                </tr>
                <tr>
                  <td  class="no-border align-left" >มูลค่าตามบัญชีต่อหุ้น(บาท)</td>
                  <td  class="no-border align-right"  >144.94</td>
                  <td   class="no-border align-right"  >111.75</td>
                  <td  class="no-border align-right" >100.73</td>
                </tr>
                <tr>
                  <td colspan="4" class="align-left  hilight1 " >กำไร - ขาดทุน</td>
                </tr>
                <tr>
                  <td class="align-left" >กำไร (ขาดทุน) สุทธิ</td>
                  <td class="align-right"  >627,072</td>
                  <td class="align-right"  >455,753</td>
                  <td class="align-right"  >481,604</td>
                </tr>
                <tr>
                  <td class="align-left" >กำไร (ขาดทุน) สุทธิต่อหุ้น (บาท)</td>
                  <td class="align-right"  >31.35</td>
                  <td class="align-right"  >22.79</td>
                  <td class="align-right"  >24.08</td>
                </tr>
                <tr>
                  <td  class="no-border align-left"  >เงินปันผลต่อหุ้น (บาท)</td>
                  <td  class="no-border align-right"  >11.00*</td>
                  <td  class="no-border align-right"  >9.00</td>
                  <td  class="no-border align-right"  >8.00</td>
                </tr>
                
                <tr>
                  <td colspan="4" class="align-left  hilight1" >อัตราส่วนทางการเงินที่สำคัญ</td>
                </tr>
                <tr>
                  <td class="align-left" >อัตราส่วนสภาพคล่อง (เท่า)</td>
                  <td class="align-right"  >1.25</td>
                  <td class="align-right"  >1.20</td>
                  <td class="align-right"  >1.19</td>
                </tr>
                <tr>
                  <td class="align-left" >อัตราหมุนเวียนเบี้ยประกันค้างรับ (วัน)</td>
                  <td class="align-right"  >33.97</td>
                  <td class="align-right"  >36.63</td>
                  <td class="align-right"  >39.19</td>
                </tr>
                <tr>
                  <td class="align-left" >อัตราการจ่ายค่าสินไหมทดแทน (%)</td>
                  <td class="align-right"  >60.42</td>
                  <td class="align-right"  >58.49</td>
                  <td class="align-right"  >54.26</td>
                </tr>
                <tr>
                  <td class="align-left" >อัตรากำไรขั้นต้น (%)</td>
                  <td class="align-right"  >5.17</td>
                  <td class="align-right"  >7.45</td>
                  <td class="align-right"  >21.21</td>
                </tr>
                <tr>
                  <td class="align-left" >อัตราส่วนค่าใช้จ่ายในการรับประกัน (%)</td>
                  <td class="align-right"  >35.21</td>
                  <td class="align-right"  >35.19</td>
                  <td class="align-right"  >39.51</td>
                </tr>
                <tr>
                  <td class="align-left" >อัตราผลตอบแทนจากการลงทุน (%)</td>
                  <td class="align-right"  >4.59</td>
                  <td class="align-right"  >3.77</td>
                  <td class="align-right"  >3.27</td>
                </tr>
                <tr>
                  <td class="align-left" >อัตรากำไรสุทธิ (%)</td>
                  <td class="align-right"  >8.11</td>
                  <td class="align-right"  >6.78</td>
                  <td class="align-right"  >7.54</td>
                </tr>
                <tr>
                  <td class="align-left" >อัตราผลตอบแทนผู้ถือหุ้น (%)</td>
                  <td class="align-right"  >24.43</td>
                  <td class="align-right"  >21.45</td>
                  <td class="align-right"  >26.60</td>
                </tr>
                <tr>
                  <td class="align-left" >อัตราผลตอบแทนต่อสินทรัพย์ (%)</td>
                  <td class="align-right"  >5.87</td>
                  <td class="align-right"  >4.77</td>
                  <td class="align-right"  >5.93</td>
                </tr>
                <tr>
                  <td class=" align-left" >อัตราส่วนหนี้สินต่อส่วนของผู้ถือหุ้น (เท่า)</td>
                  <td class=" align-right"  >2.92</td>
                  <td class=" align-right"  >3.48</td>
                  <td class=" align-right"  >3.41</td>
                </tr>
              </table>
              
              



ข้อมูลทางการเงินของปี 2554 ได้จัดทำขึ้นตามประกาศคณะกรรมการกำกับและส่งเสริมการประกอบธุรกิจประกันภัย ลงวันที่ 
27 พฤษภาคม 2553 และ ได้จัดประเภทรายการใหม่สำหรับงบการเงินปี 2553 เพื่อให้เป็นไปตามรูปแบบของข้อกำหนดดังกล่าว 
โดยไม่มีผลกระทบต่อส่วนของเจ้าของ ส่วนข้อมูลทางการเงิน ของปี 2552 บริษัทฯได้จัดทำขึ้นเพื่อให้เป็นไปตามข้อกำหนด
ในคำสั่งนายทะเบียนลงวันที่ 6 มีนาคม 2545 ออกตามความในพระราชบัญญัติการบัญชี พ.ศ. 2543 ดังนั้นข้อมูลบางรายการ 
จึงไม่อาจเปรียบเทียบกันได้
<br /><br />
 <div class="notation" >* อยู่ระหว่างการขออนุมัติจากที่ประชุมสามัญผู้ถือหุ้นประจำปี 2556</div>



</asp:Content>

