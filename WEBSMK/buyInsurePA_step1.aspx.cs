﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class buyInsurePA_step1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetDataSource();            
        }
    }
    protected void dtgDataBind(object sender, DataGridItemEventArgs e)
    {
        HyperLink lnkTmp;
        Label lblTmp;
        double dSumIns;
        double dTotalPrmm;
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (e.Item.Cells[4].Text == "9999")
            {
                e.Item.Cells[0].Visible = false;
                e.Item.Cells[1].ColumnSpan = 3;
                if (e.Item.ItemIndex > 1)
                {
                    e.Item.Cells[1].Text = "&nbsp;</td></tr>";
                    e.Item.Cells[1].Text += "<tr><td colspan=\"3\">";
                }                               
                e.Item.Cells[1].Text += "<h2 class=\"subject-detail\">" + e.Item.Cells[6].Text + "</h2>";
                e.Item.Cells[1].Text += "</td></tr>";
                e.Item.Cells[1].Text += "<tr><td colspan=\"3\">" + e.Item.Cells[5].Text;
                e.Item.Cells[1].Text += "</td></tr>";
                e.Item.Cells[1].Text += "<tr><td class=\"tb-buy-Info-head\">" + "แผน" + "</td><td class=\"tb-buy-Info-head\">" + "ทุนประกัน(บาท)" + "</td><td class=\"tb-buy-Info-head\">" + "อัตราค่าเบี้ยประกันภัย(บาท)";
                //e.Item.Cells[1].Text += " <tr><td height=\"3\" colspan=\"3\"><img src=\"../../Theme/images/spacer.gif\" width=\"1\" height=\"3\" />";
                e.Item.Cells[1].HorizontalAlign = HorizontalAlign.Left;
                e.Item.Cells[1].CssClass = "";
                for (int i = 2; i <= 2; i++)
                {
                    e.Item.Cells[i].Visible = false;
                }
            }
            else
            {
                lnkTmp = (HyperLink)e.Item.Cells[0].FindControl("lnkPromotion");
                if (lnkTmp != null)
                {
                    lnkTmp.NavigateUrl = "./buyInsurePA_step2.aspx?pm_major_cd=" + e.Item.Cells[3].Text + "&pm_minor_cd=" + e.Item.Cells[4].Text;
                    dSumIns = 0;
                    if (e.Item.Cells[7].Text == "Y")
                        dSumIns += Convert.ToDouble(e.Item.Cells[8].Text); // pa1
                    if (e.Item.Cells[9].Text == "Y")
                        dSumIns += Convert.ToDouble(e.Item.Cells[10].Text); // pa2
                    if (e.Item.Cells[11].Text == "Y")
                        dSumIns += Convert.ToDouble(e.Item.Cells[12].Text); // ttd
                    if (e.Item.Cells[13].Text == "Y")
                        dSumIns += Convert.ToDouble(e.Item.Cells[14].Text); // ptd
                    if (e.Item.Cells[15].Text == "Y")
                        dSumIns += Convert.ToDouble(e.Item.Cells[16].Text); // med
                    e.Item.Cells[1].Text = dSumIns.ToString("#,###,###,##0.00");
                    lblTmp = (Label)e.Item.FindControl("lblPrmm");
                    if (lblTmp != null)
                        dTotalPrmm = Convert.ToDouble(lblTmp.Text);
                    else
                        dTotalPrmm = 0;
                    dTotalPrmm += Convert.ToDouble(e.Item.Cells[17].Text);
                    dTotalPrmm += Convert.ToDouble(e.Item.Cells[18].Text);
                    e.Item.Cells[2].Text = dTotalPrmm.ToString("#,###,###,##0.00");
                }
            }
        }
    }

    public void SetDataSource()
    {
        DataSet dsGrid;
        DataSet dsPromotion;
        DataSet dsOthPromotion;
        DataRow[] drs;
        DataRow[] drs2;
        NonMotorManager cmNonMotor = new NonMotorManager();
        dsPromotion = cmNonMotor.getPAPromotion();
        dsOthPromotion = cmNonMotor.getOthPAPromotion();
        // sort data before bind
        drs = dsPromotion.Tables[0].Select("pm_minor_cd = '9999'", "pm_major_cd");
        dsGrid = dsPromotion.Clone();
        for (int i = 0; i < drs.Length; i++)
        {
            dsGrid.Tables[0].ImportRow(drs[i]);
            drs2 = dsPromotion.Tables[0].Select("pm_minor_cd <> '9999' AND pm_major_cd = '" + drs[i]["pm_major_cd"].ToString() + "'", "pm_minor_cd");
            for (int j = 0; j < drs2.Length; j++)
            {
                dsGrid.Tables[0].ImportRow(drs2[j]);
            }
        }
        dtgData.DataSource = dsGrid;
        dtgData.DataBind();

        // dtgOth
        dtgDataOth.DataSource = dsOthPromotion;
        dtgDataOth.DataBind();
    }
    public string GetUrl(object strPmMajorCd, object strPmMinorCd)
    {
        string strRet;
        strRet = "./buyInsurePA_step2.aspx?pm_major_cd=" + strPmMajorCd.ToString() + "&pm_minor_cd=" + strPmMinorCd.ToString();
        return strRet;
    }
}
