﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ShowMessageBox : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string strMessage = "";
        if (Request.QueryString["msg"] != null)
        {
            strMessage = Request.QueryString["msg"];
            lblMessage.Text = strMessage.Replace("-", "</br>&nbsp;&nbsp;&nbsp;-");
        }
        else
            litScript.Text = "<script>window.parent.closeMessagePage();</script>";
    }
}
