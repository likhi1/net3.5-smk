﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="buyInsureTravel.aspx.cs" Inherits="onlineInsTravel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div id="page-buy-insure">
         <h1 class="subject1">ซื้อประกัน-ประกันเดินทาง  </h1>
          <div class="stepOnline"> <img src="images/step-buy-insure1.jpg" /></div>
 
    <h2 class="subject-detail">รายละเอียดผู้เอาประกัน  </h2>

<div >กรณีมีผู้เอาประกันมากกว่า 1 คน กรุณาแฟกซ์ชื่อผู้เอาประกันไปที่ 02-3772097 หรือ Email ไปที่ reg_misc@smk.co.th</div>
       
    
<table class="tb-buy-insure ">
  <tr>
    <td width="90"  class="label" > จำนวนบุคคล :</td>
    <td width="100" >
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox><span class="star">* </span>คน
    
      </td>

        <td class="label" width="130"  >วันที่เริ่มเดินทาง :</td>
    <td width="180"  >
        <asp:TextBox ID="TextBox10" runat="server"></asp:TextBox><span class="star">* </span>วัน
      </td>

    
  </tr>
  <tr>
    <td class="label">ชื่อ :</td>
    <td>
        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox><span class="star">*</span>
      </td>
       <td class="label">จำนวนวันเดินทาง :</td>
    <td>
        <asp:DropDownList ID="DropDownList5" runat="server" >
        </asp:DropDownList> 
      </td>
   
  </tr>
  <tr>
    <td class="label">นามสกุล :</td>
    <td>
        <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox><span class="star">*</span>
      </td>
       <td class="label">อีเมล์ :</td>
    <td>
        <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox><span class="star">*</span>
      </td>
      


  
  </tr>
  <tr>
      <td width="114" class="label">จังหวัด :</td>
    <td width="252">
        <asp:DropDownList ID="DropDownList1" runat="server">
        </asp:DropDownList>
      </td>
       <td class="label">เบอร์โทรศัพท์ :</td>
    <td>
        <asp:TextBox ID="TextBox9" runat="server"></asp:TextBox><span class="star">*</span>
      </td>
     
  </tr>
  <tr>
    <td class="label">อำเภอ/เขต :</td>
    <td>
        <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox><span class="star">*</span>
      </td>
    <td class="label">เวลาให้ติดต่อกลับ :</td>
    <td>
        <asp:DropDownList ID="DropDownList3" runat="server" Width="30px">
        </asp:DropDownList> : 
         <asp:DropDownList ID="DropDownList4" runat="server" Width="30px">
        </asp:DropDownList> น.
      </td>
  </tr>
  <tr>
    <td class="label">รหัสไปรษณีย์ :</td>
    <td>
        <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
      </td>
    <td class="label">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
      <td class="label">ที่อยู่ :</td>
    <td colspan="3">
        <asp:TextBox ID="TextBox7" runat="server" TextMode="MultiLine"></asp:TextBox><span class="star">* </span>ใส่ บ้านเลขที่, ซอย, หมู่, หมู่บ้าน, ถนน
      </td>

   
  </tr>


  <tr>
      <td class="label">&nbsp;</td>
    <td colspan="3">
<asp:TextBox ID="TextBox4" runat="server" TextMode="MultiLine"></asp:TextBox><span class="star">* </span>ใส่ ตำบล, แขวง

        
      </td>
   
  </tr>
  </table>


 <h2 class="subject-detail">รายละเอียดผู้รับประโยชน์  </h2>
<div >ไม่ต้องระบุ ในกรณีมีผู้รับประโยชน์มากกว่า 1 คน กรุณาแฟกซ์ชื่อผู้รับประโยชน์ไปที่ 02-3772097 <br /> หรือ Email ไปที่ reg_misc@smk.co.th </div>
 
   

        
<table class="tb-buy-insure ">
  <tr>
    <td class="label" width="90" >ชื่อ :</td>
    <td width="100" >
        <asp:TextBox ID="TextBox13" runat="server"></asp:TextBox><span class="star">*</span>
      </td>
       <td class="label"  width="130">จังหวัด :</td>
    <td  width="180">
        <asp:DropDownList ID="DropDownList11" runat="server">
        </asp:DropDownList>
       

    </td>
   
  </tr>
  <tr>
    <td class="label">นามสกุล :</td>
    <td>
        <asp:TextBox ID="TextBox14" runat="server"></asp:TextBox><span class="star">*</span>
      </td>
    <td class="label">อำเภอ/เขต :</td>
    <td>
        <asp:TextBox ID="TextBox24" runat="server" ></asp:TextBox> 
      </td>
     

  
  </tr>
  <tr>
      <td width="114" class="label">ความสัมพันธ์	 :</td>
    <td width="252">
        <asp:DropDownList ID="DropDownList9" runat="server">
        </asp:DropDownList><span class="star">*</span>
      </td>
    <td class="label">รหัสไปรษณีย์ :</td>
    <td>
        <asp:TextBox ID="TextBox25" runat="server"></asp:TextBox>
      </td>
     
  </tr>
  <tr>
      <td width="114" class="label">เบอร์โทรศัพท์ :</td>
    <td width="252">
        <asp:DropDownList ID="DropDownList6" runat="server">
        </asp:DropDownList>
      </td>
    <td class="label">อีเมล์ :</td>
    <td width="252">
        <asp:TextBox ID="TextBox26" runat="server"></asp:TextBox>
      </td>
     
  </tr>
  <tr>
    <td class="label">ที่อยู่ :</td>
    <td colspan="3">
        <asp:RadioButtonList ID="RadioButtonList1" runat="server">
            <asp:ListItem>ที่อยู่เดียวกับผู้เอาประกัน</asp:ListItem>
            <asp:ListItem>ที่อยู่อื่นๆ</asp:ListItem>
        </asp:RadioButtonList>
      </td>
  </tr>
  <tr>
      <td class="label">&nbsp;</td>
    <td colspan="3">
        <asp:TextBox ID="TextBox19" runat="server" TextMode="MultiLine"></asp:TextBox> ใส่ บ้านเลขที่, ซอย, หมู่, หมู่บ้าน, ถนน
      </td>

   
  </tr>


  <tr>
      <td class="label">&nbsp;</td>
    <td colspan="3">
<asp:TextBox ID="TextBox20" runat="server" TextMode="MultiLine"></asp:TextBox> ใส่ ตำบล, แขวง

        
      </td>
   
  </tr>
  </table>

     
         <h2 class="subject-detail">รายละเอียดความคุ้มครอง </h2>

        
<table class="tb-buy-insure">
  <tr>
    <td class="label" width="180" >การเสียชีวิต การสูญเสียอวัยวะ	 :</td>
    <td width="100" >
        <asp:DropDownList ID="DropDownList12" runat="server">
        </asp:DropDownList> บาท
        
      </td>
    <td class="label">ค่ารักษาพยาบาล :</td>
    <td  width="180">
  <asp:DropDownList ID="DropDownList2" runat="server">
        </asp:DropDownList><span class="star">*</span>  บาท
    </td>
   
  </tr>
  </table>

        <br />

         <div class="notation" style="text-align:center;">       
บริษัทสินมั่นคงประกันภัย จำกัด (มหาชน) <br />ขอสงวนสิทธิในการให้ความคุ้มครองและเปลี่ยนแปลงเบี้ยประกันภัย ในกรณีข้อมูลของท่านไม่ตรงกับความเป็นจริง
     </div>



</div><!-- End id="page-buy-insure"-->

</asp:Content>

