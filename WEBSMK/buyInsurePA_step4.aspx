﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="buyInsurePA_step4.aspx.cs" Inherits="buyInsurePA_step4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div id="page-online-insCar">
        <h1 class="subject1">ทำประกันออนไลน์ </h1>
        การทำรายการประกันภัยของท่านเรียบร้อยแล้ว หากท่านมีข้อสงสัยเพิ่มเติม สามารถติดต่อสอบถามได้ที่ 02-378-7167<br />
        <br />        
        <div class="prc-row">    
        <h2>
        เลขที่อ้างอิง : <asp:Label ID="lblRefNo" runat="server" Text=""></asp:Label>
        </h2>    
        <h2 class="subject-detail">รายละเอียดผู้เอาประกัน</h2>
        <table width="630px">
            <tr>
                <td valign="top">
                    <table class="tb-online-insCar">              
                        <tr>
                            <td class="label" >
                                ชื่อ :
                            </td>
                            <td >
                                <asp:Label ID="lblFName" runat="server" Text=""></asp:Label>
                            </td>    
                            <td class="label">
                                นามสกุล :
                            </td>
                            <td>
                                <asp:Label ID="lblLName" runat="server" Text=""></asp:Label>
                            </td>                            
                        </tr>
                        <tr>
                            <td class="label">
                                ที่อยู่ :
                            </td>
                            <td colspan="3">
                                <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>                    
                            <td class="label"  >
                                ตำบล / แขวง :
                            </td>
                            <td colspan="3">
                                <asp:Label ID="lblSubDistrict" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" >
                                อำเภอ / เขต :
                            </td>
                            <td width="150px">
                                <asp:Label ID="lblDistrict" runat="server" Text=""></asp:Label>
                            </td>      
                            <td class="label">
                                จังหวัด :
                            </td>
                            <td  >
                                <asp:Label ID="lblProvince" runat="server" Text=""></asp:Label>
                            </td>                          
                        </tr>
                        <tr>
                            <td class="label">
                                รหัสไปรษณีย์ :
                            </td>
                            <td width="130px">
                                <asp:Label ID="lblPostCode" runat="server" Text=""></asp:Label>
                            </td>                          
                        </tr>
                        <tr>
                            <td class="label">
                                เบอร์โทรศัพท์มือถือ :
                            </td>
                            <td>
                                <asp:Label ID="lblMobile" runat="server" Text=""></asp:Label>
                            </td>  
                            <td class="label">
                                เบอร์โทรศัพท์บ้าน :
                            </td>
                            <td>
                                <asp:Label ID="lblTelephone" runat="server" Text=""></asp:Label>
                            </td>                                                  
                        </tr>
                        <tr>
                            <td class="label">
                                อีเมล์ :
                            </td>
                            <td>
                                <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label>
                            </td>                    
                        </tr>    
                        <tr>
                            <td class="label">
                                ประเภทบัตร :
                            </td>
                            <td>
                                <asp:Label ID="lblCardType" runat="server" Text=""></asp:Label>
                            </td>  
                            <td class="label">
                                เลขที่บัตร :
                            </td>
                            <td>
                                <asp:Label ID="lblCardNo" runat="server" Text=""></asp:Label>
                            </td>                                                  
                        </tr>
                        <tr>
                            <td class="label">
                                อาชีพ :
                            </td>
                            <td>
                                <asp:Label ID="lblOccupy" runat="server" Text=""></asp:Label>
                            </td>  
                            <td class="label">
                                วันเกิด :
                            </td>
                            <td>
                                <asp:Label ID="lblBirthDate" runat="server" Text=""></asp:Label>
                            </td>                                                  
                        </tr>
                        <tr>
                            <td class="label">
                                วันที่เริ่มคุ้มครอง :
                            </td>
                            <td>
                                <asp:Label ID="lblStartDate" runat="server" Text=""></asp:Label>
                            </td>  
                            <td class="label">
                                วันที่สิ้นสุด :
                            </td>
                            <td>
                                <asp:Label ID="lblStopDate" runat="server" Text=""></asp:Label>
                            </td>                                                  
                        </tr>                    
                    </table>
                </td>
            </tr>            
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        
        <h2 class="subject-detail">
            รายละเอียดผู้รับประโยชน์  
        </h2>            
        <table class="tb-online-insCar">              
            <tr>
                <td class="label" >
                    ชื่อ :
                </td>
                <td >
                    <asp:Label ID="lblBefFName" runat="server" Text=""></asp:Label>
                </td>    
                <td class="label">
                    นามสกุล :
                </td>
                <td>
                    <asp:Label ID="lblBefLName" runat="server" Text=""></asp:Label>
                </td>                            
            </tr>
            <tr>
                <td class="label">ความสัมพันธ์	 :</td>
                <td>
                    <asp:Label ID="lblRelationship" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="label" valign="top">ที่อยู่ :</td>
                <td colspan="3">
                    <asp:Label ID="lblBefAddress1" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>                    
                <td class="label"  >
                    ตำบล / แขวง :
                </td>
                <td colspan="3">
                    <asp:Label ID="lblBefAddress2" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="label" >
                    อำเภอ / เขต :
                </td>
                <td width="150px">
                    <asp:Label ID="lblBefDistrict" runat="server" Text=""></asp:Label>
                </td>      
                <td class="label">
                    จังหวัด :
                </td>
                <td  >
                    <asp:Label ID="lblBefProvince" runat="server" Text=""></asp:Label>
                </td>                          
            </tr>
            <tr>
                <td class="label">
                    รหัสไปรษณีย์ :
                </td>
                <td width="130px">
                    <asp:Label ID="lblBefPostCode" runat="server" Text=""></asp:Label>
                </td>      
                <td class="label">
                    เบอร์โทรศัพท์ :
                </td>
                <td>
                    <asp:Label ID="lblBefTelephone" runat="server" Text=""></asp:Label>
                </td>                                                  
            </tr>
            <tr>
                <td class="label">
                    อีเมล์ :
                </td>
                <td>
                    <asp:Label ID="lblBefEmail" runat="server" Text=""></asp:Label>
                </td>                    
            </tr> 
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>    
        <h2 class="subject-detail">
            รายละเอียดความคุ้มครองหลัก
        </h2>    
        <table class="tb-online-insCar">
            <tr>
                <td align="center">
                    ความคุ้มครอง
                </td>
                <td align="center">
                    จำนวนเงินเอาประกัน
                </td>
            </tr>
            <tr>
                <td>
                    การเสียชีวิต สูญเสียอวัยวะ สายตาหรือ ทุพพลภาพถาวรสิ้นเชิง
                </td>
                <td class="label">
                    <asp:Label ID="lblPa1Si" runat="server" Text="100,000.00" CssClass="Data"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    การเสียชีวิต สูญเสียอวัยวะ สายตา การรับฟัง การพูดออกเสียง หรือทุพพลภาพถาวรสิ้นเชิง
                </td>
                <td class="label">
                    <asp:Label ID="lblPa2Si" runat="server" Text="100,000.00" CssClass="Data"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="ทุพพลภาพชั่วคราวสิ้นเชิงและไม่เกิน"></asp:Label>
                    <asp:Label ID="lblTtdWeek" runat="server" Text="3" CssClass="Data"></asp:Label>
                    <asp:Label ID="Label7" runat="server" Text="สัปดาห์"></asp:Label>
                </td>
                <td class="label">
                    <asp:Label ID="lblTtdSi" runat="server" Text="100,000.00" CssClass="Data"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label8" runat="server" Text="ทุพพลภาพชั่วคราวบางส่วนและไม่เกิน"></asp:Label>
                    <asp:Label ID="lblPtdWeek" runat="server" Text="4" CssClass="Data"></asp:Label>
                    <asp:Label ID="Label9" runat="server" Text="สัปดาห์"></asp:Label>
                </td>
                <td class="label">
                    <asp:Label ID="lblPtdSi" runat="server" Text="100,000.00" CssClass="Data"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label10" runat="server" Text="ค่ารักษาพยาบาล"></asp:Label>
                </td>
                <td class="label">
                    <asp:Label ID="lblMedicine" runat="server" Text="10,000.00" CssClass="Data"></asp:Label>
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>
        </table>
        <h2 class="subject-detail">
            รายละเอียดความคุ้มครองเพิ่ม
        </h2>    
        <table class="tb-online-insCar">
            <tr>
                <td>
                <asp:Label ID="lblWar" runat="server" Text="การสงคราม"></asp:Label>
                <asp:Label ID="lblStr" runat="server" Text="การนัดหยุดงาน การจลาจล และการที่ประชนก่อความวุนวายถึงขนาดลุกฮือต่อต้านรัฐบาล"></asp:Label>
                <asp:Label ID="lblSpt" runat="server" Text="การเล่นหรือการแข่งขันกีฬาอันตราย"></asp:Label>
                <asp:Label ID="lblMtr" runat="server" Text="การขับขี่หรือโดยสารรถจักรยานยนต์(กรณีที่ท่านมีการขับขี่หรือโดยสารรถจักรยานยนต์)"></asp:Label>
                <asp:Label ID="lblAir" runat="server" Text="การโดยสารในฐานะผู้โดยสารอากาศยานที่มิได้ประกอบการโดยสารการบินพาณิชย์"></asp:Label>
                <asp:Label ID="lblMurder" runat="server" Text="คุ้มครองการถูกฆ่าหรือถูกทำร้ายร่างกาย"></asp:Label>
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>
        </table>
        </div>                
        <h2 class="subject-detail">รายละเอียดการชำระเงินและการจัดส่งเอกสาร</h2>
        <table width="630px">
            <tr>
                <td valign="top">
                    <table class="tb-online-insCar">                        
                        <tr>
                            <td class="label"   >เบี้ยสุทธิ :</td>
                            <td>
                                <asp:Label ID="lblPremium" runat="server" Text=""></asp:Label>
                            </td>
                            <td class="label">อากร :</td>
                            <td>
                                <asp:Label ID="lblStamp" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="label"   >ภาษี :</td>
                            <td  >
                                <asp:Label ID="lblVat" runat="server" Text=""></asp:Label>
                            </td>
                            <td class="label">เบี้ยรวม :</td>
                            <td>
                                <asp:Label ID="lblGrossPremium" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>   
                        <tr>
                            <td colspan="4">
                                <table>
                                    <tr>
                                        <td class="label" valign="top">วิธีการชำระเบี้ยประกันภัย :</td>
                                        <td colspan="4">
                                            <asp:Label ID="lblPaidMethod" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label"><div style="display:none">สถานที่จัดส่งเอกสาร :</div></td>
                                        <td colspan="4">
                                            <asp:Label ID="lblSendDoc" runat="server" Text="" Visible="false"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="4">
                                            <asp:Label ID="lblDocAddress1" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>                            
                        </tr>                        
                    </table>
                </td>
            </tr>
        </table>
        
        <strong>ขอบคุณที่ท่านไว้วางใจเลือกใช้บริการกับ บริษัท สินมั่นคงประกันภัย จำกัด (มหาชน)</strong>
        <br />
        <br />
        <div style="margin: 0px auto; width: 190px;  margin-top: 15px;">
            <asp:LinkButton ID="LinkButton1" runat="server" OnClick="btnPrint_Click">รูปแบบ pdf</asp:LinkButton>&nbsp;
            <a href="home.aspx">กลับสู่หน้าแรก</a><br />
        </div>
    </div>
    <!-- End  <div class="context page-app"> -->
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

