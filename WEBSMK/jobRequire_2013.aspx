﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="jobRequire.aspx.cs" Inherits="jobRequire" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 
      <script>
          $(function () {  
               $(".acc-bullet").addClass('acc-icon');    //////set start all icon  

                //================ Get Element by Jquery
                // $(this).children($(".acc-bullet")) //////////   Get All Children
                // $(this).children('.acc-bullet') //////////   Get One Children
                // $(this).children()[0] //////////   Get One Children
                // $(this).find($(".acc-bullet")); //////////   Get One Children     

                $(".acc-title").click(function () {
                //================ Set Var
                elmDetail = $(this).next();
                elmBullet = $(this).children()[0];
                elmDetailAll = $('.acc-detail');
                elmBulletAll = $('.acc-bullet');

                //=============== Set All
                elmDetailAll.slideUp(200);                elmBulletAll.removeClass('acc-icon2');
                elmBulletAll.addClass('acc-icon');
                //=============== Set Target Event
                  if (elmDetail.is(':hidden')) {
                      elmDetail.slideDown(200);
                      elmBullet.removeClass('acc-icon');
                      elmBullet.addClass('acc-icon2');
                  } else {
                      elmDetail.slideUp(200);
                      elmBullet.removeClass('acc-icon2');
                      elmBullet.addClass('acc-icon');
                     
                  } 
              })

          });//end ready
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
        <div id="page-jobRequire">
        <div class="subject1">สมัครงาน <asp:Label ID="lblPageName" runat="server"  ></asp:Label></div>
        ท่านสามารถสมัครออนไลน์ หรือส่งประวัติการทำงานของท่านมาที่   ฝ่ายบริหารทรัพยากรบุคคล 
        <br>  <strong>บริษัท สินมั่นคงประกันภัย จำกัด (มหาชน)</strong>
         <br>        313 ถนนศรีนครินทร์  แขวงหัวหมาก เขตบางกะปิ กรุงเทพฯ 10240<br />
        โทรศัพท์ : 02-378-7000 ต่อ 8121-2 ,   โทรสาร : 02-377-3322 ,  Email : <a href="mailto:hr@smk.co.th">hr@smk.co.th</a>
        <br>
        <br>
        <div id="accordion">
           

<asp:DataList ID="DataList1" runat="server" OnItemDataBound="DataList1_ItemDataBound">
<ItemTemplate>    
<div class="acc-row">
<div class="acc-title">
    <div class="acc-bullet" ></div>

<div class="posi-topic">
<div class="posi-name"> 
<asp:Label ID="lblPosiName" runat="server" > </asp:Label> 
<asp:Literal ID="ltlPiority" runat="server"></asp:Literal> 
</div>  
<div class="posi-branch">
<asp:Label ID="lblPosiBranch" runat="server" ></asp:Label>  
</div>
</div><!-- End  class="posi-topic" -->



<div class="posi-qnt">  
<asp:Label ID="lblQnt" runat="server"  ></asp:Label> 
</div>
    <div class="clear"></div>  
</div> <!-- End  class="acc-title" -->
<div class="acc-detail" style ="display:none">

<div class='posi-detail' > 
<asp:Label ID="lblDetail" runat="server" Text="Label"></asp:Label>
</div>      
<div class='posi-btn' >
<asp:Button ID="btnApply" runat="server" Text="สมัครงาน" class="btn-default  btn-small"  />
</div>
   <div class="clear"></div>              
</div>
                <!-- End  class="acc-detail" -->
            </div>
            <!-- end class="acc-row" -->
    </ItemTemplate>
    </asp:DataList>
        </div>
        <!-- End <div id="accordion"> -->
    </div>
    <!-- End  id="page-jobOfficerRequire"  -->
</asp:Content>

