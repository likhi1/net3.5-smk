﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//--- connect
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
//--- arrayList
using System.Collections;

 

public partial class network : System.Web.UI.Page {
    protected int CatId { 
        get { 
             int catId ;
             if (string.IsNullOrEmpty(Request.QueryString["cat"])) {
                 catId = 1;
             } else {
                 catId = Convert.ToInt32(Request.QueryString["cat"]);
             }
             return catId ; 
        } 
    }
    protected int ZoneId {
        get {
            int zoneId;
            if (string.IsNullOrEmpty(Request.QueryString["zone"])) {
                zoneId = 1;
            } else {
                zoneId = Convert.ToInt32(Request.QueryString["zone"]);
            }
            return zoneId;
        }
    }
    
    protected string PageName; 
    protected DataTable DtCat;

    //====== Create Object for Add Session
    //public NetworkData  networkData ;

    //==============  Set Empty varible ;
    protected string Type;
    protected string Pro;
    protected string Amp;
    protected string Key;
  
    
    protected void Page_Load(object sender, EventArgs e) {

        ///// Set Varible from => Page networkSearch.aspx
        //ddlNetworkType.SelectedValue = "";
        //ddlProvince.SelectedValue = "";
        //ddlAmphoe.SelectedValue = "";
        //tbxKeyWord.Text = "";

        ///// BindDropDown 
        SelectCat();
        ddlProvinceBindData();
        ddlAmphoeBindData();
        ddlBranchTypeBindData(CatId);

        /////// Set Pagename
        GetNamePage();
        lblPageName.Text = PageName;

        if (!Page.IsPostBack) {  
            BindData();
        } else {
           
        }

    }

    protected void BindData() {
        DataSet ds = SelectData(CatId, ZoneId);

        GridView1.AllowPaging = true;
        GridView1.PageSize = 20;
        GridView1.PagerStyle.CssClass = "cssPager";

        GridView1.DataSource = ds;
        GridView1.DataBind();

    }

    protected DataSet SelectData(int ct, int zo) {
        string sql1 = "SELECT * FROM tbNtw "
                      + "WHERE NtwCatId='" + ct + "'  "
                      + "AND NtwZoneId='" + zo + "'   "
                      + "AND NtwStatus= '1' "
                      + "AND lang = '0' "
                      + "ORDER BY NtwSort ASC , NtwName ASC ,   NtwId DESC   ";  // Sort By NtwSort  &   NtwName  &  NtwId
        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql1, "tbNtw");
        return ds;

    }

    //=======================  Set PageName
    protected string GetNamePage() {
        foreach (DataRow r in DtCat.Rows) {
            if ((int)r[0] == CatId) {
                PageName = r[1].ToString();
            }
        }
        return PageName;
    }

    //=============  Set DropDown
    protected DataTable SelectCat() {
        string sql = "SELECT  NtwCatId  ,  NtwCatName  FROM tbNtwCat  ORDER BY NtwCatId ";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "_tb");
        DtCat = ds.Tables[0];
        return DtCat;
    }

    protected void ddlBranchTypeBindData(int CatId) {
        string sql = "SELECT * FROM tbNtwType WHERE NtwTypeCat='" + CatId + "'  ORDER BY NtwTypeId";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "_tb");

        ddlNetworkType.DataSource = ds;
        //**** Add frist Item of dropdownlist
        ddlNetworkType.AppendDataBoundItems = true;
        ddlNetworkType.DataTextField = "NtwTypeNameTh";
        ddlNetworkType.DataValueField = "NtwTypeId";
        ddlNetworkType.DataBind();
    }

    protected void ddlProvinceBindData() {
        string sql = "SELECT ds_major_cd , RTRIM(ds_desc_t) AS ds_desc_t  FROM setcode  WHERE (ds_major_cd = 'CH') ORDER BY  ds_desc_t ";

        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "tb");

        ddlProvince.DataSource = ds;
        //**** Add frist Item of dropdownlist
        ddlProvince.AppendDataBoundItems = true;
        ddlProvince.DataTextField = "ds_desc_t";
        //ddlProvince.DataValueField = "ds_desc_t";
        ddlProvince.DataBind();

    }

    protected void ddlAmphoeBindData() {
        string sql = "SELECT ds_major_cd , ds_desc , RTRIM(ds_desc_t) AS ds_desc_t  FROM setcode  WHERE (ds_major_cd = 'AM') AND (ds_desc = '0')  ORDER BY ds_desc_t  ";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "tb");

        ddlAmphoe.DataSource = ds;
        //**** Add frist Item of dropdownlist
        ddlAmphoe.AppendDataBoundItems = true;
        ddlAmphoe.DataTextField = "ds_desc_t";
        // ddlAmphoe.DataValueField = "ds_desc_t";
        ddlAmphoe.DataBind();

    }

    //============== Set Gridview
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e) {
  
          if (e.Row.RowType == DataControlRowType.DataRow) {

            Label lblName = (Label)(e.Row.FindControl("lblName"));
            if (lblName != null) {
                 lblName.Text = DataBinder.Eval(e.Row.DataItem, "NtwName").ToString();
                lblName.Style.Add("font-weight", "bold"); 
            }
         
            Label lblAddress = (Label)(e.Row.FindControl("lblAddress"));
            if (lblAddress != null) {
                lblAddress.Text = DataBinder.Eval(e.Row.DataItem, "NtwAddress").ToString();
            }
               
            Label lblPhone = (Label)(e.Row.FindControl("lblPhone"));
            object NtwTelPhone = DataBinder.Eval(e.Row.DataItem, "NtwTelPhone");
            object NtwTelMobile = DataBinder.Eval(e.Row.DataItem, "NtwTelMobile");

            if (!Convert.IsDBNull(NtwTelPhone) && NtwTelPhone.ToString() != ""){ 
                lblPhone.Text =  "โทรศัพท์ : " + NtwTelPhone.ToString()  ;

                if( (!Convert.IsDBNull(NtwTelMobile) && NtwTelMobile.ToString() != "")  )  
                lblPhone.Text +=  ",  " +NtwTelMobile.ToString()  ;
  
            }else{
                if( (!Convert.IsDBNull(NtwTelMobile) && NtwTelMobile.ToString() != "")  )  
                lblPhone.Text +=  "โทรศัพท์ : " + NtwTelMobile.ToString()  ;

            }

            Label lblProvince = (Label)(e.Row.FindControl("lblProvince"));
            if (lblProvince != null) {
                lblProvince.Text = DataBinder.Eval(e.Row.DataItem, "NtwProvince").ToString();
                lblProvince.Attributes.Add("style", "text-align:center");
            }

            Label lblService = (Label)(e.Row.FindControl("lblService"));
            string NtwService = DataBinder.Eval(e.Row.DataItem, "NtwService").ToString();
            if (!String.IsNullOrEmpty(NtwService)) {
                lblService.Text = "(" + NtwService + ")";
                lblService.Style.Add("color", "#F17503");
            }


            Label lblNotation = (Label)(e.Row.FindControl("lblNotation"));
            if (lblNotation != null) {
                string strNotation = DataBinder.Eval(e.Row.DataItem, "NtwNotation").ToString();
                lblNotation.Text = (strNotation != "" ? "หมายเหตุ : " : "") + strNotation;
            }

            Label lblAmphoe = (Label)(e.Row.FindControl("lblAmphoe"));
            if (lblAmphoe != null) {
                string strAmphoe = DataBinder.Eval(e.Row.DataItem, "NtwAmphoe").ToString(); 
                if (!String.IsNullOrEmpty(strAmphoe)) {
                    lblAmphoe.Text = "<br>";
                    lblAmphoe.Text += "(" + strAmphoe + ")";
                    lblAmphoe.Attributes.Add("style", "text-align:center"); 
                }
            }


            HyperLink hplMap = (HyperLink)(e.Row.FindControl("hplMap"));
            if (hplMap != null) {
                string strNtwId = DataBinder.Eval(e.Row.DataItem, "NtwId").ToString();
                hplMap.CssClass = "openerCover";
                hplMap.Text = "ดูแผนที่"; 
                hplMap.NavigateUrl = "networkMap.aspx?id=" + strNtwId  ;
                hplMap.Attributes.Add("OnClick", "clickModal();return false ;");
            } 

            HyperLink hplWebsite = (HyperLink)(e.Row.FindControl("hplWebsite"));
            object NtwWebsite = DataBinder.Eval(e.Row.DataItem, "NtwWebsite");
            if (NtwWebsite.ToString() != "") {
                if (hplWebsite != null && !Convert.IsDBNull(NtwWebsite)) {
                    string strNtwWebsite = NtwWebsite.ToString().Replace("http://", "");
                    hplWebsite.NavigateUrl = "http://" + NtwWebsite.ToString();
                    hplWebsite.Target = "blank";
                    hplWebsite.Text = NtwWebsite.ToString();
                }
            } else {
                hplWebsite.Visible = false;
            }
        

        }
        
          
    }
     
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e) {
        GridView1.PageIndex = e.NewPageIndex;
        BindData();
    }

    //=============  Set Event
    protected void BtnSearch_Click(object sender, EventArgs e) {

        //====== Create Object for Add Session
        //networkData.province = ddlProvince.SelectedValue;
        //networkData.amphoe = ddlAmphoe.SelectedValue;
        //networkData.keyWord = tbxKeyWord.Text;
        //networkData.networkType = ddlNetworkType.SelectedValue;
        //Session.Add("SessionNetwork", networkData);

        //======= Serach By QueryString 
        Type = ddlNetworkType.SelectedValue;
        Pro = Server.HtmlEncode ( ddlProvince.SelectedValue ) ;
        Amp = Server.HtmlEncode ( ddlAmphoe.SelectedValue ) ;
        Key = Server.HtmlEncode ( tbxKeyWord.Text )  ;

        Response.Redirect("networkSearch.aspx?cat=" + CatId + "&zone=" + ZoneId + "&type=" + Type + "&pro=" + Pro + "&amp=" + Amp + "&key=" + Key);


    } 



}