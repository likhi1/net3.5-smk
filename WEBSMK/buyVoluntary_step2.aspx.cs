﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class buyVoluntary_step2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        if (Session["DSRegister"] != null)
            ds = (DataSet)Session["DSRegister"];
        else
            Response.Redirect("buyVoluntary_step1.aspx", true);

        if (!IsPostBack)
        {
            initialDropdownList();

            docToUI(ds);
        }        
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        string strTmpCD = "";
        bool isSave = true;
        RegisterManager cmRegister = new RegisterManager();
		TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        double dStartDate = 0;
        double dMinDate = 0;
        string strMinDate = "";

        if (DateTime.Now.Hour >= 16 && DateTime.Now.Minute >= 30)
        {
            dMinDate = Convert.ToDouble(cmDate.convertDateStringForDBTH(FormatStringApp.FormatDate(DateTime.Now.AddDays(1))).Replace("/",""));
            strMinDate = FormatStringApp.FormatDate(DateTime.Now.AddDays(1));
        }
        else
        {
            dMinDate = Convert.ToDouble(cmDate.convertDateStringForDBTH(FormatStringApp.FormatDate(DateTime.Now)).Replace("/",""));
            strMinDate = FormatStringApp.FormatDate(DateTime.Now);
        }
        dStartDate = Convert.ToDouble(cmDate.convertDateStringForDBTH(txtStartDate.Text).Replace("/",""));
        if (dStartDate < dMinDate)
        {
            litScript.Text = "<script>";
            litScript.Text += "showMessagePage('วันที่เริ่มคุ้มครองต้องมีค่าตั้งแต่วันที่ " + strMinDate + "');";
            litScript.Text += "</script>";
        }
		else
		{		
			if (Session["DSRegister"] != null)
				ds = (DataSet)Session["DSRegister"];
			// register
			//ds.Tables[0].Rows[0]["rg_main_class"] = "M";
			//ds.Tables[0].Rows[0]["rg_ins_fname"] = txtRegName.Text;
			//ds.Tables[0].Rows[0]["rg_ins_lname"] = txtRegSur.Text;
			//ds.Tables[0].Rows[0]["rg_ins_tel"] = txtRegMobile.Text;
			//ds.Tables[0].Rows[0]["rg_ins_email"] = txtRegEmail.Text;
			// voluntary
			ds.Tables[0].Rows[0]["vl_drv1"] = txtDriver1Name.Text;
			ds.Tables[0].Rows[0]["vl_drv2"] = txtDriver2Name.Text;
			// mtveh
			ds.Tables[0].Rows[0]["mv_license_no"] = txtCarLicense.Text;
			if (txtCarCode.Text == "110")
			{
				ds.Tables[0].Rows[0]["mv_veh_seat"] = "0";
				ds.Tables[0].Rows[0]["mv_veh_weight"] = "0";
			}
			else if (txtCarCode.Text == "210")
			{
				ds.Tables[0].Rows[0]["mv_veh_cc"] = "0";
				ds.Tables[0].Rows[0]["mv_veh_seat"] = "12";
				ds.Tables[0].Rows[0]["mv_veh_weight"] = "0";
			}
			else if (txtCarCode.Text == "320")
			{
				ds.Tables[0].Rows[0]["mv_veh_cc"] = "0";
				ds.Tables[0].Rows[0]["mv_veh_seat"] = "0";
				ds.Tables[0].Rows[0]["mv_veh_weight"] = "4";
			}
			// new column
			ds.Tables[0].Rows[0]["rg_effect_dt"] = txtStartDate.Text;
			DateTime dt = new DateTime(Convert.ToInt32(txtStartDate.Text.Substring(6, 4))-543, Convert.ToInt32(txtStartDate.Text.Substring(3, 2)), Convert.ToInt32(txtStartDate.Text.Substring(0, 2)));
			dt = dt.AddYears(1);
			ds.Tables[0].Rows[0]["rg_pol_type"] = txtPolType.Text;
			ds.Tables[0].Rows[0]["rg_expiry_dt"] = FormatStringApp.FormatDate(dt);
			ds.Tables[0].Rows[0]["rg_regis_dt"] = FormatStringApp.FormatDate(DateTime.Now);
			ds.Tables[0].Rows[0]["rg_regis_time"] = FormatStringApp.FormatTime(DateTime.Now); 
			ds.Tables[0].Rows[0]["mv_chas_no"] = txtChasis.Text;
			ds.Tables[0].Rows[0]["mv_engin_no"] = txtEngineNo.Text;
			ds.Tables[0].Rows[0]["rg_ins_fname"] = txtFName.Text;
			ds.Tables[0].Rows[0]["rg_ins_lname"] = txtLName.Text;
			ds.Tables[0].Rows[0]["rg_ins_mobile"] = txtMobile.Text;
			ds.Tables[0].Rows[0]["rg_ins_email"] = txtEmail.Text;
			ds.Tables[0].Rows[0]["rg_ins_addr1"] = txtAddress.Text;
			ds.Tables[0].Rows[0]["rg_ins_addr2"] = txtSubDistrict.Text;
			ds.Tables[0].Rows[0]["rg_ins_amphor"] = txtDistrict.Text;
			ds.Tables[0].Rows[0]["rg_ins_tel"] = txtTelephone.Text;
			ds.Tables[0].Rows[0]["rg_ins_changwat"] = ddlProvince.SelectedValue;
			ds.Tables[0].Rows[0]["rg_ins_postcode"] = txtPostCode.Text;
			ds.Tables[0].Rows[0]["rg_ins_idcard"] = txtIDCard.Text;        

			// cover
			DataSet dsTmp = new DataSet();
			OnlineVoluntaryManager cmOnline = new OnlineVoluntaryManager();
			dsTmp = cmOnline.getDataCover(txtCampaign.Text, txtPolType.Text, txtCarCode.Text, txtCarSize.Text,txtCarBody.Text);
			ds.Tables[0].Rows[0]["vl_tpbi_person"] = dsTmp.Tables[0].Rows[0]["tpbi1"].ToString();
			ds.Tables[0].Rows[0]["vl_tpbi_time"] = dsTmp.Tables[0].Rows[0]["tpbi2"].ToString();
			ds.Tables[0].Rows[0]["vl_tppd_time"] = dsTmp.Tables[0].Rows[0]["tppd"].ToString();
			ds.Tables[0].Rows[0]["vl_od_exc"] = dsTmp.Tables[0].Rows[0]["od_dd"].ToString();
			ds.Tables[0].Rows[0]["rg_sum_ins"] = lblCarOD.Text.Replace(",","");//dsTmp.Tables[0].Rows[0]["car_od"].ToString();
			ds.Tables[0].Rows[0]["vl_01_11"] = dsTmp.Tables[0].Rows[0]["perm_d_01"].ToString();
			ds.Tables[0].Rows[0]["vl_01_121"] = dsTmp.Tables[0].Rows[0]["perm_p_num"].ToString();
			ds.Tables[0].Rows[0]["vl_01_122"] = dsTmp.Tables[0].Rows[0]["perm_p_01"].ToString();
			ds.Tables[0].Rows[0]["vl_01_21"] = dsTmp.Tables[0].Rows[0]["temp_d_01"].ToString();
			ds.Tables[0].Rows[0]["vl_02_person"] = dsTmp.Tables[0].Rows[0]["temp_p_num"].ToString();
			ds.Tables[0].Rows[0]["vl_01_212"] = dsTmp.Tables[0].Rows[0]["temp_p_01"].ToString();
			ds.Tables[0].Rows[0]["vl_02"] = dsTmp.Tables[0].Rows[0]["cover_02"].ToString();
			ds.Tables[0].Rows[0]["vl_03"] = dsTmp.Tables[0].Rows[0]["cover_03"].ToString();
			ds.Tables[0].Rows[0]["vl_veh_cd"] = txtCarCode.Text;
			ds.Tables[0].Rows[0]["mv_license_area"] = ddlLicenseProvince.SelectedValue;
			ds.Tables[0].Rows[0]["vl_prmm"] = lblPremNet.Text.Replace(",","");
			ds.Tables[0].Rows[0]["vl_campaign"] = txtCampaign.Text;
	        
			strTmpCD = cmRegister.insertTmpVoluntary(ds);
			ds.Tables[0].Rows[0]["tm_tmp_cd"] = strTmpCD;
			ds.Tables[0].Rows[0]["campaign"] = txtCampaign.Text;
			Session.Add("DSRegister", ds);
			if (strTmpCD != "")
				Response.Redirect("buyVoluntary_step3.aspx", true);
		}
    }

    public void initialDropdownList()
    {
        DataSet ds;
        OnlineVoluntaryManager cmOnline = new OnlineVoluntaryManager();

        ds = cmOnline.getAllProvince();
        ddlProvince.DataSource = ds;
        ddlProvince.DataTextField = "ds_desc";
        ddlProvince.DataValueField = "ds_minor_cd";
        ddlProvince.DataBind();
        ddlProvince.Items.Insert(0, new ListItem(" - เลือก - ", ""));

        ddlLicenseProvince.DataSource = ds;
        ddlLicenseProvince.DataTextField = "ds_desc";
        ddlLicenseProvince.DataValueField = "ds_minor_cd";
        ddlLicenseProvince.DataBind();
        ddlLicenseProvince.Items.Insert(0, new ListItem(" - เลือก - ", ""));
    }
    public void docToUI(DataSet ds)
    {
        double dDiscount = 0;
        double dPrem = 0;
        double dStamp = 0;
        double dTax = 0;
        string strMotGar = "";
        DataSet dsPrem = new DataSet();
        DataSet dsTmp = new DataSet();
        OnlineVoluntaryManager cmOnline = new OnlineVoluntaryManager();

        if (Request.QueryString["type"] != null)
        {
            if (Request.QueryString["type"]  == "G")
                strMotGar = "N";
            else
                strMotGar = "Y";
        }
        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            dsPrem = cmOnline.getDataOnlineWebByID(Request.QueryString["web_id"], ds.Tables[0].Rows[0]["mv_combine"].ToString());
            if (dsPrem.Tables[0].Rows.Count > 0)
            {
                txtCampaign.Text = dsPrem.Tables[0].Rows[0]["campaign"].ToString();
                lblCampaignName.Text = dsPrem.Tables[0].Rows[0]["name"].ToString();
                txtPolType.Text = dsPrem.Tables[0].Rows[0]["pol_typ"].ToString();
                lblPolType.Text = "ประเภท " + dsPrem.Tables[0].Rows[0]["pol_typ"].ToString();
                lblMotGar.Text = (strMotGar == "N" ? "ซ่อมอู่" : "ซ่อมห้าง");
                lblCarOD.Text = FormatStringApp.FormatInt(dsPrem.Tables[0].Rows[0]["car_od"]);
                txtCarBody.Text = dsPrem.Tables[0].Rows[0]["car_body"].ToString();
                if (strMotGar == "N")
                {
                    dPrem = Convert.ToDouble(dsPrem.Tables[0].Rows[0]["pre_netgar"].ToString());                    
                    // หาส่วนลดประวัติดี สำหรับซ่อมอุ่
                    if (txtCampaign.Text != "013")
                    {
                        if (ds.Tables[0].Rows[0]["exp_percent"].ToString() != "" &&
                            dsPrem.Tables[0].Rows[0]["pol_typ"].ToString() != "3")
                        {
                            dDiscount = Convert.ToDouble(ds.Tables[0].Rows[0]["exp_percent"].ToString());
                            dPrem = dPrem - (dPrem * dDiscount / 100);
                        }
                    }
                    dStamp = Math.Ceiling(dPrem * 0.4 / 100);
                    dTax = (dPrem + dStamp) * 7 /100;
                    lblPremNet.Text = FormatStringApp.Format2Digit(dPrem + dStamp + dTax);
                    ds.Tables[0].Rows[0]["rg_prmm"] = dPrem.ToString();
                    ds.Tables[0].Rows[0]["rg_tax"] = dTax.ToString();
                    ds.Tables[0].Rows[0]["rg_stamp"] = dStamp.ToString();
                    ds.Tables[0].Rows[0]["rg_prmmgross"] = FormatStringApp.Format2Digit(dPrem + dStamp + dTax);
                    //lblPremNet.Text = FormatStringApp.Format2Digit(dsPrem.Tables[0].Rows[0]["pre_grsgar"]);
                    //dsRegister.Tables[0].Rows[0]["rg_prmm"] = dsPrem.Tables[0].Rows[0]["pre_netgar"].ToString();
                    //dsRegister.Tables[0].Rows[0]["rg_tax"] = dsPrem.Tables[0].Rows[0]["pre_taxgar"].ToString();
                    //dsRegister.Tables[0].Rows[0]["rg_stamp"] = dsPrem.Tables[0].Rows[0]["pre_stmgar"].ToString();
                }
                else if (strMotGar == "Y")
                {
                    dPrem = Convert.ToDouble(dsPrem.Tables[0].Rows[0]["pre_netservice"].ToString());
                    // หาส่วนลดประวัติดี สำหรับซ่อมห้าง
                    if (txtCampaign.Text != "013")
                    {
                        if (ds.Tables[0].Rows[0]["exp_percent"].ToString() != "")
                        {
                            if (txtCampaign.Text == "500")
                            {
                                if (Convert.ToDouble(ds.Tables[0].Rows[0]["exp_percent"].ToString()) > 20)
                                    ds.Tables[0].Rows[0]["exp_percent"] = "20";
                            }
                            dDiscount = Convert.ToDouble(ds.Tables[0].Rows[0]["exp_percent"].ToString());
                            dPrem = dPrem - (dPrem * dDiscount / 100);
                        }
                    }
                    dStamp = Math.Ceiling(dPrem * 0.4 / 100);
                    dTax = (dPrem + dStamp) * 7 / 100;
                    lblPremNet.Text = FormatStringApp.Format2Digit(dPrem + dStamp + dTax);
                    ds.Tables[0].Rows[0]["rg_prmm"] = dPrem.ToString();
                    ds.Tables[0].Rows[0]["rg_tax"] = dTax.ToString();
                    ds.Tables[0].Rows[0]["rg_stamp"] = dStamp.ToString();
                    ds.Tables[0].Rows[0]["rg_prmmgross"] = FormatStringApp.Format2Digit(dPrem + dStamp + dTax);
                    //lblPremNet.Text = FormatStringApp.Format2Digit(dsPrem.Tables[0].Rows[0]["pre_grsservice"]);
                    //dsRegister.Tables[0].Rows[0]["rg_prmm"] = dsPrem.Tables[0].Rows[0]["pre_netservice"].ToString();
                    //dsRegister.Tables[0].Rows[0]["rg_tax"] = dsPrem.Tables[0].Rows[0]["pre_taxservice"].ToString();
                    //dsRegister.Tables[0].Rows[0]["rg_stamp"] = dsPrem.Tables[0].Rows[0]["pre_stmservice"].ToString();
                }
                //if (txtCampaign.Text != "013")
                //{
                    // หาเบี้ย พรบ.เพิ่ม
                    if (ds.Tables[0].Rows[0]["mv_combine"].ToString() == "Y")
                    {
                        lblFlagCompul.Text = FormatStringApp.Format2Digit(dsPrem.Tables[0].Rows[0]["pre_grs"]);
                        ds.Tables[0].Rows[0]["cp_veh_cd"] = "";
                        ds.Tables[0].Rows[0]["cp_comp_prmm"] = dsPrem.Tables[0].Rows[0]["pre_net"].ToString();
                        ds.Tables[0].Rows[0]["cp_comp_tax"] = dsPrem.Tables[0].Rows[0]["pre_tax"].ToString();
                        ds.Tables[0].Rows[0]["cp_comp_stamp"] = dsPrem.Tables[0].Rows[0]["pre_stm"].ToString();

                        TDS.Utility.MasterUtil.writeLog("SMK :: BuyVoluntary_step2-docToUI", "Combine");
                        TDS.Utility.MasterUtil.writeLog("SMK :: BuyVoluntary_step2-docToUI", "rg_prmm-0" + ds.Tables[0].Rows[0]["rg_prmm"].ToString());
                        //ds.Tables[0].Rows[0]["rg_prmm"] = FormatStringApp.Format2Digit(Convert.ToDouble(ds.Tables[0].Rows[0]["cp_comp_prmm"]) + Convert.ToDouble(ds.Tables[0].Rows[0]["rg_prmm"]));
                        //ds.Tables[0].Rows[0]["rg_tax"] = FormatStringApp.Format2Digit(Convert.ToDouble(ds.Tables[0].Rows[0]["cp_comp_tax"]) + Convert.ToDouble(ds.Tables[0].Rows[0]["rg_tax"]));
                        //ds.Tables[0].Rows[0]["rg_stamp"] = FormatStringApp.Format2Digit(Convert.ToDouble(ds.Tables[0].Rows[0]["cp_comp_stamp"]) + Convert.ToDouble(ds.Tables[0].Rows[0]["rg_stamp"]));
                        ds.Tables[0].Rows[0]["rg_prmmgross"] = FormatStringApp.Format2Digit(Convert.ToDouble(ds.Tables[0].Rows[0]["rg_prmmgross"]) + Convert.ToDouble(ds.Tables[0].Rows[0]["cp_comp_prmm"]) + Convert.ToDouble(ds.Tables[0].Rows[0]["cp_comp_tax"]) + Convert.ToDouble(ds.Tables[0].Rows[0]["cp_comp_stamp"]));
                        TDS.Utility.MasterUtil.writeLog("SMK :: BuyVoluntary_step2-docToUI", "rg_prmm" + ds.Tables[0].Rows[0]["rg_prmm"].ToString());
                        TDS.Utility.MasterUtil.writeLog("SMK :: BuyVoluntary_step2-docToUI", "cp_comp_prmm" + ds.Tables[0].Rows[0]["cp_comp_prmm"].ToString());
                    }
                    else
                        lblFlagCompul.Text = "-";
                //}
                //else 
                if (txtCampaign.Text == "013")
                {
                    lblLabelComprem.Visible = false;
                    lblFlagCompul.Visible = false;
                    lblLabelVolunPrem.Text = "เบี้ย Save&Safe (รวม พรบ.) :";
                    if (strMotGar == "N")
                    {
                        lblPremNet.Text = FormatStringApp.Format2Digit(Convert.ToDouble(dsPrem.Tables[0].Rows[0]["pre_grsgar"]));
                        ds.Tables[0].Rows[0]["rg_prmmgross"] = FormatStringApp.Format2Digit(Convert.ToDouble(dsPrem.Tables[0].Rows[0]["pre_grsgar"]));
                    }
                    else
                    {
                        lblPremNet.Text = FormatStringApp.Format2Digit(Convert.ToDouble(dsPrem.Tables[0].Rows[0]["pre_grsservice"]));
                        ds.Tables[0].Rows[0]["rg_prmmgross"] = FormatStringApp.Format2Digit(Convert.ToDouble(dsPrem.Tables[0].Rows[0]["pre_grsservice"]));
                    }
                }
                ds.Tables[0].Rows[0]["vl_repair"] = strMotGar;
                txtCarCode.Text = dsPrem.Tables[0].Rows[0]["car_cod"].ToString().Trim();
                if (dsPrem.Tables[0].Rows[0]["car_cod"].ToString().Trim() == "110")
                {
                    lblCarType.Text = "รถยนต์นั่ง";
                    lblCarUseDesc.Text = "ส่วนบุคคล";
                }
                else if (dsPrem.Tables[0].Rows[0]["car_cod"].ToString().Trim() == "210")
                {
                    lblCarType.Text = "รถยนต์โดยสาร";
                    lblCarUseDesc.Text = "ส่วนบุคคล";
                }
                else if (dsPrem.Tables[0].Rows[0]["car_cod"].ToString().Trim() == "320")
                {
                    lblCarType.Text = "รถยนต์บรรทุก";
                    lblCarUseDesc.Text = "เพื่อการพาณิชย์";
                }
            }
            if (ds.Tables[0].Rows[0]["vl_drv_flag"].ToString() == "Y")
            {
                lblFlagDriver.Text = "ระบุผู้ขับขี่";                
                if (ds.Tables[0].Rows[0]["vl_birth_drv1"].ToString() != "" &&
                    ds.Tables[0].Rows[0]["vl_birth_drv1"].ToString() != "__/__/____")
                {
                    lblDriver1BrithDate.Text = ds.Tables[0].Rows[0]["vl_birth_drv1"].ToString();
                }
                if (ds.Tables[0].Rows[0]["vl_birth_drv2"].ToString() != "" &&
                    ds.Tables[0].Rows[0]["vl_birth_drv2"].ToString() != "__/__/____")
                {
                    lblDriver2BrithDate.Text = ds.Tables[0].Rows[0]["vl_birth_drv2"].ToString();
                    litScript.Text = "<script>document.getElementById('spanDriverName2').style.display = 'inline';</script>";
                }
                else
                {
                    litScript.Text = "<script>document.getElementById('spanDriverName2').style.display = 'none';</script>";
                    txtDriver2Name.CssClass = "TEXTBOXDIS";
                    txtDriver2Name.ReadOnly = true;
                }
                divDriver.Attributes["style"] = "display:inline;";
            }
            else if (ds.Tables[0].Rows[0]["vl_drv_flag"].ToString() == "N")
            {
                lblFlagDriver.Text = "ไม่ระบุผู้ขับขี่";
                txtDriver1Name.CssClass = "";
                txtDriver1Name.ReadOnly = true;
                txtDriver2Name.CssClass = "";
                txtDriver2Name.ReadOnly = true;
                lblDriver1BrithDate.Text = "";
                lblDriver2BrithDate.Text = "";
                divDriver.Attributes["style"] = "display:none;";
            }

            dsTmp = cmOnline.getDataCover(txtCampaign.Text, txtPolType.Text, txtCarCode.Text, txtCarSize.Text,txtCarBody.Text);
            if (dsTmp.Tables[0].Rows.Count > 0)
            {
                if (dsTmp.Tables[0].Rows[0]["od_dd"].ToString() == "0" || dsTmp.Tables[0].Rows[0]["od_dd"].ToString() == "0.00")
                    lblFlagDeduct.Text = "-";
                else
                    lblFlagDeduct.Text = FormatStringApp.FormatInt(dsTmp.Tables[0].Rows[0]["od_dd"].ToString());
            }
            else
            {
                lblFlagDeduct.Text = "-";
            }
            if (ds.Tables[0].Rows[0]["oth_oldpolicy"].ToString() == "Y")
                lblFlagOldPolicy.Text = "มี";
            else
                lblFlagOldPolicy.Text = "ไม่มี";
            if (ds.Tables[0].Rows[0]["insc_id"].ToString() != "")
            {
                dsTmp = cmOnline.getDataInsuranceCompanyByID(ds.Tables[0].Rows[0]["insc_id"].ToString());
                if (dsTmp.Tables[0].Rows.Count > 0)
                    lblOldInsuraceComp.Text = dsTmp.Tables[0].Rows[0]["insc_name"].ToString();
            }
            else
                lblOldInsuraceComp.Text = "-";
            if (ds.Tables[0].Rows[0]["mott_id"].ToString() != "")
            {
                dsTmp = cmOnline.getDataMotorTypeByID(ds.Tables[0].Rows[0]["mott_id"].ToString());
                if (dsTmp.Tables[0].Rows.Count > 0)
                    lblOldPolType.Text = dsTmp.Tables[0].Rows[0]["mott_name"].ToString();                
            }
            else
                lblOldPolType.Text = "-";

            dsTmp = cmOnline.getDataCarNameByCode(ds.Tables[0].Rows[0]["mv_major_cd"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                lblCarName.Text = dsTmp.Tables[0].Rows[0]["desc_t"].ToString();

            dsTmp = cmOnline.getDataCarMarkByCode(ds.Tables[0].Rows[0]["mv_minor_cd"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                lblCarMark.Text = dsTmp.Tables[0].Rows[0]["desc_t"].ToString();

            lblCarRegisYear.Text = ds.Tables[0].Rows[0]["mv_veh_year"].ToString();
            txtCarLicense.Text = "";
            ddlLicenseProvince.Text = "";
            if (ds.Tables[0].Rows[0]["oth_distance"].ToString() != "")
            {
                dsTmp = cmOnline.getDataDriveDistanceByID(ds.Tables[0].Rows[0]["oth_distance"].ToString());
                if (dsTmp.Tables[0].Rows.Count > 0)
                    lblDistance.Text = dsTmp.Tables[0].Rows[0]["ddis_name"].ToString();
            }
            else
                lblDistance.Text = "-";

            txtCarSize.Text = ds.Tables[0].Rows[0]["mv_veh_cc"].ToString();
            lblCarSize.Text = FormatStringApp.FormatInt(ds.Tables[0].Rows[0]["mv_veh_cc"].ToString());
            lblDriveRegion.Text = (ds.Tables[0].Rows[0]["oth_region"].ToString() == "1" ? "กรุงเทพฯ" : "ต่างจังหวัด");
            txtChasis.Text = "";
            txtFName.Text = ds.Tables[0].Rows[0]["rg_ins_fname"].ToString();            
            txtLName.Text = ds.Tables[0].Rows[0]["rg_ins_lname"].ToString();
            txtMobile.Text = ds.Tables[0].Rows[0]["rg_ins_mobile"].ToString();
            txtEmail.Text = ds.Tables[0].Rows[0]["rg_ins_email"].ToString();

            txtAddress.Text = "";
            txtSubDistrict.Text = "";            
            txtDistrict.Text = "";
            txtTelephone.Text = "";
            ddlProvince.SelectedValue = "";            
            txtPostCode.Text = "";
            txtIDCard.Text = "";

            Session["DSRegister"] = ds;
        }
    }
}
