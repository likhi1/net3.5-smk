﻿/**
 * Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

// This file contains style definitions that can be used by CKEditor plugins.
//
// The most common use for it is the "stylescombo" plugin, which shows a combo
// in the editor toolbar, containing all styles. Other plugins instead, like
// the div plugin, use a subset of the styles on their feature.
//
// If you don't have plugins that depend on this file, you can simply ignore it.
// Otherwise it is strongly recommended to customize this file to match your
// website requirements and design properly.

CKEDITOR.stylesSet.add( 'default', [
	/* Block Styles */

	// These styles are already available in the "Format" combo ("format" plugin),
	// so they are not needed here by default. You may enable them to avoid
	// placing the "Format" combo in the toolbar, maintaining the same features.
	/*
	{ name: 'Paragraph',		element: 'p' },
	{ name: 'Heading 1',		element: 'h1' },
	{ name: 'Heading 2',		element: 'h2' },
	{ name: 'Heading 3',		element: 'h3' },
	{ name: 'Heading 4',		element: 'h4' },
	{ name: 'Heading 5',		element: 'h5' },
	{ name: 'Heading 6',		element: 'h6' },
	{ name: 'Preformatted Text',element: 'pre' },
	{ name: 'Address',			element: 'address' },
	

	{ name: 'Italic Title',		element: 'h2', styles: { 'font-style': 'italic' } },
	{ name: 'Subtitle',			element: 'h3', styles: { 'color': '#aaa', 'font-style': 'italic' } },
	{
		name: 'Special Container',
		element: 'div',
		styles: {
			padding: '5px 10px',
			background: '#eee',
			border: '1px solid #ccc'
		}
	}, 
    */
{ name: 'Subject1', element: 'span', styles: { 'color': 'blue', 'border-bottom': '1px solid #ffde00', 'margin-bottom': '15px', 'font-weight': 'bold', ' diplay': 'none' }, attributes: {} },
{ name: 'Subject2', element: 'span', styles: { 'color': 'blue', 'border-bottom': '1px solid #ffde00', 'margin-bottom': '15px', 'font-weight': 'bold', ' diplay': 'none' }, attributes: {} },
{ name: 'Subject3', element: 'span', styles: { 'color': '#ef980f', 'border-bottom': '1px solid  #64acf4', 'margin-bottom': '15px', 'font-weight': 'none' }, attributes: {} },
 
{ name: 'AlignLeft', element: 'div', styles: { 'text-align': 'left' }, attributes: {} },
{ name: 'AlignRight', element: 'div', styles: { 'text-align': 'Right' }, attributes: {} },
{ name: 'AlignCenter', element: 'div', styles: { 'text-align': 'center' }, attributes: {} },
{ name: 'TextBold', element: 'div', styles: { 'font-weight': 'bold' }, attributes: {} }, 
{ name: 'TextBlue', element: 'div', styles: { 'color': 'blue' }, attributes: {} },
{ name: 'TextBlue-Bold', element: 'div', styles: { 'color': 'blue', 'font-weight': 'bold',   }, attributes: {} },
{ name: 'TextOrange', element: 'div', styles: { 'color': 'Orange' }, attributes: {} },
{ name: 'TextOrange-Bold', element: 'div', styles: { 'color': 'Orange', 'font-weight': 'bold' }, attributes: {} },
{ name: 'TextYelllow', element: 'div', styles: { 'color': 'Yellow' }, attributes: {} },
{ name: 'TextYellow-Bold', element: 'div', styles: { 'color': 'Yellow', 'font-weight': 'bold' }, attributes: {} },
{ name: 'TextRed', element: 'div', styles: { 'color': 'red' }, attributes: {} },
{ name: 'TextRed-Bold', element: 'div', styles: { 'color': 'red', 'font-weight': 'bold' }, attributes: {} },
 
 

 { name: 'Compact table',  element: 'table',
     attributes: {
         cellpadding: '5',
         cellspacing: '0',
         border: '1',
         bordercolor: '#ccc'
     },
     styles: {  'border-collapse': 'collapse'   }
 },

	{ name: 'Borderless Table', element: 'table', styles: { 'border-style': 'hidden', 'background-color': '#E6E6FA' } },
	{ name: 'Square Bulleted List', element: 'ul', styles: { 'list-style-type': 'square' } }

]);

