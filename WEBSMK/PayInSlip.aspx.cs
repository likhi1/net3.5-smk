using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Module_Paid_PayInSlip : System.Web.UI.Page
{
    public string insName;
    public string insSurName;
    public string insTel;
    public string tmpCd;
    public string oldPolicyNo;
    public double dPrmm;
    public string strPrmm;
    protected void Page_Load(object sender, EventArgs e)
    {
        RegisterManager cmRegister;   
        DataSet dsRegister;
        double dPremium = 0;
        double dTax = 0;
        double dStamp = 0;
        if (!IsPostBack)
        {
            cmRegister = new RegisterManager();            
            tmpCd = Request.QueryString["tmpCd"].ToString();
            dsRegister = cmRegister.getTmpRegisterDetail(tmpCd);
            if (dsRegister.Tables.Count > 0 && dsRegister.Tables[0].Rows.Count > 0)
            {
               tmpCd = dsRegister.Tables[0].Rows[0]["tm_regis_no"].ToString();
               if (tmpCd == "")
				tmpCd = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                insName = dsRegister.Tables[0].Rows[0]["tm_ins_fname"].ToString();
                insSurName = dsRegister.Tables[0].Rows[0]["tm_ins_lname"].ToString();
                insTel = dsRegister.Tables[0].Rows[0]["tm_ins_tel"].ToString();
                oldPolicyNo = dsRegister.Tables[0].Rows[0]["tm_old_policy"].ToString();
                if (!Convert.IsDBNull(dsRegister.Tables[0].Rows[0]["tm_prmm"]))
                    dPremium = Convert.ToDouble(dsRegister.Tables[0].Rows[0]["tm_prmm"].ToString());
                if (!Convert.IsDBNull(dsRegister.Tables[0].Rows[0]["tm_tax"]))
                    dTax = Convert.ToDouble(dsRegister.Tables[0].Rows[0]["tm_tax"].ToString());
                if (!Convert.IsDBNull(dsRegister.Tables[0].Rows[0]["tm_stamp"]))
                    dStamp = Convert.ToDouble(dsRegister.Tables[0].Rows[0]["tm_stamp"].ToString());
                dPrmm = Convert.ToDouble(dsRegister.Tables[0].Rows[0]["tm_prmmgross"].ToString()); //dPremium + dTax + dStamp;
                strPrmm = FormatStringApp.changeMoneyToWord(dPrmm.ToString());
            }
        }        
    }
}
