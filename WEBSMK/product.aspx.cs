﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;
using System.Collections;
 


public partial class product   : System.Web.UI.Page{

    //=================== Global Varible  
    protected string CatId { get { return  Request.QueryString["cat"]; } }
    
    
    protected void Page_Load(object sender, EventArgs e) { 

        if (!IsPostBack) { 
            //===== Set NamePage
            Utility obj = new Utility();
            string strPageName = obj.SetPagenameProducts(CatId);
            lblPageName.Text = strPageName; 
            BindData();   
        } 
    }
 

    protected void BindData() {
        //// Check  Querystring  To Show  Car Insurance
        if (CatId != null && CatId != "2") {  ////  All  Catagory  Insure 
             DataSet ds = SelectData(CatId );
             DataList0.DataSource = ds;
             DataList0.DataBind();
        } else if (CatId != null && CatId == "2" ){  ////  Catagory Car Insure 
            DataList[] arrDtl = { DataList1, DataList2, DataList3 }; // Array  of  DataList 
                int [] arrConType  = { 1 , 2 , 3   }; // Array of PrdConType

                for (int i = 0; i <= (arrDtl.Length - 1); i++) { 
                        DataSet ds = SelectData(CatId, arrConType[i]);
                        arrDtl[i].DataSource = ds;
                        arrDtl[i].DataBind();
                         
 
                }

        }else{
            //Response.Redirect("home.aspx");
        }
    }

    //=======  Load Data
    protected DataSet SelectData(string _CatId) {

        string sql1 = "SELECT    tbPrdCon.* , tbPrdCat.PrdCatName  "
                    + "FROM  tbPrdCon  "
                    + "LEFT JOIN tbPrdCat  ON  tbPrdCon.PrdCatId  =  tbPrdCat.PrdCatId  "
                    + "WHERE "
                    + "tbPrdCon.PrdConStatus ='1'  " // Check Status Content
                    + "AND  tbPrdCon.PrdCatId ='" + _CatId + "'  "
                    + "AND lang = '0' "  
                    + "AND  (GETDATE() between tbPrdCon.PrdConDtShow and tbPrdCon.PrdConDtHide )  "  // Check  Preriod Online 
                    + "ORDER BY   tbPrdCon.PrdConSort    ASC ,   tbPrdCon.PrdConId  DESC "; // Sort by  PrdConSort &PrdConId 
      
        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql1, "_tbPrdCon"); 
        return ds; 
    }

    protected DataSet SelectData(string _CatId, int _PrdConType) {

        string sql1 =   "SELECT    tbPrdCon.* , tbPrdCat.PrdCatName  "
                    + "FROM  tbPrdCon  "
                    + "LEFT JOIN tbPrdCat  ON  tbPrdCon.PrdCatId  =  tbPrdCat.PrdCatId  "
                    + "WHERE "
                    + "tbPrdCon.PrdConStatus ='1'  " // Check Status Content
                    + "AND  tbPrdCon.PrdCatId ='"+_CatId+"'  "
                    + "AND  tbPrdCon.PrdConType ='" + _PrdConType + "'  "
                    + "AND lang = '0' "  
                    + "AND  (GETDATE() between tbPrdCon.PrdConDtShow and tbPrdCon.PrdConDtHide )  "  // Check  Preriod Online 
                    + "ORDER BY  tbPrdCon.PrdConSort    ASC ,   tbPrdCon.PrdConId  DESC ";  // Sort by  PrdConSort &PrdConId 
  
        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql1, "_tbPrdCon");

        return ds;
    }

     
    protected void DataList_ItemDataBound(object sender, DataListItemEventArgs e) {
         if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem) {
             ///======= Set Globalvar
            string strId = DataBinder.Eval(e.Item.DataItem, "PrdConId").ToString();
             
            //========= Data Bound
            Label lblPrdTopic = (Label)e.Item.FindControl("lblPrdTopic");
            if (lblPrdTopic != null) {
                lblPrdTopic.Text = DataBinder.Eval(e.Item.DataItem, "PrdConTopic").ToString();
            }

            HyperLink hplPrdId = (HyperLink)e.Item.FindControl("hplPrdId");
            DataBinder.Eval(e.Item.DataItem, "PrdPicSmall").ToString();
            if (hplPrdId != null) {
            ////// Add Query String
                hplPrdId.NavigateUrl = "productView.aspx?cat=" + CatId + "&id=" + strId;
            }

            Image lblPrdPic = (Image)e.Item.FindControl("lblPrdPic");
            if (lblPrdPic != null)  { 
                //////  Set Show Pic
                string strPicSmall = DataBinder.Eval(e.Item.DataItem, "PrdPicSmall").ToString();
                if (strPicSmall != "") {
                    lblPrdPic.ImageUrl = strPicSmall;
                } else {
                    lblPrdPic.ImageUrl = "images/blank_photo_small.jpg";
                }
            }

            Label lblPrdContent = (Label)e.Item.FindControl("lblPrdContent");
            if (lblPrdContent != null)  {
                lblPrdContent.Text = DataBinder.Eval(e.Item.DataItem, "PrdConDetailShort").ToString();
            }

            HyperLink hplPrdView = (HyperLink)e.Item.FindControl("hplPrdView");
            if (hplPrdView != null)   { 
                ////// Add Query String
                hplPrdView.NavigateUrl = "productView.aspx?cat=" + CatId + "&id=" + strId; 
            }

            HyperLink hplPrdBuy = (HyperLink)e.Item.FindControl("hplPrdBuy");
            if (hplPrdBuy != null) {  
               string strPrdConId = DataBinder.Eval(e.Item.DataItem, "PrdConId").ToString();

                ////// Add Query String
                if (CatId == "1") { // Insure Health 
                    hplPrdBuy.NavigateUrl = "buyInsure.aspx?main_class=h";
                } else if (CatId == "2") { // Insure Car Voluntary
                    ///// Check PrdConId  for show hplPrdBuy
                    if (strPrdConId == "4" ) { 
                        hplPrdBuy.Visible = false;
                    } else {
                        hplPrdBuy.NavigateUrl = "buyVoluntary_step1.aspx";
                    }
                } else if (CatId == "3") { // Insure Cancer
                    hplPrdBuy.NavigateUrl = "buyInsure.aspx?main_class=c";
                } else if (CatId == "4") {// Insure Accident 
                    hplPrdBuy.NavigateUrl = "buyInsure.aspx?main_class=p";//"buyInsurePA_step1.aspx";
                } else if (CatId == "5") { // Insure Fire
                    hplPrdBuy.NavigateUrl = "buyInsure.aspx?main_class=f";
                } else if (CatId == "6") { // Insure Travel
                    hplPrdBuy.NavigateUrl = "buyInsure.aspx?main_class=p"; //"buyInsureTravel_step1.aspx";
                } else if (CatId == "7") { // Insure Other
                    hplPrdBuy.NavigateUrl = "buyInsure.aspx?main_class=o";
                } else if (CatId == "8") { // Insure Car Compulsary
                    hplPrdBuy.NavigateUrl = "buyCompulsary_step1.aspx";

                } else {
                    hplPrdBuy.NavigateUrl = "buyVoluntary_step1.aspx";
                }

            }

             



        } 


    } // End ItemDataBound

     
}