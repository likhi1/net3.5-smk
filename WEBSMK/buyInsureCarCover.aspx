﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="buyInsureCarCover.aspx.cs" Inherits="buyInsureCarCover" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>รายละเอียด ความคุ้มครอง</title>
    <link href="css/reset.css" rel="stylesheet" />
    <link href="css/font.css" rel="stylesheet" />
    <style>
#page-Carcover { font-size: 13px; }
#page-Carcover .subject1 { font-size: 23px; color: #0b36c9; margin: 8px 0px 5px 0px; font-family: 'RSULight'; clear: both; margin: 0px; border-bottom:1px solid  #ffde00 ; width: 600px ;height: 25px;margin-bottom:10px;  }
#page-Carcover table { border-collapse: collapse; border-spacing: 0; background-color: white; font-size: 13px; margin-bottom: 20px; }

#page-Carcover .title { color:#0b36c9;  }
#page-Carcover th { color: white; height: 20px; border: 1px solid white; background-color: #356DC2; font-family: 'RSULight'; font-size: 18px; }
#page-Carcover td { height: 20px; background-color: #fff;  border: 1px solid #ddd;  line-height: 20px; vertical-align: top;padding:   5px 10px; }
#page-Carcover ul {   padding-left: 30px; }
#page-Carcover ul li { list-style: disc; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="page-Carcover">
            <div class="subject1">รายละเอียด ความคุ้มครอง</div>
            <table style="width: 600px" class="schedule">
                <tr>
                    <th width="50%">ความคุ้มครอง</th>
                    <th width="50%">ความคุ้มครองตามเอกสารแนบท้าย</th>
                </tr>
                <tr>
                    <td><div class="title">ความเสียหายต่อร่างกาย หรืออนามัย</div>

                        <ul>
                            <li>
                                <asp:Label ID="lbltpbi1" runat="server"  ></asp:Label>
                                บาท/คน  
                            </li>
                            <li>
                                <asp:Label ID="lbltpbi2" runat="server"  ></asp:Label>
                                บาท/ครั้ง</li>
                        </ul>
                    </td>
                    <td><div class="title">เสียชีวิต สูญเสียอวัยวะ ทุพพลภาพ
                     </div>
                        <ul>
                            <li>ผู้ขับขี่  1  คน  
                                <asp:Label ID="lblperm_d_01" runat="server"  ></asp:Label>
                                บาท</li>
                            <li>ผู้โดยสาร 
                                <asp:Label ID="lblperm_p_num" runat="server"  ></asp:Label>
                                คน    
                                <asp:Label ID="lblperm_p_01" runat="server"  ></asp:Label>
                                บาท/คน </li>

                        </ul>
                         
                    </td>
                </tr>
                <tr>
                    <td><div class="title">ความเสียหายต่อทรัพย์สิน</div>
                     <ul>
                      <li><asp:Label ID="lbltppd" runat="server"  ></asp:Label> บาท/ครั้ง </li>
                     </ul>
                    </td>
                    <td><div class="title">ทุพพลภาพชั่วคราว</div> 
                        <ul>
                          <li>ผู้ขับขี่  1    คน   
                          <asp:Label ID="lbltemp_d_01" runat="server"  ></asp:Label>
                                บาท/สัปดาห์ </li>
                          <li>  ผู้โดยสาร  
                          <asp:Label ID="lbltemp_p_num" runat="server"  ></asp:Label>
                             คน                          
                          <asp:Label ID="lbltemp_p_01" runat="server"  ></asp:Label>
                            บาท/คน/สัปดาห์
                          </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="title">
                        ความเสียหายต่อตัวรถยนต์
                        </div>
                        <ul><li>
                            <asp:Label ID="lblCarOD" runat="server" ></asp:Label>บาท
						</li></ul>
                    </td>
                    <td><div class="title">ค่ารักษาพยาบาล</div>
						<ul><li>   <asp:Label ID="lblcover_02" runat="server"  ></asp:Label>บาท/คน 
						</li>   </ul>
                    </td>
                </tr>
                <tr>
                    <td><div class="title">ทุนสูญหาย/ไฟไหม้</div>
						<ul><li>     <asp:Label ID="lblCarFire" runat="server" ></asp:Label>บาท
						</li></ul> 
                    </td>
                    <td><div class="title">ประกันตัวผู้ขับขี่ </div>           
						<ul><li>  <asp:Label ID="lblcover_03" runat="server" > </asp:Label>บาท/คน 
						</li></ul>
                    </td>
                </tr>
                <tr>
                    <td><div class="title">ความเสียหายส่วนแรก</div>
						<ul><li>     <asp:Label ID="lblDeduct" runat="server" ></asp:Label>บาท/ครั้ง
						</li></ul> 
                    </td>
                </tr>
                </table>
        </div>



 



    </form>
</body>
</html>
