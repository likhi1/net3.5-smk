﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="buyInsureCar_step1_1.aspx.cs" Inherits="buyInsureCar_step1_1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <!------- Jquery colorbox ---------->
    <link rel="stylesheet" href="js/colorbox/example2/colorbox.css" />

    <script src="js/colorbox/colorbox/jquery.colorbox.js"></script>

    <script>
        $(function () {
            //$("#insure-rate").hide();
            //------- show  rate ----------//
            $("#btn-show-rate").click(function () {
                $("#insure-rate").slideDown();
            })
            //------- Jquery colorbox-----------//
            $(".iframe").colorbox({ iframe: true, width: "550", height: "430" });
        });
    </script>

<div   id="page-online-insCar" >
        <h1 class="subject1">  ซื้อประกัน-รถยนต์</h1>
        <div class="stepOnline"> <img src="images/app-step-1.jpg" /></div>
<div class="prc-row">
         
         
       
  <h2 class="subject-detail">โปรดกรอกรายละเอียดเพื่อเลือกแบบความคุ้มครอง<h2>
<asp:Button ID="Button11" runat="server" Text="TOYOTA" CssClass="btn-carbrand" />
<asp:Button ID="Button12" runat="server" Text="HONDA" CssClass="btn-carbrand" />
<asp:Button ID="Button15" runat="server" Text="NISSAN" CssClass="btn-carbrand" />
<asp:Button ID="Button14" runat="server" Text="MAZDA" CssClass="btn-carbrand" />
<asp:Button ID="Button13" runat="server" Text="ISUZU" CssClass="btn-carbrand" />
      </h2>


<table class="tb-online-insCar">
  <tr>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
  </tr>
  <tr>
    <td class="label">ยี้ห้อรถ : </td>
    <td><asp:DropDownList ID="DropDownList3" runat="server" > </asp:DropDownList>
      <span class="star">*</span></td>
    <td class="label">มีตารางกรมธรรม์เดิมหรือไม่ : </td>
    <td><asp:RadioButtonList ID="RadioButtonList1" runat="server"  RepeatDirection="Horizontal"  CssClass="tb-radioList"  >
        <asp:ListItem Value="true">มี</asp:ListItem>
        <asp:ListItem Value="false">ไม่มี</asp:ListItem>
      </asp:RadioButtonList>
      <span class="star">*</span></td>
  </tr>
  <tr>
    <td class="label">รุ่นรถ : </td>
    <td>  <asp:DropDownList ID="DropDownList4" runat="server">
                        </asp:DropDownList>
    <span class="star">*</span></td>
    <td class="label">บริษัทที่ทำประกันภัยอยู่    : </td>
    <td><asp:DropDownList ID="DropDownList1" runat="server">
                        </asp:DropDownList>
                        <span class="star">* </span></td>
  </tr>
  <tr>
    <td class="label">ประเภทรถ : </td>
    <td><asp:DropDownList ID="DropDownList5" runat="server">
                        </asp:DropDownList>
    <span class="star">*</span></td>
    <td class="label">ประเภทมี่ทำประกันภัยอยู่ : </td>
    <td><asp:DropDownList ID="DropDownList2" runat="server">
                        </asp:DropDownList>
                        <span class="star">*</span></td>
  </tr>
  <tr>
    <td class="label"> ปีจดทะเบียน : </td>
    <td><asp:DropDownList ID="DropDownList6" runat="server">
                        </asp:DropDownList>
    <span class="star">*</span></td>
    <td class="label"> ชื่อ : </td>
    <td><asp:TextBox ID="TextBox16" runat="server"></asp:TextBox>
                        <span class="star">*</span></td>
  </tr>
  <tr>
    <td class="label">ลักษณะการใช้รถ : </td>
    <td><asp:DropDownList ID="DropDownList7" runat="server">
                        </asp:DropDownList>
    <span class="star">*</span></td>
    <td class="label">สกุล : </td>
    <td><asp:TextBox ID="TextBox11" runat="server"></asp:TextBox>
                        <span class="star">*</span></td>
  </tr>
  <tr>
    <td class="label">ขับรถวันละกี่กิโลเมตร  :</td>
    <td> <asp:DropDownList ID="DropDownList10" runat="server">
                        </asp:DropDownList>
    <span class="star">*</span></td>
    <td class="label">  เพศ : </td>
    <td><asp:RadioButtonList ID="RadioButtonList6" runat="server" RepeatDirection="Horizontal"  CssClass="tb-radioList">
                                <asp:ListItem Value="true">หญิง</asp:ListItem>
                                <asp:ListItem Value="false">ชาย</asp:ListItem>
                            </asp:RadioButtonList>
                            <span class="star">*</span></td>
  </tr>
  <tr>
    <td class="label">ปรกติใช้รถใน : </td>
    <td>   
                            <asp:RadioButtonList ID="RadioButtonList5" runat="server"   RepeatDirection="Horizontal"  CssClass="tb-radioList">
                                <asp:ListItem Value="true">กรุงเทพฯ</asp:ListItem>
                                <asp:ListItem Value="false">ต่างจังหวัด</asp:ListItem>
      </asp:RadioButtonList>
                    
    <span class="star">*</span></td>
    <td class="label">อายุ : </td>
    <td><asp:DropDownList ID="DropDownList9" runat="server">
                        </asp:DropDownList>
                        <span class="star">*</span></td>
  </tr>
  <tr>
    <td class="label">ผู้ขับขี่ : </td>
    <td>  
                            <asp:RadioButtonList ID="RadioButtonList3" runat="server" RepeatDirection="Horizontal" CssClass="tb-radioList">
                                <asp:ListItem Value="true">ระบุผู้ขับ</asp:ListItem>
                                <asp:ListItem Value="false">ไม่ระบุผู้ขับ</asp:ListItem>
      </asp:RadioButtonList>
                        
    <span class="star">*</span></td>
    <td class="label">อีเมล์ : </td>
    <td><asp:TextBox ID="TextBox12" runat="server"></asp:TextBox>
                        <span class="star">*</span></td>
  </tr>
  <tr>
    <td class="label">ซื้อ พรบ. : </td>
    <td> 
                            <asp:RadioButtonList ID="RadioButtonList7" runat="server" RepeatDirection="Horizontal" CssClass="tb-radioList">
                                <asp:ListItem Value="true">ซื้อ</asp:ListItem>
                                <asp:ListItem Value="false">ไม่ซื้อ</asp:ListItem>
      </asp:RadioButtonList> 
    <span class="star">*</span></td>
    <td class="label">เบอร์โทรศัพท์มือถือ : </td>
    <td><asp:TextBox ID="TextBox15" runat="server"></asp:TextBox>
                        <span class="star">*</span></td>
  </tr>
  <tr>
    <td class="label">เลือกแบบมี Deduc : </td>
    <td> <asp:RadioButtonList ID="RadioButtonList2" runat="server"  RepeatDirection="Horizontal" CssClass="tb-radioList">
                                <asp:ListItem Value="true">ใช่</asp:ListItem>
                                <asp:ListItem Value="false">ไม่ใช่</asp:ListItem>
                            </asp:RadioButtonList>
                
    <span class="star">*</span></td>
    <td> </td>
    <td> </td>
  </tr>
  <tr>
    <td  valign="top"><div style="float: right;">
                            <asp:CheckBox ID="CheckBox1" runat="server" />
                        </div></td>
    <td colspan="3">   ข้าพเจ้ายินยอมให้บริษัทใช้หมายเลขโทรศัพท์ ที่อยู่ และ อีเมล์ ที่ให้ไว้ข้างต้น
                       <br /> ในการแจ้งผลการสมัคร ข่าวสาร หรืออื่นๆ  หรือการเสนอขายผลิตภัณฑ์ของบริษัทและคู่ค้า</td>
  </tr>

  <tr>
    <td> </td>
    <td colspan="2"><div style="margin: 0px auto; width: 190px; margin-top: 15px;">
                            <input id="btn-show-rate" type="button" class="btn-cal-th" />
                            <asp:Button ID="Button10" runat="server" CssClass="btn-edit-th" />
                        </div></td>
    <td> </td>
  </tr>
</table>

<br />
            <div id="insure-rate" > 

              
<!--*********** Start Overlapping Tabs  *****************--> 
<div id="tabOvl-onlineInsureCar">
<ul class="obtabs">
<li ><span><a href="buyInsureCar_step1.aspx">ชั้น 1</a></span></li> 
<li id="current" ><span><a href="buyInsureCar_step1_1.aspx">ชั้น 5/3</a></span></li>
<li ><span><a href="#">พิเศษ ชั้น 3</a></span></li>
</ul>
</div> 
<!--*********** End Overlapping Tabs  *****************-->





                <table style="width: 100%;" class="tb-prd-result">
                    <tr>
                        <th width="20%">
                            ผลิตภัณฑ์
                        </th>
                        <th width="10%">
                            ทุนประกัน
                        </th>
                        <th width="10%">
                              เบี้ยประกัน<br />
                            ซ่อมห้าง
                        </th>
                        <th width="5%">
                            คลิ๊กซื้อ
                        </th>
                        <th width="10%">
                            เบี้ยประกัน<br />
                            ซ่อมอู่
                        </th>
                        <th width="5%">
                            คลิ๊กซื้อ
                        </th>
                        <th>
                            ค่าเสียหาย<br />
                            ส่วนแรก
                        </th>
                        <th width="12%">
                            ความคุ้มครอง
                        </th>
                        <th width="10%">
                            หมายเหตุ
                        </th>
                    </tr>
                    <tr>
                        <td class="product-name">
                            
                            <a href="#">มาตราฐาน</a>
                           
                        </td>
                        <td>300,000 </td>
                        <td class="bgClame1">
                              20,000
                        </td>
                        <td class="bgClame1">
                            <asp:Button ID="Button1" runat="server" 
                            CssClass="btn-order-mini-th" 
                            OnClientClick="location='buyInsureCar_step2.aspx';return false; " />
                          
                        </td>
                        <td class="bgClame2">
                            18,000
                        </td>
                        <td class="bgClame2">
                        <asp:Button ID="Button2" runat="server" 
                            CssClass="btn-order-mini-th" 
                            OnClientClick="location='buyInsureCar_step2.aspx';return false; " />
                        </td>
                        <td>
                        </td>
                        <td>
                            <a class='iframe' href="buyOnlineInsCarRate.aspx">รายละเอียด</a>
                        </td>
                        <td>
                             <a class='iframe' href="buyOnlineInsCarVocher.aspx">Vocher</a> 
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                             
                        </td>
                        <td>300,000 </td>
                        <td class="bgClame1">
                              20,000
                        </td>
                        <td class="bgClame1">
                              <asp:Button ID="Button3" runat="server" 
                            CssClass="btn-order-mini-th" 
                            OnClientClick="location='buyInsureCar_step2.aspx';return false; " />
                        </td>
                        <td class="bgClame2">
                            18,000
                        </td>
                        <td class="bgClame2">
                             <asp:Button ID="Button4" runat="server" 
                            CssClass="btn-order-mini-th" 
                            OnClientClick="location='buyInsureCar_step2.aspx';return false; " />
                        </td>
                        <td>
                        </td>
                        <td>
                            <a class='iframe' href="insureRate.aspx">รายละเอียด</a>
                        </td>
                        <td>
                            <a class='iframe' href="buyOnlineInsCarVocher.aspx">Vocher</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             
                        </td>
                        <td>
                              300,000
                        </td>
                        <td class="bgClame1">
                              20,000
                        </td>
                        <td class="bgClame1">
                             <asp:Button ID="Button5" runat="server" 
                            CssClass="btn-order-mini-th" 
                            OnClientClick="location='buyInsureCar_step2.aspx';return false; " />
                        </td>
                        <td class="bgClame2">
                            18,000
                        </td>
                        <td class="bgClame2">
                             <asp:Button ID="Button6" runat="server" 
                            CssClass="btn-order-mini-th" 
                            OnClientClick="location='buyInsureCar_step2.aspx';return false; " />
                        </td>
                        <td>
                        </td>
                        <td>
                            <a class='iframe' href="insureRate.aspx">รายละเอียด</a>
                        </td>
                        <td>
                             <a class='iframe' href="buyOnlineInsCarVocher.aspx">Vocher</a>
                        </td>
                    </tr>
                    <tr>
                        <td class="break-line" colspan="9">
                        </td>
                    </tr>
                    <tr>
                        <td class="product-name">
                              <a href="#">ตามไมล์</a>
                        </td>
                        <td>300,000
                        </td>
                        <td class="bgClame1">
                              20,000
                        </td>
                        <td class="bgClame1">
                               <asp:Button ID="Button7" runat="server" 
                            CssClass="btn-order-mini-th" 
                            OnClientClick="location='buyInsureCar_step2.aspx';return false; " />
                        </td>
                        <td class="bgClame2">
                            18,000
                        </td>
                        <td class="bgClame2">
                             <asp:Button ID="Button8" runat="server" 
                            CssClass="btn-order-mini-th" 
                            OnClientClick="location='buyInsureCar_step2.aspx';return false; " />
                        </td>
                        <td>
                             
                        </td>
                        <td>
                            <a class='iframe' href="insureRate.aspx">รายละเอียด</a>
                        </td>
                        <td>
                              <a class='iframe' href="buyOnlineInsCarVocher.aspx">Vocher</a>
                        </td>
                    </tr>
                </table>
            </div>  <!-- End id="insure-rate"-->
            
     
        </div>
        <!-- End class="prc-row"-->
    </div>
    <!-- End <div class="context page-app"> -->   
</asp:Content>
