﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true"
    CodeFile="buyInsureCar_step2.aspx.cs" Inherits="buyInsureCar_step2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div  id="page-online-insCar">
        <h1 class="subject1">
            ทำประกันออนไลน์</h1>
        <div class="stepOnline">
            <img src="images/app-step-2.jpg" />
        </div>
        <div class="prc-row">
        
                 <h2 class="subject-detail">รายละเอียดประกันที่เลือก</h2>
            <table class="tb-online-insCar">
          
                <tr>
                    <td class="label"  >
                        ชนิดประกัน :
                    </td>
                    <td  >
                        มาตราฐาน
                    </td>
                    <td class="label"  >
                        ผู้ขับขี่ :
                    </td>
                    <td  >
                        ระบุผู้ขับขี่ / ไม่ระบุผู้ขับขี่
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        ประเภทความคุ้มครอง :
                    </td>
                    <td  >
                        ประเภท 3
                    </td>
                    <td class="label"  >
                        ชนิดเบี้ยประกัน :
                    </td>
                    <td  >
                        ซ่อมห้าง / ซ่อมอู่
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        ทุนประกัน :
                        <td  >
                            300,000 บาท
                        </td>
                        <td class="label"  >
                            เลือกแบบมี Deduc :
                        </td>
                        <td  >
                            ใช่ / ไม่ใช่
                        </td>
                </tr>
                <tr>
                    <td class="label">
                        เบี้ยประกัน :
                    </td>
                    <td  >
                        22,000 บาท
                    </td>
                    <td class="label"  >
                        ซื้อ พรบ. :
                    </td>
                    <td  >
                        ซื้อ / ไม่ซื้อ
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        วันที่เริ่มคุ้มครอง :
                        <td  >
                            <asp:DropDownList ID="DropDownList5" runat="server">
                            </asp:DropDownList>
                            <span class="star">* </sp* </span>
                        </td>
                        <td class="label"  >
                            <td  >
                                
                            </td>
                </tr>
                <tr>
                    <td colspan="4">
                    </td>
                </tr>
            </table>
            
             <h2 class="subject-detail">รายละเอียดรถยนต์</h2>
            <table class="tb-online-insCar">
                
                <tr>
                    <td class="label"  >
                        ยี้ห้อรถ :
                    </td>
                    <td  >
                        TOYOTA
                    </td>
                    <td class="label"  >
                        มีตารางกรมธรรม์เดิมหรือไม่ 
                            : 
                    </td>
                    <td  >
                        <div  >
                            มี / ไม่มี</div>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        รุ่นรถ :
                        </td>
                        <td>
                            ALTIS 
                        </td>
                        <td class="label"  >
                            บริษัทที่เคยทำประกันภัย
                                :
                        </td>
                        <td  >
                            xxx
                        </td>
                </tr>
                <tr>
                    <td class="label">
                        ประเภทรถ :
                        <td>
                            เก๋ง</tเก๋ง
                        </td>
                        <td class="label">
                            ประเภทที่เคยทำประกันภัย :
                        </td>
                        <td  >
                            xxx
                        </td>
                </tr>
                <tr>
                    <td class="label">
                        ปีจดทะเบียน :
                    </td>
                    <td>
                        2555
                    </td>
                    <td class="label">
                        เลขทะเบียนรถ :
                    </td>
                    <td  >
                        <asp:TextBox ID="TextBox22" runat="server"></asp:TextBox>
                        <span class="star">*</span>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        ลักษณะการใช้รถ :
                    </td>
                    <td class="style1">
                        รถเก๋ง ส่วนบุคคล
                    </td>
                    <td class="label">
                        จังหวัดที่จดทะเบียน :
                    </td>
                    <td  >
                        <asp:TextBox ID="TextBox18" runat="server"></asp:TextBox>
                        <span class="star">*</span>
                    </td>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        ขับรถวันละ :
                    </td>
                    <td>
                        xx กิโลเมตร
                    </td>
                    <td class="label">
                        ขนาดซีซีเครื่องยนต์ :
                    </td>
                    <td  >
                        <asp:TextBox ID="TextBox19" runat="server"></asp:TextBox>
                        <span class="star">*</span>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        ปรกติใช้รถใน :
                    </td>
                    <td>
                        กรุงเทพกรุงเทพฯ / ต่างจังหวัด
                    </td>
                    <td class="label">
                        เลขตัวถัง :
                    </td>
                    <td  >
                        <asp:TextBox ID="TextBox20" runat="server"></asp:TextBox>
                        <span class="star">*</span>
                    </td>
                </tr>
            </table>
           
             <h2 class="subject-detail">รายละเอียดผู้เอาประกัน</h2>
            <table class="tb-online-insCar">
              
                <tr>
                    <td class="label"  >
                        ชื่อ :
                    </td>
                    <td  >
                        xxxxxxxxxx
                    </td>
                    <td class="label">
                        ที่อยู่ :
                    </td>
                    <td  >
                        <asp:TextBox ID="TextBox25" runat="server"></asp:TextBox>
                        <span class="star">*</span>
                    </td>
                </tr>
                <tr>
                    <td class="label"  >
                        นามสกุล
                            :
                    </td>
                    <td  >
                        xxxxxxxxxx
                    </td>
                    <td class="label"  >
                        ตำบล / แขวง :
                    </td>
                    <td  >
                        <asp:TextBox ID="TextBox26" runat="server"></asp:TextBox>
                        <span class="star">*</span>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        เบอร์โทรศัพท์มือถือ :
                    </td>
                    <td>
                        xxxxxxxxxx
                    </td>
                    <td class="label"  >
                        
                            อำเภอ / เขต :
                    </td>
                    <td  >
                        <asp:TextBox ID="TextBox27" runat="server"></asp:TextBox>
                        <span class="star">*</span>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        เบอร์โทรศัพท์บ้าน :
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox23" runat="server"></asp:TextBox>
                        <span class="star">*</span>
                    </td>
                    <td class="label">
                        จังหวัด :
                    </td>
                    <td  >
                        <asp:TextBox ID="TextBox28" runat="server"></asp:TextBox>
                        <span class="star">*</span>
                    </td>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        อีเมล์ :
                    </td>
                    <td>
                        xxx
                    </td>
                    <td class="label">
                        รหัสไปรษณีย์ :
                    </td>
                    <td  >
                        <asp:TextBox ID="TextBox29" runat="server"></asp:TextBox>
                        <span class="star">*</span>
                    </td>
                </tr>
            </table>
            <div style="margin: 0px auto; width: 190px; margin-top: 15px;">
                <asp:Button ID="Button9" runat="server"  class="btn-default" style="margin-right:10px;" Text="ยืนยัน"  OnClientClick="window.location='buyInsureCar_step3.aspx' ;return false;" />
                <asp:Button ID="Button10" runat="server"  class="btn-default" Text="ย้อนกลับ"   OnClientClick="history.back();return false;"/>
 


            </div>
        </div>
        <!-- End class="prc-row"-->
    </div>
    <!-- End     <div class="context page-app"> -->
</asp:Content>
