﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class SendBack : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        RegisterManager cmRegister = new RegisterManager();
        DataSet dsRegister;
        string strParam = "";
        string strPage = "";
        dsRegister = cmRegister.getTmpRegByRegNo(Request.QueryString["regisNo"].ToString());
        TDS.Utility.MasterUtil.writeLog("SENDBACK : regis_no", Request.QueryString["regisNo"]);
        string[] strKeys = Request.Form.AllKeys;
        for (int iTmp = 0; iTmp <= strKeys.Length - 1; iTmp++)
        {
            TDS.Utility.MasterUtil.writeLog("SENDBACK : " + strKeys[iTmp],  Request.Form[strKeys[iTmp]]);
        }        
        // ทำการ update ผลการตัดบัตรเครดิต
        string strApproved = Request.Form["HOSTRESP"].ToString();
        if (strApproved != null && strApproved != "")
        {
            cmRegister.updateCreditResult(Request.QueryString["regisNo"], strApproved);
        }
        if (dsRegister.Tables.Count > 0 && 
            dsRegister.Tables[0].Rows.Count > 0 &&
            Request.QueryString["regisNo"].Replace("-", "") == Request.Form["RETURNINV"].Substring(5))
        {
            if (dsRegister.Tables[0].Rows[0]["tm_main_class"].ToString() == "M")
            {
                if (dsRegister.Tables[0].Rows[0]["tm_pol_type"].ToString() == "C")
                    strPage = "buyCompulsary_step3.aspx";
                else
                {
                    strPage = "buyVoluntary_step4.aspx";
                }
            }
            else if (dsRegister.Tables[0].Rows[0]["tm_main_class"].ToString() == "P")
            {
                strPage = "buyPA_step3.aspx";
            }
            else if (dsRegister.Tables[0].Rows[0]["tm_main_class"].ToString() == "T")
            {
                strPage = "buyInsureTravel_step3.aspx";
            }
        }
        else
        {
            Literal1.Text = "<script language='javascript'>alert('ท่านไม่สามารถชำระผ่านบัตรเครดิตได้ กรุณาทำรายการใหม่');</script>";
            strPage = "home.aspx";
        }
        strParam = "?regis_no=" + Request.QueryString["regisNo"].ToString() + "&paid_at=4";
        TDS.Utility.MasterUtil.writeLog("SENDBACK : " + Request.QueryString["regisNo"] + ":" + strApproved, strPage + strParam);
        Literal1.Text += "<script language='javascript'>document.location='./" + strPage + strParam + "';</script>";
        Session.Remove("regisNo");
        Session.Remove("creditResponse");
        Session.Remove("FROM");        
    }
}
