﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---------- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;
using System.Collections;



public partial class faq : System.Web.UI.Page
{ 

    protected void Page_Load(object sender, EventArgs e) {
        if (!Page.IsPostBack) {
            BindData();
           
        }

    }

    protected void BindData() {
        DataSet ds = SelectData();
        DataList1.DataSource = ds;
        DataList1.DataBind();
    }

    protected DataSet SelectData() {
        string sql = "SELECT * FROM tbFaq "
                    + "WHERE "
                    + "FaqStatus = '1' "
                    + "AND lang = '0' " 
                    + "ORDER BY  FaqSort ASC ,  FaqId DESC ";


        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql, "tb");
        return ds;
    }





    protected void DataList1_ItemDataBound(object sender, DataListItemEventArgs e) {

        Label lblQuiz = (Label)(e.Item.FindControl("lblQuiz"));
        if (lblQuiz != null) {
            lblQuiz.Text = DataBinder.Eval(e.Item.DataItem, "FaqQuiz").ToString();
            lblQuiz.Attributes.Add("style", "float:left");

        }

        Label lblAnswer = (Label)(e.Item.FindControl("lblAnswer"));
        if (lblAnswer != null) {
            lblAnswer.Text = DataBinder.Eval(e.Item.DataItem, "FaqAnswer").ToString();
            lblAnswer.Attributes.Add("style", "float:left");

        }
         

 


    }




}
