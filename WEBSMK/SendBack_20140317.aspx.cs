﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class SendBack : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        RegisterManager cmRegister = new RegisterManager();
        DataSet dsRegister;
        string strParam = "";
        string strPage = "";
        dsRegister = cmRegister.getTmpRegByRegNo(Request.QueryString["regisNo"].ToString());
        // ทำการ update ผลการตัดบัตรเครดิต
        if (Request.QueryString["credit"] != null)
        {
            cmRegister.updateCreditResult(Request.QueryString["regisNo"], Request.QueryString["credit"]);
        }
        if (Request.QueryString["credit"].ToString().ToLower() == "approved")
        {
            if (dsRegister.Tables.Count > 0 && dsRegister.Tables[0].Rows.Count > 0)
            {
                if (dsRegister.Tables[0].Rows[0]["tm_main_class"].ToString() == "M")
                {
                    if (dsRegister.Tables[0].Rows[0]["tm_pol_type"].ToString() == "C")
                        strPage = "buyCompulsary_step3.aspx";
                    else
                    {
                        strPage = "buyVoluntary_step4.aspx";
                    }
                }
                else if (dsRegister.Tables[0].Rows[0]["tm_main_class"].ToString() == "P")
                {
                    strPage = "buyPA_step3.aspx";
                }
                else if (dsRegister.Tables[0].Rows[0]["tm_main_class"].ToString() == "T")
                {
                    strPage = "buyInsureTravel_step3.aspx";
                }
            }
            else
            {
                strPage = "DisplayPayment.aspx";
            }
            strParam = "?regis_no=" + Session["regisNo"].ToString() + "&paid_at=4";
            Literal1.Text = "<script language='javascript'>document.location='./" + strPage + strParam + "';</script>";
            Session.Remove("regisNo");
            Session.Remove("creditResponse");
            Session.Remove("FROM");
        }
        else
        {
            // error from scb
            if (dsRegister.Tables.Count > 0 && dsRegister.Tables[0].Rows.Count > 0)
            {
                if (dsRegister.Tables[0].Rows[0]["tm_main_class"].ToString() == "M")
                {
                    if (dsRegister.Tables[0].Rows[0]["tm_pol_type"].ToString() == "C")
                    {
                        strParam = "?pol_type=C";
                    }
                    else
                    {
                        strParam = "?pol_type=V";
                    }
                }
                else if (dsRegister.Tables[0].Rows[0]["tm_main_class"].ToString() == "P")
                {
                    strParam = "?pol_type=P";
                }
                else if (dsRegister.Tables[0].Rows[0]["tm_main_class"].ToString() == "T")
                {
                    strParam = "?pol_type=T";
                }
                strParam += "&tmp_cd=" + dsRegister.Tables[0].Rows[0]["tm_tmp_cd"].ToString();
            }
            if (dsRegister.Tables.Count > 0 && dsRegister.Tables[0].Rows.Count > 0)
            {
                if (dsRegister.Tables[0].Rows[0]["tm_main_class"].ToString() == "M")
                {
                    if (dsRegister.Tables[0].Rows[0]["tm_pol_type"].ToString() == "C")
                        strPage = "buyCompulsary_step2.aspx";
                    else
                    {
                        strPage = "buyVoluntary_step3.aspx";
                    }
                }
                else if (dsRegister.Tables[0].Rows[0]["tm_main_class"].ToString() == "P")
                {
                    strPage = "buyPA_step2.aspx";
                }
                else if (dsRegister.Tables[0].Rows[0]["tm_main_class"].ToString() == "T")
                {
                    strPage = "buyInsureTravel_step2.aspx";
                }
            }
            //strPage = "Paid.aspx";
            Literal1.Text = "<script language='javascript'>alert('ท่านไม่สามารถชำระผ่านบัตรเครดิตได้ กรุณาเลือกวิธีชำระเงินใหม่');document.location='./" + strPage + strParam + "';</script>";
            //UcError1.showMessage("ท่านไม่สามารถชำระผ่านบัตรเครดิตได้ กรุณาเลือกวิธีชำระเงินใหม่");
            Session.Remove("regisNo");
            Session.Remove("creditResponse");
        }
    }
}
