﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true"   
     CodeFile="buyInsureCar_step1.aspx.cs" Inherits="buyInsureCar_step1"    EnableEventValidation="false"   %> 

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">  
 
   
    <!-- ===============  Date  Picker  -->
    <script>
        $(function () {
             $("[id*=DatePicker]").attr('readOnly', 'true');

            $("[id*=DatePicker]").datepicker({
                /////  Change year /////
                changeMonth: true,
                changeYear: true,
                ///////  Limit Year  //////
                //yearRange: "1901:2012", // Limit By Define Year
                //yearRange: "-60:+0",
                yearRange: "-100:-18", //18 years or older up until 100  (oldest person ever, can be sensibly set to something much smaller in most cases)
                maxDate: "-18Y", //Will only allow the selection of dates more than 18 years ago, useful if you need to restrict this
                minDate: "-100Y",
                ////// Budda  Year //////
                isBE: true,
                autoConversionField: false

               // onClose: function (dateText, inst) {
                //    $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');
              //  } 
            })

        });
    </script>

      <!-- ===============  Modal  --> 
      <script>
          $(function () { 
              
              $("[id*=dialog]").dialog({
                  autoOpen: false,
                  show: "fade",
                  hide: "fade",
                  modal: true,
                  //open: function (ev, ui) {
                  //    $('#myIframe').src = 'http://www.google.com';
                  //},
                  height: '500',
                  width: '600',
                  resizable: true,
                  //title: 'Title Topic'
              });

              coverIframe = $("#coverIframe");
              $(".openerCover").click(function (e) {
                  e.preventDefault();

                  link =  $(this).attr('href');
                  coverIframe.attr('src', link);

                  $("#dialog1").dialog("open"); 
               //   return false;
              })
               

              voucherIframe = $("#voucherIframe");
              $(".openerVoucher").click(function (e) {
                  e.preventDefault();

                  link = $(this).attr('href');
                  voucherIframe.attr('src', link);

                  $("#dialog2").dialog("open");
                  //   return false;
              })
         });
    </script>
   
   <!-- =============================== Global Vaiable   ======================== --> 
    <script >
        $(function () {
            /////// Varible - Ajax 
            loc = '<%= Request.ApplicationPath %>';
            rblCarCat = $("input[name=carCat]");
            ddlCarName = $("#<%=ddlCarName.ClientID%>");
            ddlCarNameVal = $("#<%=ddlCarName.ClientID%>").val();
            ddlCarMark = $("#<%=ddlCarMark.ClientID%>");
            ddlCarMarkVal = $("#<%=ddlCarMark.ClientID%>").val();
            txtCarCc = $("#<%=txtCarCc.ClientID%>");  
            ddlCarAge = $("#ddlCarAge"); 
             
            ///// Varible - calulation od_min & od_max
            hdlOdMin = $('#hdlOdMin');
            hdlOdMax = $('#hdlOdMax'); 
            hdlOdMinDisc = $('#hdlOdMinDisc');
            hdlOdMaxDisc = $('#hdlOdMaxDisc');
            hdlCarAge = $('#hdlCarAge');
            

            ///// Varible Form
            rblCarDriver = $("input:radio[id*=rblCarDriver]");
            rblCarDriver0 = $("input:radio[id*=rblCarDriver][value=0]");
            rblCarDriver1 = $("input:radio[id*=rblCarDriver][value=1]");
            DatePickerDriver1 = $("#DatePickerDriver1");
            DatePickerDriver2 = $("#DatePickerDriver2");
            tbxRegName = $("#<%=tbxRegName.ClientID%>");
            tbxRegSur = $("#<%=tbxRegSur.ClientID%>");
            tbxRegAge = $("#<%=ddlRegAge.ClientID%>");
            tbxRegEmail = $("#<%=tbxRegEmail.ClientID%>");
            tbxRegMobile = $("#<%=tbxRegMobile.ClientID%>"); 
        })


     
        </script>  
 
    <!-- ==================================== Onload  ======================== --> 
    <script  >
        $(function () {
  
            ////// Set CarDiver  CarMark
            if ($("input:radio[id*=rblCarDriver][value=1]").is(':checked')) {
                $("#areaCarDriverAge").slideDown();
            }

            ////// Set First Time - CarCode  & CarType 
            $("#hdlCarcode").val(110);
            $("#hdlCarType").val("K");
            $("input[name=carCat]").click(function () {
                RblCarCatChange();
            })

          

            ////// Show & Hide  Define   Driver Age  
            $("[id*=rblCarDriver]").click(function () {
                //DatePickerDriver1.removeAttr('value');
              rdoVal =  $("[name*=rblCarDriver]:checked").val(); 
             // console.log(rdoVal);
              if (rdoVal == 1) {
                  $("#areaCarDriverAge").slideDown();
              } else {
                  $("#areaCarDriverAge").slideUp();
              }
            })
             

            ////// Set First Time -   CarMark
            ddlCarMark.attr("disabled", "disabled");
             

            ////// Populate Cacade DropDown
            ddlCarName.change(function () {
                PopulateCarMark();
            })
              
            //////  Set  CarMark & CarReg Hidden Input On Change 
            ddlCarMark.change(function () {
                ///// Assign  groupCode , od_min , od_max
                PopulateCarGroupCode(); 
            })

           

        }); // end On Load

       
         

    </script> 


    <!-- =============================== Cal   CarAge , OdMin , OdMax ======================== -->  
    <script> 
        ////////  Set car_year , od_min , od_max  ( on ddlCarAge change  )
        $(function () {  
            ddlCarAge.change(function () {
                regYear = GetRegYear(ddlCarAge.val());
                hdlCarAge.val(regYear);
                discMin = GetDiscMin(regYear, hdlOdMin.val());
                discMax = GetDiscMax(regYear, hdlOdMax.val());
                hdlOdMinDisc.val(discMin);
                hdlOdMaxDisc.val(discMax);
            })  
        }); // end On Load 

        function GetRegYear(_yearSelect) {
            if (_yearSelect != 0) {
                var yearNow = new Date().getFullYear();
                var regYear = yearNow - (_yearSelect - 543);
                if (regYear > 10) {
                    regYear = 99;
                }
            } else {
                var regYear = 0;
            }
            return regYear;
        }

        function GetDiscMin( _regYear , _odMin) { 
            for( i = 1 ;  i<= _regYear ; i++ ){
                odMinNew = _odMin - ((_odMin * 5)/100 ) ; 
                _odMin =  odMinNew; 
            }
            return _odMin;
        }

        function GetDiscMax(_regYear, _odMax) {
            for (i = 1 ; i <= _regYear ; i++) {
                odMaxNew = _odMax - ((_odMax * 5) / 100);
                _odMax = odMaxNew;
            }
            return _odMax;
        }
         
    </script>  


       <!-- =============================== Validate ======================== --> 
    <script>

        $(function () { 
            ///// Froce type Only Number 
            txtCarCc.keypress(validateNumber); 
            ///// Validate On Submit 
            $("#<%=btnInsrate.ClientID%>").click(function () {
                return Validate();
            }) 
        });

        //////// Check Number
        function validateNumber(event) {
            var key = window.event ? event.keyCode : event.which;
            if (event.keyCode == 8 || event.keyCode == 46
             || event.keyCode == 37 || event.keyCode == 39) {
                return true;
            }
            else if (key < 48 || key > 57) {
                return false;
            }
            else return true;
        }

        /////////// Validate All
        function Validate() {
            if (ddlCarName.val() == "0" || ddlCarName.val() == null) {
                alert("กรุณาระบุ ยี้ห้อรถยนต์"); return false;
            } else if (ddlCarMark.val() == "0" || ddlCarMark.val() == null) {
                alert("กรุณาระบุ รุ่นรถยนต์"); return false;
            } else if (ddlCarAge.val() == "0" || ddlCarAge.val() == null) {
                alert("กรุณาระบุ ปีจดทะเบียน"); return false;

            } else if (($("[name*=rblCarDriver]:checked").val() == 1) && (DatePickerDriver1.val() == "")) {
                //  } else if (( $("[name*=rblCarDriver]:checked").val() == 1 ) ) {  
                alert("กรุณาระบุ วันเกิดของผู้ขับขี่ด้วย"); return false;

            }

            //else if (txtCarCc.val() == "" || txtCarCc.val() == null) {
            //    alert("กรุณาระบุ เครื่องยนต์"); return false;
            //} else if (tbxRegName.val() == "") {
            //    alert("กรุณาระบุ ชื่อผู้ซื้อ"); return false;
            //} else if (tbxRegSur.val() == "") {
            //    alert("กรุณาระบุ นามสกุลผู้ซื้อ"); return false;
            //} else if (tbxRegAge.val() == "") {
            //    alert("กรุณาระบุ อายุผู้ซื้อ"); return false;
            //} else if (tbxRegEmail.val() == "") {
            //    alert("กรุณาระบุ อายุผู้ซื้อ"); return false;
            //} else if (tbxRegMobile.val() == "") {
            //    alert("กรุณาระบุ อีเมล์ผู้ซื้อ"); return false;
            //}

            return true;
        }



    </script>
     
    <!-- ====================================== AJAX  ===============================  --> 
    <!-- ===============  (AJAX)  Change  RblCarCat  -->
    <script>
        function RblCarCatChange() {
            var carCatVal = $("input[name=carCat]:checked").val();
            var _CarCode = SetCarCodeVal(carCatVal);
            var _CarType = ManageCarCat(carCatVal);
            $("#hdlCarcode").val(_CarCode);
            $("#hdlCarType").val(_CarType);
            console.log(_CarCode);
            console.log(_CarType); 

            //var rblCarCatVal = ManageCarCat($("input[name=carCat]:checked").val());
            var ddlCarName = $("#<%=ddlCarName.ClientID%>");
            var ddlCarNameVal = $("#<%=ddlCarName.ClientID%>").val();
            var ddlCarMark = $("#<%=ddlCarMark.ClientID%>");
            console.log(ddlCarNameVal);

           // ManageCarUse();

            if (ddlCarNameVal == 0) {

            } else {
                ddlCarMark.empty().append('<option selected="selected" value="0">Loading..</option>');
                var loc = '<%= Request.ApplicationPath %>';
                $.ajax({
                    type: "POST",
                    url: loc + '/buyInsureCar_step1.aspx/RblCarCatChange',
                    data: "{carnamcodId: '" + ddlCarNameVal + "', carcatId: '" + _CarType + "' }",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: OnRblCarCatPopulated,
                    error: function (response) {
                     //   alert("Error OR No respond ");
                     //   alert(response.d);
                    }
                });
            }

            ddlCarName.val(ddlCarNameVal).attr('selected', true);
            // clear data
            $("#hdlCarMarkcode").val(''); 

        }

        ///// Work with Ajax  Response
        function OnRblCarCatPopulated(response) {
            //alert("success");
            //alert(response.d);
            PopulateRblCarCat(response.d, $("#<%=ddlCarMark.ClientID %>"));
        }
        ///// Assign Option in Dropdown
        function PopulateRblCarCat(list, control) {
            if (list.length > 0) {
                control.removeAttr("disabled");
                control.empty().append('<option selected="selected" value="0">-- กรุณาระบุ --</option>');
                $.each(list, function () {
                    control.append($("<option></option>").val(this['Value']).html(this['Text']));
                });
            }
            else {
                control.empty().append('<option selected="selected" value="">-- ไม่มีข้อมูล --</option>');
                control.attr("disabled", "disable");
            }
        }

    </script>
      
    <!-- ===============  (AJAX) Set CarMark   -->
    <script>
        function PopulateCarMark() {

            var rblCarCatVal = $("input[name=carCat]:checked").val();
            var carCatVal = ManageCarCat(rblCarCatVal);

            var ddlCarName = $('#<%=ddlCarName.ClientID%>');
            var ddlCarMark = $("#<%=ddlCarMark.ClientID%>");
            console.log(ddlCarName.val());
            console.log(carCatVal);
             

            // ddlCarMark.attr("disabled", "disabled");
            if (ddlCarName.val() == "0") {
                //ddlCarMark.attr("disabled", "disabled");
                ddlCarMark.empty().append('<option selected="selected" value="0">-- กรุณาระบุ --</option>');
            }
            else {
                ddlCarMark.empty().append('<option selected="selected" value="0">Loading...</option>');
                var loc = '<%= Request.ApplicationPath %>';
                $.ajax({
                    type: "POST",
                    url: loc + '/buyInsureCar_step1.aspx/PopulateCarMark',
                    data: "{ carNameId : '" + ddlCarName.val() + "' , carCatVal:'" + carCatVal + "' }",

                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: OnCarMarkPopulated,
                    error: function (response) {
                        //alert("Error OR No respond ");
                        //alert(response.d);
                    }
                });
            }
        }
        ////// Work with Ajax  Response
        function OnCarMarkPopulated(response) {
            PopulateControl(response.d, $("#<%=ddlCarMark.ClientID %>"));
        }
        ///// Assign Option in Dropdown
        function PopulateControl(list, control) {
            if (list.length > 0) {
                control.removeAttr("disabled");
                control.empty().append('<option selected="selected" value="0">-- กรุณาระบุ --</option>');
                $.each(list, function () {
                    control.append($("<option></option>").val(this['Value']).html(this['Text']));
                });
            }
            else {
                control.empty().append('<option selected="selected" value="">-- ไม่มีข้อมูล --</option>');
            }
        }



    </script> 

     <!-- ===============  (AJAX) Set CarMarkCode , CarGroupCode , OdMin , OdMax , CarCc-->
    <script>
           function PopulateCarGroupCode() {
               var carmakcodIdVal = $("#<%=ddlCarMark.ClientID%>").val();
            console.log(carmakcodIdVal);

            var loc = '<%= Request.ApplicationPath %>';
            $.ajax({
                type: "POST",
                url: loc + '/buyInsureCar_step1.aspx/PopulateCarGroupCode',
                data: "{ carmakcodId : '" + carmakcodIdVal + "'  }",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    $("#hdlCarMarkcode").val(carmakcodIdVal);
                    /////// Get Value From JSON Data
                    var obj = jQuery.parseJSON(response.d); 
                    $.each(obj, function(key, val){ 
                        //console.log(val.group_code, val.od_max, val.od_min);
                       
                        $("#hdlCarGroup").val(val.group_code);
                        $("#hdlOdMin").val(val.od_min);
                        $("#hdlOdMax").val(val.od_max);
                        txtCarCc.val(val.car_cc);
                    })

                    ////////  Set car_year , od_min , od_max  ( on ddlCarMark change  )
                    regYear = GetRegYear(ddlCarAge.val());
                    hdlCarAge.val(regYear);
                    discMin = GetDiscMin(regYear, hdlOdMin.val());
                    discMax = GetDiscMax(regYear, hdlOdMax.val());
                    hdlOdMinDisc.val(discMin);
                    hdlOdMaxDisc.val(discMax);
 

                    console.log(response.d);
                },
                error: function (response) {
                    // alert(response.d);
                }
            });
        }

    </script>  

    <!-- ====================================  Share Function  ===============================  -->
    <script >
        //////// Check CarCode To Compare
        function SetCarCodeVal(prCarCatVal) {
            var CarCat;
            if (prCarCatVal == 1) {
                CarCode = "110";
            } else if (prCarCatVal == 2) {
                CarCode = "210";
            } else if (prCarCatVal == 3) {
                CarCode = "320";
            } else if (prCarCatVal == 4) {
                CarCode = "210";
            }
            return CarCode;
        }

        //////// Check CarCat To Compare
        function ManageCarCat(prCarCat) {
            var CarCat;
            if (prCarCat == 1) {
                CarCat = "K";
            } else if (prCarCat == 2) {
                CarCat = "P";
            } else if (prCarCat == 3) {
                CarCat = "P";
            } else if (prCarCat == 4) {
                CarCat = "V";
            }
            return CarCat;
        }

     
        //////// Manage Button  BranName  Toyota , Honda , Nissan , Mazda ,Isuzu
        function ManageHotLink(carMakcod){ 
            ddlCarName.val(carMakcod).attr("selected", "selected");
            ddlCarMark.removeAttr("Disabled");
            $("#hdlCarMarkcode").val(''); 
            PopulateCarMark();

            ddlCarMark.removeAttr("disabled");
            if (ddlCarMark.val() == 0) {
                ddlCarMark.attr("disabled", "disable");
            } 
        } 
   
   </script>

    <!-- =================================== NO USE ===================================  -->
    <script >
            function ManageCarType() {
                var rblCarCatVal = $('input[name=carCat]:checked').val();
                var ddlCarType = $("#ddlCarType");
                ddlCarType.empty();

                if (rblCarCatVal == 1) {
                    ddlCarType.append('<option selected="selected" value="0">-- กรุณาระบุ --</option>');
                    ddlCarType.append('<option  value="1">รถยนต์นั่ง</option>');
                } else if (rblCarCatVal == 2) {
                    ddlCarType.append('<option selected="selected" value="0">-- กรุณาระบุ --</option>');
                    ddlCarType.append('<option  value="2">รถโดยสาร</option>');
                    ddlCarType.append('<option   value="3">รถบรรทุก</option>');
                } else if (rblCarCatVal == 3) {
                    ddlCarType.append('<option selected="selected" value="0">-- กรุณาระบุ --</option>');
                    ddlCarType.append('<option   value="3">รถบรรทุก</option>');
                } else if (rblCarCatVal == 4) {
                    ddlCarType.append('<option selected="selected" value="0">-- กรุณาระบุ --</option>');
                    ddlCarType.append('<option   value="2">รถโดยสาร</option>');
                }
            }

            function ManageCarUse() {
                var rblCarCatVal = $('input[name=carCat]:checked').val();
                var ddlCarUse = $("#ddlCarUse");
                ddlCarUse.empty();
                ddlCarUse.removeAttr("disabled");

                if (rblCarCatVal == 1) {
                    ddlCarUse.append('<option selected="selected" value="0">-- กรุณาระบุ --</option>');
                    ddlCarUse.append('<option   value="1">ส่วนบุคคล</option>');
                } else if (rblCarCatVal == 2) {
                    ddlCarUse.append('<option selected="selected" value="0">-- กรุณาระบุ --</option>');
                    ddlCarUse.append('<option   value="1">ส่วนบุคคล</option>');
                    ddlCarUse.append('<option   value="2">เพื่อการพาณิชย์</option>');
                } else if (rblCarCatVal == 3) {
                    ddlCarUse.append('<option selected="selected" value="0">-- กรุณาระบุ --</option>');
                    ddlCarUse.append('<option   value="2">เพื่อการพาณิชย์</option>');
                } else if (rblCarCatVal == 4) {
                    ddlCarUse.attr("disabled", "disable");
                    ddlCarUse.append('<option selected="selected" value="0">-- ไม่ต้องระบุ --</option>');
                }
            }

    </script>
     
 

    
     
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div id="page-online-insCar"> 
           <h1 class="subject1">ซื้อประกัน-รถยนต์</h1>
        <div class="stepOnline">
            <img src="images/app-step-1.jpg" />
        </div>

        <h2 class="subject-detail">โปรดกรอกรายละเอียดเพื่อเลือกแบบความคุ้มครอง</h2> 

        <div style="height :35px; ">

             <a class="btn-carbrand"  btn-iconOrder"  onclick='ManageHotLink("TO")' >TOYOTA</a>
            <a class="btn-carbrand"  btn-iconOrder"  onclick='ManageHotLink("HO")' >HONDA</a>
            <a class="btn-carbrand"  btn-iconOrder"  onclick='ManageHotLink("NI")' >NISSAN</a>
            <a class="btn-carbrand"  btn-iconOrder"  onclick='ManageHotLink("MA")' >MAZDA</a>
            <a class="btn-carbrand"  btn-iconOrder"  onclick='ManageHotLink("IS")' >ISUZU</a>
 
</div>

            <div id="result" style="color: red;"></div>

        <table   class="tb-online-insCar">
           
            <tr>
                <td  width="130px"  class="label"  >ชนิดรถ :</td>
                <td><input type="radio" value="1" id="rblCarCat1" name="carCat" checked   />เก๋ง  
<input type="radio" value="2" id="rblCarCat2" name="carCat"     />กระบะส่วนบุคคล
<input type="radio" value="3" id="rblCarCat3" name="carCat"  />กระบะบรรทุก 
<input type="radio" value="4" id="rblCarCat4" name="carCat"  />ตู้ส่วนบุคคล
 
<input type="hidden" ID="hdlCarcode" name="hdlCarcode" />  
<input type="hidden" ID="hdlCarType" name="hdlCarType" />  
                </td>
              
            </tr>
            
        </table>
       
            <table width="630px">
                <tr>
                    <td>
                        <table class="tb-online-insCar">
                            
                   
                            <tr>
                                <td class="label"   width="130px"  >ยี้ห้อรถ : </td>
                                <td> 
                                    <asp:DropDownList ID="ddlCarName" runat="server" AppendDataBoundItems="true" AutoPostBack="true">
                                        <asp:ListItem Text="-- กรุณาระบุ --" Value="0"></asp:ListItem>
                                    </asp:DropDownList>

                                    <span class="star">*</span></td>
                            </tr>
                            <tr>
                                <td class="label">รุ่นรถ : </td>
                                <td>

                                    <asp:DropDownList ID="ddlCarMark" runat="server">
                                        <asp:ListItem Text="-- กรุณาระบุ --" Value="0"></asp:ListItem>
                                    </asp:DropDownList>
                                    <span class="star">*</span>

                                    <input  type="hidden" id="hdlCarMarkcode"  name="hdlCarMarkcode" /> 
                                    <input  type="hidden" id="hdlCarGroup"  name="hdlCarGroup" />
                                    <input  type="hidden" id="hdlCarAge"  name="hdlCarAge" />
                                    <input  type="hidden" id="hdlOdMin"  name="hdlOdMin" />
                                    <input  type="hidden" id="hdlOdMax"  name="hdlOdMax" />
                                    <input  type="hidden" id="hdlCarCc"  name="hdlOdMax" />
                                
                                </td>
 



                            </tr>
                            <tr>
                                <td class="label">ปีที่จดทะเบียน :</td>
                                <td>
                                   
                                    
                                    
<%--  
<asp:DropDownList ID="ddlCarAge" runat="server" AppendDataBoundItems="true" >
<asp:ListItem Text="-- กรุณาระบุ --" Value=""></asp:ListItem> 
</asp:DropDownList> 
--%>

<select id="ddlCarAge" name="ddlCarAger"> 
<option value="0" selected="selected" >-- กรุณาระบุ --</option> 
<%
int intTodayYear = Convert.ToInt32(DateTime.Today.ToString("yyyy", new System.Globalization.CultureInfo("th-TH")));  
for(int i =0 ;i <=20 ; i++){
int intYear = intTodayYear - i;
%>
<option value="<%=intYear%>"><%=intYear%></option> 
<%}%> 
</select>

<span class="star">*</span>
<input  type="hidden" id="hdlOdMinDisc"  name="hdlOdMinDisc" />
<input  type="hidden" id="hdlOdMaxDisc"  name="hdlOdMaxDisc" />

                                </td>
                            </tr>
                          <%--    <tr>
                                <td class="label">ประเภท : </td>
                                <td>
                                    <select id="ddlCarType"></select>

                                    <span class="star">*</span></td>
                            </tr>
                          <tr>
                                <td class="label">ลักษณะการใช้รถ : </td>
                                <td>
                                    <select id="ddlCarUse"></select>


                                    <span class="star">*</span></td>
                            </tr>--%>

                            <tr>
                                <td class="label">c.c. เครื่องยนต์ : </td>
                                <td>

                             
                                    <asp:TextBox ID="txtCarCc" runat="server" MaxLength="4"></asp:TextBox>
                                    <span class="star">*</span></td>
                            </tr>
                            <tr>
                                <td class="label">ขับรถวันละกี่กิโลเมตร  :</td>
                                <td>
                                    <asp:DropDownList ID="ddlCarLeng" runat="server"  AppendDataBoundItems="true">
                                         <asp:ListItem Text="-- กรุณาระบุ --" Value="0"></asp:ListItem>
                                        <asp:ListItem>ไม่ถึง 10 กม.</asp:ListItem>
                                        <asp:ListItem>มากกว่า  10 กม.</asp:ListItem>
                                        <asp:ListItem>มากกว่า  25 กม.</asp:ListItem>
                                        <asp:ListItem>มากกว่า  50 กม.</asp:ListItem>
                                        <asp:ListItem>มากกว่า 100 กม.</asp:ListItem>
                                        <asp:ListItem>มากกว่า 200 กม.</asp:ListItem>
                                    </asp:DropDownList>
                                   </td>
                            </tr>
                            <tr>
                                <td class="label">ปรกติใช้รถใน : </td>
                                <td>
                                    <asp:RadioButtonList ID="RadioButtonList6" runat="server" RepeatDirection="Horizontal" CssClass="tb-radioList">
                                        <asp:ListItem Value="true" Selected="True">กรุงเทพฯ</asp:ListItem>
                                        <asp:ListItem Value="false">ต่างจังหวัด</asp:ListItem>
                                    </asp:RadioButtonList>
                                    </td>
                            </tr>
                            <tr>
                                <td class="label">ผู้ขับขี่ : </td>
                                <td> 
                                     


<asp:RadioButtonList ID="rblCarDriver" runat="server" RepeatDirection="Horizontal" CssClass="tb-radioList"   > 
<asp:ListItem Value="0" Selected="True"  >ไม่ระบุผู้ขับ</asp:ListItem>
<asp:ListItem Value="1">ระบุผู้ขับ</asp:ListItem>
</asp:RadioButtonList>
                                    </td>
                            </tr>

                          
                              <!-- Start id="areaDriBirth" -->
                            <tr  id="areaCarDriverAge" style="display: none;" >
                                  <td colspan="2">

                                      <Table  class="tb-online-insCar" >

                                    <tr>  
                                    <td class="label" width="130px" >วันเกิด ผู้ขับขี่ คนที่1 : </td>
                                    <td  > 
                                        
                                   <input  type="text"  ID="DatePickerDriver1"  name ="tbxDateDriver1" style="margin-right:0px"  class="datepicker"/>&nbsp;
                                    </td</tr>

                                    <tr>
                                    <td class="label">วันเกิด ผู้ขับขี่ คนที่2 :  </td>
                                    <td> 


                                    <input   type="text"  ID="DatePickerDriver2" name ="tbxDateDriver2"  style="margin-right:0px"   class="datepicker" />&nbsp;
                                    </td>
                                    </tr> 


                                    
                                    </Table>
</td>
                                </tr>

<!-- End id="areaDriBirth" -->
                            <tr>
                                <td class="label">ซื้อ พรบ. : </td>
                                <td>
                                    <asp:RadioButtonList ID="rdoRegPlus" runat="server" RepeatDirection="Horizontal" CssClass="tb-radioList">
                                        <asp:ListItem Value="true" Selected="True">ซื้อ</asp:ListItem>
                                        <asp:ListItem Value="false">ไม่ซื้อ</asp:ListItem>
                                    </asp:RadioButtonList>
                                    </td>
                            </tr>
                            <tr>
                                <td class="label">เลือกแบบมี Deduc : </td>
                                <td>
                                    <asp:RadioButtonList ID="rdoRegDeduc" runat="server" RepeatDirection="Horizontal" CssClass="tb-radioList">
                                        <asp:ListItem Value="true">ใช่</asp:ListItem>
                                        <asp:ListItem Value="false" Selected="True">ไม่ใช่</asp:ListItem>
                                    </asp:RadioButtonList>
                                    </td>
                            </tr>

                            <tr>
                                <td></td>
                            </tr>


                        </table>
                    </td>
                    <td valign="top">

                        <table class="tb-online-insCar">
                            <tr>
                                <td class="label" width="150px">มีตารางกรมธรรม์ : </td>
                                <td  >
                                    <asp:RadioButtonList ID="rdoRegInsHave" runat="server" RepeatDirection="Horizontal" CssClass="tb-radioList">
                                        <asp:ListItem>มี</asp:ListItem>
                                        <asp:ListItem Selected="True">ไม่มี</asp:ListItem>
                                    </asp:RadioButtonList>
                                     </td>
                            </tr>

                            <tr>
                                <td class="label">บริษัทที่ทำประกันภัยอยู่    : </td>
                                <td>
                                    <asp:DropDownList ID="ddlRegInsNameOld" runat="server" 
                                  AppendDataBoundItems="true" >
 <asp:ListItem Text="-- กรุณาระบุ --" Value=""></asp:ListItem>
                                        <asp:ListItem>กรุงเทพประกันภัย</asp:ListItem>
                                        <asp:ListItem>วิริยะประกันภัย</asp:ListItem>
                                        <asp:ListItem>สินมั่นคงประกันภัย</asp:ListItem>
                                        <asp:ListItem>อาคเนย์ประกันภัย</asp:ListItem>
                                    </asp:DropDownList>
                                     </td>
                            </tr>
                            <tr>
                                <td class="label">ประเภทมี่ทำประกันภัยอยู่ : </td>
                                <td>
                                    <asp:DropDownList ID="ddlRegInsTypeOld" runat="server" AppendDataBoundItems="true" >
 <asp:ListItem Text="-- กรุณาระบุ --" Value=""></asp:ListItem>


                                    </asp:DropDownList>
                                     </td>
                            </tr>

                            <tr>
                                <td class="label">ชื่อ : </td>
                                <td>
                                    <asp:TextBox ID="tbxRegName" runat="server"></asp:TextBox>
                                    <span class="star">*</span></td>
                            </tr>

                            <tr>
                                <td class="label">สกุล : </td>
                                <td>
                                    <asp:TextBox ID="tbxRegSur" runat="server"></asp:TextBox>
                                    <span class="star">*</span></td>
                            </tr>
                            <tr>
                                <td class="label">เพศ : </td>
                                <td>
                                    <asp:RadioButtonList ID="rdoRegSex" runat="server" RepeatDirection="Horizontal" CssClass="tb-radioList">
                                        <asp:ListItem Value="0" >หญิง</asp:ListItem>
                                        <asp:ListItem Value="1" Selected="True" >ชาย</asp:ListItem>
                                    </asp:RadioButtonList>
                                   </td>
                            </tr>
                            <tr>
                                <td class="label">อายุ : </td>
                                <td>
                                    <asp:DropDownList ID="ddlRegAge" runat="server" AppendDataBoundItems="true" >
 <asp:ListItem Text="-- กรุณาระบุ --" Value="">

 </asp:ListItem>


                                    </asp:DropDownList>
                                    <span class="star">*</span></td>
                            </tr>
                            <tr>
                                <td class="label">อีเมล์ : </td>
                                <td>
                                    <asp:TextBox ID="tbxRegEmail" runat="server"></asp:TextBox>
                                    <span class="star">*</span></td>
                            </tr>

                            <tr>
                                <td class="label">เบอร์โทรศัพท์มือถือ : </td>
                                <td>
                                    <asp:TextBox ID="tbxRegMobile" runat="server"></asp:TextBox>
                                    <span class="star">*</span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" valign="top">
                        <div style="margin: 0px 10px 0px 20px; float: left; height: 50px;">
                            <asp:CheckBox ID="CheckBox2" runat="server" />
                        </div>
                        ข้าพเจ้ายินยอมให้บริษัทใช้หมายเลขโทรศัพท์ ที่อยู่ และ อีเมล์ ที่ให้ไว้ข้างต้น
                        <br />
                        ในการแจ้งผลการสมัคร ข่าวสาร หรืออื่นๆ  หรือการเสนอขายผลิตภัณฑ์ของบริษัทและคู่ค้า</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div style="margin: 0px auto; width: 190px; margin-top: 15px;">
                            
                                 <asp:Button ID="btnInsrate" runat="server" Text="คำนวณเบี้ย" class="btn-default" Style="margin-right: 10px;"  OnClick="BtnPremiumShow_Click"     />


                            <input id="btnClear" type="reset" class="btn-default " value="เคียร์ข้อมูล" />


                        </div>
                    </td>
                      <a name="grd"></a>
                </tr>
            </table>
       

      <br />


<div id="insure-rate" >  
<!--  Start Overlapping Tabs   -->
<div id="tabOvl-onlineInsureCar" style="display:none">
<ul class="obtabs">
<%-- <%= (Request.QueryString["pol"] )== "1"?"id='current'":"";%> --%>                     
<li class="current" ><span><a href="#grd" rel="pol1"  name="grd">ชั้น 1</a></span></li>
<li  ><span><a href="#grd" rel="pol2"   >ชั้น 2 </a></span></li>
<li><span><a href="#grd" rel="pol3"   >ชั้น 3</a></span></li>
</ul>
</div>
<!--  End Overlapping Tabs   --> 

<script> 
$(function () {

$('.obtabs li a').click(function () {
    // set active
    /// $(this).parent('li').removeClass('current');
    $('.obtabs li').removeAttr("class");
    $(this).parent().parent().addClass('current');

    // Set Hide
    $("#selectData div[id^=pol]").hide();

    //Set Show 
    elmSelect = $(this).attr("rel");
    console.log(elmSelect);
    $("#selectData #" + elmSelect).show();

});
});
                     
</script>
                 
          <style>
            /*===== Control  GridView  Width (support IE9 & Over) =====*/
            .tb-prd-result th {height:20px;   color:#fff ;   padding:5px 5px 7px 5px; font-size:13px;  vertical-align:top ;  }
            
            .tb-prd-result th:nth-child(3) {background-color:#ffa200; }
            .tb-prd-result th:nth-child(4)  {background-color:#ffa200; width: 5%;  }
            .tb-prd-result th:nth-child(5) { background-color:#3942f2 ; } 
            .tb-prd-result th:nth-child(6) { background-color:#3942f2 ; width: 5%; }

            .tb-prd-result  td {padding:2px 5px;height:20px; line-height: 18px;} 
            .tb-prd-result td:nth-child(1) {  width: 20%;  } 
            .tb-prd-result td:nth-child(3) {background-color:#fffeb3;  }
            .tb-prd-result  td:nth-child(4)  {background-color:#fffeb3;   }
            .tb-prd-result  td:nth-child(5) { background-color:#e4f2fd ; } 
            .tb-prd-result td:nth-child(6) { background-color:#e4f2fd ;  } 
            .tb-prd-result td:nth-child(8) {   font-size: 12px; } 
            .tb-prd-result td:nth-child(9) {  width: 5%; font-size: 12px;  } 

        </style>

<div id="selectData">

 <div id="pol1">  
     <%--  <h3>GridView1</h3>--%>
    <asp:GridView ID="GridView1" runat="server"  class="tb-prd-result"
 
    OnRowDataBound="GridView1_RowDataBound"  
    DataKeyNames="campaign"  AutoGenerateColumns="false"   >

                    <Columns>
                        <asp:TemplateField   HeaderText="ผลิตภัณฑ์"  > 
                            <ItemTemplate>
                            <asp:HyperLink runat="server" id="hplCampaign"  ></asp:HyperLink> 
</ItemTemplate>
  </asp:TemplateField>
                        <asp:TemplateField   HeaderText="ทุนประกัน" >
                             <ItemTemplate>
                            <asp:Label runat="server"   id="lblCover" ></asp:Label>
                        </ItemTemplate>
                                 </asp:TemplateField>
                        <asp:TemplateField    HeaderText="เบี้ยประกัน<br />
                            ซ่อมห้าง" >
                             <ItemTemplate   > 
                             <asp:Label runat="server"   id="lblPremium1" ></asp:Label>
                                 </ItemTemplate>
                        </asp:TemplateField    >
                        <asp:TemplateField   HeaderText="คลิ๊ก<br />ซื้อ" >
                             <ItemTemplate>
                                 <asp:HyperLink id="hplBuy1"  runat="server"  class="btn-default  btn-iconOrder" >ซื้อ<span></span></asp:HyperLink>
 
                                 </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField   HeaderText="เบี้ยประกัน<br />
                            ซ่อมอู่" >
                             <ItemTemplate>
                             <asp:Label runat="server"    id="lblPremium2"></asp:Label>
                                 </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField   HeaderText="คลิ๊ก<br />ซื้อ" >
                             <ItemTemplate>
                           <asp:HyperLink id="hplBuy2"  runat="server"  class="btn-default  btn-iconOrder" >ซื้อ<span></span></asp:HyperLink>
              </ItemTemplate>
                        </asp:TemplateField   >
                        <asp:TemplateField    HeaderText="Dedug" >
                             <ItemTemplate>
                                <asp:Label runat="server"   id="lblDedug"  ></asp:Label>
                                 </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField   HeaderText="ความ<br />คุ้มครอง" >
                             <ItemTemplate>
                             <asp:HyperLink runat="server" id="hplDetail"  ></asp:HyperLink> 
                                 </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField   HeaderText="หมาย<br />เหตุ" >
                             <ItemTemplate>
                             <asp:HyperLink runat="server" id="hplVocher"></asp:HyperLink> 
                                 </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
</div>

<div id="pol2"  style="display:none" >
<%--  <h3>GridView2</h3>--%>
     <asp:GridView ID="GridView2" runat="server"  class="tb-prd-result"
 
    OnRowDataBound="GridView2_RowDataBound"  
    DataKeyNames="campaign"  AutoGenerateColumns="false"   >

                    <Columns>
                        <asp:TemplateField   HeaderText="ผลิตภัณฑ์"  > 
                            <ItemTemplate>
                            <asp:HyperLink runat="server" id="hplCampaign"  ></asp:HyperLink> 
</ItemTemplate>
  </asp:TemplateField>
                        <asp:TemplateField   HeaderText="ทุนประกัน" >
                             <ItemTemplate>
                            <asp:Label runat="server"   id="lblCover" ></asp:Label>
                        </ItemTemplate>
                                 </asp:TemplateField>
                        <asp:TemplateField    HeaderText="เบี้ยประกัน<br />
                            ซ่อมห้าง" >
                             <ItemTemplate   > 
                             <asp:Label runat="server"   id="lblPremium1" ></asp:Label>
                                 </ItemTemplate>
                        </asp:TemplateField    >
                        <asp:TemplateField   HeaderText="คลิ๊ก<br />ซื้อ" >
                             <ItemTemplate>
                                 <asp:HyperLink id="hplBuy1"  runat="server"  class="btn-default  btn-iconOrder" >ซื้อ<span></span></asp:HyperLink>
 
                                 </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField   HeaderText="เบี้ยประกัน<br />
                            ซ่อมอู่" >
                             <ItemTemplate>
                             <asp:Label runat="server"    id="lblPremium2"></asp:Label>
                                 </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField   HeaderText="คลิ๊ก<br />ซื้อ" >
                             <ItemTemplate>
                           <asp:HyperLink id="hplBuy2"  runat="server"  class="btn-default  btn-iconOrder" >ซื้อ<span></span></asp:HyperLink>
              </ItemTemplate>
                        </asp:TemplateField   >
                        <asp:TemplateField    HeaderText="ค่าเสียหาย<br />
                            ส่วนแรก" >
                             <ItemTemplate>
                                <asp:Label runat="server"   id="lblDedug"  ></asp:Label>
                                 </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField   HeaderText="ความ<br />คุ้มครอง" >
                             <ItemTemplate>
                             <asp:HyperLink runat="server" id="hplDetail"  > </asp:HyperLink> 
                                 </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField   HeaderText="หมาย<br />เหตุ" >
                             <ItemTemplate>
                             <asp:HyperLink runat="server" id="hplVocher"> </asp:HyperLink> 
                                 </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
</div>

<div id="pol3"   style="display:none"  >
   <%-- <h3>GridView3</h3>--%>
     <asp:GridView ID="GridView3" runat="server"  class="tb-prd-result"
 
    OnRowDataBound="GridView3_RowDataBound"  
    DataKeyNames="campaign"  AutoGenerateColumns="false"   >

                    <Columns>
                        <asp:TemplateField   HeaderText="ผลิตภัณฑ์"  > 
                            <ItemTemplate>
                            <asp:HyperLink runat="server" id="hplCampaign"  ></asp:HyperLink> 
</ItemTemplate>
  </asp:TemplateField>
                        <asp:TemplateField   HeaderText="ทุนประกัน" >
                             <ItemTemplate>
                            <asp:Label runat="server"   id="lblCover" ></asp:Label>
                        </ItemTemplate>
                                 </asp:TemplateField>
                        <asp:TemplateField    HeaderText="เบี้ยประกัน<br />
                            ซ่อมห้าง" >
                             <ItemTemplate   > 
                             <asp:Label runat="server"   id="lblPremium1" ></asp:Label>
                                 </ItemTemplate>
                        </asp:TemplateField    >
                        <asp:TemplateField   HeaderText="คลิ๊ก<br />ซื้อ" >
                             <ItemTemplate>
                                 <asp:HyperLink id="hplBuy1"  runat="server"  class="btn-default  btn-iconOrder" >ซื้อ<span></span></asp:HyperLink>
 
                                 </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField   HeaderText="เบี้ยประกัน<br />
                            ซ่อมอู่" >
                             <ItemTemplate>
                             <asp:Label runat="server"    id="lblPremium2"></asp:Label>
                                 </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField   HeaderText="คลิ๊ก<br />ซื้อ" >
                             <ItemTemplate>
                           <asp:HyperLink id="hplBuy2"  runat="server"  class="btn-default  btn-iconOrder" >ซื้อ<span></span></asp:HyperLink>
              </ItemTemplate>
                        </asp:TemplateField   >
                        <asp:TemplateField    HeaderText="ค่าเสียหาย<br />
                            ส่วนแรก" >
                             <ItemTemplate>
                                <asp:Label runat="server"   id="lblDedug"  ></asp:Label>
                                 </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField   HeaderText="ความ<br />คุ้มครอง" >
                             <ItemTemplate>
                             <asp:HyperLink runat="server" id="hplDetail"  > </asp:HyperLink> 
                                 </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField   HeaderText="หมาย<br />เหตุ" >
                             <ItemTemplate>
                             <asp:HyperLink runat="server" id="hplVocher"> </asp:HyperLink> 
                                 </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
</div>
</div><!-- id="selectData" -->


<div id="dialog1" style="display:none" title="รายละเอียด ความคุ้มครอง"   >
  <iframe id="coverIframe"   frameborder="0" width="570" height="440" scrolling="no"></iframe>
</div>

<div id="dialog2" style="display:none" title="รายละเอียด Voucher"   >
<iframe id="voucherIframe"   frameborder="0" width="570" height="440" scrolling="no"></iframe>
</div>
    






            </div>
            <!-- End id="insure-rate"-->



            <!-- End class="prc-row"-->
    </div>
    <!-- End <div class="context page-app"> -->
</asp:Content>

