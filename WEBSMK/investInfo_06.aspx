﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="investInfo_06.aspx.cs" Inherits="investInfo_06" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div class="subject1">
      รายงานผู้สอบบัญชี</div>
 <table class="tb-quarter">
        <tr>
            <th class="no-borderleft">&nbsp;
                
            </th>
            <th>
                ไตรมาสที่ 1
            </th>
            <th>
                ไตรมาสที่ 2
            </th>
            <th>
                ไตรมาสที่ 3
            </th>
            <th class="no-borderright">
              ไตรมาสที่ 4          </th>
        </tr>
        <tr>
            <td width="142" class="no-borderleft">
                ปี 2555
            </td>
            <td   ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"    /><br />
              <a href="downloads/investment/audit-q1-2555.pdf" target="_blank" title="ไตรมาสที่ 1" >สำหรับงวดสิ้นสุดวันที่<br />
                31 มีนาคม 2555</a></td>
            <td   ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"    /><br />
              <a href="downloads/investment/audit-q2-2555.pdf" target="_blank" title="ไตรมาสที่ 2">สำหรับงวดสิ้นสุดวันที่<br />
                30 มิถุนายน 2555</a></td>
            <td   ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"    /><br />
              <a href="downloads/investment/audit-q3-2555.pdf" target="_blank" title="ไตรมาสที่ 3" >สำหรับงวดสิ้นสุดวันที่<br />
                30 กันยายน 2555</a></td>
            <td   class="no-borderright"><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"    /><br />
              <a href="downloads/investment/audit-q4-2555.pdf" target="_blank" title="ไตรมาสที่ 4">สำหรับงวดสิ้นสุดวันที่<br />
                31 ธันวาคม 2555</a></td>
        </tr>
        <tr>
            <td class="no-borderleft">
                ปี 2554
            </td>
            <td   ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"    /><br />
              <a href="downloads/investment/audit-q1-2554.pdf" target="_blank"  title="ไตรมาสที่ 1">สำหรับงวดสิ้นสุดวันที่<br />
                31 มีนาคม 2554</a></td>
            <td   ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"    /><br />
              <a href="downloads/investment/audit-q2-2554.pdf" target="_blank" title="ไตรมาสที่ 2" >สำหรับงวดสิ้นสุดวันที่<br />
                30 มิถุนายน 2554</a></td>
            <td   ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"    /><br />
              <a href="downloads/investment/audit-q3-2554.pdf" target="_blank"  title="ไตรมาสที่ 3" >สำหรับงวดสิ้นสุดวันที่<br />
                30 กันยายน 2554</a></td>
            <td   class="no-borderright"><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"    /><br />
              <a href="downloads/investment/audit-q4-2554.pdf" target="_blank" title="ไตรมาสที่ 4" >สำหรับงวดสิ้นสุดวันที่<br />
                31 ธันวาคม 2554</a></td>
        </tr>
        <tr>
            <td class="no-borderleft">
                ปี 2553
            </td>
            <td   ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"    /><br />
              <a href="downloads/investment/audit-q1-2553.pdf" target="_blank" title="ไตรมาสที่ 1" >สำหรับงวดสิ้นสุดวันที่<br />
                31 มีนาคม 2553</a></td>
            <td   ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"    /><br />
              <a href="downloads/investment/audit-q2-2553.pdf" target="_blank" title="ไตรมาสที่ 2" >สำหรับงวดสิ้นสุดวันที่<br />
                30 มิถุนายน 2553</a></td>
          <td   ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"    /><br />
              <a href="downloads/investment/audit-q3-2553.pdf" target="_blank"  title="ไตรมาสที่ 3">สำหรับงวดสิ้นสุดวันที่<br />
                30 กันยายน 2553</a></td>
            <td   class="no-borderright"><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"    /><br />
              <a href="downloads/investment/audit-q4-2553.pdf" target="_blank"  title="ไตรมาสที่ 4" >สำหรับงวดสิ้นสุดวันที่<br />
                31 ธันวาคม 2553</a></td>
        </tr>
    </table>
    
    <div class="notation2">
เอกสารเป็นไฟล์ PDF การเปิดดูจำเป็นต้องมีโปรแกรม  Adobe Reader คลิ๊กที่ไอคอนโปรแกรมเพื่อติดตั้ง   
<a href="http://www.adobe.com/products/acrobat/readstep2.html"
target="_blank"><img src="<%=Config.SiteUrl%>images/icon-pdf-download.gif"  />
</a>
</div>
    
</asp:Content>

