﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;
using System.Collections;
using TDSLib.Libs.net.mail;
using System.Net.Mail;

public partial class callcenter : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    { 
        if (!Page.IsPostBack) {
         BindData();
        }
    }



    protected  void BindData(){
        //string ConId = String.Format("{0:00000}", intId);
        ddlTimeHours.DataSource  = SetHourse();
        ddlTimeHours.DataBind();
        ddlTimeHours.AppendDataBoundItems = true;
        ddlTimeHours.Items.Insert(0, new ListItem("00", "00")); 

        ddlTimeMinute.DataSource = SetMinute();
        ddlTimeMinute.DataBind();
        ddlTimeMinute.AppendDataBoundItems = true;
        ddlTimeMinute.Items.Insert(0, new ListItem("00", "00")); 

    }
     

     protected int SetData() { 
        string sql = "INSERT INTO tbCnt Values (@CntName, @CntTel, @CntEmail ,@CntDetail, @CntDateTimeGet, @CntDateSet, @CntTimeSet, @CntStatus ) ";

        SqlParameterCollection objParam1 = new SqlCommand().Parameters; 
        objParam1.AddWithValue("@CntName", tbxName.Text) ;
        objParam1.AddWithValue("@CntTel",  tbxTel.Text);
        objParam1.AddWithValue("@CntEmail", tbxEmail.Text);
        objParam1.AddWithValue("@CntDetail", txrDetail.Text);
        objParam1.AddWithValue("@CntDateTimeGet", DateTime.Now);
        objParam1.AddWithValue("@CntDateSet", Convert.ToDateTime( tbxDateGet.Text  ,  new CultureInfo("th-TH") ) ) ;
        objParam1.AddWithValue("@CntTimeSet", (ddlTimeHours.SelectedValue +":"+ ddlTimeMinute.SelectedValue ) );
        objParam1.AddWithValue("@CntStatus", "0");

        DBClass obj = new DBClass();
        int i2 = obj.SqlExecute(sql ,  objParam1); 
        return  i2 ;  
    }

    protected void btnSubmit_Click(object sender, EventArgs e) { 
            if (SetData() == 1) {
                sendMail();
                Response.Redirect("response.aspx"); 
        } else {
            Response.Write("<script>alert('ไม่สามารถเพิ่มข้อมูลได้'); history.back() ;</script>"); 
        }
    }

    protected void btnReset_Click(object sender, EventArgs e) {
        tbxName.Text = string.Empty;
        tbxTel.Text = string.Empty; 
            tbxEmail.Text = string.Empty;
            txrDetail.Text = string.Empty;

            tbxDateGet.Text = string.Empty;
            ddlTimeHours.SelectedIndex = 0;
            ddlTimeMinute.SelectedIndex = 0; 

    }


    //======= SetUp ================================================
    protected ArrayList SetHourse() {
        ArrayList arl = new ArrayList();
        for (int i = 1; i <= 24; i++) {
            if (i < 10) {
                arl.Add("0" + i);
            } else {
                arl.Add(i);
            }
        }
        return arl;
    }

    protected ArrayList SetMinute() {
        ArrayList arl = new ArrayList();
        for (int i = 1; i <= 59; i++) {
            if (i < 10) {
                arl.Add("0" + i);
            } else {
                arl.Add(i);
            }
        }
        return arl;
    }

    private void sendMail()
    {
        string strSMTPDomainName = "";
        TDSMail mail;
        string strBody = "";
        string strType = "";
        MailAddressCollection mAddrs = new MailAddressCollection();
        MailManager cmMail = new MailManager();
        cmMail.setXmlFilePath(System.Configuration.ConfigurationManager.AppSettings["appPath"] + "/Adminweb/xml/ContactUs.xml");
        // ส่งเมล์ไปให้บุคคลที่มีหน้าทีรับผิดชอบ
        strSMTPDomainName = cmMail.getDataByKey("SMTPMail");
        mail = new TDSMail(strSMTPDomainName, 25);
        mail.TO = new MailAddressCollection();
        mail.Subject = "ลูกค้าจาก Internet  ร้องเรียนหรือให้บริษัทฯติดต่อกลับ จากคุณ " + tbxName.Text;
        mail.IsBodyHtml = true;
        strBody = " Dear เจ้าหน้าที่ลูกค้าสัมพันธ์ ," + "<br>";
        strBody += " ลูกค้าได้ทำการบันทึกข้อมูลร้องเรียนหรือให้บริษัทฯติดต่อกลับ <br>เรื่องที่ต้องการติดต่อ :" + txrDetail.Text + "<br><br>";
        mAddrs.Add(new MailAddress(cmMail.getDataByKey("EmailCallcenter")));
        strBody += " Best Regards," + "<br>";
        strBody += " &nbsp;&nbsp;&nbsp;บริษัท สินมั่นคงประกันภัย จำกัด(มหาชน)." + "<br>";
        mail.Body = strBody;
        mail.TO = mAddrs;
        for (int i = 0; i <= mAddrs.Count - 1; i++)
        {

        }
        try
        {
            string strFrom = cmMail.getDataByKey("EmailAdmin");
            string strPassword = cmMail.getDataByKey("EmailAdminPassword");
            mail.Send(new MailAddress(strFrom), strPassword, false);
        }
        catch (Exception ex)
        {
            TDS.Utility.MasterUtil.writeError("buyVoluntary_step4.sendMail() ", ex.ToString());
        }
    }
}
