﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class buyInsure_step2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        RegisterManager cmRegister = new RegisterManager();
        if (Request.QueryString["ct_code"] != "")
        {
            ds = cmRegister.getDataContact(Request.QueryString["ct_code"]);
        }
        else
        {
            Response.Redirect("buyInsure.aspx", true);
        }

        if (!IsPostBack)
        {
            docToUI(ds);
        }
    }

    public void docToUI(DataSet dsData)
    {
        lblPolType.Text = "";
        if (dsData.Tables[0].Rows[0]["ct_motor"].ToString() == "Y")
            lblPolType.Text = "ประกันภัยรถยนต์";
        if (dsData.Tables[0].Rows[0]["ct_marine"].ToString() == "Y")
            lblPolType.Text += "<br>" + "ประกันภัยทางทะเลและขนส่ง";
        if (dsData.Tables[0].Rows[0]["ct_fire"].ToString() == "Y")
            lblPolType.Text += "<br>" + "ประกันอัคคีภัย";
        if (dsData.Tables[0].Rows[0]["ct_pa"].ToString() == "Y")
            lblPolType.Text += "<br>" + "ประกันภัยอุบัติเหตุส่วนบุคคล และการเดินทาง";
        if (dsData.Tables[0].Rows[0]["ct_health"].ToString() == "Y")
            lblPolType.Text += "<br>" + "ประกันภัยสุขภาพ";
        if (dsData.Tables[0].Rows[0]["ct_cancer"].ToString() == "Y")
            lblPolType.Text += "<br>" + "ประกันภัยมะเร็ง";        
        if (dsData.Tables[0].Rows[0]["ct_other"].ToString() == "Y")
            lblPolType.Text += "<br>" + "อื่นๆ";
        if (lblPolType.Text.StartsWith("<br>"))
            lblPolType.Text = lblPolType.Text.Substring(4);
        lblName.Text = dsData.Tables[0].Rows[0]["ct_name"].ToString();
        lblTelNo.Text = dsData.Tables[0].Rows[0]["ct_telephone"].ToString();
        lblEmail.Text = dsData.Tables[0].Rows[0]["ct_email"].ToString();
        lblTime.Text = dsData.Tables[0].Rows[0]["ct_con_time"].ToString();
        lblRemark.Text = "<pre>" + dsData.Tables[0].Rows[0]["ct_detail"].ToString() + "</pre>";
    }
}
