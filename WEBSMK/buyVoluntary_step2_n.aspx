﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="buyVoluntary_step2.aspx.cs" Inherits="buyVoluntary_step2" %>
<%@ Register src="uc/ucTxtDate.ascx" tagname="ucTxtDate" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script>
        function validateForm() {
            var isRet = true;
            var isRequire = true;
            var isLength = true;
            var isDate = true;
            var strMsg = "";
            var strMsgRequire = "";
            var strMsgLength = "";
            var strMsgDate = "";            
            if (document.getElementById("<%=txtStartDate.txtID%>").value == "__/__/____") {
                strMsg += "    - วันที่เริ่มคุ้มครอง\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtCarLicense.ClientID %>").value == "") {
                strMsg += "    - เลขทะเบียนรถ\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=ddlLicenseProvince.ClientID %>").value == "") {
                strMsg += "    - จังหวัดที่จดทะเบียน\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtChasis.ClientID %>").value == "") {
                strMsg += "    - เลขตัวถัง\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtEngineNo.ClientID %>").value == "") {
                strMsg += "    - เลขเครื่องยนต์\n";
                isRet = false;
                isRequire = false;
            }
            
            if (document.getElementById("<%=txtFName.ClientID %>").value == "") {
                strMsg += "    - ชื่อ\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtLName.ClientID %>").value == "") {
                strMsg += "    - สกุล\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtAddress.ClientID %>").value == "") {
                strMsg += "    - ที่อยู่\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtSubDistrict.ClientID %>").value == "") {
                strMsg += "    - ตำบล / แขวง\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtDistrict.ClientID %>").value == "") {
                strMsg += "    - อำเภอ / เขต\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=ddlProvince.ClientID %>").value == "") {
                strMsg += "    - จังหวัด\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtPostCode.ClientID %>").value == "") {
                strMsg += "    - รหัสไปรษณีย์\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtMobile.ClientID %>").value == "") {
                strMsg += "    - เบอร์โทรศัพท์มือถือ\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtIDCard.ClientID %>").value == "") {
                strMsg += "    - เลขที่บัตรประชาชน\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=divDriver.ClientID %>").style.display == "inline") {
                if (document.getElementById("<%=txtDriver1Name.ClientID %>").className == "" &&
                document.getElementById("<%=txtDriver1Name.ClientID %>").value == "") {
                    strMsg += "    - ผู้ขับขี่คนที่ 1\n";
                    isRet = false;
                    isRequire = false;
                }
                if (document.getElementById("<%=txtDriver2Name.ClientID %>").className == "" &&
                document.getElementById("<%=txtDriver2Name.ClientID %>").value == "") {
                    strMsg += "    - ผู้ขับขี่คนที่ 2\n";
                    isRet = false;
                    isRequire = false;
                }
            }


            if (isRequire == false) {
                strMsg = "!กรุณาระบุ\n" + strMsg;
            }            

            if (isLength == false) {
                strMsgLength = "!ขนาดข้อมูลที่ระบุไม่ถูกต้อง\n" + strMsgLength;
            }

            if (isRet == false) {
                //alert(strMsg + strMsgLength + strMsgDate);
                showMessagePage(strMsg + strMsgLength + strMsgDate);
            }
            return isRet;
        }
        function showMessagePage(strMsg) {
            document.getElementById("MsgIframe").setAttribute("src", "ShowMessageBox.aspx?msg=" + strMsg);
            //alert("url : " + link);
            $("[id*=dialog]").dialog({
                autoOpen: false,
                show: "fade",
                hide: "fade",
                modal: true,
                //open: function (ev, ui) {
                //    $('#myIframe').src = 'http://www.google.com';
                //},
                maxHeight: '400',
                width: '400',
                resizable: true
                //title: 'Title Topic'
            });
            //            $("#dialog1").dialog();

            $("#dialogMsg").dialog("open");
        }
        function closeMessagePage() {
            $('#dialogMsg').dialog('close');
            return false;

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div  id="page-online-insCar">
        <h1 class="subject1">
            ทำประกันออนไลน์</h1>
        <div class="stepOnline">
            <img src="images/app-step-2.jpg" />
        </div>
        <div class="prc-row">        
        <h2 class="subject-detail">รายละเอียดประกันที่เลือก</h2>
        <table width="630px">
            <tr>
                <td valign="top">
                    <table class="tb-online-insCar">          
                        <tr>
                            <td class="label"  >
                                ชนิดประกัน :
                            </td>
                            <td  >
                                <asp:Label ID="lblCampaignName" runat="server" Text="Label"></asp:Label>
                            </td>                            
                        </tr>
                        <tr>
                            <td class="label">
                                ประเภทความคุ้มครอง :
                            </td>
                            <td  >
                                <asp:Label ID="lblPolType" runat="server" Text="Label"></asp:Label>
                            </td>                            
                        </tr>
                        <tr>
                            <td class="label">
                                ทุนประกัน :
                                <td  >
                                    <asp:Label ID="lblCarOD" runat="server" Text="Label"></asp:Label>
                                </td>                                
                        </tr>
                        <tr>
                            <td class="label">
                                <asp:Label ID="lblLabelVolunPrem" runat="server" Text="เบี้ยประกันสมัครใจ :"></asp:Label>
                            </td>
                            <td  >
                                <asp:Label ID="lblPremNet" runat="server" Text="Label"></asp:Label>
                            </td>                            
                        </tr>
                        <tr>
                            <td class="label">
                                วันที่เริ่มคุ้มครอง :
                            </td>
                            <td  >
                                <uc1:ucTxtDate ID="txtStartDate" runat="server" /><span class="star">*</span>
                            </td>
                            <td class="label"  >
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <table class="tb-online-insCar">
                        <tr>
                            <td class="label"  >
                                ผู้ขับขี่ :
                            </td>
                            <td  >
                                <asp:Label ID="lblFlagDriver" runat="server" Text="Label"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="label"  >
                                ชนิดเบี้ยประกัน :
                            </td>
                            <td  >
                                <asp:Label ID="lblMotGar" runat="server" Text="Label"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="label"  >
                                ความเสียหายส่วนแรก :
                            </td>
                            <td  >
                                <asp:Label ID="lblFlagDeduct" runat="server" Text="Label"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="label"  >
                                <asp:Label ID="lblLabelComprem" runat="server" Text="เบี้ย พรบ. :"></asp:Label>
                            </td>
                            <td  >
                                <asp:Label ID="lblFlagCompul" runat="server" Text="Label"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>        
        <h2 class="subject-detail">รายละเอียดรถยนต์</h2>
            <table width="630px">
                <tr>
                    <td valign="top">
                        <table class="tb-online-insCar">                
                            <tr>
                                <td class="label"  >
                                    ยี่ห้อรถ :
                                </td>
                                <td  >
                                    <asp:Label ID="lblCarName" runat="server" Text="Label"></asp:Label>
                                </td>                                
                            </tr>
                            <tr>
                                <td class="label">
                                    รุ่นรถ :
                                </td>
                                <td>
                                    <asp:Label ID="lblCarMark" runat="server" Text="Label"></asp:Label>
                                </td>                                
                            </tr>
                            <tr>
                                <td class="label">
                                    ปีจดทะเบียน :
                                </td>
                                <td>
                                    <asp:Label ID="lblCarRegisYear" runat="server" Text="Label"></asp:Label>
                                </td>                                                    
                            </tr>
                            <tr>
                                <td class="label">
                                    ประเภทรถ :
                                </td>
                                <td>
                                    <asp:Label ID="lblCarType" runat="server" Text="Label"></asp:Label>
                                </td>                                
                            </tr>
                            <tr>
                                <td class="label">
                                    ลักษณะการใช้รถ :
                                </td>
                                <td class="style1">
                                    <asp:Label ID="lblCarUseDesc" runat="server" Text="Label"></asp:Label>
                                </td>                                
                            </tr>
                            <tr>
                                <td class="label">
                                    ขับรถวันละ :
                                </td>
                                <td>
                                    <asp:Label ID="lblDistance" runat="server" Text="Label"></asp:Label>
                                </td>                                
                            </tr>
                            <tr>
                                <td class="label">
                                    ปรกติใช้รถใน :
                                </td>
                                <td>
                                    <asp:Label ID="lblDriveRegion" runat="server" Text="Label"></asp:Label>
                                </td>                                
                            </tr>
                            <tr>
                                <td class="label">
                                    เลขเครื่องยนต์ :
                                </td>
                                <td  >
                                    <asp:TextBox ID="txtEngineNo" runat="server"></asp:TextBox>
                                    <span class="star">*</span>
                                </td>
                            </tr>                            
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                         <table class="tb-online-insCar">
                            <tr>
                                <td class="label"  >
                                    มีตารางกรมธรรม์เดิมหรือไม่ : 
                                </td>
                                <td  >
                                    <asp:Label ID="lblFlagOldPolicy" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    ประเภทที่เคยทำประกันภัย :
                                </td>
                                <td  >
                                    <asp:Label ID="lblOldPolType" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="label"  >
                                    บริษัทที่เคยทำประกันภัย :
                                </td>
                                <td  >
                                    <asp:Label ID="lblOldInsuraceComp" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    เลขทะเบียนรถ :
                                </td>
                                <td  >
                                    <asp:TextBox ID="txtCarLicense" runat="server"></asp:TextBox>                        
                                    <span class="star">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    จังหวัดที่จดทะเบียน :
                                </td>
                                <td  >
                                    <asp:DropDownList ID="ddlLicenseProvince" runat="server">
                                    </asp:DropDownList>
                                    <span class="star">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    ขนาดซีซีเครื่องยนต์ :
                                </td>
                                <td >
                                    <asp:Label ID="lblCarSize" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    เลขตัวถัง :
                                </td>
                                <td  >
                                    <asp:TextBox ID="txtChasis" runat="server"></asp:TextBox>
                                    <span class="star">*</span>
                                </td>
                            </tr>                            
                         </table>
                    </td>
                </tr>
            </table>
            
           
            <h2 class="subject-detail">รายละเอียดผู้เอาประกัน</h2>
            <table width="630px">
                <tr>
                    <td valign="top">
                        <table class="tb-online-insCar">              
                            <tr>
                                <td class="label" >
                                    ชื่อ :
                                </td>
                                <td >
                                    <asp:TextBox ID="txtFName" runat="server"></asp:TextBox>
                                    <span class="star">*</span>
                                </td>    
                                <td class="label">
                                    นามสกุล :
                                </td>
                                <td>
                                    <asp:TextBox ID="txtLName" runat="server"></asp:TextBox>
                                    <span class="star">*</span>
                                </td>                            
                            </tr>
                            <tr>
                                <td class="label">
                                    ที่อยู่ :
                                </td>
                                <td colspan="3" nowrap>
                                    <asp:TextBox ID="txtAddress" runat="server" Width="250" MaxLength="50"></asp:TextBox>
                                    <span class="star">*</span>(ใส่ บ้านเลขที่, หมู่, ถนน)                                     
                                </td>
                            </tr>
                            <tr>                    
                                <td class="label"  >
                                    
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtSubDistrict" runat="server" Width="250" MaxLength="50"></asp:TextBox>
                                    <span class="star">*</span>(ใส่ ตำบล, แขวง)
                                </td>
                            </tr>
                            <tr>
                                <td class="label" >
                                    อำเภอ / เขต :
                                </td>
                                <td width="150px">
                                    <asp:TextBox ID="txtDistrict" runat="server"></asp:TextBox>
                                    <span class="star">*</span>
                                </td>      
                                <td class="label">
                                    จังหวัด :
                                </td>
                                <td  >
                                    <asp:DropDownList ID="ddlProvince" runat="server">
                                    </asp:DropDownList>
                                    <span class="star">*</span>
                                </td>                          
                            </tr>
                            <tr>
                                <td class="label">
                                    รหัสไปรษณีย์ :
                                </td>
                                <td width="130px">
                                    <asp:TextBox ID="txtPostCode" runat="server" MaxLength="5"></asp:TextBox>
                                    <span class="star">*</span>
                                </td>      
                                <td class="label">
                                    เลขที่บัตรประชาชน :
                                </td>
                                <td  >
                                    <asp:TextBox ID="txtIDCard" runat="server"></asp:TextBox>                        
                                    <span class="star">*</span>
                                </td>                         
                            </tr>
                            <tr>
                                <td class="label">
                                    เบอร์โทรศัพท์มือถือ :
                                </td>
                                <td>
                                    <asp:TextBox ID="txtMobile" runat="server"></asp:TextBox>
                                    <span class="star">*</span>
                                </td>  
                                <td class="label">
                                    เบอร์โทรศัพท์บ้าน :
                                </td>
                                <td>
                                    <asp:TextBox ID="txtTelephone" runat="server"></asp:TextBox>                        
                                </td>                                                  
                            </tr>
                            <tr>
                                <td class="label">
                                    อีเมล์ :
                                </td>
                                <td>
                                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>                                    
                                </td>                    
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
            </table>
            
            
            <div id="divDriver" runat="server" style="display:inline">
            <h2 class="subject-detail">รายละเอียดผู้ขับขี่</h2>
            <table width="630px">
                <tr>
                    <td valign="top">
                        <table class="tb-online-insCar">              
                            <tr>
                                <td class="label"  >
                                    ผู้ขับขี่คนที่ 1 :
                                </td>
                                <td  >
                                    <asp:TextBox ID="txtDriver1Name" runat="server" Width="150"></asp:TextBox>
                                    <span class="star">*</span>
                                </td>                                
                            </tr>
                            <tr>
                                <td class="label"  >
                                    ผู้ขับขี่คนที่ 2 :
                                </td>
                                <td  >
                                    <asp:TextBox ID="txtDriver2Name" runat="server" Width="150"></asp:TextBox>
                                    <span class="star" id="spanDriverName2">*</span>
                                </td>                                
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table class="tb-online-insCar">
                            <tr>
                                <td class="label"  >
                                    วันเกิด :
                                </td>
                                <td  >
                                    <asp:Label ID="lblDriver1BrithDate" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="label"  >
                                    วันเกิด :
                                </td>
                                <td  >
                                    <asp:Label ID="lblDriver2BrithDate" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>            
            </div>
            <div style="margin: 0px auto; width: 190px; margin-top: 15px;">
                <asp:Button ID="btnSave" runat="server"  class="btn-default" style="margin-right:10px;" Text="ยืนยัน" OnClientClick="return validateForm();" OnClick="btnSave_Click"  />
                <asp:Button ID="btnBack" runat="server"  class="btn-default" Text="ย้อนกลับ"   OnClientClick="history.back();return false;"/>
 


            </div>
        </div><!-- End class="prc-row"-->
        <div id="dialogMsg" style="display:none" title="การตรวจสอบข้อมูล"   >
            <iframe id="MsgIframe"   frameborder="0" style="max-height:360" width="370" scrolling="auto"></iframe>
        </div>
        
    </div>    
    <!-- End     <div class="context page-app"> -->
    <div style="display:none">
        <asp:TextBox ID="txtCarCode" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtCampaign" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtPolType" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtCarSize" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtCarBody" runat="server"></asp:TextBox>
    </div>
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

