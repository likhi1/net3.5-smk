﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront1_th.master" AutoEventWireup="true" CodeFile="home.aspx.cs" Inherits="home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div id="page-home">
        <div class="cat-show cat-show-margin">
            <div class="cat-col-left">
                <h1>ประกันสุขภาพ</h1>
                <div class="cat-des">
                    สุขภาพมีไว้ดูแล เราขอดูแลคุณด้วยการประกันสุขภาพ รูปแบบใหม่ ยิ่งฟิตมาก ยิ่งลดมาก  
                </div>
    <%--
    <div class="cat-type">
    <ul>
    <li><a href="#">แบบประกันยอดนิยม</a></li>
    <li><a href="#">แบบประกันล่าสุด</a></li>
    </ul>
    </div>
    --%>
            </div>
            <!-- End class="cat-col-left"  -->
            <div class="cat-col-right">
                <img src="<%=Config.SiteUrl%>images/insurance-products/catTb-1.jpg" />
            </div>
            <div class="clear">
            </div>
            <div class="cat-purchase">
                <a class="btn-default  btn-iconMore" href="product.aspx?cat=1"> รายละเอียด <span></span> </a> 
                <a class="btn-default  btn-iconOrder"  href="buyInsure.aspx?main_class=h"> สั่งซื้อ <span></span></a>
            </div>
        </div>
        <!-- End class="cat-show" -->
        <div class="cat-show">
            <div class="cat-col-left">
                <h1>ประกันรถยนต์</h1>
                <div class="cat-des">
                  อุ่นใจทุกครั้งที่เดินทาง ด้วยการประกันรถยนต์ หลากหลายรูปแบบให้เลือกได้ตามความต้องการ
                </div>
            </div>
            <!-- End class="cat-col-left"  -->
            <div class="cat-col-right">
                <img src="<%=Config.SiteUrl%>images/insurance-products/catTb-2.jpg" />
                
            </div>
            <div class="clear">
            </div>
            <div class="cat-purchase">
          
                    <a class="btn-default  btn-iconMore" href="product.aspx?cat=2"> รายละเอียด <span></span></a> 
                <a class="btn-default  btn-iconOrder"  href="buyVoluntary_step1.aspx"> สั่งซื้อ <span></span></a>
            </div>
        </div>
        <!-- End class="cat-show" -->
        <div class="cat-show cat-show-margin">
            <div class="cat-col-left">
                <h1>ประกันโรคมะเร็ง</h1>
                <div class="cat-des">
                 พบปุ๊ป จ่ายปั๊บทันทีที่ตรวจพบมะเร็ง เลือกแบบประกันมะเร็งที่ประหยัดแต่คุ้มครองสูง
                </div>
            </div>
            <!-- End class="cat-col-left"  -->
            <div class="cat-col-right">
                <img src="<%=Config.SiteUrl%>images/insurance-products/catTb-3.jpg" />
            </div>
            <div class="clear">
            </div>
            <div class="cat-purchase">
                    <a class="btn-default  btn-iconMore" href="product.aspx?cat=3"> รายละเอียด <span></span></a> 
                <a class="btn-default  btn-iconOrder"  href="buyInsure.aspx?main_class=c"> สั่งซื้อ <span></span></a>
 
            </div>
        </div>
        <!-- End class="cat-show" -->
        <div class="cat-show">
            <div class="cat-col-left">
                <h1>ประกันอุบัติเหตุ</h1>
                <div class="cat-des">
                   อุบัติเหตุสามารถเกิดขึ้นได้ตลอดเวลา เลือกแบบประกันอุบัติเหตุ ที่คุ้มครองทุกที่ทุกเวลา ทั่วโลก
                </div>
            </div>
            <!-- End class="cat-col-left"  -->
            <div class="cat-col-right">
                <img src="<%=Config.SiteUrl%>images/insurance-products/catTb-4.jpg" />
            </div>
            <div class="clear">
            </div>
            <div class="cat-purchase">
                    <a class="btn-default  btn-iconMore" href="product.aspx?cat=4"> รายละเอียด <span></span></a> 
                <a class="btn-default  btn-iconOrder"  href="buyInsure.aspx?main_class=p"> สั่งซื้อ <span></span></a> 
            </div>
        </div>
        <!-- End class="cat-show" -->
        <div class="cat-show  cat-show-margin">
            <div class="cat-col-left">
                <h1>ประกันอัคคีภัย</h1>
                <div class="cat-des">
                  รักบ้าน รักทรัพย์สินของท่านด้วยการประกันอัคคีภัยเพื่อคุ้มครองบ้านและทรัพย์สินที่ท่านรัก 
                </div>
            </div>
            <!-- End class="cat-col-left"  -->
            <div class="cat-col-right">
                <img src="<%=Config.SiteUrl%>images/insurance-products/catTb-5.jpg" />
            </div>
            <div class="clear">
            </div>
            <div class="cat-purchase">
                <a class="btn-default  btn-iconMore" href="product.aspx?cat=5"> รายละเอียด <span></span></a> 
                <a class="btn-default  btn-iconOrder"  href="buyInsure.aspx?main_class=f"> สั่งซื้อ <span></span></a> 
            </div>
        </div>
        <!-- End class="cat-show" -->
        <div class="cat-show">
            <div class="cat-col-left">
                <h1>ประกันเดินทาง</h1>
                <div class="cat-des">
        เดินทางอย่างสบายใจและมั่นใจในทุกหนแห่ง ด้วยประกันการเดินทาง
                </div>
            </div>
            <!-- End class="cat-col-left"  -->
            <div class="cat-col-right">
                <img src="<%=Config.SiteUrl%>images/insurance-products/catTb-6.jpg" />
            </div>
            <div class="clear">
            </div>
            <div class="cat-purchase">
                    <a class="btn-default  btn-iconMore" href="product.aspx?cat=6"> รายละเอียด <span></span></a> 
                <a class="btn-default  btn-iconOrder"  href="buyInsure.aspx?main_class=p"> สั่งซื้อ <span></span></a>
                 
            </div>
        </div>
        <!-- End class="cat-show" -->
    </div>
    <!-- End id="page-home" -->
</asp:Content>

