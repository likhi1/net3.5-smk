﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//--- connect
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
//--- arrayList
using System.Collections;
//---------- DateTime Region
using System.Globalization;
//---------- JSON Serialize & Derialize
using Newtonsoft.Json;
//--- Jquery Ajax
//using System.Web.Services;
 
 

public partial class buyInsureCar_step1 : System.Web.UI.Page
{
    //=======  Set Global Varible  ================================
    //Default field
    protected string strCarCode = HttpContext.Current.Request.Form["hdlCarcode"];
    protected string strCarMark = HttpContext.Current.Request.Form["hdlCarMarkcode"];
    protected string strCarGroup = HttpContext.Current.Request.Form["hdlCarGroup"];
    protected string strCarAge = HttpContext.Current.Request.Form["hdlCarAge"];
    protected string strAgentCode = "A"; 
    protected string strCarSize;
    protected string strCarBody;

    // DriverAge field 
    protected string specifyDriver { get{return rblCarDriver.Text ; }  set{ rblCarDriver.Text = value; } } 
    protected string strBirthDv1 = HttpContext.Current.Request.Form["tbxDateDriver1"];
    protected string strBirthDv2 = HttpContext.Current.Request.Form["tbxDateDriver2"];
    protected  int strAgeDriver;
    
    // Option field
    protected string strOdMinDisc = HttpContext.Current.Request.Form["hdlOdMinDisc"];
    protected string strOdMaxDisc =  HttpContext.Current.Request.Form["hdlOdMaxDisc"];
   //rotected string strPolType  { get; set;}
    protected string strPolType;
    protected string strSQL;

    protected void Page_Load(object sender, EventArgs e) { 

         if (!Page.IsPostBack) {
             AddDdlRegAge();
             PopulateCarName();
 
         } else {
             GridView1.Focus();
            string strCarname =  ddlCarName.SelectedValue.ToString();
            ddlCarName.SelectedValue = strCarname;
         //  Page.RegisterClientScriptBlock("OnLoad", "PopulateCarGroupCode();"); 
         }
    }
     
    //=======  Set Control ===============================================
    protected void ddlRegYear_addlist( ) {
         string strYear = DateTime.Today.ToString("yyyy"  , new CultureInfo("th-TH"));
         int intYear = int.Parse(strYear);
         ArrayList arrList = new ArrayList();
         for (int i = 0; i <= 20; i++) {
             int y = intYear - i;
             arrList.Add(y);
         }

         //ddlRegYear.DataSource = arrList;
         //ddlRegYear.DataBind();   // bind data 
         // ddlRegYear.Items.Insert(0, new ListItem("---- Select ----", ""));   // add Empty Item 
     } 
     
    protected void AddDdlRegAge( ) {
          ArrayList arrList = new ArrayList();
         for (int i = 18; i <= 100; i++) {
             int y =  i;
             arrList.Add(y);
         }

         ddlRegAge.DataSource = arrList;
         ddlRegAge.DataBind();   // bind data 
         
     } 
     
    //======= Set  Varible  befor select  ================================ 
   protected void SetDriver() {
        //+++++++++ Get Age Car Diver +++++++++++++++++ 
          //int specifyDriver = Convert.ToInt32( rblCarDriver.Text);
        int intTodayYear = Convert.ToInt32(DateTime.Today.ToString("yyyy", new CultureInfo("th-TH")));  
        int AgeDv1;
        int AgeDv2;
        int intAgeMin; 

        ////// Not Define Driver
        if (specifyDriver == "0") {
            strAgeDriver = 0;
        } else { 
            ////// Define Driver 
            if (strBirthDv2 == "") {
                string[] strYearDv1 = (strBirthDv1).Split('/');
                int intBirthYearDv1 = Convert.ToInt32(strYearDv1[2]);
                AgeDv1 = intTodayYear - intBirthYearDv1;
                intAgeMin = AgeDv1;
            } else {
                string[] strYearDv1 = (strBirthDv1).Split('/');
                int intBirthYearDv1 = Convert.ToInt32(strYearDv1[2]);
                AgeDv1 = intTodayYear - intBirthYearDv1;

                string[] strYearDv2 = (strBirthDv2).Split('/');
                int intBirthYearDv2 = Convert.ToInt32(strYearDv2[2]);
                AgeDv2 = intTodayYear - intBirthYearDv2;

                ////// Compare Age
                intAgeMin = (AgeDv1 > AgeDv2) ? AgeDv2 : AgeDv1;
            } 
            //// Get  Range  Age
            strAgeDriver = GetAgeDriver(intAgeMin);
        }  
    } 

    protected int GetAgeDriver(int Age) {
         string sql = " SELECT  TOP (1) defv_id, car_cod, defv_type, defv_value "
         + "FROM  mst_defaultvalue "
         + "WHERE  (defv_type = 'DRIVER') AND (defv_value <=" + Age + ") "
         + "ORDER BY defv_value DESC ";

         DBClass obj = new DBClass();
         DataSet ds = obj.SqlGet(sql, "tb_mst_defaultvalue");
         
         DataTable dt = ds.Tables[0];
         if (dt.Rows.Count > 0) { 
             Age = Convert.ToInt32(dt.Rows[0]["defv_value"]);
         }  
          
         return Age  ;

     }
      
    protected  void  SetCarSize(){
         //////////// Get Car Size  
        string SetCarSize =  txtCarCc.Text;
        if (Convert.ToInt32(strCarCode) == 110) {
            if (Convert.ToInt32(strCarSize) <= 2000) {
                strCarSize = "2000";
            } else {
                strCarSize = "9999";
            }

        } else {
            strCarSize = "0";
        }
         
    }

    protected void SetCarBody() {
        //+++++++++  Get Car Body ++++++++++++
        
        if ((strCarCode != "110") && (strCarCode == "210")) {
            strCarBody = "1";
        } else if ((strCarCode != "110") && (strCarCode == "320")) {
            strCarBody = "2";
        } else {
            strCarBody = "A";
        }

    }
    
    protected  void SetPoltype(){ 
    }


    //======= Get Data  ================================
    protected void BtnPremiumShow_Click(object sender, EventArgs e) {
        BindData();

   
    }

    protected void BindData() {

        SetDriver();
        SetCarSize();
        SetCarBody();

        strPolType = "1";
        DataSet ds1 = SelectData(strPolType);  // Assign  polType 
        GridView1.DataSource = ds1;
        GridView1.DataBind();

        strPolType = "2";
        DataSet ds2 = SelectData("2");
        GridView2.DataSource = ds2;
        GridView2.DataBind();

        strPolType = "3";
        DataSet ds3 = SelectData("3");
        GridView3.DataSource = ds3;
        GridView3.DataBind(); 

        ////// Count Row Befor Show PolType Tab
        // if (ds1.Tables[0].Rows.Count > 0 || ds2.Tables[0].Rows.Count > 0 || ds3.Tables[0].Rows.Count > 0) {
       // if (ds1.Tables[0].Rows.Count > 0 ) {

            ////////  Comand Show PolType Tab ( after reload )
            String csname1 = "PopupScript";
            if (!IsClientScriptBlockRegistered(csname1)) {
                String cstext1 = "<script> $('#tabOvl-onlineInsureCar').css({'display':'block'})</script>";
                RegisterStartupScript(csname1, cstext1);
            } 
         
      //  }

    } 
     
     protected DataSet SelectData( string _polType  ) { 
 
    strSQL= "SELECT a.* , b.name as cp_name  FROM  online_web a, online_campaign b  "
    +  "WHERE a.campaign = b.campaign AND " 
    +  "car_cod = @car_cod AND  "
    +  "car_age = @car_age AND  "
    +  "car_group = @car_group AND  "
    +  "( car_mak = @car_mak OR  car_mak =  'A') AND  "
    +  "car_size = @car_size AND  car_body = @car_body AND  "
    +  "agt_cod = @agt_cod AND  car_driver =  @car_driver  AND "
    +  "( (GETDATE() between start_dte and end_dte ) OR  (start_dte <= GETDATE() and end_dte is null) )  " ;
   

    if(_polType == "1" ){
    //Send polType  1
    //Send  @od_min ,  @od_max 
    strSQL+= "AND pol_typ ='"+_polType+"' "
    + "AND  ( car_od between  @od_min and  @od_max )  ";
     }else if(_polType=="2"){ 
    //Send polType  2 or 5
    //Send  @od_min ,  @od_max
        strSQL+= "AND ( (pol_typ ='2') OR (pol_typ ='5') )  "  
    +  "AND ( car_od between  @od_min and  @od_max )  " ;
    }else if(_polType=="3"){
    //Send polType  3
    //No send  @od_min ,  @od_max
    strSQL += "AND pol_typ ='"+_polType+"' "  ; 
    }
 
 
        SqlParameterCollection param1 = new SqlCommand().Parameters;

        param1.AddWithValue("@car_cod", strCarCode); 
        param1.AddWithValue("@car_age", strCarAge);
        param1.AddWithValue("@car_group", strCarGroup);
        param1.AddWithValue("@car_mak", strCarMark);
        param1.AddWithValue("@car_size", strCarSize);
        param1.AddWithValue("@car_body", strCarBody);
        param1.AddWithValue("@agt_cod", strAgentCode);
        param1.AddWithValue("@car_driver", strAgeDriver);
        param1.AddWithValue("@pol_typ", _polType);
         
        if (_polType == "1" || _polType == "2") {
        param1.AddWithValue("@od_min", strOdMinDisc);
        param1.AddWithValue("@od_max", strOdMaxDisc);
        }
        
                DBClass obj = new DBClass();
                DataSet ds = obj.SqlGet(strSQL , "tb_online_web", param1);  
                return ds ; 
        
    }

         

     protected void GridView1_RowDataBound(object s, GridViewRowEventArgs e) { 

             if (e.Row.RowType == DataControlRowType.DataRow) {

                 //========= ItemTemplate          
                 HyperLink hplCampaign = (HyperLink)(e.Row.FindControl("hplCampaign"));
                 if (hplCampaign != null) {  
                  hplCampaign.Text = DataBinder.Eval(e.Row.DataItem, "cp_name").ToString();
                  hplCampaign.NavigateUrl = "#"; 
                 }
                 Label lblCover = (Label)(e.Row.FindControl("lblCover"));
                 if (lblCover != null) {
                     lblCover.Text = string.Format("{0:#,###0}", DataBinder.Eval(e.Row.DataItem, "car_od"));
                    // lblCover.Text += " บาท";
                 }
                 Label lblPremium1 = (Label)(e.Row.FindControl("lblPremium1"));
                 if (lblPremium1 != null) {
                     lblPremium1.Text = string.Format("{0:#,###0}", DataBinder.Eval(e.Row.DataItem, "pre_grsservice")); ;
                    // lblPremium1.Text += " บาท";
                 }
                 HyperLink hplBuy1 = (HyperLink)(e.Row.FindControl("hplBuy1"));
                 if (hplBuy1 != null) {
                     //hplBuy1.Text =  DataBinder.Eval(e.Row.DataItem, "").ToString() ;
                    string   webID =  DataBinder.Eval(e.Row.DataItem, "web_id").ToString() ;
                    hplBuy1.NavigateUrl = "buyInsureCar_step2.aspx?pre=ser&id=" + webID  ;
                 }
                 Label lblPremium2 = (Label)(e.Row.FindControl("lblPremium2"));
                 if (lblPremium2 != null) {
                     lblPremium2.Text = string.Format("{0:#,###0}", DataBinder.Eval(e.Row.DataItem, "pre_grsgar") ) ;
                   //  lblPremium2.Text += " บาท";
                 }
                 HyperLink hplBuy2 = (HyperLink)(e.Row.FindControl("hplBuy2"));
                 if (hplBuy2 != null) {
                    // hplBuy2.Text = "";
                     string webID = DataBinder.Eval(e.Row.DataItem, "web_id").ToString();
                     hplBuy2.NavigateUrl = "buyInsureCar_step2.aspx?pre=gar&id=" + webID  ;
                 }
                 Label lblDedug = (Label)(e.Row.FindControl("lblDedug"));
                 if (lblDedug != null) {
                     lblDedug.Text = "";
                 }

                 HyperLink hplDetail = (HyperLink)(e.Row.FindControl("hplDetail"));
                 if (hplDetail != null) {
                     hplDetail.Text = DataBinder.Eval(e.Row.DataItem, "pre_pregar").ToString();

                    string strCamPaign =  DataBinder.Eval(e.Row.DataItem, "campaign").ToString();
                  
                    hplDetail.Text = "ราย<br>ละเอียด";
                    
                   
                    hplDetail.NavigateUrl = "buyInsureCarCover.aspx?cp=" + strCamPaign + "&pl=" + strPolType + "&cc=" + strCarCode + "&cs=" + strCarSize;
                    hplDetail.CssClass = "openerCover";

                    }


                 HyperLink hplVocher = (HyperLink)(e.Row.FindControl("hplVocher"));
                 if (hplVocher != null) {
                     hplVocher.Text = DataBinder.Eval(e.Row.DataItem, "pre_pregar").ToString();
                     hplVocher.Text = "Gif<br />Voucher"; 
                     hplVocher.NavigateUrl = "buyInsureCarVoucher.aspx";
                     hplVocher.CssClass = "openerVoucher";
                     
                 }


             }
         }

     protected void GridView2_RowDataBound(object s, GridViewRowEventArgs e) {

         if (e.Row.RowType == DataControlRowType.DataRow) {

             //========= ItemTemplate          
             HyperLink hplCampaign = (HyperLink)(e.Row.FindControl("hplCampaign"));
             if (hplCampaign != null) {
                 hplCampaign.Text = DataBinder.Eval(e.Row.DataItem, "cp_name").ToString();
                 hplCampaign.NavigateUrl = "#";
             }
             Label lblCover = (Label)(e.Row.FindControl("lblCover"));
             if (lblCover != null) {
                 lblCover.Text = string.Format("{0:#,###0}", DataBinder.Eval(e.Row.DataItem, "car_od"));
                 // lblCover.Text += " บาท";
             }
             Label lblPremium1 = (Label)(e.Row.FindControl("lblPremium1"));
             if (lblPremium1 != null) {
                 lblPremium1.Text = string.Format("{0:#,###0}", DataBinder.Eval(e.Row.DataItem, "pre_grsservice")); ;
                 // lblPremium1.Text += " บาท";
             }
             HyperLink hplBuy1 = (HyperLink)(e.Row.FindControl("hplBuy1"));
             if (hplBuy1 != null) {
                 //hplBuy1.Text =  DataBinder.Eval(e.Row.DataItem, "").ToString() ;
                 string webID = DataBinder.Eval(e.Row.DataItem, "web_id").ToString();
                 hplBuy1.NavigateUrl = "buyInsureCar_step2.aspx?pre=ser&id=" + webID  ;
             }
             Label lblPremium2 = (Label)(e.Row.FindControl("lblPremium2"));
             if (lblPremium2 != null) {
                 lblPremium2.Text = string.Format("{0:#,###0}", DataBinder.Eval(e.Row.DataItem, "pre_grsgar"));
                 //  lblPremium2.Text += " บาท";
             }
             HyperLink hplBuy2 = (HyperLink)(e.Row.FindControl("hplBuy2"));
             if (hplBuy2 != null) {
                 // hplBuy2.Text = "";
                 string webID = DataBinder.Eval(e.Row.DataItem, "web_id").ToString();
                 hplBuy2.NavigateUrl = "buyInsureCar_step2.aspx?pre=gar&id=" + webID ;
             }
             Label lblDedug = (Label)(e.Row.FindControl("lblDedug"));
             if (lblDedug != null) {
                 lblDedug.Text = "";
             }

             HyperLink hplDetail = (HyperLink)(e.Row.FindControl("hplDetail"));
             if (hplDetail != null) {
                 hplDetail.Text = DataBinder.Eval(e.Row.DataItem, "pre_pregar").ToString();

                 string strCamPaign = DataBinder.Eval(e.Row.DataItem, "campaign").ToString(); 

                 hplDetail.Text = "ราย<br>ละเอียด";
                 hplDetail.NavigateUrl = "buyInsureCarCover.aspx?cp=" + strCamPaign + "&pl=" + strPolType + "&cc=" + strCarCode + "&cs=" + strCarSize;
                 hplDetail.CssClass = "openerCover";

             }


             HyperLink hplVocher = (HyperLink)(e.Row.FindControl("hplVocher"));
             if (hplVocher != null) {
                 hplVocher.Text = DataBinder.Eval(e.Row.DataItem, "pre_pregar").ToString();
                 hplVocher.Text = "Gif<br />Voucher";
                 hplVocher.NavigateUrl = "buyInsureCarVoucher.aspx";
                 hplVocher.CssClass = "openerVoucher";

             }


         }
     }

     protected void GridView3_RowDataBound(object s, GridViewRowEventArgs e) {

         if (e.Row.RowType == DataControlRowType.DataRow) {

             //========= ItemTemplate          
             HyperLink hplCampaign = (HyperLink)(e.Row.FindControl("hplCampaign"));
             if (hplCampaign != null) {
                 hplCampaign.Text = DataBinder.Eval(e.Row.DataItem, "cp_name").ToString();
                 hplCampaign.NavigateUrl = "#";
             }
             Label lblCover = (Label)(e.Row.FindControl("lblCover"));
             if (lblCover != null) {
                 lblCover.Text = string.Format("{0:#,###0}", DataBinder.Eval(e.Row.DataItem, "car_od"));
                 // lblCover.Text += " บาท";
             }
             Label lblPremium1 = (Label)(e.Row.FindControl("lblPremium1"));
             if (lblPremium1 != null) {
                 lblPremium1.Text = string.Format("{0:#,###0}", DataBinder.Eval(e.Row.DataItem, "pre_grsservice")); ;
                 // lblPremium1.Text += " บาท";
             }
             HyperLink hplBuy1 = (HyperLink)(e.Row.FindControl("hplBuy1"));
             if (hplBuy1 != null) {
                 //hplBuy1.Text =  DataBinder.Eval(e.Row.DataItem, "").ToString() ;
                 string webID = DataBinder.Eval(e.Row.DataItem, "web_id").ToString();
                 hplBuy1.NavigateUrl = "buyInsureCar_step2.aspx?pre=ser&id=" + webID ;
             }
             Label lblPremium2 = (Label)(e.Row.FindControl("lblPremium2"));
             if (lblPremium2 != null) {
                 lblPremium2.Text = string.Format("{0:#,###0}", DataBinder.Eval(e.Row.DataItem, "pre_grsgar"));
                 //  lblPremium2.Text += " บาท";
             }
             HyperLink hplBuy2 = (HyperLink)(e.Row.FindControl("hplBuy2"));
             if (hplBuy2 != null) {
                 // hplBuy2.Text = "";
                 string webID = DataBinder.Eval(e.Row.DataItem, "web_id").ToString();
                 hplBuy2.NavigateUrl = "buyInsureCar_step2.aspx?pre=gar&id=" + webID ;
             }
             Label lblDedug = (Label)(e.Row.FindControl("lblDedug"));
             if (lblDedug != null) {
                 lblDedug.Text = "";
             }

             HyperLink hplDetail = (HyperLink)(e.Row.FindControl("hplDetail"));
             if (hplDetail != null) {
                 hplDetail.Text = DataBinder.Eval(e.Row.DataItem, "pre_pregar").ToString();

                 string strCamPaign = DataBinder.Eval(e.Row.DataItem, "campaign").ToString(); 


                 hplDetail.Text = "ราย<br>ละเอียด";
                 hplDetail.NavigateUrl = "buyInsureCarCover.aspx?cp=" + strCamPaign + "&pl=" + strPolType + "&cc=" + strCarCode + "&cs=" + strCarSize;
                 hplDetail.CssClass = "openerCover";

             }


             HyperLink hplVocher = (HyperLink)(e.Row.FindControl("hplVocher"));
             if (hplVocher != null) {
                 hplVocher.Text = DataBinder.Eval(e.Row.DataItem, "pre_pregar").ToString();
                 hplVocher.Text = "Gif<br />Voucher";
                 hplVocher.NavigateUrl = "buyInsureCarVoucher.aspx";
                 hplVocher.CssClass = "openerVoucher"; 
             } 

         }
     }
      
    
     //======= ( AJAX ) Set  CarName & CarType   ======================

     //++++++++  Populating CarName DropDownList 
     [System.Web.Services.WebMethod]   // this line for use ajax (Web Services)
     public  void PopulateCarName() {

         SqlConnection objConn = new ConnectDB().SqlStrCon();
         string strCmd = "Select RTRIM(carnamcod) As carnamcod, RTRIM(desc_t) As desc_t, RTRIM(desc_e) As desc_e "
       + "FROM online_carname "
       + "WHERE  desc_e  IS  NOT NULL   AND RTRIM(desc_e) != ''  "
       + "ORDER BY desc_e  ASC ";

         SqlDataAdapter da = new SqlDataAdapter(strCmd, objConn);
         DataTable dt = new DataTable();
         da.Fill(dt);
         da = null;
         objConn.Close();
         objConn = null;

         ddlCarName.DataSource = dt;
         ddlCarName.DataTextField = "desc_e"; // name of field in DB
         ddlCarName.DataValueField = "carnamcod"; // name of field in DB
         ddlCarName.DataBind();
     }

     //++++++++ Populating CarMark
     [System.Web.Services.WebMethod] // this line for use ajax (Web Services)
     public static ArrayList PopulateCarMark(string carNameId, string carCatVal) {
        
         string strCmd ; 
         ////// Check Car Cat  = Van
        if(carCatVal == "V"){
        strCmd = "SELECT RTRIM(desc_e) AS desc_e ,  RTRIM(carmakcod) As  carmakcod   "
        + "From  online_carmark "
        + "WHERE carnamcod =  '" + carNameId + "' "
        + "AND car_type LIKE '%V%'"; 
        }else{
        strCmd = "SELECT RTRIM(desc_e) AS desc_e ,  RTRIM(carmakcod) As  carmakcod   "
        + "From  online_carmark "
        + "WHERE carnamcod =  '" + carNameId + "' "
        + "AND carmakcod LIKE '_" + carCatVal + "%' " 
         + "ORDER BY desc_e  ASC ";
        } 

        DBClass obj1 = new DBClass();
        DataSet ds1 = obj1.SqlGet(strCmd , "tblCarmark" );  //  get dataset


        DataTable  dt1 = ds1.Tables[0]; // set datatable  

        DataTableReader dr1 = dt1.CreateDataReader(); // covert dt to dr

        ArrayList list = new ArrayList();
        while (dr1.Read()) {
            list.Add(new ListItem(
           dr1["desc_e"].ToString(), // name of field in DB
           dr1["carmakcod"].ToString() // name of field in DB
            ));
        } 
        return list;  
          
     }

     //++++++++ Populate CarMarkCode & CarGroupCode & Od_min & Od_max
     [System.Web.Services.WebMethod] // this line for use ajax (Web Services)
     public static string PopulateCarGroupCode(string carmakcodId) {

         string strCmd = "SELECT RTRIM(group_code) AS group_code, " // ,  RTRIM(carnamcod) As  carnamcod 
        + "RTRIM(od_min) AS od_min, "
        + "RTRIM(od_max) AS od_max,  "
        + "RTRIM(car_cc) AS car_cc  " 
        + "FROM  online_carmark "
        + "WHERE carmakcod =  '" + carmakcodId + "' ";   

         DBClass obj1 = new DBClass();
         DataSet ds1 = obj1.SqlGet(strCmd, "_tbCarmark");  //  get dataset
         DataTable dt = ds1.Tables[0];
            
          
        ////// Add result to ArrayList
         //ArrayList arlCarMark = new ArrayList();
         //for (int i = 0; i < (dt.Columns.Count) ; i++) {
         //    arlCarMark.Add(dt.Rows[0][i]);
         //}

         string json = JsonConvert.SerializeObject(dt, Formatting.Indented);
         return json; 
     }

     //++++++++ Change CarCat
     [System.Web.Services.WebMethod] // this line for use ajax (Web Services) 
     public static ArrayList RblCarCatChange(string carnamcodId, string carcatId) {
         
         //---------- Check Empty Row
         string strCmd1;
        if (carcatId == "V") {
            strCmd1 = "SELECT COUNT(carmakcod) "
                + "From  online_carmark "
                + "WHERE carnamcod =  '" + carnamcodId + "' "
                + "AND car_type LIKE '%" + carcatId + "%' "; 
        } else {
            strCmd1 = "SELECT COUNT(carmakcod) "
            + "From  online_carmark "
            + "WHERE carnamcod =  '" + carnamcodId + "' "
            + "AND carmakcod LIKE '_" + carcatId + "%' ";
        }

         DBClass obj1 = new DBClass();
         int iRow = obj1.SqlExecuteScalar(strCmd1);  //  count row

          ArrayList list = new ArrayList();
         if (iRow > 0) {  
             //-------Check For Add Carmark 
             string strCmd2; 
             if (carcatId=="V") {
               strCmd2 = "SELECT RTRIM(desc_e) AS desc_e ,  RTRIM(carmakcod) As  carmakcod   "
              + "From  online_carmark "
              + "WHERE carnamcod =  '" + carnamcodId + "' "
              + "AND car_type LIKE '%" + carcatId + "%' "
              + "ORDER BY  desc_e ASC"; 
             }else{
                 strCmd2 = "SELECT RTRIM(desc_e) AS desc_e ,  RTRIM(carmakcod) As  carmakcod   "
               + "From  online_carmark "
               + "WHERE carnamcod =  '" + carnamcodId + "' "
               + "AND carmakcod LIKE '_" + carcatId + "%' "
               + "ORDER BY  desc_e  ASC"; 
             }

                DBClass obj2 = new DBClass();
                DataSet ds1 = obj2.SqlGet(strCmd2, "tblCarmark");  //  get dataset
                DataTable dt1 = ds1.Tables[0]; 

                DataTableReader dr1 = dt1.CreateDataReader(); // covert dt to dr 
                
                while (dr1.Read()) {
                    list.Add(new ListItem(
                   dr1["desc_e"].ToString(), // name of field in DB
                   dr1["carmakcod"].ToString() // name of field in DB
                    ));
                } 
                    
            
         }   //else {  list.Add(new ListItem("Not available", "0")); }
        
         
         return list;
     }


     //======== No USE  ============================================

     protected DataSet TestSelectData() {
         string sql = "SELECT TOP 5 * FROM online_web ORDER BY campaign ";
         DBClass obj = new DBClass();
         DataSet ds = obj.SqlGet(sql, "tb_online_web");
         return ds;
     }

     protected   void  TestBindData(){
        //======Test
       //DataSet ds   = TestSelectData();
       //GridView.DataSource = ds ;
       //GridView.DataBind();
    }

 


     protected int GetAgeRange(int Age) {
         if ((Age >= 18) && (Age < 25)) {
             Age = 18;
         } else if ((Age >= 25) && (Age < 36)) {
             Age = 25;
         } else if ((Age >= 36) && (Age < 51)) {
             Age = 36;
         } else if (Age >= 51) {
             Age = 51;
         }

         return Age;
     }
     

    /////////// Error   Problem 
    //  1. Access value method  acess varible in  other method
    //  2. how protect error if user not asign datetime   and we use  datetime cultureInfo
    //  3. Error  =>  if (Convert.ToInt32(strCarCode) == 110)  ???
    //  4. <%-- <%= (Request.QueryString["pol"] )== "1"?"id='current'":"";%> --%>  
    //  5. how to still control  view stage  if click  on Tab    => buyInsureCar_step1.aspx?pol=1
    // 6.  protected string strCarSize { get { return  txtCarCc.Text  ; } set { txtCarCc.Text = value; } }    ?????? ใช้ไง



     ///////// Get SetSendParamiter 
     //protected ArrayList SetSendParamiter(string _campaign, string strPolTyp, string _Carcode, string _CarSize) {

     //    ArrayList arlParamiter = new ArrayList();
     //    arlParamiter.Add(_campaign);
     //    arlParamiter.Add(strPolTyp);
     //    arlParamiter.Add(_Carcode);
     //    arlParamiter.Add(_CarSize);

     //    return arlParamiter;
     //}  
    // ArrayList atl = SetSendParamiter( strCamPaign, strPolTyp, strCarCode, strCarSize); ///// ?????????????


    /////////  Get Register Year 
    //int intTodayYear = Convert.ToInt32(DateTime.Today.ToString("yyyy", new CultureInfo("th-TH")) );
    //int intRegYear  =  Convert.ToInt32(ddlRegYear.SelectedValue) ;

    /////////  Get Car Age   
    // int intCarAge ;
    // int intDiffYear   = (intTodayYear - intRegYear) + 1;

    // if (intDiffYear > 10) {
    //     intCarAge = 99;
    // } else {
    //     intCarAge = intDiffYear ;
    // }


}//  end class