﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="investInfo_17.aspx.cs" Inherits="investInfo_17" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
<div class="subject1">รายงานประจำปี</div>

          <table class="tb-quarter"   >
        <tr>
            <th class="no-borderleft" width="30%"> ปี</th>
            <th colspan="2">
                รายงานประจำปี</th>
        </tr>
        <tr>
            <td width="218" class="no-borderleft">
                2555
            </td>
            <td width="64"  class="no-border-leftright" ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"  /> 
            </td>
            <td width="341" class="no-border-leftright   align-left" > <a href="downloads/investment/fannual-2555-1.pdf">สิ้นสุดวันที่ 31 ธันวาคม 2555 </a></td>
        </tr>
        <tr>
            <td class="no-borderleft">
                2554
            </td>
            <td class="no-border-leftright" ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"  /> 
              </td>
            <td class="no-border-leftright   align-left" > <a href="downloads/investment/fannual-2554-1.pdf">สิ้นสุดวันที่ 31 ธันวาคม 2554 </a>
            </td>
        </tr>
        <tr>
            <td class="no-borderleft">
                2553
            </td>
            <td class="no-border-leftright" ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"  /> 
              </td>
            <td class="no-border-leftright  align-left" valign="middle" > <a href="downloads/investment/fannual-2553-1.pdf">สิ้นสุดวันที่ 31 ธันวาคม 2553 </a>
            </td>
        </tr>
        <tr>
          <td class="no-borderleft"> 2552</td>
          <td class="no-border-leftright" ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"  /></td>
          <td class="no-border-leftright   align-left" ><a href="downloads/investment/fannual-2552-1.pdf">สิ้นสุดวันที่ 31 ธันวาคม 2552 </a>
          </td>
        </tr>
        <tr>
          <td class="no-borderleft">2551</td>
          <td class="no-border-leftright" ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"  /></td>
          <td class="no-border-leftright   align-left" ><a href="downloads/investment/fannual-2551-1.pdf">สิ้นสุดวันที่ 31 ธันวาคม 2551 </a></td>
        </tr>
        <tr>
          <td class="no-borderleft">2550</td>
          <td class="no-border-leftright" ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"  /></td>
          <td class="no-border-leftright   align-left" ><a href="downloads/investment/fannual-2550-1.pdf">สิ้นสุดวันที่ 31 ธันวาคม 2550 </a></td>
        </tr>
        <tr>
          <td class="no-borderleft">2549</td>
          <td class="no-border-leftright" ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"  /></td>
          <td class="no-border-leftright   align-left" ><a href="downloads/investment/fannual-2549-1.pdf">สิ้นสุดวันที่ 31 ธันวาคม 2549 </a></td>
        </tr>
        <tr>
          <td class="no-borderleft">2548</td>
          <td class="no-border-leftright" ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"  /></td>
          <td class="no-border-leftright   align-left" ><a href="downloads/investment/fannual-2548-1.pdf">สิ้นสุดวันที่ 31 ธันวาคม 2548</a></td>
        </tr>
     </table> 
   
<div class="notation2">
เอกสารเป็นไฟล์ PDF การเปิดดูจำเป็นต้องมีโปรแกรม  Adobe Reader คลิ๊กที่ไอคอนโปรแกรมเพื่อติดตั้ง   
<a href="http://www.adobe.com/products/acrobat/readstep2.html"
target="_blank"><img src="<%=Config.SiteUrl%>images/icon-pdf-download.gif"  />
</a>
</div>

</asp:Content>

