﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="networkMap.aspx.cs" Inherits="networkMap" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title> NetWork-Map</title> 
<link href="css/reset.css" rel="stylesheet" />
<link href="css/font.css" rel="stylesheet" />

<style>
/*=====Page NetworkMap ==================*/
#pageNetworkMap { width: 1000px; }
#pageNetworkMap  .subject1 { font-size: 23px; color: #0b36c9; margin: 8px 0px 5px 0px; font-family: 'RSULight'; clear: both; margin: 0px; border-bottom:1px solid  #ffde00 ; width: 950px ;height: 25px;margin-bottom:10px;  }
#pageNetworkMap .label {    text-align: right; padding-right: 3px; } 
#pageNetworkMap #networkDetail { font-size:13px;}
#pageNetworkMap  #networkDetail #colLeft { float: left; margin-right: 10px; width:640px;height:450px;
/*background:#f1fafe url('images/logo_smk_opacity.png' )  no-repeat  170px 170px;*/ 
background:  url('images/bg_pattern1.png' ) repeat-x  0px  0px; 
border:1px solid #ddd;  }
#pageNetworkMap  #networkDetail #colRight { float: left; width:300px;}
 
#pageNetworkMap ul {   padding-left: 30px; }
#pageNetworkMap ul li { list-style: disc; }  
</style>
 
    <script src="scripts/jquery-1.9.1.js"></script>
    <!--================ bxSlider Plugin (SlideShow)  -->
    <script src="<%=Config.SiteUrl%>scripts/jquery.bxslider/jquery.bxslider.js"></script>
    <link href="scripts/jquery.bxslider/jquery.bxslider.css" rel="stylesheet"></link>

    <script>
        $(function () {
            $('.bxslider').bxSlider({
                mode: 'fade',
                auto: true,
                speed: 500,
                randomStart: false,
                // tickerHover: true,
                controls: false,
                autoHover: true
            })

            /*=====BxSlide Config ==========*/
            $('.bxslider').css({ "left": -30 , "height":188});
            $('.bx-pager').css({ "display": "none" });
            $('.bxslider li').css({ "width": 300, "height": 188, "overflow": "hidden", "color":"#f1fafe" });

        });
    </script>
  


</head>
<body>
    <form id="form1" runat="server">

        <div id="pageNetworkMap">
             <div class="subject1">ข้อมูลเครื่อข่าย และแผนที่</div>
            <div id="networkDetail">
                <div id="colLeft">
                    <div id="areaMap" runat="server"></div>
                </div>
                <!--End id="colLeft"  -->

                <div id="colRight">
                    <!--###### bxSlider Image   ######## -->

                    <ul class="bxslider">
                        <li>
                            <asp:Image ID="imgPic1" runat="server" /></li>
                        <li>
                            <asp:Image ID="imgPic2" runat="server" /></li>
                    </ul>

                
 
<style> 
[id*=DetailsView] { border-collapse: collapse; border-spacing: 0; width:320px;  margin-top:10px; }
[id*=DetailsView] td {  background-color: #fff; padding: 1px;height:20px; 
                        vertical-align:top ; border : 1px solid #fff ; border-bottom: 1px dotted #EEE;    }
[id*=DetailsView] tr {   border-bottom: 1px dotted  red;    }
[id*=DetailsView] td:nth-child(1) { width:65px;   color:#0943AD; text-align:right; }
[id*=DetailsView] td:nth-child(2) { text-align:left;  } 
</style>

<asp:DetailsView ID="DetailsView1" runat="server"  OnDataBound="DetailsView1_DataBound" EnableModelValidation="True"   AutoGenerateRows="False" >
    <Fields>
        <asp:TemplateField HeaderText="ชื่อ :"   > 
            <ItemTemplate>
                <asp:Label ID="lblName" runat="server"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="ที่อยู่ :"> 
            <ItemTemplate>
                <asp:Label ID="lblAddress" runat="server"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField  HeaderText="จังหวัด :"> 
            <ItemTemplate>
                <asp:Label ID="lblProvince" runat="server"></asp:Label>
                 <asp:Label ID="lblAmphoe" runat="server"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
         
        <asp:TemplateField HeaderText="โทรศัพท์ :"> 
            <ItemTemplate>
                <asp:Label ID="lblPhone" runat="server"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="มือถือ :"> 
            <ItemTemplate>
                <asp:Label ID="lblMobile" runat="server"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
          
         <asp:TemplateField HeaderText="หมายเหตุ :"> 
            <ItemTemplate>
                <asp:Label ID="lblNotation" runat="server"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField> 

    </Fields> 

</asp:DetailsView>


 

                </div>
                <!--End id="colRight"  -->

            </div>
            <!-- End   id="networkDetail" -->



        </div>
        <!-- End   id="pageNetworkMap"  --> 

    </form>
</body>
</html>
