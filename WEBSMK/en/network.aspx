﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront1_en.master" AutoEventWireup="true" CodeFile="network.aspx.cs" Inherits="network" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- ===============  Modal  -->
    <script>
        $(function () {

            $("[id*=dialog]").dialog({
                autoOpen: false,
                show: "fade",
                hide: "fade",
                modal: true,
                //open: function (ev, ui) {
                //    $('#myIframe').src = 'http://www.google.com';
                //},
                //resizable: true,  
                //title: 'Title Topic',
                height: '550',
                width: '985',
                dialogClass: 'yourclassname',
                headerVisible: false,// or true
                draggable: false,
                buttons: { CLOSE: function () { $(this).dialog("close"); } }
            })

            ////  Set  Attr  Link For Iframe 
            $(".openerCover").click(function (e) {
                e.preventDefault();

                $("#areaIframe1").attr('src', $(this).attr('href'));

                $("#dialog1").dialog("open");
                //   return false;
            })

            ///  Hide Title Bar
            $(".ui-dialog-titlebar").hide();
            $("#dialog1").css("padding", 10);
        });
    </script>

        <!-- =============== Command Overlapping Tabs  -->
    <script>
        $(function () {

            cat = parseInt( <%=CatId%>);
            zone = parseInt( <%=ZoneId%>);

            //=========  Overlapping Tabs Manage 
            /// Show/Hide Overlapping Tab  ///
            //if (cat == 4) {
            //    $("#tabOvl-network1").show();
            //} else {
            //    $("#tabOvl-network2").show().siblings().hide();
            //}
            ///  Selected Tabs  ///
            $now = $('.obtabs li.item<%=ZoneId%>').attr("id", "current");

            /// Show/Hide Map For Category 4 /// 
            if (zone == 0) {
                $("#smkCenter").show();
            }

            /////// Select Map
            $("[id^=btnMap]").click(function () {
                elm = ($(this).attr("id"));
                elmId = elm.substring(6);
                $("[id^=mapSmk]").hide();
                $("#mapSmk" + elmId).show();
            });

        });
    </script>

    <style>
        /*===== Control  GridView  Width (support IE9 & Over) =====*/
        [id*=GridView] { width: 100%; /*border: 1px solid red;*/ }
        [id*=GridView] th { height: 20px; color: #fff; background-color: #485ed1; padding: 5px 5px 7px 5px; font-size: 13px; vertical-align: top; border: 1px solid #fff; }
        [id*=GridView] th:nth-child(1) { }
        [id*=GridView] th:nth-child(2) { width: 15%; text-align: center; }
        [id*=GridView] th:nth-child(3) { width: 10%; text-align: center; }
        [id*=GridView] td { padding: 2px 5px; height: 20px; line-height: 18px; vertical-align: top; border: 1px dotted #ddd; }
        [id*=GridView] td .name { }
        [id*=GridView] td:nth-child(2) { text-align: center; }
        [id*=GridView] td:nth-child(3) { text-align: center; }
        </style>

    <!-- ===============  Limit  Select DropDownList  -->
    <script>
        $(function () {
            ddlProvince = $("#<%=ddlProvince.ClientID%>");
            ddlAmphoe = $("#<%=ddlAmphoe.ClientID%>");
            //  areaAmphoe = $("#areaAmphoe");


            ddlProvince.change(function () {
                if (ddlProvince.val() != "กรุงเทพมหานคร") {
                    ddlAmphoe.val('');
                    //$("#areaAmphoe").hide();
                    $("#areaAmphoe").attr("style", "display:none");
                    //ddlAmphoe.attr("disabled", "disabled");
                } else {
                    //$("#areaAmphoe").show();
                    $("#areaAmphoe").attr("style", "display:block")
                    //ddlAmphoe.removeAttr("disabled");
                }
            })

        });

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div id="page-network">


        <h1 class="subject1">เครือข่าย 
            <asp:Label ID="lblPageName" runat="server"></asp:Label></h1>

        <!-- =========================================== Search Area  -->
        <div id="search-area">

            <div style="float: left; margin-right: 5px;">
                เลือกจังหวัด : </span><asp:DropDownList ID="ddlProvince" runat="server">
                    <asp:ListItem Value="">-- โปรดระบุ  --</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div style="display: none" id="areaAmphoe">
                <div style="float: left; margin-right: 5px;">
                    เลือกเขต :
                    <asp:DropDownList ID="ddlAmphoe" runat="server">
                        <asp:ListItem Value="">-- โปรดระบุ  --</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div style="float: left; margin-right: 5px;">
                เลือกคำค้น : 
                <asp:TextBox ID="tbxKeyWord" runat="server" Width="100px"></asp:TextBox>
            </div>
            <div style="float: left; margin-right: 5px;">
                <asp:Button ID="Button1" runat="server" Text="ค้นหา" class="btn-default" Style="height: 22px;" OnClick="BtnSearch_Click" />
            </div>


        </div>


        <div class="clear"></div>

        <!-- =========================================== Overlab Tab  -->
        <!--/// Tab Menu For Category 4 ///-->
         <% if(CatId!=4){ %>
        <style>
        #tabOvl-network1 ul.obtabs li { width: 100px; }
        </style>
         <%} %>

        <div id="tabOvl-network1"  >
            <ul class="obtabs">
                <% if(CatId==4){ %>
                <li class="item0"><span><a href="network.aspx?cat=<%=CatId%>&zone=0" rel="tabMenu0" name="grd">สำนักงาน<br />
                    ใหญ่</a></span></li>
                <%} %>
                <li class="item1"><span><a href="network.aspx?cat=<%=CatId%>&zone=1" rel="tabMenu1" name="grd">กรุงเทพฯ<br />
                    & ปริมณฑล</a></span></li>
                <li class="item2"><span><a href="network.aspx?cat=<%=CatId%>&zone=2" rel="tabMenu2" name="grd">กลาง</a></span></li>
                <li class="item3"><span><a href="network.aspx?cat=<%=CatId%>&zone=3" rel="tabMenu3" name="grd">ตะวันออก</a></span></li>
                <li class="item4"><span><a href="network.aspx?cat=<%=CatId%>&zone=4" rel="tabMenu4" name="grd">ตะวันออก<br />
                    เฉียงเหนือ</a></span></li>
                <li class="item5"><span><a href="network.aspx?cat=<%=CatId%>&zone=5" rel="tabMenu5" name="grd">เหนือ</a></span></li>
                <li class="item6"><span><a href="network.aspx?cat=<%=CatId%>&zone=6" rel="tabMenu6" name="grd">ใต้</a></span></li>
                <li class="item7"><span><a href="network.aspx?cat=<%=CatId%>&zone=7" rel="tabMenu7" name="grd">ตะวันตก</a></span></li>
            </ul>
        </div>
 
        <!--=======================================  GridView Panel   -->
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
            <ProgressTemplate>
                <asp:Label ID="lblProgress"
                    runat="server"
                    Text="">
                </asp:Label>
            </ProgressTemplate>
        </asp:UpdateProgress>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

                <asp:GridView ID="GridView1" runat="server" EnableModelValidation="True"
                    DataKeyNames="NtwId"
                    OnRowDataBound="GridView1_RowDataBound"
                    AutoGenerateColumns="false"
                    OnPageIndexChanging="GridView1_PageIndexChanging"
                    AllowPaging="True"
                    PageSize="3"
                    ShowHeader="true">


                    <Columns>

                        <asp:TemplateField HeaderText="ชื่อ-ที่อยู่">
                            <ItemTemplate>
                                <div class="rowAddress">

                                    <div class="name">
                                        <asp:Label runat="server" ID="lblName"></asp:Label>
                                        <asp:Label runat="server" ID="lblService"></asp:Label>
                                    </div>

                                    <div>
                                        <asp:Label runat="server" ID="lblAddress"></asp:Label></div>
                                    <div>
                                        <asp:Label runat="server" ID="lblMobile"></asp:Label></div>
                                    <div>
                                        <asp:Label runat="server" ID="lblPhone"></asp:Label></div>
                                    <div>
                                        <asp:Label runat="server" ID="lblNotation"></asp:Label></div>

                                </div>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="จังหวัด/อำเภอ">
                            <ItemTemplate>

                                <asp:Label runat="server" ID="lblProvince"></asp:Label>
                                <asp:Label runat="server" ID="lblAmphoe"></asp:Label>


                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="แผนที่">
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="hplMap"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

            </ContentTemplate>
        </asp:UpdatePanel>

        <!--========================================  SMK Center Branch  -->
        <!-- /// Map For Category 4 ///-->
        <table class="tb-network" width="100%" id="smkCenter" style="display: none">

            <tr>
                <td valign="top" class="align-left" colspan="3">

                    <div class="network-name">สำนักงานใหญ่ บริษัท สินมั่นคงประกันภัย จำกัด (มหาชน)</div>
                    <div class="network-address">ลำสาลี 1 เขต บางกะปิ บางกะปิ กรุงเทพมหานคร 10240</div>
                    <div class="network-tel">โทรศัพท์ : 02 378 7000 แฟกต์ : 02 378 7000  </div>

                    <br />

                    <input type="button" id="btnMap1" value="แผนที่ ขนาดย่อ" class="btn-default" style="margin-right: 10px;" />
                    <input type="button" id="btnMap2" value="แผนที่ Google Map" class="btn-default" />
                    <br />

                    <br />
                    <div id="mapSmk1" style="border: solid 1px #ddd;">
                        <img src="images/smkCenterMap2.jpg" width="620px" />
                    </div>

                    <div id="mapSmk2" style="display: none;">

                        <a href="https://www.google.co.th/maps/ms?msid=202246687830969586016.0004ea79fbe5e0d92f886&msa=0&ll=13.749807,100.645205&spn=0.008493,0.009645&iwloc=0004ea7a1c1f4b60d5eb9
" style="color: #0000FF; text-align: left" target="_blank">
                            <img src="images/mapSmk2.jpg" /></a>

                    </div>
                </td>
            </tr>
        </table>



        <!--======================================  Dialog PopUp -->
        <div id="dialog1" style="display: none; z-index: 1111; overflow: hidden">
            <iframe id="areaIframe1"   frameborder="0"  scrolling="no" width="1000px"   height="550px";></iframe>
        </div>



    </div>
    <!-- End  id="page-network"-->

</asp:Content>

