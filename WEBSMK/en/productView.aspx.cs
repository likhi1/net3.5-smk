﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;
 



public partial class viewPrd : System.Web.UI.Page {

    protected string CatId { get { return Request.QueryString["cat"]; } }
    protected string NewsId { get { return Request.QueryString["id"]; } }


    protected void Page_Load(object sender, EventArgs e) {

        //=====  Chk  Query String 
        if (CatId == null || NewsId == null) {
            Response.Redirect("product.aspx?cat=1");
        }
         

        if (!Page.IsPostBack) {
            BindData( NewsId , CatId ) ;
        }  

}

    protected void  BindData(string _ConId, string _CatId) {
        string sql1 = "select * from tbPrdCon Where PrdCatId='" + _CatId + "'  "
                    +"AND PrdConId='" + _ConId + "' "
                    +"AND  PrdConStatus='1'  "
                    + "AND lang = '0' ";

        DBClass obj = new DBClass();
        DataSet ds1 = obj.SqlGet(sql1, "_tbPrdCon");
        DataTable dt1 = ds1.Tables[0];

        //----------- check if no data in db
        if (dt1.Rows.Count > 0) {
            lblTitle.Text = dt1.Rows[0]["PrdConTopic"].ToString();
            ////// set Format DateTime
            DateTime dtDateShow = (DateTime)dt1.Rows[0]["PrdConDtShow"];
            lblDateShow.Text = "วันที่ : "+dtDateShow.ToString("d MMM yy", new CultureInfo("th-TH"));
           // lblDtShow.Text = dt1.Rows[0]["PrdConDtShow"].ToString();

            lblDetail.Text = dt1.Rows[0]["PrdConDetailLong"].ToString();  
            ////// check empty pic
            if (dt1.Rows[0]["PrdPicBig"] == "") {
                imgPrdPicBig.ImageUrl =  "images/blank_photo_big.jpg";
            } else {
               imgPrdPicBig.ImageUrl = dt1.Rows[0]["PrdPicBig"].ToString();
            } 
            
               string strPrdConId =  dt1.Rows[0]["PrdConId"].ToString(); 
                ////// Add Query String
                if (CatId == "1") { // Insure Health 
                    hplPrdBuy.NavigateUrl = "buyInsure.aspx?main_class=h";
                } else if (CatId == "2") { // Insure Car Voluntary
                  ///// Check PrdConId  for show hplPrdBuy
                    if (strPrdConId == "4" ) { 
                        hplPrdBuy.Visible = false;
                    } else {
                        hplPrdBuy.NavigateUrl = "buyVoluntary_step1.aspx";
                    }
                 
                } else if (CatId == "3") { // Insure Cancer
                    hplPrdBuy.NavigateUrl = "buyInsure.aspx?main_class=c";
                } else if (CatId == "4") {// Insure Accident 
                    hplPrdBuy.NavigateUrl = "buyInsurePA_step1.aspx";
                } else if (CatId == "5") { // Insure Fire
                    hplPrdBuy.NavigateUrl = "buyInsure.aspx?main_class=f";
                } else if (CatId == "6") { // Insure Travel
                    hplPrdBuy.NavigateUrl = "buyInsureTravel_step1.aspx";
                } else if (CatId == "7") { // Insure Other
                    hplPrdBuy.NavigateUrl = "buyInsure.aspx?main_class=o";
                } else if (CatId == "8") { // Insure Car Compulsary
                    hplPrdBuy.NavigateUrl = "buyCompulsary_step1.aspx";
                } else {
                    hplPrdBuy.NavigateUrl = "buyVoluntary_step1.aspx";
                }
             
             
        } else {
           Response.Redirect("product.aspx?cat=1");
        }
    }
    
     
}