﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_en.master" AutoEventWireup="true" CodeFile="product.aspx.cs" Inherits="product" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div id="page-prd-list">
        <h1 class="subject1">
            <asp:Label ID="lblPageName" runat="server"></asp:Label></h1>

        <%//// Check  Querystring  To Show  Car Insurance
            if (Convert.ToInt32(CatId) != 2) {
        %>

        <asp:DataList ID="DataList0" runat="server" OnItemDataBound="DataList_ItemDataBound">
            <ItemTemplate>
                <div class="product-row">
                    <div class="colLeft">
                        <asp:HyperLink ID="hplPrdId" runat="server"><asp:Image ID="lblPrdPic" runat="server" /></asp:HyperLink>
                        </div>
                    <div class="colRight">
                        <h2>
                            <asp:Label ID="lblPrdTopic" runat="server" Text="Text"></asp:Label></h2>
                        <div class="detailShort">
                            <asp:Label ID="lblPrdContent" runat="server" Text="Content"></asp:Label></div>
                       
                         <div class="btnRow">
 
<asp:HyperLink ID="hplPrdView" runat="server" class="btn-default  btn-iconMore">รายละเอียด<span></span> 
</asp:HyperLink>

<asp:HyperLink ID="hplPrdBuy" runat="server" class="btn-default   btn-iconOrder">สั่งซื้อ<span></span> 
</asp:HyperLink>
  
                        </div>
                        <!-- End class="btnRow" -->
                    </div>
                    <!-- End class="colRight" -->
                    <div class="clear"></div>
                </div>
                <!-- End class="product-row" -->
            </ItemTemplate>

        </asp:DataList>

        <%} else {    %>

        <h2 class="pName">ประกันชั้น 1</h2>
        <asp:DataList ID="DataList1" runat="server" OnItemDataBound="DataList_ItemDataBound">
            <ItemTemplate>

                <div class="product-row">
                    <div class="colLeft"> 

                         <asp:HyperLink ID="hplPrdId" runat="server"><asp:Image ID="lblPrdPic" runat="server" /></asp:HyperLink>
                    </div>
    <div class="colRight">
    <h2>
    <asp:Label ID="lblPrdTopic" runat="server" Text="Text"></asp:Label></h2>
    <div class="detailShort">
    <asp:Label ID="lblPrdContent" runat="server" Text="Content"></asp:Label></div>
    <div class="btnRow">
    <asp:HyperLink ID="hplPrdView" runat="server" class="btn-default  btn-iconMore">รายละเอียด<span></span></asp:HyperLink>

<asp:HyperLink ID="hplPrdBuy" runat="server" class="btn-default   btn-iconOrder">สั่งซื้อ<span></span> 
</asp:HyperLink>

    </div>
    <!-- End class="btnRow" -->
    </div>
                    <!-- End class="colRight" -->
                    <div class="clear"></div>
                </div>
                <!-- End class="product-row" -->
            </ItemTemplate>
        </asp:DataList>
        <h2 class="pName">ประกันชั้น 2</h2>
        <asp:DataList ID="DataList2" runat="server" OnItemDataBound="DataList_ItemDataBound">
            <ItemTemplate>
            
                <div class="product-row">
                    <div class="colLeft">
                             <asp:HyperLink ID="hplPrdId" runat="server"><asp:Image ID="lblPrdPic" runat="server" /></asp:HyperLink>
                     </div>
                    <div class="colRight">
                        <h2>
                            <asp:Label ID="lblPrdTopic" runat="server" Text="Text"></asp:Label></h2>
                        <div class="detailShort">
                            <asp:Label ID="lblPrdContent" runat="server" Text="Content"></asp:Label></div>
                        <div class="btnRow">
                            <asp:HyperLink ID="hplPrdView" runat="server" class="btn-default  btn-iconMore">รายละเอียด<span></span></asp:HyperLink>
                       <asp:HyperLink ID="hplPrdBuy" runat="server" class="btn-default   btn-iconOrder">สั่งซื้อ<span></span> 
</asp:HyperLink>

                        </div>
                        <!-- End class="btnRow" -->
                    </div>
                    <!-- End class="colRight" -->
                    <div class="clear"></div>
                </div>
                <!-- End class="product-row" -->
            </ItemTemplate>
        </asp:DataList>
        <h2 class="pName">ประกันชั้น 3</h2>
        <asp:DataList ID="DataList3" runat="server" OnItemDataBound="DataList_ItemDataBound">
            <ItemTemplate>
               
                <div class="product-row">
                    <div class="colLeft">
                         <asp:HyperLink ID="hplPrdId" runat="server"><asp:Image ID="lblPrdPic" runat="server" /></asp:HyperLink> 
                    </div>
                    <div class="colRight">
                        <h2>
                            <asp:Label ID="lblPrdTopic" runat="server" Text="Text"></asp:Label></h2>
                        <div class="detailShort">
                            <asp:Label ID="lblPrdContent" runat="server" Text="Content"></asp:Label></div>
                        <div class="btnRow">
                            <asp:HyperLink ID="hplPrdView" runat="server" class="btn-default  btn-iconMore">รายละเอียด<span></span></asp:HyperLink>
                       <asp:HyperLink ID="hplPrdBuy" runat="server" class="btn-default   btn-iconOrder">สั่งซื้อ<span></span> 
</asp:HyperLink>

                        </div>
                        <!-- End class="btnRow" -->
                    </div>
                    <!-- End class="colRight" -->
                    <div class="clear"></div>
                </div>
                <!-- End class="product-row" -->
            </ItemTemplate>
        </asp:DataList>

       <!--
        <h2 class="pName">ประกันชั้น 5</h2> 
        <asp:DataList ID="DataList4" runat="server" OnItemDataBound="DataList_ItemDataBound">
            <ItemTemplate>
              
                <div class="product-row">
                    <div class="colLeft">
                        <asp:Image ID="lblPrdPic" runat="server" /></div>
                    <div class="colRight">
                        <h2>
                            <asp:Label ID="lblPrdTopic" runat="server" Text="Text"></asp:Label></h2>
                        <div class="detailShort">fxit
                            <asp:Label ID="lblPrdContent" runat="server" Text="Content"></asp:Label></div>
                        <div class="btnRow">
                            <asp:HyperLink ID="hplPrdView" runat="server" class="btn-default  btn-iconMore">รายละเอียด<span></span></asp:HyperLink>
                       <asp:HyperLink ID="hplPrdBuy" runat="server" class="btn-default   btn-iconOrder">สั่งซื้อ<span></span> 
</asp:HyperLink>

                        </div> 
                    </div> 
                    <div class="clear"></div>
                </div> 
            </ItemTemplate>
        </asp:DataList> 
        -->

        <% 
 
} // end else %>
    </div>
    <!-- End id="page-prd-list" -->

</asp:Content>

