﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//--- connect
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
//--- arrayList
using System.Collections;



public partial class networkSearch : System.Web.UI.Page {

     public string CatId { get { return Request.QueryString["cat"]; } }
    ///===== Get Session From network.aspx
     public string Pro { get { return Convert.ToString(Session["pro"]); }   }
     public string Amp { get { return Convert.ToString(Session["amp"]); }   }
     public string Key { get { return Convert.ToString(Session["key"]); }   }


     // Label1.Text = Pro + " / " + Amp + " / " + Key + " / ";      // Check Session 
 
    protected void Page_Load(object sender, EventArgs e) { 
        //===== Run Bind Data  
        if (!Page.IsPostBack) { 
            ddlProvinceBindData();
            ddlAmphoeBindData();
            BindData();

 
        } else {

         
        }
    }

    protected void BtnSearch_Click(object sender, EventArgs e) {
       
        //=========  Set  Renew  Seession 
        Session["pro"] = ddlProvince.SelectedValue;
        Session["amp"] = ddlAmphoe.SelectedValue;
        Session["key"] = tbxKeyWord.Text;
       
        Response.Redirect("networkSearch.aspx?cat=" + CatId);
    }


    protected void BindData() {
        DataSet ds = SelectData( );
        DataTable dt1 = ds.Tables[0];

        int RowCount = dt1.Rows.Count;

        //=== Set Pager From code Behind
        GridView1.AllowPaging = true;
        GridView1.PageSize = 20;
        GridView1.PagerStyle.CssClass = "cssPager";

        //===== Bind  Data
        GridView1.DataSource = dt1;
        GridView1.DataBind();

    }

    //======================  Set Up  
 

    protected ArrayList ddlConSortAddList() {
        ArrayList arl = new ArrayList();
        for (int i = 1; i <= 10; i++) {
            arl.Add(i);
        } 
        return arl;
    }

    protected void ddlProvinceBindData() {
        string sql = "SELECT ds_major_cd , RTRIM(ds_desc_t) AS ds_desc_t  FROM setcode  WHERE (ds_major_cd = 'CH')  ORDER BY  ds_desc_t  ";

        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "tb");

        ddlProvince.DataSource = ds;
        //**** Add frist Item of dropdownlist
        ddlProvince.AppendDataBoundItems = true;
        ddlProvince.DataTextField = "ds_desc_t";
        //ddlProvince.DataValueField = "ds_desc_t";
        ddlProvince.DataBind();

    }
     
    protected void ddlAmphoeBindData() {
        string sql = "SELECT ds_major_cd , ds_desc , RTRIM(ds_desc_t) AS ds_desc_t  FROM setcode  WHERE (ds_major_cd = 'AM') AND (ds_desc = '0') ORDER BY  ds_desc_t  ";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "tb");

        ddlAmphoe.DataSource = ds;
        //**** Add frist Item of dropdownlist
        ddlAmphoe.AppendDataBoundItems = true;
        ddlAmphoe.DataTextField = "ds_desc_t";
        // ddlAmphoe.DataValueField = "ds_desc_t";
        ddlAmphoe.DataBind();

    }

    protected DataSet SelectData( ) {

        string sql1 = "SELECT tbNtw.* , tbNtwCat.NtwCatId ,   tbNtwZone.NtwZoneId "
        + "FROM  tbNtw "
        + "LEFT JOIN tbNtwCat ON tbNtw.NtwCatId  = tbNtwCat.NtwCatId "
        + "LEFT JOIN tbNtwZone ON tbNtw.NtwZoneId  = tbNtwZone.NtwZoneId  " 
        + "WHERE  tbNtw.NtwStatus ='1'   "
        + "AND  tbNtw.NtwCatId  ='" + CatId + "'  "
        + "AND lang = '0' ";  

        if (Pro != "") {
            //sql1 += "AND   tbNtw.NtwProvince  = '" + Pro + "'  ";
            sql1 += "AND (NtwName   LIKE  '%" + Pro + "%'  OR   "
                + "NtwProvince   LIKE  '%" + Pro + "%'   " 
                 + " )   "; 
        }

        if (Amp != "") {
           // sql1 += "AND   tbNtw.NtwAmphoe  = '" + Amp + "'  ";
            sql1 += "AND (NtwName   LIKE  '%" + Amp + "%'  OR   "
                  + "NtwProvince   LIKE  '%" + Amp + "%'  OR   "
                  + "NtwAmphoe    LIKE  '%" + Amp + "%'  OR   "
                  + "NtwAddress   LIKE  '%" + Amp + "%'  OR   "
                  + "NtwService   LIKE  '%" + Amp + "%' )   ";  
        }

        if (Key != "") {
            sql1 += "AND (NtwName   LIKE  '%" + Key + "%'  OR   "
                    + "NtwProvince   LIKE  '%" + Key + "%'  OR   "
                    + "NtwAmphoe    LIKE  '%" + Key + "%'  OR   "
                    + "NtwAddress   LIKE  '%" + Key + "%'  OR   "
                    + "NtwService   LIKE  '%" + Key + "%' )   "; 
        }
         
 sql1 +=  "ORDER BY tbNtw.NtwSort ASC , tbNtw.NtwName ASC ,   tbNtw.NtwId DESC   ";    // Sort By NtwSort  &   NtwName  &  NtwId  
 

        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql1, "tbNtwData");

        return ds;

    }
     

    //======================  Bind DropDown from  Database
    protected DataSet SelectZone() {
        string sql = "SELECT * From tbNtwZone";
        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql, "_tbNtwZone");
        return ds;
    }

     
    //========================  Event 

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e) {
        if (e.Row.RowType == DataControlRowType.DataRow) {

            Label lblName = (Label)(e.Row.FindControl("lblName"));
            if (lblName != null) {
                lblName.Text = DataBinder.Eval(e.Row.DataItem, "NtwName").ToString();
                lblName.Style.Add("font-weight", "bold"); 
            }

            Label lblAddress = (Label)(e.Row.FindControl("lblAddress"));
            if (lblAddress != null) {
                lblAddress.Text = DataBinder.Eval(e.Row.DataItem, "NtwAddress").ToString();
            }

            Label lblMobile = (Label)(e.Row.FindControl("lblMobile"));
            if (lblMobile != null) {
                string strMobile = DataBinder.Eval(e.Row.DataItem, "NtwTelMobile").ToString();
                lblMobile.Text = (strMobile != "" ? "มือถือ : " : "") + strMobile;
            }

            Label lblPhone = (Label)(e.Row.FindControl("lblPhone"));
            if (lblPhone != null) {
                string strPhone = DataBinder.Eval(e.Row.DataItem, "NtwTelPhone").ToString();
                lblPhone.Text = (strPhone != "" ? "โทรศัพท์ : " : "") + strPhone;
            }

            Label lblProvince = (Label)(e.Row.FindControl("lblProvince"));
            if (lblProvince != null) {
                lblProvince.Text = DataBinder.Eval(e.Row.DataItem, "NtwProvince").ToString();
                lblProvince.Attributes.Add("style", "text-align:center");
            }

            HyperLink hplMap = (HyperLink)(e.Row.FindControl("hplMap")); 

            if (!Convert.IsDBNull(hplMap)) {
                hplMap.Text = "ดูแผนที่";
                string strNtwId = DataBinder.Eval(e.Row.DataItem, "NtwId").ToString();
                hplMap.NavigateUrl = "networkMap.aspx?id=" + strNtwId;
                hplMap.CssClass = "openerCover";
                hplMap.Attributes.Add("style", "text-aligh:center"); 
            }
             

            Label lblService = (Label)(e.Row.FindControl("lblService"));
            string NtwService = DataBinder.Eval(e.Row.DataItem, "NtwService").ToString();
            if (!String.IsNullOrEmpty(NtwService)) {
                lblService.Text = "(" + NtwService + ")";
                lblService.Style.Add("color", "#F17503");
            }


            Label lblNotation = (Label)(e.Row.FindControl("lblNotation"));
            if (lblNotation != null) {
                string strNotation = DataBinder.Eval(e.Row.DataItem, "NtwNotation").ToString();
                lblNotation.Text = (strNotation != "" ? "หมายเหตุ : " : "") + strNotation;
            }

 

            Label lblAmphoe = (Label)(e.Row.FindControl("lblAmphoe"));
            if (lblAmphoe != null) {
                string strAmphoe = DataBinder.Eval(e.Row.DataItem, "NtwAmphoe").ToString();
                if (!String.IsNullOrEmpty(strAmphoe)) {
                    lblAmphoe.Text = "<br>"; 
                    lblAmphoe.Text += "(" + strAmphoe + ")";
                    lblAmphoe.Attributes.Add("style", "text-align:center");
                    //  lblAmphoe.Style.Add("color", "#038DFA");
                }
            }

        }
    }

    protected void GridView1_Deleting(object s, GridViewDeleteEventArgs e) {
        int a = e.RowIndex;
        string sql2 = "DELETE  FROM  tbNtw  WHERE NtwId = '" + GridView1.DataKeys[e.RowIndex].Value + "'";
        int i = new DBClass().SqlExecute(sql2);
        GridView1.EditIndex = -1;
        BindData();
    }

 

    //======================  Pager
    protected void GridView1_IndexChanging(object sender, GridViewPageEventArgs e) {
        GridView1.PageIndex = e.NewPageIndex;
        BindData();
    }


    //======================  Command Gridview

    protected void modEditCommand(object sender, GridViewEditEventArgs e) {
        GridView1.EditIndex = e.NewEditIndex;
        BindData();
    }

    protected void modCancelCommand(object sender, GridViewCancelEditEventArgs e) {
        GridView1.EditIndex = -1;
        BindData();
    }

    protected void modUpdateCommand(object sender, GridViewUpdateEventArgs e) {


        DropDownList ddlStatus = (DropDownList)GridView1.Rows[e.RowIndex].FindControl("ddlStatus");
        TextBox tbxSort = (TextBox)GridView1.Rows[e.RowIndex].FindControl("tbxSort");

        string strSQL = "UPDATE tbNtw SET " +

             "NtwStatus = '" + ddlStatus.Text + "', " +
             "NtwSort = '" + tbxSort.Text + "' " +
             " WHERE  NtwId = '" + GridView1.DataKeys[e.RowIndex].Value + "'";

        DBClass obj = new DBClass();
        int i = obj.SqlExecute(strSQL);

        GridView1.EditIndex = -1;
        BindData();
    }






  
}