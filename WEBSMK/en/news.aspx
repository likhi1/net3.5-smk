﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_en.master" AutoEventWireup="true" CodeFile="news.aspx.cs" Inherits="news" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div id="page-news-list"> 
<h1 class="subject1"><asp:Label ID="lblNewsCatName" runat="server" ></asp:Label></h1>

<asp:Repeater ID="RepeaterData" runat="server" OnItemDataBound="RepeaterData_ItemDataBound" > 
<ItemTemplate> 
<div class="news-row">
 
<h2> <asp:Label ID="lblTopic" runat="server" Text="Text"></asp:Label></h2> 
<div class="detailShort">
    
<asp:HyperLink ID="hplContent" runat="server">HyperLink</asp:HyperLink>
</div>
    
 
 
 
<div class="clear"></div>
</div>  <!-- End class="news-row" --> 
</ItemTemplate>
</asp:Repeater>



<!-- Start Paging --->         
<div style="position:relative">
    <div style="position:absolute; right:0px;">
<asp:Repeater ID="RepeaterPaging" runat="server" OnItemCommand="pagingRepeater_ItemCommand" >
<ItemTemplate>

    <div style="display:block;width:5px; float:left; margin-right:5px;">
<asp:LinkButton ID="LinkButton1" CommandArgument="<%#Container.DataItem %>"  runat="server"   > <%# Container.DataItem %></asp:LinkButton> 
 </div>

</ItemTemplate>
</asp:Repeater>
        </div>
 </div><!-- End Paging --->  

</div>
<!-- End id="page-prd-list" -->
</asp:Content>

