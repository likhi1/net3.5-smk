﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//--- connect
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
//--- arrayList
using System.Collections;


public partial class network : System.Web.UI.Page {
    public int CatId { get { return Convert.ToInt32(Request.QueryString["cat"]); } }
    public int ZoneId { get { return Convert.ToInt32(Request.QueryString["zone"]); } }


    protected void Page_Load(object sender, EventArgs e) {
        //========= Clear All Session 
        Session.RemoveAll();

        ddlProvinceBindData();
        ddlAmphoeBindData();
        BindData();

        /// Set PageName ///
        Utility obj = new Utility();
        string strPageName = obj.SetPageName(CatId.ToString());
        lblPageName.Text = strPageName; 
    }

    //============== Set Gridview
    protected void BindData() { 
        DataSet ds = SelectData(CatId, ZoneId);

        GridView1.AllowPaging = true;
        GridView1.PageSize = 20;
        GridView1.PagerStyle.CssClass = "cssPager";

        GridView1.DataSource = ds;
        GridView1.DataBind(); 

    } 

    protected DataSet SelectData(int ct, int zo) {
        string sql1 = "SELECT * FROM tbNtw "
                      + "WHERE NtwCatId='" + ct + "'  "
                      + "AND NtwZoneId='" + zo + "'   "
                      + "AND NtwStatus= '1' "
                      + "AND lang = '0' " 
                      + "ORDER BY NtwSort ASC , NtwName ASC ,   NtwId DESC   ";  // Sort By NtwSort  &   NtwName  &  NtwId
        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql1, "tbNtw");
        return ds;

    }
     
  
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e) {
 
         
            
          if (e.Row.RowType == DataControlRowType.DataRow) {

            Label lblName = (Label)(e.Row.FindControl("lblName"));
            if (lblName != null) {
                 lblName.Text = DataBinder.Eval(e.Row.DataItem, "NtwName").ToString();
                lblName.Style.Add("font-weight", "bold"); 
            }
         
            Label lblAddress = (Label)(e.Row.FindControl("lblAddress"));
            if (lblAddress != null) {
                lblAddress.Text = DataBinder.Eval(e.Row.DataItem, "NtwAddress").ToString();
            }

            Label lblMobile = (Label)(e.Row.FindControl("lblMobile"));
            if (lblMobile != null) {
                string strMobile = DataBinder.Eval(e.Row.DataItem, "NtwTelMobile").ToString();
                lblMobile.Text = (strMobile != "" ? "มือถือ : " : "") + strMobile;
            }

            Label lblPhone = (Label)(e.Row.FindControl("lblPhone"));
            if (lblPhone != null) {
                string strPhone = DataBinder.Eval(e.Row.DataItem, "NtwTelPhone").ToString();
                lblPhone.Text = (strPhone != "" ? "โทรศัพท์ : " : "") + strPhone;
            }

            Label lblProvince = (Label)(e.Row.FindControl("lblProvince"));
            if (lblProvince != null) {
                lblProvince.Text = DataBinder.Eval(e.Row.DataItem, "NtwProvince").ToString();
                lblProvince.Attributes.Add("style", "text-align:center");
            }


            Label lblService = (Label)(e.Row.FindControl("lblService"));
            string NtwService = DataBinder.Eval(e.Row.DataItem, "NtwService").ToString();
            if (!String.IsNullOrEmpty(NtwService)) {
                lblService.Text = "(" + NtwService + ")";
                lblService.Style.Add("color", "#F17503");
            }


            Label lblNotation = (Label)(e.Row.FindControl("lblNotation"));
            if (lblNotation != null) {
                string strNotation = DataBinder.Eval(e.Row.DataItem, "NtwNotation").ToString();
                lblNotation.Text = (strNotation != "" ? "หมายเหตุ : " : "") + strNotation;
            }

            Label lblAmphoe = (Label)(e.Row.FindControl("lblAmphoe"));
            if (lblAmphoe != null) {
                string strAmphoe = DataBinder.Eval(e.Row.DataItem, "NtwAmphoe").ToString(); 
                if (!String.IsNullOrEmpty(strAmphoe)) {
                    lblAmphoe.Text = "<br>";
                    lblAmphoe.Text += "(" + strAmphoe + ")";
                    lblAmphoe.Attributes.Add("style", "text-align:center"); 
                }
            }


            HyperLink hplMap = (HyperLink)(e.Row.FindControl("hplMap"));
            if (hplMap != null) {
                hplMap.Text = "ดูแผนที่";
                string strNtwId = DataBinder.Eval(e.Row.DataItem, "NtwId").ToString();
                hplMap.NavigateUrl = "networkMap.aspx?id=" + strNtwId;
                hplMap.CssClass = "openerCover";

            }

            
        }
        
          
    }
     
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e) {
        GridView1.PageIndex = e.NewPageIndex;
        BindData();
    }

     
    //=============  Set Option
    protected void ddlProvinceBindData() {
        string sql = "SELECT ds_major_cd , RTRIM(ds_desc_t) AS ds_desc_t  FROM setcode  WHERE (ds_major_cd = 'CH') ORDER BY  ds_desc_t ";

        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "tb");

        ddlProvince.DataSource = ds;
        //**** Add frist Item of dropdownlist
        ddlProvince.AppendDataBoundItems = true;
        ddlProvince.DataTextField = "ds_desc_t";
        //ddlProvince.DataValueField = "ds_desc_t";
        ddlProvince.DataBind();

    }

    protected void ddlAmphoeBindData() {
        string sql = "SELECT ds_major_cd , ds_desc , RTRIM(ds_desc_t) AS ds_desc_t  FROM setcode  WHERE (ds_major_cd = 'AM') AND (ds_desc = '0')  ORDER BY ds_desc_t  ";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "tb");

        ddlAmphoe.DataSource = ds;
        //**** Add frist Item of dropdownlist
        ddlAmphoe.AppendDataBoundItems = true;
        ddlAmphoe.DataTextField = "ds_desc_t";
        // ddlAmphoe.DataValueField = "ds_desc_t";
        ddlAmphoe.DataBind();

    }
      

    protected void BtnSearch_Click(object sender, EventArgs e) {
        //=========== Set New Session 
        Session["pro"] = ddlProvince.SelectedValue;
        Session["amp"] = ddlAmphoe.SelectedValue;
        Session["key"] = tbxKeyWord.Text;

        Response.Redirect("networkSearch.aspx?cat=" + CatId);

    }



}