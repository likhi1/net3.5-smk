﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---------- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;
using System.Collections;

public partial class jobRequire : System.Web.UI.Page{

    protected string Cat { get { return Request.QueryString["cat"] ; } }
    protected string Id { get { return Request.QueryString["id"]; } }
     
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            BindData();
            if (Cat == "1") {
                lblPageName.Text = "พนักงาน";
            } else if (Cat == "2") {
                lblPageName.Text = "ตัวแทน";
            } else {
                lblPageName.Text = "";
            }
        }

    }

    protected void  BindData(){
      DataSet ds =  SelectData();
      DataList1.DataSource = ds;
      DataList1.DataBind();
    }

    protected DataSet SelectData() {
        string sql = "SELECT * FROM tbJobPosi "
                    + "WHERE "
                    + "JobPosiStatus = '1' "
                    + "AND JobCatId = '"+Cat+"'  "
                    + "AND lang = '0' "
                    + "AND  (GETDATE() between JobPosiDtStart  and JobPosiDtStop )  "  // Check  Preriod Online  
                    + "ORDER BY  JobPosiSort ASC ,  JobPosiId DESC ";


        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql, "tb");
        return ds;
    }

 

     

    protected void DataList1_ItemDataBound(object sender, DataListItemEventArgs e) {

       Label    lblPosiName = (Label) (e.Item.FindControl("lblPosiName")) ;
       if(lblPosiName != null ){ 
           lblPosiName.Text =   DataBinder.Eval(  e.Item.DataItem , "JobPosiName"  ).ToString()  ;
           lblPosiName.Attributes.Add("style", "float:left");

       }

       Literal ltlPiority = (Literal)(e.Item.FindControl("ltlPiority"));
       if (ltlPiority != null) {
           int intPiority = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "JobPiority"));
           if (intPiority == 1) {
               ltlPiority.Text = "<span style='background:url(images/wanted.gif) no-repeat;  width: 40px;height: 20px; display: block;float: left;'></span> ";
           }

       } 


       Label lblPosiBranch = (Label)(e.Item.FindControl("lblPosiBranch"))  ;
       if (lblPosiBranch != null) {
          lblPosiBranch.Text = DataBinder.Eval(e.Item.DataItem, "JobPosiBranch").ToString();
           
       }

       Label lblQnt = (Label)(e.Item.FindControl("lblQnt")) ;
       if (lblQnt != null) {

         string   strQnt  = DataBinder.Eval(e.Item.DataItem, "JobPosiQnt").ToString();
         //======== Check Data Value Is String Or Int 
         int result ;
         if (int.TryParse(strQnt, out result )) { 
             lblQnt.Text = strQnt + " อัตรา";
         }else{
             lblQnt.Text = strQnt;
           }

       }



       Label lblDetail = (Label)(e.Item.FindControl("lblDetail")) ;
       if (lblDetail != null) {
           lblDetail.Text = DataBinder.Eval(e.Item.DataItem, "JobPosiDetail").ToString();
       }



       Button btnApply = (Button)(e.Item.FindControl("btnApply"))  ;
       if (lblPosiBranch != null) { 
           string strPosiId = DataBinder.Eval(e.Item.DataItem, "JobPosiId").ToString();
           if (Cat=="1") {
           btnApply.Attributes.Add("onclick", "location='"+Config.SiteUrl+"jobApply.aspx?cat="+Cat+"&id=" + strPosiId+"' ;return false; ");
           }else if (Cat=="2"){
           btnApply.Attributes.Add("onclick", "location='"+Config.SiteUrl+"jobApplyAgent.aspx?cat="+Cat+"&id=" + strPosiId+"' ;return false; ");          
           }else{ 

           }
       }


    }



}