﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_en.master" AutoEventWireup="true"
    CodeFile="faq.aspx.cs" Inherits="faq" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
 
    <script>
        $(function () {  
            $(".acc-bullet").addClass('acc-icon');    //////set start all icon  

            //================ Get Element by Jquery
            // $(this).children($(".acc-bullet")) //////////   Get All Children
            // $(this).children('.acc-bullet') //////////   Get One Children
            // $(this).children()[0] //////////   Get One Children
            // $(this).find($(".acc-bullet")); //////////   Get One Children     

            $(".acc-title").click(function () {
                //================ Set Var
                elmDetail = $(this).next();
                elmBullet = $(this).children()[0];
                elmDetailAll = $('.acc-detail');
                elmBulletAll = $('.acc-bullet');

                //=============== Set All
                elmDetailAll.slideUp(200);                elmBulletAll.removeClass('acc-icon2');
                elmBulletAll.addClass('acc-icon');
                //=============== Set Target Event
                if (elmDetail.is(':hidden')) {
                    elmDetail.slideDown(200);
                    elmBullet.removeClass('acc-icon');
                    elmBullet.addClass('acc-icon2');
                } else {
                    elmDetail.slideUp(200);
                    elmBullet.removeClass('acc-icon2');
                    elmBullet.addClass('acc-icon');

                }
            })
        }); //end ready
    </script>

    <div id="page-faq">
            <div class="subject1">ถาม-ตอบ</div>
        <div id="accordion">
            <asp:DataList ID="DataList1" runat="server" OnItemDataBound="DataList1_ItemDataBound">
<ItemTemplate>   

<div class="acc-row">
<div class="acc-title ">
<div class="acc-bullet"></div>  <div class="caption">ถาม : </div>
<div class="quiz">
    <asp:Label ID="lblQuiz" runat="server" Text="Label"></asp:Label>
</div>
    <div class="clear"></div>
</div><!-- end  class="acc-title" -->
<div class="acc-detail "> 
<div class="caption">ตอบ : </div>
<div class="answer">
    <asp:Label ID="lblAnswer" runat="server" Text="Label"></asp:Label>

</div>  
<div class="clear"></div>
</div> <!-- end  class="acc-detail" -->
             
</div>
            <!-- end class="acc-row" --> 
    </ItemTemplate>
    </asp:DataList>

        </div>
        <!-- end  id="accordion" -->

    </div>
    <!-- End id="page-faq"  -->
</asp:Content>
