﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;




public partial class newsView : System.Web.UI.Page {

    protected string CatId { get { return Request.QueryString["cat"];  } }
    protected string ConId { get {  return Request.QueryString["id"] ;} }

    protected void Page_Load(object sender, EventArgs e) {

        //=====  Chk  Query String 
        if (CatId == null ) {
           //Response.Redirect("news.aspx?cat=1");
        }  
         
        if (!Page.IsPostBack) { 
          //  BindData( NewsId, CatId );
            BindData( );
        }   
}

    //protected void  BindData(string _ConId, string _CatId) {
    protected void  BindData( ) {
                    string sql1 = "SELECT * FROM tbNewsCon "
                    + "WHERE NewsCatId='" + CatId + "'  " 
                    + "AND NewsConId='" + ConId + "' "
                    + "AND  NewsConStatus='1'  "
                    + "AND lang = '0'  ";// TH= 0  , En= 1

        DBClass obj = new DBClass();
        DataSet ds1 = obj.SqlGet(sql1, "tb");
        DataTable dt1 = ds1.Tables[0];

        //----------- check if no data in db
        if (dt1.Rows.Count > 0) {
            lblTitle.Text = dt1.Rows[0]["NewsConTopic"].ToString();
            ////// set Format DateTime
            DateTime dtDateShow = (DateTime)dt1.Rows[0]["NewsConDtShow"];
            lblDateShow.Text = "วันที่ : "+dtDateShow.ToString("d MMM yy", new CultureInfo("th-TH"));  
            lblDetail.Text = dt1.Rows[0]["NewsConDetailLong"].ToString();   

                ////// Content Is Ad Youtube ( Show  VDO )
               if(CatId  == "3") {
                   if (dt1.Rows[0]["NewsVdo"] != "") {
                       ltlVdo.Text = dt1.Rows[0]["NewsVdo"].ToString();
                   }
               ////// Content Is Agent & Content Is Static ( Not Show  Pic) 
               } else if (CatId == "4" || CatId == "5") {

               //////  Content Knowleage & News   ( Show  Pic  )
               } else {
                   if (dt1.Rows[0]["NewsPicBig"] == "")  
                    imgPrdPicBig.ImageUrl = "images/blank_photo_big.jpg";
                  else  
                    imgPrdPicBig.ImageUrl = dt1.Rows[0]["NewsPicBig"].ToString();
                    
               } // end else if


  
        } else {
            ///// Can't find  ConId
            Response.Redirect("home.aspx");
        }
    }
    
     
}