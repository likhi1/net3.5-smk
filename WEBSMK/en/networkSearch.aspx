﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_en.master" AutoEventWireup="true" CodeFile="networkSearch.aspx.cs" Inherits="networkSearch"    %>
 
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <!-- ===============  Modal  --> 
<style>
#areaIframe1{ width:1000px;height:550px; } 
</style>
<script>
    $(function () {
        $("#dialog1").css("padding", 0);

        $("[id*=dialog]").dialog({
            autoOpen: false,
            show: "fade",
            hide: "fade",
            modal: true,
            //open: function (ev, ui) {
            //    $('#myIframe').src = 'http://www.google.com';
            //},
            //resizable: true,  
            //title: 'Title Topic',
            height: '570',
            width: '1000',
            dialogClass: 'yourclassname',
            headerVisible: false,// or true
            draggable: false,  
            buttons: { CLOSE: function () { $(this).dialog("close"); } }

        })

        ////  Set  Attr  Link For Iframe
        areaIframe1 = $("#areaIframe1");
        $(".openerCover").click(function (e) {
            e.preventDefault();

            link = $(this).attr('href');
            areaIframe1.attr('src', link);

            $("#dialog1").dialog("open");
            //   return false;
        })

        ///  Hide Title Bar
        $(".ui-dialog-titlebar").hide();
        $("#dialog1").css("padding", 10);

    });
</script>  

             <!-- ===============  Limit  Select DropDownList  -->
     <script>
         $(function () {
             ddlProvince = $("#<%=ddlProvince.ClientID%>");
             ddlAmphoe = $("#<%=ddlAmphoe.ClientID%>");
        
             ddlProvince.change(function () {
                 if (ddlProvince.val() != "กรุงเทพมหานคร") {
                     ddlAmphoe.val('');
                     //$("#areaAmphoe").hide();
                     $("#areaAmphoe").attr("style", "display:none");
                     //ddlAmphoe.attr("disabled", "disabled");
                 } else {
                     //$("#areaAmphoe").show();
                     $("#areaAmphoe").attr("style", "display:block")
                     //ddlAmphoe.removeAttr("disabled");
                 }
             })
         });
   </script> 

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
<div id="page-network">
<h1 class="subject1">ผลการค้นหา </h1> 

<%--<asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>--%>
 
<div id="search-area">
            
            <div style="float:left;margin-right:5px;">
เลือกจังหวัด : </span><asp:DropDownList ID="ddlProvince" runat="server"  >  <asp:ListItem Value="">-- โปรดระบุ  --</asp:ListItem></asp:DropDownList>  
</div> 
<div style="float:left;margin-right:5px; display:none" id="areaAmphoe">
     <div  style="float:left;margin-right:5px;  " >
เลือกเขต : <asp:DropDownList ID="ddlAmphoe" runat="server">  <asp:ListItem Value="">-- โปรดระบุ  --</asp:ListItem></asp:DropDownList>  
</div> 
    </div> 
<div style="float:left;margin-right:5px;">
เลือกคำค้น :  <asp:TextBox ID="tbxKeyWord" runat="server" Width="100px"  ></asp:TextBox>   
</div> 
<div style="float:left;margin-right:5px;">  
<asp:Button ID="Button1" runat="server" Text="ค้นหา" class="btn-default" style="height:22px;"  OnClick="BtnSearch_Click"  /> 
</div> 
         

        </div>
        <!--  End id="search-area" -->

        <div class="clear"></div>
 


        <style>
        /*===== Control  GridView  Width (support IE9 & Over) =====*/
        [id*=GridView] { width:100% ;}
        [id*=GridView] th {height:20px;   color:#fff ;background-color:#485ed1; padding:5px 5px 7px 5px; font-size:13px;  vertical-align:top ;  border : 1px solid #fff;  } 
        [id*=GridView] th:nth-child(1) {   }
        [id*=GridView] th:nth-child(2)  { width: 15%;  text-align:center ;  }
        [id*=GridView] th:nth-child(3)  { width: 10%;  text-align:center ;  } 
        [id*=GridView] td {padding:2px 5px;height:20px; line-height: 18px; vertical-align:top;  border : 1px dotted #ddd;    }
        [id*=GridView] td .name{  }
        [id*=GridView] td:nth-child(2) {text-align:center ; }
        [id*=GridView] td:nth-child(3) {text-align:center ; }
         
        </style>

            <asp:GridView ID="GridView1" runat="server" EnableModelValidation="True"
                 AutoGenerateColumns="false" 
            DataKeyNames="NtwId"
            OnRowDataBound="GridView1_RowDataBound" 
            OnRowDeleting="GridView1_Deleting"
            OnPageIndexChanging="GridView1_IndexChanging"
            OnRowEditing="modEditCommand"
            OnRowUpdating="modUpdateCommand"
            OnRowCancelingEdit="modCancelCommand"  >

                        <Columns>

                            <asp:TemplateField   HeaderText="ชื่อ-ที่อยู่" >
<ItemTemplate>
<div class="rowAddress">
<div  class="name"><asp:Label runat="server" ID="lblName"></asp:Label>
<asp:Label runat="server" ID="lblService"></asp:Label> 
</div> 
                                     
<div  ><asp:Label runat="server" ID="lblAddress"></asp:Label></div>
<div  ><asp:Label runat="server" ID="lblMobile"></asp:Label></div>
<div  ><asp:Label runat="server" ID="lblPhone"></asp:Label></div>
<div  ><asp:Label runat="server" ID="lblNotation"></asp:Label></div>

</div>
</ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField   HeaderText="จังหวัด/อำเภอ" >
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblProvince"></asp:Label> 

                                    <asp:Label runat="server" ID="lblAmphoe"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField    HeaderText="แผนที่" >
                                <ItemTemplate>
                                    <asp:HyperLink runat="server" ID="hplMap"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField> 

                        </Columns>
                    </asp:GridView>


<div id="dialog1" style="display:none; z-index:1111; overflow:hidden"     >
  <iframe id="areaIframe1"   frameborder="0"  scrolling="no"  ></iframe>
</div>

    </div>
    <!-- End  id="page-network"-->

</asp:Content>

