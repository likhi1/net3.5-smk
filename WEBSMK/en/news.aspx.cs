﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;
using System.Collections;


public partial class news : System.Web.UI.Page
{ 
    //=======================  Global Varible  
    protected string CatId { get { return Request.QueryString["cat"] ;} }
   
        

    protected void Page_Load(object sender, EventArgs e) {

        if (!IsPostBack) {
            //// Check if no Querystring 
            if (CatId != null)
                SelectData(CatId);
            else
                Response.Redirect("news.aspx?cat=1");
        }

    }

     
    //=======================  Event 
    protected void RepeaterData_ItemDataBound(object sender, RepeaterItemEventArgs e) {

        if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem) {

            Label lblNewsTopic = (Label)e.Item.FindControl("lblTopic");
            if (lblNewsTopic != null) {
                lblNewsTopic.Text = DataBinder.Eval(e.Item.DataItem, "NewsConTopic").ToString();
            }

            Image lblNewsPic = (Image)e.Item.FindControl("lblPic");
            if (lblNewsPic != null) {

                //////  Set Show Pic
                string strPicSmall = DataBinder.Eval(e.Item.DataItem, "NewsPicSmall").ToString();
                if (strPicSmall != "") {
                    lblNewsPic.ImageUrl = strPicSmall;
                } else {
                    lblNewsPic.ImageUrl = "images/blank_photo_small.jpg";
                }
            }

            HyperLink  hplContent = (HyperLink)e.Item.FindControl("hplContent");
            if (hplContent != null) {
                hplContent.Text = DataBinder.Eval(e.Item.DataItem, "NewsConDetailShort").ToString();
                string strId = DataBinder.Eval(e.Item.DataItem, "NewsConId").ToString();
                hplContent.NavigateUrl = "newsView.aspx?cat=" + CatId + "&id=" + strId;
            }

           

        }// end if (e.Item. 

    }// end Method  


    //======================= Pager 
    //=======  Get-Set Varible  (Now PageNumber From ViewStage)
    public int PageNumber {
        get {
            if (ViewState["PageNumber"] != null) {
                return Convert.ToInt32(ViewState["PageNumber"]);
            } else {
                return 0;
            } 
        } 
        set {
            ViewState["PageNumber"] = value; 
        }

    } 

    //=======  Load Data
    protected void SelectData(string _CatId) {

         string sql1 = "SELECT    tbNewsCon.* , tbNewsCat.NewsCatName  "
                    + "FROM  tbNewsCon  "
                    + "LEFT JOIN tbNewsCat  ON  tbNewsCon.NewsCatId   =  tbNewsCat.NewsCatId  "
                    + "WHERE "
                    + "tbNewsCon.NewsConStatus ='1'  " // Check Status Content
                    + "AND  tbNewsCon.NewsCatId ='" + _CatId + "'  " 
                    + "AND  (GETDATE() between tbNewsCon.NewsConDtShow and tbNewsCon.NewsConDtHide )  "  // Check  Preriod Online 
                    + "ORDER BY   tbNewsCon.NewsConSort    ASC ,   tbNewsCon.NewsConId  DESC "; // Sort by  PrdConSort &PrdConId 


 
        // , a.NewsCatId  DESC";
      
        DBClass obj = new DBClass();
        DataSet ds1 = obj.SqlGet(sql1, "_tbNewsCon");
        DataTable dt1 = ds1.Tables[0];
  
          
        //****** Set Data 
         
        //// Add CatName 
        if (dt1.Rows.Count > 0) {
            lblNewsCatName.Text = dt1.Rows[0]["NewsCatName"].ToString();
        }


            //// Bind  to PageDataSource 
            PagedDataSource pgitems = new PagedDataSource();
            DataView dv = new DataView(dt1);
            pgitems.DataSource = dv; 
            ////// Config Paging  
            pgitems.AllowPaging = true;
            pgitems.PageSize = 12;
            pgitems.CurrentPageIndex = PageNumber;

            //******** Set Paging
            if (pgitems.PageCount > 1) {
                ///// Set rptPages Webcontrol 
                RepeaterPaging.Visible = true;
                ArrayList pages = new ArrayList();
                for (int i = 0; i < pgitems.PageCount; i++)
                    pages.Add((i + 1).ToString());
                //// Bind paging 
                RepeaterPaging.DataSource = pages;
                RepeaterPaging.DataBind();
            } else {
                ///// Set rptPages Webcontrol  ( can't count  = 1 Page  )
                RepeaterPaging.Visible = false;
            }
            ///// Bind Content
            RepeaterData.DataSource = pgitems;
            RepeaterData.DataBind(); 

    }
     
    //=======  pagingRepeater ItemCommand
    protected void pagingRepeater_ItemCommand(object source, RepeaterCommandEventArgs e) {
        PageNumber = Convert.ToInt32(e.CommandArgument) - 1;
        SelectData(CatId);
    }





}