﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_en.master" AutoEventWireup="true" CodeFile="callcenter.aspx.cs" Inherits="callcenter" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <title>สินมั่นคงประกันภัย - เราประกัน..คุณมั่นใจ - ทำประกันภัยออนไลน์ ต่ออายุประกันภัยออนไลน์</title>
      <!-- ===============  Date  Picker - Config -->
    <script>
        $(function () {
            $("[id*=tbxDate]").attr('readOnly', 'true');
            $("[id*=tbxDate]").datepicker({
              changeMonth: true,
              //  changeYear: true,
               // yearRange: "-0:+10",
                isBE: true,
                autoConversionField: false
            })

        });
    </script>

    <!-- ===============  Validate  -->

     <script> 
         $(function () {
              
             $("#<%=btnSubmit.ClientID%>").click(function () {

                    tbxName = $("#<%=tbxName.ClientID%>").val();
                    tbxTel = $("#<%=tbxTel.ClientID%>").val();
                    tbxEmail = $("#<%=tbxEmail.ClientID%>").val();
                    txrDetail = $("#<%=txrDetail.ClientID%>").val();
                    tbxDateGet = $("#<%=tbxDateGet.ClientID%>").val();
                    emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
               
                    if (tbxName == "" || tbxTel == "" || tbxEmail == "" || txrDetail == "" || tbxDateGet == "") {
                    alert("กรุณาป้อนข้อมูลให้ครบก่อนค่ะ");
                    } else if (!emailReg.test(tbxEmail)) {
                    alert("ป้อนอีเมล์ไม่ถูกต้องค่ะ");
                    $("#<%=tbxEmail.ClientID%>").focus();
                    } else {
                    return true;
                    }

              return false;  
            }) 
    });
  
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    
 
<div   id="page-callcenter"   >
<h1 class="subject1">ให้บริษัทฯ ติดต่อกลับ</h1>  
 โปรดกรอกรายละเอียด หากท่านประสงค์จะให้ทางบริษัท เมื่อทางบริษัทได้รับข้อมูลแล้ว จะรีบทำการติดต่อกลับไป
 
 <br /><br />
 <table    > 
  
     <tr>
            <td class="label"   >
                ชื่อลูกค้า : 
                </td>
            <td  >
                <asp:TextBox ID="tbxName" runat="server" Width="300px"></asp:TextBox> <span class="star">*</span>
            </td>
        </tr>
        
        <tr>
            <td class="label"    >
                เบอร์โทรศัพท์ : 
                </td>
            <td  >
                <asp:TextBox ID="tbxTel" runat="server"  Width="300px"></asp:TextBox> <span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td class="label"     >
                อีเมล์ :</td>
            <td  >
                <asp:TextBox ID="tbxEmail" runat="server" Width="300px"></asp:TextBox> <span class="star">*</span>
            </td>
        </tr>

        <tr>
            <td class="label"    valign="top"  >
                เรื่องที่ต้องการติดต่อ :</td>
            <td  style="vertical-align:top" >
                <asp:TextBox ID="txrDetail" runat="server" TextMode="MultiLine" Width ="300px"  Height="100px"></asp:TextBox> <span class="star">*</span>
            </td>
        </tr>
       
         <tr>
            <td class="label"   >
                วันที่ติดต่อกลับ :</td>
            <td  >
                

                <asp:TextBox ID="tbxDateGet" runat="server"  Width="300px"></asp:TextBox> <span class="star">*</span>
            </td>
        </tr>
         <tr>
            <td class="label"   >
                เวลาติดต่อกลับ :</td>
            <td  >
                

                    <asp:DropDownList ID="ddlTimeHours" runat="server" Width="50px" ></asp:DropDownList> : 
                <asp:DropDownList ID="ddlTimeMinute" runat="server" Width="50px"></asp:DropDownList>

                น.
            </td>
        </tr>
    
         </table>
  
  <div class="btn-submit">
<asp:Button ID="btnSubmit" runat="server"   Text="ยืนยัน"   CssClass="btn-default" style="margin-right:8px;" OnClick="btnSubmit_Click"  />
<asp:Button ID="btnReset" runat="server"  Text="เคียร์"      CssClass="btn-default"   UseSubmitBehavior="false" OnClick="btnReset_Click" />
</div>


 
</div><!-- End  id="page-callcenter" --> 

 
</asp:Content>

