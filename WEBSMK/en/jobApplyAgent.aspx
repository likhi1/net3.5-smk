﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_en.master" AutoEventWireup="true" CodeFile="jobApplyAgent.aspx.cs" Inherits="jobApplyAgent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
         <!-- ===============  Date  Picker - Config -->
    <script>
        $(function () {
            $("[id*=tbxDate]").attr('readOnly', 'true');
            $("[id*=tbxDate]").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "-70:+0",
                isBE: true,
                autoConversionField: false
            }) 
        });
    </script>

    

       <!-- ===============  Validate  -->

     <script>
         $(function () {
             //// Set Varbles
             ddlPosition = $("#<%=ddlPosition.ClientID%>");
             ddlNamePrefix = $("#<%=ddlNamePrefix.ClientID%>"); 
            tbxName1 = $("#<%=tbxName1.ClientID%>");
            tbxName2 = $("#<%=tbxName2.ClientID%>");
            tbxDateBrith = $("#<%=tbxDateBrith.ClientID%>");
            ddlAge = $("#<%=ddlAge.ClientID%>");
            tbxIdCard = $("#<%=tbxIdCard.ClientID%>");
            tbxTel1 = $("#<%=tbxTel1.ClientID%>");
            tbxEmail = $("#<%=tbxEmail.ClientID%>");
            tarAddress1 = $("#<%=tarAddress1.ClientID%>");
            tarAddress2 = $("#<%=tarAddress2.ClientID%>"); 
            btnSubmit = $("#<%=btnSave.ClientID%>");

             btnSubmit.click(function () {
                 return ChkNull();
             })

         });

         function ChkNull() {
             if (ddlPosition.val() == "" || ddlPosition.val() == null) {
                 alert("กรุณาระบุตำแหน่งงานด้วย");
                 return false;
             } else if (ddlNamePrefix.val() == "" || ddlNamePrefix.val() == null) {
                 alert("กรุณาระบุคำนำหน้า");
                 return false;
             } else if (tbxName1.val() == "" || tbxName1.val() == null) {
                 alert("กรุณาระบุชื่อ");
                 return false;
             } else if (tbxName2.val() == "" || tbxName2.val() == null) {
                 alert("กรุณาระบุนามสกุล");
                 return false; 
              } else if (tbxDateBrith.val() == "" || tbxDateBrith.val() == null) {
                 alert("กรุณาระบุวันเกิดด้วย");
                 return false;
             } else if (ddlAge.val() == "" || ddlAge.val() == null) {
                 alert("กรุณาระบุอายุด้วย");
                 return false;
             } else if (tbxIdCard.val() == "" || tbxIdCard.val() == null) {
                 alert("กรุณาระบุเลขที่บัตรประชาชนด้วย");
                 return false;
             } else if (tbxTel1.val() == "" || tbxTel1.val() == null) {
                 alert("กรุณาระบุเบอร์มือถือด้วย");
                 return false;
             } else if (tbxEmail.val() == "" || tbxEmail.val() == null) {
                 alert("กรุณาระบุอีเมลด้วย");
                 return false;
             } else if (tarAddress1.val() == "" || tarAddress1.val() == null) {
                 alert("กรุณาระบุที่อยู่ปัจจุบันที่ติดต่อได้ด้วย");
                 return false;
             } 

             return true;
         }

    </script>




</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    
<div id="page-jobProfile">
<div class="subject1">สมัครตัวแทนออนไลน์</div>


<style>
#page-jobProfile { }
#page-jobProfile table td { vertical-align: top; }
#page-jobProfile .label { text-align: right; padding-right: 5px; vertical-align: top; }
#page-jobProfile .tbPastWork td:nth-child(2) { border-right: 1px dotted #ccc; }
 
</style>


<table style="width: 100%;">
<tr>
<td class="label" width="130px">
ตำแหน่ง :
</td>
    <td>   <asp:DropDownList ID="ddlPosition" runat="server">   
 </asp:DropDownList> <span class="star">*</span>
</td>
</tr>
</table>

     <table style="width: 100%;">

        <table style="width: 100%;">
            <tr>
                <td>
                    <table width="100%" id="Table2">
                        <tr>
                            <td class="label" width="130px">คำนำหน้า :</td>
                            <td>

                                <asp:DropDownList ID="ddlNamePrefix" runat="server">  
                                    <asp:ListItem Selected="True" Value="">-- กรุณาระบุ --</asp:ListItem>
                                    <asp:ListItem>นาย</asp:ListItem>
                                    <asp:ListItem>นาง</asp:ListItem>
                                    <asp:ListItem>นางสาว</asp:ListItem>
                                </asp:DropDownList> <span class="star">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">ชื่อ
        :</td>
                            <td>


                                <asp:TextBox ID="tbxName1" runat="server"></asp:TextBox> <span class="star">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">นามสกุล
        :
                            </td>
                            <td>


                                <asp:TextBox ID="tbxName2" runat="server"></asp:TextBox> <span class="star">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">วันเกิด :</td>
                            <td>
                                <asp:TextBox ID="tbxDateBrith" runat="server"></asp:TextBox> <span class="star">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">อายุ :</td>
                            <td>

                                <asp:DropDownList ID="ddlAge" runat="server">
                                </asp:DropDownList> 

                                ปี <span class="star">*</span></td>
                        </tr>

                        </table>
                </td>
                <td>
                     <table width="100%" id="Table1">
                         <tr>
                            <td class="label">เลขที่บัตรประชาชน :</td>
                            <td>
                                <asp:TextBox ID="tbxIdCard" runat="server"></asp:TextBox> <span class="star">*</span></td> 
                         </tr>
                         <tr>
                            <td class="label">เบอร์มือถือ :</td>
                            <td>
                                <asp:TextBox ID="tbxTel1" runat="server"></asp:TextBox> <span class="star">*</span>

                            </td>
                         </tr>
                         <tr>
                            <td class="label">เบอร์บ้าน :</td>
                            <td>
                                <asp:TextBox ID="tbxTel2" runat="server"></asp:TextBox>

                            </td>
                         </tr>
                         <tr>
                            <td class="label">อีเมลล์ :</td>
                            <td>
                                <asp:TextBox ID="tbxEmail" runat="server"></asp:TextBox> <span class="star">*</span></td>
                         </tr>
                        <tr>
                            <td class="label">&nbsp;</td>
                            <td>


                                &nbsp;</td>
                        </tr>
                        
                        </table>
                </td>

            </tr>
        </table>

        <table width="100%" id="TbProfile">

            <tr>
                <td class="label" width="130px">ที่อยู่ปัจจุบันที่ติดต่อได้ :</td>
                <td>

                    <asp:TextBox ID="tarAddress1" runat="server" TextMode="MultiLine" Style="width: 465px"></asp:TextBox> <span class="star">*</span>

                </td>
            </tr>
            <tr>
                <td class="label">ที่อยู่ตามบัตรประชาชน :</td>
                <td>
                    <asp:TextBox ID="tarAddress2" runat="server" TextMode="MultiLine" Style="width: 465px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label">ข้อมูลอื่นๆเพิ่มเติม :<br />
                    (เพื่อแนะนำตัวท่าน)</td>
                <td>
                    <asp:TextBox ID="tarMoreInfo" runat="server" TextMode="MultiLine" Style="width: 465px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label">&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
      
        <div style="margin: 0px auto; width: 170px;height:50px;">

            <asp:Button ID="btnSave" runat="server" Text="ส่งใบสมัครงาน" class="btn-default btn-small " Style="margin-right: 5px;" OnClick="btnSave_Click" />
            <asp:Button ID="Button2" runat="server" Text="ยกเลิก" class="btn-default btn-small " />


        </div>



    </div>
    <!-- End id ="page-jobProfile"  -->


</asp:Content>

