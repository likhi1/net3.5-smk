﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---------- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;
using System.Collections;
 

public partial class jobApplyAgent : System.Web.UI.Page {
    protected string Cat { get { return Request.QueryString["cat"]; } }
    protected string Id { get { return Request.QueryString["id"]; } }
    protected string stringObject = "";

    protected void Page_Load(object sender, EventArgs e) {
        if (!Page.IsPostBack) {
            //======= Setup =========
            Setposition();
            SetAge();
            ddlPosition.SelectedValue = Id; 
        }
    }
     
   
    protected String SetConcatString(string[] ArrayControl) {
        foreach (String val in ArrayControl) {
            stringObject += val + "|";
        }
        return stringObject;
    }

    void Setposition() {
        string sql = "SELECT * FROM tbJobPosi WHERE  JobCatId = '" + Cat + "'  "
        +"AND  JobPosiStatus  ='1' "
        // + "AND lang = '0' " 
        +"ORDER BY JobPosiId   ";
        
        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql, "tb");
        ddlPosition.DataSource = ds;
        ddlPosition.AppendDataBoundItems = true;
        ddlPosition.Items.Insert(0, new ListItem("-- กรุณาระบุ --", ""));

        //==============
        ddlPosition.DataTextField = "JobPosiName";
        ddlPosition.DataValueField = "JobPosiId";
        ddlPosition.DataBind(); 

    }

    protected void btnSave_Click(object sender, EventArgs e) {

        string strPosition = ddlPosition.SelectedValue;
        string strPrfSex = ddlNamePrefix.SelectedValue; 
        string strName1 = tbxName1.Text;
        string strName2 = tbxName2.Text;
        string strBirth  =  tbxDateBrith.Text;
        object dtBirth = (string.IsNullOrEmpty(strBirth) ? (object)"" : Convert.ToDateTime(strBirth, new CultureInfo("th-TH")));   
        string strAge = ddlAge.SelectedValue;
 
        string strEmail = tbxEmail.Text;
        
        string strIdCard = tbxIdCard.Text;
        string strTel1 = tbxTel1.Text;
        string strTel2 =  tbxTel2.Text;
        string strAddress1 = tarAddress1.Text;
        string strAddress2 = tarAddress2.Text; 
        string strMoreInfo = tarMoreInfo.Text;
       

        string sql = "INSERT INTO  tbJobPrf VALUES ( "
          + "@JobCatId , @JobPrfPosition1,@JobPrfPosition2,@JobPrfPosition3,@JobPrfSaraly,@JobPrfSex, "
          + "@JobPrfName1,@JobPrfName2, @JobPrfBirth,@JobPrfAge,@JobPrfIdCard,@JobPrfNationality, "
          + "@JobPrfReligion,@JobPrfWeight,@JobPrfHeight,@JobPrfMilitary,@JobPrfAddress1,@JobPrfAddress2,  "
          + "@JobPrfTel1, @JobPrfTel2,@JobPrfEmail,@JobPrfEdu1 ,@JobPrfEdu2,@JobPrfEdu3,  "
          + "@JobPrfEduActivity,@JobPrfSkLang1,@JobPrfSkLang1,@JobPrfSkCom,@JobPrfSkOther, "
          + "@JobPrfPastWork1,@JobPrfPastWork2,@JobPrfMoreInfo,@JobPrfDateReg,@JobPrfStatus "
          + " ) ";

      
         
       // string.IsNullOrEmpty(strBirth)?  (object)"" : Convert.ToDateTime(strBirth, new CultureInfo("th-TH")) ) 

        SqlParameterCollection objParam1 = new SqlCommand().Parameters;
        objParam1.AddWithValue("@JobCatId", Convert.ToInt32(Cat));
        objParam1.AddWithValue("@JobPrfPosition1", strPosition);
        objParam1.AddWithValue("@JobPrfPosition2", "");
        objParam1.AddWithValue("@JobPrfPosition3", "");
        objParam1.AddWithValue("@JobPrfSaraly", "");
        objParam1.AddWithValue("@JobPrfSex", strPrfSex);
        objParam1.AddWithValue("@JobPrfName1", strName1);
        objParam1.AddWithValue("@JobPrfName2", strName2);
        objParam1.AddWithValue("@JobPrfBirth",  dtBirth );  
        objParam1.AddWithValue("@JobPrfAge", strAge  );
        objParam1.AddWithValue("@JobPrfIdCard", strIdCard);
        objParam1.AddWithValue("@JobPrfNationality", "");
        objParam1.AddWithValue("@JobPrfReligion", "");
        objParam1.AddWithValue("@JobPrfWeight", "");
        objParam1.AddWithValue("@JobPrfHeight", "");
        objParam1.AddWithValue("@JobPrfMilitary", "");
        objParam1.AddWithValue("@JobPrfAddress1", strAddress1);
        objParam1.AddWithValue("@JobPrfAddress2", strAddress2);
        objParam1.AddWithValue("@JobPrfTel1", strTel1);
        objParam1.AddWithValue("@JobPrfTel2", strTel2);
        objParam1.AddWithValue("@JobPrfEmail", strEmail);
        objParam1.AddWithValue("@JobPrfEdu1", "");
        objParam1.AddWithValue("@JobPrfEdu2", "");
        objParam1.AddWithValue("@JobPrfEdu3", "");
        objParam1.AddWithValue("@JobPrfEduActivity", "");

        objParam1.AddWithValue("@JobPrfSkLang1", "");
        objParam1.AddWithValue("@JobPrfSkLang2", "");
        objParam1.AddWithValue("@JobPrfSkCom", "");
        objParam1.AddWithValue("@JobPrfSkOther", "");
        objParam1.AddWithValue("@JobPrfPastWork1", "");
        objParam1.AddWithValue("@JobPrfPastWork2", "");
        objParam1.AddWithValue("@JobPrfMoreInfo", strMoreInfo);
        objParam1.AddWithValue("@JobPrfDateReg", DateTime.Now);
        objParam1.AddWithValue("@JobPrfStatus", "0");




        int i1 = new DBClass().SqlExecute(sql, objParam1);

        if (i1 == 1) {
            //Response.Write("<script>alert('คุณทำการสมัครงานเรียบร้อยแล้ว')</script>"); 
            Response.Redirect("response.aspx"); 
        } else {
            Response.Write("<script>alert('คุณไม่สามารถทำการสมัครงานได้')</script>");
            Response.Redirect("JobApply.aspx");
        }

    }

    
    
    void SetAge() {
        ArrayList arrList = new ArrayList();
        for (int i = 20; i <= 70; i++) {
            arrList.Add(i);
        }
        //==============
        ddlAge.AppendDataBoundItems = true;
        ddlAge.Items.Insert(0, new ListItem("-- กรุณาระบุ --", ""));
        //==============
        ddlAge.DataSource = arrList;
        ddlAge.DataBind();
    }


}