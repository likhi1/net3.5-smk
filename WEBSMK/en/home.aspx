﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront1_en.master" AutoEventWireup="true"
    CodeFile="home.aspx.cs" Inherits="home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server"> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div id="page-home">
        <div class="cat-show cat-show-margin">
            <div class="cat-col-left">
                <h1>Health Insurance</h1>
                <div class="cat-des">
                  สุขภาพมีไว้ดูแล เราขอดูแลคุณด้วยการประกันสุขภาพ รูปแบบใหม่ ยิ่งฟิตมาก ยิ่งลดมาก
                </div>
                <%--
<div class="cat-type">
<ul>
<li><a href="#">แบบประกันยอดนิยม</a></li>
<li><a href="#">แบบประกันล่าสุด</a></li>
</ul>
</div>
                --%>
            </div>
            <!-- End class="cat-col-left"  -->
            <div class="cat-col-right">
                <img src="<%=Config.SiteUrl%>images/insurance-products/catTb-1.jpg" />
            </div>
            <div class="clear">
            </div>
            <div class="cat-purchase">
                <a class="btn-default  btn-iconMore" href="<%=Config.SiteUrl%>en/product.aspx?cat=1">Read More<span></span></a> <a class="btn-default  btn-iconOrder" 
                    href="<%=Config.SiteUrl%>buyInsureCar_step1.aspx">Order<span></span></a>
            </div>
        </div>
        <!-- End class="cat-show" -->
        <div class="cat-show">
            <div class="cat-col-left">
                <h1>Car Insurance</h1>
                <div class="cat-des">
                   อุ่นใจทุกครั้งที่เดินทาง ด้วยการประกันรถยนต์ หลากหลายรูปแบบให้เลือกได้ตามความต้องการ
                </div>
            </div>
            <!-- End class="cat-col-left"  -->
            <div class="cat-col-right">
                <img src="<%=Config.SiteUrl%>images/insurance-products/catTb-2.jpg" />
            </div>
            <div class="clear">
            </div>
            <div class="cat-purchase">
                <a class="btn-default  btn-iconMore" href="<%=Config.SiteUrl%>en/product.aspx?cat=2">Read More<span></span></a> <a class="btn-default  btn-iconOrder" 
                    href="<%=Config.SiteUrl%>buyInsureCar_step1.aspx">Order<span></span></a>
            </div>
        </div>
        <!-- End class="cat-show" -->
        <div class="cat-show cat-show-margin">
            <div class="cat-col-left">
                <h1>Cancer Insurance</h1>
                <div class="cat-des">
                   พบปุ๊ป จ่ายปั๊ปทันที่ที่ตรวจพบมะเร็ง เลือกแบบประกันมะเร็งที่ประหยัดแต่คุ้มครองสูง
                </div>
            </div>
            <!-- End class="cat-col-left"  -->
            <div class="cat-col-right">
                <img src="<%=Config.SiteUrl%>images/insurance-products/catTb-3.jpg" />
            </div>
            <div class="clear">
            </div>
            <div class="cat-purchase">
                <a  class="btn-default  btn-iconMore"  href="<%=Config.SiteUrl%>en/product.aspx?cat=3">Read More<span></span></a> <a class="btn-default  btn-iconOrder" 
                    href="<%=Config.SiteUrl%>buyInsureCar_step1.aspx">Order<span></span></a>
            </div>
        </div>
        <!-- End class="cat-show" -->
        <div class="cat-show">
            <div class="cat-col-left">
                <h1>Accident Insurance</h1>
                <div class="cat-des">
                  อุบัติเหตุสามารถเกิดขึ้นได้ตลอดเวลา เลือกแบบประกันอุบัติเหตุ ที่คุ้มครองทุกที่ทุกเวลา ทั่วโลก
                </div>
            </div>
            <!-- End class="cat-col-left"  -->
            <div class="cat-col-right">
                <img src="<%=Config.SiteUrl%>images/insurance-products/catTb-4.jpg" />
            </div>
            <div class="clear">
            </div>
            <div class="cat-purchase">
                <a  class="btn-default  btn-iconMore"  href="<%=Config.SiteUrl%>en/product.aspx?cat=4">Read More<span></span></a> <a class="btn-default  btn-iconOrder" 
                    href="<%=Config.SiteUrl%>buyInsureCar_step1.aspx">Order<span></span></a>
            </div>
        </div>
        <!-- End class="cat-show" -->
        <div class="cat-show  cat-show-margin">
            <div class="cat-col-left">
                <h1>Fire Insurance</h1>
                <div class="cat-des">
                รักบ้าน รักทรัพย์สินของท่านด้วยการประกันอัคคีภัยเพื่อคุ้มครองบ้านและทรัพย์สินที่ท่านรัก 
                </div>
            </div>
            <!-- End class="cat-col-left"  -->
            <div class="cat-col-right">
                <img src="<%=Config.SiteUrl%>images/insurance-products/catTb-5.jpg" />
            </div>
            <div class="clear">
            </div>
            <div class="cat-purchase">
                <a  class="btn-default  btn-iconMore"  href="<%=Config.SiteUrl%>en/product.aspx?cat=5">Read More<span></span></a> <a class="btn-default  btn-iconOrder" 
                    href="<%=Config.SiteUrl%>buyInsureCar_step1.aspx">Order<span></span></a>
            </div>
        </div>
        <!-- End class="cat-show" -->
        <div class="cat-show">
            <div class="cat-col-left">
                <h1>Travel Insurance</h1>
                <div class="cat-des">
                     เดินทางอย่างสบายใจและมั่นใจในทุกหนแห่ง ด้วยประกันการเดินทาง
                </div>
            </div>
            <!-- End class="cat-col-left"  -->
            <div class="cat-col-right">
                <img src="<%=Config.SiteUrl%>images/insurance-products/catTb-6.jpg" />
            </div>
            <div class="clear">
            </div>
            <div class="cat-purchase">
                <a  class="btn-default  btn-iconMore"  href="<%=Config.SiteUrl%>en/product.aspx?cat=6">Read More<span></span></a> <a class="btn-default  btn-iconOrder" 
                    href="<%=Config.SiteUrl%>buyInsureCar_step1.aspx">Order<span></span></a>
            </div>
        </div>
        <!-- End class="cat-show" -->
    </div>
    <!-- End id="page-home" -->
</asp:Content>
