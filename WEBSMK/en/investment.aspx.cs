﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---------- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;
using System.Collections;

public partial class investment : System.Web.UI.Page
{

    protected String ConId { get { return Request.QueryString["Id"]; } }
    protected int CatId { get { return Convert.ToInt32(Request.QueryString["cat"]); } }

    protected void Page_Load(object sender, EventArgs e) {
        BindData(); 
    } 

    void BindData() {
        DataSet ds = SelectData();
        DataTable dt = ds.Tables[0];
         if (dt.Rows.Count > 0) {    
             
            lblPageName.Text = dt.Rows[0]["InvTopic"].ToString();

            lblDetail.Text = dt.Rows[0]["InvDetailLong"].ToString();
            

        }

    }

    DataSet SelectData() {
        string sql = "SELECT * FROM tbInv WHERE InvId ='" + ConId + "' AND InvCatId='" + CatId + "'   ";
        sql += "AND InvStatus ='1'  ";// check status content  


        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql , "mytb"); 
        return ds; 


    }




}