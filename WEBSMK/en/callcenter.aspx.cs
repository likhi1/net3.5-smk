﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;
using System.Collections;

public partial class callcenter : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    { 
        if (!Page.IsPostBack) {
         BindData();
        }
    }



    protected  void BindData(){
        //string ConId = String.Format("{0:00000}", intId);
        ddlTimeHours.DataSource  = SetHourse();
        ddlTimeHours.DataBind();
        ddlTimeHours.AppendDataBoundItems = true;
        ddlTimeHours.Items.Insert(0, new ListItem("00", "00")); 

        ddlTimeMinute.DataSource = SetMinute();
        ddlTimeMinute.DataBind();
        ddlTimeMinute.AppendDataBoundItems = true;
        ddlTimeMinute.Items.Insert(0, new ListItem("00", "00")); 

    }
     

     protected int SetData() { 
        string sql = "INSERT INTO tbCnt Values (@CntName, @CntTel, @CntEmail ,@CntDetail, @CntDateTimeGet, @CntDateSet, @CntTimeSet, @CntStatus ) ";

        SqlParameterCollection objParam1 = new SqlCommand().Parameters; 
        objParam1.AddWithValue("@CntName", tbxName.Text) ;
        objParam1.AddWithValue("@CntTel",  tbxTel.Text);
        objParam1.AddWithValue("@CntEmail", tbxEmail.Text);
        objParam1.AddWithValue("@CntDetail", txrDetail.Text);
        objParam1.AddWithValue("@CntDateTimeGet", DateTime.Now);
        objParam1.AddWithValue("@CntDateSet", Convert.ToDateTime( tbxDateGet.Text  ,  new CultureInfo("th-TH") ) ) ;
        objParam1.AddWithValue("@CntTimeSet", (ddlTimeHours.SelectedValue +":"+ ddlTimeMinute.SelectedValue ) );
        objParam1.AddWithValue("@CntStatus", "0");

        DBClass obj = new DBClass();
        int i2 = obj.SqlExecute(sql ,  objParam1); 
        return  i2 ;  
    }

    protected void btnSubmit_Click(object sender, EventArgs e) { 
            if (SetData() == 1) {
                Response.Redirect("response.aspx"); 
        } else {
            Response.Write("<script>alert('ไม่สามารถเพิ่มข้อมูลได้'); history.back() ;</script>"); 
        }
    }

    protected void btnReset_Click(object sender, EventArgs e) {
        tbxName.Text = string.Empty;
        tbxTel.Text = string.Empty; 
            tbxEmail.Text = string.Empty;
            txrDetail.Text = string.Empty;

            tbxDateGet.Text = string.Empty;
            ddlTimeHours.SelectedIndex = 0;
            ddlTimeMinute.SelectedIndex = 0; 

    }


    //======= SetUp ================================================
    protected ArrayList SetHourse() {
        ArrayList arl = new ArrayList();
        for (int i = 1; i <= 24; i++) {
            if (i < 10) {
                arl.Add("0" + i);
            } else {
                arl.Add(i);
            }
        }
        return arl;
    }

    protected ArrayList SetMinute() {
        ArrayList arl = new ArrayList();
        for (int i = 1; i <= 59; i++) {
            if (i < 10) {
                arl.Add("0" + i);
            } else {
                arl.Add(i);
            }
        }
        return arl;
    }


}
