﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---------- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;
using System.Collections;

 
public partial class  jobApply : System.Web.UI.Page
{
    protected string Cat { get { return Request.QueryString["cat"]; } }
    protected string Id { get { return Request.QueryString["id"]; } }
    protected string stringObject = "";
    protected DataSet  _posi;
    protected DataSet Posi { get { return _posi; } }
     
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            //======= Setup =========
           _posi =  Selectposition(); 
            Setposition();
            SetEducation();
            SetLanguageSkl();
            SetAge();

            ddlSelectPosi1.SelectedValue = Id;

        }
    }
     

//============  Utility Method
    protected String SetConcatString( string [] ArrayControl ) { 
        foreach (String val in ArrayControl) {
            stringObject += val +"|";
        } 
      return stringObject ;
    } 

    protected void btnSave_Click(object sender, EventArgs e) {

        string strPosition1 = ddlSelectPosi1.SelectedValue;
        string strPosition2 = ddlSelectPosi2.SelectedValue;
        string strPosition3 = ddlSelectPosi3.SelectedValue;
        string strSalary = tbxSalary.Text;
        string strFirstApply = rblFirstApply.SelectedValue;
        string strPrfSex = ddlNamePrefix.SelectedValue;
        string strName1 = tbxName1.Text;
        string strName2 = tbxName2.Text;
        string strBirth = tbxDateBrith.Text;
        string strAge = ddlAge.SelectedValue;
        string strNation= tbxNation.Text;
        string strReligion = tbxReligion.Text;
        string strWeight = tbxWeight.Text;
        string strHeight = tbxHeight.Text;
        string strEmail = tbxEmail.Text;
        string strMilitary = rblMilitary.SelectedValue;
        string strIdCard = tbxIdCard.Text;
        string strTel1 = tbxTel1.Text;
        string strTel2 = tbxTel2.Text;
        string strAddress1 = tarAddress1.Text;
        string strAddress2 = tarAddress2.Text;
        string strEduActivity = tarEduActivity.Text;
        string strSklCom = tarSklCom.Text;
        string strSklOther = tarSklOther.Text;
        string strMoreInfo = tarMoreInfo.Text;
       
        
        //string[] arrSelectPosi = new string[] { ddlSelectPosi1.SelectedValue, ddlSelectPosi2.SelectedValue, ddlSelectPosi3.SelectedValue };
        string[] arrEdu1 = new string[] { ddlEduLevel1.SelectedValue, tbxEduName1.Text, tbxEduBg1.Text, tbxEduMajor1.Text, tbxEduAvg1.Text, tbxEduYear1.Text, };
        string[] arrEdu2 = new string[] { ddlEduLevel2.SelectedValue, tbxEduName2.Text, tbxEduBg2.Text, tbxEduMajor2.Text, tbxEduAvg2.Text, tbxEduYear2.Text, };
        string[] arrEdu3 = new string[] { ddlEduLevel3.SelectedValue, tbxEduName3.Text, tbxEduBg3.Text, tbxEduMajor3.Text, tbxEduAvg3.Text, tbxEduYear3.Text, };
        string[] arrJobPast1 = new string[] { tbxPsjName1.Text, tbxPsjAddress1.Text, tbxPsjPosi1.Text, tbxPsjAge1.Text, tbxPsjSalary1.Text, tbxPsjDuty1.Text, tbxPsjReason1.Text };
        string[] arrJobPast2 = new string[] { tbxPsjName2.Text, tbxPsjAddress2.Text, tbxPsjPosi2.Text, tbxPsjAge2.Text, tbxPsjSalary2.Text, tbxPsjDuty2.Text, tbxPsjReason2.Text };
        string[] arrSklLang1 = new string[] { tbxSklLangName1.Text, ddlSklLangListen1.SelectedValue, ddlSklLangSpeak1.SelectedValue, ddlSklLangRead1.SelectedValue, ddlSklLangWrite1.SelectedValue };
        string[] arrSklLang2 = new string[] { tbxSklLangName2.Text, ddlSklLangListen2.SelectedValue, ddlSklLangSpeak2.SelectedValue, ddlSklLangRead2.SelectedValue, ddlSklLangWrite2.SelectedValue }; 
 
      string   sql = "INSERT INTO  tbJobPrf VALUES ( "
        + "@JobCatId , @JobPrfPosition1,@JobPrfPosition2,@JobPrfPosition3,@JobPrfSaraly,@JobPrfSex, " 
        + "@JobPrfName1,@JobPrfName2, @JobPrfBirth,@JobPrfAge,@JobPrfIdCard,@JobPrfNationality, "
        + "@JobPrfReligion,@JobPrfWeight,@JobPrfHeight,@JobPrfMilitary,@JobPrfAddress1,@JobPrfAddress2,  "
        + "@JobPrfTel1, @JobPrfTel2,@JobPrfEmail,@JobPrfEdu1 ,@JobPrfEdu2,@JobPrfEdu3,  "
        + "@JobPrfEduActivity,@JobPrfSkLang1,@JobPrfSkLang2,@JobPrfSkCom,@JobPrfSkOther, "
        + "@JobPrfPastWork1,@JobPrfPastWork2,@JobPrfMoreInfo,@JobPrfDateReg,@JobPrfStatus "
        +" ) ";

           SqlParameterCollection objParam1 = new SqlCommand().Parameters; 
        objParam1.AddWithValue("@JobCatId", Convert.ToInt32(Cat) );
        objParam1.AddWithValue("@JobPrfPosition1", strPosition1);
        objParam1.AddWithValue("@JobPrfPosition2", strPosition2);
        objParam1.AddWithValue("@JobPrfPosition3", strPosition3);
        objParam1.AddWithValue("@JobPrfSaraly", strSalary);
        objParam1.AddWithValue("@JobPrfSex", strPrfSex);
        objParam1.AddWithValue("@JobPrfName1", strName1);
        objParam1.AddWithValue("@JobPrfName2", strName2);
     
        objParam1.AddWithValue("@JobPrfBirth", Convert.ToDateTime(strBirth, new CultureInfo("th-TH") ) );
        objParam1.AddWithValue("@JobPrfAge",  strAge); 
        objParam1.AddWithValue("@JobPrfIdCard", strIdCard);
        objParam1.AddWithValue("@JobPrfNationality", strNation);
        objParam1.AddWithValue("@JobPrfReligion", strReligion);
        objParam1.AddWithValue("@JobPrfWeight", strWeight);
        objParam1.AddWithValue("@JobPrfHeight", strHeight);
        objParam1.AddWithValue("@JobPrfMilitary", strMilitary);
        objParam1.AddWithValue("@JobPrfAddress1", strAddress1);
        objParam1.AddWithValue("@JobPrfAddress2", strAddress2);
        objParam1.AddWithValue("@JobPrfTel1", strTel1);
        objParam1.AddWithValue("@JobPrfTel2", strTel2);
        objParam1.AddWithValue("@JobPrfEmail", strEmail);
        objParam1.AddWithValue("@JobPrfEdu1", string.Join("|", arrEdu1));
        objParam1.AddWithValue("@JobPrfEdu2", string.Join("|", arrEdu2));
        objParam1.AddWithValue("@JobPrfEdu3", string.Join("|", arrEdu3));
        objParam1.AddWithValue("@JobPrfEduActivity", strEduActivity);

        objParam1.AddWithValue("@JobPrfSkLang1", string.Join("|", arrSklLang1));
        objParam1.AddWithValue("@JobPrfSkLang2", string.Join("|", arrSklLang2));
        objParam1.AddWithValue("@JobPrfSkCom", strSklCom);
        objParam1.AddWithValue("@JobPrfSkOther", strSklOther);
        objParam1.AddWithValue("@JobPrfPastWork1", string.Join("|", arrJobPast1));
        objParam1.AddWithValue("@JobPrfPastWork2", string.Join("|", arrJobPast2));
        objParam1.AddWithValue("@JobPrfMoreInfo", strMoreInfo);
        objParam1.AddWithValue("@JobPrfDateReg", DateTime.Now);
        objParam1.AddWithValue("@JobPrfStatus", "0");
         

           int i1 = new DBClass().SqlExecute(sql, objParam1);

        if (i1 == 1) {
           // Response.Write("<script>alert('คุณทำการสมัครงานเรียบร้อยแล้ว')</script>");
            Response.Redirect("response.aspx");
        } else {
            Response.Write("<script>alert('คุณไม่สามารถทำการสมัครงานได้')</script>");
            Response.Redirect("JobApply.aspx");
        }
          
    }


    //========  Initial  =============================================
    DataSet   Selectposition() {
        string sql = "SELECT * FROM tbJobPosi WHERE  JobCatId= '" + Cat + "'  AND  JobPosiStatus  ='1'   ORDER BY JobPosiName   ";
        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql, "tb");
        return  ds ;
    }

       void Setposition() {
        DropDownList[] arr = new DropDownList[] { ddlSelectPosi1, ddlSelectPosi2, ddlSelectPosi3 };
        for (int i = 0; i <= arr.Length - 1; i++) { 
            arr[i].DataSource = _posi;
            //==============
            arr[i].AppendDataBoundItems = true;
            arr[i].Items.Insert(0, new ListItem("-- กรุณาระบุ --", "")); 
     
            //==============
            arr[i].DataTextField = "JobPosiName";
            arr[i].DataValueField = "JobPosiId";
            arr[i].DataBind(); 
        }


    }
    void SetEducation() {
        string[] arr = { "มัธยมศึกษา", "ปวช", "ปวส", "ปริญญาตรี", "ปริญญาโท", "ปริญญาเอก" };
        DropDownList[] arrDd = new DropDownList[] { ddlEduLevel1, ddlEduLevel2, ddlEduLevel3 };
        for (int i = 0; i <= arrDd.Length - 1; i++) {
            //==============
            arrDd[i].AppendDataBoundItems = true;
            arrDd[i].Items.Insert(0, new ListItem("-- กรุณาระบุ --", ""));
            //==============
            arrDd[i].DataSource = arr;
            arrDd[i].DataBind();
        }

    }
    void SetAge() {
        ArrayList arrList = new ArrayList();
        for (int i = 20; i <= 70; i++) {
            arrList.Add(i);
        }
        //==============
        ddlAge.AppendDataBoundItems = true;
        ddlAge.Items.Insert(0, new ListItem("-- กรุณาระบุ --", ""));
        //==============
        ddlAge.DataSource = arrList;
        ddlAge.DataBind();
    }
    void SetLanguageSkl() {

        string[] arr = { "ดีมาก", "ดี", "ปานกลาง", "พอใช้" };
        DropDownList[] arrDd = new DropDownList[] { ddlSklLangListen1, ddlSklLangSpeak1, ddlSklLangRead1, ddlSklLangWrite1, ddlSklLangListen2, ddlSklLangSpeak2, ddlSklLangRead2, ddlSklLangWrite2 };

        for (int i = 0; i <= arrDd.Length - 1; i++) {
            //==============
            arrDd[i].AppendDataBoundItems = true;
            arrDd[i].Items.Insert(0, new ListItem("-- กรุณาระบุ --", ""));
            //==============
            arrDd[i].DataSource = arr;
            arrDd[i].DataBind();
        }
    }


     
}