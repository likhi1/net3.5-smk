﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//--- connect
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
//--- arrayList
using System.Collections;


public partial class networkMap : System.Web.UI.Page
{
    public string NtwId  { get{ return Request.QueryString["id"]; } } 

    protected void Page_Load(object sender, EventArgs e) {
        BindData();
    } 

    protected void BindData() { 

      DataSet ds  = SelectData(NtwId);
      DetailsView1.DataSource = ds;
      DetailsView1.DataBind(); 


      DataTable dt = ds.Tables[0];  
              if (dt.Rows.Count > 0) { 
                    //lblName.Text = dt.Rows[0]["NtwName"].ToString();
                    //lblAddress.Text = dt.Rows[0]["NtwAddress"].ToString();
                    //lblService.Text = dt.Rows[0]["NtwService"].ToString();
                   
                    //lblMobile.Text = dt.Rows[0]["NtwTelMobile"].ToString();
                    //lblPhone.Text = dt.Rows[0]["NtwTelPhone"].ToString();
                    //lblProvince.Text = dt.Rows[0]["NtwProvince"].ToString();

                    string pic1 = dt.Rows[0]["NtwPic1"].ToString();
                    string pic2 = dt.Rows[0]["NtwPic2"].ToString();
                    string picDefault = "images/blank_photo_mediuem.jpg";  
                    pic1 = (pic1 == "") ? picDefault : pic1;
                    pic2 = (pic2 == "") ? picDefault : pic2; 

                    imgPic1.Attributes.Add("src", pic1);
                    imgPic2.Attributes.Add("src", pic2);  

                    //////// Add HTML Code To <div>
                    areaMap.InnerHtml =  dt.Rows[0]["NtwMap"].ToString();
                    //areaMap.Attributes["style"] =  "width:620px; height:260px;margin-bottom:50px;clear:both;overflow:hidden;" ; 

                    ///////// Add  Attributes src(Url)  to iframe   
                    //ifmMap.Visible = true;
                    //ifmMap.Attributes.Add ( "src" , dt.Rows[0]["NtwMap"].ToString()  ) ;
              } 
   }


    protected DataSet SelectData( string _NtwId  ) { 
        string strSql = "SELECT *  FROM  tbNtw "
            + "WHERE  NtwId='" + NtwId + "'  "; 
        DBClass obj = new DBClass ();
        DataSet ds = obj.SqlGet(strSql ,"tb");

        return ds; 

    }




    protected void DetailsView1_DataBound(object sender, EventArgs e) { 

                Label lblName = (Label)(DetailsView1.FindControl("lblName")  );
                if (lblName != null) {
                    lblName.Text = DataBinder.Eval(DetailsView1.DataItem, "NtwName").ToString(); 
                }  

                Label lblAddress = (Label)(DetailsView1.FindControl("lblAddress"));
                string NtwAddress = DataBinder.Eval(DetailsView1.DataItem, "NtwAddress").ToString();
                if (String.IsNullOrEmpty(NtwAddress)) {
                    DetailsView1.Fields[1].Visible = false;
                } else {
                    lblAddress.Text = NtwAddress ;
                }  
              
                Label lblProvince = (Label)(DetailsView1.FindControl("lblProvince"));
                if (lblProvince != null) {
                    lblProvince.Text = DataBinder.Eval(DetailsView1.DataItem, "NtwProvince").ToString(); 
                }

                Label lblAmphoe = (Label)(DetailsView1.FindControl("lblAmphoe"));
                string NtwAmphoe = DataBinder.Eval(DetailsView1.DataItem, "NtwAmphoe").ToString();
                if (!String.IsNullOrEmpty(NtwAmphoe)) { 
                    lblAmphoe.Text = "("+NtwAmphoe+")" ;
                }  

                Label lblPhone = (Label)(DetailsView1.FindControl("lblPhone"));
                string NtwTelPhone  = DataBinder.Eval(DetailsView1.DataItem, "NtwTelPhone").ToString();
                if (String.IsNullOrEmpty(NtwTelPhone)) {
                    DetailsView1.Fields[3].Visible = false;
                } else {
                    lblPhone.Text = NtwTelPhone;
                }  
               
                Label lblMobile = (Label)(DetailsView1.FindControl("lblMobile"));
                string NtwTelMobile  = DataBinder.Eval(DetailsView1.DataItem, "NtwTelMobile").ToString();
                if (String.IsNullOrEmpty(NtwTelMobile)) {
                    DetailsView1.Fields[4].Visible = false;
                } else {
                    lblMobile.Text = NtwTelMobile;
                }

                Label lblNotation = (Label)(DetailsView1.FindControl("lblNotation"));
                string strNotation = DataBinder.Eval(DetailsView1.DataItem, "NtwNotation").ToString();
                if (String.IsNullOrEmpty(strNotation)) {
                    DetailsView1.Fields[5].Visible = false;
                }
                else {
                    lblNotation.Text = strNotation;
                }

         
   }


         



}