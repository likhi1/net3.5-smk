﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="investInfo_07.aspx.cs" Inherits="investInfo_07" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

 <div class="subject1">ข้อมูลทางการเงิน</div > 

 
<table class="tb-border-bottom" >
  <tr>
                  <td   class="no-border" ></td>
                  <td   class="no-border"  >&nbsp;</td>
                  <td   class="no-border"  >&nbsp;</td>
                  <td   class="no-border"  >(หน่วย : ล้านบาท)</td>
                  </tr>
                <tr>
                  <th class="align-left">ผลประกอบการรอบปี</th>
                  <th width="17%">2555</th> 
                  <th width="17%" >2554</th>
                  <th width="17%">2553</th>
                </tr>
  
                  <td colspan="4"  class="hilight1 align-left">งบกำไรขาดทุน</td>
                </tr>
                <tr>
                  <td class="align-left" >เบี้ยประกันภัยรับ</td>
                  <td class="align-right"  >8,072.30</td>
                  <td class="align-right"  >6,927.63</td>
                  <td class="align-right"  >6,145.09</td>
                </tr>
                <tr>
                  <td class="align-left" >เบี้ยประกันภัยต่อ</td>
                  <td class="align-right"  >297.47</td>
                  <td class="align-right"  >268.75</td>
                  <td class="align-right"  >253.74</td>
                </tr>
                <tr>
                  <td class="align-left" >เบี้ยประกันภัยรับสุทธิ</td>
                  <td class="align-right"  >7,774.83</td>
                  <td class="align-right"  >6,658.88</td>
                  <td class="align-right"  >5,891.35</td>
                </tr>
                <tr>
                  <td class="align-left" >เงินสำรองเบี้ยประกันภัยที่ยังไม่ถือเป็นรายได้เพิ่มขึ้นจากปีก่อน</td>
                  <td class="align-right"  >622.52</td>
                  <td class="align-right"  >357.03</td>
                  <td class="align-right"  >491.23</td>
                </tr>
                <tr>
                  <td class="align-left" >เบี้ยประกันภัยที่ถือเป็นรายได้</td>
                  <td class="align-right"  >7,152.31</td>
                  <td class="align-right"  >6,301.86</td>
                  <td class="align-right"  >5,400.12</td>
                </tr>
                <tr>
                  <td class="align-left" >รายได้ค่าจ้างและค่าบำเหน็จ</td>
                  <td class="align-right"  >89.74</td>
                  <td class="align-right"  >97.23</td>
                  <td class="align-right"  >87.37</td>
                </tr>
                <tr>
                  <td class="align-left" >ค่าใช้จ่ายในการรับประกันภัยก่อนค่าใช้จ่ายดำเนินงาน</td>
                  <td class="align-right"  >5,840.05</td>
                  <td class="align-right"  >5,076.70</td>
                  <td class="align-right"  >4,406.78</td>
                </tr>
                <tr>
                  <td class="align-left" >ค่าใช้จ่ายในการดำเนินงาน</td>
                  <td class="align-right"  >1,000.13</td>
                  <td class="align-right"  >826.63</td>
                  <td class="align-right"  >712.60</td>
                </tr>
                <tr>
                  <td class="align-left" >กำไรจากการรับประกันภัย</td>
                  <td class="align-right"  >401.87</td>
                  <td class="align-right"  >495.75</td>
                  <td class="align-right"  >368.10</td>
                </tr>
                <tr>
                  <td class="align-left" >กำไรจากการลงทุน</td>
                  <td class="align-right"  >428.51</td>
                  <td class="align-right"  >307.30</td>
                  <td class="align-right"  >239.70</td>
                </tr>
                <tr>
                  <td class="align-left" >รายได้อื่น</td>
                  <td class="align-right"  >57.43</td>
                  <td class="align-right"  >13.09</td>
                  <td class="align-right"  >12.52</td>
                </tr>
                <tr>
                  <td class="align-left" >กำไรก่อนภาษีเงินได้นิติบุคคล</td>
                  <td class="align-right"  >842.02</td>
                  <td class="align-right"  >777.94</td>
                  <td class="align-right"  >588.54</td>
                </tr>
                <tr>
                  <td class="align-left" >ภาษีเงินได้นิติบุคคล</td>
                  <td class="align-right"  >214.94</td>
                  <td class="align-right"  >322.19</td>
                  <td class="align-right"  >106.93</td>
                </tr>
                <tr>
                  <td class="align-left" >กำไรสุทธิ</td>
                  <td class="align-right"  >627.07</td>
                  <td class="align-right"  >455.75</td>
                  <td class="align-right"  >481.60</td>
                </tr>
                <tr>
                  <td class="align-left" >กำไรต่อหุ้น (บาท)</td>
                  <td class="align-right"  >31.35</td>
                  <td class="align-right"  >22.79</td>
                  <td class="align-right"  >24.08</td>
                </tr>
                <tr>
                  <td class="no-border align-left" >เงินปันผลต่อหุ้น (บาท)</td>
                  <td class="no-border align-right"  >11.00</td>
                  <td class="no-border align-right"  >9.00</td>
                  <td class="no-border align-right"  >8.00</td>
                </tr>
                <tr>
                  <td colspan="4" class="hilight1 align-left" >งบดุล</td>
                </tr>
                <tr>
                  <td class="align-left" >สินทรัพย์รวม</td>
                  <td class="align-right"  >11,348.90</td>
                  <td class="align-right"  >10,013.89</td>
                  <td class="align-right"  >9,085.89</td>
                </tr>
                <tr>
                  <td class="align-left" >ที่ดิน อาคาร และ อุปกรณ์ - สุทธิ</td>
                  <td class="align-right"  >334.44</td>
                  <td class="align-right"  >279.85</td>
                  <td class="align-right"  >295.46</td>
                </tr>
                <tr>
                  <td class="align-left" >หนี้สินรวม</td>
                  <td class="align-right"  >8,450.14</td>
                  <td class="align-right"  >7,778.96</td>
                  <td class="align-right"  >7,071.20</td>
                </tr>
                <tr>
                  <td class="align-left" >ส่วนของผู้ถือหุ้น</td>
                  <td class="align-right"  >2,898.77</td>
                  <td class="align-right"  >2,234.93</td>
                  <td class="align-right"  >2,014.69</td>
                </tr>
                <tr>
                  <td class="align-left" >มูลค่าตามบัญชี (บาท)</td>
                  <td class="align-right"  >144.94</td>
                  <td class="align-right"  >111.75</td>
                  <td class="align-right"  >100.73</td>
                </tr>
     </table>
     
     <div class="notation" >* ข้อมูลแสดงราคาหลักทรัพย์ ของบริษัท สินมั่นคงประกันภัย จำกัด (มหาชน)  นำเสนอโดย SETTREDE</div>
</asp:Content>

