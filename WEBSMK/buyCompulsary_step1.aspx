﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="buyCompulsary_step1.aspx.cs" Inherits="buyCompulsary_step1" EnableEventValidation="false"%>
<%@ Register src="~/uc/ucTxtDate.ascx" tagname="ucTxtDate" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script>
        function PopulateCategory() {
            var pageUrl = '<%=ResolveUrl("~/buyCompulsary_step1.aspx")%>';
            var strCarName;
            var strCarType;
            var ddlChild;

            //$(ddlChild).attr("disabled", "disabled");
            strCarName = document.getElementById("<%=ddlBrand.ClientID %>").value;
            ddlChild = document.getElementById("<%=ddlCategory.ClientID %>");
            $.ajax({
                async: false,
                type: "POST",
                url: pageUrl + '/GetCarCategoryData',
                data: '{strCarName:"' + strCarName + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(response) {
                    PopulateDropdownControl(response.d, $(ddlChild))
                },
                error: function(response) {
                    alert("error " + response.d);
                }
            });
        }
        function PopulateSize() {
            var pageUrl = '<%=ResolveUrl("~/buyCompulsary_step1.aspx")%>';
            var strCarName;
            var strCarCategory;
            var strCarUse;
            var ddlChild;

            //$(ddlChild).attr("disabled", "disabled");
            strCarName = document.getElementById("<%=ddlBrand.ClientID %>").value;
            strCarCategory = document.getElementById("<%=ddlCategory.ClientID %>").value;
            if (document.getElementById("<%=rdoPersonal.ClientID %>").checked == true)
                strCarUse = "P";
            else if (document.getElementById("<%=rdoWork.ClientID %>").checked == true)
                strCarUse = "S";
            else
                strCarUse = "R";
            ddlChild = document.getElementById("<%=ddlSize.ClientID %>");
            $.ajax({
                async: false,
                type: "POST",
                url: pageUrl + '/GetCarSizeData',
                data: '{strCarName:"' + strCarName + '",strCarCategory:"' + strCarCategory + '",strVehUse:"' + strCarUse + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(response) {
                    PopulateDropdownControl(response.d, $(ddlChild))
                },
                error: function(response) {
                    alert("error " + response.d);
                }
            });
        }
        function PopulateDropdownControl(list, control) {
            //control.removeAttr("disabled");
            if (list.length > 0) {
                //control.empty().append('<option selected="selected" value="0">Please select</option>');
                control.empty();
                $.each(list, function() {
                    control.append($("<option></option>").val(this['Value']).html(this['Text']));
                });
                control.removeAttr("disabled");
            }
            else {
                control.empty().append('<option selected="selected" value=""> - เลือก - <option>');
                control.attr("disabled", "disabled");
            }
        }
        function validateForm() {
            var isRet = true;
            var isRequire = true;
            var isLength = true;
            var isDate = true;
            var strMsg = "";
            var strMsgRequire = "";
            var strMsgLength = "";
            var strMsgDate = "";
            if (document.getElementById("<%=ddlBrand.ClientID%>").value == "") {
                strMsg += "    - ยี่ห้อ \n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=ddlCategory.ClientID %>").value == "") {
                strMsg += "    - ประเภท\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=ddlSize.ClientID%>").value == "") {
                strMsg += "    - ลักษณะตัวรถ\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtRegisNo.ClientID %>").value == "") {
                strMsg += "    - เลขทะเบียนรถ\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=ddlLicenseProvince.ClientID %>").value == "") {
                strMsg += "    - จังหวัดที่จดทะเบียน\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=ddlYear.ClientID %>").value == "") {
                strMsg += "    - ปีจดทะเบียน\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtFName.ClientID %>").value == "") {
                strMsg += "    - หมายเลขเครื่อง\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtFName.ClientID %>").value == "") {
                strMsg += "    - หมายเลขตัวถัง\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtStartDate.txtID%>").value == "__/__/____") {
                strMsg += "    - วันที่เริ่มคุ้มครอง \n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtFName.ClientID %>").value == "") {
                strMsg += "    - ชื่อ\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtLName.ClientID %>").value == "") {
                strMsg += "    - สกุล\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtAddress.ClientID %>").value == "") {
                strMsg += "    - ที่อยู่\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtSubDistrict.ClientID %>").value == "") {
                strMsg += "    - ตำบล / แขวง\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtDistrict.ClientID %>").value == "") {
                strMsg += "    - อำเภอ / เขต\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=ddlProvince.ClientID %>").value == "") {
                strMsg += "    - จังหวัด\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtPostCode.ClientID %>").value == "") {
                strMsg += "    - รหัสไปรษณีย์\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtMobile.ClientID %>").value == "") {
                strMsg += "    - เบอร์โทรศัพท์มือถือ\n";
                isRet = false;
                isRequire = false;
            }


            if (isRequire == false) {
                strMsg = "!กรุณาระบุ\n" + strMsg;
            }


            if (isLength == false) {
                strMsgLength = "!ขนาดข้อมูลที่ระบุไม่ถูกต้อง\n" + strMsg;
            }

            if (isRet == false) {
                //alert(strMsg + strMsgLength + strMsgDate);
                showMessagePage(strMsg + strMsgLength + strMsgDate);
            }
            else {
                document.getElementById("divLoading").style.display = "block";
                document.getElementById('divPremium').style.display = 'none';                
            }
            return isRet;
        }
        function showMessagePage(strMsg) {
            document.getElementById("MsgIframe").setAttribute("src", "ShowMessageBox.aspx?msg=" + strMsg);
            //alert("url : " + link);
            $("[id*=dialog]").dialog({
                autoOpen: false,
                show: "fade",
                hide: "fade",
                modal: true,
                //open: function (ev, ui) {
                //    $('#myIframe').src = 'http://www.google.com';
                //},
                maxHeight: '400',
                width: '400',
                resizable: true
                //title: 'Title Topic'
            });
            //            $("#dialog1").dialog();

            $("#dialogMsg").dialog("open");
        }
        function closeMessagePage() {
            $('#dialogMsg').dialog('close');
            return false;

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div id="page-online-insCar"> 
        <h1 class="subject1">ซื้อประกัน-รถยนต์ภาคบังคับ</h1>
        <%--<div class="btnCallBack"><a href="./buyInsure.aspx?main_class=M" title="ซื้อประกันภัยรถยนต์">ซื้อประกัน</a></div>--%>
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="True">
        </asp:ScriptManager>
        <div class="stepOnline">
            <img src="images/navi2step_s-1.jpg"/>
        </div>
        <div class="prc-row">     
            <h2 class="subject-detail">ข้อมูลรายละเอียดรถยนต์</h2>
            <table class="tb-online-insCar">
                <tr>
                    <td class="label">
                        ยี่ห้อ :
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlBrand" runat="server">
                        </asp:DropDownList>
                        <span class="star">*</span>    
                    </td>
                    <td class="label">
                        ประเภท :
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCategory" runat="server">
                        </asp:DropDownList>
                        <span class="star">*</span>    
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        ลักษณะการใช้งาน :
                    </td>
                    <td>
                        <asp:RadioButton ID="rdoPersonal" runat="server" GroupName="Use" Text="ส่วนบุคคล" Checked="true"/>
                        <asp:RadioButton ID="rdoWork" runat="server" GroupName="Use" Text="รับจ้าง"/>
                        <asp:RadioButton ID="rdoRent" runat="server" GroupName="Use" Text="ให้เช่า"/>
                        
                    </td>
                    <td class="label">
                        ลักษณะตัวรถ :
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlSize" runat="server">
                        </asp:DropDownList>
                        <span class="star">*</span>    
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        จังหวัดจดทะเบียน :
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlLicenseProvince" runat="server">
                        </asp:DropDownList>
                        <span class="star">*</span>
                    </td>
                    <td class="label">
                        เลขทะเบียน :
                    </td>
                    <td>
                        <asp:TextBox ID="txtRegisNo" runat="server" MaxLength="20"></asp:TextBox>
                        <span class="star">*</span>                                                
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><span class="star">(รถป้ายแดง โปรดระบุ"ป้ายแดง")</span></td>
                </tr>
                <tr>
                    <td class="label">
                        ปีจดทะเบียน :
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlYear" runat="server">
                        </asp:DropDownList>
                        <span class="star">*</span>
                    <td class="label">
                        หมายเลขเครื่อง :
                    </td>
                    <td>
                        <asp:TextBox ID="txtEngineNo" runat="server" MaxLength="20"></asp:TextBox>
                        <span class="star">*</span>    
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        หมายเลขตัวถัง :
                    </td>
                    <td>
                        <asp:TextBox ID="txtChasNo" runat="server" MaxLength="20"></asp:TextBox>                        
                        <span class="star">*</span>
                    </td>
                    <td class="label">
                        วันที่เริ่มคุ้มครอง :
                    </td>
                    <td>
                        <uc1:ucTxtDate ID="txtStartDate" runat="server" /><span class="star">*</span>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        <asp:Label ID="Label13" runat="server" Text="(กรมธรรม์คุ้มครอง 1 ปี)"></asp:Label>
                    </td>
                </tr>
            </table>
            
            <h2 class="subject-detail">รายละเอียดผู้เอาประกัน</h2>
            <table width="630px">
                <tr>
                    <td valign="top">
                        <table class="tb-online-insCar">              
                            <tr>
                                <td class="label" >
                                    ชื่อ :
                                </td>
                                <td >
                                    <asp:TextBox ID="txtFName" runat="server"></asp:TextBox>
                                    <span class="star">*</span>
                                </td>    
                                <td class="label">
                                    นามสกุล :
                                </td>
                                <td>
                                    <asp:TextBox ID="txtLName" runat="server"></asp:TextBox>
                                    <span class="star">*</span>
                                </td>                            
                            </tr>
                            <tr>
                                <td class="label">
                                    ที่อยู่ :
                                </td>
                                <td colspan="3" nowrap>
                                    <asp:TextBox ID="txtAddress" runat="server" Width="250" MaxLength="50"></asp:TextBox>
                                    <span class="star">*</span>(ใส่ บ้านเลขที่, หมู่, ถนน)  
                                </td>
                            </tr>
                            <tr>                    
                                <td class="label"  >
                                    
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtSubDistrict" runat="server" Width="250" MaxLength="50"></asp:TextBox>
                                    <span class="star">*</span>(ใส่ ตำบล, แขวง)
                                </td>
                            </tr>
                            <tr>
                                <td class="label" >
                                    อำเภอ / เขต :
                                </td>
                                <td width="150px">
                                    <asp:TextBox ID="txtDistrict" runat="server"></asp:TextBox>
                                    <span class="star">*</span>
                                </td>      
                                <td class="label">
                                    จังหวัด :
                                </td>
                                <td  >
                                    <asp:DropDownList ID="ddlProvince" runat="server">
                                    </asp:DropDownList>
                                    <span class="star">*</span>
                                </td>                          
                            </tr>
                            <tr>
                                <td class="label">
                                    รหัสไปรษณีย์ :
                                </td>
                                <td width="130px">
                                    <asp:TextBox ID="txtPostCode" runat="server" MaxLength="5"></asp:TextBox>
                                    <span class="star">*</span>
                                </td>      
                                <td class="label">
                                    เลขที่บัตรประชาชน :
                                </td>
                                <td  >
                                    <asp:TextBox ID="txtIDCard" runat="server" MaxLength="13"></asp:TextBox>                        
                                </td>                         
                            </tr>
                            <tr>
                                <td class="label">
                                    เบอร์โทรศัพท์มือถือ :
                                </td>
                                <td>
                                    <asp:TextBox ID="txtMobile" runat="server"></asp:TextBox>
                                    <span class="star">*</span>
                                </td>  
                                <td class="label">
                                    เบอร์โทรศัพท์บ้าน :
                                </td>
                                <td>
                                    <asp:TextBox ID="txtTelephone" runat="server"></asp:TextBox>                        
                                </td>                                                  
                            </tr>
                            <tr>
                                <td class="label">
                                    อีเมล์ :
                                </td>
                                <td>
                                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>                                    
                                </td>                    
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div style="margin: 0px auto; width: 250px; margin-top: 15px;">                        
                            <asp:Button ID="btnCalculate" runat="server" Text="คำนวณเบี้ย" class="btn-default" Style="margin-right: 10px;" OnClick="btnCalculate_Click" OnClientClick="return validateForm();"/>
                            <input id="btnClear" type="reset" class="btn-default " value="เคียร์ข้อมูล" />
                        </div>
                    </td>
                    <a name="grd"></a>
                </tr>
            </table>
            <div id="divLoading" style="text-align:center;display:none;">
                <img src="./images/loading.gif" />
            </div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divPremium" style="display:none">
            <h2 class="subject-detail">ยอดเบี้ยประกันที่ต้องชำระ</h2>
            <table   class="tb-online-insCar">           
                <tr>
                    <td class="label">เบี้ยสุทธิ :</td>
                    <td>
                        <asp:Label ID="lblPremium" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="label">อากร :</td>
                    <td>
                        <asp:Label ID="lblStamp" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="label"   >ภาษี :</td>
                    <td  >
                        <asp:Label ID="lblVat" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="label"  >เบี้ยรวม :</td>
                    <td   >
                        <asp:Label ID="lblGrossPremium" runat="server" Text=""></asp:Label>
                    </td>
                </tr>                
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="4">
                        <div style="margin: 0px auto; width: 250px; margin-top: 15px;">                        
                            <asp:Button ID="btnSave" runat="server" Text="ถัดไป >>" class="btn-default" Style="margin-right: 10px;" OnClick="btnSave_Click"/>
                            <input id="Reset1" type="reset" class="btn-default " value="ยกเลิก" />
                        </div>
                    </td>
                    <a name="grd"></a>
                </tr>
            </table>   
            </div>
                    <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnCalculate" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel> 
        </div><!-- End class="prc-row"-->
        <div id="dialogMsg" style="display:none" title="การตรวจสอบข้อมูล"   >
            <iframe id="MsgIframe"   frameborder="0" style="max-height:360" width="370" scrolling="auto"></iframe>
        </div>
    </div><!-- End  class="page-online-insCar" -->
    <div style="display:none">
        <asp:TextBox ID="txtTmpCD" runat="server"></asp:TextBox>
    </div>
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

