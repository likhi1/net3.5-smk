<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="buyVoluntary_step3.aspx.cs" Inherits="buyVoluntary_step3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script>
        function validateForm() {
            var isRet = true;
            var isRequire = true;
            var isLength = true;
            var isDate = true;
            var strMsg = "";
            var strMsgRequire = "";
            var strMsgLength = "";
            var strMsgDate = "";
            if (document.getElementById("<%=rdoCheck.ClientID%>").checked == false &&
                document.getElementById("<%=rdoTransfer.ClientID%>").checked == false &&
                document.getElementById("<%=rdoSCB.ClientID%>").checked == false &&
                document.getElementById("<%=rdoDelivery.ClientID%>").checked == false &&
                document.getElementById("<%=rdoCredit.ClientID%>").checked == false) {
                strMsg += "    - �Ըա�ê������»�Сѹ���\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=rdoCheck.ClientID %>").checked == true &&
                document.getElementById("<%=ddlBranch.ClientID %>").value == "") {
                strMsg += "    - �Ңҷ���ͧ��ê����Թ�����Թʴ������\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=rdoChange.ClientID %>").checked == true &&
                document.getElementById("<%=txtFName.ClientID %>").value == "") {
                strMsg += "    - ����\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=rdoChange.ClientID %>").checked == true &&
                document.getElementById("<%=txtLName.ClientID %>").value == "") {
                strMsg += "    - ʡ��\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=rdoChange.ClientID %>").checked == true &&
                document.getElementById("<%=txtAddress.ClientID %>").value == "") {
                strMsg += "    - �������\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=rdoChange.ClientID %>").checked == true &&
                document.getElementById("<%=txtSubDistrict.ClientID %>").value == "") {
                strMsg += "    - �Ӻ� / �ǧ\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=rdoChange.ClientID %>").checked == true &&
                document.getElementById("<%=txtDistrict.ClientID %>").value == "") {
                strMsg += "    - ����� / ࢵ\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=rdoChange.ClientID %>").checked == true &&
                document.getElementById("<%=ddlProvince.ClientID %>").value == "") {
                strMsg += "    - �ѧ��Ѵ\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=rdoChange.ClientID %>").checked == true &&
                document.getElementById("<%=txtPostCode.ClientID %>").value == "") {
                strMsg += "    - ������ɳ���\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=rdoChange.ClientID %>").checked == true &&
                document.getElementById("<%=txtTelephone.ClientID %>").value == "") {
                strMsg += "    - �������Ѿ��\n";
                isRet = false;
                isRequire = false;
            }


            if (isRequire == false) {
                strMsg = "!��س��к�\n" + strMsg;
            }

            if (isLength == false) {
                strMsgLength = "!��Ҵ�����ŷ���к����١��ͧ\n" + strMsg;
            }

            if (isRet == false) {
                //alert(strMsg + strMsgLength + strMsgDate);
                showMessagePage(strMsg + strMsgLength + strMsgDate);
            }
            return isRet;
        }
        function showMessagePage(strMsg) {
            document.getElementById("MsgIframe").setAttribute("src", "ShowMessageBox.aspx?msg=" + strMsg);
            //alert("url : " + link);
            $("[id*=dialog]").dialog({
                autoOpen: false,
                show: "fade",
                hide: "fade",
                modal: true,
                //open: function (ev, ui) {
                //    $('#myIframe').src = 'http://www.google.com';
                //},
                maxHeight: '400',
                width: '400',
                resizable: true
                //title: 'Title Topic'
            });
            //            $("#dialog1").dialog();

            $("#dialogMsg").dialog("open");
        }
        function closeMessagePage() {
            $('#dialogMsg').dialog('close');
            return false;

        }
        function openPayInSlip() {
            window.open('PayInSlip.aspx?tmpCd=' + document.getElementById("<%=txtTmpCd.ClientID %>").value  , 'payinslip', 'location=no,menu=no,height=530,width=800');
        }
        function copydata() {
            if (document.getElementById("<%=rdoOld.ClientID %>").checked == true) {
                document.getElementById("<%=txtFName.ClientID %>").value = "<%=dsUI.Tables[0].Rows[0]["rg_ins_fname"]%>";
                document.getElementById("<%=txtLName.ClientID %>").value = "<%=dsUI.Tables[0].Rows[0]["rg_ins_lname"]%>";
                document.getElementById("<%=txtAddress.ClientID %>").value = "<%=dsUI.Tables[0].Rows[0]["rg_ins_addr1"]%>";
                document.getElementById("<%=txtSubDistrict.ClientID %>").value = "<%=dsUI.Tables[0].Rows[0]["rg_ins_addr2"]%>";
                document.getElementById("<%=txtDistrict.ClientID %>").value = "<%=dsUI.Tables[0].Rows[0]["rg_ins_amphor"]%>";
                document.getElementById("<%=ddlProvince.ClientID %>").value = "<%=dsUI.Tables[0].Rows[0]["rg_ins_changwat"]%>";
                document.getElementById("<%=txtPostCode.ClientID %>").value = "<%=dsUI.Tables[0].Rows[0]["rg_ins_postcode"]%>";
                document.getElementById("<%=txtTelephone.ClientID %>").value = "<%=dsUI.Tables[0].Rows[0]["rg_ins_tel"]%>";
            }
        }
        function cleardata() {
            if (document.getElementById("<%=rdoChange.ClientID %>").checked == true) {
                document.getElementById("<%=txtFName.ClientID %>").value = "";
                document.getElementById("<%=txtLName.ClientID %>").value = "";
                document.getElementById("<%=txtAddress.ClientID %>").value = "";
                document.getElementById("<%=txtSubDistrict.ClientID %>").value = "";
                document.getElementById("<%=txtDistrict.ClientID %>").value = "";
                document.getElementById("<%=ddlProvince.ClientID %>").value = "";
                document.getElementById("<%=txtPostCode.ClientID %>").value = "";
                document.getElementById("<%=txtTelephone.ClientID %>").value = "";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div   id="page-online-insCar" >
        <h1 class="subject1">�ӻ�Сѹ�͹�Ź�</h1>
        <div class="stepOnline">
            <img src="images/app-step-3.jpg" />
        </div>
        <div class="prc-row"> 
           <h2 class="subject-detail">�ʹ���»�Сѹ����ͧ����</h2>
           <div id="divPremium" style="display:inline">
           <table   class="tb-online-insCar" >           
                <tr>
                    <td width="100">������Ѥ��</td>
                    <td class="label"   >�����ط�� :</td>
                    <td>
                        <asp:Label ID="lblPremium" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="label" width="150">�ҡ� :</td>
                    <td>
                        <asp:Label ID="lblStamp" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td class="label"   >���� :</td>
                    <td  >
                        <asp:Label ID="lblVat" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="label"  >������� :</td>
                    <td   >
                        <asp:Label ID="lblGrossPremium" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="100">���� �ú.</td>            
                    <td></td>
                    <td  >
                        <asp:Label ID="lblComPremium" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="100">�������������</td>            
                    <td></td>
                    <td  >
                        <asp:Label ID="lblPaid" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>   
            </div>
            <div id="div013Premium" style="display:none">
           <table   class="tb-online-insCar" >                           
                <tr>
                    <td>���� Save&save(��� �ú.) :</td>            
                    <td></td>
                    <td  >
                        <asp:Label ID="lblPaid2" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>   
            </div>
            <h2 class="subject-detail">���͡�Ըա�ê����Թ</h2>
            <table class="tb-online-insCar" >                      
                <tr>
                    <td> 
                        ��ҹ����ö���͡�Ըա�ê������»�Сѹ��¡Ѻ ����ѷ �Թ��蹤���Сѹ��� �ӡѴ (��Ҫ�) 
                        <br />
                        �����Ը���Ը�˹�觨ҡ������͡��ҧ��ҧ���
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td> 
                        <table>
                            <tr>
                                <td>
                                    <asp:RadioButton ID="rdoCheck" runat="server" GroupName="pay_type" Text="�����Թ�����Թʴ������ ������ѷ�Թ��蹤���Сѹ��¨ӡѴ (��Ҫ�) ����" />
                                    <asp:DropDownList ID="ddlBranch" runat="server" Width="350">
                                        <asp:ListItem Value="">--- �ô�к��Ң� ---</asp:ListItem>
                                        <asp:ListItem VALUE="100">�ӹѡ�ҹ�˭� (�����չ��Թ���) ��. 379-3140</asp:ListItem>
                                        <asp:ListItem Value="101">�Ң��ǹ���� (����) �� 2261646-54, 2250076</asp:ListItem>
                                        <asp:ListItem VALUE="102">�ҢҴ͹���ͧ ��. 5334053, 5334050, 5334059</asp:ListItem>
                                        <asp:ListItem VALUE="103">�ҢҺҧ� ��. 4543959-66 FAX:5453967</asp:ListItem>
                                        <asp:ListItem VALUE="130">�Ң��ѵ�Ҹ����� ��. 9217582-9 FAX:9217581</asp:ListItem>
                                        <asp:ListItem VALUE="500">�ҢҪź��� ��. (038) 791672-9 FAX : (038) 270363</asp:ListItem>
                                        <asp:ListItem VALUE="201">�ҢҾ�ɳ��š ��. (055) 216978-82 FAX : (055) 242239</asp:ListItem>
                                        <asp:ListItem VALUE="200">�Ң���§���� ��. (053) 409410-8 FAX : (055) 409419</asp:ListItem>
                                        <asp:ListItem VALUE="601">�Ң��Ҵ�˭� ��. (074) 343650-8 FAX : (074) 343657 
                                      (074) 343659</asp:ListItem>
                                        <asp:ListItem VALUE="600">�Ң�����ɮ��ҹ� ��. (044) 261795-803 FAX : (044) 
                                      261804</asp:ListItem>
                                        <asp:ListItem VALUE="300">�Ңҹ�û�� ��. (034) 284754-6 FAX : (034) 284758</asp:ListItem>
                                        <asp:ListItem VALUE="401">�ҢҢ͹�� ��. (043) 324720-7 FAX : (043) 324728</asp:ListItem>
                                    </asp:DropDownList>
                                    <br />
                                    <div>��ҹ����ö�����Թ���ºѵ��ôԵ VISA , Master Card �����ӹѡ�ҹ�˭�</div>     
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton ID="rdoTransfer" runat="server" GroupName="pay_type" Text="�����Թ �����Ըա���͹��Һѭ�բͧ����ѷ�Թ��蹤���Сѹ��� �ӡѴ (��Ҫ�)" />
                                    <asp:Button ID="btnPrintDoc" runat="server" Text="������͡���" CssClass="btn" OnClientClick="openPayInSlip();return false;" />
                                    <table>
                                        <td width="30px"></td>
                                        <td>
                                            <ul>
                                                <li>
                                                    <IMG BORDER="0" SRC="./Images/paid.h4.gif" WIDTH="25" HEIGHT="23">
                                                    <div style="display:inline">��Ҥ�á�ا෾� �ӡѴ (��Ҫ�) �ҢҶ���Ѳ�ҡ�� (Br. No. 198) �ѭ�ա��������ѹ �Ţ��� 198-3-04200-1</div>
                                                </li>
                                                <li>
                                                    <IMG BORDER="0" SRC="./Images/paid.h5.gif" WIDTH="17" HEIGHT="16">
                                                    <div style="display:inline">��Ҥ���¾ҳԪ�� �ӡѴ (��Ҫ�) �Ң���չ��Թ��� �ѭ�ա��������ѹ �Ţ��� 080-3-00250-2 (Tr.code :3650)</div>
                                                </li>
                                                <li>
                                                    <IMG BORDER="0" SRC="./Images/bay2.gif" WIDTH="17" HEIGHT="16" alt=""> 
                                                    <div style="display:inline">��Ҥ�á�ا�����ظ�� �ӡѴ (��Ҫ�) �ҢҶ����չ��Թ��� ��ا෾��ձ� �ѭ�ա��������ѹ �Ţ��� 307-0-00444-4</div>
                                                </li>
                                            </ul>
                                        </td>
                                    </table>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton ID="rdoSCB" runat="server" GroupName="pay_type" Text="�����Թ �����Ըա���͹��Һѭ�բͧ����ѷ�Թ��蹤���Сѹ��� �ӡѴ(��Ҫ�) ��ҹ" />                                    
                                    <asp:HyperLink ID="lnkSCB" runat="server" NavigateUrl="http://www.scbeasy.com" Target="_blank">www.scbeasy.com</asp:HyperLink>
                                    <div style="display:inline">(��ͧ��Ѥ�����Ҫԡ�ͧ scbeasy ��͹)</div>
                                    <table>
                                        <tr>
                                            <td width="30px"></td>
                                            <td>
                                                <ul>
                                                    <li>�Ţ���ѭ�բͧ ���.�Թ��蹤���Сѹ���(080-3-00250-2) ,�����١��� (�������ѧ���) ��� CUST_NO (�ҡ�Ţ�����ҧ�ԧ �������Ѻ�˹�ҶѴ�)</li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>                                    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton ID="rdoDelivery" runat="server" GroupName="pay_type" Text="��ԡ�èѴ����ШѴ�����»�Сѹ�֧��ҹ (੾������ࢵ��ا෾�)" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton ID="rdoCredit" runat="server" GroupName="pay_type" Text="�����Թ ���ºѵ��ôԵ" />
                                </td>
                            </tr>
                        </table>                
                     </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>  
            <h2 class="subject-detail">ʶҹ���Ѵ���͡��� (¡��鹡�����͡���д����Թʴ ������)</h2>
            <table   class="tb-online-insCar" >
                <tr>
                    <td  colspan=4 >
                        <asp:RadioButton ID="rdoOld" runat="server" GroupName="sendAt" text="�����������������" Checked="true" onclick="copydata();"/>
                        <asp:RadioButton ID="rdoChange" runat="server" GroupName="sendAt" text="����" onclick="cleardata();" />
                    </td>
                </tr>
                <tr>
                    <td class="label"  >
                        ���� :
                    </td>
                    <td  >
                        <asp:TextBox ID="txtFName" runat="server"></asp:TextBox>
                        <span class="star">*</span>
                    </td>
                    <td class="label"  >
                        ���ʡ�� :
                    </td>
                    <td  >
                        <asp:TextBox ID="txtLName" runat="server"></asp:TextBox>
                        <span class="star">*</span>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        ������� :
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="txtAddress" runat="server" Width="250" MaxLength="50"></asp:TextBox>
                        <span class="star">*</span>(��� ��ҹ�Ţ���, ����, ���) 
                    </td>
                </tr>
                <tr>                    
                    <td class="label"  >
                        
                    </td>
                    <td  colspan="3">
                        <asp:TextBox ID="txtSubDistrict" runat="server" Width="250" MaxLength="50"></asp:TextBox>
                        <span class="star">*</span>(��� �Ӻ�, �ǧ)
                    </td>
                </tr>
                <tr>
                    <td class="label"  >
                        ����� / ࢵ :
                    </td>
                    <td  >
                        <asp:TextBox ID="txtDistrict" runat="server"></asp:TextBox>
                        <span class="star">*</span>
                    </td> 
                    <td class="label">
                        �ѧ��Ѵ :
                    </td>
                    <td  >
                        <asp:DropDownList ID="ddlProvince" runat="server">
                        </asp:DropDownList>
                        <span class="star">*</span>
                    </td>                               
                </tr>
                <tr>
                    <td class="label">
                        ������ɳ��� :
                    </td>
                    <td  >
                        <asp:TextBox ID="txtPostCode" runat="server"></asp:TextBox>
                        <span class="star">*</span>
                    </td>                              
                    <td class="label">
                        �������Ѿ�� :
                    </td>
                    <td>
                        <asp:TextBox ID="txtTelephone" runat="server"></asp:TextBox>                        
                    </td> 
                </tr>
            </table>

        <div style="margin:0px auto; width:190px;margin-top:15px;">       
            <asp:Button ID="btnSave" runat="server"  class="btn-default" style="margin-right:10px;" Text="�׹�ѹ"  OnClientClick="return validateForm();" OnClick="btnSave_Click" />
            <asp:Button ID="btnBack" runat="server"  class="btn-default" Text="��͹��Ѻ"   OnClientClick="history.back();return false;"  />
        </div>
         
        <div id="dialogMsg" style="display:none" title="��õ�Ǩ�ͺ������"   >
            <iframe id="MsgIframe"   frameborder="0" style="max-height:360" width="370" scrolling="auto"></iframe>
        </div>
        </div><!-- End class="prc-row"-->        
    </div><!-- End  <div class="context page-app"> -->
    <div style="display:none">
        <asp:TextBox ID="txtTmpCd" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtPaid" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtCampaign" runat="server"></asp:TextBox>
    </div>
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

