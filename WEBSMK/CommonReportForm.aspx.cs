using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using CrystalDecisions.Shared;

public partial class Module_Report_CommonReportForm : System.Web.UI.Page
{
    string strAppPath = System.Configuration.ConfigurationManager.AppSettings["appPath"].ToString();
    CrystalDecisions.CrystalReports.Engine.ReportDocument orpt = new CrystalDecisions.CrystalReports.Engine.ReportDocument();

    protected void Page_Load(object sender, EventArgs e)
    {
        string strParam = "";
        string[] arrParam;
        DataSet ds;        
        // �Ըա���� ������ ��ͧ�դ�� ?rptName=&���.rpt
        //                  &sid=&����session ����红����� dataset
        //                  &formula=&fomulavalue
        //                  &paramname1=&paramvalue1 �� &P_LANGUAGE=TH  
        //                  &paramname2=&paramvalue2
        // ��� parameter ��ͧ�������ú�ء���
        // ������ҧ url ����鷴�ͺ  http://localhost/LMS/Module/Report/CommonReportForm.aspx?rptName=/module/report/quotation.rpt&sid=dsReport&formula={DSL_QUOT_LN.sqh_id}="Q51060001"&P_companyname=1111
        Request.ContentEncoding = System.Text.Encoding.UTF8;
        //Response.ContentEncoding = System.Text.Encoding.UTF8;
        if (Request.QueryString["rptName"] != null)
        {
            // set report file name
            orpt.Load(strAppPath + "" + Request.QueryString["rptName"], OpenReportMethod.OpenReportByDefault);
            // set database or datasource
            TDS.Utility.MasterUtil.writeError("CommonReport ", Request.QueryString["sid"]);
            if (Request.QueryString["sid"] != null)
            {
                string sid = Request.QueryString["sid"];
                ds = (DataSet)(Session[sid.Trim()]);
                TDS.Utility.MasterUtil.writeError("CommonReport -ds.Count", ds.Tables.Count.ToString());
                for (int i = 0; i <= ds.Tables.Count - 1; i++)
                {
                    if (i == 0)
                        orpt.Database.Tables[0].SetDataSource(ds.Tables[0]);
                    else
                    {
                        orpt.Subreports[i - 1].Database.Tables[0].SetDataSource(ds.Tables[i]);
                    }
                }
            }
            else
            {
                orpt.SetDatabaseLogon("sa", "tdsadmin", "agnvmi_odbc", "agnvmi");
            }

            // set formula value
            //CrystalReportViewer1.SelectionFormula = Request.QueryString["formula"];

            // set parameter value �����ҹ�ҡ querystring ������Ƿ�� 2 �繵��
            for (int i = 2; i <= Request.QueryString.Count - 1; i++)
            {
                strParam = Request.QueryString[i];
                try
                {
                    if (Request.QueryString.GetKey(i) == "IsTest")
                    {
                        orpt.SetParameterValue(Request.QueryString.GetKey(i), Boolean.Parse(Request.QueryString[i]));
                    }
                    if (Request.QueryString.GetKey(i).StartsWith("P_"))
                    {
                        orpt.SetParameterValue(Request.QueryString.GetKey(i), Request.QueryString[i]);
                    }
                }
                catch (Exception ex)
                {
                    TDS.Utility.MasterUtil.writeLog("Report", ex.ToString() + "\n" + Request.QueryString["rptName"] + " : " + Request.QueryString.GetKey(i));
                }
            }
            System.IO.Stream oStream;
            byte[] byteArray = null;

            oStream = orpt.ExportToStream(ExportFormatType.PortableDocFormat);
            byteArray = new byte[oStream.Length];
            oStream.Read(byteArray, 0, Convert.ToInt32(oStream.Length));

            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf;charset=UTF-8";
            Response.ContentEncoding = System.Text.Encoding.UTF32;
            Response.Charset = "UTF-8";
            Response.AddHeader("content-lenght", byteArray.Length.ToString());
            Response.AddHeader("content-disposion", "attachment;filename=smk.pdf");
            Response.BinaryWrite(byteArray);
            Response.Flush();
            Response.End();


            //CrystalReportViewer1.PrintMode = CrystalDecisions.Web.PrintMode.Pdf;
            //CrystalReportViewer1.DisplayGroupTree = false;
            //if (Request.QueryString["P_ISEXPORT"] == "N")
            //    CrystalReportViewer1.HasExportButton = false;
            //else
            //    CrystalReportViewer1.HasExportButton = true;
            //CrystalReportViewer1.HasCrystalLogo = false;
            //CrystalReportViewer1.HasZoomFactorList = false;
            //CrystalReportViewer1.HasSearchButton = false;
            //CrystalReportViewer1.HasDrillUpButton = false;
            //CrystalReportViewer1.HasToggleGroupTreeButton = false;
            //CrystalReportViewer1.HasDrillUpButton = false;
            //CrystalReportViewer1.DisplayGroupTree = false;
            //CrystalReportViewer1.EnableDrillDown = false;
            //if (Request.QueryString["view"] != null)
            //    CrystalReportViewer1.HasPrintButton = false;
            //CrystalReportViewer1.ReportSource = orpt;
            //CrystalReportViewer1.DataBind();
        }
        
    }
    protected void Page_Unload(object sender, EventArgs e)
    {
        orpt.Close();
        orpt.Dispose();
    }
}
