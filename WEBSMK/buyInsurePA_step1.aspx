﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="buyInsurePA_step1.aspx.cs" Inherits="buyInsurePA_step1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div id="page-online-insCar"> 
        <h1 class="subject1">ซื้อประกัน-อุบัติเหตุ  </h1>
        <%--<div class="btnCallBack"><a href="./buyInsure.aspx?main_class=P" title="ซื้อประกันภัยอุบัติเหตุส่วนบุคคล และการเดินทาง">ซื้อประกัน</a></div>--%>
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="True">
        </asp:ScriptManager>
        <div class="stepOnline"> <img src="images/navi3step_s-1.jpg" /></div>
        <div class="prc-row">                
            <table width="630px">
                <tr>
                    <td>
                        <asp:DataGrid ID="dtgData" runat="server" AutoGenerateColumns="False" OnItemDataBound="dtgDataBind" 
                            Width="630px" ShowHeader="False" BorderWidth="0px" ShowFooter="false" class="tb-papromotion-Info">
                            <Columns>
                                <asp:TemplateColumn >
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Center" CssClass="tdCell1subColor" />
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lnkPromotion" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pm_desc") %>'></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Right" CssClass="tdCell2subColor" />
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pm_pa1_si") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="right" CssClass="tdCell2subColor" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblPrmm" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pm_prmm") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="pm_major_cd" ReadOnly="True" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="pm_minor_cd" ReadOnly="True" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="pm_desc2" ReadOnly="True" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="pm_desc" ReadOnly="True" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="pm_pa1" ReadOnly="True" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="pm_pa1_si" ReadOnly="True" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="pm_pa2" ReadOnly="True" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="pm_pa2_si" ReadOnly="True" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="pm_ttd" ReadOnly="True" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="pm_ttd_si" ReadOnly="True" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="pm_ptd" ReadOnly="True" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="pm_ptd_si" ReadOnly="True" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="pm_med" ReadOnly="True" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="pm_med_si" ReadOnly="True" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="pm_tax" ReadOnly="True" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="pm_stamp" ReadOnly="True" Visible="False"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>
            <h2 class="subject-detail">
                แผนประกันภัยอื่นๆ
            </h2>
            <table width="630px">
                <tr>
                    <td>
                        <asp:DataGrid ID="dtgDataOth" runat="server" AutoGenerateColumns="False" OnItemDataBound="dtgDataBind" Width="100%" ShowHeader="False" BorderWidth="0px">
                            <ItemStyle CssClass="t_cell" />
                            <HeaderStyle CssClass="t_heading" />
                            <Columns>
                                <asp:TemplateColumn>
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Width="40px" Text='<%# (int)DataBinder.Eval(Container, "DataSetIndex") +1%>'></asp:Label>
                                    </ItemTemplate>                        
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Center" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lnkOth" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pm_desc")%>' NavigateUrl='<%#GetUrl(DataBinder.Eval(Container, "DataItem.pm_major_cd"),DataBinder.Eval(Container, "DataItem.pm_minor_cd")) %>'></asp:HyperLink>
                                    </ItemTemplate>                                                
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="pm_major_cd" ReadOnly="True" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="pm_minor_cd" ReadOnly="True" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="pm_desc2" ReadOnly="True" Visible="False"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>
            <h2 class="subject-detail">
                <div onclick="window.location='./buyInsurePA_step2.aspx';" style="cursor:pointer">กำหนดความคุ้มครองเอง</div>                
            </h2>            
        </div><!-- End class="prc-row"-->
        <div id="dialogMsg" style="display:none" title="การตรวจสอบข้อมูล"   >
            <iframe id="MsgIframe"   frameborder="0" style="max-height:360" width="370" scrolling="auto"></iframe>
        </div>
    </div><!-- End  class="page-online-insCar" -->
    <div style="display:none">
        
    </div>
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

