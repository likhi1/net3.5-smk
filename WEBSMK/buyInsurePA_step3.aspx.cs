﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class buyInsurePA_step3 : System.Web.UI.Page
{
	public DataSet dsUI = new DataSet();
    protected void Page_Load(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        RegisterManager cmRegister = new RegisterManager();
        string strRegisNo = "";
        if (Session["DSRegister"] != null)
            ds = (DataSet)Session["DSRegister"];
        else
            Response.Redirect("buyInsurePA_step1.aspx", true);

        if (!IsPostBack)
        {
            initialDropdownList();
            strRegisNo = cmRegister.insertDataPersonelAccident(ds);
            ds.Tables[0].Rows[0]["rg_regis_no"] = strRegisNo;
            Session.Add("DSRegister", ds);
            docToUI(ds);
        }
        dsUI = ds.Copy();
        litScript.Text += "<script>copydata();</script>";
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        RegisterManager cmRegister = new RegisterManager();
        string strRegisNo = "";
        bool isSave = false;

        if (Session["DSRegister"] != null)
            ds = (DataSet)Session["DSRegister"];

        if (rdoCheck.Checked == true)
        {
            ds.Tables[0].Rows[0]["rg_pay_type"] = "1";
            ds.Tables[0].Rows[0]["sed_branch"] = ddlBranch.SelectedValue;
        }
        else if (rdoTransfer.Checked == true)
            ds.Tables[0].Rows[0]["rg_pay_type"] = "2";
        else if (rdoSCB.Checked == true)
            ds.Tables[0].Rows[0]["rg_pay_type"] = "3";
        else if (rdoDelivery.Checked == true)
            ds.Tables[0].Rows[0]["rg_pay_type"] = "4";
        else if (rdoCredit.Checked == true)
            ds.Tables[0].Rows[0]["rg_pay_type"] = "5";

        if (rdoOld.Checked == true)
            ds.Tables[0].Rows[0]["sed_at"] = "1";
        else if (rdoChange.Checked == true)
        {
            ds.Tables[0].Rows[0]["sed_at"] = "2";
            ds.Tables[0].Rows[0]["sed_name"] = txtFName.Text + " " + txtLName.Text;
            ds.Tables[0].Rows[0]["sed_ins_addr"] = txtAddress.Text + " " + txtSubDistrict.Text;
            ds.Tables[0].Rows[0]["sed_amphor"] = txtDistrict.Text;
            ds.Tables[0].Rows[0]["sed_changwat"] = ddlProvince.SelectedValue;
            ds.Tables[0].Rows[0]["sed_postcode"] = txtPostCode.Text;
            ds.Tables[0].Rows[0]["sed_tel"] = txtTelephone.Text;
        }

        Session.Add("DSRegister", ds);
        strRegisNo = ds.Tables[0].Rows[0]["rg_regis_no"].ToString();
        
        //if (strRegisNo != "")
            isSave = cmRegister.insertPaidData(ds);
            

        if (isSave == true)
        {
            if (rdoCredit.Checked == true)
            {
                Session["amount" + strRegisNo] = txtPaid.Text;
                Session["ref_no" + strRegisNo] = strRegisNo;
                Response.Redirect("PayCreditCard.aspx?RegisNo=" + strRegisNo, true);
            }
            else
            {
                Response.Redirect("buyInsurePA_step4.aspx?regis_no=" + strRegisNo, true);
            }
        }
        else
        {

        }
    }

    public void initialDropdownList()
    {
        DataSet ds;
        OnlineVoluntaryManager cmOnline = new OnlineVoluntaryManager();

        ds = cmOnline.getAllProvince();
        ddlProvince.DataSource = ds;
        ddlProvince.DataTextField = "ds_desc";
        ddlProvince.DataValueField = "ds_minor_cd";
        ddlProvince.DataBind();
        ddlProvince.Items.Insert(0, new ListItem(" - เลือก - ", ""));
    }
    public void docToUI(DataSet ds)
    {
        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            double dPrem = 0;
            double dStamp = 0;
            double dTax = 0;
            double dGross = 0;
            double dPaid = 0;
            dPrem = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["rg_prmm"]);
            dStamp = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["rg_stamp"]);
            dTax = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["rg_tax"]);
            dGross = dPrem + dStamp + dTax;
            dPaid = dGross;
            lblPremium.Text = FormatStringApp.FormatNDigit(ds.Tables[0].Rows[0]["rg_prmm"], 2) + " บาท";
            lblStamp.Text = FormatStringApp.FormatNDigit(ds.Tables[0].Rows[0]["rg_stamp"], 2) + " บาท";
            lblVat.Text = FormatStringApp.FormatNDigit(ds.Tables[0].Rows[0]["rg_tax"], 2) + " บาท";
            lblGrossPremium.Text = FormatStringApp.FormatNDigit(dGross, 2) + " บาท";
            txtPaid.Text = dGross.ToString();
            txtTmpCd.Text = ds.Tables[0].Rows[0]["tm_tmp_cd"].ToString();
        }
    }
}
