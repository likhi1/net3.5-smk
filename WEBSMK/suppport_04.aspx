﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="suppport_04.aspx.cs" Inherits="suppport_04" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

<div class="subject1">ระบบงานตัวแทน</div>
<div class="subject-detail">รวมโปรแกรมสำเร็จสำหรับตัวแทน</div>
<div class="subject3">QP program</div>
<ul><li>โปรแกรมคำนวณเบี้ยประกัน (QP)สำหรับตัวแทน <a href="downloads/agent/QP_Setup_2009.zip" class="commonLink" >&raquo;  Click Download</a> </li>
<li>Upgrade โปรแกรมคำนวณเบี้ยประกัน (QP) สำหรับตัวแทน Version 2013.1.2 <a href="downloads/agent/QPAgt_upgrade13-2.zip"  class="commonLink" >&raquo;  Click Download</a>  </li></ul>

<div class="subject3">MI2000 Program สำหรับ Window Service Pack 2  </div> 
<ul>
<li>โปรแกรมติดตั้ง MI2000 (Full Version ขนาด 86.8 MB.)  <a href="downloads/agent/MI2000_SETUP.zip"  class="commonLink" >&raquo; Click Download</a> </li>
<li>สำหรับเลขเครื่องหมาย 13 หลักเท่านั่น(ควรใช้เลขเครื่องหมาย 11 หลักให้หมดก่อน )  <a href="downloads/agent/upgrade_2009.zip"  class="commonLink" >&raquo; Click Download</a></li>
<li>สำหรับตัวแทน ธกส. <a href="downloads/agent/Upgrade_Baac.zip"  class="commonLink" >&raquo;  Click Download</a></li>
<li>Upgrade MI2000 ประจำวันที่ 22/07/2011 (เพิ่มรหัสรถ) <a href="downloads/agent/Upgrade2011_ORG.zip"  class="commonLink" >&raquo;  Click Download</a></li></ul>
</asp:Content>

