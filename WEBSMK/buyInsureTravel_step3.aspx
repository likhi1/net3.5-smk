﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="buyInsureTravel_step3.aspx.cs" Inherits="buyInsureTravel_step3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div id="page-online-insCar">
        <h1 class="subject1">ทำประกันออนไลน์ </h1>
        การทำรายการประกันภัยของท่านเรียบร้อยแล้ว หากท่านมีข้อสงสัยเพิ่มเติม สามารถติดต่อสอบถามได้ที่ 02-378-7167<br />
        <br />        
        <div class="prc-row">    
        <h2>
        เลขที่อ้างอิง : <asp:Label ID="lblRefNo" runat="server" Text=""></asp:Label>
        </h2>    
        <h2 class="subject-detail">รายละเอียดผู้เอาประกัน</h2>
        <table width="630px">
            <tr>
                <td valign="top">
                    <table class="tb-online-insCar">              
                        <tr>
                            <td class="label" >
                                ชื่อ :
                            </td>
                            <td >
                                <asp:Label ID="lblFName" runat="server" Text=""></asp:Label>
                            </td>    
                            <td class="label">
                                นามสกุล :
                            </td>
                            <td>
                                <asp:Label ID="lblLName" runat="server" Text=""></asp:Label>
                            </td>                            
                        </tr>
                        <tr>
                            <td class="label">
                                ที่อยู่ :
                            </td>
                            <td colspan="3">
                                <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>                    
                            <td class="label"  >
                                ตำบล / แขวง :
                            </td>
                            <td colspan="3">
                                <asp:Label ID="lblSubDistrict" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" >
                                อำเภอ / เขต :
                            </td>
                            <td width="150px">
                                <asp:Label ID="lblDistrict" runat="server" Text=""></asp:Label>
                            </td>      
                            <td class="label">
                                จังหวัด :
                            </td>
                            <td  >
                                <asp:Label ID="lblProvince" runat="server" Text=""></asp:Label>
                            </td>                          
                        </tr>
                        <tr>
                            <td class="label">
                                รหัสไปรษณีย์ :
                            </td>
                            <td width="130px">
                                <asp:Label ID="lblPostCode" runat="server" Text=""></asp:Label>
                            </td>      
                            <td class="label">
                                เลขที่บัตรประชาชน :
                            </td>
                            <td  >
                                <asp:Label ID="lblIDCard" runat="server" Text=""></asp:Label>
                            </td>                         
                        </tr>
                        <tr>
                            <td class="label">
                                เบอร์โทรศัพท์มือถือ :
                            </td>
                            <td>
                                <asp:Label ID="lblMobile" runat="server" Text=""></asp:Label>
                            </td>  
                            <td class="label">
                                เบอร์โทรศัพท์บ้าน :
                            </td>
                            <td>
                                <asp:Label ID="lblTelephone" runat="server" Text=""></asp:Label>
                            </td>                                                  
                        </tr>
                        <tr>
                            <td class="label">
                                อีเมล์ :
                            </td>
                            <td>
                                <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label>
                            </td>                    
                        </tr>                        
                    </table>
                </td>
            </tr>            
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        
        <h2 class="subject-detail">
            รายละเอียดผู้รับประโยชน์  
        </h2>            
        <table class="tb-online-insCar">              
            <tr>
                <td class="label" >
                    ชื่อ :
                </td>
                <td >
                    <asp:Label ID="lblBefFName" runat="server" Text=""></asp:Label>
                </td>    
                <td class="label">
                    นามสกุล :
                </td>
                <td>
                    <asp:Label ID="lblBefLName" runat="server" Text=""></asp:Label>
                </td>                            
            </tr>
            <tr>
                <td class="label">ความสัมพันธ์	 :</td>
                <td>
                    <asp:Label ID="lblRelationship" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="label" valign="top">ที่อยู่ :</td>
                <td colspan="3">
                    <asp:Label ID="lblBefAddress1" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>                    
                <td class="label"  >
                    ตำบล / แขวง :
                </td>
                <td colspan="3">
                    <asp:Label ID="lblBefAddress2" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="label" >
                    อำเภอ / เขต :
                </td>
                <td width="150px">
                    <asp:Label ID="lblBefDistrict" runat="server" Text=""></asp:Label>
                </td>      
                <td class="label">
                    จังหวัด :
                </td>
                <td  >
                    <asp:Label ID="lblBefProvince" runat="server" Text=""></asp:Label>
                </td>                          
            </tr>
            <tr>
                <td class="label">
                    รหัสไปรษณีย์ :
                </td>
                <td width="130px">
                    <asp:Label ID="lblBefPostCode" runat="server" Text=""></asp:Label>
                </td>      
                <td class="label">
                    เบอร์โทรศัพท์ :
                </td>
                <td>
                    <asp:Label ID="lblBefTelephone" runat="server" Text=""></asp:Label>
                </td>                                                  
            </tr>
            <tr>
                <td class="label">
                    อีเมล์ :
                </td>
                <td>
                    <asp:Label ID="lblBefEmail" runat="server" Text=""></asp:Label>
                </td>                    
            </tr> 
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>        
        </div>                
        <h2 class="subject-detail">รายละเอียดการชำระเงินและการจัดส่งเอกสาร</h2>
        <table width="630px">
            <tr>
                <td valign="top">
                    <table class="tb-online-insCar">
                        <tr>
                            <td>วันที่เริ่มคุ้มครอง</td>
                            <td>&nbsp;</td>
                            <td class="label"></td>
                            <td>
                                <asp:Label ID="lblStartDate" runat="server" Text=""></asp:Label>
                            </td>                            
                        </tr>
                        <tr>
                            <td class="label"   >เบี้ยสุทธิ :</td>
                            <td>
                                <asp:Label ID="lblPremium" runat="server" Text=""></asp:Label>
                            </td>
                            <td class="label">อากร :</td>
                            <td>
                                <asp:Label ID="lblStamp" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">ภาษี :</td>
                            <td  >
                                <asp:Label ID="lblVat" runat="server" Text=""></asp:Label>
                            </td>
                            <td class="label">เบี้ยรวม :</td>
                            <td>
                                <asp:Label ID="lblGrossPremium" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">เบี้ยรวมทั้งสิ้น :</td>      
                            <td  >
                                <asp:Label ID="lblPaid" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>    
                        <tr>
                            <td colspan="4">
                                <table>
                                    <tr>
                                        <td class="label" valign="top">วิธีการชำระเบี้ยประกันภัย :</td>
                                        <td colspan="4">
                                            <asp:Label ID="lblPaidMethod" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label"><div style="display:none">สถานที่จัดส่งเอกสาร :</div></td>
                                        <td colspan="4">
                                            <asp:Label ID="lblSendDoc" runat="server" Text="" Visible="false"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="4">
                                            <asp:Label ID="lblDocAddress1" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>                            
                        </tr>                        
                    </table>
                </td>
            </tr>
        </table>
        
        <strong>ขอบคุณที่ท่านไว้วางใจเลือกใช้บริการกับ บริษัท สินมั่นคงประกันภัย จำกัด (มหาชน)</strong>
        <br />
        <br />
        <div style="margin: 0px auto; width: 190px;  margin-top: 15px;">
            <asp:LinkButton ID="LinkButton1" runat="server" OnClick="btnPrint_Click">รูปแบบ pdf</asp:LinkButton>&nbsp;
            <a href="home.aspx">กลับสู่หน้าแรก</a><br />
        </div>
    </div>
    <!-- End  <div class="context page-app"> -->
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

