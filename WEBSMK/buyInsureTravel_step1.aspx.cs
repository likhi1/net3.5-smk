﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data;
using System.Web.Services;

public partial class buyInsureTravel_step1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initialDropdownList();
            ddlHour.SelectedValue = DateTime.Now.Hour.ToString();
            ddlMinute.SelectedValue = DateTime.Now.Minute.ToString();
            ddlDays.SelectedValue = "1";
            rdoInsAddress.Attributes.Add("onclick", "rdoBefAddress_Click()");
            rdoInsAddress.Attributes.Add("onclick", "rdoBefAddress_Click()");
            ddlSumIns.Attributes.Add("onchange", "PopulateMedical()");
        }
    }
    protected void btnCalculate_Click(object sender, EventArgs e)
    {
		TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        double dStartDate = 0;
        double dMinDate = 0;
        string strMinDate = "";
        string strScript = "";

        if (DateTime.Now.Hour >= Convert.ToInt32(ddlHour.SelectedValue)  && DateTime.Now.Minute >= Convert.ToInt32(ddlMinute.SelectedValue))
        {
            dMinDate = Convert.ToDouble(cmDate.convertDateStringForDBTH(FormatStringApp.FormatDate(DateTime.Now.AddDays(1))).Replace("/",""));
            strMinDate = FormatStringApp.FormatDate(DateTime.Now.AddDays(1));
        }
        else
        {
            dMinDate = Convert.ToDouble(cmDate.convertDateStringForDBTH(FormatStringApp.FormatDate(DateTime.Now)).Replace("/",""));
            strMinDate = FormatStringApp.FormatDate(DateTime.Now);
        }
        dStartDate = Convert.ToDouble(cmDate.convertDateStringForDBTH(txtStartDate.Text).Replace("/",""));        
        if (dStartDate < dMinDate)
        {
            strScript = "<script>";
            strScript += " document.getElementById('divLoading').style.display = 'none';";
			strScript += "showMessagePage('วันที่เริ่มคุ้มครองต้องมีค่าตั้งแต่วันที่ " + strMinDate + "');";
			strScript += " </script>";
			ScriptManager.RegisterClientScriptBlock(Literal1, typeof(Literal), "s", strScript, false);
        }
		else
		{	
			calPremium();
		}
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        string strTmpCD = "";
        RegisterManager cmRegister = new RegisterManager();
		
		TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        double dStartDate = 0;
        double dMinDate = 0;
        string strMinDate = "";
        string strScript = "";

        if (DateTime.Now.Hour >= Convert.ToInt32(ddlHour.SelectedValue) && DateTime.Now.Minute >= Convert.ToInt32(ddlMinute.SelectedValue))
        {
            dMinDate = Convert.ToDouble(cmDate.convertDateStringForDBTH(FormatStringApp.FormatDate(DateTime.Now.AddDays(1))).Replace("/",""));
            strMinDate = FormatStringApp.FormatDate(DateTime.Now.AddDays(1));
        }
        else
        {
            dMinDate = Convert.ToDouble(cmDate.convertDateStringForDBTH(FormatStringApp.FormatDate(DateTime.Now)).Replace("/",""));
            strMinDate = FormatStringApp.FormatDate(DateTime.Now);
        }
        dStartDate = Convert.ToDouble(cmDate.convertDateStringForDBTH(txtStartDate.Text).Replace("/",""));        
        if (dStartDate < dMinDate)
        {
            strScript = "<script>";
            strScript += " document.getElementById('divLoading').style.display = 'none';";
			strScript += "showMessagePage('วันที่เริ่มคุ้มครองต้องมีค่าตั้งแต่วันที่ " + strMinDate + "');";
			strScript += " </script>";
			ScriptManager.RegisterClientScriptBlock(Literal1, typeof(Literal), "s", strScript, false);
        }
		else
		{	
			if (Session["DSRegister"] != null)
				ds = (DataSet)Session["DSRegister"];


			strTmpCD = cmRegister.insertTmpTravelAccident(ds);
			ds.Tables[0].Rows[0]["tm_tmp_cd"] = strTmpCD;
			Session.Add("DSRegister", ds);
			if (strTmpCD != "")
				Response.Redirect("buyInsureTravel_step2.aspx", true);	
		}
    }

    public void initialDropdownList()
    {
        DataSet ds;
        DataView dvTmp;
        OnlineCompulsaryManager cmOnline = new OnlineCompulsaryManager();
        OnlineVoluntaryManager cmOnlineVol = new OnlineVoluntaryManager();
        NonMotorManager cmNonMotor = new NonMotorManager();
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        string[] columnDaysNames = { "text", "value" };
        string[] columnDaysTypes = { "string", "string" };
        int iIndex = 0;

        ds = cmOnlineVol.getAllProvince();
        ddlProvince.DataSource = ds;
        ddlProvince.DataTextField = "ds_desc";
        ddlProvince.DataValueField = "ds_minor_cd";
        ddlProvince.DataBind();
        ddlProvince.Items.Insert(0, new ListItem("-- กรุณาระบุ --", ""));

        ddlBefProvince.DataSource = ds;
        ddlBefProvince.DataTextField = "ds_desc";
        ddlBefProvince.DataValueField = "ds_minor_cd";
        ddlBefProvince.DataBind();
        ddlBefProvince.Items.Insert(0, new ListItem("-- กรุณาระบุ --", ""));

        ds = new DataSet();
        ds.Tables.Add("");
        TDS.Utility.MasterUtil.AddColumnToDataTable(ds.Tables[0], columnDaysTypes, columnDaysNames);
        for (int i = 0; i <= 23; i++)
        {
            ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
            ds.Tables[0].Rows[i]["text"] = (i + 1).ToString("00");
            ds.Tables[0].Rows[i]["value"] = (i + 1);
        }
        ddlHour.DataSource = ds;
        ddlHour.DataTextField = "text";
        ddlHour.DataValueField = "value";
        ddlHour.DataBind();

        ds = new DataSet();
        ds.Tables.Add("");
        TDS.Utility.MasterUtil.AddColumnToDataTable(ds.Tables[0], columnDaysTypes, columnDaysNames);
        for (int i = 0; i <= 59; i++)
        {
            ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
            ds.Tables[0].Rows[i]["text"] = (i + 1).ToString("00");
            ds.Tables[0].Rows[i]["value"] = (i + 1);
        }
        ddlMinute.DataSource = ds;
        ddlMinute.DataTextField = "text";
        ddlMinute.DataValueField = "value";
        ddlMinute.DataBind();
        
        ds = new DataSet();
        ds.Tables.Add("");
        TDS.Utility.MasterUtil.AddColumnToDataTable(ds.Tables[0], columnDaysTypes, columnDaysNames);
        for (int i = 0; i <= 59; i++)
        {
            ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
            ds.Tables[0].Rows[i]["text"] = (i + 1).ToString("00");
            ds.Tables[0].Rows[i]["value"] = (i + 1);            
        }
        ddlDays.DataSource = ds;
        ddlDays.DataTextField = "text";
        ddlDays.DataValueField = "value";
        ddlDays.DataBind();

        ds = new DataSet();
        ds.Tables.Add("");
        TDS.Utility.MasterUtil.AddColumnToDataTable(ds.Tables[0], columnDaysTypes, columnDaysNames);        
        for (int i = 100000; i <= 1000000; i = i + 100000)
        {
            ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
            ds.Tables[0].Rows[iIndex]["text"] = i.ToString("#,###,##0.00");
            ds.Tables[0].Rows[iIndex]["value"] = i;
            iIndex++;
        }
        ddlSumIns.DataSource = ds;
        ddlSumIns.DataTextField = "text";
        ddlSumIns.DataValueField = "value";
        ddlSumIns.DataBind();

        ds = new DataSet();
        ds.Tables.Add("");
        TDS.Utility.MasterUtil.AddColumnToDataTable(ds.Tables[0], columnDaysTypes, columnDaysNames);
        iIndex = 0;
        for (int i = 0; i <= 1000000; i = i + 1000000)
        {
            ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
            ds.Tables[0].Rows[iIndex]["text"] = i.ToString("#,###,##0.00");
            ds.Tables[0].Rows[iIndex]["value"] = i;
            iIndex++;
        }
        ddlMedical.DataSource = ds;
        ddlMedical.DataTextField = "text";
        ddlMedical.DataValueField = "value";
        ddlMedical.DataBind();

        ds = cmNonMotor.getAllRelation();
        ddlBefRelationship.DataSource = ds;
        ddlBefRelationship.DataTextField = "ds_desc";
        ddlBefRelationship.DataValueField = "ds_minor_cd";
        ddlBefRelationship.DataBind();
        ddlBefRelationship.Items.Insert(0, "-- ระบุความสัมพันธ์ --");
        ddlBefRelationship.Items[0].Value = "";
    }
    private void calPremium()
    {
        NonMotorManager cmComp = new NonMotorManager();
        RegisterManager cmRegister = new RegisterManager();
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        DataSet dsData;
        DataSet dsPremium;
        string strTmpCD;
        string strScript = "";
        string strFlagMedical = "N";
        if (Request.Form[ddlMedical.UniqueID] == "0")
            strFlagMedical = "N";
        else if (Convert.ToDouble("0" + Request.Form[ddlMedical.UniqueID]) > 0)
            strFlagMedical = "Y";

        dsPremium = cmComp.getTravelAccidentNewPremium(ddlDays.SelectedValue, ddlSumIns.SelectedValue, strFlagMedical, txtNumIns.Text);
        lblPremium.Text = FormatStringApp.Format2Digit(dsPremium.Tables[0].Rows[0]["sum_prmm"].ToString());
        lblInternetPremium.Text = FormatStringApp.Format2Digit(dsPremium.Tables[0].Rows[0]["total_prmm"].ToString());
        txtPremium.Text = dsPremium.Tables[0].Rows[0]["ta_prmm"].ToString();
        txtStamp.Text = dsPremium.Tables[0].Rows[0]["stamp_prmm"].ToString();
        txtTax.Text = dsPremium.Tables[0].Rows[0]["tax_prmm"].ToString();
        txtDiscountRate.Text = dsPremium.Tables[0].Rows[0]["disc_rate"].ToString();
        txtDiscountAmt.Text = dsPremium.Tables[0].Rows[0]["disc_amt"].ToString();
        txtPremiumDiscount.Text = dsPremium.Tables[0].Rows[0]["net_prmm"].ToString();
        txtGrossPremiumDiscount.Text = dsPremium.Tables[0].Rows[0]["total_prmm"].ToString();
        dsData = UIToDoc();

        litScript.Text += "<script>";
        litScript.Text += " document.getElementById('divPremium').style.display = 'inline';";
        litScript.Text += "</script>";
        strTmpCD = cmRegister.insertTmpTravelAccident(dsData);
        txtTmpCD.Text = strTmpCD;
        dsData.Tables[0].Rows[0]["tm_tmp_cd"] = strTmpCD;
        if (strTmpCD == "")
        {
            //dsErrLabel = (DataSet)Session["dsLabelError"];
            //UcError1.showMessage(cmLabel.getLabel(dsErrLabel, "err000002", Session["Language"].ToString()));
        }
        ViewState.Add("KeyTmp", strTmpCD);
        Session.Add("DSRegister", dsData);

        strScript = "<script>";
        strScript += " document.getElementById('divPremium').style.display = 'block';";
        strScript += " document.getElementById('divLoading').style.display = 'none';";
        strScript += " </script>";
        ScriptManager.RegisterClientScriptBlock(Literal1, typeof(Literal), "s", strScript, false);
    }
    private DataSet UIToDoc()
    {
        DataSet dsData = new DataSet();
        // insert tmpreg, tmpvolun, tmpmtveh, tmpcompul
        string[] ColumnName = { "tm_tmp_cd",
                                    //register
                                    "rg_regis_no", "rg_member_cd", "rg_member_ref", "rg_main_class", "rg_pol_type", 
                                    "rg_effect_dt", "rg_expiry_dt", "rg_regis_dt", "rg_regis_time", "rg_regis_type", 
                                    "rg_ins_fname", "rg_ins_lname", "rg_ins_addr1", "rg_ins_addr2", "rg_ins_amphor",
                                    "rg_ins_changwat", "rg_ins_postcode", "rg_ins_tel", "rg_ins_email", "rg_sum_ins", 
                                    "rg_prmm", "rg_tax", "rg_stamp", "rg_pmt_cd", "rg_fleet_perc", 
                                    "rg_pay_type", "rg_payment_stat", "rg_ref_bank", "rg_approve", "rg_prmmgross", 
                                    // register - new column
                                    "rg_ins_mobile", "rg_ins_idcard", "oth_distance", "oth_region", "oth_flagdeduct", 
                                    "oth_oldpolicy", "insc_id", "mott_id",
                                    //personal
                                    "pe_regis_no", "pe_eff_time", "pe_card_type", "pe_card_no", "pe_birth_dt", 
                                    "pe_occup", "pe_ben_fname", "pe_ben_lname", "pe_ben_addr1", "pe_ben_addr2", 
                                    "pe_ben_amphor", "pe_ben_changwat", "pe_ben_postcode", "pe_ben_tel", "pe_ben_email",
                                    "pe_relation", "flag_befaddress",
                                    // ta_cover
                                    "ta_regis_no", "ta_tot_ins", "ta_sum_ins", "ta_medic_exp", "ta_prmm",
                                    // senddoc
                                    "sed_regis_no", "sed_branch", "sed_name", "sed_ins_addr", "sed_amphor",
                                    "sed_changwat", "sed_postcode", "sed_tel", "sed_at"
                                    };
        string[] ColumnType = { "string", 
                                  //
                                    "string", "string", "string", "string", "string", 
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    //
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string",                                    
                                    //
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string",
                                    //
                                    "string", "string", "string", "string", "string",
                                    //
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string"};
        DataTable dt = new DataTable();
        dt = TDS.Utility.MasterUtil.AddColumnToDataTable(dt, ColumnType, ColumnName);
        dsData.Tables.Add(dt);
        dsData.Tables[0].Rows.Add(dsData.Tables[0].NewRow());        
        // register
        dsData.Tables[0].Rows[0]["rg_main_class"] = "T";
        dsData.Tables[0].Rows[0]["rg_pol_type"] = "T";
        dsData.Tables[0].Rows[0]["rg_effect_dt"] = txtStartDate.Text;
        DateTime dte = new DateTime(Convert.ToInt32(txtStartDate.Text.Substring(6, 4)) - 543, Convert.ToInt32(txtStartDate.Text.Substring(3, 2)), Convert.ToInt32(txtStartDate.Text.Substring(0, 2)));
        dte = dte.AddYears(1);
        dsData.Tables[0].Rows[0]["rg_expiry_dt"] = FormatStringApp.FormatDate(dte);
        dsData.Tables[0].Rows[0]["rg_regis_dt"] = FormatStringApp.FormatDate(DateTime.Now);
        dsData.Tables[0].Rows[0]["rg_regis_time"] = FormatStringApp.FormatTime(DateTime.Now); 
        dsData.Tables[0].Rows[0]["rg_ins_fname"] = txtFName.Text;
        dsData.Tables[0].Rows[0]["rg_ins_lname"] = txtLName.Text;
        dsData.Tables[0].Rows[0]["rg_ins_addr1"] = txtAddress.Text;
        dsData.Tables[0].Rows[0]["rg_ins_addr2"] = txtSubDistrict.Text;
        dsData.Tables[0].Rows[0]["rg_ins_amphor"] = txtDistrict.Text;
        dsData.Tables[0].Rows[0]["rg_ins_changwat"] = ddlProvince.SelectedValue;
        dsData.Tables[0].Rows[0]["rg_ins_postcode"] = txtPostCode.Text;
        dsData.Tables[0].Rows[0]["rg_ins_tel"] = txtTelephone.Text;
        dsData.Tables[0].Rows[0]["rg_ins_mobile"] = txtMobile.Text;
        dsData.Tables[0].Rows[0]["rg_ins_email"] = txtEmail.Text;
        dsData.Tables[0].Rows[0]["rg_sum_ins"] = ddlSumIns.SelectedValue;
        dsData.Tables[0].Rows[0]["rg_prmm"] = txtPremiumDiscount.Text.Replace(",", "");
        dsData.Tables[0].Rows[0]["rg_tax"] = txtTax.Text.Replace(",", "");
        dsData.Tables[0].Rows[0]["rg_stamp"] = txtStamp.Text.Replace(",", "");
        dsData.Tables[0].Rows[0]["rg_prmmgross"] = txtGrossPremiumDiscount.Text;
        // personel        
        dsData.Tables[0].Rows[0]["pe_regis_no"] = "";
        dsData.Tables[0].Rows[0]["pe_eff_time"] = ddlHour.SelectedValue + ":" + ddlMinute.SelectedValue;
        dsData.Tables[0].Rows[0]["pe_card_type"] = "";
        dsData.Tables[0].Rows[0]["pe_card_no"] = txtIDCard.Text;
        dsData.Tables[0].Rows[0]["pe_birth_dt"] = "";
        dsData.Tables[0].Rows[0]["pe_occup"] = "0";
        dsData.Tables[0].Rows[0]["pe_ben_fname"] = txtBefFName.Text;
        dsData.Tables[0].Rows[0]["pe_ben_lname"] = txtBefLName.Text;
        dsData.Tables[0].Rows[0]["pe_relation"] = ddlBefRelationship.SelectedValue;
        if (rdoInsAddress.Checked == true)
        {
            dsData.Tables[0].Rows[0]["pe_ben_addr1"] = txtAddress.Text;
            dsData.Tables[0].Rows[0]["pe_ben_addr2"] = txtSubDistrict.Text;
            dsData.Tables[0].Rows[0]["pe_ben_amphor"] = txtDistrict.Text;
            dsData.Tables[0].Rows[0]["pe_ben_changwat"] = ddlProvince.SelectedValue;
            dsData.Tables[0].Rows[0]["pe_ben_postcode"] = txtPostCode.Text;
            //dsData.Tables[0].Rows[0]["pe_ben_tel"] = txtTelephone.Text;
            //dsData.Tables[0].Rows[0]["pe_ben_email"] = txtEmail.Text;
        }
        else if (rdoOtherAddress.Checked == true)
        {
            dsData.Tables[0].Rows[0]["pe_ben_addr1"] = txtBefAddress.Text;
            dsData.Tables[0].Rows[0]["pe_ben_addr2"] = txtBefSubDistrict.Text;
            dsData.Tables[0].Rows[0]["pe_ben_amphor"] = txtBefDistrict.Text;
            dsData.Tables[0].Rows[0]["pe_ben_changwat"] = ddlBefProvince.SelectedValue;
            dsData.Tables[0].Rows[0]["pe_ben_postcode"] = txtBefPostcode.Text;
            //dsData.Tables[0].Rows[0]["pe_ben_tel"] = txtBefTelephone.Text;
            //dsData.Tables[0].Rows[0]["pe_ben_email"] = txtBefEmail.Text;
        }
        dsData.Tables[0].Rows[0]["pe_ben_tel"] = txtBefTelephone.Text;
        dsData.Tables[0].Rows[0]["pe_ben_email"] = txtBefEmail.Text;
        // ta_cover
        dsData.Tables[0].Rows[0]["ta_regis_no"] = "";
        dsData.Tables[0].Rows[0]["ta_tot_ins"] = txtNumIns.Text;
        dsData.Tables[0].Rows[0]["ta_sum_ins"] = ddlSumIns.SelectedValue;
        dsData.Tables[0].Rows[0]["ta_medic_exp"] = Request.Form[ddlMedical.UniqueID];
        dsData.Tables[0].Rows[0]["ta_prmm"] = txtGrossPremiumDiscount.Text;
        // new column
        dsData.Tables[0].Rows[0]["oth_distance"] = "";
        dsData.Tables[0].Rows[0]["oth_region"] = "";
        dsData.Tables[0].Rows[0]["oth_flagdeduct"] = "";
        dsData.Tables[0].Rows[0]["oth_oldpolicy"] = "";
        dsData.Tables[0].Rows[0]["insc_id"] = "";
        dsData.Tables[0].Rows[0]["mott_id"] = "";

        return dsData;
    }

    [WebMethod]
    public static ArrayList GetCoverMedical(string strSumIns)
    {
        DataSet ds = new DataSet();
        ArrayList list = new ArrayList();
        double dMedicine = Convert.ToDouble(strSumIns) * 10 / 100;
        list.Add(new ListItem("-- กรุณาระบุ --",""));
        list.Add(new ListItem("0", "0"));
        list.Add(new ListItem(FormatStringApp.FormatMoney(dMedicine), dMedicine.ToString()));        
        return list;
    }
}
