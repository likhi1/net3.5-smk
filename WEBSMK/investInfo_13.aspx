﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="investInfo_13.aspx.cs" Inherits="investInfo_13" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

  
  <div class="subject1">มติที่ประชุมคณะกรรมการ /ผู้ถือหุ้น</div>
  
  <div class="subject2">มติที่ประชุมคณะกรรมการบริษัทฯ</div> 
      
     <table class="tb-quarter"  width="80%">
        <tr>
            <th class="no-borderleft" width="30%"> ปี</th>
            <th colspan="2">
              มติที่ประชุมคณะกรรมการบริษัทฯ</th>
        </tr>
        <tr>
            <td width="30%" class="no-borderleft">
                2555
            </td>
            <td width="64"  class="no-border-leftright" ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"  /> 
            </td>
            <td width="341" class="no-border-leftright   align-left" > 
             <a href="downloads/investment/meet-6-2555.pdf">มติที่ประชุมคณะกรรมการบริษัทฯ ครั้งที่ 6/2555</a> </td>
        </tr>
        <tr>
          <td class="no-borderleft"> 2554</td>
          <td  class="no-border-leftright" ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"  /></td>
          <td class="no-border-leftright   align-left" ><a href="downloads/investment/meet-5-2554.pdf">มติที่ประชุมคณะกรรมการบริษัทฯ ครั้งที่ 5/2554</a></td>
        </tr>
     </table>
     
  <div class="subject2">มติที่ประชุมผู้ถือหุ้น</div>
       
     <table class="tb-quarter"  width="80%">
       <tr>
         <th class="no-borderleft" width="30%"> ปี</th>
         <th colspan="2">มติที่ประชุมผู้ถือหุ้น</th>
       </tr>
       <tr>
         <td width="30%" class="no-borderleft"> 2555 </td>
         <td width="64"  class="no-border-leftright" ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"  /></td>
         <td width="341" class="no-border-leftright   align-left" ><a href="downloads/investment/meet-6-2555">มติที่ประชุมผู้ถือหุ้น ประจำปี 2555</a></td>
       </tr>
       <tr>
         <td class="no-borderleft"> 2554 </td>
         <td  class="no-border-leftright" ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"  /></td>
         <td class="no-border-leftright   align-left" ><a href="downloads/investment/meet-7-2554.pdf">มติที่ประชุมผู้ถือหุ้น ประจำปี 2554</a></td>
       </tr>
       <tr>
         <td class="no-borderleft"> 2553 </td>
         <td  class="no-border-leftright" ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"  /></td>
         <td class="no-border-leftright   align-left" ><a href="downloads/investment/meet-6-2553.pdf">มติที่ประชุมผู้ถือหุ้น ประจำปี 2553</a></td>
       </tr>
     </table>
    
  
     <div class="notation2">
เอกสารเป็นไฟล์ PDF การเปิดดูจำเป็นต้องมีโปรแกรม  Adobe Reader คลิ๊กที่ไอคอนโปรแกรมเพื่อติดตั้ง   
<a href="http://www.adobe.com/products/acrobat/readstep2.html"
target="_blank"><img src="<%=Config.SiteUrl%>images/icon-pdf-download.gif"  />
</a>
</div>
  
   
</asp:Content>

