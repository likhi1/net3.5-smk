﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.Web.Services;

public partial class buyInsurePA_step2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        string strMajor = "";
        string strMinor = "";
        NonMotorManager cm = new NonMotorManager();
        if (!IsPostBack)
        {
            initialDropdownList();
            ddlPa1.Attributes.Add("onchange", "PopulateMedical()");
            ddlPa2.Attributes.Add("onchange", "PopulateMedical()");
            if (Request.QueryString["pm_major_cd"] != null && Request.QueryString["pm_minor_cd"] != null)
            {
                strMajor = Request.QueryString["pm_major_cd"];
                strMinor = Request.QueryString["pm_minor_cd"];
                ds = cm.GetPAPromotionByCode(strMajor, strMinor);
                docToUI(ds);
                litScript.Text += "<script>";
                litScript.Text += "document.getElementById('divFixCover').style.display = 'block';";
                litScript.Text += "</script>";
            }
            else
            {
                litScript.Text += "<script>\n";
                litScript.Text += "document.getElementById('divChooseCover').style.display = 'block';\n";
                litScript.Text += "document.getElementById('" + rdoPa1.ClientID + "').checked = true;\nrdoPA_Click('PA1');\nPopulateMedical()\n";
                litScript.Text += "</script>";
            }
        }
    }
    protected void btnCalculate_Click(object sender, EventArgs e)
    {
        NonMotorManager cm = new NonMotorManager();
        RegisterManager cmRegister = new RegisterManager();
        DataSet dsData;
        DataSet dsPremium;
        string strTmpCD = "";
        string strScript = "";

		TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        double dStartDate = 0;
        double dMinDate = 0;
        string strMinDate = "";

        if (DateTime.Now.Hour >= 12 && DateTime.Now.Minute >= 00)
        {
            dMinDate = Convert.ToDouble(cmDate.convertDateStringForDBTH(FormatStringApp.FormatDate(DateTime.Now.AddDays(1))).Replace("/",""));
            strMinDate = FormatStringApp.FormatDate(DateTime.Now.AddDays(1));
        }
        else
        {
            dMinDate = Convert.ToDouble(cmDate.convertDateStringForDBTH(FormatStringApp.FormatDate(DateTime.Now)).Replace("/",""));
            strMinDate = FormatStringApp.FormatDate(DateTime.Now);
        }
        dStartDate = Convert.ToDouble(cmDate.convertDateStringForDBTH(txtStartDate.Text).Replace("/",""));        
        if (dStartDate < dMinDate)
        {
			strScript = "<script>";
            strScript += " document.getElementById('divLoading').style.display = 'none';";
			strScript += "showMessagePage('วันที่เริ่มคุ้มครองต้องมีค่าตั้งแต่วันที่ " + strMinDate + "');";
			strScript += " </script>";
			ScriptManager.RegisterClientScriptBlock(Literal1, typeof(Literal), "s", strScript, false);
        }
        else
        {
			dsData = UIToDoc();
			dsPremium = cm.getPANewPremium(dsData);
			lblPremium.Text = FormatStringApp.Format2Digit(dsPremium.Tables[0].Rows[0]["sum_prmm"].ToString());
			lblInternetPremium.Text = FormatStringApp.Format2Digit(dsPremium.Tables[0].Rows[0]["total_prmm"].ToString());
			txtPremium.Text = dsPremium.Tables[0].Rows[0]["prmm"].ToString();
			txtStamp.Text = dsPremium.Tables[0].Rows[0]["stamp_prmm"].ToString();
			txtTax.Text = dsPremium.Tables[0].Rows[0]["tax_prmm"].ToString();
			txtDiscountRate.Text = dsPremium.Tables[0].Rows[0]["disc_rate"].ToString();
			txtDiscountAmt.Text = dsPremium.Tables[0].Rows[0]["disc_amt"].ToString();
			txtPremiumDiscount.Text = dsPremium.Tables[0].Rows[0]["net_prmm"].ToString();
			txtGrossPremiumDiscount.Text = dsPremium.Tables[0].Rows[0]["total_prmm"].ToString();
			txtDeathPrmm.Text = dsPremium.Tables[0].Rows[0]["death_prmm"].ToString();
			txtTtdPrmm.Text = dsPremium.Tables[0].Rows[0]["ttd_prmm"].ToString();
			txtPtdPrmm.Text = dsPremium.Tables[0].Rows[0]["ptd_prmm"].ToString();
			txtMedPrmm.Text = dsPremium.Tables[0].Rows[0]["med_prmm"].ToString();
			txtAddPrmm.Text = dsPremium.Tables[0].Rows[0]["add_prmm"].ToString();
			txtDiscPrmm.Text = dsPremium.Tables[0].Rows[0]["disc_prmm"].ToString();
			txtNetPrmm.Text = dsPremium.Tables[0].Rows[0]["net_prmm"].ToString();
			dsData = UIToDoc();


			litScript.Text += "<script>";
			litScript.Text += " document.getElementById('divPremium').style.display = 'inline';";
			litScript.Text += "</script>";
			strTmpCD = cmRegister.insertTmpPersonelAccident(dsData);
			txtTmpCD.Text = strTmpCD;
			dsData.Tables[0].Rows[0]["tm_tmp_cd"] = strTmpCD;
			if (strTmpCD == "")
			{
				//dsErrLabel = (DataSet)Session["dsLabelError"];
				//UcError1.showMessage(cmLabel.getLabel(dsErrLabel, "err000002", Session["Language"].ToString()));
			}
			ViewState.Add("KeyTmp", strTmpCD);
			Session.Add("DSRegister", dsData);

			strScript = "<script>";
			strScript += " document.getElementById('divPremium').style.display = 'block';";
			strScript += " document.getElementById('divLoading').style.display = 'none';";
			strScript += " </script>";
			ScriptManager.RegisterClientScriptBlock(Literal1, typeof(Literal), "s", strScript, false);
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        string strTmpCD = "";
        RegisterManager cmRegister = new RegisterManager();
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        double dStartDate = 0;
        double dMinDate = 0;
        string strMinDate = "";
        string strScript = "";

        if (DateTime.Now.Hour >= 12 && DateTime.Now.Minute >= 00)
        {
            dMinDate = Convert.ToDouble(cmDate.convertDateStringForDBTH(FormatStringApp.FormatDate(DateTime.Now.AddDays(1))).Replace("/",""));
            strMinDate = FormatStringApp.FormatDate(DateTime.Now.AddDays(1));
        }
        else
        {
            dMinDate = Convert.ToDouble(cmDate.convertDateStringForDBTH(FormatStringApp.FormatDate(DateTime.Now)).Replace("/",""));
            strMinDate = FormatStringApp.FormatDate(DateTime.Now);
        }
        dStartDate = Convert.ToDouble(cmDate.convertDateStringForDBTH(txtStartDate.Text).Replace("/",""));        
        if (dStartDate < dMinDate)
        {
			strScript = "<script>";
            strScript += " document.getElementById('divLoading').style.display = 'none';";
			strScript += "showMessagePage('วันที่เริ่มคุ้มครองต้องมีค่าตั้งแต่วันที่ " + strMinDate + "');";
			strScript += " </script>";
			ScriptManager.RegisterClientScriptBlock(Literal1, typeof(Literal), "s", strScript, false);
        }
        else
        {
			if (Session["DSRegister"] != null)
				ds = (DataSet)Session["DSRegister"];
			strTmpCD = cmRegister.insertTmpPersonelAccident(ds);
			ds.Tables[0].Rows[0]["tm_tmp_cd"] = strTmpCD;
			Session.Add("DSRegister", ds);
			if (strTmpCD != "")
				Response.Redirect("buyInsurePA_step3.aspx", true);
		}
    }
    private DataSet UIToDoc()
    {
        DataSet dsData = new DataSet();
        // insert tmpreg, tmpvolun, tmpmtveh, tmpcompul
        string[] ColumnName = { "tm_tmp_cd", "pm_major_cd", "pm_minor_cd",
                                    //register
                                    "rg_regis_no", "rg_member_cd", "rg_member_ref", "rg_main_class", "rg_pol_type", 
                                    "rg_effect_dt", "rg_expiry_dt", "rg_regis_dt", "rg_regis_time", "rg_regis_type", 
                                    "rg_ins_fname", "rg_ins_lname", "rg_ins_addr1", "rg_ins_addr2", "rg_ins_amphor",
                                    "rg_ins_changwat", "rg_ins_postcode", "rg_ins_tel", "rg_ins_email", "rg_sum_ins", 
                                    "rg_prmm", "rg_tax", "rg_stamp", "rg_pmt_cd", "rg_fleet_perc", 
                                    "rg_pay_type", "rg_payment_stat", "rg_ref_bank", "rg_approve", "rg_prmmgross",
                                    // register - new column
                                    "rg_ins_mobile", "rg_ins_idcard", "oth_distance", "oth_region", "oth_flagdeduct", 
                                    "oth_oldpolicy", "insc_id", "mott_id",
                                    //personal
                                    "pe_regis_no", "pe_eff_time", "pe_card_type", "pe_card_no", "pe_birth_dt", 
                                    "pe_occup", "pe_ben_fname", "pe_ben_lname", "pe_ben_addr1", "pe_ben_addr2", 
                                    "pe_ben_amphor", "pe_ben_changwat", "pe_ben_postcode", "pe_ben_tel", "pe_ben_email",
                                    "pe_relation", "flag_befaddress",
                                    // pa_cover
                                    "pa_regis_no", "pa_type", "pa_death_si", "pa_death_exc", "pa_death_prmm",
                                    "pa_ttd_si", "pa_ttd_week", "pa_ttd_exc", "pa_ttd_prmm", "pa_ptd_si",
                                    "pa_ptd_week", "pa_ptd_exc", "pa_ptd_prmm", "pa_med_si", "pa_med_exc",
                                    "pa_med_prmm", "pa_war", "pa_str", "pa_spt", "pa_mtr", "pa_air", 
                                    "pa_murder", "pa_add_prmm", "pa_disc_prmm", "pa_net_prmm",
                                    // senddoc
                                    "sed_regis_no", "sed_branch", "sed_name", "sed_ins_addr", "sed_amphor",
                                    "sed_changwat", "sed_postcode", "sed_tel", "sed_at"
                                    };
        string[] ColumnType = { "string", "string", "string",
                                  //
                                    "string", "string", "string", "string", "string", 
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    //
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string",                                    
                                    //
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string",
                                    //
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string",
                                    //
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string"};
        DataTable dt = new DataTable();
        dt = TDS.Utility.MasterUtil.AddColumnToDataTable(dt, ColumnType, ColumnName);
        dsData.Tables.Add(dt);
        dsData.Tables[0].Rows.Add(dsData.Tables[0].NewRow());
        dsData.Tables[0].Rows[0]["pm_major_cd"] = Request.QueryString["pm_major_cd"];
        dsData.Tables[0].Rows[0]["pm_minor_cd"] = Request.QueryString["pm_minor_cd"];
        // register
        dsData.Tables[0].Rows[0]["rg_main_class"] = "P";
        dsData.Tables[0].Rows[0]["rg_pol_type"] = "P";
        dsData.Tables[0].Rows[0]["rg_regis_dt"] = FormatStringApp.FormatDate(DateTime.Now);
        dsData.Tables[0].Rows[0]["rg_regis_time"] = FormatStringApp.FormatTime(DateTime.Now); 
        dsData.Tables[0].Rows[0]["rg_effect_dt"] = txtStartDate.Text;
        DateTime dte = new DateTime(Convert.ToInt32(txtStartDate.Text.Substring(6, 4)) - 543, Convert.ToInt32(txtStartDate.Text.Substring(3, 2)), Convert.ToInt32(txtStartDate.Text.Substring(0, 2)));
        dte = dte.AddYears(1);
        dsData.Tables[0].Rows[0]["rg_expiry_dt"] = FormatStringApp.FormatDate(dte);
        dsData.Tables[0].Rows[0]["rg_ins_fname"] = txtFName.Text;
        dsData.Tables[0].Rows[0]["rg_ins_lname"] = txtLName.Text;
        dsData.Tables[0].Rows[0]["rg_ins_addr1"] = txtAddress.Text;
        dsData.Tables[0].Rows[0]["rg_ins_addr2"] = txtSubDistrict.Text;
        dsData.Tables[0].Rows[0]["rg_ins_amphor"] = txtDistrict.Text;
        dsData.Tables[0].Rows[0]["rg_ins_changwat"] = ddlProvince.SelectedValue;
        dsData.Tables[0].Rows[0]["rg_ins_postcode"] = txtPostCode.Text;
        dsData.Tables[0].Rows[0]["rg_ins_tel"] = txtTelephone.Text;
        dsData.Tables[0].Rows[0]["rg_ins_mobile"] = txtMobile.Text;
        dsData.Tables[0].Rows[0]["rg_ins_email"] = txtEmail.Text;
        dsData.Tables[0].Rows[0]["rg_sum_ins"] = "0";
        dsData.Tables[0].Rows[0]["rg_prmm"] = txtPremiumDiscount.Text.Replace(",", "");
        dsData.Tables[0].Rows[0]["rg_tax"] = txtTax.Text.Replace(",", "");
        dsData.Tables[0].Rows[0]["rg_stamp"] = txtStamp.Text.Replace(",", "");
        dsData.Tables[0].Rows[0]["rg_prmmgross"] = txtGrossPremiumDiscount.Text.Replace(",", "");
        // personel        
        dsData.Tables[0].Rows[0]["pe_regis_no"] = "";
        dsData.Tables[0].Rows[0]["pe_eff_time"] = "";
        dsData.Tables[0].Rows[0]["pe_card_type"] = ddlCardType.SelectedValue;
        dsData.Tables[0].Rows[0]["pe_card_no"] = txtCardNo.Text;
        dsData.Tables[0].Rows[0]["pe_birth_dt"] = txtBirthDate.Text;
        dsData.Tables[0].Rows[0]["pe_occup"] = ddlOccupy.SelectedValue;
        dsData.Tables[0].Rows[0]["pe_ben_fname"] = txtBefFName.Text;
        dsData.Tables[0].Rows[0]["pe_ben_lname"] = txtBefLName.Text;
        if (rdoInsAddress.Checked == true)
        {
            dsData.Tables[0].Rows[0]["pe_ben_addr1"] = txtAddress.Text;
            dsData.Tables[0].Rows[0]["pe_ben_addr2"] = txtSubDistrict.Text;
            dsData.Tables[0].Rows[0]["pe_ben_amphor"] = txtDistrict.Text;
            dsData.Tables[0].Rows[0]["pe_ben_changwat"] = ddlProvince.SelectedValue;
            dsData.Tables[0].Rows[0]["pe_ben_postcode"] = txtPostCode.Text;
            //dsData.Tables[0].Rows[0]["pe_ben_tel"] = txtTelephone.Text;
            //dsData.Tables[0].Rows[0]["pe_ben_email"] = txtEmail.Text;            
        }
        else
        {
            dsData.Tables[0].Rows[0]["flag_befaddress"] = "";
            dsData.Tables[0].Rows[0]["pe_ben_addr1"] = txtBefAddress.Text;
            dsData.Tables[0].Rows[0]["pe_ben_addr2"] = txtBefSubDistrict.Text;
            dsData.Tables[0].Rows[0]["pe_ben_amphor"] = txtBefDistrict.Text;
            dsData.Tables[0].Rows[0]["pe_ben_changwat"] = ddlBefProvince.SelectedValue;
            dsData.Tables[0].Rows[0]["pe_ben_postcode"] = txtBefPostcode.Text;
        }
        dsData.Tables[0].Rows[0]["pe_ben_tel"] = txtBefTelephone.Text;
        dsData.Tables[0].Rows[0]["pe_ben_email"] = txtBefEmail.Text;
        dsData.Tables[0].Rows[0]["pe_relation"] = ddlBefRelationship.SelectedValue;
        // pa_cover
        if (txtPromotionMajor.Text == "" && txtPromotionMinor.Text == "")
        {
            // กำหนดความคุ้มครองเอง
            dsData.Tables[0].Rows[0]["pa_regis_no"] = "";
            dsData.Tables[0].Rows[0]["pa_type"] = (rdoPa1.Checked == true ? "PA1" : (rdoPa2.Checked == true ? "PA2" : ""));
            if (rdoPa1.Checked == true)
            {
                dsData.Tables[0].Rows[0]["pa_type"] = "PA1";
                dsData.Tables[0].Rows[0]["pa_death_si"] = ddlPa1.SelectedValue;
            }
            else if (rdoPa2.Checked == true)
            {
                dsData.Tables[0].Rows[0]["pa_type"] = "PA2";
                dsData.Tables[0].Rows[0]["pa_death_si"] = ddlPa2.SelectedValue;
            }
            dsData.Tables[0].Rows[0]["pa_death_exc"] = "0";
            dsData.Tables[0].Rows[0]["pa_death_prmm"] = txtDeathPrmm.Text;
            dsData.Tables[0].Rows[0]["pa_ttd_si"] = ddlTtdSI.SelectedValue;
            dsData.Tables[0].Rows[0]["pa_ttd_week"] = txtTtdWeek.Text;
            dsData.Tables[0].Rows[0]["pa_ttd_exc"] = "0";
            dsData.Tables[0].Rows[0]["pa_ttd_prmm"] = txtTtdPrmm.Text;            
            dsData.Tables[0].Rows[0]["pa_ptd_si"] = ddlPtdSI.SelectedValue;
            dsData.Tables[0].Rows[0]["pa_ptd_week"] = ddlPtdWeek.SelectedValue;
            dsData.Tables[0].Rows[0]["pa_ptd_exc"] = "0";
            dsData.Tables[0].Rows[0]["pa_ptd_prmm"] = txtPtdPrmm.Text;
            dsData.Tables[0].Rows[0]["pa_med_si"] = Request.Form[ddlMedicine.UniqueID];
            dsData.Tables[0].Rows[0]["pa_med_exc"] = "0";
            dsData.Tables[0].Rows[0]["pa_med_prmm"] = txtMedPrmm.Text;
            dsData.Tables[0].Rows[0]["pa_war"] = (chkWar.Checked ? "Y" : "N");
            dsData.Tables[0].Rows[0]["pa_str"] = (chkStr.Checked ? "Y" : "N");
            dsData.Tables[0].Rows[0]["pa_spt"] = (chkSpt.Checked ? "Y" : "N");
            dsData.Tables[0].Rows[0]["pa_mtr"] = (chkMtr.Checked ? "Y" : "N");
            dsData.Tables[0].Rows[0]["pa_air"] = (chkAir.Checked ? "Y" : "N");
            dsData.Tables[0].Rows[0]["pa_murder"] = (chkMurder.Checked ? "Y" : "N");
            dsData.Tables[0].Rows[0]["pa_add_prmm"] = txtAddPrmm.Text;
            dsData.Tables[0].Rows[0]["pa_disc_prmm"] = txtDiscPrmm.Text;
            dsData.Tables[0].Rows[0]["pa_net_prmm"] = txtNetPrmm.Text;
        }
        else
        {
            // ตาม pa_promotion
            dsData.Tables[0].Rows[0]["pa_regis_no"] = "";
            dsData.Tables[0].Rows[0]["pa_type"] = txtPAType.Text;
            if (txtPAType.Text == "PA1")
                dsData.Tables[0].Rows[0]["pa_death_si"] = txtPa1.Text;
            else if (txtPAType.Text == "PA2")
                dsData.Tables[0].Rows[0]["pa_death_si"] = txtPa2.Text;
            dsData.Tables[0].Rows[0]["pa_death_exc"] = "0";
            dsData.Tables[0].Rows[0]["pa_ttd_si"] = (txtTtd.Text == "" ? "0" : txtTtd.Text);
            dsData.Tables[0].Rows[0]["pa_ttd_week"] = txtTtdWeek.Text;
            dsData.Tables[0].Rows[0]["pa_ttd_exc"] = "0";
            dsData.Tables[0].Rows[0]["pa_ptd_si"] = (txtPtd.Text == "" ? "0" : txtPtd.Text);
            dsData.Tables[0].Rows[0]["pa_ptd_week"] = txtPtdWeek.Text;
            dsData.Tables[0].Rows[0]["pa_ptd_exc"] = "0";
            dsData.Tables[0].Rows[0]["pa_med_si"] = (txtMedicine.Text == "" ? "0" : txtMedicine.Text);
            dsData.Tables[0].Rows[0]["pa_med_exc"] = "0";
            dsData.Tables[0].Rows[0]["pa_war"] = (lblWar.Visible ? "Y" : "N");
            dsData.Tables[0].Rows[0]["pa_str"] = (lblStr.Visible ? "Y" : "N");
            dsData.Tables[0].Rows[0]["pa_spt"] = (lblSpt.Visible ? "Y" : "N");
            dsData.Tables[0].Rows[0]["pa_mtr"] = (lblMtr.Visible ? "Y" : "N");
            dsData.Tables[0].Rows[0]["pa_air"] = (lblAir.Visible ? "Y" : "N");
            dsData.Tables[0].Rows[0]["pa_murder"] = (lblMurder.Visible ? "Y" : "N");            
        }
        dsData.Tables[0].Rows[0]["pa_death_prmm"] = txtDeathPrmm.Text;
        dsData.Tables[0].Rows[0]["pa_ttd_prmm"] = txtTtdPrmm.Text;
        dsData.Tables[0].Rows[0]["pa_ptd_prmm"] = txtPtdPrmm.Text;
        dsData.Tables[0].Rows[0]["pa_med_prmm"] = txtMedPrmm.Text;
        dsData.Tables[0].Rows[0]["pa_add_prmm"] = txtAddPrmm.Text;
        dsData.Tables[0].Rows[0]["pa_disc_prmm"] = txtDiscPrmm.Text;
        dsData.Tables[0].Rows[0]["pa_net_prmm"] = txtNetPrmm.Text;

        // new column
        dsData.Tables[0].Rows[0]["oth_distance"] = "";
        dsData.Tables[0].Rows[0]["oth_region"] = "";
        dsData.Tables[0].Rows[0]["oth_flagdeduct"] = "";
        dsData.Tables[0].Rows[0]["oth_oldpolicy"] = "";
        dsData.Tables[0].Rows[0]["insc_id"] = "";
        dsData.Tables[0].Rows[0]["mott_id"] = "";

        return dsData;
    }
    public void docToUI(DataSet dsPromotion)
    {
        double dTmp;
        txtPromotionMajor.Text = Request.QueryString["pm_major_cd"];
        txtPromotionMinor.Text = Request.QueryString["pm_minor_cd"];
        #region PA1
        if (dsPromotion.Tables[0].Rows[0]["pm_pa1"].ToString() == "Y")
        {
            Label4.Visible = true;
            txtPa1.Visible = true;
            lblPa1.Visible = false;
            dTmp = Convert.ToDouble("0" + dsPromotion.Tables[0].Rows[0]["pm_pa1_si"].ToString());
            txtPa1.Text = dTmp.ToString("#,###,###,##0.00");
            txtPAType.Text = "PA1";
        }
        else
        {
            txtPa1.Visible = false;
            lblPa1.Visible = true;
        }
        #endregion
        #region PA2
        if (dsPromotion.Tables[0].Rows[0]["pm_pa2"].ToString() == "Y")
        {
            Label5.Visible = true;
            txtPa2.Visible = true;
            lblPa2.Visible = false;
            dTmp = Convert.ToDouble("0" + dsPromotion.Tables[0].Rows[0]["pm_pa2_si"].ToString());
            txtPa2.Text = dTmp.ToString("#,###,###,##0.00");
            txtPAType.Text = "PA2";
        }
        else
        {
            txtPa2.Visible = false;
            lblPa2.Visible = true;
        }
        #endregion
        #region TTD
        if (dsPromotion.Tables[0].Rows[0]["pm_ttd"].ToString() == "Y")
        {
            divTtd.Visible = true;
            txtTtd.Visible = true;
            lblTtd.Visible = false;
            dTmp = Convert.ToDouble("0" + dsPromotion.Tables[0].Rows[0]["pm_ttd_si"].ToString());
            txtTtd.Text = dTmp.ToString("#,###,###,##0.00");
            dTmp = Convert.ToDouble("0" + dsPromotion.Tables[0].Rows[0]["pm_ttd_week"].ToString());
            txtTtdWeek.Text = dTmp.ToString("0");
        }
        else
        {
            txtTtd.Visible = false;
            lblTtd.Visible = true;
            txtTtdWeek.Text = "0";
        }
        #endregion
        #region PTD
        if (dsPromotion.Tables[0].Rows[0]["pm_ptd"].ToString() == "Y")
        {
            divPtd.Visible = true;
            txtPtd.Visible = true;
            lblPtd.Visible = false;
            dTmp = Convert.ToDouble("0" + dsPromotion.Tables[0].Rows[0]["pm_ptd_si"].ToString());
            txtPtd.Text = dTmp.ToString("#,###,###,##0.00");
            //if (dsPromotion.Tables[0].Rows[0]["pm_ptd_si"].ToString() != "0.00")
            //{
            //    dTmp = Convert.ToDouble(dsPromotion.Tables[0].Rows[0]["pm_ptd_si"].ToString());
            //    ddlPtd.Items.Add(dTmp.ToString("#,###,###,##0.00"));
            //    ddlPtd.Items[0].Value = dTmp.ToString();
            //}
            //else
            //{
            //    ddlPtd.DataSource = getSumIns();
            //    ddlPtd.DataTextField = "Text";
            //    ddlPtd.DataValueField = "Code";
            //    ddlPtd.DataBind();
            //}
            dTmp = Convert.ToDouble("0" + dsPromotion.Tables[0].Rows[0]["pm_ptd_week"].ToString());
            txtPtdWeek.Text = dTmp.ToString("0");
            //if (dsPromotion.Tables[0].Rows[0]["pm_ptd_week"].ToString() != "0")
            //{
            //    ddlPtdWeek.Items.Add(dsPromotion.Tables[0].Rows[0]["pm_ptd_week"].ToString());
            //    ddlPtdWeek.Items[0].Value = dsPromotion.Tables[0].Rows[0]["pm_ptd_week"].ToString();
            //}
            //else
            //{
            //    ddlPtdWeek.DataSource = getWeek();
            //    ddlPtdWeek.DataTextField = "Text";
            //    ddlPtdWeek.DataValueField = "Code";
            //    ddlPtdWeek.DataBind();
            //}
        }
        else
        {
            //divPtd.Visible = false;
            txtPtd.Visible = false;
            lblPtd.Visible = true;
            txtPtdWeek.Text = "0";
        }
        #endregion
        #region MEDICINE
        if (dsPromotion.Tables[0].Rows[0]["pm_med"].ToString() == "Y")
        {
            //Label10.Visible = true;
            txtMedicine.Visible = true;
            lblMedicine.Visible = false;
            dTmp = Convert.ToDouble(dsPromotion.Tables[0].Rows[0]["pm_med_si"].ToString());
            txtMedicine.Text = dTmp.ToString("#,###,###,##0.00");
            //if (dsPromotion.Tables[0].Rows[0]["pm_med_si"].ToString() != "0.00")
            //{
            //    dTmp = Convert.ToDouble(dsPromotion.Tables[0].Rows[0]["pm_med_si"].ToString());
            //    ddlMedicine.Items.Add(dTmp.ToString("#,###,###,##0.00"));
            //    ddlMedicine.Items[0].Value = dTmp.ToString();
            //}
            //else
            //{
            //    setDdlMed();
            //}
        }
        else
        {
            //Label10.Visible = true;
            txtMedicine.Visible = false;
            lblMedicine.Visible = true;
        }
        #endregion
        if (dsPromotion.Tables[0].Rows[0]["pm_war"].ToString() == "Y")
            lblWar.Visible = true;
        else
            lblWar.Visible = false;
        if (dsPromotion.Tables[0].Rows[0]["pm_str"].ToString() == "Y")
            lblStr.Visible = true;
        else
            lblStr.Visible = false;
        if (dsPromotion.Tables[0].Rows[0]["pm_spt"].ToString() == "Y")
            lblSpt.Visible = true;
        else
            lblSpt.Visible = false;
        if (dsPromotion.Tables[0].Rows[0]["pm_mtr"].ToString() == "Y")
            lblMtr.Visible = true;
        else
            lblMtr.Visible = false;
        if (dsPromotion.Tables[0].Rows[0]["pm_air"].ToString() == "Y")
            lblAir.Visible = true;
        else
            lblAir.Visible = false;
        if (dsPromotion.Tables[0].Rows[0]["pm_murder"].ToString() == "Y")
            lblMurder.Visible = true;
        else
            lblMurder.Visible = false;
    }
    public void initialDropdownList()
    {
        DataSet ds;
        DataView dvTmp;
        OnlineCompulsaryManager cmOnline = new OnlineCompulsaryManager();
        OnlineVoluntaryManager cmOnlineVol = new OnlineVoluntaryManager();
        NonMotorManager cmNonMotor = new NonMotorManager();

        ds = cmNonMotor.getAllCardType();
        ddlCardType.DataSource = ds;
        ddlCardType.DataTextField = "ds_desc";
        ddlCardType.DataValueField = "ds_minor_cd";
        ddlCardType.DataBind();
        ddlCardType.Items.Insert(0, new ListItem("-- กรุณาระบุ --", ""));

        ds = cmNonMotor.getAllOccupation();
        ddlOccupy.DataSource = ds;
        ddlOccupy.DataTextField = "ds_desc";
        ddlOccupy.DataValueField = "ds_minor_cd";
        ddlOccupy.DataBind();
        ddlOccupy.Items.Insert(0, new ListItem("-- กรุณาระบุ --", ""));

        ds = cmOnlineVol.getAllProvince();
        ddlProvince.DataSource = ds;
        ddlProvince.DataTextField = "ds_desc";
        ddlProvince.DataValueField = "ds_minor_cd";
        ddlProvince.DataBind();
        ddlProvince.Items.Insert(0, new ListItem("-- กรุณาระบุ --", ""));

        ddlBefProvince.DataSource = ds;
        ddlBefProvince.DataTextField = "ds_desc";
        ddlBefProvince.DataValueField = "ds_minor_cd";
        ddlBefProvince.DataBind();
        ddlBefProvince.Items.Insert(0, new ListItem("-- กรุณาระบุ --", ""));

        ds = cmNonMotor.getAllRelation();
        ddlBefRelationship.DataSource = ds;
        ddlBefRelationship.DataTextField = "ds_desc";
        ddlBefRelationship.DataValueField = "ds_minor_cd";
        ddlBefRelationship.DataBind();
        ddlBefRelationship.Items.Insert(0, "-- ระบุความสัมพันธ์ --");
        ddlBefRelationship.Items[0].Value = "";

        ds = getSumIns();
        ddlPa1.DataSource = ds;
        ddlPa1.DataTextField = "text";
        ddlPa1.DataValueField = "code";
        ddlPa1.DataBind();

        ddlPa2.DataSource = ds;
        ddlPa2.DataTextField = "text";
        ddlPa2.DataValueField = "code";
        ddlPa2.DataBind();

        ds = getWeek();
        ddlTtdWeek.DataSource = ds;
        ddlTtdWeek.DataTextField = "text";
        ddlTtdWeek.DataValueField = "code";
        ddlTtdWeek.DataBind();

        ds = getSumIns();
        ddlTtdSI.DataSource = ds;
        ddlTtdSI.DataTextField = "text";
        ddlTtdSI.DataValueField = "code";
        ddlTtdSI.DataBind();

        ds = getWeek();
        ddlPtdWeek.DataSource = ds;
        ddlPtdWeek.DataTextField = "text";
        ddlPtdWeek.DataValueField = "code";
        ddlPtdWeek.DataBind();

        ds = getSumIns();
        ddlPtdSI.DataSource = ds;
        ddlPtdSI.DataTextField = "text";
        ddlPtdSI.DataValueField = "code";
        ddlPtdSI.DataBind();

    }
    private DataSet getSumIns()
    {
        DataSet dsSumIns;
        DataRow drSumIns;
        string[] columnNames = { "Text", "Code" };
        string[] columnTypes = { "string", "string" };
        dsSumIns = new DataSet();
        dsSumIns.Tables.Add("");
        TDS.Utility.MasterUtil.AddColumnToDataTable(dsSumIns.Tables[0], columnTypes, columnNames);
        for (double i = 100000; i <= 1000000; i += 100000)
        {
            drSumIns = dsSumIns.Tables[0].NewRow();
            drSumIns["Text"] = i.ToString("#,###,###,##0.00");
            drSumIns["Code"] = i.ToString();
            dsSumIns.Tables[0].Rows.Add(drSumIns);
        }
        for (double i = 2000000; i <= 5000000; i += 1000000)
        {
            drSumIns = dsSumIns.Tables[0].NewRow();
            drSumIns["Text"] = i.ToString("#,###,###,##0.00");
            drSumIns["Code"] = i.ToString();
            dsSumIns.Tables[0].Rows.Add(drSumIns);
        }
        return dsSumIns;
    }
    private DataSet getWeek()
    {
        DataSet dsWeek;
        DataRow drWeek;
        string[] columnNames = { "Text", "Code" };
        string[] columnTypes = { "string", "string" };
        dsWeek = new DataSet();
        dsWeek.Tables.Add("");
        TDS.Utility.MasterUtil.AddColumnToDataTable(dsWeek.Tables[0], columnTypes, columnNames);
        for (double i = 1; i <= 4; i++)
        {
            drWeek = dsWeek.Tables[0].NewRow();
            drWeek["Text"] = i.ToString();
            drWeek["Code"] = i.ToString();
            dsWeek.Tables[0].Rows.Add(drWeek);
        }
        return dsWeek;
    }

    [WebMethod]
    public static ArrayList GetCoverMedicine(string strSumIns)    
    {
        NonMotorManager cm = new NonMotorManager();
        DataSet dsPaMed;
        //string strSumIns = "0";
        double dMedSiFrom = 0;
        double dMedSiTo = 0;
        double dMedSiStep = 1;
        ArrayList list = new ArrayList();
        dsPaMed = cm.GetPAMedicineSi(strSumIns);
        if (dsPaMed.Tables.Count > 0 && dsPaMed.Tables[0].Rows.Count > 0)
        {
            dMedSiFrom = Convert.ToDouble(dsPaMed.Tables[0].Rows[0]["pam_med_si_from"].ToString());
            dMedSiTo = Convert.ToDouble(dsPaMed.Tables[0].Rows[0]["pam_med_si_to"].ToString());
            dMedSiStep = Convert.ToDouble(dsPaMed.Tables[0].Rows[0]["pam_med_si_step"].ToString());
        }
        for (double i = dMedSiFrom; i <= dMedSiTo; i += dMedSiStep)
        {            
            list.Add(new ListItem(i.ToString("#,###,###,##0.00"),i.ToString()));
        }
        return list;
    }
}
