<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="buyVoluntary_step1.aspx.cs" Inherits="buyVoluntary_step1" EnableEventValidation="false"%>
<%@ Register src="~/uc/ucTxtDate.ascx" tagname="ucTxtDate" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script>
        function btnTopName_Click(strCarName) {
            if (strCarName == "") {
                document.getElementById("<%=ddlCarName.ClientID %>").focus();
                document.getElementById("<%=ddlCarName.ClientID %>").value = "";
            }
            else {
                document.getElementById("<%=ddlCarName.ClientID %>").value = strCarName;
                PopulateCarMark();
            }
        }
        function PopulateCarMark() {
            var pageUrl = '<%=ResolveUrl("~/buyVoluntary_step1.aspx")%>';
            var strCarName;
            var strCarType;
            var ddlChild; 
           
            //$(ddlChild).attr("disabled", "disabled");
            strCarName = document.getElementById("<%=ddlCarName.ClientID %>").value;
            if (document.getElementById("<%=rdoCarK.ClientID %>").checked)
                strCarType = "1";
            else if (document.getElementById("<%=rdoCarP2.ClientID %>").checked)
                strCarType = "2";
            else if (document.getElementById("<%=rdoCarP3.ClientID %>").checked)
                strCarType = "3";
            else if (document.getElementById("<%=rdoCarV.ClientID %>").checked)
                strCarType = "4";
            ddlChild = document.getElementById("<%=ddlCarMark.ClientID %>");
            $.ajax({
                async: false,
                type: "POST",
                url: pageUrl + '/GetCarMakData',
                data: '{strCarName:"' + strCarName + '",strCarType:"' + strCarType + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(response) {                    
                    PopulateDropdownControl(response.d, $(ddlChild))
                },
                error: function(response) {
                    alert("error " + response.d);
                }
            });
        }        
        function PopulateDropdownControl(list, control) {
            //control.removeAttr("disabled");
            if (list.length > 0) {
                //control.empty().append('<option selected="selected" value="0">Please select</option>');
                control.empty();
                $.each(list, function() {
                    control.append($("<option></option>").val(this['Value']).html(this['Text']));
                });
                control.removeAttr("disabled");
            }
            else {
                control.empty().append('<option selected="selected" value=""> - ���͡ - <option>');
                control.attr("disabled", "disabled");
            }
            PopulateCarDetail();
        }
        function PopulateCarDetail() {
            var pageUrl = '<%=ResolveUrl("~/buyVoluntary_step1.aspx")%>';
            var strCarMak;
            var txtCC;
            var txtCarGroup;
            var txtOdMin;
            var txtOdMax;
            //$(ddlChild).attr("disabled", "disabled");
            strCarMark = document.getElementById("<%=ddlCarMark.ClientID %>").value;
            txtCC = document.getElementById("<%=txtCarCC.ClientID %>");
            txtCarGroup = document.getElementById("<%=txtCarGroup.ClientID %>");
            txtOdMin = document.getElementById("<%=txtOdMin.ClientID %>");
            txtOdMax = document.getElementById("<%=txtOdMax.ClientID %>");
            $.ajax({
                async: false,
                type: "POST",
                url: pageUrl + '/GetCarDetailData',
                data: '{strCarMark:"' + strCarMark + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(response) {
                    //alert(response);
                    PopulateCarDetailControl(response.d, $(txtCC), $(txtCarGroup), $(txtOdMin), $(txtOdMax))
                },
                error: function(xhr, status, error) {
                    var msg = JSON.parse(xhr.responseText);
                    alert(msg.Message);
                }
            });
        }
        function PopulateCarDetailControl(list, controlCC, controlCarGroup, controlOdMin, controlOdMax) {
            var obj = jQuery.parseJSON(list);
            if (obj != "") {
            $.each(obj, function() {
            controlCC.val(this.Table[0]['car_cc']);
            controlCarGroup.val(this.Table[0]['group_code']);
            controlOdMin.val(this.Table[0]['od_min']);
            controlOdMax.val(this.Table[0]['od_max']);
                });
            }
            else {
                controlCC.value = "";
                controlCarGroup.value = "";
                controlOdMin.value = "";
                controlOdMax.value = "";
            }
        }
        function rdoDriver_Click() {
            if (document.getElementById("<%=rdoDriverY.ClientID%>").checked == true)
            {
                $("#areaCarDriverAge").slideDown();
            }
            else
            {
                $("#areaCarDriverAge").slideUp();
            }

        }
        function poltype_click(polType) {
            document.getElementById('divPol1').style.display = 'none';
            document.getElementById('divPol2').style.display = 'none';
            document.getElementById('divPol3').style.display = 'none';
            document.getElementById("liPol1").className = "";
            document.getElementById("liPol2").className = "";
            document.getElementById("liPol3").className = "";
            if (polType == "1") {
                document.getElementById('divPol1').style.display = 'block';
                document.getElementById("liPol1").className = "current";
            }
            else if (polType == "2") {
            document.getElementById('divPol2').style.display = 'block';
            document.getElementById("liPol2").className = "current";
            }
            else if (polType == "3") {
            document.getElementById('divPol3').style.display = 'block';
            document.getElementById("liPol3").className = "current";
            }
        }
        function validateForm() {
            var isRet = true;
            var isRequire = true;
            var isLength = true;
            var isDate = true;
            var strMsg = "";
            var strMsgRequire = "";
            var strMsgLength = "";
            var strMsgDate = "";
            if (document.getElementById("<%=ddlCarName.ClientID %>").value == "") {
                strMsg += "    - ������ö\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=ddlCarMark.ClientID %>").value == "") {
                strMsg += "    - ���ö\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=ddlCarAge.ClientID %>").value == "") {
                strMsg += "    - �շ�訴����¹\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtCarCC.ClientID %>").value == "") {
                strMsg += "    - c.c. ����ͧ¹��\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=rdoDriverY.ClientID %>").enabled == true &&
                document.getElementById("<%=txtDriver1BrithDate.txtID %>").value == "__/__/____") {
                strMsg += "    - �ѹ�Դ ���Ѻ��� �����1\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtRegName.ClientID %>").value == "") {
                strMsg += "    - ����\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtRegSur.ClientID %>").value == "") {
                strMsg += "    - ʡ��\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=ddlRegAge.ClientID %>").value == "") {
                strMsg += "    - ����\n";
                isRet = false;
                isRequire = false;
            }
            if (document.getElementById("<%=txtRegMobile.ClientID %>").value == "") {
                strMsg += "    - �������Ѿ����Ͷ��\n";
                isRet = false;
                isRequire = false;
            }
            
            
            
            if (isRequire == false) {
                strMsg = "!��س��к�\n" + strMsg;
            }

            if (isRet == false) {
                //alert(strMsg + strMsgLength + strMsgDate);
                showMessagePage(strMsg + strMsgLength + strMsgDate);
            }
            else {
                document.getElementById("divLoading").style.display = "block";                
                document.getElementById('tabOvl-onlineInsureCar').style.display = 'none';                
                document.getElementById('divPol1').style.display = 'none';
                document.getElementById('divPol2').style.display = 'none';
                document.getElementById('divPol3').style.display = 'none';                
            }
            return isRet;
        }
        function lnkCover_click(lnk) {
            var link = lnk.getAttribute("url");            
            document.getElementById("coverIframe").setAttribute("src", link);
            //alert("url : " + link);
            $("[id*=dialog]").dialog({
                  autoOpen: false,
                  show: "fade",
                  hide: "fade",
                  modal: true,
                  //open: function (ev, ui) {
                  //    $('#myIframe').src = 'http://www.google.com';
                  //},
                  height: '500',
                  width: '600',
                  resizable: true
                  //title: 'Title Topic'
              });
//            $("#dialog1").dialog();

              $("#dialog1").dialog("open");
          }
          function lnkVoucher_click(lnk) {
              var link = lnk.getAttribute("url");
              document.getElementById("voucherIframe").setAttribute("src", link);
              //alert("url : " + link);
              $("[id*=dialog]").dialog({
                  autoOpen: false,
                  show: "fade",
                  hide: "fade",
                  modal: true,
                  //open: function (ev, ui) {
                  //    $('#myIframe').src = 'http://www.google.com';
                  //},
                  height: '500',
                  width: '600',
                  resizable: true
                  //title: 'Title Topic'
              });
              //            $("#dialog1").dialog();

              $("#dialog2").dialog("open");
          }
          function imgBuy_click(imgBuy) {
          
              window.location = imgBuy.getAttribute("url");
          }
          function showMessagePage(strMsg) {
              document.getElementById("MsgIframe").setAttribute("src", "ShowMessageBox.aspx?msg=" + strMsg);
              //alert("url : " + link);
              $("[id*=dialog]").dialog({
                  autoOpen: false,
                  show: "fade",
                  hide: "fade",
                  modal: true,
                  //open: function (ev, ui) {
                  //    $('#myIframe').src = 'http://www.google.com';
                  //},
                  //                  height: '200',
                    maxHeight : '400',
                    width: '400',

                    resizable: true
                  //title: 'Title Topic'
              });
              //            $("#dialog1").dialog();

              $("#dialogMsg").dialog("open");
          }
          function closeMessagePage() {
              $('#dialogMsg').dialog('close');
              return false;

          }
          function disabledDiscount() {
              var iRegisYear = 0;
              var iCurrentYear = new Date().getFullYear() + 543;
              document.getElementById("<%=ddlDiscount.ClientID %>").disabled = false;              
              if (document.getElementById("<%=ddlCarAge.ClientID %>").value != "") {
                  iRegisYear = parseFloat(document.getElementById("<%=ddlCarAge.ClientID %>").value);                  
                  if ((iCurrentYear - iRegisYear) >= 8)
                      document.getElementById("<%=ddlDiscount.ClientID %>").disabled = true;
              }
              //alert(new Date().getFullYear());
          }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div id="page-online-insCar"> 
        <h1 class="subject1">���ͻ�Сѹ-ö¹���Ҥ��Ѥ��</h1>
        
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="True">
        </asp:ScriptManager>
        <div class="stepOnline">
            <img src="images/app-step-1.jpg" />
        </div>
        <h2 class="subject-detail">�ô��͡��������´�������͡Ẻ����������ͧ</h2> 
        <div style="height :35px; ">
            <a class="btn-carbrand"   onclick="btnTopName_Click('TO')" >TOYOTA</a>
            <a class="btn-carbrand"     onclick="btnTopName_Click('HO')" >HONDA</a>
            <a class="btn-carbrand"    onclick="btnTopName_Click('NI')" >NISSAN</a>
            <a class="btn-carbrand"     onclick="btnTopName_Click('MA')" >MAZDA</a>
            <a class="btn-carbrand"    onclick="btnTopName_Click('IS')" >ISUZU</a>
            <a class="btn-carbrand btn-iconmore2"  onclick="btnTopName_Click('')" >����<span></span></a>
        </div>
        <div id="result" style="color: red;"></div>
        <table class="tb-online-insCar">           
            <tr>
                <td  width="130px"  class="label"  >��Դö :</td>
                <td>
                    <asp:RadioButton ID="rdoCarK" name="caruse" runat="server" value="1" GroupName="caruse" onclick="PopulateCarMark();" Checked/>��  
                    <asp:RadioButton ID="rdoCarP2" name="caruse" runat="server" value="2" GroupName="caruse" onclick="PopulateCarMark();" />��к���ǹ�ؤ��
                    <asp:RadioButton ID="rdoCarP3" name="caruse" runat="server" value="3" GroupName="caruse" onclick="PopulateCarMark();" />��кк�÷ء 
                    <asp:RadioButton ID="rdoCarV" name="caruse" runat="server" value="4" GroupName="caruse" onclick="PopulateCarMark();" />�����ǹ�ؤ�� 
                    <div style="display:none">
                        <asp:TextBox ID="txtCarCode" runat="server"></asp:TextBox>
                        <asp:TextBox ID="txtCarType" runat="server"></asp:TextBox>
                    </div> 
                </td>              
            </tr>            
        </table>       
        <table width="630px">
            <tr>
                <td>
                    <table class="tb-online-insCar">                                               
                        <tr>
                            <td class="label"   width="130px"  >������ö : </td>
                            <td> 
                                <asp:DropDownList ID="ddlCarName" runat="server">
                                    <asp:ListItem Text="-- ��س��к� --" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <span class="star">*</span></td>
                        </tr>
                        <tr>
                            <td class="label">���ö : </td>
                            <td>
                                <asp:DropDownList ID="ddlCarMark" runat="server">
                                    <asp:ListItem Text="-- ��س��к� --" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <span class="star">*</span>
                                <div style="display:none">
                                    <asp:TextBox ID="txtCarGroup" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="txtOdMin" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="txtOdMax" runat="server"></asp:TextBox>
                                </div>                         
                            </td>
                        </tr>
                        <tr>
                            <td class="label">�շ�訴����¹ :</td>
                            <td>
                                <asp:DropDownList ID="ddlCarAge" runat="server" onchange="disabledDiscount()">
                                    <asp:ListItem Text="-- ��س��к� --" Value=""></asp:ListItem> 
                                </asp:DropDownList> 
                                <span class="star">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">c.c. ����ͧ¹�� : </td>
                            <td>                         
                                <asp:TextBox ID="txtCarCC" runat="server" MaxLength="4" CssClass="TEXTBOXMONEY" onkeydown="return isKeyInteger();" onbeforedeactivate="return isInteger(this,'c.c.����ͧ¹��')"></asp:TextBox>
                                <span class="star">*</span>
                            </td>
                        </tr>                        
                        <tr>
                            <td class="label">���Ѻ��� : </td>
                            <td>                                  
                                <asp:RadioButton ID="rdoDriverN" runat="server" value="N" CssClass="tb-radioList" GroupName="driver" Checked onclick="rdoDriver_Click();"/>����кؼ��Ѻ���
                                <asp:RadioButton ID="rdoDriverY" runat="server" value="Y" CssClass="tb-radioList" GroupName="driver" onclick="rdoDriver_Click();"/>�кؼ��Ѻ���
                            </td>
                        </tr>
                        <!-- Start id="areaDriBirth" -->
                        <tr  id="areaCarDriverAge" style="display: none;" >
                            <td colspan="2">
                                <table class="tb-online-insCar" >
                                    <tr>  
                                        <td class="label" width="130px" >�ѹ�Դ ���Ѻ��� �����1 : </td>
                                        <td>                                     
                                            <uc1:ucTxtDate ID="txtDriver1BrithDate" runat="server"/>
                                            <span class="star">*</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label">�ѹ�Դ ���Ѻ��� �����2 :  </td>
                                        <td> 
                                            <uc1:ucTxtDate ID="txtDriver2BrithDate" runat="server" />
                                        </td>
                                    </tr> 
                                </table>
                            </td>
                        </tr>
                        <!-- End id="areaDriBirth" -->
                        <tr>
                            <td class="label">��ǹŴ����ѵԴ�  :</td>
                            <td>
                                <asp:DropDownList ID="ddlDiscount" runat="server">
                                     <asp:ListItem Text="-- ��س��к� --" Value="0"></asp:ListItem>                                    
                                     <asp:ListItem Text="20%" Value="20"></asp:ListItem>
                                     <asp:ListItem Text="30%" Value="30"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">���� �ú. : </td>
                            <td>
                                <asp:RadioButton ID="rdoCompulY" runat="server" value="Y" CssClass="tb-radioList" GroupName="compul" Checked/>����
                                <asp:RadioButton ID="rdoCompulN" runat="server" value="N" CssClass="tb-radioList" GroupName="compul"/>������                                
                            </td>
                        </tr>
                        <tr>
                            <td class="label">�Ѻö�ѹ�С���������  :</td>
                            <td>
                                <asp:DropDownList ID="ddlDriveLength" runat="server">
                                     <asp:ListItem Text="-- ��س��к� --" Value="0"></asp:ListItem>                                    
                                </asp:DropDownList>
                               </td>
                        </tr>
                        <tr>
                            <td class="label">�á����ö� : </td>
                            <td>
                                <asp:RadioButton ID="rdoRegionDrive1" runat="server" value="1" CssClass="tb-radioList" GroupName="regiondrive" Checked/>��ا෾�
                                <asp:RadioButton ID="rdoRegionDrive2" runat="server" value="2" CssClass="tb-radioList" GroupName="regiondrive"/>��ҧ�ѧ��Ѵ                                
                            </td>
                        </tr>                        
                        <tr>
                            <td></td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <table class="tb-online-insCar">
                        <tr>
                            <td class="label" width="150px">�յ��ҧ�������� : </td>
                            <td>
                                <asp:RadioButtonList ID="rdoRegInsHave" runat="server" RepeatDirection="Horizontal" CssClass="tb-radioList">
                                    <asp:ListItem>��</asp:ListItem>
                                    <asp:ListItem Selected="True">�����</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">����ѷ��Сѹ������    : </td>
                            <td>
                                <asp:DropDownList ID="ddlOldInsurance" runat="server" >
                                    <asp:ListItem Text="-- ��س��к� --" Value=""></asp:ListItem>                                    
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">��������Сѹ������ : </td>
                            <td>
                                <asp:DropDownList ID="ddlOldPolType" runat="server">
                                    <asp:ListItem Text="-- ��س��к� --" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                 </td>
                        </tr>

                        <tr>
                            <td class="label">���� : </td>
                            <td>
                                <asp:TextBox ID="txtRegName" runat="server"></asp:TextBox>
                                <span class="star">*</span></td>
                        </tr>

                        <tr>
                            <td class="label">ʡ�� : </td>
                            <td>
                                <asp:TextBox ID="txtRegSur" runat="server"></asp:TextBox>
                                <span class="star">*</span></td>
                        </tr>
                        <tr>
                            <td class="label">�� : </td>
                            <td>
                                <asp:RadioButtonList ID="rdoRegSex" runat="server" RepeatDirection="Horizontal" CssClass="tb-radioList">
                                    <asp:ListItem Value="0" >˭ԧ</asp:ListItem>
                                    <asp:ListItem Value="1" Selected="True" >���</asp:ListItem>
                                </asp:RadioButtonList>
                               </td>
                        </tr>
                        <tr>
                            <td class="label">���� : </td>
                            <td>
                                <asp:DropDownList ID="ddlRegAge" runat="server">
                                    <asp:ListItem Text="-- ��س��к� --" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <span class="star">*</span></td>
                        </tr>                        
                        <tr>
                            <td class="label">�������Ѿ����Ͷ�� : </td>
                            <td>
                                <asp:TextBox ID="txtRegMobile" runat="server"></asp:TextBox>
                                <span class="star">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">������ : </td>
                            <td>
                                <asp:TextBox ID="txtRegEmail" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" valign="top">
                    <div style="display:none">
                    <div style="margin: 0px 10px 0px 20px; float: left; height: 50px;">
                        <asp:CheckBox ID="CheckBox2" runat="server" />
                    </div>
                    ��Ҿ����Թ���������ѷ�������Ţ���Ѿ�� ������� ��� ������ ����������ҧ��
                    <br />
                    㹡���駼š����Ѥ� ������� ��������  ���͡���ʹ͢�¼�Ե�ѳ��ͧ����ѷ��Ф����
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div style="margin: 0px auto; width: 250px; margin-top: 15px;">                        
                        <asp:Button ID="btnSearch" runat="server" Text="�ӹǳ����" class="btn-default" Style="margin-right: 10px;" OnClick="btnSearch_Click" OnClientClick="return validateForm();"/>
                        <input id="btnClear" type="reset" class="btn-default " value="����������" />
                    </div>
                </td>
                <a name="grd"></a>
            </tr>
        </table>
        <br />
        <div id="divLoading" style="text-align:center;display:none;">
            <img src="./images/loading.gif" />
        </div>
        <div id="insure-rate" >             
            <!--  Start Overlapping Tabs   -->            
            <div id="tabOvl-onlineInsureCar" style="display:none">            
            <ul class="obtabs">
            <li id="liPol1" class="current"><span><a href="#grd" onclick="poltype_click('1');">��� 1</a></span></li>
            <li id="liPol2"  ><span><a href="#grd" rel="pol2" onclick="poltype_click('2');">��� 2 </a></span></li>
            <li id="liPol3"><span><a href="#grd" rel="pol3" onclick="poltype_click('3');">��� 3</a></span></li>
            </ul>
            </div>
            <!--  End Overlapping Tabs   -->             
              <style>
                /*===== Control  GridView  Width (support IE9 & Over) =====*/
                /*
                .tb-prd-result th {height:20px;   color:#fff ;   padding:5px 5px 7px 5px; font-size:13px;  vertical-align:top ;  }
                
                .tb-prd-result th:nth-child(3) {background-color:#ffa200; }
                .tb-prd-result th:nth-child(4)  {background-color:#ffa200; width: 5%;  }
                .tb-prd-result th:nth-child(5) { background-color:#3942f2 ; } 
                .tb-prd-result th:nth-child(6) { background-color:#3942f2 ; width: 5%; }

                .tb-prd-result  td {padding:2px 5px;height:20px; line-height: 18px;} 
                .tb-prd-result td:nth-child(1) {  width: 20%;  } 
                .tb-prd-result td:nth-child(3) {background-color:#fffeb3;  }
                .tb-prd-result  td:nth-child(4)  {background-color:#fffeb3;   }
                .tb-prd-result  td:nth-child(5) { background-color:#e4f2fd ; } 
                .tb-prd-result td:nth-child(6) { background-color:#e4f2fd ;  } 
                .tb-prd-result td:nth-child(8) {   font-size: 12px; } 
                .tb-prd-result td:nth-child(9) {  width: 5%; font-size: 12px;  } 
                */
            </style>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                <div id="selectData">                
                <div id="divPol1">  
                    <%--  <h3>GridView1</h3>--%>
                    <asp:GridView ID="GridView1" runat="server"  class="tb-prd-result"                    
                    DataKeyNames="campaign"  AutoGenerateColumns="false" 
                        onrowdatabound="GridView1_RowDataBound">
                        <Columns>
                            <asp:TemplateField   HeaderText="��Ե�ѳ��"   HeaderStyle-CssClass="col1"   > 
                   
                                <ItemTemplate>
                                    <asp:Label runat="server" id="lblCampaign" Text="<%#(Eval(ColumnName[0]))%>" ></asp:Label> 
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField   HeaderText="�ع��Сѹ*"   HeaderStyle-CssClass="col2" >
                                <ItemTemplate>
                                    <asp:Label runat="server"   id="lblCover" Text="<%#FormatStringApp.FormatInt(Eval(ColumnName[1]))%>" ></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField    HeaderText="���»�Сѹ<br />�����ٹ��"   HeaderStyle-CssClass="col3"   >
                                        <ItemStyle CssClass="bgSer" />     
                                <ItemTemplate> 
                                <asp:Label runat="server"   id="lblPremium1" Text="<%#showPremium(FormatStringApp.Format2Digit(Eval(ColumnName[2])))%>" ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField    >
                            <asp:TemplateField   HeaderText="����<br />����"  HeaderStyle-CssClass="col4"  >
                                  <ItemStyle CssClass="bgSer" />    
                                <ItemTemplate>
                                    <asp:HyperLink id="hplBuy1"  runat="server"  class="btn-default  btn-iconOrder" Visible="<%#isShowBuyIcon(Eval(ColumnName[2]))%>" url='<%#showBuyIcon(Eval(ColumnName[3]),"S") %>'  onclick="imgBuy_click(this)">����<span></span></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField   HeaderText="���»�Сѹ<br />�������"  HeaderStyle-CssClass="col5"  >
                                  <ItemStyle CssClass="bgGar" />    
                                <ItemTemplate>
                                    <asp:Label runat="server"    id="lblPremium2" Text="<%#showPremium(FormatStringApp.Format2Digit(Eval(ColumnName[4])))%>"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField   HeaderText="����<br />����"   HeaderStyle-CssClass="col6" >
                            <ItemStyle CssClass="bgGar" />     
                                <ItemTemplate>
                                    <asp:HyperLink id="hplBuy2"  runat="server"  class="btn-default  btn-iconOrder" Visible="<%#isShowBuyIcon(Eval(ColumnName[4]))%>" url='<%#showBuyIcon(Eval(ColumnName[3]),"G") %>' onclick="imgBuy_click(this)">����<span></span></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField   >
                            <asp:TemplateField   HeaderText="����<br />������ͧ"  HeaderStyle-CssClass="col7"  >
                                <ItemTemplate>
                                    <asp:HyperLink runat="server" id="hplDetail" Text="���<br>�����´" CssClass="openerCover" style="cursor:pointer;cursor:hand;" url="<%#showCoverDetail(Eval(ColumnName[5]),Eval(ColumnName[6]),Eval(ColumnName[7]),Eval(ColumnName[8]),Eval(ColumnName[1]),Eval(ColumnName[11])) %>" onclick="lnkCover_click(this);"></asp:HyperLink> 
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField   HeaderText="����<br />�˵�"   HeaderStyle-CssClass="col8" >
                                <ItemTemplate>
                                    <asp:HyperLink runat="server" id="hplVocher" Text="Gift<br />Voucher" CssClass="openerCover" style="cursor:pointer;cursor:hand"  url="<%#showVoucherDetail(Eval(ColumnName[10]),Eval(ColumnName[9])) %>" onclick="lnkVoucher_click(this);"></asp:HyperLink> 
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <div id="divPol2">  
                    <%--  <h3>GridView1</h3>--%>
                    <asp:GridView ID="GridView2" runat="server"  class="tb-prd-result"                    
                    DataKeyNames="campaign"  AutoGenerateColumns="false"   onrowdatabound="GridView2_RowDataBound" >
                        <Columns>
                            <asp:TemplateField   HeaderText="��Ե�ѳ��"    HeaderStyle-CssClass="col1"  > 
                                <ItemTemplate>
                                    <asp:Label runat="server" id="lblCampaign" Text="<%#(Eval(ColumnName[0]))%>" ></asp:Label> 
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField   HeaderText="�ع��Сѹ*"   HeaderStyle-CssClass="col2"  >
                                <ItemTemplate>
                                    <asp:Label runat="server"   id="lblCover" Text="<%#FormatStringApp.FormatInt(Eval(ColumnName[1]))%>" ></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField    HeaderText="���»�Сѹ<br />�����ٹ��"   HeaderStyle-CssClass="col3" >
                
                                <ItemStyle CssClass="bgSer" />     
                                <ItemTemplate> 
                                <asp:Label runat="server"   id="lblPremium1" Text="<%#showPremium(FormatStringApp.Format2Digit(Eval(ColumnName[2])))%>" ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField    >
                            <asp:TemplateField   HeaderText="����<br />����"   HeaderStyle-CssClass="col4" >
                           <ItemStyle CssClass="bgSer" />      
                                <ItemTemplate>
                                    <asp:HyperLink id="hplBuy1"  runat="server"  class="btn-default  btn-iconOrder" Visible="<%#isShowBuyIcon(Eval(ColumnName[2]))%>" url='<%#showBuyIcon(Eval(ColumnName[3]),"S") %>'  onclick="imgBuy_click(this)">����<span></span></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField   HeaderText="���»�Сѹ<br />�������"    HeaderStyle-CssClass="col5" >
                               <ItemStyle CssClass="bgGar" />     
                                <ItemTemplate>
                                    <asp:Label runat="server"    id="lblPremium2" Text="<%#showPremium(FormatStringApp.Format2Digit(Eval(ColumnName[4])))%>"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField   HeaderText="����<br />����"   HeaderStyle-CssClass="col6"  >
                             <ItemStyle CssClass="bgGar" />      
                                <ItemTemplate>
                                    <asp:HyperLink id="hplBuy2"  runat="server"  class="btn-default  btn-iconOrder" Visible="<%#isShowBuyIcon(Eval(ColumnName[4]))%>" url='<%#showBuyIcon(Eval(ColumnName[3]),"G") %>' onclick="imgBuy_click(this)">����<span></span></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField   >
                            <asp:TemplateField   HeaderText="����<br />������ͧ"   HeaderStyle-CssClass="col7" >
                                <ItemTemplate>
                                    <asp:HyperLink runat="server" id="hplDetail" Text="���<br>�����´" CssClass="openerCover" style="cursor:pointer;cursor:hand" url="<%#showCoverDetail(Eval(ColumnName[5]),Eval(ColumnName[6]),Eval(ColumnName[7]),Eval(ColumnName[8]),Eval(ColumnName[1]),Eval(ColumnName[11])) %>" onclick="lnkCover_click(this);"></asp:HyperLink> 
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField   HeaderText="����<br />�˵�"   HeaderStyle-CssClass="col8" >
                                <ItemTemplate>
                                    <asp:HyperLink runat="server" id="hplVocher" Text="Gift<br />Voucher" CssClass="openerCover" style="cursor:pointer;cursor:hand"  url="<%#showVoucherDetail(Eval(ColumnName[10]),Eval(ColumnName[9])) %>" onclick="lnkVoucher_click(this);"></asp:HyperLink> 
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <div id="divPol3">  
                    <%--  <h3>GridView1</h3>--%>
                    <asp:GridView ID="GridView3" runat="server"  class="tb-prd-result"                    
                    DataKeyNames="campaign"  AutoGenerateColumns="false"  onrowdatabound="GridView3_RowDataBound"  >
                        <Columns>
                            <asp:TemplateField   HeaderText="��Ե�ѳ��"    HeaderStyle-CssClass="col1"  >  
                                <ItemTemplate>
                                    <asp:Label runat="server" id="lblCampaign" Text="<%#(Eval(ColumnName[0]))%>" ></asp:Label> 
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField   HeaderText="�ع��Сѹ*"   HeaderStyle-CssClass="col2" >
                                <ItemTemplate>
                                    <asp:Label runat="server"   id="lblCover" Text="<%#FormatStringApp.FormatInt(Eval(ColumnName[1]))%>" ></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField    HeaderText="���»�Сѹ<br />�����ٹ��"     HeaderStyle-CssClass="col3"  >
            <ItemStyle CssClass="bgSer" />     
                                <ItemTemplate> 
                                <asp:Label runat="server"   id="lblPremium1" Text="<%#showPremium(FormatStringApp.Format2Digit(Eval(ColumnName[2])))%>" ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField    >
                            <asp:TemplateField   HeaderText="����<br />����"   HeaderStyle-CssClass="col4" >
                                  <ItemStyle CssClass="bgSer" />      
                                <ItemTemplate>
                                    <asp:HyperLink id="hplBuy1"  runat="server"  class="btn-default  btn-iconOrder" Visible="<%#isShowBuyIcon(Eval(ColumnName[2]))%>" url='<%#showBuyIcon(Eval(ColumnName[3]),"S") %>'  onclick="imgBuy_click(this)">����<span></span></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField   HeaderText="���»�Сѹ<br />�������"   HeaderStyle-CssClass="col5" >
                                   <ItemStyle CssClass="bgGar" />    
                                <ItemTemplate>
                                    <asp:Label runat="server"    id="lblPremium2" Text="<%#showPremium(FormatStringApp.Format2Digit(Eval(ColumnName[4])))%>"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField   HeaderText="����<br />����"   HeaderStyle-CssClass="col6" >
                                  <ItemStyle CssClass="bgGar" />     
                                <ItemTemplate>
                                    <asp:HyperLink id="hplBuy2"  runat="server"  class="btn-default  btn-iconOrder" Visible="<%#isShowBuyIcon(Eval(ColumnName[4]))%>" url='<%#showBuyIcon(Eval(ColumnName[3]),"G") %>' onclick="imgBuy_click(this)">����<span></span></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField   >
                            <asp:TemplateField   HeaderText="����<br />������ͧ"   HeaderStyle-CssClass="col7" >
                                <ItemTemplate>
                                    <asp:HyperLink runat="server" id="hplDetail" Text="���<br>�����´" CssClass="openerCover" style="cursor:pointer;cursor:hand" url="<%#showCoverDetail(Eval(ColumnName[5]),Eval(ColumnName[6]),Eval(ColumnName[7]),Eval(ColumnName[8]),Eval(ColumnName[1]),Eval(ColumnName[11])) %>" onclick="lnkCover_click(this);"></asp:HyperLink> 
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField   HeaderText="����<br />�˵�"   HeaderStyle-CssClass="col8"  >
                                <ItemTemplate>
                                    <asp:HyperLink runat="server" id="hplVocher" Text="Gift<br />Voucher" CssClass="openerCover" style="cursor:pointer;cursor:hand"  url="<%#showVoucherDetail(Eval(ColumnName[10]),Eval(ColumnName[9])) %>" onclick="lnkVoucher_click(this);"></asp:HyperLink> 
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                </div><!-- id="selectData" -->                
                <div style="display:none">
                    <asp:TextBox ID="txtTmpCD" runat="server"></asp:TextBox>
                    <asp:TextBox ID="txtTmpCampaign" runat="server"></asp:TextBox>
                </div>
                <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>            
            <div id="divRemark" style="display:none">* �ع��Сѹ�ӹǳ�ҡ 80% �ͧ�Ҥ�ö</div>                                 
            <div id="dialog1" style="display:none" title="��������´ ����������ͧ"   >
                <iframe id="coverIframe"   frameborder="0" width="570" height="440" scrolling="no"></iframe>
            </div>
            <div id="dialog2" style="display:none" title="��������´ Voucher"   >
                <iframe id="voucherIframe"   frameborder="0" width="570" height="440" scrolling="no"></iframe>
            </div>
            <div id="dialogMsg" style="display:none" title="��õ�Ǩ�ͺ������"   >
                <iframe id="MsgIframe"   frameborder="0" style="max-height:360" width="370" scrolling="auto"></iframe>
            </div>
        </div>
        
        <!-- End id="insure-rate"-->
    </div>
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
    <!-- End <div class="context page-app"> -->
</asp:Content>

