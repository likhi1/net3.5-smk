﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="investInfo_03.aspx.cs" Inherits="investInfo_03" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
<div class="subject1">สัดส่วนผู้ถือหุ้นรายใหญ่</div> 

<table  class="tb-ver-center">
                  <tr  >
				    <th    >รายชื่อผู้ถือหุ้นรายใหญ่</th>
                    <th width="20%"  >จำนวนหุ้นที่ถือ</th>
					<th width="10%"  >%</th>
				  </tr>
         
                  <tr >
				    <td class="align-left" >ตระกูลดุษฎีสุรพจน์</td>
                    <td class="align-right"  >12,514,449</td>
					<td class="align-right"  >62.57</td>
  </tr>
                  <tr >
                    <td class="align-left" >ROYAL &amp; SUNALLIANCE GROUP</td>
                    <td class="align-right"  >3,999,999</td>
					<td class="align-right"  >20.00</td>
  </tr>
                  
                  <tr >
                    <td class="align-left" >บริษัท ไทยเอ็นวีดีอาร์ จำกัด</td>
                    <td class="align-right"  >426,333</td>
					<td class="align-right"  >2.13</td>
  </tr>
                  <tr >
                    <td class="align-left" >ตระกูลรอดลอยทุกข์</td>
                    <td class="align-right"  >380,152</td>
					<td class="align-right"  >1.90</td>
  </tr>
				  <tr >
                    <td class="align-left" >ตระกูลอัษฎาธร</td>
                    <td class="align-right"  >264,388</td>
					<td class="align-right"  >1.32</td>
  </tr>
				  <tr >
                    <td class="align-left" >CREDIT SUISSE SECURITIES (EUROPE) LIMITED </td>
                    <td class="align-right"  >224,777</td>
					<td class="align-right"  >1.12</td>
  </tr>
                  <tr >
                    <td class="align-left" >MR. YUE  KWOK-LEUNG</td>
                    <td class="align-right"  >172,900</td>
					<td class="align-right"  >0.86</td>
  </tr>
                  <tr >
                    <td class="align-left" >ตระกูลเริงพิทยา</td>
                    <td class="align-right"  >154,033</td>
					<td class="align-right"  >0.77</td>
  </tr>
                  <tr >
                    <td class="align-left" >MORGAN STANLEY & CO. INTERNATIONAL PLC</td>
                    <td class="align-right"  >106,084</td>
					<td class="align-right"  >0.53</td>
  </tr>
                  <tr >
                    <td class="align-left" >นางสาวนงราม เลาหอารีดิลก</td>
                    <td class="align-right"  >103,000</td>
					<td class="align-right"  >0.52</td>
  </tr>
       
                </table>

                
             
			   <div class="notation" > ณ.วันที่ 30 มีนาคม พ.ศ.2555</div>
</asp:Content>

