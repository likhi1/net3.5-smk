﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//---- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


/// <summary>
/// http://www.thaicreate.com/dotnet/forum/055716.html
/// Summary description for DBClass
/// Class สำหรับ connect database สามารถ ใช้งานได้กับ ACCESS,SQL Server,Oracle
/// เขียนขึ้นโดย Pheak Email manop.muangpia@hotmail.com
/// มีข้อสงสัย,bug แจ้งได้ทาง Email ครับ
/// 
/// string sql1 = "Select * from Employees";
/// DataSet dsEmp1 = new DBClass().SqlGet(sql1,"tblEmployee"); 
/// 
/// ****** DB insert,delete,update  **********
/// command insert,delete,update
/// string sql3 = "Insert into Employees(empId,empName) " +
/// " Values(1,'pheak')";
/// int i = new DBClass().SqlExecute(sql3);
/// 
/// ****** สามารถใช้งาน Parameters  *********
/// string sql4 = "Insert into Employees(empId,empName) " + " Values(@empId,@empName) ";
/// SqlParameterCollection param2 = new SqlCommand().Parameters;
/// param2.AddWithValue("empId", SqlDbType.Int).Value = 1;
/// param2.AddWithValue("empName",SqlDbType.VarChar).Value = "pheak";
/// int i2 = new DBClass().SqlExecute(sql4, param2);     //   i2 คือตัวย่อของ  intailCount
///   
/// </summary> 

//ประกาศ Connection ของ Database
public class ConnectDB {
    //SQL Server
    public SqlConnection SqlStrCon() {
        string strConn;  
        strConn = ConfigurationManager.AppSettings["strConnApp"];
        return new SqlConnection(strConn);  

    }
}


public class DBClass {
    //SQL Server Class
    #region
    public DataSet SqlGet(string sql, string tblName) {
        SqlConnection conn = new ConnectDB().SqlStrCon(); // use method SqlStrCon() to connect
        SqlDataAdapter da = new SqlDataAdapter(sql, conn);
        DataSet ds = new DataSet();
        da.Fill(ds, tblName);
        return ds;
    }
    public DataSet SqlGet(string sql, string tblName, SqlParameterCollection parameters) {
        SqlConnection conn = new ConnectDB().SqlStrCon();
        SqlDataAdapter da = new SqlDataAdapter(sql, conn);
        DataSet ds = new DataSet();
        foreach (SqlParameter param in parameters) { 
              da.SelectCommand.Parameters.AddWithValue(param.ParameterName, param.SqlDbType).Value = param.Value;
        } 
        da.Fill(ds, tblName);
        return ds;
    }
    public int SqlExecute(string sql) {
        int i;
        SqlConnection conn = new ConnectDB().SqlStrCon();
        SqlCommand cmd = new SqlCommand(sql, conn);
        conn.Open();
        i = cmd.ExecuteNonQuery();
        conn.Close();
        return i;
    }
    public int SqlExecute(string sql, SqlParameterCollection parameters) {
        int i;
        SqlConnection conn = new ConnectDB().SqlStrCon();
        SqlCommand cmd = new SqlCommand(sql, conn);

        foreach (SqlParameter param in parameters) {
            cmd.Parameters.AddWithValue(param.ParameterName, param.SqlDbType).Value = param.Value;
        }
        conn.Open();
        i =  cmd.ExecuteNonQuery();  
        conn.Close();
       return i; 
    }
    //------USE With COUNT()
    public int SqlExecuteScalar(string sql ) {
        SqlConnection conn = new ConnectDB().SqlStrCon();
        SqlCommand cmd = new SqlCommand(sql, conn); 
        conn.Open();
        int intCount = (int)cmd.ExecuteScalar();

        conn.Close();
        return intCount;
    }
    public int SqlExecuteScalar(string sql, SqlParameterCollection parameters) { 
        SqlConnection conn = new ConnectDB().SqlStrCon();
        SqlCommand cmd = new SqlCommand(sql, conn);
        foreach (SqlParameter param in parameters) {
            cmd.Parameters.AddWithValue(param.ParameterName, param.SqlDbType).Value = param.Value;
        }
        conn.Open(); 
        int intCount = (int)cmd.ExecuteScalar();

        conn.Close(); 
        return intCount;
    }
    public DataSet SqlExcSto(string stpName, string tblName, SqlParameterCollection parameters) {
        SqlConnection conn = new ConnectDB().SqlStrCon();
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = stpName;
        foreach (SqlParameter param in parameters) {
            cmd.Parameters.AddWithValue(param.ParameterName, param.SqlDbType).Value = param.Value;
        }
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        da.Fill(ds, tblName);
        return ds;
    }
    #endregion

}

 