﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
  

/// <summary>
/// xhttp://csharpdotnetfreak.blogspot.com/2008/12/finding-ip-address-behind-proxy-using-c.html
/// </summary>
public class GetIP
{
	public GetIP()
	{
		//
		// TODO: Add constructor logic here
		//
	}

  
    //============ Check IP Even use Proxy
    public string IpAddress() {
        string strIpAddress;
        strIpAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        if (strIpAddress == null) {
            strIpAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        }
        return strIpAddress;

    }
}