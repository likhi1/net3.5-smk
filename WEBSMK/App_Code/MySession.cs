﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// This Class For Create Session
/// xhttp://stackoverflow.com/questions/621549/how-to-access-session-variables-from-any-class-in-asp-net#comment12516897_621553
/// </summary>
public class MySession
{
    //****** Constructer
	public MySession() {
        Property1 = "default value";
	}


    //****** Method  Current 
    public static MySession Current {  // Gets the current session.
        get {
            MySession session =  (MySession)HttpContext.Current.Session["__MySession__"];
            if (session == null) {
                session = new MySession();
                HttpContext.Current.Session["__MySession__"] = session;
            }
            return session;
        }
    }


    // **** add your session properties here, e.g like this:
    public string Property1 { get; set; }
    public DateTime MyDate { get; set; }
    public int LoginId { get; set; }
    public string SessAdmin { get; set; } 

}


// This class stores one instance of itself in the ASP.NET session and allows you to access your session properties in a type-safe way from any class, e.g like this:


//class Starter (){ 
//    int loginId = MySession.Current.LoginId; 
//    string property1 = MySession.Current.Property1;
//    MySession.Current.Property1 = newValue; 
//    DateTime myDate = MySession.Current.MyDate;
//    MySession.Current.MyDate = DateTime.Now; 
//}



//*****  This approach has several advantages ********
//it saves you from a lot of type-casting
//you don't have to use hard-coded session keys throughout your application (e.g. Session["loginId"]
//you can document your session items by adding XML doc comments on the properties of MySession
//you can initialize your session variables with default values (e.g. assuring they are not null)
