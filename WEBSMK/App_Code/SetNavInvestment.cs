﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
//////////////   

/// <summary>
/// Summary description for SetNavigation
/// </summary>ss
public class SetNavInvestment
{
    // protected string CatId { get { return HttpContext.Current.Request.QueryString["cat"]; } }

    public static int catId;
    public  static int CatId {
        get {
              catId = Convert.ToInt32(HttpContext.Current.Request.QueryString["cat"]);
              return catId ;
        } 
    }

    public static DataTable DtMinbyByCat { get;  set; }
    public static int Language { get; set; }  
   

    public SetNavInvestment( ) {
        // dtMinSubmenu =  SelectMinOfSubmenu( ); 
    } 
       
    public  static  void  SelectMinIdByCat( ) {
        string sql = "SELECT  InvCatId  , min(InvId)  AS MinInvId ,  min(InvSort)  AS MinInvSort   ";
        sql += "FROM tbInv  ";
        sql += "WHERE lang= '" + Language + "'  AND  InvStatus = 1 ";
        sql += "GROUP BY  InvCatId ";  
        //  select invcatid,min(invid) as inv  ,  invsort  from tbinv where lang=1 group by invcatid order by invsort 

        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql, "tb"); 
        DataTable dt = ds.Tables[0];
        // string  rs =  dt.Rows[0].["setCat"].tostring()  ;  // Get value on Rows[0]  by  field "setCat"
        DtMinbyByCat = dt; // Set Datatable when Run static method   
    }

    public static int SelectMinIdBySort(int cat  , int minInvSort) {
           string sql = "SELECT  InvId  " ;
                sql += "FROM tbInv  ";
                sql += "WHERE lang= '" + Language + "'  AND  InvStatus = 1 ";
                sql += "AND  InvCatId = '" + cat + "'  ";  
                sql += "AND  InvSort = '" + minInvSort+ "'  ";
               

        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql, "tb");
        object InvId = ds.Tables[0].Rows[0][0] ;
        return  Convert.ToInt32( InvId ) ;

    } 

    //// This Get Id for show on Menu Investment LevelOne///// 
    ////  By check InvSort == 9999  use value of  InvId  for sort  /////
    public static string GetFirstId(int cat) { 

        DtMinbyByCat.PrimaryKey = new DataColumn[] { DtMinbyByCat.Columns["InvCatId"] }; // Set PirmaryKey  for Datatable
        DataRow row = DtMinbyByCat.Rows.Find(cat); // Retrieving value from DataTable using PrimaryKey

           int rsFinal =  0 ; 
           if (null != row) {
               int MinInvId = Convert.ToInt32(row["MinInvId"]); // *****  Get  min  InvId  of  Category
               int MinInvSort = Convert.ToInt32(row["MinInvSort"]); // *****  Get  min  InvSort  of  Category 

               //////// Is InvSort ??? /////  
               if (MinInvSort == 9999) { // If User not insert  default value = 9999  
                   rsFinal = MinInvId;   // Result if User not Assign InvSort 
               }else{
                   rsFinal = SelectMinIdBySort(cat, MinInvSort);  // Result if User Assign InvSort 
               }


           } // end  if (null != row)

           return rsFinal.ToString();
       }


    public static DataSet SelectDataSubmenu(int cat) {
        string sql1 = "SELECT *  FROM tbInv "
        + "WHERE  InvCatId = '" + cat  + "' " // Catagory 1 => Banner Slide
        + "AND InvStatus ='1'  " // check status content 
        + "AND lang = '" + Language + "'  " //  TH = 0 ,  En =  1 
        + "ORDER BY   InvSort ASC  , InvId ASC  ";

        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql1, "tb");
        // DataTable dt = ds.Tables[0];
        return ds;
    }
     

    ///// For Design View Set Menu Level One  
    public static string SetLinkMenuLevelOne(int cat, string name) {
        string nameMenu;
        if ( CatId == cat) { //  CatId in queryString ==  cat in paramiter
            nameMenu = "<a href='#' class='active' >" + name + "</a>";
        } else {

            string strId = SetNavInvestment.GetFirstId(cat);
            if (Language == 0) 
                nameMenu = "<a href='" + Config.SiteUrl + "investment.aspx?cat=" + cat + "&id=" + strId + "' >" + name + "</a>";
            else 
                nameMenu = "<a href='" + Config.SiteUrl + "en/investment.aspx?cat=" + cat + "&id=" + strId + "' >" + name + "</a>";
            
        }


        return nameMenu;
    }



    
} //end Class

 



