using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Globalization;

/// <summary>
/// Summary description for FormatStringApp
/// </summary>
public class FormatStringApp
{
    public FormatStringApp()
    {
        //
        // TODO: Add constructor logic here
        //
    }


    // Modify By : Somza
    // Modiry Date : 06/10/1007
    static int iMoneyDigits = 4;
    public static string FormatMoney(double dataMoney)
    {
        dataMoney = Math.Round(Convert.ToDouble(dataMoney), iMoneyDigits);
        return dataMoney.ToString("###,###,###,##0.0000");
    }
    public static string FormatMoney(string dataMoney)
    {
        double dTmp = 0;
        if (dataMoney == null)
            dataMoney = "0";
        dataMoney = dataMoney.Trim();
        if (dataMoney != "")
            dTmp = Math.Round(Convert.ToDouble(dataMoney), iMoneyDigits);
        else
            dTmp = 0;
        return FormatMoney(dTmp);
    }
    public static string FormatMoney(object dataMoney)
    {
        double dTmp = 0;
        if (dataMoney != null && !Convert.IsDBNull(dataMoney))
            dTmp = Math.Round(Convert.ToDouble(dataMoney), iMoneyDigits);
        else
            dTmp = 0;
        return FormatMoney(dTmp);
    }
    public static string FormatInt(double dataInt)
    {
        return dataInt.ToString("###,###,###,##0");
    }
    public static string FormatInt(string dataInt)
    {
        double dTmp = 0;
        if (dataInt == null)
            dataInt = "0";
        dataInt = dataInt.Trim();
        if (dataInt != "")
            dTmp = Convert.ToDouble(dataInt);
        else
            dTmp = 0;
        return FormatInt(dTmp);
    }
    public static string FormatInt(object dataInt)
    {
        double dTmp = 0;
        if (dataInt != null)
            dTmp = Convert.ToDouble(Convert.IsDBNull(dataInt) ? 0 : dataInt);
        else
            dTmp = 0;
        return FormatInt(dTmp);
    }
    public static string Format2Digit(double dataMoney)
    {
        return dataMoney.ToString("###,###,###,##0.00");
    }
    public static string Format2Digit(string dataMoney)
    {
        double dTmp = 0;
        if (dataMoney == null)
            dataMoney = "0";
        dataMoney = dataMoney.Trim();
        if (dataMoney != "")
            dTmp = Convert.ToDouble(dataMoney);
        else
            dTmp = 0;
        return Format2Digit(dTmp);
    }
    public static string Format2Digit(object dataMoney)
    {
        double dTmp = 0;
        if (dataMoney != null)
            dTmp = Convert.ToDouble(dataMoney);
        else
            dTmp = 0;
        return Format2Digit(dTmp);
    }
    public static string Format3Digit(double dataMoney)
    {
        return dataMoney.ToString("###,###,###,##0.000");
    }
    public static string Format3Digit(string dataMoney)
    {
        double dTmp = 0;
        if (dataMoney == null)
            dataMoney = "0";
        dataMoney = dataMoney.Trim();
        if (dataMoney != "")
            dTmp = Convert.ToDouble(dataMoney);
        else
            dTmp = 0;
        return Format3Digit(dTmp);
    }
    public static string Format3Digit(object dataMoney)
    {
        double dTmp = 0;
        if (dataMoney != null)
            dTmp = Convert.ToDouble(dataMoney);
        else
            dTmp = 0;
        return Format3Digit(dTmp);
    }
    public static string Format4Digit(double dataMoney)
    {
        return dataMoney.ToString("###,###,###,##0.0000");
    }
    public static string Format4Digit(string dataMoney)
    {
        double dTmp = 0;
        if (dataMoney == null)
            dataMoney = "0";
        dataMoney = dataMoney.Trim();
        if (dataMoney != "")
            dTmp = Convert.ToDouble(dataMoney);
        else
            dTmp = 0;
        return Format4Digit(dTmp);
    }
    public static string Format4Digit(object dataMoney)
    {
        double dTmp = 0;
        if (dataMoney != null && dataMoney != "")
            dTmp = Convert.ToDouble("0" + dataMoney);
        else
            dTmp = 0;
        return Format4Digit(dTmp);
    }
    public static string FormatNDigit(double dataMoney, int iDigit)
    {
        string strDigit = "";
        for (int i = 0; i <= iDigit - 1; i++)
        {
            strDigit += "0";
        }
        return dataMoney.ToString("###,###,###,##0." + strDigit);
    }
    public static string FormatNDigit(object dataMoney, int iDigit)
    {
        double dTmp = 0;
        if (dataMoney != null && !Convert.IsDBNull(dataMoney))
            dTmp = Convert.ToDouble(dataMoney);
        else
            dTmp = 0;
        return FormatNDigit(dTmp, iDigit);
    }
    public static string FormatNDigit(string dataMoney, int iDigit)
    {
        double dTmp = 0;
        if (dataMoney == null)
            dataMoney = "0";
        dataMoney = dataMoney.Trim();
        if (dataMoney != "")
            dTmp = Convert.ToDouble(dataMoney);
        else
            dTmp = 0;
        return FormatNDigit(dTmp, iDigit);
    }    
    public static string replaceQuote(string strData)
    {
        string strRet = "";
        strRet = strData.Replace("\"", "&quot;");
        return strRet;
    }
    /// <summary>
    /// Create by : somza
    /// create date : 01/06/2551
    /// �� ��˹������������� delete �����ʶҹ�㹡�Դ�����ҡѺ�����ҹ
    /// </summary>
    /// <param name="dataStatus">��͸Ժ��ʶҹ�</param>
    /// <param name="dataID"></param>
    /// <returns></returns>
    public static string showDelete(object dataStatus, object dataID)
    {
        string strImg = "";
        if (dataStatus.ToString() == "��ҹ")
        {
            strImg = "<img title=\"ź��¡��\"  id=\"imgDelete\" style=\"cursor: hand;\" border=\"0\" src=\"../../Theme/Images/delete_icon.gif\" onclick=\"deleteData('" + dataID.ToString() + "');\" >";
        }
        return strImg;
    }

    public static string FormatDate(object dataDate)
    {
        string strRet = "";
        if ((!Convert.IsDBNull(dataDate)) && (dataDate.ToString() != "") && (dataDate.ToString() != "__/__/____"))
        {
            strRet = TDS.Utility.MasterUtil.getDateString(Convert.ToDateTime(dataDate), "dd/MM/yyyy", "th-TH");
        }
        return strRet;
    }
    public static string FormatShortDate(object dataDate)
    {
        string strRet = "";
        if ((!Convert.IsDBNull(dataDate)) && (dataDate.ToString() != ""))
        {
            strRet = TDS.Utility.MasterUtil.getDateString(Convert.ToDateTime(dataDate), "dd/MM/yy", "th-TH");
        }
        return strRet;
    }
    public static string showDate(object objDate)
    {
        string strRet = "";
        if ((!Convert.IsDBNull(objDate)) && (objDate.ToString() != ""))
            strRet = TDS.Utility.MasterUtil.getDateString(Convert.ToDateTime(objDate), "dd/MM/yyyy", "th-TH");
        return strRet;
    }
    public static string showRangeDate(object objFromDate, object objToDate)
    {
        string strRet = "";
        if (!Convert.IsDBNull(objFromDate))
            strRet = TDS.Utility.MasterUtil.getDateString(Convert.ToDateTime(objFromDate), "dd/MM/yyyy", "th-TH");
        if (!Convert.IsDBNull(objToDate))
        {
            if (strRet != "")
                strRet += " - ";
            strRet += TDS.Utility.MasterUtil.getDateString(Convert.ToDateTime(objToDate), "dd/MM/yyyy", "th-TH");
        }
        else
        {
            strRet = "����� " + strRet + " �繵��";
        }

        return strRet;
    }
    public static string showShortRangeDate(object objFromDate, object objToDate)
    {
        string strRet = "";
        if (!Convert.IsDBNull(objFromDate))
            strRet = TDS.Utility.MasterUtil.getDateString(Convert.ToDateTime(objFromDate), "dd/MM/yy", "th-TH");
        if (!Convert.IsDBNull(objToDate))
        {
            if (strRet != "")
                strRet += " - ";
            strRet += TDS.Utility.MasterUtil.getDateString(Convert.ToDateTime(objToDate), "dd/MM/yy", "th-TH");
        }
        else
        {
            strRet = "����� " + strRet + " �繵��";
        }

        return strRet;
    }
    public static string showShortRangeDateAndToDesc(object objFromDate, object objToDate, object objToDesc)
    {
        string strRet = "";
        if (!Convert.IsDBNull(objFromDate))
            strRet = TDS.Utility.MasterUtil.getDateString(Convert.ToDateTime(objFromDate), "dd/MM/yy", "th-TH");
        if (!Convert.IsDBNull(objToDate))
        {
            if (strRet != "")
                strRet += " - ";
            strRet += TDS.Utility.MasterUtil.getDateString(Convert.ToDateTime(objToDate), "dd/MM/yy", "th-TH");
        }
        else
        {
            if (objToDesc.ToString() != "")
                strRet += " - " + objToDesc.ToString();
            else
                strRet = "����� " + strRet + " �繵��" + objToDesc.ToString();
        }

        return strRet;
    }
    public static string FormatDateTime(object dataDate)
    {
        string strRet = "";
        if (!Convert.IsDBNull(dataDate))
        {
            strRet = TDS.Utility.MasterUtil.getDateString(Convert.ToDateTime(dataDate), "dd/MM/yyyy HH:mm:ss", "th-TH");
        }
        return strRet;
    }
    public static string FormatTime(object dataDate)
    {
        string strRet = "";
        if (!Convert.IsDBNull(dataDate))
        {
            strRet = TDS.Utility.MasterUtil.getDateString(Convert.ToDateTime(dataDate), "HH:mm", "th-TH");
        }
        return strRet;
    }
    
    public static string showIndexNo(int iIndex, object objStatus)
    {
        string strRet = "";
        strRet = iIndex.ToString();
        if (!Convert.IsDBNull(objStatus) && objStatus.ToString() == "C")
        {
            strRet = strRet + "C";
        }
        else if (!Convert.IsDBNull(objStatus) && objStatus.ToString() == "CC")
        {
            strRet = strRet + "C";
        }
        return strRet;
    }
    

    public static string getStatusName(string strStatus)
    {
        string strRet = "��ҹ";
        if (strStatus == "C")
            strRet = "�����ҹ";

        return strRet;
    }
    public static string getStatusName(object strStatus)
    {
        string strRet = "��ҹ";
        if (strStatus.ToString() == "C")
            strRet = "�����ҹ";

        return strRet;
    }
    public static string getStatusName2(object strStatus)
    {
        string strRet = "��ҹ";
        if (strStatus.ToString() == "C")
            strRet = "¡��ԡ";

        return strRet;
    }
    public static DataSet getMonthName()
    {
        DataSet dsRet = new DataSet();
        dsRet.Tables.Add("");
        dsRet.Tables[0].Columns.Add("month_name");
        dsRet.Tables[0].Columns.Add("month_id");

        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_name"] = "���Ҥ�";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_id"] = "1";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_name"] = "����Ҿѹ��";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_id"] = "2";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_name"] = "�չҤ�";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_id"] = "3";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_name"] = "����¹";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_id"] = "4";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_name"] = "����Ҥ�";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_id"] = "5";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_name"] = "�Զع�¹";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_id"] = "6";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_name"] = "�á�Ҥ�";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_id"] = "7";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_name"] = "�ԧ�Ҥ�";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_id"] = "8";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_name"] = "�ѹ��¹";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_id"] = "9";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_name"] = "���Ҥ�";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_id"] = "10";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_name"] = "��Ȩԡ�¹";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_id"] = "11";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_name"] = "�ѹ�Ҥ�";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_id"] = "12";

        return dsRet;
    }
    

    // write file
    public static string FormatFileMoney(object dataMoney)
    {
        //double dTmp = 0;
        //if (dataMoney != null && !Convert.IsDBNull(dataMoney))
        //    dTmp = Convert.ToDouble(dataMoney);
        //else
        //    dTmp = 0;
        //return dTmp.ToString("00000000.00");
        return FormatMoney(dataMoney);
    }

    public static ProfileData getLoginSession(System.Web.UI.Page objPage, System.Web.SessionState.HttpSessionState session)
    {
        ProfileData loginData;
        //string strAppName = System.Configuration.ConfigurationManager.AppSettings["appName"];
        if (session["SessAdmin"] == null)
        {
            objPage.Server.Transfer("~/Adminweb/Main.aspx", false);
            loginData = new ProfileData();
        }
        else
        {
            loginData = (ProfileData)session["SessAdmin"];
        }
        return loginData;
    }


    // --------  SMK  ---------------
    public static DataSet getAllPolType()
    {
        DataSet dsData;
        DataRow drData;
        string[] columnNames = { "Code", "Text_th", "Text_en" };
        string[] columnTypes = { "string", "string", "string" };
        dsData = new DataSet();
        dsData.Tables.Add("");
        TDS.Utility.MasterUtil.AddColumnToDataTable(dsData.Tables[0], columnTypes, columnNames);
        drData = dsData.Tables[0].NewRow();
        drData["Code"] = "C";
        drData["Text_th"] = "��Сѹ���ö¹���Ҥ�ѧ�Ѻ";
        drData["Text_en"] = "Compulsary";
        dsData.Tables[0].Rows.Add(drData);
        drData = dsData.Tables[0].NewRow();
        drData["Code"] = "V";
        drData["Text_th"] = "��Сѹ���ö¹���Ҥ��Ѥ��";
        drData["Text_en"] = "Voluntary";
        dsData.Tables[0].Rows.Add(drData);
        drData = dsData.Tables[0].NewRow();
        drData["Code"] = "VC";
        drData["Text_th"] = "��Сѹ���ö¹���Ҥ��Ѥ�� ��� �.�.�.";
        drData["Text_en"] = "Voluntary combine Compulsary";
        dsData.Tables[0].Rows.Add(drData);
        drData = dsData.Tables[0].NewRow();
        drData["Code"] = "P";
        drData["Text_th"] = "��Сѹ����غѵ��˵���ǹ�ؤ��";
        drData["Text_en"] = "Personal";
        dsData.Tables[0].Rows.Add(drData);
        drData = dsData.Tables[0].NewRow();
        drData["Code"] = "T";
        drData["Text_th"] = "��Сѹ����Թ�ҧ";
        drData["Text_en"] = "Traval";
        dsData.Tables[0].Rows.Add(drData);

        return dsData;
    }
    public static string changeMoneyToWord(string money)
    {
        string[] arrTemp1;
        string strReturn;
        money = Convert.ToDouble(money).ToString("0.00");
        arrTemp1 = money.Split('.');     // �¡ �ҷ �Ѻ ʵҧ��
        strReturn = intToWord(arrTemp1[0]) + "�ҷ";
        if (arrTemp1[1] != "00")
            strReturn = strReturn + intToWord(arrTemp1[1]) + "ʵҧ��";
        else
            strReturn = strReturn + "��ǹ";
        return strReturn;
    }

    private static string intToWord(string strInt)
    {
        string strReturn = "";
        string strWord;
        string[] arrUnit;
        char[] arrChar;
        int iCount = 0;
        int iUnit = 0;
        int iUnitTemp;
        string strUnit;
        arrUnit = new string[7] { "", "�Ժ", "����", "�ѹ", "����", "�ʹ", "��ҹ" };
        arrChar = strInt.ToCharArray();
        iCount = arrChar.Length - 1;
        while (iCount >= 0)
        {
            if (arrChar[iCount] == '0')
                strWord = "�ٹ��";
            else if (arrChar[iCount] == '1')
            {
                if ((iUnit == 0) || (iUnit % 6 == 0))
                    strWord = "���";
                else
                    strWord = "˹��";
            }
            else if (arrChar[iCount] == '2')
            {
                if ((iUnit == 1) || (iUnit % 7 == 0 && iUnit != 0))
                    strWord = "���";
                else
                    strWord = "�ͧ";
            }
            else if (arrChar[iCount] == '3')
                strWord = "���";
            else if (arrChar[iCount] == '4')
                strWord = "���";
            else if (arrChar[iCount] == '5')
                strWord = "���";
            else if (arrChar[iCount] == '6')
                strWord = "ˡ";
            else if (arrChar[iCount] == '7')
                strWord = "��";
            else if (arrChar[iCount] == '8')
                strWord = "Ỵ";
            else if (arrChar[iCount] == '9')
                strWord = "���";
            else
                strWord = arrChar[iCount].ToString();
            if (strWord != "�ٹ��")
            {
                iUnitTemp = iUnit;
                strUnit = "";
                while (iUnitTemp > 6)
                {
                    iUnitTemp = iUnitTemp - 6;
                }
                if (strWord == arrChar[iCount].ToString())
                    strUnit = strUnit;
                else
                    strUnit = arrUnit[iUnitTemp] + strUnit;
                if (strWord == "˹��" && strUnit == "�Ժ")
                    strReturn = strUnit + strReturn;
                else
                    strReturn = strWord + strUnit + strReturn;
            }
            iCount = iCount - 1;
            iUnit = iUnit + 1;
        }
        return strReturn;
    }
    public static string ShowPaymentType(object dataPayType)
    {
        string strRet = "";
        string strPayType = dataPayType.ToString();
        if (strPayType == "1")
			strRet = "���з���Ң�";
		else if (strPayType == "2")
			strRet = "�͹��Һѭ��";
		else if (strPayType == "3")
			strRet = "SCB EASY";
		else if (strPayType == "4")
			strRet = "�������������";
		else if (strPayType == "5")
			strRet = "�ѵ��ôԵ";
        return strRet;
    }
}
