using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Xml;


/// <summary>
/// Summary description for MailManager
/// </summary>
public class MailManager
{
    string dataFileName = "";
	public MailManager()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public void setXmlFilePath(string dataPath)
    {
        dataFileName = dataPath;
    }
    public string getDataByKey(string dataID)
    {
        XmlTextReader m_xmlr ;        
        
        string strKey = "";
        string strRet = "";        
        m_xmlr = new XmlTextReader(dataFileName);
        m_xmlr.WhitespaceHandling = WhitespaceHandling.None;
        m_xmlr.Read();
        m_xmlr.Read();

        //Load the Loop
        while (! m_xmlr.EOF)
        {
            //Go to the name tag
            m_xmlr.Read();
            //Get the Gender Attribute Value
            strKey = m_xmlr.GetAttribute("key");
            if (dataID == strKey)
            {
                strRet = m_xmlr.ReadString();
                break;
            }
        }    
        //close the reader
        m_xmlr.Close();
        return strRet;
    }
}
