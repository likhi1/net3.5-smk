﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for PremiumManager
/// </summary>
public class PremiumManager : TDS.BL.BLWorker_MSSQL
{
	public PremiumManager()
	{
		//
		// TODO: Add constructor logic here
		//
        strConnection = "strConnLocal";
	}    
    public SqlConnection getMyConnection()
    {
        return getDbConnection();
    }
    public DataSet TestQuery(string strSQL, ref string strError)
    {
        DataSet ds;
        try
        {
            ds = executeQuery(strSQL);
        }
        catch (Exception e)
        {
            strError = e.ToString();
            ds = new DataSet();
            throw e;
        }
        finally
        {
        }
        return ds;
    }
    public DataSet getDataDefaultValue(string strCarCode, string strType)
    {
        DataSet ds;
        string strSQL = "";        
        strSQL = "SELECT * " + System.Environment.NewLine;
        strSQL += " FROM mst_defaultvalue " + System.Environment.NewLine;
        strSQL += " WHERE car_cod in ('" + strCarCode + "','A') ";
        if (strType != "")
            strSQL += " AND defv_type = '" + strType + "' " + System.Environment.NewLine;
        strSQL += " ORDER BY defv_type, defv_id ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public string addDataOnlineHeadPremium(string strCampaign, string strPromotion,
                                            string strCarCode, string strCarODFrom,
                                            string strCarODTo, string strRowNum,
                                            SqlConnection conn, SqlTransaction trans)
    {
        string strRet = "";
        string strSQL = "";        
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        strRet = getMaxID("hprem_id", "online_headpremium", "", 6, true, true, conn, trans);
        strSQL = "INSERT INTO online_headpremium(hprem_id, " + System.Environment.NewLine;
        strSQL += " campaign, promotion, car_cod, car_odfrom, car_odto, " + System.Environment.NewLine;
        strSQL += " row_num, sys_dte " + System.Environment.NewLine;
        strSQL += " ) VALUES (" + System.Environment.NewLine;
        strSQL += " '" + strRet + "', " + System.Environment.NewLine;
        strSQL += " '" + strCampaign + "' , " + System.Environment.NewLine;
        strSQL += " '" + strPromotion + "' , " + System.Environment.NewLine;
        strSQL += " '" + strCarCode + "' , " + System.Environment.NewLine;
        strSQL += " '" + strCarODFrom + "' , " + System.Environment.NewLine;
        strSQL += " '" + strCarODTo + "' , " + System.Environment.NewLine;
        strSQL += " '" + strRowNum + "' , " + System.Environment.NewLine;
        strSQL += " getDate()  " + System.Environment.NewLine;
        strSQL += " ) ";
        executeQuery(strSQL, conn, trans);
        return strRet;
    }
    public bool clearDataOnlinePremiumCover(string strCampaign, string strCarCode,
                                            string strCarODFrom, string strCarODTo,
                                            SqlConnection conn, SqlTransaction trans)
    {
        bool isRet = true;
        string strSQL = "";
        strSQL += " DELETE FROM online_cover " + System.Environment.NewLine;
        strSQL += " WHERE cover_id in ( SELECT cover_id FROM online_premium " + System.Environment.NewLine;
        strSQL += " WHERE campaign = '" + strCampaign + "' AND " + System.Environment.NewLine;
        strSQL += "     car_cod = '" + strCarCode + "' AND " + System.Environment.NewLine;
        strSQL += "     car_od between '" + strCarODFrom + "' AND '" + strCarODTo + "' )" ;
        executeQuery(strSQL, conn, trans);
        
        strSQL = "DELETE FROM online_premium " + System.Environment.NewLine;
        strSQL += " WHERE campaign = '" + strCampaign + "' AND " + System.Environment.NewLine;
        strSQL += "     car_cod = '" + strCarCode + "' AND " + System.Environment.NewLine;
        strSQL += "     car_od between '" + strCarODFrom + "' AND '" + strCarODTo + "' " + System.Environment.NewLine;
        executeQuery(strSQL, conn, trans);
        return isRet;
    }
    public bool updateDataOnlineHeadPremium(string strHeadPremID, string strRowNum,
                                            SqlConnection conn, SqlTransaction trans)
    {
        bool isRet = true;
        string strSQL = "";
        strSQL = "UPDATE online_headpremium SET " + System.Environment.NewLine;
        strSQL += " row_num = '" + strRowNum + "' " + System.Environment.NewLine;
        strSQL += " WHERE hprem_id = '" + strHeadPremID + "' ";
        executeQuery(strSQL, conn, trans);
        return isRet;
    }
    public string addDataOnlineCover(string strCampaign, string strCarCode,
                                            string strPolType, string strMotGar,
                                            string strCarAge, string strCarGroup,
                                            string strCarMak, string strCarSize,
                                            string strCarBody, string strAgentCode,
                                            string strTpbi1, string strTbpi2,
                                            string strTppd, string strTppdDD,
                                            string strODDD, string strPermD01,
                                            string strPremPNum, string strPermP01,
                                            string strTempD01, string strTempPNum,
                                            string strTempP01, string strCover02,
                                            string strCover03,
                                            SqlConnection conn, SqlTransaction trans)
    {
        string strRet = "";
        string strSQL = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        strRet = getMaxID("cover_id", "online_cover", "", 6, true, true, conn, trans);
        strSQL = "INSERT INTO online_cover(cover_id," + System.Environment.NewLine;
        strSQL += " campaign, car_cod, pol_typ, mot_gar, car_age, " + System.Environment.NewLine;
        strSQL += " car_group, car_mak, car_size, car_body, agt_cod, " + System.Environment.NewLine;
        strSQL += " tpbi1, tpbi2, tppd, " + System.Environment.NewLine;
        strSQL += " tppd_dd, od_dd, perm_d_01, perm_p_num, perm_p_01, " + System.Environment.NewLine;
        strSQL += " temp_d_01, temp_p_num, temp_p_01, cover_02, cover_03, " + System.Environment.NewLine;
        strSQL += " sys_dte " + System.Environment.NewLine;
        strSQL += " ) VALUES (" + System.Environment.NewLine;
        strSQL += " '" + strRet + "' , " + System.Environment.NewLine;
        strSQL += " '" + strCampaign + "' , " + System.Environment.NewLine;
        strSQL += " '" + strCarCode + "' , " + System.Environment.NewLine;
        strSQL += " '" + strPolType + "' , " + System.Environment.NewLine;
        strSQL += " '" + strMotGar + "' , " + System.Environment.NewLine;
        strSQL += " '" + strCarAge + "' , " + System.Environment.NewLine;
        strSQL += " '" + strCarGroup + "' , " + System.Environment.NewLine;
        strSQL += " '" + strCarMak + "' , " + System.Environment.NewLine;
        strSQL += " '" + strCarSize + "' , " + System.Environment.NewLine;
        strSQL += " '" + strCarBody + "' , " + System.Environment.NewLine;
        strSQL += " '" + strAgentCode + "' , " + System.Environment.NewLine;
        strSQL += " '" + strTpbi1 + "' , " + System.Environment.NewLine;
        strSQL += " '" + strTbpi2 + "' , " + System.Environment.NewLine;
        strSQL += " '" + strTppd + "' , " + System.Environment.NewLine;
        strSQL += " '" + strTppdDD + "' , " + System.Environment.NewLine;
        strSQL += " '" + strODDD + "' , " + System.Environment.NewLine;
        strSQL += " '" + strPermD01 + "' , " + System.Environment.NewLine;
        strSQL += " '" + strPremPNum + "' , " + System.Environment.NewLine;
        strSQL += " '" + strPermP01 + "' , " + System.Environment.NewLine;
        strSQL += " '" + strTempD01 + "' , " + System.Environment.NewLine;
        strSQL += " '" + strTempPNum + "' , " + System.Environment.NewLine;
        strSQL += " '" + (strTempP01 == "" ? "0" : strTempP01) + "' , " + System.Environment.NewLine;
        strSQL += " '" + (strCover02 == "" ? "0" : strCover02)+ "' , " + System.Environment.NewLine;
        strSQL += " '" + (strCover03 == "" ? "0" : strCover03) + "' , " + System.Environment.NewLine;
        strSQL += " getDate()  " + System.Environment.NewLine;
        strSQL += " ) ";
        try
        {
            executeQuery(strSQL, conn, trans);
        }
        catch (Exception e)
        {
            strRet = "";
            TDS.Utility.MasterUtil.writeLog("SQL - addDataOnlineCover", e.ToString());
            TDS.Utility.MasterUtil.writeLog("addDataOnlineCover", strSQL);            
        }        
        return strRet;
    }
    public string addDataOnlinePremium(string strCoverID, string strHeadPremID,
                                            string strCampaign, string strCarCode,
                                            string strPolType, string strMotGar,
                                            string strCarAge, string strCarGroup,
                                            string strCarMak, string strCarSize,
                                            string strCarBody, string strAgentCode,
                                            string strCarDriver, string strCarOD, 
                                            string strPremium, string strExtend,
                                            string strNet, string strStamp,
                                            string strTax, string strGross,                                            
                                            SqlConnection conn, SqlTransaction trans)
    {
        string strRet = "";
        string strSQL = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        strSQL = "INSERT INTO online_premium(cover_id, hprem_id," + System.Environment.NewLine;
        strSQL += " campaign, car_cod, pol_typ, mot_gar, car_age, " + System.Environment.NewLine;
        strSQL += " car_group, car_mak, car_size, car_body, agt_cod, " + System.Environment.NewLine;
        strSQL += " car_driver, car_od, pre_pre, pre_ext, " + System.Environment.NewLine;
        strSQL += " pre_net, pre_stm, pre_tax, pre_grs, sys_dte " + System.Environment.NewLine;
        strSQL += " ) VALUES (" + System.Environment.NewLine;
        strSQL += " '" + strCoverID + "' , " + System.Environment.NewLine;
        strSQL += " '" + strHeadPremID + "' , " + System.Environment.NewLine;
        strSQL += " '" + strCampaign + "' , " + System.Environment.NewLine;
        strSQL += " '" + strCarCode + "' , " + System.Environment.NewLine;
        strSQL += " '" + strPolType + "' , " + System.Environment.NewLine;
        strSQL += " '" + strMotGar + "' , " + System.Environment.NewLine;
        strSQL += " '" + strCarAge + "' , " + System.Environment.NewLine;
        strSQL += " '" + strCarGroup + "' , " + System.Environment.NewLine;
        strSQL += " '" + strCarMak + "' , " + System.Environment.NewLine;
        strSQL += " '" + strCarSize + "' , " + System.Environment.NewLine;
        strSQL += " '" + strCarBody + "' , " + System.Environment.NewLine;
        strSQL += " '" + strAgentCode + "' , " + System.Environment.NewLine;
        strSQL += " '" + (strCarDriver == "" ? "0" : strCarDriver.Replace(",", "")) + "' , " + System.Environment.NewLine;
        strSQL += " '" + (strCarOD == "" ? "0" : strCarOD.Replace(",", "")) + "' , " + System.Environment.NewLine;
        strSQL += " '" + (strPremium == "" ? "0" : strPremium.Replace(",","")) + "' , " + System.Environment.NewLine;
        strSQL += " '" + (strExtend == "" ? "0" : strExtend.Replace(",", "")) + "' , " + System.Environment.NewLine;
        strSQL += " '" + (strNet == "" ? "0" : strNet.Replace(",", "")) + "' , " + System.Environment.NewLine;
        strSQL += " '" + (strStamp == "" ? "0" : strStamp.Replace(",", "")) + "' , " + System.Environment.NewLine;
        strSQL += " '" + (strTax == "" ? "0" : strTax.Replace(",", "")) + "' , " + System.Environment.NewLine;
        strSQL += " '" + (strGross == "" ? "0" : strGross.Replace(",", "")) + "' , " + System.Environment.NewLine;
        strSQL += " getDate()  " + System.Environment.NewLine;
        strSQL += " ) ";
        try
        {
            executeQuery(strSQL, conn, trans);
        }
        catch (Exception e)
        {
            TDS.Utility.MasterUtil.writeLog("addDataOnlinePremium", strSQL);
        }
        return strRet;
    }

    public DataSet getDistinctCarCode()
    {
        string strSQL = "";
        DataSet ds;
        strSQL = "SELECT distinct car_cod " + System.Environment.NewLine;
        strSQL += " FROM online_premium " + System.Environment.NewLine;
        strSQL += " ORDER BY car_cod ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getDistinctPolType()
    {
        string strSQL = "";
        DataSet ds;
        strSQL = "SELECT distinct pol_typ " + System.Environment.NewLine;
        strSQL += " FROM online_premium " + System.Environment.NewLine;
        strSQL += " ORDER BY pol_typ " + System.Environment.NewLine;
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getDistinctCarGroup()
    {
        string strSQL = "";
        DataSet ds;
        strSQL = "SELECT distinct car_group " + System.Environment.NewLine;
        strSQL += " FROM online_premium " + System.Environment.NewLine;
        strSQL += " ORDER BY car_group ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getDistinctDriverAge()
    {
        string strSQL = "";
        DataSet ds;
        strSQL = "SELECT distinct car_driver " + System.Environment.NewLine;
        strSQL += " FROM online_premium " + System.Environment.NewLine;
        strSQL += " ORDER BY car_driver ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getPeriodPremium(string strType)
    {
        string strSQL = "";
        DataSet ds;
        strSQL = "SELECT * " + System.Environment.NewLine;
        strSQL += " FROM mst_premiumperiod " + System.Environment.NewLine;
        strSQL += " WHERE peri_type = '" + strType + "' " + System.Environment.NewLine;
        strSQL += " ORDER BY peri_value ";
        ds = executeQuery(strSQL);
        return ds;
    }
    
    public bool addDataOnlineWeb(string strCampaign, string strCarCode,
                                            string strFromOd, string strToOd,
                                            string strPeriodOd, string strStartDate,
                                            string strEndDate)
    {
        string strSQL = "";        
        string strOD = "";
        bool isRet = true;
        double dFromOd = 0;
        double dToOd = 0;
        double dPeriod = 0;
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();

        try
        {
            if (strFromOd != "")
                dFromOd = Convert.ToDouble(strFromOd);
            if (strToOd != "")
                dToOd = Convert.ToDouble(strToOd);
            if (strPeriodOd != "")
                dPeriod = Convert.ToDouble(strPeriodOd);

            for (double dOd = dFromOd; dOd <= dToOd; dOd = dOd + dPeriod)
            {
                strOD += "'" + dOd + "',";
            }
            if (strOD != "")
                strOD = strOD.Substring(0, strOD.Length - 1);
            strSQL = "insert into online_web " + System.Environment.NewLine;
            strSQL += " select campaign, car_cod, pol_typ, car_age, car_group, " + System.Environment.NewLine;
	        strSQL += " car_mak, car_size, car_body, agt_cod, car_od, " + System.Environment.NewLine;
	        strSQL += " car_driver, 0, " + System.Environment.NewLine;
            strSQL += "'" + cmDate.convertDateStringForDBTH(strStartDate) + "'," + System.Environment.NewLine;
            if (strEndDate == "__/__/____")
                strSQL += " null, " + System.Environment.NewLine;
            else
                strSQL += "'" + cmDate.convertDateStringForDBTH(strEndDate) + "'," + System.Environment.NewLine;
	        strSQL += " max(case when mot_gar = 'N' then pre_pre else 0 end )as pre_pregar ," + System.Environment.NewLine;
            strSQL += " max(case when mot_gar = 'N' then pre_ext else 0 end )as  pre_extgar," + System.Environment.NewLine;
            strSQL += " max(case when mot_gar = 'N' then pre_net else 0 end )as pre_netgar ," + System.Environment.NewLine;
            strSQL += " max(case when mot_gar = 'N' then pre_stm else 0 end )as pre_stmgar ," + System.Environment.NewLine;
            strSQL += " max(case when mot_gar = 'N' then pre_tax else 0 end )as pre_taxgar ," + System.Environment.NewLine;
            strSQL += " max(case when mot_gar = 'N' then pre_grs else 0 end )as pre_grsgar ," + System.Environment.NewLine;
            strSQL += " max(case when mot_gar = 'Y' then pre_pre else 0 end) as pre_preservice," + System.Environment.NewLine;
            strSQL += " max(case when mot_gar = 'Y' then pre_ext else 0 end) as pre_extservice," + System.Environment.NewLine;
            strSQL += " max(case when mot_gar = 'Y' then pre_net else 0 end) as pre_netservice," + System.Environment.NewLine;
            strSQL += " max(case when mot_gar = 'Y' then pre_stm else 0 end )as pre_stmservice ," + System.Environment.NewLine;
            strSQL += " max(case when mot_gar = 'Y' then pre_tax else 0 end )as pre_taxservice ," + System.Environment.NewLine;
            strSQL += " max(case when mot_gar = 'Y' then pre_grs else 0 end) as pre_grsservice," + System.Environment.NewLine;
	        strSQL += " GETDATE()" + System.Environment.NewLine;
            strSQL += " from online_premium " + System.Environment.NewLine;
            strSQL += " where campaign = '" + strCampaign + "' and " + System.Environment.NewLine;
            strSQL += " car_cod = '" + strCarCode + "' and " + System.Environment.NewLine;
            strSQL += " (car_od in (" + strOD + ") ) " + System.Environment.NewLine;
            strSQL += " group by campaign, car_cod, pol_typ, car_age, car_group," + System.Environment.NewLine;
            strSQL += " car_mak, car_size, car_body, agt_cod, car_driver, car_od" + System.Environment.NewLine;
            executeUpdate(strSQL);
        }
        catch (Exception e)
        {
            isRet = false;
            TDS.Utility.MasterUtil.writeError("Error at addDataOnlineWeb - e.ToString", e.ToString());
            TDS.Utility.MasterUtil.writeError("Error at addDataOnlineWeb - SQL", strSQL);
        }
        return isRet;
    }
    public DataSet searchDataOnlineWeb(string strCampaign, string strCarCode,
                                        string strPolType, string strFromCarAge,
                                        string strToCarAge, string strCarGroup,
                                        string strCarMak, string strCarSize,
                                        string strCarBody, string strAgentCode, 
                                        string strFromCarOD, string strToCarOD, 
                                        string strCarDriver, 
                                        string strFromStartDate, string strToStartDate,
                                        string strFromEndDate, string strToEndDate)
    {
        string strSQL = "";
        DataSet ds;
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        strSQL = "SELECT campaign ,  " + System.Environment.NewLine;
        strSQL += " car_cod , pol_typ , car_age , car_group , car_mak , " + System.Environment.NewLine;
        strSQL += " car_size , car_body , agt_cod , car_od , car_driver , " + System.Environment.NewLine;
        strSQL += " start_dte , end_dte , pre_grsgar , pre_grsservice " + System.Environment.NewLine;
        strSQL += " FROM online_web " + System.Environment.NewLine;
        strSQL += " WHERE campaign = '" + strCampaign + "' AND " + System.Environment.NewLine;
        strSQL += "     car_cod = '" + strCarCode + "' AND " + System.Environment.NewLine;
        strSQL += "     pol_typ = '" + strPolType + "' AND " + System.Environment.NewLine;
        if (strFromCarAge != "")
            strSQL += "     car_age >= '" + strFromCarAge + "' AND " + System.Environment.NewLine;
        if (strToCarAge != "")
            strSQL += "     car_age <= '" + strToCarAge + "' AND " + System.Environment.NewLine;
        if (strCarGroup != "")
            strSQL += "     car_group = '" + strCarGroup + "' AND " + System.Environment.NewLine;
        if (strCarMak != "")
            strSQL += "     car_mak = '" + strCarMak + "' AND " + System.Environment.NewLine;
        if (strCarSize != "")
            strSQL += "     car_size = '" + strCarSize + "' AND " + System.Environment.NewLine;
        if (strCarBody != "")
            strSQL += "     car_body = '" + strCarBody + "' AND " + System.Environment.NewLine;
        if (strAgentCode != "")
            strSQL += "     agt_cod = '" + strAgentCode + "' AND " + System.Environment.NewLine;
        if (strFromCarOD != "")
            strSQL += "     car_od >= '" + strFromCarOD.Replace(",","") + "' AND " + System.Environment.NewLine;
        if (strToCarOD != "")
            strSQL += "     car_od <= '" + strToCarOD.Replace(",", "") + "' AND " + System.Environment.NewLine;
        if (strCarDriver != "")
            strSQL += "     car_driver = '" + strCarDriver + "' AND " + System.Environment.NewLine;
        if (strFromStartDate != "__/__/____")
            strSQL += "     start_dte >= '" + cmDate.convertDateStringForDBTH(strFromStartDate) + "' AND " + System.Environment.NewLine;
        if (strToStartDate != "__/__/____")
            strSQL += "     start_dte <= '" + cmDate.convertDateStringForDBTH(strToStartDate) + "' AND " + System.Environment.NewLine;
        if (strFromEndDate != "__/__/____")
            strSQL += "     end_dte >= '" + cmDate.convertDateStringForDBTH(strFromEndDate) + "' AND " + System.Environment.NewLine;
        if (strToEndDate != "__/__/____")
            strSQL += "     end_dte <= '" + cmDate.convertDateStringForDBTH(strToEndDate) + "' AND " + System.Environment.NewLine;

        if (strSQL.EndsWith("AND " + System.Environment.NewLine))
            strSQL = strSQL.Substring(0, strSQL.Length - 6);
        ds = executeQuery(strSQL);
        return ds;
    }
    public bool deleteDataOnlineWeb(string strCampaign, string strCarCode,
                                        string strPolType, string strFromCarAge,
                                        string strToCarAge, string strCarGroup,
                                        string strCarMak, string strCarSize,
                                        string strCarBody, string strAgentCode,
                                        string strFromCarOD, string strToCarOD,
                                        string strCarDriver,
                                        string strFromStartDate, string strToStartDate,
                                        string strFromEndDate, string strToEndDate)
    {
        bool isRet = true;
        string strSQL = "";
        DataSet ds;
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        try
        {
            strSQL = "DELETE online_web " + System.Environment.NewLine;
            strSQL += " WHERE campaign = '" + strCampaign + "' AND " + System.Environment.NewLine;
            strSQL += "     car_cod = '" + strCarCode + "' AND " + System.Environment.NewLine;
            strSQL += "     pol_typ = '" + strPolType + "' AND " + System.Environment.NewLine;
            if (strFromCarAge != "")
                strSQL += "     car_age >= '" + strFromCarAge + "' AND " + System.Environment.NewLine;
            if (strToCarAge != "")
                strSQL += "     car_age <= '" + strToCarAge + "' AND " + System.Environment.NewLine;
            if (strCarGroup != "")
                strSQL += "     car_group = '" + strCarGroup + "' AND " + System.Environment.NewLine;
            if (strCarMak != "")
                strSQL += "     car_mak = '" + strCarMak + "' AND " + System.Environment.NewLine;
            if (strCarSize != "")
                strSQL += "     car_size = '" + strCarSize + "' AND " + System.Environment.NewLine;
            if (strCarBody != "")
                strSQL += "     car_body = '" + strCarBody + "' AND " + System.Environment.NewLine;
            if (strAgentCode != "")
                strSQL += "     agt_cod = '" + strAgentCode + "' AND " + System.Environment.NewLine;
            if (strFromCarOD != "")
                strSQL += "     car_od >= '" + strFromCarOD.Replace(",", "") + "' AND " + System.Environment.NewLine;
            if (strToCarOD != "")
                strSQL += "     car_od <= '" + strToCarOD.Replace(",", "") + "' AND " + System.Environment.NewLine;
            if (strCarDriver != "")
                strSQL += "     car_driver = '" + strCarDriver + "' AND " + System.Environment.NewLine;
            if (strFromStartDate != "__/__/____")
                strSQL += "     start_dte >= '" + cmDate.convertDateStringForDBTH(strFromStartDate) + "' AND " + System.Environment.NewLine;
            if (strToStartDate != "__/__/____")
                strSQL += "     start_dte <= '" + cmDate.convertDateStringForDBTH(strToStartDate) + "' AND " + System.Environment.NewLine;
            if (strFromEndDate != "__/__/____")
                strSQL += "     end_dte >= '" + cmDate.convertDateStringForDBTH(strFromEndDate) + "' AND " + System.Environment.NewLine;
            if (strToEndDate != "__/__/____")
                strSQL += "     end_dte <= '" + cmDate.convertDateStringForDBTH(strToEndDate) + "' AND " + System.Environment.NewLine;

            if (strSQL.EndsWith("AND " + System.Environment.NewLine))
                strSQL = strSQL.Substring(0, strSQL.Length - 6);
            executeUpdate(strSQL);
        }
        catch (Exception e)
        {
            isRet = false;
        }
        return isRet;
    }

    public bool updatStartEndDateeDataOnlineWeb(string strCampaign, string strCarCode,
                                        string strPolType, string strFromCarAge,
                                        string strToCarAge, string strCarGroup,
                                        string strCarMak, string strCarSize,
                                        string strCarBody, string strAgentCode,
                                        string strFromCarOD, string strToCarOD,
                                        string strCarDriver,
                                        string strFromStartDate, string strToStartDate,
                                        string strFromEndDate, string strToEndDate,
                                        string strNewStartDate, string strNewEndDate)
    {
        bool isRet = true;
        string strSQL = "";
        DataSet ds;
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        try
        {
            strSQL = "UPDATE online_web " + System.Environment.NewLine;
            strSQL += " SET start_dte = '" + cmDate.convertDateStringForDBTH(strNewStartDate) + "', " + System.Environment.NewLine;
            if (strNewEndDate != "__/__/____")
                strSQL += "     end_dte = '" + cmDate.convertDateStringForDBTH(strNewEndDate) + "' " + System.Environment.NewLine;
            else
                strSQL += "     end_dte = null " + System.Environment.NewLine;
            strSQL += " WHERE campaign = '" + strCampaign + "' AND " + System.Environment.NewLine;
            strSQL += "     car_cod = '" + strCarCode + "' AND " + System.Environment.NewLine;
            strSQL += "     pol_typ = '" + strPolType + "' AND " + System.Environment.NewLine;
            if (strFromCarAge != "")
                strSQL += "     car_age >= '" + strFromCarAge + "' AND " + System.Environment.NewLine;
            if (strToCarAge != "")
                strSQL += "     car_age <= '" + strToCarAge + "' AND " + System.Environment.NewLine;
            if (strCarGroup != "")
                strSQL += "     car_group = '" + strCarGroup + "' AND " + System.Environment.NewLine;
            if (strCarMak != "")
                strSQL += "     car_mak = '" + strCarMak + "' AND " + System.Environment.NewLine;
            if (strCarSize != "")
                strSQL += "     car_size = '" + strCarSize + "' AND " + System.Environment.NewLine;
            if (strCarBody != "")
                strSQL += "     car_body = '" + strCarBody + "' AND " + System.Environment.NewLine;
            if (strAgentCode != "")
                strSQL += "     agt_cod = '" + strAgentCode + "' AND " + System.Environment.NewLine;
            if (strFromCarOD != "")
                strSQL += "     car_od >= '" + strFromCarOD.Replace(",", "") + "' AND " + System.Environment.NewLine;
            if (strToCarOD != "")
                strSQL += "     car_od <= '" + strToCarOD.Replace(",", "") + "' AND " + System.Environment.NewLine;
            if (strCarDriver != "")
                strSQL += "     car_driver = '" + strCarDriver + "' AND " + System.Environment.NewLine;
            if (strFromStartDate != "__/__/____")
                strSQL += "     start_dte >= '" + cmDate.convertDateStringForDBTH(strFromStartDate) + "' AND " + System.Environment.NewLine;
            if (strToStartDate != "__/__/____")
                strSQL += "     start_dte <= '" + cmDate.convertDateStringForDBTH(strToStartDate) + "' AND " + System.Environment.NewLine;
            if (strFromEndDate != "__/__/____")
                strSQL += "     end_dte >= '" + cmDate.convertDateStringForDBTH(strFromEndDate) + "' AND " + System.Environment.NewLine;
            if (strToEndDate != "__/__/____")
                strSQL += "     end_dte <= '" + cmDate.convertDateStringForDBTH(strToEndDate) + "' AND " + System.Environment.NewLine;

            if (strSQL.EndsWith("AND " + System.Environment.NewLine))
                strSQL = strSQL.Substring(0, strSQL.Length - 6);
            executeUpdate(strSQL);
        }
        catch (Exception e)
        {
            isRet = false;
        }
        return isRet;
    }
    
    public DataSet searchDataOnlineCarMark(string strCarMark, string strCarName, string strCarGroup)
    {
        DataSet ds = new DataSet();
        string strSQL = "";
        string strWhere = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();

        strSQL = "SELECT A.* ";
        strSQL += " FROM online_carmark A ";
        strSQL += " WHERE 1 = 1 ";

        if (strCarMark != "")
            strWhere += " A.carmakcod like '%" + strCarMark + "%' AND ";
        if (strCarName != "")
            strWhere += " A.carnamcod like '%" + strCarName + "%' AND ";
        if (strCarGroup != "")
            strWhere += " A.group_code like '%" + strCarGroup + "%' AND ";
                
        if (strWhere != "")
            strSQL = strSQL + " AND " + strWhere.Substring(0, strWhere.Length - 4);
        ds = executeQuery(strSQL);

        return ds;
    }    
    public string addDataOnlineCarMark(DataRow dr, string strLogin)
    {
        string strRet = "";
        string strSQL = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        SqlConnection conn;
        SqlTransaction trans;
        conn = getDbConnection();
        trans = conn.BeginTransaction();
        try
        {
            strRet = addDataOnlineCarMark(dr, strLogin, conn, trans);
            trans.Commit();
        }
        catch (Exception e)
        {
            strRet = "";
            trans.Rollback();
            TDS.Utility.MasterUtil.writeError("Error on addDataOnlineCarMark - ERROR", e.ToString());
            TDS.Utility.MasterUtil.writeError("Error on addDataOnlineCarMark - SQL", strSQL);
        }
        finally
        {
            trans.Dispose();
            conn.Dispose();
            conn.Close();
        }
        return strRet;
    }    
    public string addDataOnlineCarMark(DataRow dr, string strLogin, SqlConnection conn, SqlTransaction trans)
    {
        string strRet = "";
        string strSQL = "";
        bool isAddMaster = true;
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        try
        {
            strSQL = "INSERT INTO online_carmark(carmakcod, carnamcod, group_code, desc_t, desc_e, ";
            strSQL += " car_type, od_min, od_max, car_cc, input_date, input_user ";
            strSQL += " ) VALUES( ";
            strSQL += " '" + dr["carmakcod"].ToString() + "', ";
            strSQL += " '" + dr["carnamcod"].ToString() + "', ";
            strSQL += " '" + dr["group_code"].ToString() + "', ";
            strSQL += " '" + dr["desc_t"].ToString() + "', ";
            strSQL += " '" + dr["desc_e"].ToString() + "', ";
            strSQL += " '" + dr["car_type"].ToString() + "', ";
            strSQL += " '" + dr["od_min"].ToString().Replace(",", "") + "', ";
            strSQL += " '" + dr["od_max"].ToString().Replace(",", "") + "', ";
            strSQL += " '" + dr["car_cc"].ToString().Replace(",", "") + "', ";
            strSQL += " getDate(), ";
            strSQL += " '" + strLogin + "') ";

            executeUpdate(strSQL, conn, trans);
        }
        catch (Exception e)
        {
            strRet = "ERROR";
            TDS.Utility.MasterUtil.writeError("Error on addDataOnlineCarMark - ERROR", e.ToString());
            TDS.Utility.MasterUtil.writeError("Error on addDataOnlineCarMark - SQL", strSQL);
        }
        return strRet;
    }
    public bool editDataOnlineCarMark(DataRow dr, string strLogin)
    {
        bool isRet = true;
        bool isAddMaster = true;
        string strSQL = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        SqlConnection conn;
        SqlTransaction trans;
        conn = getDbConnection();
        trans = conn.BeginTransaction();        
        try
        {
            strSQL = "UPDATE online_carmark SET ";
            strSQL += " carnamcod = '" + dr["carnamcod"].ToString() + "', ";
            strSQL += " group_code = '" + dr["group_code"].ToString() + "', ";
            strSQL += " desc_t = '" + dr["desc_t"].ToString() + "', ";
            strSQL += " desc_e = '" + dr["desc_e"].ToString() + "', ";
            strSQL += " car_type = '" + dr["car_type"].ToString() + "', ";
            strSQL += " od_min = '" + dr["od_min"].ToString().Replace(",", "") + "', ";
            strSQL += " od_max = '" + dr["od_max"].ToString().Replace(",", "") + "', ";
            strSQL += " car_cc = '" + dr["car_cc"].ToString().Replace(",", "") + "', ";
            strSQL += " input_date = getDate(),";
            strSQL += " input_user = '" + strLogin + "' ";
            strSQL += " WHERE carmakcod = '" + dr["carmakcod"].ToString() + "' ";
            executeUpdate(strSQL, conn, trans);
            trans.Commit();
        }
        catch (Exception e)
        {
            isRet = false;
            TDS.Utility.MasterUtil.writeError("Error on editDataOnlineCarMark - ERROR", e.ToString());
            TDS.Utility.MasterUtil.writeError("Error on editDataOnlineCarMark - SQL", strSQL);
        }
        finally
        {
            trans.Dispose();
            conn.Dispose();
            conn.Close();
        }
        return isRet;
    }    
    public bool isDuplicateOnlineCarMark(DataRow dr)
    {
        bool isRet = true;
        string strSQL = "";
        DataSet ds;
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();


        strSQL = "SELECT * " + System.Environment.NewLine;
        strSQL += " FROM online_carmark " + System.Environment.NewLine;
        strSQL += " WHERE carmakcod = '" + dr["carmakcod"].ToString() + "'  " + System.Environment.NewLine;
        ds = executeQuery(strSQL);
        if (ds.Tables[0].Rows.Count > 0)
            isRet = false;

        return isRet;
    }
}
