﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for OnlineCompulsaryManager
/// </summary>
public class OnlineCompulsaryManager : TDS.BL.BLWorker_MSSQL
{
	public OnlineCompulsaryManager()
	{
		//
		// TODO: Add constructor logic here
		//
        strConnection = "strConnLocal";
	}
    public DataSet getAllBrandComp()
    {
        DataSet dsRet;
        string strSQL;
        strSQL = "SELECT mdc_major_cd, mdc_desc_t as mdc_desc";
        strSQL += " FROM mtdesc_comp ";
        strSQL += " WHERE mdc_minor_cd = '0000'";
        strSQL += " ORDER BY mdc_desc ";
        dsRet = executeQuery(strSQL);
        return dsRet;
    }
    public DataSet getBrandCompByCode(string strMajorCD)
    {
        DataSet dsRet;
        string strSQL;
        strSQL = "SELECT mdc_major_cd, mdc_desc_t as mdc_desc";
        strSQL += " FROM mtdesc_comp ";
        strSQL += " WHERE mdc_minor_cd = '0000' AND ";
        strSQL += "         mdc_major_cd = '" + strMajorCD + "' ";
        strSQL += " ORDER BY mdc_desc ";
        dsRet = executeQuery(strSQL);
        return dsRet;
    }
    public DataSet getCategoryCompByBrand(string dataBrand)
    {
        string strSQL;
        DataSet dsRet;
        string[] columnName = { "mdc_minor_cd", "mdc_desc" };
        string[] columnType = { "string", "string" };
        if (dataBrand != "")
        {
            strSQL = "SELECT mdc_minor_cd , mdc_desc_t as mdc_desc";
            strSQL += " FROM mtdesc_comp ";
            strSQL += " WHERE mdc_minor_cd <> '0000' AND mdc_major_cd = '" + dataBrand + "'";
            strSQL += " ORDER BY mdc_desc ";
            TDS.Utility.MasterUtil.writeLog("getCategoryCompByBrand", strSQL);
            dsRet = executeQuery(strSQL);
        }
        else
        {
            dsRet = new DataSet();
            dsRet.Tables.Add("");
            TDS.Utility.MasterUtil.AddColumnToDataTable(dsRet.Tables[0], columnType, columnName);
        }
        return dsRet;
    }
    public DataSet getCategoryCompByCode(string strMajorCD, string strMinorCD)
    {
        string strSQL;
        DataSet dsRet;
        string[] columnName = { "mdc_minor_cd", "mdc_desc" };
        string[] columnType = { "string", "string" };
        if (strMajorCD != "")
        {
            strSQL = "SELECT mdc_minor_cd , mdc_desc_t as mdc_desc";
            strSQL += " FROM mtdesc_comp ";
            strSQL += " WHERE mdc_minor_cd <> '0000' AND mdc_major_cd = '" + strMajorCD + "'";
            strSQL += "     AND mdc_minor_cd = '" + strMinorCD + "' ";
            strSQL += " ORDER BY mdc_desc ";
            TDS.Utility.MasterUtil.writeLog("getCategoryCompByCode", strSQL);
            dsRet = executeQuery(strSQL);
        }
        else
        {
            dsRet = new DataSet();
            dsRet.Tables.Add("");
            TDS.Utility.MasterUtil.AddColumnToDataTable(dsRet.Tables[0], columnType, columnName);
        }
        return dsRet;
    }
    public DataSet getSizeCompByCategory(string dataBrand, string dataCategory, string dataVehUse)
    {
        DataSet dsRet;
        string strSQL;
        string[] columnName = { "mdc_veh_comp", "mdcv_veh_cd", "mdcv_desc" };
        string[] columnType = { "string", "string", "string" };
        if (dataCategory != "")
        {
            strSQL = "SELECT mdc_veh_comp, mdcv_veh_cd, mdcv_desc_t as mdcv_desc ";
            strSQL += " FROM mtdesc_comp inner join mtdesc_comp_veh on mtdesc_comp.mdc_veh_comp = mtdesc_comp_veh.mdcv_veh_comp";
            strSQL += " WHERE mtdesc_comp.mdc_major_cd = '" + dataBrand + "'";
            strSQL += "     AND mtdesc_comp.mdc_minor_cd = '" + dataCategory + "'";
            strSQL += "     AND mtdesc_comp_veh.mdcv_veh_use = '" + dataVehUse + "'";

            dsRet = executeQuery(strSQL);

        }
        else
        {
            dsRet = new DataSet();
            dsRet.Tables.Add("");
            TDS.Utility.MasterUtil.AddColumnToDataTable(dsRet.Tables[0], columnType, columnName);
        }
        return dsRet;
    }
    public DataSet getSizeCompByCategoryByCode(string dataBrand, string dataCategory, string dataVehUse, string mdcv_veh_cd)
    {
        DataSet dsRet;
        string strSQL;
        string[] columnName = { "mdc_veh_comp", "mdcv_veh_cd", "mdcv_desc" };
        string[] columnType = { "string", "string", "string" };
        if (dataCategory != "")
        {
            strSQL = "SELECT mdc_veh_comp, mdcv_veh_cd, mdcv_desc_t as mdcv_desc ";
            strSQL += " FROM mtdesc_comp inner join mtdesc_comp_veh on mtdesc_comp.mdc_veh_comp = mtdesc_comp_veh.mdcv_veh_comp";
            strSQL += " WHERE mtdesc_comp.mdc_major_cd = '" + dataBrand + "'";
            strSQL += "     AND mtdesc_comp.mdc_minor_cd = '" + dataCategory + "'";
            //strSQL += "     AND mtdesc_comp_veh.mdcv_veh_use = '" + dataVehUse + "'";
            strSQL += "     AND mtdesc_comp_veh.mdcv_veh_cd = '" + mdcv_veh_cd + "'";
            dsRet = executeQuery(strSQL);

        }
        else
        {
            dsRet = new DataSet();
            dsRet.Tables.Add("");
            TDS.Utility.MasterUtil.AddColumnToDataTable(dsRet.Tables[0], columnType, columnName);
        }
        return dsRet;
    }

    public DataSet getNewPremium(string dataVehCd)
    {
        VoluntaryManager cmVolun = new VoluntaryManager();
        string[] columnNames = { "com_prmm", "stamp_prmm", "tax_prmm", "sum_prmm", "disc_rate", "disc_amt", "net_prmm", "total_prmm" };
        string[] columnTypes = { "string", "string", "string", "string", "string", "string", "string", "string" };
        DataSet dsRet;
        DataRow dr;
        string strSQL;
        DataSet dsCompPrmm;
        DataSet dsTaxRate;
        double dComPrmm = 0;
        double dStampPrmm = 0;
        double dSumPrmm = 0;            // รวมเบี้ยประกันก่อนหักส่วนลด
        double dDiscountRate = 0;
        double dDiscountAmt = 0;
        double dNetPrmm = 0;
        double dTaxPrmm = 0;
        double dTotalPrmm = 0;          // รวมเบี้ยประกันหลังหักส่วนลด 
        // หาค่าเบี้ยประกัน (ยังไม่หักส่วนลด)
        strSQL = "SELECT cp_com_prmm, cp_stamp, cp_tax";
        strSQL += " FROM comprmm";
        strSQL += " WHERE cp_veh_cd = '" + dataVehCd + "'";
        TDS.Utility.MasterUtil.writeLog("getNewPremium - compprmm", strSQL);
        dsCompPrmm = executeQuery(strSQL);
        if (dsCompPrmm.Tables.Count > 0 && dsCompPrmm.Tables[0].Rows.Count > 0)
        {
            dComPrmm = Convert.ToDouble(dsCompPrmm.Tables[0].Rows[0]["cp_com_prmm"]);
            dStampPrmm = Convert.ToDouble(dsCompPrmm.Tables[0].Rows[0]["cp_stamp"]);
            dTaxPrmm = Convert.ToDouble(dsCompPrmm.Tables[0].Rows[0]["cp_tax"]);
            dSumPrmm = dComPrmm + dStampPrmm + dTaxPrmm;
        }
        // หาค่าส่วนลด
        dDiscountRate = Convert.ToDouble(getDiscountPercent("COM"));
        if (dDiscountRate > 0)
        {
            dDiscountAmt = dComPrmm * dDiscountRate / 100;
            dNetPrmm = dComPrmm - dDiscountAmt;
            dNetPrmm = Math.Ceiling(dNetPrmm);
            dsTaxRate = cmVolun.getTaxRate(TDS.Utility.MasterUtil.getDateString(DateTime.Now, "dd/MM/yyyy", "th-TH"));            
            if (dsTaxRate.Tables[0].Rows.Count > 0)
            {
                dStampPrmm = dNetPrmm * Convert.ToDouble(dsTaxRate.Tables[0].Rows[0]["stm_rate"]) / 100;
                dStampPrmm = Math.Ceiling(dStampPrmm);
                dTaxPrmm = (dNetPrmm + dStampPrmm) * Convert.ToDouble(dsTaxRate.Tables[0].Rows[0]["tax_rate"]) / 100;
            }
        }
        else
            dNetPrmm = dComPrmm;
        dTotalPrmm = (dNetPrmm + dStampPrmm + dTaxPrmm);
        // กำหนดค่าลง Dataset
        dsRet = new DataSet();
        dsRet.Tables.Add("");
        TDS.Utility.MasterUtil.AddColumnToDataTable(dsRet.Tables[0], columnTypes, columnNames);
        dr = dsRet.Tables[0].NewRow();
        dr["com_prmm"] = dComPrmm.ToString("##,###,###,##0.00");
        dr["stamp_prmm"] = dStampPrmm.ToString("#,##0.00");
        dr["tax_prmm"] = dTaxPrmm.ToString("#,##0.00");
        dr["sum_prmm"] = dSumPrmm.ToString("##,###,###,##0.00");
        dr["net_prmm"] = dNetPrmm.ToString("##,###,###,##0.00");
        dr["total_prmm"] = dTotalPrmm.ToString("##,###,###,##0.00");
        dr["disc_rate"] = dDiscountRate.ToString("##0.00");
        dr["disc_amt"] = dDiscountAmt.ToString("##,###,###,##0.00");
        dsRet.Tables[0].Rows.Add(dr);

        return dsRet;
    }
    public string getDiscountPercent(string dataCover)
    {
        string strSQL;
        DataSet dsDiscount;
        string strRet = "0";
        strSQL = "SELECT dc_perc ";
        strSQL += " FROM discload ";
        strSQL += " WHERE  dc_major_cd = 'IN' AND dc_minor_cd = '" + dataCover + "'";
        dsDiscount = executeQuery(strSQL);
        if (dsDiscount.Tables.Count > 0 && dsDiscount.Tables[0].Rows.Count > 0)
        {
            strRet = dsDiscount.Tables[0].Rows[0]["dc_perc"].ToString();
        }
        return strRet;
    }

}
