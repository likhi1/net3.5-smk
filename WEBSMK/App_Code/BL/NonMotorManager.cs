﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for NonMotorManager
/// </summary>
public class NonMotorManager : TDS.BL.BLWorker_MSSQL
{
	public NonMotorManager()
	{
		//
		// TODO: Add constructor logic here
		//
        strConnection = "strConnLocal";
	}
    public double getDataPercentByName(string dataName)
    {
        DataSet dsData;
        double dData = 0;
        string sql = "SELECT * ";
        sql += " FROM rate ";
        sql += " WHERE pc_name = '" + dataName + "' ";
        sql += "    AND pc_status = 'A' ";
        sql += " ORDER BY pc_cd ";
        dsData = executeQuery(sql);
        if (dsData.Tables.Count > 0 && dsData.Tables[0].Rows.Count > 0)
            dData = Convert.ToDouble(dsData.Tables[0].Rows[0]["pc_value"].ToString());
        return dData;
    }
    public string getDiscountPercent(string dataCover)
    {
        string strSQL;
        DataSet dsDiscount;
        string strRet = "0";
        strSQL = "SELECT dc_perc ";
        strSQL += " FROM discload ";
        strSQL += " WHERE  dc_major_cd = 'IN' AND dc_minor_cd = '" + dataCover + "'";
        dsDiscount = executeQuery(strSQL);
        if (dsDiscount.Tables.Count > 0 && dsDiscount.Tables[0].Rows.Count > 0)
        {
            strRet = dsDiscount.Tables[0].Rows[0]["dc_perc"].ToString();
        }
        return strRet;
    }
    public DataSet getTravelAccidentNewPremium(string strDays, string strSumIns, string strFlagMedical, string strNumIns)
    {
        string[] columnNames = { "ta_prmm", "stamp_prmm", "tax_prmm", "sum_prmm", "disc_rate", "disc_amt", "net_prmm", "total_prmm" };
        string[] columnTypes = { "string", "string", "string", "string", "string", "string", "string", "string" };
        DataSet dsRet;
        DataRow drRet;
        string strSQL = "";
        DataSet dsTAPrmm;
        double dStampPrmm = 0;
        double dSumPrmm = 0;            // รวมเบี้ยประกันก่อนหักส่วนลด
        double dDiscountRate = 0;
        double dDiscountAmt = 0;
        double dNetPrmm = 0;
        double dTaxPrmm = 0;
        double dTotalPrmm = 0;          // รวมเบี้ยประกันหลังหักส่วนลด 
        double dTaPrmm = 0;
        // หาค่าเบี้ยประกัน (ยังไม่หักส่วนลด)
        strSQL = "SELECT tr_prmm";
        strSQL += " FROM ta_rate";
        strSQL += " WHERE tr_day = '" + strDays + "'";
        strSQL += "     AND tr_sum_ins = " + strSumIns.Replace(",","");
        strSQL += "     AND tr_medic = '" + strFlagMedical + "' ";
        dsTAPrmm = executeQuery(strSQL);
        if (dsTAPrmm.Tables.Count > 0 && dsTAPrmm.Tables[0].Rows.Count > 0)
        {
            dTaPrmm = Convert.ToDouble(dsTAPrmm.Tables[0].Rows[0]["tr_prmm"]);
            dTaPrmm = dTaPrmm * (Convert.ToDouble(strNumIns));
            if (dTaPrmm < 208)
                dTaPrmm = 208;
            dTaPrmm = Math.Ceiling(dTaPrmm);
            dStampPrmm = dTaPrmm * getDataPercentByName("STAMP") / 100;
            dStampPrmm = Math.Ceiling(dStampPrmm);
            dTaxPrmm = dTaPrmm * getDataPercentByName("TAX") / 100;
        }
        dSumPrmm = dTaPrmm + dStampPrmm + dTaxPrmm;
        // หาค่าส่วนลด
        dDiscountRate = Convert.ToDouble(getDiscountPercent("TA"));
        if (dDiscountRate > 0)
        {
            dDiscountAmt = dTaPrmm * dDiscountRate / 100;
            dNetPrmm = dTaPrmm - dDiscountAmt;
            dNetPrmm = Math.Ceiling(dNetPrmm);
            dStampPrmm = dTaPrmm * getDataPercentByName("STAMP") / 100;
            dStampPrmm = Math.Ceiling(dStampPrmm);
            dTaxPrmm = (dNetPrmm + dStampPrmm) * getDataPercentByName("TAX") / 100;
        }
        else
            dNetPrmm = dTaPrmm;
        dTotalPrmm = dNetPrmm + dTaxPrmm + dStampPrmm;
        // กำหนดค่าลง Dataset
        dsRet = new DataSet();
        dsRet.Tables.Add("");
        TDS.Utility.MasterUtil.AddColumnToDataTable(dsRet.Tables[0], columnTypes, columnNames);
        drRet = dsRet.Tables[0].NewRow();
        drRet["ta_prmm"] = dTaPrmm.ToString("##,###,###,##0.00");
        drRet["stamp_prmm"] = dStampPrmm.ToString("#,##0.00");
        drRet["tax_prmm"] = dTaxPrmm.ToString("#,##0.00");
        drRet["sum_prmm"] = dSumPrmm.ToString("##,###,###,##0.00");
        drRet["net_prmm"] = dNetPrmm.ToString("##,###,###,##0.00");
        drRet["total_prmm"] = dTotalPrmm.ToString("##,###,###,##0.00");
        drRet["disc_rate"] = dDiscountRate.ToString("##0.00");
        drRet["disc_amt"] = dDiscountAmt.ToString("##,###,###,##0.00");
        dsRet.Tables[0].Rows.Add(drRet);
        return dsRet;

    }
    public DataSet getAllRelation()
    {
        DataSet dsRet;
        string strSQL;
        strSQL = "SELECT ds_minor_cd, ds_desc_t as ds_desc";
        strSQL += " FROM setcode ";
        strSQL += " WHERE ds_major_cd = 'RE'";
        dsRet = executeQuery(strSQL);
        return dsRet;
    }
    public DataSet getDataRelationByCode(string strMinorCD)
    {
        DataSet dsRet;
        string strSQL;
        strSQL = "SELECT ds_minor_cd, ds_desc_t as ds_desc";
        strSQL += " FROM setcode ";
        strSQL += " WHERE ds_major_cd = 'RE' AND ds_minor_cd = '" + strMinorCD + "' ";
        dsRet = executeQuery(strSQL);
        return dsRet;
    }
    public DataSet getAllCardType()
    {
        DataSet dsRet;
        string strSQL;
        strSQL = "SELECT ds_minor_cd, ds_desc_t as ds_desc";
        strSQL += " FROM setcode ";
        strSQL += " WHERE ds_major_cd = 'CT'";
        dsRet = executeQuery(strSQL);
        return dsRet;
    }
    public DataSet getDataCardTypeByCode(string strMinorCD)
    {
        DataSet dsRet;
        string strRet = "";
        string strSQL;
        strSQL = "SELECT ds_minor_cd, ds_desc_t as ds_desc";
        strSQL += " FROM setcode ";
        strSQL += " WHERE ds_major_cd = 'CT' AND ds_minor_cd = '" + strMinorCD + "' ";
        dsRet = executeQuery(strSQL);
        return dsRet;
    }
    public DataSet getAllOccupation()
    {
        DataSet dsRet;
        string strSQL;
        strSQL = "SELECT ds_minor_cd, ds_desc_t as ds_desc";
        strSQL += " FROM setcode ";
        strSQL += " WHERE ds_major_cd = 'OC'";
        strSQL += "ORDER BY ds_desc_t ";
        dsRet = executeQuery(strSQL);
        return dsRet;
    }
    public DataSet getDataOccupationByCode(string strMinorCD)
    {
        DataSet dsRet;
        string strSQL;
        strSQL = "SELECT ds_minor_cd, ds_desc_t as ds_desc";
        strSQL += " FROM setcode ";
        strSQL += " WHERE ds_major_cd = 'OC' AND ds_minor_cd = '" + strMinorCD + "' ";
        strSQL += "ORDER BY ds_desc_t ";
        dsRet = executeQuery(strSQL);
        return dsRet;
    }

    public DataSet getPAPromotion()
    {
        DataSet dsPromotion;
        string strSQL = "";
        strSQL = "SELECT * FROM pa_promotion ";
        strSQL += " WHERE pm_minor_cd <> 'OTH'";
        strSQL += " ORDER BY pm_major_cd, pm_minor_cd desc ";
        dsPromotion = executeQuery(strSQL);
        return dsPromotion;
    }
    public DataSet getOthPAPromotion()
    {
        DataSet dsPromotion;
        string strSQL = "";
        strSQL = "SELECT * FROM pa_promotion ";
        strSQL += " WHERE pm_minor_cd = 'OTH'";
        strSQL += " ORDER BY pm_major_cd, pm_minor_cd ";
        dsPromotion = executeQuery(strSQL);
        return dsPromotion;
    }
    public DataSet GetPAPromotionByCode(string dataMajorCd, string dataMinorCd)
    {
        DataSet dsPromotion;
        string strSQL = "";
        strSQL = "SELECT * FROM pa_promotion ";
        strSQL += " WHERE pm_major_cd = '" + dataMajorCd + "' ";
        strSQL += "     AND pm_minor_cd = '" + dataMinorCd + "' ";
        dsPromotion = executeQuery(strSQL);
        return dsPromotion;
    }
    public DataSet GetPAMedicineSi(string dataSumIns)
    {
        DataSet dsMed;
        string strSQL;
        strSQL = "SELECT * ";
        strSQL += " FROM pa_medicine ";
        strSQL += " WHERE pam_sum_ins = " + dataSumIns;
        dsMed = executeQuery(strSQL);
        return dsMed;
    }
    private double getPARate(string dataPACode)
    {
        DataSet dsRate;
        string strSQL;
        string strRet = "0";
        strSQL = "SELECT pa_rate";
        strSQL += " FROM pa_tariff";
        strSQL += " WHERE pa_code = '" + dataPACode + "'";
        dsRate = executeQuery(strSQL);
        if (dsRate.Tables.Count > 0 && dsRate.Tables[0].Rows.Count > 0)
            strRet = dsRate.Tables[0].Rows[0]["pa_rate"].ToString();
        return Convert.ToDouble(strRet);
    }
    private DataSet getPaTariff(string dataPACode)
    {
        DataSet dsTariff;
        string strSQL;
        strSQL = "SELECT *";
        strSQL += " FROM pa_tariff";
        strSQL += " WHERE pa_code = '" + dataPACode + "'";
        dsTariff = executeQuery(strSQL);
        return dsTariff;
    }
    private double getAddRate(string dataType, string dataValue)
    {
        DataSet dsRate;
        string strSQL;
        string strRet = "0";
        strSQL = "SELECT ad_rate";
        strSQL += " FROM add_prmm";
        strSQL += " WHERE ad_major_cd = '" + dataType + "'";
        strSQL += "         AND ad_minor_cd = '" + dataValue + "'";
        dsRate = executeQuery(strSQL);
        if (dsRate.Tables.Count > 0 && dsRate.Tables[0].Rows.Count > 0)
            strRet = dsRate.Tables[0].Rows[0]["ad_rate"].ToString();
        return Convert.ToDouble(strRet);
    }

    private double getAddRateAge(string dataType, string dataValue)
    {
        DataSet dsRate;
        string strSQL;
        string strRet = "0";
        strSQL = "SELECT ad_rate";
        strSQL += " FROM add_prmm";
        strSQL += " WHERE ad_major_cd = '" + dataType + "'";
        strSQL += "         AND ad_minor_cd >= '" + dataValue + "'";
        strSQL += " ORDER BY ad_minor_cd ";
        dsRate = executeQuery(strSQL);
        if (dsRate.Tables.Count > 0 && dsRate.Tables[0].Rows.Count > 0)
            strRet = dsRate.Tables[0].Rows[0]["ad_rate"].ToString();
        return Convert.ToDouble(strRet);
    }
    private double getShortRate(string dataDay)
    {
        DataSet dsRate;
        string strSQL;
        string strRet = "0";
        strSQL = "SELECT sr_rate";
        strSQL += " FROM short_rate";
        strSQL += " WHERE sr_day = '" + dataDay + "'";
        dsRate = executeQuery(strSQL);
        if (dsRate.Tables.Count > 0 && dsRate.Tables[0].Rows.Count > 0)
            strRet = dsRate.Tables[0].Rows[0]["sr_rate"].ToString();
        return Convert.ToDouble(strRet);
    }
    public string getDiscountPercentPA(string dataMajor, string dataMinor)
    {
        string strSQL;
        DataSet dsDiscount;
        string strRet = "0";
        strSQL = "SELECT pm_discount ";
        strSQL += " FROM pa_promotion ";
        strSQL += " WHERE  pm_major_cd = '" + dataMajor + "'";
        strSQL += " AND  pm_minor_cd = '" + dataMinor + "'";
        dsDiscount = executeQuery(strSQL);
        if (dsDiscount.Tables.Count > 0 && dsDiscount.Tables[0].Rows.Count > 0)
        {
            strRet = dsDiscount.Tables[0].Rows[0]["pm_discount"].ToString();
        }
        else
        {
            strRet = getDiscountPercent("PA");
        }
        return strRet;
    }
    public DataSet getPANewPremium(DataSet dsData)
    {
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        DateTime dtBirthDate;
        string[] columnNames = { "prmm", "stamp_prmm", "tax_prmm", "sum_prmm", "disc_rate", "disc_amt", "net_prmm", "total_prmm", "add_prmm", "disc_prmm", "sum_ins", "death_prmm", "ttd_prmm", "ptd_prmm", "med_prmm" };
        string[] columnTypes = { "string", "string", "string", "string", "string", "string", "string", "string", "string", "string", "string", "string", "string", "string", "string" };
        DataSet dsRet = new DataSet();
        DataRow dr;
        string strSQL;
        DataSet dsPrmm;
        DataSet dsMed1;
        DataSet dsMed2;
        double dPrmm = 0;
        double dStampPrmm = 0;
        double dSumPrmm = 0;            // รวมเบี้ยประกันก่อนหักส่วนลด
        double dDiscountRate = 0;
        double dDiscountAmt = 0;
        double dNetPrmm = 0;
        double dTaxPrmm = 0;
        double dTotalPrmm = 0;          // รวมเบี้ยประกันหลังหักส่วนลด 
        double dSumIns = 0;
        double dTtdSi = 0;
        double dPtdSi = 0;
        double dMedSi = 0;
        double dDeathPrmm = 0;
        double dBasePrmm = 0;
        double dWarPrmm = 0;
        double dStrPrmm = 0;
        double dSptPrmm = 0;
        double dMtrPrmm = 0;
        double dAirPrmm = 0;
        double dOccPrmm = 0;
        double dAgePrmm = 0;
        double dDiscDhtPrmm = 0;
        double dDiscTtdPrmm = 0;
        double dDiscPtdPrmm = 0;
        int iAge = 0;
        int iCover = 0;

        // หาค่า SumIns, หาค่า DeathPrmm 
        if (dsData.Tables[0].Rows[0]["pa_type"].ToString() == "PA1")
        {
            dSumIns = Convert.ToDouble(dsData.Tables[0].Rows[0]["pa_death_si"].ToString());
            dDeathPrmm = dSumIns * getPARate("PA1") / 100;
        }
        else
        {
            dSumIns = Convert.ToDouble(dsData.Tables[0].Rows[0]["pa_death_si"].ToString());
            dDeathPrmm = dSumIns * getPARate("PA2") / 100;
        }
        TDS.Utility.MasterUtil.writeLog("เบี้ย PA1", Convert.ToString(dDeathPrmm));
        dTtdSi = Convert.ToDouble(dsData.Tables[0].Rows[0]["pa_ttd_si"].ToString());
        dPtdSi = Convert.ToDouble(dsData.Tables[0].Rows[0]["pa_ptd_si"].ToString());
        dMedSi = Convert.ToDouble(dsData.Tables[0].Rows[0]["pa_med_si"].ToString());
        if (dMedSi > 0)
        {
            dsMed1 = getPaTariff("MED1");
            dsMed2 = getPaTariff("MED2");
            dMedSi = Convert.ToDouble(dsMed1.Tables[0].Rows[0]["pa_prmm"].ToString()) + ((dMedSi - Convert.ToDouble(dsMed1.Tables[0].Rows[0]["pa_sum_ins"].ToString())) * Convert.ToDouble(dsMed2.Tables[0].Rows[0]["pa_prmm"].ToString()) / Convert.ToDouble(dsMed2.Tables[0].Rows[0]["pa_sum_ins"].ToString()));
        }
        // หาค่า basePrmm        
        dBasePrmm = dDeathPrmm + dTtdSi + dPtdSi + dMedSi;
        // หาค่า WarPrmm, StrPrmm, SptPrmm, MtrPrmm, AirPrmm, OccPrmm, AgePrmm
        if (dsData.Tables[0].Rows[0]["pa_war"].ToString() == "Y")
            dWarPrmm = dBasePrmm * getPARate("WAR") / 100;
        if (dsData.Tables[0].Rows[0]["pa_str"].ToString() == "Y")
            dStrPrmm = dBasePrmm * getPARate("STR") / 100;
        if (dsData.Tables[0].Rows[0]["pa_spt"].ToString() == "Y")
            dSptPrmm = dBasePrmm * getPARate("SPT") / 100;
        if (dsData.Tables[0].Rows[0]["pa_mtr"].ToString() == "Y")
            dMtrPrmm = dBasePrmm * getPARate("MTR") / 100;
        if (dsData.Tables[0].Rows[0]["pa_air"].ToString() == "Y")
            dAirPrmm = dBasePrmm * getPARate("AIR") / 100;
        dOccPrmm = dBasePrmm * getAddRate("OC", dsData.Tables[0].Rows[0]["pe_occup"].ToString()) / 100;
        dtBirthDate = Convert.ToDateTime(dsData.Tables[0].Rows[0]["pe_birth_dt"].ToString());
        iAge = DateTime.Now.Year - dtBirthDate.Year + 543 + 1;
        dAgePrmm = dBasePrmm * getAddRateAge("AG", iAge.ToString()) / 100;
        // หาค่า discDhtPrmm, discTtdPrmm, discPtdPrmm
        dDiscDhtPrmm = dDeathPrmm * getPARate("DHT") / 100;
        dDiscTtdPrmm = dTtdSi * getAddRate("TTD", dsData.Tables[0].Rows[0]["pa_ttd_week"].ToString()) / 100;
        dDiscPtdPrmm = dPtdSi * getAddRate("PTD", dsData.Tables[0].Rows[0]["pa_ptd_week"].ToString()) / 100;
        // หาค่า premium
        dPrmm = dBasePrmm + dOccPrmm + dAgePrmm + dWarPrmm + dStrPrmm + dSptPrmm + dMtrPrmm + dAirPrmm;
        dPrmm = dPrmm - dDiscTtdPrmm - dDiscPtdPrmm - dDiscDhtPrmm;
        iCover = cmDate.daysDiff(dsData.Tables[0].Rows[0]["rg_effect_dt"].ToString(), dsData.Tables[0].Rows[0]["rg_expiry_dt"].ToString());
        dPrmm = dPrmm - (dPrmm * getShortRate(iCover.ToString()) / 100);
        dPrmm = Math.Ceiling(dPrmm);
        dStampPrmm = dPrmm * getDataPercentByName("STAMP") / 100;
        dStampPrmm = Math.Ceiling(dStampPrmm);
        dTaxPrmm = dPrmm * getDataPercentByName("TAX") / 100;
        dSumPrmm = dPrmm + dStampPrmm + dTaxPrmm;
        TDS.Utility.MasterUtil.writeLog("เบี้ย PA2", Convert.ToString(dPrmm));
        // หาค่าส่วนลด
        //dDiscountRate = Convert.ToDouble(cmDisc.getDiscountPercent("PA"));
        dDiscountRate = Convert.ToDouble(getDiscountPercentPA(dsData.Tables[0].Rows[0]["pm_major_cd"].ToString(), dsData.Tables[0].Rows[0]["pm_minor_cd"].ToString()));
        if (dDiscountRate > 0)
        {
            dDiscountAmt = dPrmm * dDiscountRate / 100;
            dNetPrmm = dPrmm - dDiscountAmt;
            dNetPrmm = Math.Ceiling(dNetPrmm);
            dStampPrmm = Math.Ceiling(dNetPrmm * getDataPercentByName("STAMP") / 100);
            dTaxPrmm = (dNetPrmm + dStampPrmm) * getDataPercentByName("TAX") / 100;
        }
        else
            dNetPrmm = dPrmm;
        dTotalPrmm = (dNetPrmm + dStampPrmm + dTaxPrmm);
        // กำหนดค่าลง Dataset
        dsRet = new DataSet();
        dsRet.Tables.Add("");
        TDS.Utility.MasterUtil.AddColumnToDataTable(dsRet.Tables[0], columnTypes, columnNames);
        dr = dsRet.Tables[0].NewRow();
        dr["prmm"] = dPrmm.ToString("##,###,###,##0.00");
        dr["stamp_prmm"] = dStampPrmm.ToString("#,##0.00");
        dr["tax_prmm"] = dTaxPrmm.ToString("#,##0.00");
        dr["sum_prmm"] = dSumPrmm.ToString("##,###,###,##0.00");
        dr["net_prmm"] = dNetPrmm.ToString("##,###,###,##0.00");
        dr["total_prmm"] = dTotalPrmm.ToString("##,###,###,##0.00");
        dr["disc_rate"] = dDiscountRate.ToString("##0.00");
        dr["disc_amt"] = dDiscountAmt.ToString("##,###,###,##0.00");
        dr["add_prmm"] = Convert.ToString(dOccPrmm + dAgePrmm + dWarPrmm + dStrPrmm + dSptPrmm + dMtrPrmm + dAirPrmm);
        dr["disc_prmm"] = Convert.ToString(dDiscDhtPrmm + dDiscTtdPrmm + dDiscPtdPrmm);
        dr["sum_ins"] = dSumIns.ToString("##,###,###,##0.00");
        dr["death_prmm"] = dDeathPrmm.ToString("##,###,###,##0.00");
        dr["ttd_prmm"] = dDiscTtdPrmm.ToString();
        dr["ptd_prmm"] = dDiscPtdPrmm.ToString();
        dr["med_prmm"] = dMedSi.ToString();
        dsRet.Tables[0].Rows.Add(dr);

        return dsRet;
    }
}
