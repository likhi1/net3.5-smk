﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for OnlineVoluntaryManager
/// </summary>
public class OnlineVoluntaryManager : TDS.BL.BLWorker_MSSQL
{
	public OnlineVoluntaryManager()
	{
		//
		// TODO: Add constructor logic here
		//
        strConnection = "strConnLocal";
	}
    public DataSet getActiveDataInsuranceCompany()
    {
        DataSet ds;
        string strSQL = "";
        strSQL = "SELECT * " + System.Environment.NewLine;
        strSQL += " FROM mst_insurancecompany " + System.Environment.NewLine;
        strSQL += " WHERE insc_status = 'A' " + System.Environment.NewLine;
        strSQL += " ORDER BY insc_name";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getActiveDataMotorType()
    {
        DataSet ds;
        string strSQL = "";
        strSQL = "SELECT * " + System.Environment.NewLine;
        strSQL += " FROM mst_motortype " + System.Environment.NewLine;
        strSQL += " WHERE mott_status = 'A' " + System.Environment.NewLine;
        strSQL += " ORDER BY mott_name";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getAllDataCarName()
    {
        DataSet ds;
        string strSQL = "";
        strSQL = "SELECT rtrim(carnamcod) as carnamcod, desc_t, desc_e " + System.Environment.NewLine;
        strSQL += " FROM online_carname " + System.Environment.NewLine;
        strSQL += " ORDER BY desc_t, carnamcod";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getDataCarMarkByCarnamcod(string strCarNameCode, string strCarType)
    {
        DataSet ds;
        string strSQL = "";
        strSQL = "SELECT * " + System.Environment.NewLine;
        strSQL += " FROM online_carmark " + System.Environment.NewLine;
        strSQL += " WHERE carnamcod like '" + strCarNameCode + "%' AND " + System.Environment.NewLine;
        if (strCarType == "1")
        {
            strSQL += "     carmakcod like '_K%' " + System.Environment.NewLine;
        }
        else if (strCarType == "2" || strCarType == "3")
        {
            strSQL += "     carmakcod like '_P%' AND car_type <> 'V' " + System.Environment.NewLine;
        }
        else if (strCarType == "4")
        {
            strSQL += "      car_type = 'V'" + System.Environment.NewLine;
        }
        else
        {
            strSQL += "     1 = 0 " + System.Environment.NewLine;
        }
        strSQL += " ORDER BY desc_t, carmakcod ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getDataCarMarkByCarMarkCode(string strCarMarkCode)
    {
        DataSet ds;
        string strSQL = "";
        strSQL = "SELECT * " + System.Environment.NewLine;
        strSQL += " FROM online_carmark " + System.Environment.NewLine;
        strSQL += " WHERE carmakcod = '" + strCarMarkCode + "' " + System.Environment.NewLine;
        strSQL += " ORDER BY desc_t, carmakcod ";
        TDS.Utility.MasterUtil.writeLog("getDataCarMarkByCarMarkCode", strSQL);
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getDataPremium(string strCarCod, string strRegisYear, string strCarGroup,
                                    string strCarMark, string strCarSize, string strCarBody,
                                    string strAgtCod, string strFlagDriver, string strDriver1BrithDate,
                                    string strDriver2BrithDate, string strPolType, string strOdMin,
                                    string strOdMax, string strFlagCompul, string strFlagDeduct,
                                    string strExpPercent)
    {
        string strSQL = "";
        string strCarAge = "";
        string strDriver1Age = "0";
        string strDriver2Age = "0";
        DataSet ds;
        // อายุรถ
        strCarAge = Convert.ToString(DateTime.Now.Year + 543 - Convert.ToInt32(strRegisYear) + 1);

        // อายุผู้ขับขี่
        if (strFlagDriver == "Y")
        {
            if (strDriver1BrithDate != "__/__/____" && strDriver1BrithDate != "")
                strDriver1Age = Convert.ToString(DateTime.Now.Year + 543 - Convert.ToInt32(strDriver1BrithDate.Substring(6,4)) );
            if (strDriver2BrithDate != "__/__/____" && strDriver2BrithDate != "")
                strDriver2Age = Convert.ToString(DateTime.Now.Year + 543 - Convert.ToInt32(strDriver2BrithDate.Substring(6, 4)));
             if (strDriver2Age != "0")
             {
                if (Convert.ToInt32(strDriver2Age) < Convert.ToInt32(strDriver1Age))
                strDriver1Age = strDriver2Age;
             }
        }
        strSQL = "SELECT TOP (1) defv_id, car_cod, defv_type, defv_value " + System.Environment.NewLine;
        strSQL += " FROM mst_defaultvalue " + System.Environment.NewLine;
        strSQL += " WHERE (defv_type = 'DRIVER') AND (car_cod = 'A') AND " + System.Environment.NewLine;
        strSQL += "         (defv_value <= '" + strDriver1Age + "') " + System.Environment.NewLine;
        strSQL += " ORDER BY defv_value DESC";
        ds = executeQuery(strSQL);
        if (ds.Tables[0].Rows.Count > 0)
            strDriver1Age = ds.Tables[0].Rows[0]["defv_value"].ToString();

        // car_size
        strSQL = "SELECT TOP (1) defv_id, car_cod, defv_type, defv_value " + System.Environment.NewLine;
        strSQL += " FROM mst_defaultvalue " + System.Environment.NewLine;
        strSQL += " WHERE (defv_type = 'CAR_SIZE') AND (car_cod = '" + strCarCod + "') AND " + System.Environment.NewLine;
        strSQL += "         (defv_value >= '" + strCarSize + "') " + System.Environment.NewLine;
        strSQL += " ORDER BY defv_value asc";
        ds = executeQuery(strSQL);
        if (ds.Tables[0].Rows.Count > 0)
            strCarSize = ds.Tables[0].Rows[0]["defv_value"].ToString();

        // ผลิตภัณฑ์
        #region
        //strSQL = "SELECT B.name, A.web_id, A.campaign, A.car_cod, A.pol_typ, car_age, car_group," + System.Environment.NewLine;
        //strSQL += " car_mak, car_size, A.car_body, agt_cod, car_od, car_driver, start_dte, end_dte, " + System.Environment.NewLine;
        //if (strFlagCompul == "Y")
        //{
        //    //if (strExpPercent != "0")
        //    //{
        //    //    strSQL += " pre_pregar" + strExpPercent + " as pre_pregar, pre_extgar" + strExpPercent + " as pre_extgar, " + System.Environment.NewLine;
        //    //    strSQL += " pre_netgar" + strExpPercent + " + C.pre_net as pre_netgar, pre_stmgar" + strExpPercent + " + C.pre_stm as pre_stmgar, " + System.Environment.NewLine;
        //    //    strSQL += " pre_taxgar" + strExpPercent + " + C.pre_tax as pre_taxgar, " + System.Environment.NewLine;
        //    //    strSQL += " case when pre_grsgar" + strExpPercent + "  > 0 then pre_grsgar" + strExpPercent + " + C.pre_grs else pre_grsgar" + strExpPercent + "  end as pre_grsgar,  " + System.Environment.NewLine;
        //    //    strSQL += " pre_preservice" + strExpPercent + " as pre_preservice, pre_extservice" + strExpPercent + " as pre_extservice, " + System.Environment.NewLine;
        //    //    strSQL += " pre_netservice" + strExpPercent + " + C.pre_net as pre_netservice, pre_stmservice" + strExpPercent + " + C.pre_stm as pre_stmservice, " + System.Environment.NewLine;
        //    //    strSQL += " pre_taxservice" + strExpPercent + " + C.pre_tax as pre_taxservice, " + System.Environment.NewLine;
        //    //    strSQL += " case when pre_grsservice" + strExpPercent + "  > 0 then pre_grsservice" + strExpPercent + " + C.pre_grs else pre_grsservice" + strExpPercent + " end as pre_grsservice,  " + System.Environment.NewLine;
        //    //}
        //    //else
        //    //{
        //        strSQL += " pre_pregar, pre_extgar, " + System.Environment.NewLine;
        //        strSQL += " pre_netgar + C.pre_net as pre_netgar, pre_stmgar + C.pre_stm as pre_stmgar, " + System.Environment.NewLine;
        //        strSQL += " pre_taxgar + C.pre_tax as pre_taxgar, " + System.Environment.NewLine;
        //        strSQL += " case when pre_grsgar  > 0 then pre_grsgar + C.pre_grs else pre_grsgar  end as pre_grsgar,  " + System.Environment.NewLine;
        //        strSQL += " pre_preservice, pre_extservice, " + System.Environment.NewLine;
        //        strSQL += " pre_netservice + C.pre_net as pre_netservice, pre_stmservice + C.pre_stm as pre_stmservice, " + System.Environment.NewLine;
        //        strSQL += " pre_taxservice + C.pre_tax as pre_taxservice, " + System.Environment.NewLine;
        //        strSQL += " case when pre_grsservice  > 0 then pre_grsservice + C.pre_grs else pre_grsservice end as pre_grsservice,  " + System.Environment.NewLine;
        //    //}
        //}
        //else
        //{
        //    //if (strExpPercent != "0")
        //    //{
        //    //    strSQL += " pre_pregar" + strExpPercent + " as pre_pregar, pre_extgar" + strExpPercent + " as pre_extgar, " + System.Environment.NewLine;
        //    //    strSQL += " pre_netgar" + strExpPercent + " as pre_netgar, pre_stmgar" + strExpPercent + " as pre_stmgar, " + System.Environment.NewLine;
        //    //    strSQL += " pre_taxgar" + strExpPercent + " as pre_taxgar, pre_grsgar" + strExpPercent + " as pregrsgar,  " + System.Environment.NewLine;
        //    //    strSQL += " pre_preservice" + strExpPercent + " as pre_preservice, pre_extservice" + strExpPercent + " as pre_extservice, " + System.Environment.NewLine;
        //    //    strSQL += " pre_netservice" + strExpPercent + " as pre_netservice, pre_stmservice" + strExpPercent + " as pre_stmservice, " + System.Environment.NewLine;
        //    //    strSQL += " pre_taxservice" + strExpPercent + " as pre_taxservice, pre_grsservice" + strExpPercent + " as pre_grsservice, " + System.Environment.NewLine;
        //    //}
        //    //else
        //    //{
        //        strSQL += " pre_pregar, pre_extgar, pre_netgar, pre_stmgar, pre_taxgar, pre_grsgar,  " + System.Environment.NewLine;
        //        strSQL += " pre_preservice, pre_extservice, pre_netservice, pre_stmservice, pre_taxservice, pre_grsservice,  " + System.Environment.NewLine;
        //    //}
        //}
        //strSQL += " sys_dte " + System.Environment.NewLine;
        //strSQL += " FROM online_web A  INNER JOIN online_campaign B ON A.campaign = B.campaign AND " + System.Environment.NewLine;
        //strSQL += "         (A.pol_typ = B.pol_typ   OR case when  A.pol_typ in ('1','3') then 'A' else A.pol_typ end = B.pol_typ ) " + System.Environment.NewLine;
        //strSQL += "     INNER JOIN online_compprem C ON A.car_cod = C.car_cod AND case when (A.car_body = 'A') then '0' else A.car_body end = C.car_body " + System.Environment.NewLine;
        //strSQL += " WHERE     (A.car_cod = '" + strCarCod + "') AND " + System.Environment.NewLine;
        //strSQL += "             (A.pol_typ in ('" + strPolType + "')) AND (car_age = '" + strCarAge + "') AND " + System.Environment.NewLine;
        //strSQL += "             (car_group = '" + strCarGroup + "') AND " + System.Environment.NewLine;
        //strSQL += "             (car_mak = '" + strCarMark + "' OR " + System.Environment.NewLine;        
        //strSQL += "                 (car_mak = 'A'  AND (SELECT count(*) FROM online_web T " + System.Environment.NewLine;
        //strSQL += "                                     WHERE T.campaign = A.campaign AND T.car_cod = A.car_cod AND " + System.Environment.NewLine;
        //strSQL += "                                         T.pol_typ = A.pol_typ  AND  " + System.Environment.NewLine;
        //strSQL += "                                         T.car_age = A.car_age AND T.car_group = A.car_group AND " + System.Environment.NewLine;
        //strSQL += "                                         T.car_mak = '" + strCarMark + "' AND T.car_size = A.car_size AND " + System.Environment.NewLine;
        //strSQL += "                                         T.car_body = A.car_body AND T.agt_cod = A.agt_cod AND " + System.Environment.NewLine;
        //strSQL += "                                         T.car_od = A.car_od AND T.car_driver = A.car_driver) = 0 )) AND " + System.Environment.NewLine;
        //strSQL += "             (car_size = '" + strCarSize + "') AND (A.car_body = '" + strCarBody + "') AND " + System.Environment.NewLine;
        //strSQL += "             (agt_cod = '" + strAgtCod + "') AND " + System.Environment.NewLine;
        //if (strOdMin == "0" && strOdMax == "0")
        //{
        //}
        //else
        //{
        //    strSQL += " (car_od between '" + strOdMin + "' AND '" + strOdMax + "') AND" + System.Environment.NewLine;
        //}
        //strSQL += "             (car_driver = '" + strDriver1Age + "') AND " + System.Environment.NewLine;
        ////if (strFlagDeduct == "Y")
        ////    strSQL += "             (car_od > 0) AND " + System.Environment.NewLine;
        ////else if (strFlagDeduct == "N")
        ////    strSQL += "             (car_od = 0) AND " + System.Environment.NewLine;         
        ////strSQL += "             ( (GETDATE() between start_dte and end_dte ) OR (start_dte <= GETDATE() and end_dte is null) ) ";
        //strSQL += " 1 = 1 ";
        //strSQL += " ORDER BY A.campaign, B.name, car_od ";
        #endregion
        strSQL = "exec sp_calonlineweb '" + strFlagCompul + "',";
	    strSQL += "'" + strCarCod + "',";
	    strSQL += "'" + strPolType + "',";
        strSQL += "'" + strCarAge + "',";
        strSQL += "'" + strCarGroup + "',";
        strSQL += "'" + strCarMark + "',";
        strSQL += "'" + strCarSize + "',";
        strSQL += "'" + strCarBody + "',";
        strSQL += "'" + strAgtCod + "',";
        strSQL += strOdMin + ",";
        strSQL += strOdMax + ",";
        strSQL += "'" + strDriver1Age + "',";
	    strSQL += strExpPercent ;
        TDS.Utility.MasterUtil.writeLog("getDataPremium", strSQL);
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getDataCover(string strCampaign, string strPolType, string strCarCode, string strCarSize, string strCarBody)
    {
        string strSQL = "";
        DataSet ds;
        if (strCarCode == "110")
            strCarBody = "A";
        if (strCarCode == "320")
            strCarBody = "N";
        // car_size
        strSQL = "SELECT TOP (1) defv_id, car_cod, defv_type, defv_value " + System.Environment.NewLine;
        strSQL += " FROM mst_defaultvalue " + System.Environment.NewLine;
        strSQL += " WHERE (defv_type = 'CAR_SIZE') AND (car_cod = '" + strCarCode + "') AND " + System.Environment.NewLine;
        strSQL += " (defv_value >= '" + strCarSize + "') " + System.Environment.NewLine;
        strSQL += " ORDER BY defv_value asc";
        ds = executeQuery(strSQL);
        if (ds.Tables[0].Rows.Count > 0)
            strCarSize = ds.Tables[0].Rows[0]["defv_value"].ToString();

        strSQL = "SELECT *  " + System.Environment.NewLine;
        strSQL += " FROM  online_cover " + System.Environment.NewLine;
        strSQL += " WHERE campaign = '" + strCampaign + "' AND  " + System.Environment.NewLine;
        strSQL += " pol_typ  = '" + strPolType + "'  AND  " + System.Environment.NewLine;
        strSQL += " car_cod  = '" + strCarCode + "'  AND  " + System.Environment.NewLine;
        strSQL += " car_size = '" + strCarSize + "' And car_body = '" + strCarBody + "'   ";
        ds = executeQuery(strSQL);
        //TDS.Utility.MasterUtil.writeLog("CalculateService ::getDataCover", strSQL);
        return ds;
    }
    public DataSet getDataVoucherByPrem(string strPrem)
    {
        DataSet ds;
        string strSQL = "";
        strSQL = "SELECT * " + System.Environment.NewLine;
        strSQL += " FROM mst_giftvoucher " + System.Environment.NewLine;
        strSQL += " WHERE '" + strPrem.Replace(",", "") + "' between gvou_premmin and gvou_premmax " + System.Environment.NewLine;        
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getAllProvince()
    {
        DataSet dsRet;
        string strSQL;
        strSQL = "SELECT rtrim(ds_minor_cd) as ds_minor_cd, ds_desc_t as ds_desc";
        strSQL += " FROM setcode ";
        strSQL += " WHERE ds_major_cd = 'CH'";
        dsRet = executeQuery(strSQL);
        return dsRet;
    }
    public DataSet getAllDriveDistance()
    {
        DataSet dsRet;
        string strSQL;
        strSQL = "SELECT *";
        strSQL += " FROM mst_drivedistance ";
        dsRet = executeQuery(strSQL);
        return dsRet;
    }
    public DataSet getDataOnlineWebByID(string strWebID, string strFlagCompul)
    {
        string strSQL = "";
        DataSet ds;
        strSQL = "SELECT A.web_id, A.campaign, A.car_cod, A.pol_typ, car_age, car_group," + System.Environment.NewLine;
        strSQL += " car_mak, car_size, A.car_body, agt_cod, car_od, car_driver, start_dte, end_dte, " + System.Environment.NewLine;
        strSQL += " pre_pregar, pre_extgar, pre_netgar, pre_stmgar, pre_taxgar, pre_grsgar,  " + System.Environment.NewLine;
        strSQL += " pre_preservice, pre_extservice, pre_netservice, pre_stmservice, pre_taxservice, pre_grsservice,  " + System.Environment.NewLine;

        if (strFlagCompul == "Y")
        {
            strSQL += " C.pre_net, C.pre_stm , C.pre_tax, C.pre_grs,  " + System.Environment.NewLine;
        }
        else
        {
            strSQL += " 0 as pre_net,  0 as pre_stm , 0 as pre_tax, 0 as pre_grs,  " + System.Environment.NewLine;
        }
        strSQL += " B.name ";
        strSQL += " FROM online_web A  INNER JOIN online_campaign B ON A.campaign = B.campaign AND " + System.Environment.NewLine;
        strSQL += "         (A.pol_typ = B.pol_typ   OR case when  A.pol_typ in ('1','3') then 'A' else A.pol_typ end = B.pol_typ )" + System.Environment.NewLine;
        strSQL += "     INNER JOIN online_compprem C ON A.car_cod = C.car_cod AND case when (A.car_body in ('A','N') ) then '0' else A.car_body end = C.car_body " + System.Environment.NewLine;
        strSQL += " WHERE A.web_id = '" + strWebID + "' ";

        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getDataInsuranceCompanyByID(string strInscID)
    {
        DataSet ds;
        string strSQL = "";
        strSQL = "SELECT * " + System.Environment.NewLine;
        strSQL += " FROM mst_insurancecompany " + System.Environment.NewLine;
        strSQL += " WHERE insc_id = '" + strInscID + "'  " + System.Environment.NewLine;
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getDataMotorTypeByID(string strMottID)
    {
        DataSet ds;
        string strSQL = "";
        strSQL = "SELECT * " + System.Environment.NewLine;
        strSQL += " FROM mst_motortype " + System.Environment.NewLine;
        strSQL += " WHERE mott_id = '" + strMottID + "'  " + System.Environment.NewLine;
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getDataCarNameByCode(string strCarNameCode)
    {
        DataSet ds;
        string strSQL = "";
        strSQL = "SELECT * " + System.Environment.NewLine;
        strSQL += " FROM online_carname " + System.Environment.NewLine;
        strSQL += " WHERE carnamcod = '" + strCarNameCode + "'  " + System.Environment.NewLine;
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getDataCarMarkByCode(string strCarMarkCode)
    {
        DataSet ds;
        string strSQL = "";
        strSQL = "SELECT * " + System.Environment.NewLine;
        strSQL += " FROM online_carmark " + System.Environment.NewLine;
        strSQL += " WHERE carmakcod = '" + strCarMarkCode + "'  " + System.Environment.NewLine;
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getDataDriveDistanceByID(string strID)
    {
        DataSet ds;
        string strSQL = "";
        strSQL = "SELECT * " + System.Environment.NewLine;
        strSQL += " FROM mst_drivedistance " + System.Environment.NewLine;
        strSQL += " WHERE ddis_id = '" + strID + "'  ";
        ds = executeQuery(strSQL);
        return ds;
    }

    
    


    public DataSet getDataProvinceByCode(string strMinorCD)
    {
        DataSet ds;
        string strSQL = "";
        strSQL = "SELECT rtrim(ds_minor_cd) as ds_minor_cd, ds_desc_t as ds_desc " + System.Environment.NewLine;
        strSQL += " FROM setcode " + System.Environment.NewLine;
        strSQL += " WHERE ds_major_cd = 'CH' AND " + System.Environment.NewLine;
        strSQL += "     ds_minor_cd = '" + strMinorCD + "'  " + System.Environment.NewLine;
        ds = executeQuery(strSQL);
        return ds;
    }
}
