﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for GeneratePremiumManager
/// </summary>
public class VoluntaryManager : TDS.BL.BLWorker_MSSQL
{
    public VoluntaryManager()
	{
		//
		// TODO: Add constructor logic here
		//
        strConnection = "strConnVoluntary";
	}
    public DataSet getAllDataProduct()
    {
        DataSet ds;
        string strSQL = "";
        strSQL = "SELECT * " + System.Environment.NewLine;
        strSQL += " FROM  campaign " + System.Environment.NewLine;
        strSQL += " WHERE cp_sts = 'ACTIVE' AND " + System.Environment.NewLine;
        strSQL += "     pattern in ('S', 'C') ";
        strSQL += " ORDER BY campaign";        
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getAllDataPromotion(string strCampaign)
    {
        DataSet ds;
        string strSQL = "";
        strSQL = "SELECT * " + System.Environment.NewLine;
        strSQL += " FROM  campaign " + System.Environment.NewLine;
        strSQL += " WHERE cp_sts = 'ACTIVE' AND " + System.Environment.NewLine;
        strSQL += "     pattern in ('P') AND " + System.Environment.NewLine;
        strSQL += "     'S' = (SELECT pattern FROM campaign WHERE campaign = '" + strCampaign + "' ) " + System.Environment.NewLine;
        strSQL += " ORDER BY campaign";
        TDS.Utility.MasterUtil.writeLog("", strSQL);
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getDataDistinctTariffInsure(string strCampaign, string strCarCode)
    {
        DataSet ds;
        string strSQL = "";
        strSQL = "SELECT distinct pol_typ, mot_gar, car_mak, agt_cod " + System.Environment.NewLine;
        strSQL += " FROM tariff_insure " + System.Environment.NewLine;
        strSQL += " WHERE campaign = '" + strCampaign + "' AND" +System.Environment.NewLine;
        strSQL += "  car_cod = '" + strCarCode + "' " + System.Environment.NewLine;        
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getDataTariffKey(string strCampaign, string strCarCode)
    {
        DataSet ds;
        string strSQL = "";
        strSQL = "SELECT * " + System.Environment.NewLine;
        strSQL += " FROM tariff_key " + System.Environment.NewLine;
        strSQL += " WHERE campaign = '" + strCampaign + "' AND " + System.Environment.NewLine;
        strSQL += "     car_cod = '" + strCarCode + "'  ";
        strSQL += " ORDER BY pol_typ asc";
        ds = executeQuery(strSQL);
        return ds;
    }
    
    public DataSet getDataTariffInsure(string strCampaign, string strCarCode,
                                        string strPolTyp, string strMotGar,
                                        string strCarAge, string strCarGroup,
                                        string strCarMak, string strCarSize, 
                                        string strCarBody, string strAgtCod)
    {
        DataSet ds;
        string strSQL = "";
        if (strCarBody.Length > 1)
            strCarBody = strCarBody.Substring(1, 1);
        strSQL = "SELECT TOP 1 * " + System.Environment.NewLine;
        strSQL += " FROM tariff_insure " + System.Environment.NewLine;
        strSQL += " WHERE campaign = '" + strCampaign + "' AND " + System.Environment.NewLine;
        strSQL += "     car_cod = '" + strCarCode + "' AND " + System.Environment.NewLine;
        strSQL += "     pol_typ = '" + strPolTyp + "' AND " + System.Environment.NewLine;
        strSQL += "     mot_gar = '" + strMotGar + "' AND " + System.Environment.NewLine;        
        strSQL += "     car_age = '" + strCarAge + "' AND " + System.Environment.NewLine;
        strSQL += "     car_group = '" + strCarGroup + "' AND " + System.Environment.NewLine;
        strSQL += "     car_mak = '" + strCarMak + "' AND " + System.Environment.NewLine;
        strSQL += "     car_size = '" + strCarSize + "' AND " + System.Environment.NewLine;
        strSQL += "     car_body = '" + strCarBody + "' AND " + System.Environment.NewLine;
        strSQL += "     agt_cod = '" + strAgtCod + "' AND " + System.Environment.NewLine;
        strSQL += "     clm_flag = '4'  " + System.Environment.NewLine;
        strSQL += " ORDER BY start_dte desc ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getDataFactorCarUse(string strCarCode, string strPolType)
    {
        DataSet ds;
        string strSQL = "";
        strSQL = "SELECT TOP 1 * " + System.Environment.NewLine;
        strSQL += " FROM caruse " + System.Environment.NewLine;
        strSQL += " WHERE car_cod = '" + strCarCode + "' AND " + System.Environment.NewLine;
        strSQL += "     pol_typ = '" + strPolType + "' " + System.Environment.NewLine;
        strSQL += " ORDER BY start_dte desc ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getDataFactorCarSize(string strCarCode, string strPolType, string strSize)
    {
        DataSet ds;
        string strSQL = "";
        strSQL = "SELECT TOP 1 * " + System.Environment.NewLine;
        strSQL += " FROM carsize " + System.Environment.NewLine;
        strSQL += " WHERE car_cod = '" + strCarCode + "' AND " + System.Environment.NewLine;
        strSQL += "     pol_typ = '" + strPolType + "' AND " + System.Environment.NewLine;
        strSQL += "     '" + strSize + "' between size_min and size_max " + System.Environment.NewLine;
        strSQL += " ORDER BY start_dte desc ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getDataFactorDriver(string strCarCode, string strPolType, string strAge)
    {
        DataSet ds;
        string strSQL = "";
        strSQL = "SELECT TOP 1 * " + System.Environment.NewLine;
        strSQL += " FROM cardriver " + System.Environment.NewLine;
        strSQL += " WHERE car_cod = '" + strCarCode + "' AND " + System.Environment.NewLine;
        strSQL += "     pol_typ = '" + strPolType + "' AND " + System.Environment.NewLine;
        strSQL += "     '" + strAge + "' between age_min and age_max " + System.Environment.NewLine;
        strSQL += " ORDER BY start_dte desc ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getDataFactorCarAge(string strCarCode, string strPolType, string strAge)
    {
        DataSet ds;
        string strSQL = "";
        strSQL = "SELECT TOP 1 * " + System.Environment.NewLine;
        strSQL += " FROM carage " + System.Environment.NewLine;
        strSQL += " WHERE car_cod = '" + strCarCode + "' AND " + System.Environment.NewLine;
        strSQL += "     pol_typ = '" + strPolType + "' AND " + System.Environment.NewLine;
        strSQL += "     age_car = '" + strAge + "' " + System.Environment.NewLine;
        strSQL += " ORDER BY start_dte desc ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getDataFactorCarSumInsure(string strCarCode, string strPolType, string strSumInsure)
    {
        DataSet ds;
        string strSQL = "";
        strSQL = "SELECT TOP 1 * " + System.Environment.NewLine;
        strSQL += " FROM carinsure " + System.Environment.NewLine;
        strSQL += " WHERE car_cod = '" + strCarCode + "' AND " + System.Environment.NewLine;
        strSQL += "     pol_typ = '" + strPolType + "' AND " + System.Environment.NewLine;
        strSQL += "     sum_insure = '" + strSumInsure.Replace(",","") + "' " + System.Environment.NewLine;
        strSQL += " ORDER BY start_dte desc ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getDataFactorCarGroup(string strCarCode, string strPolType, string strGroup)
    {
        DataSet ds;
        string strSQL = "";
        strSQL = "SELECT TOP 1 * " + System.Environment.NewLine;
        strSQL += " FROM cargroup " + System.Environment.NewLine;
        strSQL += " WHERE car_cod = '" + strCarCode + "' AND " + System.Environment.NewLine;
        strSQL += "     pol_typ = '" + strPolType + "' AND " + System.Environment.NewLine;
        strSQL += "     [group_cod] = '" + strGroup + "' " + System.Environment.NewLine;
        strSQL += " ORDER BY start_dte desc ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getDataTpbiTppd(string strCarCode, string strPolType, string strSumIns1, string strSumIns2, string strSumIns3)
    {
        DataSet ds;
        string strSQL = "";
        strSQL = "SELECT * FROM (" + System.Environment.NewLine;
        strSQL += "SELECT TOP 1 * " + System.Environment.NewLine;
        strSQL += " FROM covertp " + System.Environment.NewLine;
        strSQL += " WHERE car_cod = '" + strCarCode + "' AND " + System.Environment.NewLine;
        strSQL += "     pol_typ = '" + strPolType + "' AND " + System.Environment.NewLine;
        strSQL += "     sum_insure = '" + strSumIns1 + "' AND " + System.Environment.NewLine;
        strSQL += "     cover_typ = '1' " + System.Environment.NewLine;
        strSQL += " ORDER BY start_dte desc " + System.Environment.NewLine;
        strSQL += " UNION ";
        strSQL += "SELECT TOP 1 * " + System.Environment.NewLine;
        strSQL += " FROM covertp " + System.Environment.NewLine;
        strSQL += " WHERE car_cod = '" + strCarCode + "' AND " + System.Environment.NewLine;
        strSQL += "     pol_typ = '" + strPolType + "' AND " + System.Environment.NewLine;
        strSQL += "     sum_insure = '" + strSumIns2 + "' AND " + System.Environment.NewLine;
        strSQL += "     cover_typ = '2' " + System.Environment.NewLine;
        strSQL += " ORDER BY start_dte desc " + System.Environment.NewLine;
        strSQL += " UNION ";
        strSQL += "SELECT TOP 1 * " + System.Environment.NewLine;
        strSQL += " FROM covertp " + System.Environment.NewLine;
        strSQL += " WHERE car_cod = '" + strCarCode + "' AND " + System.Environment.NewLine;
        strSQL += "     pol_typ = '" + strPolType + "' AND " + System.Environment.NewLine;
        strSQL += "     sum_insure = '" + strSumIns3 + "' AND " + System.Environment.NewLine;
        strSQL += "     cover_typ = '3' " + System.Environment.NewLine;
        strSQL += " ORDER BY start_dte desc " + System.Environment.NewLine;
        strSQL += " ) TMP";        
        
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getDataCar01(string strCampaign, string strCarCode, string strPolType, string strCarsize, string strCarbody)
    {
        DataSet ds = new DataSet();
        string strWhereCarsize = "";

        if (strCarCode == "210")
            strWhereCarsize = strCarbody;            
        else
            strWhereCarsize = strCarsize;
        string strSQL = "";
        strSQL = "SELECT * FROM (" + System.Environment.NewLine;
        strSQL += "SELECT TOP 1 * " + System.Environment.NewLine;
        strSQL += " FROM tariff_car01_add " + System.Environment.NewLine;
        strSQL += " WHERE campaign = '" + strCampaign + "' AND " + System.Environment.NewLine;
        strSQL += "     car_cod = '" + strCarCode + "' AND " + System.Environment.NewLine;
        strSQL += "     pol_typ = '" + strPolType + "' AND " + System.Environment.NewLine;
        strSQL += "     car_size = '" + strWhereCarsize + "' AND " + System.Environment.NewLine;
        strSQL += "     add01_typ = 'D' " + System.Environment.NewLine;
        strSQL += " ORDER BY start_dte desc " + System.Environment.NewLine;
        strSQL += " UNION ";
        strSQL += "SELECT TOP 1 * " + System.Environment.NewLine;
        strSQL += " FROM tariff_car01_add " + System.Environment.NewLine;
        strSQL += " WHERE campaign = '" + strCampaign + "' AND " + System.Environment.NewLine;
        strSQL += "     car_cod = '" + strCarCode + "' AND " + System.Environment.NewLine;
        strSQL += "     pol_typ = '" + strPolType + "' AND " + System.Environment.NewLine;
        strSQL += "     car_size = '" + strWhereCarsize + "' AND " + System.Environment.NewLine;
        strSQL += "     add01_typ = 'M' " + System.Environment.NewLine;
        strSQL += " ORDER BY start_dte desc " + System.Environment.NewLine;
        strSQL += " ) TMP";
        try
        {
            ds = executeQuery(strSQL);
        }
        catch (Exception e)
        {
            TDS.Utility.MasterUtil.writeLog("SQL - getDataCar01", e.ToString());
            TDS.Utility.MasterUtil.writeLog("getDataCar01", strSQL);
        } 
        if (ds.Tables[0].Rows.Count < 1)
        {
            strSQL = "SELECT * FROM (" + System.Environment.NewLine;
            strSQL += "SELECT TOP 1 * " + System.Environment.NewLine;
            strSQL += " FROM tariff_car01 " + System.Environment.NewLine;
            strSQL += " WHERE campaign = '" + strCampaign + "' AND " + System.Environment.NewLine;
            strSQL += "     car_cod = '" + strCarCode + "' AND " + System.Environment.NewLine;
            strSQL += "     pol_typ = '" + strPolType + "' AND " + System.Environment.NewLine;
            strSQL += "     add01_typ = 'D' " + System.Environment.NewLine;
            strSQL += " ORDER BY start_dte desc " + System.Environment.NewLine;
            strSQL += " UNION ";
            strSQL += "SELECT TOP 1 * " + System.Environment.NewLine;
            strSQL += " FROM tariff_car01 " + System.Environment.NewLine;
            strSQL += " WHERE campaign = '" + strCampaign + "' AND " + System.Environment.NewLine;
            strSQL += "     car_cod = '" + strCarCode + "' AND " + System.Environment.NewLine;
            strSQL += "     pol_typ = '" + strPolType + "' AND " + System.Environment.NewLine;
            strSQL += "     add01_typ = 'M' " + System.Environment.NewLine;
            strSQL += " ORDER BY start_dte desc " + System.Environment.NewLine;
            strSQL += " ) TMP";
            ds = executeQuery(strSQL);
        }
        if (ds.Tables[0].Rows.Count < 1)
        {
            strSQL = "SELECT * FROM (" + System.Environment.NewLine;
            strSQL += "SELECT TOP 1 * " + System.Environment.NewLine;
            strSQL += " FROM tariff_car01 " + System.Environment.NewLine;
            strSQL += " WHERE campaign = '000' AND " + System.Environment.NewLine;
            strSQL += "     car_cod = '" + strCarCode + "' AND " + System.Environment.NewLine;
            strSQL += "     pol_typ = '" + strPolType + "' AND " + System.Environment.NewLine;
            strSQL += "     add01_typ = 'D' " + System.Environment.NewLine;
            strSQL += " ORDER BY start_dte desc " + System.Environment.NewLine;
            strSQL += " UNION ";
            strSQL += "SELECT TOP 1 * " + System.Environment.NewLine;
            strSQL += " FROM tariff_car01 " + System.Environment.NewLine;
            strSQL += " WHERE campaign = '000' AND " + System.Environment.NewLine;
            strSQL += "     car_cod = '" + strCarCode + "' AND " + System.Environment.NewLine;
            strSQL += "     pol_typ = '" + strPolType + "' AND " + System.Environment.NewLine;
            strSQL += "     add01_typ = 'M' " + System.Environment.NewLine;
            strSQL += " ORDER BY start_dte desc " + System.Environment.NewLine;
            strSQL += " ) TMP";
            ds = executeQuery(strSQL);
        }
        return ds;
    }
    public DataSet getDataCar02(string strCampaign, string strCarCode, string strPolType, string strCarsize, string strCarbody, string strSumIns)
    {
        DataSet ds;
        string strWhereCarsize = "";

        if (strCarCode == "210")
            strWhereCarsize = strCarbody;
        else
            strWhereCarsize = strCarsize;
        string strSQL = "";
        strSQL += "SELECT TOP 1 * " + System.Environment.NewLine;
        strSQL += " FROM tariff_car02_add " + System.Environment.NewLine;
        strSQL += " WHERE campaign = '" + strCampaign + "' AND " + System.Environment.NewLine;
        strSQL += "     car_cod = '" + strCarCode + "' AND " + System.Environment.NewLine;
        strSQL += "     pol_typ = '" + strPolType + "' AND " + System.Environment.NewLine;
        strSQL += "     car_size = '" + strWhereCarsize + "' AND " + System.Environment.NewLine;
        strSQL += "     insure = '" + strSumIns + "' " + System.Environment.NewLine;
        strSQL += " ORDER BY start_dte desc " + System.Environment.NewLine;        
        ds = executeQuery(strSQL);
        if (ds.Tables[0].Rows.Count < 1)
        {
            strSQL = "SELECT TOP 1 * " + System.Environment.NewLine;
            strSQL += " FROM tariff_car02 " + System.Environment.NewLine;
            strSQL += " WHERE campaign = '" + strCampaign + "' AND " + System.Environment.NewLine;
            strSQL += "     car_cod = '" + strCarCode + "' AND " + System.Environment.NewLine;
            strSQL += "     pol_typ = '" + strPolType + "' AND " + System.Environment.NewLine;
            strSQL += "     insure = '" + strSumIns + "' " + System.Environment.NewLine;
            strSQL += " ORDER BY start_dte desc " + System.Environment.NewLine;
            ds = executeQuery(strSQL);
        }
        if (ds.Tables[0].Rows.Count < 1)
        {
            strSQL = "SELECT TOP 1 * " + System.Environment.NewLine;
            strSQL += " FROM tariff_car02 " + System.Environment.NewLine;
            strSQL += " WHERE campaign = '000' AND " + System.Environment.NewLine;
            strSQL += "     car_cod = '" + strCarCode + "' AND " + System.Environment.NewLine;
            strSQL += "     pol_typ = '" + strPolType + "' AND " + System.Environment.NewLine;
            strSQL += "     insure = '" + strSumIns + "' " + System.Environment.NewLine;
            strSQL += " ORDER BY start_dte desc " + System.Environment.NewLine;
            ds = executeQuery(strSQL);
        }
        return ds;
    }
    public DataSet getDataCar03(string strCampaign, string strCarCode, string strPolType, string strCarsize, string strCarbody)
    {
        DataSet ds;
        string strWhereCarsize = "";

        if (strCarCode == "210")
            strWhereCarsize = strCarbody;
        else
            strWhereCarsize = strCarsize;
        string strSQL = "";
        strSQL += "SELECT TOP 1 * " + System.Environment.NewLine;
        strSQL += " FROM tariff_car03_add " + System.Environment.NewLine;
        strSQL += " WHERE campaign = '" + strCampaign + "' AND " + System.Environment.NewLine;
        strSQL += "     car_cod = '" + strCarCode + "' AND " + System.Environment.NewLine;
        strSQL += "     pol_typ = '" + strPolType + "' AND " + System.Environment.NewLine;
        strSQL += "     car_size = '" + strWhereCarsize + "' " + System.Environment.NewLine;
        strSQL += " ORDER BY start_dte desc " + System.Environment.NewLine;
        ds = executeQuery(strSQL);
        if (ds.Tables[0].Rows.Count < 1)
        {
            strSQL = "SELECT TOP 1 * " + System.Environment.NewLine;
            strSQL += " FROM tariff_car03 " + System.Environment.NewLine;
            strSQL += " WHERE campaign = '" + strCampaign + "' AND " + System.Environment.NewLine;
            strSQL += "     car_cod = '" + strCarCode + "' AND " + System.Environment.NewLine;
            strSQL += "     pol_typ = '" + strPolType + "'  " + System.Environment.NewLine;
            strSQL += " ORDER BY start_dte desc " + System.Environment.NewLine;
            ds = executeQuery(strSQL);
        }
        if (ds.Tables[0].Rows.Count < 1)
        {
            strSQL = "SELECT TOP 1 * " + System.Environment.NewLine;
            strSQL += " FROM tariff_car03 " + System.Environment.NewLine;
            strSQL += " WHERE campaign = '000' AND " + System.Environment.NewLine;
            strSQL += "     car_cod = '" + strCarCode + "' AND " + System.Environment.NewLine;
            strSQL += "     pol_typ = '" + strPolType + "'  " + System.Environment.NewLine;
            strSQL += " ORDER BY start_dte desc " + System.Environment.NewLine;
            ds = executeQuery(strSQL);
        }
        return ds;
    }
    public DataSet getDataReduceInsure(string strCampaign, string strCarCode,
                                        string strPolTyp, string strMotGar,
                                        string strCarAge, string strCarGroup,
                                        string strCarMak, string strCarSize,
                                        string strCarBody, string strAgtCod)
    {
        DataSet ds;
        string strSQL = "";
        if (strCarBody.Length > 1)
            strCarBody = strCarBody.Substring(1, 1);
        strSQL = "SELECT TOP 1 * " + System.Environment.NewLine;
        strSQL += " FROM reduce_insure " + System.Environment.NewLine;
        strSQL += " WHERE campaign = '" + strCampaign + "' AND " + System.Environment.NewLine;
        strSQL += "     car_cod = '" + strCarCode + "' AND " + System.Environment.NewLine;
        strSQL += "     pol_typ = '" + strPolTyp + "' AND " + System.Environment.NewLine;
        strSQL += "     mot_gar = '" + strMotGar + "' AND " + System.Environment.NewLine;
        strSQL += "     car_age = '" + strCarAge + "' AND " + System.Environment.NewLine;
        strSQL += "     car_group = '" + strCarGroup + "' AND " + System.Environment.NewLine;
        strSQL += "     car_mak = '" + strCarMak + "' AND " + System.Environment.NewLine;
        strSQL += "     car_size = '" + strCarSize + "' AND " + System.Environment.NewLine;
        strSQL += "     car_body = '" + strCarBody + "' AND " + System.Environment.NewLine;
        strSQL += "     agt_cod = '" + strAgtCod + "' AND " + System.Environment.NewLine;
        strSQL += "     clm_flag = '4'  " + System.Environment.NewLine;
        strSQL += " ORDER BY start_dte desc ";
        //if (strCampaign == "014" && )
        //{
        //    TDS.Utility.MasterUtil.writeLog("getDataReduceInsure", strSQL);
        //}
        ds = executeQuery(strSQL);
        if (ds.Tables[0].Rows.Count < 1)
        {
            strSQL = "SELECT TOP 1 * " + System.Environment.NewLine;
            strSQL += " FROM reduce_insure " + System.Environment.NewLine;
            strSQL += " WHERE campaign = '" + strCampaign + "' AND " + System.Environment.NewLine;
            strSQL += "     car_cod = '" + strCarCode + "' AND " + System.Environment.NewLine;
            strSQL += "     pol_typ = '" + strPolTyp + "' AND " + System.Environment.NewLine;
            strSQL += "     mot_gar = '" + strMotGar + "' AND " + System.Environment.NewLine;
            strSQL += "     car_age = '0' AND " + System.Environment.NewLine;
            strSQL += "     car_group = 'A' AND " + System.Environment.NewLine;
            strSQL += "     car_mak = 'A' AND " + System.Environment.NewLine;
            strSQL += "     car_size = '0' AND " + System.Environment.NewLine;
            strSQL += "     car_body = 'A' AND " + System.Environment.NewLine;
            strSQL += "     agt_cod = 'A' AND " + System.Environment.NewLine;
            strSQL += "     clm_flag = '4'  " + System.Environment.NewLine;
            strSQL += " ORDER BY start_dte desc ";
        }
        return ds;
    }
    public bool is3K(string strCampaign)
    {
        string strSQL = "";
        bool isRet = false;
        DataSet ds;
        strSQL = "SELECT * " + System.Environment.NewLine;
        strSQL += " FROM desc1 " + System.Environment.NewLine;
        strSQL += " WHERE desc_typ = 'VL' AND " + System.Environment.NewLine;
        strSQL += "     desc_tha1 like '%3K%' AND " + System.Environment.NewLine;
        strSQL += "     desc_cod = '" + strCampaign + "' ";
        ds = executeQuery(strSQL);
        if (ds.Tables[0].Rows.Count > 0)
            isRet = true;
        return isRet;
    }
    public DataSet getDataTariffRate(string strCampaign, string strCarCode,
                                        string strCarSize)
    {
        DataSet ds;
        string strSQL = "";
        strSQL = "SELECT TOP 1 * " + System.Environment.NewLine;
        strSQL += " FROM tariff_rate " + System.Environment.NewLine;
        strSQL += " WHERE campaign = '" + strCampaign + "' AND " + System.Environment.NewLine;
        strSQL += "     car_cod = '" + strCarCode + "' AND " + System.Environment.NewLine;
        strSQL += "     car_size = '" + strCarSize + "' " + System.Environment.NewLine;
        strSQL += " ORDER BY start_dte desc ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getTaxRate(string strStartDate)
    {
        DataSet ds = new DataSet();
        string strSQL = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();

        strSQL = "SELECT * " + System.Environment.NewLine;
        strSQL += " FROM tax_rate " + System.Environment.NewLine;
        strSQL += " WHERE '" + cmDate.convertDateStringForDBTH(strStartDate) + "' between start_date and end_date " + System.Environment.NewLine;
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getCompulPremium(string strCarCode, string strCarBody)
    {
        string strSQL = "";
        DataSet ds;
        strSQL = "SELECT TOP 1 * " + System.Environment.NewLine;
        strSQL += " FROM vol_com " + System.Environment.NewLine;
        strSQL += " WHERE car_cod = '" + strCarCode + "' " + System.Environment.NewLine;
        if (strCarCode == "210")
            strSQL += "     AND car_bod = '" + strCarBody + "' " + System.Environment.NewLine;
        else if (strCarCode == "320")
            strSQL += "     AND hi_vol = '4' " + System.Environment.NewLine;
        strSQL += " ORDER BY start_dte desc ";
        ds = executeQuery(strSQL);
        return ds;
    }
}
