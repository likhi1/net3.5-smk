﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using IBM.Data.Informix;


/// <summary>
/// Summary description for IntsmkManager
/// </summary>
public class IntsmkManager : TDS.BL.BLWorker_IFX_WS
{
	public IntsmkManager()
	{
		//
		// TODO: Add constructor logic here
		//
        strConnection = "strConInformix";
	}

    public bool insertPolicyVol(string dataBookNo, string dataInformNo, string dataRegisNo, DataSet dsData)
    {
        IfxTransaction trans;
        IfxConnection cnn;
        bool isRet = true;
        bool isResult;
        try
        {
			TDS.Utility.MasterUtil.writeLog("CalculateService :: AdminCalculatorManager","begin informix");
            cnn = getDbConnection();
            trans = cnn.BeginTransaction();
            try
            {
				TDS.Utility.MasterUtil.writeLog("CalculateService :: AdminCalculatorManager","call insertPolicy");
                //isResult = insertPolicy(dataBookNo, dataInformNo, dataRegisNo, dsData.Tables["REGISTER"], cnn, trans);
                TDS.Utility.MasterUtil.writeLog("CalculateService :: AdminCalculatorManager","pass insertPolicy");
                //if (isResult)
                //{
                isResult = insertVolTxtInsure(dataBookNo, dataInformNo, dataRegisNo, dsData, cnn, trans);
                //}
                //    isResult = insertRegister(dataRegisNo, dsData.Tables["REGISTER"], cnn, trans);
                //    if (isResult == false)
                //        throw new Exception("223");
                //}
                //if (isResult)
                //{
                //    isResult = insertSendDoc(dsData.Tables["SENDDOC"], cnn, trans);
                //    if (isResult == false)
                //        throw new Exception("224");
                //}
                //if (isResult)
                //{
                //    isResult = insertMtveh(dsData.Tables["MTVEH"], cnn, trans);
                //    if (isResult == false)
                //        throw new Exception("225");
                //}
                //if (isResult)
                //{
                //    isResult = insertVoluntary(dsData.Tables["VOLUNTARY"], cnn, trans);
                //    if (isResult == false)
                //        throw new Exception("226");

                //}
                if (isResult)
                    trans.Commit();
                else
                {
                    trans.Rollback();
                    isRet = false;
                }
            }
            catch (Exception ex)
            {
                trans.Rollback();
                isRet = false;
                throw ex;
            }
            finally
            {
                cnn.Close();
                cnn.Dispose();
            }
        }
        catch (Exception e)
        {
            isRet = false;
            throw e;
        }

        return isRet;
    }    
    public bool insertPolicyComp(string dataBookNo, string dataInformNo, string dataRegisNo, DataSet dsData)
    {
        //IfxTransaction trans;
        //IfxConnection cnn;
        bool isRet = true;
        //bool isResult;
        //cnn = getDbConnection();
        //trans = cnn.BeginTransaction();
        //try
        //{
        //    isResult = insertPolicy(dataBookNo, dataInformNo, dataRegisNo, dsData.Tables["REGISTER"], cnn, trans);
        //    if (isResult)
        //        isResult = insertRegister(dataRegisNo, dsData.Tables["REGISTER"], cnn, trans);
        //    if (isResult)
        //        isResult = insertSendDoc(dsData.Tables["SENDDOC"], cnn, trans);
        //    if (isResult)
        //        isResult = insertMtveh(dsData.Tables["MTVEH"], cnn, trans);
        //    if (isResult)
        //        isResult = insertCompulsary(dsData.Tables["COMPULSARY"], cnn, trans);
        //    if (isResult)
        //        trans.Commit();
        //    else
        //    {
        //        trans.Rollback();
        //        isRet = false;
        //    }
        //}
        //catch (Exception ex)
        //{
        //    trans.Rollback();
        //    isRet = false;
        //}
        //finally
        //{
        //    cnn.Close();
        //    cnn.Dispose();
        //}
        return isRet;
    }
    public bool insertPolicyPA(string dataBookNo, string dataInformNo, string dataRegisNo, DataSet dsData)
    {
        //IfxTransaction trans;
        //IfxConnection cnn;
        bool isRet = true;
        //bool isResult;
        //cnn = getDbConnection();
        //trans = cnn.BeginTransaction();
        //try
        //{
        //    TDS.Utility.MasterUtil.writeLog("CalculateService :: AdminCalculatorManager", "pass insertPolicyPA");
        //    isResult = insertPolicy(dataBookNo, dataInformNo, dataRegisNo, dsData.Tables["REGISTER"], cnn, trans);
        //    if (isResult)
        //        isResult = insertRegister(dataRegisNo, dsData.Tables["REGISTER"], cnn, trans);
        //    if (isResult)
        //        isResult = insertSendDoc(dsData.Tables["SENDDOC"], cnn, trans);
        //    if (isResult)
        //        isResult = insertPersonal(dsData.Tables["PERSONAL"], cnn, trans);
        //    if (isResult)
        //        isResult = insertPACover(dsData.Tables["PACOVER"], cnn, trans);
        //    if (isResult)
        //        trans.Commit();
        //    else
        //    {
        //        TDS.Utility.MasterUtil.writeLog("CalculateService :: AdminCalculatorManager", "pass insertPolicyPA");
        //        trans.Rollback();
        //        isRet = false;
        //    }
        //}
        //catch (Exception ex)
        //{
        //    trans.Rollback();
        //    isRet = false;
        //}
        //finally
        //{
        //    cnn.Close();
        //    cnn.Dispose();
        //}
        return isRet;
    }
    public bool insertPolicyTA(string dataBookNo, string dataInformNo, string dataRegisNo, DataSet dsData)
    {
        //IfxTransaction trans;
        //IfxConnection cnn;
        bool isRet = true;
        //bool isResult;
        //cnn = getDbConnection();
        //trans = cnn.BeginTransaction();
        //try
        //{
        //    isResult = insertPolicy(dataBookNo, dataInformNo, dataRegisNo, dsData.Tables["REGISTER"], cnn, trans);
        //    if (isResult)
        //        isResult = insertRegister(dataRegisNo, dsData.Tables["REGISTER"], cnn, trans);
        //    if (isResult)
        //        isResult = insertSendDoc(dsData.Tables["SENDDOC"], cnn, trans);
        //    if (isResult)
        //        isResult = insertPersonal(dsData.Tables["PERSONAL"], cnn, trans);
        //    if (isResult)
        //        isResult = insertTACover(dsData.Tables["TACOVER"], cnn, trans);
        //    if (isResult)
        //        trans.Commit();
        //    else
        //    {
        //        trans.Rollback();
        //        isRet = false;
        //    }
        //}
        //catch (Exception ex)
        //{
        //    trans.Rollback();
        //    isRet = false;
        //}
        //finally
        //{
        //    cnn.Close();
        //    cnn.Dispose();
        //}
        return isRet;
    }

    #region private method
    private bool insertPolicy(string dataBookNo, string dataInformNo, string dataRegisNo, DataTable dtData, IfxConnection cnn, IfxTransaction trans)
    {
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        bool isRet = true;
        string strSQL = "";
        try
        {
            strSQL = "INSERT INTO policy(pl_book_no, pl_inform_no, pl_regis_no, ";
            strSQL += "     pl_main_class, pl_pol_type, pl_effect_dt, ";
            strSQL += "     pl_expiry_dt, pl_regis_dt, pl_regis_time, pl_regis_type, ";
            strSQL += "     pl_ins_fname, pl_ins_lname, pl_ins_addr1, pl_ins_addr2, ";
            strSQL += "     pl_ins_amphor, pl_ins_changwat, pl_ins_postcode, pl_ins_tel, ";
            strSQL += "     pl_ins_email, pl_old_policy, pl_sum_ins, pl_prmm, pl_tax, ";
            strSQL += "     pl_stamp, pl_pmt_cd, pl_fleet_perc, pl_pay_type, pl_payment_stat, ";
            strSQL += "     pl_ref_bank, pl_stat, pl_approve_dt) ";
            strSQL += " VALUES(";
            strSQL += "     '" + dataBookNo + "', ";
            strSQL += "     '" + dataInformNo + "', ";
            strSQL += "     '" + dataRegisNo + "', ";
            strSQL += "     '" + dtData.Rows[0]["rg_main_class"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["rg_pol_type"].ToString() + "', ";
            strSQL += "     '" + convertToDateIFx(dtData.Rows[0]["tmp_effect_dt"].ToString()) + "', ";
            strSQL += "     '" + convertToDateIFx(dtData.Rows[0]["tmp_expiry_dt"].ToString()) + "', ";
            strSQL += "     '" + convertToDateIFx(dtData.Rows[0]["tmp_regis_dt"].ToString()) + "', ";
            strSQL += "     '" + dtData.Rows[0]["rg_regis_time"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["rg_regis_type"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["rg_ins_fname"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["rg_ins_lname"].ToString() + "', ";
            if (Convert.IsDBNull(dtData.Rows[0]["rg_ins_addr1"]))
            {
                strSQL += "     NULL, ";
            }
            else
            {
                strSQL += "     '" + dtData.Rows[0]["rg_ins_addr1"].ToString() + "', ";
            }
            strSQL += "     '" + dtData.Rows[0]["rg_ins_addr2"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["rg_ins_amphor"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["rg_ins_changwat"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["rg_ins_postcode"].ToString() + "', ";
            if (Convert.IsDBNull(dtData.Rows[0]["rg_ins_tel"]))
            {
                strSQL += "     NULL, ";
            }
            else
            {
                strSQL += "     '" + dtData.Rows[0]["rg_ins_tel"].ToString() + "', ";
            }
            if (Convert.IsDBNull(dtData.Rows[0]["rg_ins_email"]))
            {
                strSQL += "     NULL, ";
            }
            else
            {
                strSQL += "     '" + dtData.Rows[0]["rg_ins_email"].ToString() + "', ";
            }
            if (Convert.IsDBNull(dtData.Rows[0]["rg_old_policy"]))
            {
                strSQL += "     NULL, ";
            }
            else
            {
                strSQL += "     '" + dtData.Rows[0]["rg_old_policy"].ToString() + "', ";
            }
            strSQL += "     '" + dtData.Rows[0]["rg_sum_ins"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["rg_prmm"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["rg_tax"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["rg_stamp"].ToString() + "', ";
            if (Convert.IsDBNull(dtData.Rows[0]["rg_pmt_cd"]))
            {
                strSQL += "     NULL, ";
            }
            else
            {
                strSQL += "     '" + dtData.Rows[0]["rg_pmt_cd"].ToString() + "', ";
            }
            if (Convert.IsDBNull(dtData.Rows[0]["rg_fleet_perc"]))
            {
                strSQL += "     NULL, ";
            }
            else
            {
                strSQL += "     '" + dtData.Rows[0]["rg_fleet_perc"].ToString() + "', ";
            }
            strSQL += "     '" + dtData.Rows[0]["rg_pay_type"].ToString() + "', ";
            if (Convert.IsDBNull(dtData.Rows[0]["rg_payment_stat"]))
            {
                strSQL += "     NULL, ";
            }
            else
            {
                strSQL += "     '" + dtData.Rows[0]["rg_payment_stat"].ToString() + "', ";
            }
            if (Convert.IsDBNull(dtData.Rows[0]["rg_ref_bank"]))
            {
                strSQL += "     NULL, ";
            }
            else
            {
                strSQL += "     '" + dtData.Rows[0]["rg_ref_bank"].ToString() + "', ";
            }
            strSQL += "     'A', ";
            strSQL += "     '" + convertToDateIFx(DateTime.Now.ToString("dd/MM/yyyy")) + "') ";
            executeUpdate(strSQL, cnn, trans);
        }
        catch (Exception ex)
        {
            isRet = false;
            TDS.Utility.MasterUtil.writeLog("CalculateService :: AdminCalculatorManager - error on insertPolicy",ex.ToString());
        }
        return isRet;
    }

    private bool insertRegister(string dataRegisNo, DataTable dtData, IfxConnection cnn, IfxTransaction trans)
    {
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        bool isRet = true;
        int i = 0;
        string strSQL = "";
        try
        {
			TDS.Utility.MasterUtil.writeLog("CalculateService :: AdminCalculatorManager","1-1");
            strSQL = "INSERT INTO register(rg_regis_no, rg_main_class, rg_pol_type, ";
            strSQL += "     rg_effect_dt, rg_expiry_dt, rg_regis_dt, rg_regis_time, ";
            strSQL += "     rg_regis_type, rg_ins_fname, rg_ins_lname, rg_ins_addr1, ";
            strSQL += "     rg_ins_addr2, rg_ins_amphor, rg_ins_changwat, rg_ins_postcode, ";
            strSQL += "     rg_ins_tel, rg_ins_email, rg_old_policy, rg_sum_ins, rg_prmm, ";
            strSQL += "     rg_tax, rg_stamp, rg_pmt_cd, rg_fleet_perc, rg_pay_type, ";
            strSQL += "     rg_payment_stat, rg_ref_bank, rg_approve, rg_member_cd) ";
            strSQL += " VALUES(";
            strSQL += "     '" + dataRegisNo + "', ";
            strSQL += "     '" + dtData.Rows[0]["rg_main_class"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["rg_pol_type"].ToString() + "', ";
            strSQL += "     '" + convertToDateIFx(dtData.Rows[0]["tmp_effect_dt"].ToString()) + "', ";
            strSQL += "     '" + convertToDateIFx(dtData.Rows[0]["tmp_expiry_dt"].ToString()) + "', ";
            strSQL += "     '" + convertToDateIFx(dtData.Rows[0]["tmp_regis_dt"].ToString()) + "', ";
            strSQL += "     '" + dtData.Rows[0]["rg_regis_time"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["rg_regis_type"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["rg_ins_fname"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["rg_ins_lname"].ToString() + "', ";
            if (Convert.IsDBNull(dtData.Rows[0]["rg_ins_addr1"]))
            {
                strSQL += "     NULL, ";
            }
            else
            {
                strSQL += "     '" + dtData.Rows[0]["rg_ins_addr1"].ToString() + "', ";
            }
            strSQL += "     '" + dtData.Rows[0]["rg_ins_addr2"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["rg_ins_amphor"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["rg_ins_changwat"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["rg_ins_postcode"].ToString() + "', ";
            if (Convert.IsDBNull(dtData.Rows[0]["rg_ins_tel"]))
            {
                strSQL += "     NULL, ";
            }
            else
            {
                strSQL += "     '" + dtData.Rows[0]["rg_ins_tel"].ToString() + "', ";
            }
            if (Convert.IsDBNull(dtData.Rows[0]["rg_ins_email"]))
            {
                strSQL += "     NULL, ";
            }
            else
            {
                strSQL += "     '" + dtData.Rows[0]["rg_ins_email"].ToString() + "', ";
            }
            if (Convert.IsDBNull(dtData.Rows[0]["rg_old_policy"]))
            {
                strSQL += "     NULL, ";
            }
            else
            {
                strSQL += "     '" + dtData.Rows[0]["rg_old_policy"].ToString() + "', ";
            }
            strSQL += "     '" + dtData.Rows[0]["rg_sum_ins"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["rg_prmm"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["rg_tax"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["rg_stamp"].ToString() + "', ";
            if (Convert.IsDBNull(dtData.Rows[0]["rg_pmt_cd"]))
            {
                strSQL += "     NULL, ";
            }
            else
            {
                strSQL += "     '" + dtData.Rows[0]["rg_pmt_cd"].ToString() + "', ";
            }
            if (Convert.IsDBNull(dtData.Rows[0]["rg_fleet_perc"]))
            {
                strSQL += "     NULL, ";
            }
            else
            {
                strSQL += "     '" + dtData.Rows[0]["rg_fleet_perc"].ToString() + "', ";
            }
            strSQL += "     '" + dtData.Rows[0]["rg_pay_type"].ToString() + "', ";
            if (Convert.IsDBNull(dtData.Rows[0]["rg_payment_stat"]))
            {
                strSQL += "     NULL, ";
            }
            else
            {
                strSQL += "     '" + dtData.Rows[0]["rg_payment_stat"].ToString() + "', ";
            }
            if (Convert.IsDBNull(dtData.Rows[0]["rg_ref_bank"]))
            {
                strSQL += "     NULL, ";
            }
            else
            {
                strSQL += "     '" + dtData.Rows[0]["rg_ref_bank"].ToString() + "', ";
            }
            strSQL += "     'A', '') ";
            TDS.Utility.MasterUtil.writeLog("CalculateService :: AdminCalculatorManager",strSQL);
            executeUpdate(strSQL, cnn, trans);
            
        }
        catch (Exception ex)
        {
            isRet = false;
            TDS.Utility.MasterUtil.writeLog("CalculateService :: AdminCalculatorManager",ex.ToString());
            throw new Exception(strSQL);
        }
        return isRet;
    }

    private bool insertSendDoc(DataTable dtData, IfxConnection cnn, IfxTransaction trans)
    {
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        bool isRet = true;
        int i = 0;
        string strSQL = "";
        try
        {
            strSQL = "INSERT INTO senddoc(sed_regis_no, sed_branch, sed_name, ";
            strSQL += "     sed_ins_addr, sed_amphor, sed_changwat, sed_postcode, sed_tel) ";
            strSQL += " VALUES('" + dtData.Rows[0]["sed_regis_no"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["sed_branch"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["sed_name"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["sed_ins_addr"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["sed_amphor"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["sed_changwat"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["sed_postcode"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["sed_tel"].ToString() + "') ";
            executeUpdate(strSQL, cnn, trans);
        }
        catch (Exception ex)
        {
            isRet = false;
            TDS.Utility.MasterUtil.writeLog("CalculateService :: AdminCalculatorManager - error on insertSendDoc",ex.ToString());
        }
        return isRet;
    }

    private bool insertCompulsary(DataTable dtData, IfxConnection cnn, IfxTransaction trans)
    {
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        bool isRet = true;
        int i = 0;
        string strSQL = "";
        try
        {
            strSQL = "INSERT INTO compulsary(cp_regis_no, cp_veh_cd, cp_comp_prmm, ";
            strSQL += "     cp_comp_tax, cp_comp_stamp)";
            strSQL += " VALUES('" + dtData.Rows[0]["cp_regis_no"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["cp_veh_cd"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["cp_comp_prmm"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["cp_comp_tax"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["cp_comp_stamp"].ToString() + "') ";
            executeUpdate(strSQL, cnn, trans);
        }
        catch (Exception ex)
        {
            isRet = false;
        }
        return isRet;
    }

    private bool insertVoluntary(DataTable dtData, IfxConnection cnn, IfxTransaction trans)
    {
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        bool isRet = true;
        int i = 0;
        string strSQL = "";
        try
        {
            strSQL = "INSERT INTO voluntary(vl_regis_no, vl_veh_cd, vl_drv_flag, ";
            strSQL += "     vl_drv1, vl_birth_drv1, vl_drv2, vl_birth_drv2, vl_tpbi_person, ";
            strSQL += "     vl_tpbi_time, vl_tppd_time, vl_tppd_exc, vl_od_time, vl_od_exc, ";
            strSQL += "     vl_f_t, vl_01_11, vl_01_121, vl_01_122, vl_01_21, vl_01_211, ";
            strSQL += "     vl_01_212, vl_02_person, vl_02, vl_03, vl_repair, vl_accessory, ";
            strSQL += "     vl_fleet_perc, vl_prmm )";
            strSQL += " VALUES('" + dtData.Rows[0]["vl_regis_no"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["vl_veh_cd"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["vl_drv_flag"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["vl_drv1"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["vl_birth_drv1"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["vl_drv2"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["vl_birth_drv2"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["vl_tpbi_person"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["vl_tpbi_time"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["vl_tppd_time"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["vl_tppd_exc"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["vl_od_time"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["vl_od_exc"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["vl_f_t"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["vl_01_11"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["vl_01_121"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["vl_01_122"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["vl_01_21"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["vl_01_211"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["vl_01_212"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["vl_02_person"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["vl_02"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["vl_03"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["vl_repair"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["vl_accessory"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["vl_fleet_perc"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["vl_prmm"].ToString() + "') ";
            executeUpdate(strSQL, cnn, trans);
        }
        catch (Exception ex)
        {
            isRet = false;
        }
        return isRet;
    }

    private bool insertMtveh(DataTable dtData, IfxConnection cnn, IfxTransaction trans)
    {
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        bool isRet = true;
        int i = 0;
        string strSQL = "";
        try
        {
            strSQL = "INSERT INTO mtveh(mv_regis_no, mv_major_cd, mv_minor_cd, ";
            strSQL += "     mv_veh_year, mv_veh_cc, mv_veh_seat, mv_veh_weight, ";
            strSQL += "     mv_license_no, mv_license_area, mv_engin_no, mv_chas_no, ";
            strSQL += "     mv_combine, mv_no_claim, mv_no_claim_perc)";
            strSQL += " VALUES('" + dtData.Rows[0]["mv_regis_no"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["mv_major_cd"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["mv_minor_cd"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["mv_veh_year"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["mv_veh_cc"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["mv_veh_seat"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["mv_veh_weight"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["mv_license_no"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["mv_license_area"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["mv_engin_no"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["mv_chas_no"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["mv_combine"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["mv_no_claim"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["mv_no_claim_perc"].ToString() + "') ";
            executeUpdate(strSQL, cnn, trans);
        }
        catch (Exception ex)
        {
            isRet = false;
        }
        return isRet;
    }

    private bool insertVolTxtInsure(string dataBookNo, string dataInformNo, string dataRegisNo,DataSet ds_data, IfxConnection cnn, IfxTransaction trans)
    {
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        bool isRet = true;
        int i = 0;
        string custName = "";
        string add1 = "";
        string add2 = "";
        string strSQL = "";
        string effDate = "";
        string tyea = "0";
        
        try
        {
            effDate = ds_data.Tables["REGISTER"].Rows[i]["rg_effect_dt"].ToString().Trim();
            //TDS.Utility.MasterUtil.writeLog("insertVolTxtInsure :: effDate", effDate);
            tyea = FormatStringApp.FormatDate(effDate);
            //TDS.Utility.MasterUtil.writeLog("insertVolTxtInsure :: effDate2", tyea);
            tyea = tyea.Substring(8,2);            
            custName = Convert.ToString(ds_data.Tables["REGISTER"].Rows[i]["rg_ins_fname"].ToString().Trim() + " " + ds_data.Tables["REGISTER"].Rows[i]["rg_ins_lname"].ToString().Trim());
            add1 = Convert.ToString(ds_data.Tables["REGISTER"].Rows[i]["rg_ins_addr1"].ToString().Trim() + " " + ds_data.Tables["REGISTER"].Rows[i]["rg_ins_addr2"].ToString().Trim());
            add2 = Convert.ToString(ds_data.Tables["REGISTER"].Rows[i]["rg_ins_amphor"].ToString().Trim() + " " + ds_data.Tables["REGISTER"].Rows[i]["rg_ins_changwat"].ToString().Trim());
            strSQL = "Insert into txt_insured " +
                                             "(agt_cod,period,pol_yea,file_nam,cust_id,not_dte,car_cod," +
                                             " vol_insure,fire_insure,pre_net,pre_grs,pre_tax,pre_stm,eff_fdte,eff_tdte," +
                                             " deduct,chasis,engine,car_mod,lic_prov_desc," +
                                             " license,car_size,car_nam,car_mak,car_mak_desc,car_weigh,car_seat," +
                                             " not_nam,mot_gar,drv_nam1," +
                                             " drv_birth1,drv_nam2,drv_birth2," +
                                             " cust_name,cust_addr1,cust_addr2,cust_zip,cust_phone,rec_name,rec_addr1,rec_add2, " +
                                             " campaign,mot_typ,not_id,not_no,sys_dte)" +
                                     " Values ('0100001','1','" + tyea + "'," +
                                     "'" + dataRegisNo + "'," +
                                     "'" + ds_data.Tables["REGISTER"].Rows[i]["rg_ins_idcard"].ToString().Trim() + "'," +
                                     "'" + convertToDateIFx(ds_data.Tables["REGISTER"].Rows[i]["rg_regis_dt"].ToString().Trim()) + "'," +
                                     "'" + ds_data.Tables["VOLUNTARY"].Rows[i]["vl_veh_cd"].ToString().Trim() + "'," +
                                     "'" + ds_data.Tables["REGISTER"].Rows[i]["rg_sum_ins"].ToString().Trim() + "'," +//vol_insure
                                     "'" + ds_data.Tables["REGISTER"].Rows[i]["rg_sum_ins"].ToString().Trim() + "'," +//fire_insure
                                     "'" + ds_data.Tables["REGISTER"].Rows[i]["rg_prmm"].ToString().Trim() + "'," +//pre_net
                                     "'" + ds_data.Tables["REGISTER"].Rows[i]["rg_prmmgross"].ToString().Trim() + "'," +//pre_grs
                                     "'" + ds_data.Tables["REGISTER"].Rows[i]["rg_tax"].ToString().Trim() + "'," +//pre_tax
                                     "'" + ds_data.Tables["REGISTER"].Rows[i]["rg_stamp"].ToString().Trim() + "'," +//pre_stm
                                     "'" + convertToDateIFx(effDate) + "'," +//eff_fdte
                                     "'" + convertToDateIFx(ds_data.Tables["REGISTER"].Rows[i]["rg_expiry_dt"].ToString().Trim()) + "'," +//eff_tdte
                                     "'" + ds_data.Tables["VOLUNTARY"].Rows[i]["vl_od_exc"].ToString().Trim() + "'," +//deduct
                                     "'" + ds_data.Tables["MTVEH"].Rows[i]["mv_chas_no"].ToString().Trim() + "'," +//chasis
                                     "'" + ds_data.Tables["MTVEH"].Rows[i]["mv_engin_no"].ToString().Trim() + "'," +//engine
                                     "'" + ds_data.Tables["MTVEH"].Rows[i]["mv_veh_year"].ToString().Trim() + "'," +//car_mod
                                     "'" + ds_data.Tables["MTVEH"].Rows[i]["mv_license_area"].ToString().Trim() + "'," +//lic_prov_desc
                                      "'" + ds_data.Tables["MTVEH"].Rows[i]["mv_license_no"].ToString().Trim() + "'," +//license
                                     "'" + ds_data.Tables["MTVEH"].Rows[i]["mv_veh_cc"].ToString().Trim() + "'," +//car_size
                                     "'" + ds_data.Tables["MTVEH"].Rows[i]["mv_major_cd"].ToString().Trim() + "'," +//car_nam
                                     "'" + ds_data.Tables["MTVEH"].Rows[i]["mv_minor_cd"].ToString().Trim() + "'," +//car_mak
                                     "'" + ds_data.Tables["MTVEH"].Rows[i]["mv_minor_cd"].ToString().Trim() + "'," +//car_mak_desc
                                     "'" + ds_data.Tables["MTVEH"].Rows[i]["mv_veh_weight"].ToString().Trim() + "'," +//car_weigh
                                     "'" + ds_data.Tables["MTVEH"].Rows[i]["mv_veh_seat"].ToString().Trim() + "'," +//car_seat
                                     "'Internet'," +//not_nam
                                     "'" + ds_data.Tables["VOLUNTARY"].Rows[i]["vl_repair"].ToString().Trim() + "'," +//mot_gar
                                     "'" + ds_data.Tables["VOLUNTARY"].Rows[i]["vl_drv1"].ToString().Trim() + "'," +//drv_nam1
                                     "'" + convertToDateIFx(ds_data.Tables["VOLUNTARY"].Rows[i]["vl_birth_drv1"].ToString().Trim()) + "'," +//drv_birth1
                                     "'" + ds_data.Tables["VOLUNTARY"].Rows[i]["vl_drv2"].ToString().Trim() + "'," +//drv_nam2
                                     "'" + convertToDateIFx(ds_data.Tables["VOLUNTARY"].Rows[i]["vl_birth_drv2"].ToString().Trim()) + "'," +//drv_birth2                                    
                                     "'" + custName + "'," +//cust_name
                                     "'" + add1 + "'," +//cust_addr1
                                     "'" + add2 + "'," +//cust_addr2
                                     "'" + ds_data.Tables["REGISTER"].Rows[i]["rg_ins_postcode"].ToString().Trim() + "'," +//cust_zip
                                     "'" + ds_data.Tables["REGISTER"].Rows[i]["rg_ins_tel"].ToString().Trim() + "'," +//cust_phone
                                     "'" + ds_data.Tables["senddoc"].Rows[i]["sed_name"].ToString().Trim() + "'," +//rec_name
                                     "'" + ds_data.Tables["senddoc"].Rows[i]["sed_ins_addr"].ToString().Trim() + "'," +//rec_addr1
                                     "'" + ds_data.Tables["senddoc"].Rows[i]["sed_amphor"].ToString().Trim() + "'," +//rec_addr2
                                     "'" + ds_data.Tables["VOLUNTARY"].Rows[i]["vl_campaign"].ToString().Trim() + "'," +//campaign
                                     "'" + ds_data.Tables["REGISTER"].Rows[i]["rg_pol_type"].ToString().Trim() + "'," +//mot_typ
                                     "'" + dataBookNo + "'," +//not_id
                                     "'" + dataInformNo + "',Today )";//not_no,sys_dte
                                     TDS.Utility.MasterUtil.writeLog("insertVolTxtInsure :: AdminInsertVolTxtInsure",strSQL);
                                     executeUpdate(strSQL, cnn, trans);
        }
        catch (Exception ex)
        {
            isRet = false;
            TDS.Utility.MasterUtil.writeLog("insertVolTxtInsure :: AdminInsertVolTxtInsure", ex.ToString());
        }
        return isRet;
    }

    private bool insertPersonal(DataTable dtData, IfxConnection cnn, IfxTransaction trans)
    {
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        bool isRet = true;
        int i = 0;
        string strSQL = "";
        try
        {
            strSQL = "INSERT INTO personal(pe_regis_no, pe_eff_time, pe_card_type, ";
            strSQL += "     pe_card_no, pe_birth_dt, pe_occup, pe_ben_fname, ";
            strSQL += "     pe_ben_lname, pe_ben_addr1, pe_ben_addr2, pe_ben_amphor, ";
            strSQL += "     pe_ben_changwat, pe_ben_postcode, pe_ben_tel, pe_ben_email, ";
            strSQL += "     pe_relation) ";
            strSQL += " VALUES('" + dtData.Rows[0]["pe_regis_no"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pe_eff_time"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pe_card_type"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pe_card_no"].ToString() + "', ";
            strSQL += "     '" + convertToDateIFx(dtData.Rows[0]["tmp_birth_dt"].ToString()) + "', ";
            strSQL += "     '" + dtData.Rows[0]["pe_occup"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pe_ben_fname"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pe_ben_lname"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pe_ben_addr1"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pe_ben_addr2"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pe_ben_amphor"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pe_ben_changwat"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pe_ben_postcode"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pe_ben_tel"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pe_ben_email"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pe_relation"].ToString() + "') ";
            executeUpdate(strSQL, cnn, trans);
        }
        catch (Exception ex)
        {
            isRet = false;
        }
        return isRet;
    }

    private bool insertPACover(DataTable dtData, IfxConnection cnn, IfxTransaction trans)
    {
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        bool isRet = true;
        int i = 0;
        string strSQL = "";
        try
        {
            strSQL = "INSERT INTO pa_cover(pa_regis_no, pa_type, pa_death_si, ";
            strSQL += "     pa_death_exc, pa_death_prmm, pa_ttd_si, pa_ttd_week, ";
            strSQL += "     pa_ttd_exc, pa_ttd_prmm, pa_ptd_si, pa_ptd_week, pa_ptd_exc, ";
            strSQL += "     pa_ptd_prmm, pa_med_si, pa_med_exc, pa_med_prmm, pa_war, ";
            strSQL += "     pa_str, pa_spt, pa_mtr, pa_air, pa_murder, pa_add_prmm, ";
            strSQL += "     pa_disc_prmm, pa_net_prmm )";
            strSQL += " VALUES('" + dtData.Rows[0]["pa_regis_no"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pa_type"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pa_death_si"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pa_death_exc"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pa_death_prmm"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pa_ttd_si"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pa_ttd_week"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pa_ttd_exc"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pa_ttd_prmm"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pa_ptd_si"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pa_ptd_week"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pa_ptd_exc"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pa_ptd_prmm"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pa_med_si"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pa_med_exc"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pa_med_prmm"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pa_war"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pa_str"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pa_spt"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pa_mtr"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pa_air"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pa_murder"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pa_add_prmm"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pa_disc_prmm"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["pa_net_prmm"].ToString() + "') ";
            executeUpdate(strSQL, cnn, trans);
        }
        catch (Exception ex)
        {
            isRet = false;
        }
        return isRet;
    }

    private bool insertTACover(DataTable dtData, IfxConnection cnn, IfxTransaction trans)
    {
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        bool isRet = true;
        int i = 0;
        string strSQL = "";
        try
        {
            strSQL = "INSERT INTO ta_cover(ta_regis_no, ta_tot_ins, ta_sum_ins, ";
            strSQL += "     ta_medic_exp, ta_prmm)";
            strSQL += " VALUES('" + dtData.Rows[0]["ta_regis_no"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["ta_tot_ins"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["ta_sum_ins"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["ta_medic_exp"].ToString() + "', ";
            strSQL += "     '" + dtData.Rows[0]["ta_prmm"].ToString() + "') ";
            executeUpdate(strSQL, cnn, trans);
        }
        catch (Exception ex)
        {
            isRet = false;
        }
        return isRet;
    }
    public DataSet testConnection()
    {
        return executeQuery("SELECT * FROM POLICY");
    }

    private string convertToDateIFx(string dataDate)
    {
        string strRet = "";
        if(dataDate != ""){
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();       
        string[] arrDateTime;
        string[] arrDate;
        arrDateTime = dataDate.Split(' ');
        if (arrDateTime.Length > 0)
        {
            arrDate = arrDateTime[0].Split('/');
            if (Convert.ToInt32(arrDate[2]) > 2500)
            {
                arrDateTime[0] = cmDate.convertDateStringForDBTH(arrDateTime[0]);
                arrDate = arrDateTime[0].Split('/');
            }
            else
            {
                arrDateTime[0] = cmDate.convertDateStringForDB(arrDateTime[0]);
                arrDate = arrDateTime[0].Split('/');
            }
            // MM/dd/yyyy
            //strRet = arrDate[0] + "/" + arrDate[1] + "/" + arrDate[2]; 
            strRet = arrDate[1] + "/" + arrDate[2] + "/" + arrDate[0];
        }
        }
        return strRet;
    }

    #endregion
}
