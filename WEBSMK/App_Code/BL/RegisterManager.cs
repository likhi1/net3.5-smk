﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using TDSLib.Libs.net.mail;
using System.Net.Mail;

/// <summary>
/// Summary description for RegisterManager
/// </summary>
public class RegisterManager : TDS.BL.BLWorker_MSSQL
{
	public RegisterManager()
	{
		//
		// TODO: Add constructor logic here
		//
        strConnection = "strConnLocal";
	}

    public string insertTmpVoluntary(DataSet dsData)
    {
        bool isSave = true;
        string strKey;
        string strSQL = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        DateTime dtExpire;
        SqlConnection cnn;
        SqlTransaction trans;
        DataSet dsVol2Comp;
        cnn = getDbConnection();
        trans = cnn.BeginTransaction();
        try
        {
            // gen key
            strKey = getNextNo("TM", DateTime.Now.Year.ToString("0000").Substring(2, 2), DateTime.Now.Month.ToString("00"), DateTime.Now.Day.ToString("00"), cnn, trans);
            // insert into tmpreg        
            isSave = insertTmpRegister(strKey, dsData, cnn, trans);
            // insert into tmpvolun
            if (isSave)
                isSave = insertTmpVolun(strKey, dsData, cnn, trans);
            else
            {
                throw new Exception("Error when call insertTmpRegister");
            }
            // insert into tmpmtveh
            if (isSave)
                isSave = insertTmpMtVeh(strKey, dsData, cnn, trans);
            else
            {
                throw new Exception("Error when call insertTmpVolun");
            }
            // ถ้า เลือก รวมพ.ร.บ. ต้อง insert into tmpcomp
            if (isSave)
            {
                if (dsData.Tables[0].Rows[0]["mv_combine"].ToString() == "Y")
                {
                    isSave = insertTmpCompul(strKey, dsData, cnn, trans);
                }
            }
            else
            {
                throw new Exception("Error when call insertTmpMtVeh");
            }
            if (isSave)
            {
                trans.Commit();
            }
            else
            {
                throw new Exception("Error when call insertTmpCompul");
            }
        }
        catch (Exception e)
        {
            TDS.Utility.MasterUtil.writeError("insertTmp - error", e.ToString());
            //TDS.Utility.MasterUtil.writeError("insertTmp - strSQL", strSQL);
            trans.Rollback();
            strKey = "";
        }

        return strKey;
    }    
    public string insertDataVoluntary(DataSet dsData)
    {
        string strSQL = "";
        string strKey = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        bool isRet = true;
        bool isSave = true;
        string strTmpDt;

        SqlConnection cnn;
        SqlTransaction trans;
        cnn = getDbConnection();
        trans = cnn.BeginTransaction();
        try
        {
            if (Convert.IsDBNull(dsData.Tables[0].Rows[0]["rg_regis_no"]) ||
                dsData.Tables[0].Rows[0]["rg_regis_no"].ToString() == "")
            {
                // gen key regis_no
                strKey = getNextNo("V", "", "", "", cnn, trans);
                
                // insert register
                isSave = insertDataRegister(strKey, dsData, cnn, trans);
                // insert into voluntary
                if (isSave)
                    isSave = insertDataVolun(strKey, dsData, cnn, trans);
                else
                {
                    throw new Exception("Error when call insertDataVolun");
                }
                
                
                // insert mtveh
                if (isSave)
                    isSave = insertDataMtveh(strKey, dsData, cnn, trans);
                else
                {
                    throw new Exception("Error when call insertDataMtveh");
                }

                // insert compulsary
                // ถ้า เลือก รวมพ.ร.บ. ต้อง insert into tmpcomp
                if (isSave)
                {
                    if (dsData.Tables[0].Rows[0]["mv_combine"].ToString() == "Y")
                    {
                        isSave = insertDataCompul(strKey, dsData, cnn, trans);
                    }
                }
                else
                {
                    throw new Exception("Error when call insertDataCompul");
                }
                if (isSave)
                {
                    // UPDATE TMPREG tm_regis_no 
                    strSQL = "UPDATE TMPREG ";
                    strSQL += " SET tm_regis_no ='" + strKey + "' ";
                    strSQL += " WHERE tm_tmp_cd = '" + dsData.Tables[0].Rows[0]["tm_tmp_cd"].ToString() + "' ";
                    executeUpdate(strSQL, cnn, trans);
                    trans.Commit();
                }
                else
                {
                    throw new Exception("Error when call insertTmpCompul");
                }
            }
        }
        catch (Exception e)
        {
            trans.Rollback();
            isRet = false;
            strKey = "";
            TDS.Utility.MasterUtil.writeLog("SMK :: RegisterManager.insertDataVolunatry", e.Message);
            TDS.Utility.MasterUtil.writeLog("SQL Command", strSQL);
        }
        finally
        {
            trans.Dispose();
            cnn.Close();
            cnn.Dispose();
        }
        return strKey;
    }    
    public string insertTmpCompulsary(DataSet dsData)
    {
        string strKey;
        string strSQL;
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        DateTime dtExpire;
        SqlConnection cnn;
        SqlTransaction trans;
        bool isSave = true;
        cnn = getDbConnection();
        trans = cnn.BeginTransaction();
        try
        {
            // gen key
            strKey = getNextNo("TM", DateTime.Now.Year.ToString("0000").Substring(2, 2), DateTime.Now.Month.ToString("00"), DateTime.Now.Day.ToString("00"), cnn, trans);
            // insert into tmpreg        
            isSave = insertTmpRegister(strKey, dsData, cnn, trans);
            // insert into tmpcompul
            if (isSave)
                isSave = insertTmpCompul(strKey, dsData, cnn, trans);
            else
            {
                throw new Exception("Error when call insertTmpCompul");
            }
            // insert into tmpmtveh
            if (isSave)
                isSave = insertTmpMtVeh(strKey, dsData, cnn, trans);
            else
            {
                throw new Exception("Error when call insertTmpCompul");
            }
            if (isSave)
            {
                trans.Commit();
            }
            else
            {
                throw new Exception("Error when call insertTmpMtVeh");
            }
        }
        catch (Exception e)
        {
            trans.Rollback();
            strKey = "";
        }
        finally
        {
            trans.Dispose();
            cnn.Close();
            cnn.Dispose();
        }

        return strKey;
    }
    public string insertDataCompulsary(DataSet dsData)
    {
        string strSQL = "";
        string strKey = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        bool isRet = true;
        string strTmpDt;
        SqlConnection cnn;
        SqlTransaction trans;
        bool isSave = true;
        cnn = getDbConnection();
        trans = cnn.BeginTransaction();
        try
        {
            if (Convert.IsDBNull(dsData.Tables[0].Rows[0]["rg_regis_no"]) ||
                dsData.Tables[0].Rows[0]["rg_regis_no"].ToString() == "")
            {
                // gen key regis_no
                strKey = getNextNo(dsData.Tables[0].Rows[0]["rg_pol_type"].ToString(), "", "", "", cnn, trans);                
                // insert register
                isSave = insertDataRegister(strKey, dsData, cnn, trans);
                // insert into compul
                if (isSave)
                    isSave = insertDataCompul(strKey, dsData, cnn, trans);
                else
                {
                    throw new Exception("Error when call insertDataCompul");
                }
                // insert mtveh
                if (isSave)
                    isSave = insertDataMtveh(strKey, dsData, cnn, trans);
                else
                {
                    throw new Exception("Error when call insertDataMtveh");
                }
                if (isSave)
                {
                    // UPDATE TMPREG tm_regis_no 
                    strSQL = "UPDATE TMPREG ";
                    strSQL += " SET tm_regis_no ='" + strKey + "' ";
                    strSQL += " WHERE tm_tmp_cd = '" + dsData.Tables[0].Rows[0]["tm_tmp_cd"].ToString() + "' ";
                    executeUpdate(strSQL, cnn, trans);
                    trans.Commit();
                }
                else
                {
                    throw new Exception("Error when call insertDataMtveh");
                }
            }
        }
        catch (Exception e)
        {
            trans.Rollback();
            strKey = "";
            isRet = false;
            TDS.Utility.MasterUtil.writeLog("SMK :: RegisterManager.insertDataCompulsary", e.Message);
            TDS.Utility.MasterUtil.writeLog("SQL Command", strSQL);
        }
        finally
        {
            trans.Dispose();
            cnn.Close();
            cnn.Dispose();
        }
        return strKey;
    }
    public string insertTmpTravelAccident(DataSet dsData)
    {
        string strKey;
        string strSQL = "";
        string strTmpDt = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        DateTime dtExpire;
        SqlConnection cnn;
        SqlTransaction trans;
        bool isSave = true;
        cnn = getDbConnection();
        trans = cnn.BeginTransaction();
        try
        {
            // gen key
            strKey = getNextNo("TM", DateTime.Now.Year.ToString("0000").Substring(2, 2), DateTime.Now.Month.ToString("00"), DateTime.Now.Day.ToString("00"), cnn, trans);
            // insert into tmpreg        
            isSave = insertTmpRegister(strKey, dsData, cnn, trans);
            // insert into tmpPersonel
            if (isSave)
                isSave = insertTmpPersonel(strKey, dsData, cnn, trans);
            else
            {
                throw new Exception("Error when call insertTmpRegister");
            }
            // insert into tmpta_cover
            if (isSave)
                isSave = insertTmpCover(strKey, dsData, cnn, trans);
            else
            {
                throw new Exception("Error when call insertTmpPersonel");
            }
            if (isSave)
            {
                trans.Commit();
            }
            else
            {
                throw new Exception("Error when call insertTmpCover");
            }
        }
        catch (Exception e)
        {
            trans.Rollback();
            strKey = "";
            TDS.Utility.MasterUtil.writeLog("SMK :: RegisterManager.insertTmpTravelAccident", e.Message);
            TDS.Utility.MasterUtil.writeLog("RegisterManager.insertTmpTravelAccident :: SQL", strSQL);
        }
        finally
        {
            trans.Dispose();
            cnn.Close();
            cnn.Dispose();
        }

        return strKey;
    }
    public string insertDataTravelAccident(DataSet dsData)
    {
        string strSQL = "";
        string strKey = "";
        bool isRet = true;
        string strTmpDt = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        SqlConnection cnn;
        SqlTransaction trans;
        bool isSave = true;
        cnn = getDbConnection();
        trans = cnn.BeginTransaction();
        try
        {
            if (Convert.IsDBNull(dsData.Tables[0].Rows[0]["rg_regis_no"]) ||
                dsData.Tables[0].Rows[0]["rg_regis_no"].ToString() == "")
            {
                // gen key regis_no
                strKey = getNextNo(dsData.Tables[0].Rows[0]["rg_pol_type"].ToString(), "", "", "", cnn, trans);
                // insert register
                isSave = insertDataRegister(strKey, dsData, cnn, trans);
                // insert personal
                if (isSave)
                    isSave = insertDataPersonel(strKey, dsData, cnn, trans);
                else
                {
                    throw new Exception("Error when call insertDataRegister");
                }
                // insert ta_cover
                if (isSave)
                    isSave = insertDataCover(strKey, dsData, cnn, trans);
                else
                {
                    throw new Exception("Error when call insertDataPersonel");
                }
                if (isSave)
                {
                    // UPDATE TMPREG tm_regis_no 
                    strSQL = "UPDATE TMPREG ";
                    strSQL += " SET tm_regis_no ='" + strKey + "' ";
                    strSQL += " WHERE tm_tmp_cd = '" + dsData.Tables[0].Rows[0]["tm_tmp_cd"].ToString() + "' ";
                    executeUpdate(strSQL, cnn, trans);
                    trans.Commit();
                }
                else
                {
                    throw new Exception("Error when call insertDataCover");
                }
            }
        }
        catch (Exception e)
        {
            trans.Rollback();
            isRet = false;
            strKey = "";
            TDS.Utility.MasterUtil.writeLog("SMK :: RegisTAManager", e.Message);
            TDS.Utility.MasterUtil.writeLog("RegisTAManager :: SQL", strSQL);
        }
        finally
        {
            trans.Dispose();
            cnn.Close();
            cnn.Dispose();
        }
        return strKey;
    }
    public string insertTmpPersonelAccident(DataSet dsData)
    {
        string strKey;
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        SqlConnection cnn;
        SqlTransaction trans;
        bool isSave = true;
        cnn = getDbConnection();
        trans = cnn.BeginTransaction();
        try
        {
            // gen key
            strKey = getNextNo("TM", DateTime.Now.Year.ToString("0000").Substring(2, 2), DateTime.Now.Month.ToString("00"), DateTime.Now.Day.ToString("00"), cnn, trans);
            // insert into tmpreg        
            isSave = insertTmpRegister(strKey, dsData, cnn, trans);
            // insert personal
            if (isSave)
                isSave = insertTmpPersonel(strKey, dsData, cnn, trans);
            else
            {
                throw new Exception("Error when call insertDataRegister");
            }            
            // insert into tmppa_cover
            if (isSave)
                isSave = insertTmpPACover(strKey, dsData, cnn, trans);
            else
            {
                throw new Exception("Error when call insertTmpPersonel");
            }
            if (isSave)
            {
                trans.Commit();
            }
            else
            {
                throw new Exception("Error when call insertTmpPACover");
            }
        }
        catch (Exception e)
        {
            trans.Rollback();
            strKey = "";
        }

        return strKey;
    }
    public string insertDataPersonelAccident(DataSet dsData)
    {
        string strSQL = "";
        string strKey = "";
        bool isRet = true;
        string strTmpDt = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        SqlConnection cnn;
        SqlTransaction trans;
        bool isSave = true;
        cnn = getDbConnection();
        trans = cnn.BeginTransaction();
        try
        {
            //if (Convert.IsDBNull(dsData.Tables[0].Rows[0]["rg_regis_no"]) ||
            //    dsData.Tables[0].Rows[0]["rg_regis_no"].ToString() == "")
            //{
                // gen key regis_no
                strKey = getNextNo(dsData.Tables[0].Rows[0]["rg_pol_type"].ToString(), "", "", "", cnn, trans);
                isSave = insertDataRegister(strKey, dsData, cnn, trans);
                // insert personal
                if (isSave)
                    isSave = insertDataPersonel(strKey, dsData, cnn, trans);
                else
                {
                    throw new Exception("Error when call insertDataRegister");
                }
                // insert pa_cover
                if (isSave)
                    isSave = insertDataPACover(strKey, dsData, cnn, trans);
                else
                {
                    throw new Exception("Error when call insertDataPersonel");
                }
                if (isSave)
                {
                    // UPDATE TMPREG tm_regis_no 
                    strSQL = "UPDATE TMPREG ";
                    strSQL += " SET tm_regis_no ='" + strKey + "' ";
                    strSQL += " WHERE tm_tmp_cd = '" + dsData.Tables[0].Rows[0]["tm_tmp_cd"].ToString() + "' ";
                    executeUpdate(strSQL, cnn, trans);
                    trans.Commit();
                }
                else
                {
                    throw new Exception("Error when call insertDataPACover");
                }
            //}
        }
        catch (Exception e)
        {
            trans.Rollback();
            isRet = false;
            strKey = "";
            TDS.Utility.MasterUtil.writeLog("SMK :: RegisManager.insertDataPersonelAccident", e.Message);
            TDS.Utility.MasterUtil.writeLog("RegisManager.insertDataPersonelAccident :: SQL", strSQL);
        }
        finally
        {
            trans.Dispose();
            cnn.Close();
            cnn.Dispose();
        }
        return strKey;
    }    
    public bool insertPaidData(DataSet dsData)
    {
        bool isRet = true;
        string strKey;
        string strSQL = "";
        SqlConnection cnn;
        SqlTransaction trans;
        DataSet dsRegister;
        DataSet dsCustAddr;
        DataSet dsSenddoc;
        DataSet dsSettle;
        string strSettle;
        double dTotalPrmm = 0;
        OnlineVoluntaryManager cmOnline = new OnlineVoluntaryManager();
        cnn = getDbConnection();
        trans = cnn.BeginTransaction();
        try
        {
            dsCustAddr = new DataSet();
            strSQL = "SELECT * FROM tmpreg ";
            strSQL += " WHERE tm_tmp_cd = '" + dsData.Tables[0].Rows[0]["tm_tmp_cd"] + "'";
            dsRegister = executeQuery(strSQL, cnn, trans);
            strKey = dsRegister.Tables[0].Rows[0]["tm_regis_no"].ToString();

            // update table tmpreg
            strSQL = "UPDATE register ";
            strSQL += " SET rg_pay_type = '" + dsData.Tables[0].Rows[0]["rg_pay_type"].ToString() + "'";
            strSQL += " WHERE rg_regis_no = '" + strKey + "'";
            executeUpdate(strSQL, cnn, trans);

            // insert into senddoc
            if (dsData.Tables[0].Rows[0]["sed_at"].ToString() == "O")
            {
                dsCustAddr = getCustAddress(dsData.Tables[0].Rows[0]["tm_tmp_cd"].ToString(), cnn, trans);
                if (dsCustAddr.Tables.Count > 0 && dsCustAddr.Tables[0].Rows.Count > 0)
                {
                    dsData.Tables[0].Rows[0]["sed_name"] = dsCustAddr.Tables[0].Rows[0]["name"];
                    dsData.Tables[0].Rows[0]["sed_ins_addr"] = dsCustAddr.Tables[0].Rows[0]["address"];
                    dsData.Tables[0].Rows[0]["sed_amphor"] = dsCustAddr.Tables[0].Rows[0]["district"];
                    dsData.Tables[0].Rows[0]["sed_changwat"] = dsCustAddr.Tables[0].Rows[0]["province"];
                    dsData.Tables[0].Rows[0]["sed_postcode"] = dsCustAddr.Tables[0].Rows[0]["post_code"];
                    dsData.Tables[0].Rows[0]["sed_tel"] = dsCustAddr.Tables[0].Rows[0]["tel_no"];
                }
            }
            // ตรวจสอบการมีอยู่ของข้อมูล senddoc
            strSQL = "SELECT * FROM senddoc WHERE sed_regis_no = '" + strKey + "'";
            dsSenddoc = executeQuery(strSQL, cnn, trans);
            if (dsSenddoc.Tables.Count > 0 && dsSenddoc.Tables[0].Rows.Count > 0)
            {
            }
            else
            {
                strSQL = "INSERT INTO senddoc(sed_regis_no, sed_branch, sed_name, ";
                strSQL += " sed_ins_addr, sed_amphor, sed_changwat, sed_postcode, sed_tel)";
                strSQL += " VALUES('" + strKey + "', ";
                strSQL += "     '" + dsData.Tables[0].Rows[0]["sed_branch"].ToString() + "', ";
                strSQL += "     '" + dsData.Tables[0].Rows[0]["sed_name"].ToString() + "', ";
                strSQL += "     '" + dsData.Tables[0].Rows[0]["sed_ins_addr"].ToString() + "', ";
                strSQL += "     '" + dsData.Tables[0].Rows[0]["sed_amphor"].ToString() + "', ";
                strSQL += "     '" + dsData.Tables[0].Rows[0]["sed_changwat"].ToString() + "', ";
                strSQL += "     '" + dsData.Tables[0].Rows[0]["sed_postcode"].ToString() + "', ";
                strSQL += "     '" + dsData.Tables[0].Rows[0]["sed_tel"].ToString() + "')";
                executeUpdate(strSQL, cnn, trans);
            }
            // ถ้า paid_at = credit ให้ insert into credit_post
            if (dsData.Tables[0].Rows[0]["rg_pay_type"].ToString() == "5")
            {
                // หาค่า tmpreg
                if (dsCustAddr.Tables.Count < 1)
                {
                    dsCustAddr = getCustAddress(dsData.Tables[0].Rows[0]["tm_tmp_cd"].ToString(), cnn, trans);
                }
                // หาค่า  settle_flag
                strSQL = "SELECT ds_desc ";
                strSQL += " FROM setcode ";
                strSQL += " WHERE ds_major_cd = 'CD'";
                strSQL += "     AND ds_minor_cd = '" + dsData.Tables[0].Rows[0]["rg_pol_type"].ToString() + "'";
                dsSettle = executeQuery(strSQL, cnn, trans);
                if (dsSettle.Tables.Count > 0 && dsSettle.Tables[0].Rows.Count > 0)
                {
                    strSettle = dsSettle.Tables[0].Rows[0]["ds_desc"].ToString();
                }
                else
                {
                    strSettle = "";
                }
                dTotalPrmm = Convert.ToDouble(dsData.Tables[0].Rows[0]["rg_prmm"].ToString());
                dTotalPrmm = dTotalPrmm + Convert.ToDouble(dsData.Tables[0].Rows[0]["rg_tax"].ToString());
                dTotalPrmm = dTotalPrmm + Convert.ToDouble(dsData.Tables[0].Rows[0]["rg_stamp"].ToString());
                strSQL = "INSERT INTO credit_post(cp_mid, cp_terminal, cp_ref_no, cp_ref_date, ";
                strSQL += "     cp_cur_abbr, cp_amount, cp_cust_lname, cp_cust_fname, ";
                strSQL += "     cp_cust_email, cp_cust_country, cp_cust_addr1, cp_cust_addr2, ";
                strSQL += "     cp_cust_city, cp_cust_province, cp_cust_zip, cp_settle_flag, ";
                strSQL += "     cp_stat )";
                strSQL += " VALUES(";
                strSQL += "		'401001666477001', ";
                strSQL += "		'74400101', ";
                //strSQL += "		'" + dsData.Tables[0].Rows[0]["mid"].ToString() + "', ";
                //strSQL += "     '" + dsData.Tables[0].Rows[0]["terminal"].ToString() + "', ";                
                strSQL += "     '" + strKey + "', ";
                //strSQL += "     '" + dsData.Tables[0].Rows[0]["ref_date"].ToString() + "', ";
                strSQL += "    '" + DateTime.Now.Year.ToString("00") + DateTime.Now.ToString("MMyyhhmm") + "', ";
                strSQL += "     'BHT', ";
                strSQL += "     '" + dTotalPrmm + "', ";
                strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_ins_lname"].ToString() + "', ";
                strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_ins_fname"].ToString() + "', ";
                strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_ins_email"].ToString() + "', ";
                strSQL += "     'TH', ";
                strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_ins_addr1"].ToString() + "', ";
                strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_ins_addr2"].ToString() + "', ";
                strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_ins_amphor"].ToString() + "', ";
                strSQL += "     '" + cmOnline.getDataProvinceByCode(dsData.Tables[0].Rows[0]["rg_ins_changwat"].ToString()).Tables[0].Rows[0]["ds_desc"].ToString().Trim() + "', ";
                strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_ins_postcode"].ToString() + "', ";
                strSQL += "     '" + strSettle + "', ";
                strSQL += "     'N') ";
                executeUpdate(strSQL, cnn, trans);
            }
            trans.Commit();
        }
        catch (Exception e)
        {
            trans.Rollback();
            strKey = "";
            isRet = false;
            TDS.Utility.MasterUtil.writeLog("SMK :: RegisterManager.insertPaidData", e.ToString());
            TDS.Utility.MasterUtil.writeLog("RegisterManager.insertPaidData :: SQL ", strSQL);
        }
        finally
        {
            trans.Dispose();
            cnn.Close();
            cnn.Dispose();
        }

        return isRet;
    }
    public string insertDataContact(DataSet dsData)
    {
        string strSQL = "";
        string strKey = "";

        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        SqlConnection cnn;
        SqlTransaction trans;
        cnn = getDbConnection();
        trans = cnn.BeginTransaction();
        try
        {
            // gen key
            strKey = getNextNo("B", DateTime.Now.Year.ToString("0000").Substring(2, 2), DateTime.Now.Month.ToString("00"), DateTime.Now.Day.ToString("00"), cnn, trans);
            // insert into contact 
            strSQL = "INSERT INTO contact(ct_code, ct_name, ct_telephone, ct_email, ct_con_time, ";
            strSQL += " ct_motor, ct_fire, ct_marine, ct_pa, ct_health, ct_cancer, ct_other, ct_detail, ct_regis_dt, ";
            strSQL += " ct_cont_stat)";
            strSQL += " VALUES(";
            strSQL += "     '" + strKey + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["name"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["tel_no"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["email"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["contact_time"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["f_motor"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["f_fire"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["f_sea"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["f_pa"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["f_health"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["f_cancer"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["f_other"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["remark"].ToString() + "', ";
            strSQL += "     '" + cmDate.convertDateStringForDBTH(DateTime.Now.ToString("dd/MM/yyyy")) + "', ";
            strSQL += "     'O' ";
            strSQL += " )";
            executeUpdate(strSQL, cnn, trans);
            trans.Commit();
        }
        catch (Exception e)
        {
            trans.Rollback();
            strKey = "ERROR :: can not insert data";
        }
        try
        {
            sendMail(dsData);
        }
        catch (Exception e)
        {
            strKey = "ERROR :: can not send email to SMK";
        }
        return strKey;
    }
    public bool updateCreditResult(string dataRegisNo, string dataResult)
    {
        string strSQL = "";
        bool isRet = true;
        //if (dataResult.ToLower() == "approved")
        //    dataResult = "A";
        //else if (dataResult.ToLower() == "canceled")
        //    dataResult = "C";
        //else if (dataResult.ToLower() == "error")
        //    dataResult = "E";
        //else if (dataResult.ToLower() == "inprocess")
        //    dataResult = "I";
        //else if (dataResult.ToLower() == "declined")
        //    dataResult = "D";
        //else if (dataResult.ToLower() == "outofservice")
        //    dataResult = "O";
        strSQL = "UPDATE CREDIT_POST SET cp_stat = '" + dataResult + "' ";
        strSQL += " WHERE cp_ref_no = '" + dataRegisNo + "' ";
        try
        {
            executeUpdate(strSQL);
            TDS.Utility.MasterUtil.writeLog("RegisterManager.updateCreditResult :: SQL ", strSQL);
        }
        catch (Exception e)
        {
            isRet = false;
            TDS.Utility.MasterUtil.writeLog("SMK :: RegisterManager.updateCreditResult", e.ToString());
            TDS.Utility.MasterUtil.writeLog("RegisterManager.updateCreditResult :: SQL ", strSQL);
        }
        return isRet;

    }    

    private bool insertTmpRegister(string strKey, DataSet dsData, SqlConnection conn, SqlTransaction trans)
    {
        bool isRet = true;
        string strSQL = "";
        DateTime dtExpire;
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        try
        {
            strSQL = "INSERT INTO tmpreg(tm_tmp_cd, tm_main_class, tm_pol_type, tm_effect_dt, tm_expiry_dt, ";
            strSQL += "tm_regis_dt, tm_regis_time, tm_regis_type, tm_ins_fname, tm_ins_lname, ";
            strSQL += "tm_ins_addr1, tm_ins_addr2, tm_ins_amphor, tm_ins_changwat, tm_ins_postcode, ";
            strSQL += "tm_ins_tel, tm_ins_email, tm_sum_ins, tm_prmm, tm_tax, tm_stamp, tm_fleet_perc, tm_stat, ";
            strSQL += "tm_ins_mobile, tm_ins_idcard, tm_oth_distance, tm_oth_region, tm_oth_flagdeduct, ";
            strSQL += "tm_oth_oldpolicy, tm_mott_id, tm_insc_id, tm_prmmgross )";
            strSQL += " VALUES('" + strKey + "','M', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_pol_type"].ToString() + "', ";
            if (dsData.Tables[0].Rows[0]["rg_effect_dt"].ToString() == "" ||
                dsData.Tables[0].Rows[0]["rg_effect_dt"].ToString() == "__/__/____")
            {
                strSQL += " null, ";
                strSQL += " null, ";
            }
            else
            {
                strSQL += "     '" + cmDate.convertDateStringForDBTH(dsData.Tables[0].Rows[0]["rg_effect_dt"].ToString()) + "',";
                dtExpire = Convert.ToDateTime(cmDate.convertDateStringForDBTH(dsData.Tables[0].Rows[0]["rg_effect_dt"].ToString()));
                dtExpire = dtExpire.AddYears(1);
                strSQL += "     '" + cmDate.convertDateStringForDBTH(dtExpire.ToString("dd/MM/yyyy")) + "',";
            }
            strSQL += "     '" + cmDate.convertDateStringForDB(TDS.Utility.MasterUtil.getDateString()) + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_regis_time"].ToString() + "', ";
            strSQL += "     'N', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_ins_fname"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_ins_lname"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_ins_addr1"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_ins_addr2"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_ins_amphor"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_ins_changwat"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_ins_postcode"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_ins_tel"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_ins_email"].ToString() + "', ";
            strSQL += "     '" + (dsData.Tables[0].Rows[0]["rg_sum_ins"].ToString() == "" ? "0" : dsData.Tables[0].Rows[0]["rg_sum_ins"].ToString().Replace(",", "")) + "', ";
            strSQL += "     '" + (dsData.Tables[0].Rows[0]["rg_prmm"].ToString() == "" ? "0" : dsData.Tables[0].Rows[0]["rg_prmm"].ToString().Replace(",", "")) + "', ";
            strSQL += "     '" + (dsData.Tables[0].Rows[0]["rg_tax"].ToString() == "" ? "0" : dsData.Tables[0].Rows[0]["rg_tax"].ToString().Replace(",", "")) + "', ";
            strSQL += "     '" + (dsData.Tables[0].Rows[0]["rg_stamp"].ToString() == "" ? "0" : dsData.Tables[0].Rows[0]["rg_stamp"].ToString().Replace(",", "")) + "', ";
            strSQL += "     '" + (dsData.Tables[0].Rows[0]["rg_fleet_perc"].ToString() == "" ? "0" : dsData.Tables[0].Rows[0]["rg_fleet_perc"].ToString().Replace(",", "")) + "', ";
            strSQL += "     'N', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_ins_mobile"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_ins_idcard"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["oth_distance"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["oth_region"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["oth_flagdeduct"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["oth_oldpolicy"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["mott_id"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["insc_id"].ToString() + "', ";
            strSQL += "     '" + (dsData.Tables[0].Rows[0]["rg_prmmgross"].ToString() == "" ? "0" : dsData.Tables[0].Rows[0]["rg_prmmgross"].ToString().Replace(",", "")) + "' ";
            strSQL += ") ";
            executeUpdate(strSQL, conn, trans);
        }
        catch (Exception e)
        {
            isRet = false;
            TDS.Utility.MasterUtil.writeError("insertTmpRegister - e.ToString()", e.ToString());
            TDS.Utility.MasterUtil.writeError("insertTmpRegister - strSQL", strSQL);
        }
        return isRet;
    }
    private bool insertTmpVolun(string strKey, DataSet dsData, SqlConnection conn, SqlTransaction trans)
    {
        bool isRet = true;
        string strSQL = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        try
        {
            strSQL = " INSERT INTO tmpvolun(tvl_tmp_cd, tvl_veh_cd, tvl_drv_flag, tvl_drv1, tvl_birth_drv1, ";
            strSQL += "     tvl_drv2, tvl_birth_drv2, tvl_tpbi_person, tvl_tpbi_time, ";
            strSQL += "     tvl_tppd_time, tvl_tppd_exc, tvl_od_time, tvl_od_exc, tvl_f_t, ";
            strSQL += "     tvl_01_11, tvl_01_121, tvl_01_122, tvl_01_21, tvl_01_211, tvl_01_212, ";
            strSQL += "     tvl_02_person, tvl_02, tvl_03, tvl_repair, tvl_accessory, ";
            strSQL += "     tvl_fleet_perc, tvl_prmm, tvl_campaign )";
            strSQL += " VALUES ('" + strKey + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["vl_veh_cd"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["vl_drv_flag"].ToString() + "', ";
            if (dsData.Tables[0].Rows[0]["vl_drv_flag"].ToString() == "Y")
            {
                strSQL += "     '" + dsData.Tables[0].Rows[0]["vl_drv1"].ToString() + "', ";
                if (dsData.Tables[0].Rows[0]["vl_birth_drv1"].ToString() == "" ||
                    dsData.Tables[0].Rows[0]["vl_birth_drv1"].ToString() == "__/__/____")
                    strSQL += "     null, ";
                else
                    strSQL += "     '" + cmDate.convertDateStringForDBTH(dsData.Tables[0].Rows[0]["vl_birth_drv1"].ToString()) + "', ";                
                strSQL += "     '" + dsData.Tables[0].Rows[0]["vl_drv2"].ToString() + "', ";
                if (dsData.Tables[0].Rows[0]["vl_birth_drv2"].ToString() == "" ||
                    dsData.Tables[0].Rows[0]["vl_birth_drv2"].ToString() == "__/__/____")
                    strSQL += "     null, ";
                else
                    strSQL += "     '" + cmDate.convertDateStringForDBTH(dsData.Tables[0].Rows[0]["vl_birth_drv2"].ToString()) + "', ";
            }
            else
            {
                strSQL += " '', ";
                strSQL += " null, ";
                strSQL += " '', ";
                strSQL += " null, ";
            }
            strSQL += "     '0',";
            strSQL += "     '0',";
            strSQL += "     '0',";
            strSQL += "     '0',";
            strSQL += "     '0',";
            strSQL += "     '0',";
            strSQL += "     '0',";
            strSQL += "     '0',";
            strSQL += "     '0',";
            strSQL += "     '0',";
            strSQL += "     '0',";
            strSQL += "     '0',";
            strSQL += "     '0',";
            strSQL += "     '0',";
            strSQL += "     '0',";
            strSQL += "     '0',";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["vl_repair"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["vl_accessory"].ToString() + "', ";
            strSQL += "     '0',";
            strSQL += "     '0',";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["vl_campaign"].ToString() + "') ";
            executeUpdate(strSQL, conn, trans);
        }
        catch (Exception e)
        {
            isRet = false;
        }
        return isRet;
    }
    private bool insertTmpMtVeh(string strKey, DataSet dsData, SqlConnection conn, SqlTransaction trans)
    {
        bool isRet = true;
        string strSQL = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        try
        {
            strSQL = " INSERT INTO tmpmtveh(tv_tmp_cd, tv_major_cd, tv_minor_cd, ";
            strSQL += "     tv_veh_year, tv_veh_cc, tv_veh_seat, tv_veh_weight, ";
            strSQL += "     tv_license_no, tv_license_area, tv_engin_no, tv_chas_no, ";
            strSQL += "     tv_combine)";
            strSQL += " VALUES('" + strKey + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["mv_major_cd"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["mv_minor_cd"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["mv_veh_year"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["mv_veh_cc"].ToString().Replace(",", "") + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["mv_veh_seat"].ToString().Replace(",", "") + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["mv_veh_weight"].ToString().Replace(",", "") + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["mv_license_no"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["mv_license_area"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["mv_engin_no"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["mv_chas_no"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["mv_combine"].ToString() + "') ";
            executeUpdate(strSQL, conn, trans);
        }
        catch (Exception e)
        {
            isRet = false;
        }
        return isRet;
    }
    private bool insertTmpCompul(string strKey, DataSet dsData, SqlConnection conn, SqlTransaction trans)
    {
        bool isRet = true;
        string strSQL = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        try
        {
            strSQL = " INSERT INTO tmpcompul(tcp_tmp_cd, tcp_veh_cd, ";
            strSQL += "     tcp_comp_prmm, tcp_comp_tax, tcp_comp_stamp)";
            strSQL += " VALUES ('" + strKey + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["cp_veh_cd"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["cp_comp_prmm"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["cp_comp_tax"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["cp_comp_stamp"].ToString() + "') ";
            executeUpdate(strSQL, conn, trans);
        }
        catch (Exception e)
        {
            isRet = false;
        }
        return isRet;
    }
    private bool insertTmpPersonel(string strKey, DataSet dsData, SqlConnection conn, SqlTransaction trans)
    {
        bool isRet = true;
        string strSQL = "";
        DataSet dsCustAddr;
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        try
        {
            strSQL = " INSERT INTO tmppersonal (tpe_tmp_no, tpe_eff_time, ";
            strSQL += "     tpe_ben_fname, tpe_ben_lname, tpe_ben_addr1, tpe_ben_addr2, ";
            strSQL += "     tpe_ben_amphor, tpe_ben_changwat, tpe_ben_postcode, tpe_ben_tel, ";
            strSQL += "     tpe_ben_email, tpe_relation)";
            strSQL += " VALUES ('" + strKey + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pe_eff_time"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pe_ben_fname"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pe_ben_lname"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pe_ben_addr1"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pe_ben_addr2"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pe_ben_amphor"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pe_ben_changwat"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pe_ben_postcode"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pe_ben_tel"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pe_ben_email"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pe_relation"].ToString() + "') ";
            executeUpdate(strSQL, conn, trans);
        }
        catch (Exception e)
        {
            isRet = false;
        }
        return isRet;
    }    
    private bool insertTmpCover(string strKey, DataSet dsData, SqlConnection conn, SqlTransaction trans)
    {
        bool isRet = true;
        string strSQL = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        try
        {
            strSQL = " INSERT INTO tmpta_cover(tta_tmp_no, tta_tot_ins, tta_sum_ins, ";
            strSQL += "     tta_medic_exp, tta_prmm)";
            strSQL += " VALUES('" + strKey + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["ta_tot_ins"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["ta_sum_ins"].ToString().Replace(",", "") + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["ta_medic_exp"].ToString().Replace(",", "") + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["ta_prmm"].ToString().Replace(",", "") + "') ";
            executeUpdate(strSQL, conn, trans);
        }
        catch (Exception e)
        {
            isRet = false;
        }
        return isRet;
    }
    private bool insertTmpPACover(string strKey, DataSet dsData, SqlConnection conn, SqlTransaction trans)
    {
        bool isRet = true;
        string strSQL = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        try
        {
            strSQL = " INSERT INTO tmppa_cover(tpa_tmp_no, tpa_type, tpa_death_si, tpa_death_exc,";
            strSQL += "     tpa_death_prmm, tpa_ttd_si, tpa_ttd_week, tpa_ttd_exc, tpa_ttd_prmm, ";
            strSQL += "     tpa_ptd_si, tpa_ptd_exc, tpa_ptd_week, tpa_ptd_prmm, tpa_med_si, ";
            strSQL += "     tpa_med_exc, tpa_med_prmm, ";
            strSQL += "     tpa_war, tpa_str, tpa_spt, tpa_mtr, tpa_air, tpa_murder, ";
            strSQL += "     tpa_add_prmm, tpa_disc_prmm, tpa_net_prmm)";
            strSQL += " VALUES('" + strKey + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pa_type"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pa_death_si"].ToString().Replace(",", "") + "', ";
            strSQL += "     '0', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pa_death_prmm"].ToString().Replace(",", "") + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pa_ttd_si"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pa_ttd_week"].ToString() + "', ";
            strSQL += "     '0', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pa_ttd_prmm"].ToString().Replace(",", "") + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pa_ptd_si"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pa_ptd_week"].ToString() + "', ";
            strSQL += "     '0', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pa_ptd_prmm"].ToString().Replace(",", "") + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pa_med_si"].ToString() + "', ";
            strSQL += "     '0', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pa_med_prmm"].ToString().Replace(",", "") + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pa_war"].ToString().Replace(",", "") + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pa_str"].ToString().Replace(",", "") + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pa_spt"].ToString().Replace(",", "") + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pa_mtr"].ToString().Replace(",", "") + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pa_air"].ToString().Replace(",", "") + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pa_murder"].ToString().Replace(",", "") + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pa_add_prmm"].ToString().Replace(",", "") + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pa_disc_prmm"].ToString().Replace(",", "") + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["pa_net_prmm"].ToString().Replace(",", "") + "') ";
            executeUpdate(strSQL, conn, trans);
        }
        catch (Exception e)
        {
            isRet = false;
        }
        return isRet;
    }
    
    private bool insertDataRegister(string strKey, DataSet dsData, SqlConnection conn, SqlTransaction trans)
    {
        bool isRet = true;
        string strSQL = "";
        string strTmpDt = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        try
        {
            strSQL = " INSERT INTO register(rg_regis_no, rg_member_cd, rg_member_ref, ";
            strSQL += " rg_main_class, rg_pol_type, rg_effect_dt, rg_expiry_dt, ";
            strSQL += " rg_regis_dt, rg_regis_time, rg_regis_type, rg_ins_fname, ";
            strSQL += " rg_ins_lname, rg_ins_addr1, rg_ins_addr2, rg_ins_amphor, ";
            strSQL += " rg_ins_changwat, rg_ins_postcode, rg_ins_tel, rg_ins_email, ";
            strSQL += " rg_old_policy, rg_sum_ins, rg_prmm, rg_tax, rg_stamp, ";
            strSQL += " rg_fleet_perc, rg_pay_type, rg_payment_stat, rg_approve, rg_prmmgross, ";
            strSQL += " rg_ins_mobile, rg_ins_idcard, rg_oth_distance, rg_oth_region, rg_oth_flagdeduct, ";
            strSQL += " rg_oth_oldpolicy, rg_mott_id, rg_insc_id)";
            strSQL += " VALUES(";
            strSQL += "     '" + strKey + "' , ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_member_cd"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_member_ref"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_main_class"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_pol_type"].ToString() + "', ";
            strTmpDt = dsData.Tables[0].Rows[0]["rg_effect_dt"].ToString();
            //strTmpDt = strTmpDt.Substring(0, strTmpDt.IndexOf(" "));
            //strTmpDt = "29/11/2550";
            try
            {
                strSQL += "     '" + cmDate.convertDateStringForDBTH(strTmpDt) + "', ";
            }
            catch (Exception exx)
            {
                strSQL += "     '" + cmDate.convertDateStringForDBTH(TDS.Utility.MasterUtil.getDateString(Convert.ToDateTime(strTmpDt), "dd/MM/yyyy", "th-TH")) + "', ";
            }
            //strSQL += "     '" + cmDate.convertDateStringForDBTH(strTmpDt) + "', ";
            strTmpDt = dsData.Tables[0].Rows[0]["rg_expiry_dt"].ToString();
            //strTmpDt = strTmpDt.Substring(0, strTmpDt.IndexOf(" "));
            //strSQL += "     '" + cmDate.convertDateStringForDBTH(strTmpDt) + "', ";
            try
            {
                strSQL += "     '" + cmDate.convertDateStringForDBTH(strTmpDt) + "', ";
            }
            catch (Exception exx)
            {
                strSQL += "     '" + cmDate.convertDateStringForDBTH(TDS.Utility.MasterUtil.getDateString(Convert.ToDateTime(strTmpDt), "dd/MM/yyyy", "th-TH")) + "', ";
            }
            strTmpDt = dsData.Tables[0].Rows[0]["rg_regis_dt"].ToString();
            //strTmpDt = strTmpDt.Substring(0, strTmpDt.IndexOf(" "));
            //strSQL += "     '" + cmDate.convertDateStringForDBTH(strTmpDt) + "', ";
            try
            {
                strSQL += "     '" + cmDate.convertDateStringForDBTH(strTmpDt) + "', ";
            }
            catch (Exception exx)
            {
                strSQL += "     '" + cmDate.convertDateStringForDBTH(TDS.Utility.MasterUtil.getDateString(Convert.ToDateTime(strTmpDt), "dd/MM/yyyy", "th-TH")) + "', ";
            }
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_regis_time"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_regis_type"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_ins_fname"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_ins_lname"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_ins_addr1"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_ins_addr2"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_ins_amphor"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_ins_changwat"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_ins_postcode"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_ins_tel"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_ins_email"].ToString() + "', ";
            strSQL += "     '', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_sum_ins"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_prmm"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_tax"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_stamp"].ToString() + "', ";
            strSQL += "     '" + (dsData.Tables[0].Rows[0]["rg_fleet_perc"].ToString() == "" ? "0" : dsData.Tables[0].Rows[0]["rg_fleet_perc"].ToString()) + "', ";
            //strSQL += "     '" + dsData.Tables[0].Rows[0]["paid_at"].ToString() + "', ";
            strSQL += "     null, ";
            strSQL += "     'N', ";
            strSQL += "     'O',";
            strSQL += "     '" + (dsData.Tables[0].Rows[0]["rg_prmmgross"].ToString() == "" ? "0" : dsData.Tables[0].Rows[0]["rg_prmmgross"].ToString().Replace(",", "")) + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_ins_mobile"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["rg_ins_idcard"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["oth_distance"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["oth_region"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["oth_flagdeduct"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["oth_oldpolicy"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["mott_id"].ToString() + "', ";
            strSQL += "     '" + dsData.Tables[0].Rows[0]["insc_id"].ToString() + "' ";
            strSQL += " ) ";
            executeUpdate(strSQL, conn, trans);
        }
        catch (Exception e)
        {
            isRet = false;
        }
        return isRet;
    }
    private bool insertDataVolun(string strKey, DataSet dsData, SqlConnection conn, SqlTransaction trans)
    {
        bool isRet = true;
        string strSQL = "";
        string strTmpDt = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        try
        {
            // insert voluntary
            strSQL = " INSERT INTO voluntary(vl_regis_no, vl_veh_cd, vl_drv_flag, " + System.Environment.NewLine;
            strSQL += "     vl_drv1, vl_birth_drv1, vl_drv2, vl_birth_drv2, " + System.Environment.NewLine;
            strSQL += "     vl_tpbi_person, vl_tpbi_time, vl_tppd_time, vl_tppd_exc, " + System.Environment.NewLine;
            strSQL += "     vl_od_time, vl_od_exc, vl_f_t, vl_01_11, vl_01_121, vl_01_122, " + System.Environment.NewLine;
            strSQL += "     vl_01_21, vl_01_211, vl_01_212, vl_02_person, vl_02, vl_03, " + System.Environment.NewLine;
            strSQL += "     vl_repair, vl_accessory, vl_fleet_perc, vl_prmm, vl_campaign) " + System.Environment.NewLine;
            strSQL += " VALUES('" + strKey + "' ,";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["vl_veh_cd"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["vl_drv_flag"].ToString() + "', " + System.Environment.NewLine;
            strSQL += "         '" + dsData.Tables[0].Rows[0]["vl_drv1"].ToString() + "', ";
            if (!Convert.IsDBNull(dsData.Tables[0].Rows[0]["vl_birth_drv1"]) &&
                dsData.Tables[0].Rows[0]["vl_birth_drv1"].ToString() != "__/__/____")
            {
                DateTime dtTmp = Convert.ToDateTime(dsData.Tables[0].Rows[0]["vl_birth_drv1"].ToString());
                strTmpDt = TDS.Utility.MasterUtil.getDateString(dtTmp, "dd/MM/yyyy", "th-TH");
                strSQL += "     '" + cmDate.convertDateStringForDBTH(strTmpDt) + "', ";
            }
            else
                strSQL += "     null, ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["vl_drv2"].ToString() + "', ";
            if (!Convert.IsDBNull(dsData.Tables[0].Rows[0]["vl_birth_drv2"]) &&
                dsData.Tables[0].Rows[0]["vl_birth_drv2"].ToString() != "__/__/____")
            {
                DateTime dtTmp = Convert.ToDateTime(dsData.Tables[0].Rows[0]["vl_birth_drv2"].ToString());
                strTmpDt = TDS.Utility.MasterUtil.getDateString(dtTmp, "dd/MM/yyyy", "th-TH");
                strSQL += "     '" + cmDate.convertDateStringForDBTH(strTmpDt) + "', " + System.Environment.NewLine;
            }
            else
                strSQL += "     null, " + System.Environment.NewLine;
            strSQL += "         '" + (dsData.Tables[0].Rows[0]["vl_tpbi_person"].ToString() == "" ? "0" : dsData.Tables[0].Rows[0]["vl_tpbi_person"].ToString()) + "', ";
            strSQL += "         '" + (dsData.Tables[0].Rows[0]["vl_tpbi_time"].ToString() == "" ? "0" : dsData.Tables[0].Rows[0]["vl_tpbi_time"].ToString()) + "', ";
            strSQL += "         '" + (dsData.Tables[0].Rows[0]["vl_tppd_time"].ToString() == "" ? "0" : dsData.Tables[0].Rows[0]["vl_tppd_time"].ToString()) + "', ";
            strSQL += "         '" + (dsData.Tables[0].Rows[0]["vl_tppd_exc"].ToString() == "" ? "0" : dsData.Tables[0].Rows[0]["vl_tppd_exc"].ToString()) + "', " + System.Environment.NewLine;
            strSQL += "         '" + (dsData.Tables[0].Rows[0]["vl_od_time"].ToString() == "" ? "0" : dsData.Tables[0].Rows[0]["vl_od_time"].ToString()) + "', ";
            strSQL += "         '" + (dsData.Tables[0].Rows[0]["vl_od_exc"].ToString() == "" ? "0" : dsData.Tables[0].Rows[0]["vl_od_exc"].ToString()) + "', ";
            strSQL += "         '" + (dsData.Tables[0].Rows[0]["vl_f_t"].ToString() == "" ? "0" : dsData.Tables[0].Rows[0]["vl_f_t"].ToString()) + "', ";
            strSQL += "         '" + (dsData.Tables[0].Rows[0]["vl_01_11"].ToString() == "" ? "0" : dsData.Tables[0].Rows[0]["vl_01_11"].ToString()) + "', ";
            strSQL += "         '" + (dsData.Tables[0].Rows[0]["vl_01_121"].ToString() == "" ? "0" : dsData.Tables[0].Rows[0]["vl_01_121"].ToString()) + "', ";
            strSQL += "         '" + (dsData.Tables[0].Rows[0]["vl_01_122"].ToString() == "" ? "0" : dsData.Tables[0].Rows[0]["vl_01_122"].ToString()) + "', " + System.Environment.NewLine;
            strSQL += "         '" + (dsData.Tables[0].Rows[0]["vl_01_21"].ToString() == "" ? "0" : dsData.Tables[0].Rows[0]["vl_01_21"].ToString()) + "', ";
            strSQL += "         '" + (dsData.Tables[0].Rows[0]["vl_01_211"].ToString() == "" ? "0" : dsData.Tables[0].Rows[0]["vl_01_211"].ToString()) + "', ";
            strSQL += "         '" + (dsData.Tables[0].Rows[0]["vl_01_212"].ToString() == "" ? "0" : dsData.Tables[0].Rows[0]["vl_01_212"].ToString()) + "', ";
            strSQL += "         '" + (dsData.Tables[0].Rows[0]["vl_02_person"].ToString() == "" ? "0" : dsData.Tables[0].Rows[0]["vl_02_person"].ToString()) + "', ";
            strSQL += "         '" + (dsData.Tables[0].Rows[0]["vl_02"].ToString() == "" ? "0" : dsData.Tables[0].Rows[0]["vl_02"].ToString()) + "', ";
            strSQL += "         '" + (dsData.Tables[0].Rows[0]["vl_03"].ToString() == "" ? "0" : dsData.Tables[0].Rows[0]["vl_03"].ToString()) + "', " + System.Environment.NewLine;
            strSQL += "         '" + dsData.Tables[0].Rows[0]["vl_repair"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["vl_accessory"].ToString() + "', ";
            strSQL += "         '" + (dsData.Tables[0].Rows[0]["vl_fleet_perc"].ToString() == "" ? "0" : dsData.Tables[0].Rows[0]["vl_fleet_perc"].ToString()) + "', ";
            strSQL += "         '" + (dsData.Tables[0].Rows[0]["vl_prmm"].ToString() == "" ? "0" : dsData.Tables[0].Rows[0]["vl_prmm"].ToString()) + "', ";
            strSQL += "         '" + (dsData.Tables[0].Rows[0]["vl_campaign"].ToString()) + "') ";
            executeUpdate(strSQL, conn, trans);
        }
        catch (Exception e)
        {
            isRet = false;
        }
        return isRet;
    }
    private bool insertDataCompul(string strKey, DataSet dsData, SqlConnection conn, SqlTransaction trans)
    {
        bool isRet = true;
        string strSQL = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        try
        {
            strSQL = " INSERT INTO compulsary(cp_regis_no, cp_veh_cd, cp_comp_prmm, " + System.Environment.NewLine;
            strSQL += "     cp_comp_tax, cp_comp_stamp)";
            strSQL += " VALUES('" + strKey + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["cp_veh_cd"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["cp_comp_prmm"].ToString() + "', " + System.Environment.NewLine;
            strSQL += "         '" + dsData.Tables[0].Rows[0]["cp_comp_tax"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["cp_comp_stamp"].ToString() + "') ";
            executeUpdate(strSQL, conn, trans);
        }
        catch (Exception e)
        {
            isRet = false;
        }
        return isRet;
    }
    private bool insertDataMtveh(string strKey, DataSet dsData, SqlConnection conn, SqlTransaction trans)
    {
        bool isRet = true;
        string strSQL = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        try
        {
            strSQL = " INSERT INTO mtveh(mv_regis_no, mv_major_cd, mv_minor_cd, mv_veh_year, ";
            strSQL += "     mv_veh_cc, mv_veh_seat, mv_veh_weight, mv_license_no, mv_license_area, ";
            strSQL += "     mv_engin_no, mv_chas_no, mv_combine, mv_no_claim, mv_no_claim_perc) ";
            //strSQL += " VALUES('" + dsData.Tables[0].Rows[0]["tm_regis_no"].ToString() + "', ";
            strSQL += " VALUES('" + strKey + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["mv_major_cd"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["mv_minor_cd"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["mv_veh_year"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["mv_veh_cc"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["mv_veh_seat"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["mv_veh_weight"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["mv_license_no"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["mv_license_area"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["mv_engin_no"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["mv_chas_no"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["mv_combine"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["mv_no_claim"].ToString() + "', ";
            if (dsData.Tables[0].Rows[0]["mv_no_claim_perc"].ToString() == "")
            {
                strSQL += "         '0') ";
            }
            else
            {
                strSQL += "         '" + dsData.Tables[0].Rows[0]["mv_no_claim_perc"].ToString() + "') ";
            }
            executeUpdate(strSQL, conn, trans);
        }
        catch (Exception e)
        {
            isRet = false;
        }
        return isRet;
    }
    private bool insertDataPersonel(string strKey, DataSet dsData, SqlConnection conn, SqlTransaction trans)
    {
        bool isRet = true;
        string strSQL = "";
        string strTmpDt = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        try
        {
            strSQL = " INSERT INTO personal(pe_regis_no, pe_eff_time, pe_card_type, ";
            strSQL += "     pe_card_no, pe_birth_dt, pe_occup, pe_ben_fname, ";
            strSQL += "     pe_ben_lname, pe_ben_addr1, pe_ben_addr2, pe_ben_amphor, ";
            strSQL += "     pe_ben_changwat, pe_ben_postcode, pe_ben_tel, pe_ben_email, pe_relation) ";
            //strSQL += " VALUES('" + dsData.Tables[0].Rows[0]["tm_regis_no"].ToString() + "', ";
            strSQL += " VALUES('" + strKey + "' , ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pe_eff_time"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pe_card_type"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pe_card_no"].ToString() + "', ";
            if (Convert.IsDBNull(dsData.Tables[0].Rows[0]["pe_birth_dt"]) ||
                dsData.Tables[0].Rows[0]["pe_birth_dt"].ToString() == "")
                strSQL += "         null, ";
            else
            {
                strTmpDt = dsData.Tables[0].Rows[0]["pe_birth_dt"].ToString();
                //strTmpDt = strTmpDt.Substring(0, strTmpDt.IndexOf(" "));
                strSQL += "     '" + cmDate.convertDateStringForDBTH(strTmpDt) + "', ";
            }
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pe_occup"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pe_ben_fname"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pe_ben_lname"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pe_ben_addr1"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pe_ben_addr2"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pe_ben_amphor"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pe_ben_changwat"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pe_ben_postcode"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pe_ben_tel"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pe_ben_email"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pe_relation"].ToString() + "') ";
            executeUpdate(strSQL, conn, trans);            
        }
        catch (Exception e)
        {
            isRet = false;
        }
        return isRet;
    }
    private bool insertDataCover(string strKey, DataSet dsData, SqlConnection conn, SqlTransaction trans)
    {
        bool isRet = true;
        string strSQL = "";
        try
        {
            strSQL = " INSERT INTO ta_cover(ta_regis_no, ta_tot_ins, ta_sum_ins, ta_medic_exp, ta_prmm) ";
            //strSQL += " VALUES('" + dsData.Tables[0].Rows[0]["tm_regis_no"].ToString() + "', ";
            strSQL += " VALUES('" + strKey + "' , ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["ta_tot_ins"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["ta_sum_ins"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["ta_medic_exp"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["ta_prmm"].ToString() + "') ";
            executeUpdate(strSQL, conn, trans);
        }
        catch (Exception e)
        {
            isRet = false;
        }
        return isRet;
    }
    private bool insertDataPACover(string strKey, DataSet dsData, SqlConnection conn, SqlTransaction trans)
    {
        bool isRet = true;
        string strSQL = "";
        try
        {
            strSQL = " INSERT INTO pa_cover(pa_regis_no, pa_type, pa_death_si, pa_death_exc, ";
            strSQL += "         pa_death_prmm, pa_ttd_si, pa_ttd_week, pa_ttd_exc, pa_ttd_prmm, ";
            strSQL += "         pa_ptd_si, pa_ptd_week, pa_ptd_exc, pa_ptd_prmm, pa_med_si, ";
            strSQL += "         pa_med_exc, pa_med_prmm, pa_war, pa_str, pa_spt, pa_mtr, pa_air, ";
            strSQL += "         pa_murder, pa_add_prmm, pa_disc_prmm, pa_net_prmm ) ";
            //strSQL += " VALUES('" + dsData.Tables[0].Rows[0]["tm_regis_no"].ToString() + "', ";
            strSQL += " VALUES('" + strKey + "' , ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pa_type"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pa_death_si"].ToString().Replace(",","") + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pa_death_exc"].ToString().Replace(",", "") + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pa_death_prmm"].ToString().Replace(",", "") + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pa_ttd_si"].ToString().Replace(",", "") + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pa_ttd_week"].ToString().Replace(",", "") + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pa_ttd_exc"].ToString().Replace(",", "") + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pa_ttd_prmm"].ToString().Replace(",", "") + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pa_ptd_si"].ToString().Replace(",", "") + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pa_ptd_week"].ToString().Replace(",", "") + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pa_ptd_exc"].ToString().Replace(",", "") + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pa_ptd_prmm"].ToString().Replace(",", "") + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pa_med_si"].ToString().Replace(",", "") + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pa_med_exc"].ToString().Replace(",", "") + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pa_med_prmm"].ToString().Replace(",", "") + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pa_war"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pa_str"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pa_spt"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pa_mtr"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pa_air"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pa_murder"].ToString() + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pa_add_prmm"].ToString().Replace(",", "") + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pa_disc_prmm"].ToString().Replace(",", "") + "', ";
            strSQL += "         '" + dsData.Tables[0].Rows[0]["pa_net_prmm"].ToString().Replace(",", "") + "') ";
            executeUpdate(strSQL, conn, trans);
        }
        catch (Exception e)
        {
            isRet = false;
        }
        return isRet;
    }
    public string getNextNo(string dataMainClass, string dataYear, string dataMonth, string dataDay,
                            SqlConnection dataCnn, SqlTransaction dataTrans)
    {
        DataSet dsNext;
        string strSQL;
        string strNext;
        double dNext;
        string strPrefix;
        strSQL = "SELECT lt_prefix, lt_run_no ";
        strSQL += " FROM lastno ";
        strSQL += " WHERE lt_main_class = '" + dataMainClass + "' ";
        if (dataYear != "" && dataMonth != "" && dataDay != "")
        {
            strSQL += " AND lt_year = '" + dataYear + "'";
            strSQL += " AND lt_month = '" + dataMonth + "'";
            strSQL += " AND lt_day = '" + dataDay + "'";
        }
        dsNext = executeQuery(strSQL, dataCnn, dataTrans);
        if (dsNext.Tables.Count > 0 && dsNext.Tables[0].Rows.Count > 0)
        {
            strPrefix = dsNext.Tables[0].Rows[0]["lt_prefix"].ToString();
            strNext = dsNext.Tables[0].Rows[0]["lt_run_no"].ToString();
            dNext = Convert.ToDouble(strNext);
            dNext++;
            if (dataMainClass == "V" || dataMainClass == "C" || dataMainClass == "P" || dataMainClass == "T" ||
                dataMainClass == "RV" || dataMainClass == "RC" || dataMainClass == "B")
                strNext = dNext.ToString("00000");
            else
                strNext = dNext.ToString("000");
            // update next run_no
            strSQL = "UPDATE lastno SET lt_run_no = " + dNext;
            strSQL += " WHERE lt_main_class = '" + dataMainClass + "'";
            strSQL += "     AND lt_prefix = '" + strPrefix + "'";
            executeUpdate(strSQL, dataCnn, dataTrans);

        }
        else
        {
            // insert new record
            strNext = "1";
            if (dataMainClass == "V")
                strPrefix = "90";
            else if (dataMainClass == "C")
                strPrefix = "91";
            else if (dataMainClass == "P")
                strPrefix = "92";
            else if (dataMainClass == "T")
                strPrefix = "93";
            else if (dataMainClass == "RC")
                strPrefix = "80";
            else if (dataMainClass == "RV")
                strPrefix = "81";
            else
                strPrefix = dataYear + dataMonth + dataDay;
            strSQL = "INSERT INTO lastno(lt_main_class, lt_prefix, ";
            strSQL += "lt_year, lt_month, lt_day, lt_run_no)";
            strSQL += " VALUES('" + dataMainClass + "', '" + strPrefix + "', ";
            strSQL += "'" + dataYear + "', '" + dataMonth + "', '" + dataDay + "', " + strNext + ")";
            executeUpdate(strSQL, dataCnn, dataTrans);
            dNext = Convert.ToDouble(strNext);
            if (dataMainClass == "V" || dataMainClass == "C" || dataMainClass == "P" || dataMainClass == "T" ||
                dataMainClass == "RV" || dataMainClass == "RC")
                strNext = dNext.ToString("00000");
            else
                strNext = dNext.ToString("000");
        }
        return strPrefix + "-" + strNext;
    }
    public DataSet getTmpRegisterDetail(string dataTmpCd)
    {
        DataSet dsRet;
        string strSQL;
        strSQL = "SELECT *, isnull(tm_prmm,0) + isnull(tm_tax,0) + isnull(tm_stamp,0) as tm_total_prmm ";
        strSQL += " FROM tmpreg ";
        strSQL += " WHERE tm_tmp_cd = '" + dataTmpCd + "'";
        dsRet = executeQuery(strSQL);
        return dsRet;
    }
    public DataSet getCustAddress(string dataTmpNo, SqlConnection dataCnn, SqlTransaction dataTrans)
    {
        DataSet dsData;
        string strSQL;
        strSQL = "SELECT tm_ins_fname+ ' ' + tm_ins_lname  as name, tm_ins_addr1 + ' ' + tm_ins_addr2 as address, ";
        strSQL += "tm_ins_addr1 as addr1, tm_ins_addr2 as addr2, ";
        strSQL += "tm_ins_amphor as district, tm_ins_changwat as province, tm_ins_postcode as post_code, ";
        strSQL += "tm_ins_tel as tel_no, tm_ins_email as email, ";
        strSQL += "tm_prmm, tm_tax, tm_stamp, tm_ins_fname, tm_ins_lname, tm_ins_addr1, ";
        strSQL += "tm_ins_addr2, tm_ins_email, tm_ins_amphor, tm_ins_changwat, tm_ins_postcode";
        strSQL += " FROM tmpreg ";
        strSQL += " WHERE tm_tmp_cd = '" + dataTmpNo + "'";
        dsData = executeQuery(strSQL, dataCnn, dataTrans);
        return dsData;
    }
    public DataSet getCustAddress(string dataTmpNo)
    {
        DataSet dsData;
        string strSQL;
        strSQL = "SELECT tm_ins_fname+ ' ' + tm_ins_lname  as name, tm_ins_addr1 + ' ' + tm_ins_addr2 as address, ";
        strSQL += "tm_ins_addr1 as addr1, tm_ins_addr2 as addr2, ";
        strSQL += "tm_ins_amphor as district, tm_ins_changwat as province, tm_ins_postcode as post_code, ";
        strSQL += "tm_ins_tel as tel_no, tm_ins_email as email, ";
        strSQL += "tm_prmm, tm_tax, tm_stamp, tm_ins_fname, tm_ins_lname, tm_ins_addr1, ";
        strSQL += "tm_ins_addr2, tm_ins_email, tm_ins_amphor, tm_ins_changwat, tm_ins_postcode";
        strSQL += " FROM tmpreg ";
        strSQL += " WHERE tm_tmp_cd = '" + dataTmpNo + "'";
        dsData = executeQuery(strSQL);
        return dsData;
    }


    public DataSet getDataTmpVoluntaryByRegisNo(string dataRegisNo)
    {
        DataSet dstmpReg;
        DataSet dstmpVolun;
        DataSet dstmpMtveh;
        DataSet dsSendDoc;
        string strTmpCd = "";
        string strSQL;
        strSQL = "SELECT * FROM tmpreg";
        strSQL += " WHERE tm_regis_no = '" + dataRegisNo + "'";
        dstmpReg = executeQuery(strSQL);
        if (dstmpReg.Tables.Count > 0 && dstmpReg.Tables[0].Rows.Count > 0)
            strTmpCd = dstmpReg.Tables[0].Rows[0]["tm_tmp_cd"].ToString();
        strSQL = "SELECT * FROM tmpvolun";
        strSQL += " WHERE tvl_tmp_cd = '" + strTmpCd + "'";
        dstmpVolun = executeQuery(strSQL);
        strSQL = "SELECT * FROM tmpmtveh";
        strSQL += " WHERE tv_tmp_cd = '" + strTmpCd + "'";
        dstmpMtveh = executeQuery(strSQL);
        strSQL = "SELECT * FROM senddoc";
        strSQL += " WHERE sed_regis_no = '" + dataRegisNo + "'";
        dsSendDoc = executeQuery(strSQL);
        dstmpReg.Merge(dstmpVolun.Tables[0].Clone());
        dstmpReg.Merge(dstmpMtveh.Tables[0].Clone());
        dstmpReg.Merge(dsSendDoc.Tables[0].Clone());
        if (dstmpVolun.Tables.Count > 0 && dstmpVolun.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < dstmpVolun.Tables[0].Columns.Count; i++)
            {
                dstmpReg.Tables[0].Rows[0][dstmpVolun.Tables[0].Columns[i].ColumnName] = dstmpVolun.Tables[0].Rows[0][i];
            }
        }
        if (dstmpMtveh.Tables.Count > 0 && dstmpVolun.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < dstmpMtveh.Tables[0].Columns.Count; i++)
            {
                dstmpReg.Tables[0].Rows[0][dstmpMtveh.Tables[0].Columns[i].ColumnName] = dstmpMtveh.Tables[0].Rows[0][i];
            }
        }
        if (dsSendDoc.Tables.Count > 0 && dsSendDoc.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < dsSendDoc.Tables[0].Columns.Count; i++)
            {
                dstmpReg.Tables[0].Rows[0][dsSendDoc.Tables[0].Columns[i].ColumnName] = dsSendDoc.Tables[0].Rows[0][i];
            }
        }
        return dstmpReg;
    }
    public DataSet getDataTmpVoluntaryByTmpCd(string strTmpCd)
    {
        DataSet dstmpReg;
        DataSet dstmpVolun;
        DataSet dstmpMtveh;
        string strSQL;
        strSQL = "SELECT * FROM tmpreg";
        strSQL += " WHERE tm_tmp_cd = '" + strTmpCd + "'";
        dstmpReg = executeQuery(strSQL);
        strSQL = "SELECT * FROM tmpvolun";
        strSQL += " WHERE tvl_tmp_cd = '" + strTmpCd + "'";
        dstmpVolun = executeQuery(strSQL);
        strSQL = "SELECT * FROM tmpmtveh";
        strSQL += " WHERE tv_tmp_cd = '" + strTmpCd + "'";
        dstmpMtveh = executeQuery(strSQL);
        dstmpReg.Merge(dstmpVolun.Tables[0].Clone());
        dstmpReg.Merge(dstmpMtveh.Tables[0].Clone());
        if (dstmpVolun.Tables.Count > 0 && dstmpVolun.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < dstmpVolun.Tables[0].Columns.Count; i++)
            {
                dstmpReg.Tables[0].Rows[0][dstmpVolun.Tables[0].Columns[i].ColumnName] = dstmpVolun.Tables[0].Rows[0][i];
            }
        }
        if (dstmpMtveh.Tables.Count > 0 && dstmpVolun.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < dstmpMtveh.Tables[0].Columns.Count; i++)
            {
                dstmpReg.Tables[0].Rows[0][dstmpMtveh.Tables[0].Columns[i].ColumnName] = dstmpMtveh.Tables[0].Rows[0][i];
            }
        }
        return dstmpReg;
    }

    public DataSet getDataTmpCompulsaryByRegisNo(string dataRegisNo)
    {
        DataSet dstmpReg;
        DataSet dstmpComp;
        DataSet dstmpMtveh;
        DataSet dsSendDoc;
        string strTmpCd = "";
        string strSQL;
        strSQL = "SELECT * FROM tmpreg";
        strSQL += " WHERE tm_regis_no = '" + dataRegisNo + "'";
        dstmpReg = executeQuery(strSQL);
        if (dstmpReg.Tables.Count > 0 && dstmpReg.Tables[0].Rows.Count > 0)
            strTmpCd = dstmpReg.Tables[0].Rows[0]["tm_tmp_cd"].ToString();
        strSQL = "SELECT * FROM tmpCompul";
        strSQL += " WHERE tcp_tmp_cd = '" + strTmpCd + "'";
        dstmpComp = executeQuery(strSQL);
        strSQL = "SELECT * FROM tmpmtveh";
        strSQL += " WHERE tv_tmp_cd = '" + strTmpCd + "'";
        dstmpMtveh = executeQuery(strSQL);
        strSQL = "SELECT * FROM senddoc";
        strSQL += " WHERE sed_regis_no = '" + dataRegisNo + "'";
        dsSendDoc = executeQuery(strSQL);
        dstmpReg.Merge(dstmpComp.Tables[0].Clone());
        dstmpReg.Merge(dstmpMtveh.Tables[0].Clone());
        dstmpReg.Merge(dsSendDoc.Tables[0].Clone());
        for (int i = 0; i < dstmpComp.Tables[0].Columns.Count; i++)
        {
            dstmpReg.Tables[0].Rows[0][dstmpComp.Tables[0].Columns[i].ColumnName] = dstmpComp.Tables[0].Rows[0][i];
        }
        for (int i = 0; i < dstmpMtveh.Tables[0].Columns.Count; i++)
        {
            dstmpReg.Tables[0].Rows[0][dstmpMtveh.Tables[0].Columns[i].ColumnName] = dstmpMtveh.Tables[0].Rows[0][i];
        }
        if (dsSendDoc.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < dsSendDoc.Tables[0].Columns.Count; i++)
            {
                dstmpReg.Tables[0].Rows[0][dsSendDoc.Tables[0].Columns[i].ColumnName] = dsSendDoc.Tables[0].Rows[0][i];
            }
        }
        return dstmpReg;
    }
    public DataSet getDataTmpCompulsaryByTmpCd(string strTmpCd)
    {
        DataSet dstmpReg;
        DataSet dstmpComp;
        DataSet dstmpMtveh;
        string strSQL;
        strSQL = "SELECT * FROM tmpreg";
        strSQL += " WHERE tm_tmp_cd = '" + strTmpCd + "'";
        dstmpReg = executeQuery(strSQL);
        strSQL = "SELECT * FROM tmpCompul";
        strSQL += " WHERE tcp_tmp_cd = '" + strTmpCd + "'";
        dstmpComp = executeQuery(strSQL);
        strSQL = "SELECT * FROM tmpmtveh";
        strSQL += " WHERE tv_tmp_cd = '" + strTmpCd + "'";
        dstmpMtveh = executeQuery(strSQL);
        dstmpReg.Merge(dstmpComp.Tables[0].Clone());
        dstmpReg.Merge(dstmpMtveh.Tables[0].Clone());
        for (int i = 0; i < dstmpComp.Tables[0].Columns.Count; i++)
        {
            dstmpReg.Tables[0].Rows[0][dstmpComp.Tables[0].Columns[i].ColumnName] = dstmpComp.Tables[0].Rows[0][i];
        }
        for (int i = 0; i < dstmpMtveh.Tables[0].Columns.Count; i++)
        {
            dstmpReg.Tables[0].Rows[0][dstmpMtveh.Tables[0].Columns[i].ColumnName] = dstmpMtveh.Tables[0].Rows[0][i];
        }
        return dstmpReg;
    }

    public DataSet getDataTmpTravelAccidentByRegisNo(string dataRegisNo)
    {
        DataSet dstmpReg;
        DataSet dstmpPersonal;
        DataSet dstmpCover;
        DataSet dsSendDoc;
        string strTmpCd = "";
        string strSQL;
        strSQL = "SELECT * FROM tmpreg";
        strSQL += " WHERE tm_regis_no = '" + dataRegisNo + "'";
        dstmpReg = executeQuery(strSQL);
        if (dstmpReg.Tables.Count > 0 && dstmpReg.Tables[0].Rows.Count > 0)
            strTmpCd = dstmpReg.Tables[0].Rows[0]["tm_tmp_cd"].ToString();
        strSQL = "SELECT * FROM tmppersonal";
        strSQL += " WHERE tpe_tmp_no = '" + strTmpCd + "'";
        dstmpPersonal = executeQuery(strSQL);
        strSQL = "SELECT * FROM tmpta_cover";
        strSQL += " WHERE tta_tmp_no = '" + strTmpCd + "'";
        dstmpCover = executeQuery(strSQL);
        strSQL = "SELECT * FROM senddoc";
        strSQL += " WHERE sed_regis_no = '" + dataRegisNo + "'";
        dsSendDoc = executeQuery(strSQL);
        dstmpReg.Merge(dstmpPersonal.Tables[0].Clone());
        dstmpReg.Merge(dstmpCover.Tables[0].Clone());
        dstmpReg.Merge(dsSendDoc.Tables[0].Clone());
        for (int i = 0; i < dstmpPersonal.Tables[0].Columns.Count; i++)
        {
            dstmpReg.Tables[0].Rows[0][dstmpPersonal.Tables[0].Columns[i].ColumnName] = dstmpPersonal.Tables[0].Rows[0][i];
        }
        for (int i = 0; i < dstmpCover.Tables[0].Columns.Count; i++)
        {
            dstmpReg.Tables[0].Rows[0][dstmpCover.Tables[0].Columns[i].ColumnName] = dstmpCover.Tables[0].Rows[0][i];
        }
        for (int i = 0; i < dsSendDoc.Tables[0].Columns.Count; i++)
        {
            if (dsSendDoc.Tables[0].Rows.Count > 0)
                dstmpReg.Tables[0].Rows[0][dsSendDoc.Tables[0].Columns[i].ColumnName] = dsSendDoc.Tables[0].Rows[0][i];
        }
        return dstmpReg;
    }
    public DataSet getDataTmpTravelAccidentByTmpCd(string strTmpCd)
    {
        DataSet dstmpReg;
        DataSet dstmpPersonal;
        DataSet dstmpCover;
        string strSQL;
        strSQL = "SELECT * FROM tmpreg";
        strSQL += " WHERE tm_tmp_cd = '" + strTmpCd + "'";
        dstmpReg = executeQuery(strSQL);
        strSQL = "SELECT * FROM tmppersonal";
        strSQL += " WHERE tpe_tmp_no = '" + strTmpCd + "'";
        dstmpPersonal = executeQuery(strSQL);
        strSQL = "SELECT * FROM tmpta_cover";
        strSQL += " WHERE tta_tmp_no = '" + strTmpCd + "'";
        dstmpCover = executeQuery(strSQL);
        dstmpReg.Merge(dstmpPersonal.Tables[0].Clone());
        dstmpReg.Merge(dstmpCover.Tables[0].Clone());
        for (int i = 0; i < dstmpPersonal.Tables[0].Columns.Count; i++)
        {
            dstmpReg.Tables[0].Rows[0][dstmpPersonal.Tables[0].Columns[i].ColumnName] = dstmpPersonal.Tables[0].Rows[0][i];
        }
        for (int i = 0; i < dstmpCover.Tables[0].Columns.Count; i++)
        {
            dstmpReg.Tables[0].Rows[0][dstmpCover.Tables[0].Columns[i].ColumnName] = dstmpCover.Tables[0].Rows[0][i];
        }
        return dstmpReg;
    }

    public DataSet getDataTmpPersonelAccidentByRegisNo(string dataRegisNo)
    {
        DataSet dstmpReg;
        DataSet dstmpPersonal;
        DataSet dstmpCover;
        DataSet dsSendDoc;
        string strTmpCd = "";
        string strSQL;
        strSQL = "SELECT * FROM tmpreg";
        strSQL += " WHERE tm_regis_no = '" + dataRegisNo + "'";
        dstmpReg = executeQuery(strSQL);
        if (dstmpReg.Tables.Count > 0 && dstmpReg.Tables[0].Rows.Count > 0)
            strTmpCd = dstmpReg.Tables[0].Rows[0]["tm_tmp_cd"].ToString();
        strSQL = "SELECT * FROM tmppersonal";
        strSQL += " WHERE tpe_tmp_no = '" + strTmpCd + "'";
        dstmpPersonal = executeQuery(strSQL);
        strSQL = "SELECT * FROM tmppa_cover";
        strSQL += " WHERE tpa_tmp_no = '" + strTmpCd + "'";
        dstmpCover = executeQuery(strSQL);
        strSQL = "SELECT * FROM senddoc";
        strSQL += " WHERE sed_regis_no = '" + dataRegisNo + "'";
        dsSendDoc = executeQuery(strSQL);
        dstmpReg.Merge(dstmpPersonal.Tables[0].Clone());
        dstmpReg.Merge(dstmpCover.Tables[0].Clone());
        dstmpReg.Merge(dsSendDoc.Tables[0].Clone());
        for (int i = 0; i < dstmpPersonal.Tables[0].Columns.Count; i++)
        {
            dstmpReg.Tables[0].Rows[0][dstmpPersonal.Tables[0].Columns[i].ColumnName] = dstmpPersonal.Tables[0].Rows[0][i];
        }
        for (int i = 0; i < dstmpCover.Tables[0].Columns.Count; i++)
        {
            dstmpReg.Tables[0].Rows[0][dstmpCover.Tables[0].Columns[i].ColumnName] = dstmpCover.Tables[0].Rows[0][i];
        }
        for (int i = 0; i < dsSendDoc.Tables[0].Columns.Count; i++)
        {
            if (dsSendDoc.Tables[0].Rows.Count > 0)
                dstmpReg.Tables[0].Rows[0][dsSendDoc.Tables[0].Columns[i].ColumnName] = dsSendDoc.Tables[0].Rows[0][i];
        }
        return dstmpReg;
    }
    public DataSet getDataTmpPersonelByTmpCd(string strTmpCd)
    {
        DataSet dstmpReg;
        DataSet dstmpPersonal;
        DataSet dstmpCover;
        string strSQL;
        strSQL = "SELECT * FROM tmpreg";
        strSQL += " WHERE tm_tmp_cd = '" + strTmpCd + "'";
        dstmpReg = executeQuery(strSQL);
        strSQL = "SELECT * FROM tmppersonal";
        strSQL += " WHERE tpe_tmp_no = '" + strTmpCd + "'";
        dstmpPersonal = executeQuery(strSQL);
        strSQL = "SELECT * FROM tmppa_cover";
        strSQL += " WHERE tpa_tmp_no = '" + strTmpCd + "'";
        dstmpCover = executeQuery(strSQL);
        dstmpReg.Merge(dstmpPersonal.Tables[0].Clone());
        dstmpReg.Merge(dstmpCover.Tables[0].Clone());
        for (int i = 0; i < dstmpPersonal.Tables[0].Columns.Count; i++)
        {
            dstmpReg.Tables[0].Rows[0][dstmpPersonal.Tables[0].Columns[i].ColumnName] = dstmpPersonal.Tables[0].Rows[0][i];
        }
        for (int i = 0; i < dstmpCover.Tables[0].Columns.Count; i++)
        {
            dstmpReg.Tables[0].Rows[0][dstmpCover.Tables[0].Columns[i].ColumnName] = dstmpCover.Tables[0].Rows[0][i];
        }
        return dstmpReg;
    }

    public DataSet getDataContact(string dataCode)
    {
        DataSet dsData;
        string strSQL = "";

        strSQL = "SELECT * ";
        strSQL += " FROM contact ";
        strSQL += " WHERE ct_code = '" + dataCode + "'";
        dsData = executeQuery(strSQL);

        return dsData;
    }
    public DataSet getTmpRegByRegNo(string dataRegisNo)
    {
        DataSet dsRet;
        string strSQL;
        strSQL = "SELECT * ";
        strSQL += " FROM tmpreg ";
        strSQL += " WHERE tm_regis_no = '" + dataRegisNo + "'";
        dsRet = executeQuery(strSQL);
        return dsRet;
    }
    public DataSet getDataVoluntary(string strRegisNo)
    {
        DataSet dsRet;
        string strSQL;
        strSQL = "SELECT *, ";
        strSQL += " rg_oth_distance as oth_distance, rg_oth_region as oth_region, ";
        strSQL += " rg_oth_oldpolicy as oth_oldpolicy, rg_oth_flagdeduct as oth_flagdeduct, ";
        strSQL += " rg_mott_id as mott_id, rg_insc_id as insc_id, vl_campaign as campaign ";
        strSQL += " FROM register A INNER JOIN voluntary B ON A.rg_regis_no = B.vl_regis_no ";
        strSQL += "     INNER JOIN mtveh C ON A.rg_regis_no = C.mv_regis_no ";
        strSQL += "     INNER JOIN senddoc D ON A.rg_regis_no = D.sed_regis_no ";
        strSQL += "     LEFT JOIN compulsary E ON A.rg_regis_no = E.cp_regis_no ";
        strSQL += " WHERE rg_regis_no = '" + strRegisNo + "'";
        dsRet = executeQuery(strSQL);
        return dsRet;
    }
    public DataSet getDataCompulsary(string strRegisNo)
    {
        DataSet dsRet;
        string strSQL;
        strSQL = "SELECT * ";
        strSQL += " FROM register A INNER JOIN compulsary B ON A.rg_regis_no = B.cp_regis_no ";
        strSQL += "     INNER JOIN mtveh C ON A.rg_regis_no = C.mv_regis_no ";
        strSQL += "     INNER JOIN senddoc D ON A.rg_regis_no = D.sed_regis_no ";
        strSQL += " WHERE rg_regis_no = '" + strRegisNo + "'";
        dsRet = executeQuery(strSQL);
        return dsRet;
    }
    public DataSet getDataTravelAccident(string strRegisNo)
    {
        DataSet dsRet;
        string strSQL;
        strSQL = "SELECT * ";
        strSQL += " FROM register A INNER JOIN personal B ON A.rg_regis_no = B.pe_regis_no ";
        strSQL += "     INNER JOIN ta_cover C ON A.rg_regis_no = C.ta_regis_no ";
        strSQL += "     INNER JOIN senddoc D ON A.rg_regis_no = D.sed_regis_no ";
        strSQL += " WHERE rg_regis_no = '" + strRegisNo + "'";
        dsRet = executeQuery(strSQL);
        return dsRet;
    }
    public DataSet getDataPersonelAccident(string strRegisNo)
    {
        DataSet dsRet;
        string strSQL;
        strSQL = "SELECT * ";
        strSQL += " FROM register A INNER JOIN personal B ON A.rg_regis_no = B.pe_regis_no ";
        strSQL += "     INNER JOIN pa_cover C ON A.rg_regis_no = C.pa_regis_no ";
        strSQL += "     INNER JOIN senddoc D ON A.rg_regis_no = D.sed_regis_no ";
        strSQL += " WHERE rg_regis_no = '" + strRegisNo + "'";
        dsRet = executeQuery(strSQL);
        return dsRet;
    }

    private void sendMail(DataSet dsContact)
    {
        string strSMTPDomainName = "";
        TDSMail mail;
        string strBody = "";
        string strType = "";
        MailAddressCollection mAddrs = new MailAddressCollection();
        MailManager cmMail = new MailManager();
        cmMail.setXmlFilePath(System.Configuration.ConfigurationManager.AppSettings["appPath"] + "/Adminweb/xml/ContactUs.xml");
        // ส่งเมล์ไปให้บุคคลที่มีหน้าทีรับผิดชอบ
        strSMTPDomainName = cmMail.getDataByKey("SMTPMail");
        mail = new TDSMail(strSMTPDomainName);
        mail.TO = new MailAddressCollection();
        mail.Subject = "ลูกค้าจาก Internet ประสงค์ให้บริษัทฯติดต่อกลับ ";
        mail.IsBodyHtml = true;
        strBody = " Dear เจ้าหน้าที่รับแจ้งประกัน ," + "<br>";
        strBody += " มีผู้แจ้งความประสงค์ให้ติดต่อกลับ คือ" + "<br>";
        strBody += " &nbsp;&nbsp;&nbsp;&nbsp;คุณ  : " + dsContact.Tables[0].Rows[0]["name"].ToString() + "<br>";
        strBody += " &nbsp;&nbsp;&nbsp;&nbsp;เบอร์โทรศัพท์/มือถือ : " + dsContact.Tables[0].Rows[0]["tel_no"].ToString() + "<br>";
        strBody += " &nbsp;&nbsp;&nbsp;&nbsp;Email : " + dsContact.Tables[0].Rows[0]["email"].ToString() + "<br>";
        strBody += " &nbsp;&nbsp;&nbsp;&nbsp;เวลาสะดวกโทรกลับ : " + dsContact.Tables[0].Rows[0]["contact_time"].ToString() + "<br>";
        if (dsContact.Tables[0].Rows[0]["f_motor"].ToString() == "Y")
        {
            strType += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- ประกันภัยรถยนต์ <br>";
            mAddrs.Add(new MailAddress(cmMail.getDataByKey("EmailMotor")));
        }
        if (dsContact.Tables[0].Rows[0]["f_fire"].ToString() == "Y")
        {
            strType += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- ประกันอัคคีภัย<br>";
            mAddrs.Add(new MailAddress(cmMail.getDataByKey("EmailFire")));
        }
        if (dsContact.Tables[0].Rows[0]["f_sea"].ToString() == "Y")
        {
            strType += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- ประกันภัยทางทะเลและขนส่ง  <br>";
            mAddrs.Add(new MailAddress(cmMail.getDataByKey("EmailSea")));
        }
        if (dsContact.Tables[0].Rows[0]["f_pa"].ToString() == "Y")
        {
            strType += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- ประกันภัยอุบัติเหตุส่วนบุคคล และการเดินทาง  <br>";
            mAddrs.Add(new MailAddress(cmMail.getDataByKey("EmailPA")));
        }
        if (dsContact.Tables[0].Rows[0]["f_other"].ToString() == "Y")
        {
            strType += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- อื่นๆ <br>";
            mAddrs.Add(new MailAddress(cmMail.getDataByKey("EmailOther")));
        }
        strBody += " สนใจการประกันภัย  <br>" + strType + "<br>";
        strBody += " Best Regards," + "<br>";
        strBody += " &nbsp;&nbsp;&nbsp;บริษัท สินมั่นคงประกันภัย จำกัด." + "<br>";
        mail.Body = strBody;
        //for (int i = 0; i <= mAddrs.Count - 1; i++)
        //{
        //    try
        //    {
        //        mail.Send(new MailAddress(cmMail.getDataByKey("EmailAdmin")), mAddrs[i]);
        //    }
        //    catch (Exception ex)
        //    {
		//		TDS.Utility.MasterUtil.writeError("RegisterManager.sendMail() ", ex.ToString());
        //    }
        //}
        mail.TO = mAddrs;
        for (int i = 0; i <= mAddrs.Count - 1; i++)
        {
            
        }
        try
		{
			mail.Send(new MailAddress(cmMail.getDataByKey("EmailAdmin")), cmMail.getDataByKey("EmailAdmin"));
		}
		catch (Exception ex)
		{
			TDS.Utility.MasterUtil.writeError("buyVoluntary_step4.sendMail() ", ex.ToString());
		}
    }
}
