﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for RequestPolicyManager
/// </summary>
public class RequestPolicyManager : TDS.BL.BLWorker_MSSQL
{
	public RequestPolicyManager()
	{
		//
		// TODO: Add constructor logic here
		//
        strConnection = "strConnLocal";
	}
    public DataSet getDataForApprove(string dataPolType)
    {
        DataSet dsData;
        string strSQL = "";
        strSQL = "SELECT register.*, tm_tmp_cd, tm_regis_no, sed_branch, ";
        strSQL += " rg_ins_fname + ' ' + rg_ins_lname as rg_ins_fullname ";
        strSQL += " FROM register inner join tmpreg on tm_regis_no = rg_regis_no";
        strSQL += "     left join mtveh on rg_regis_no = mv_regis_no";
        strSQL += "     inner join senddoc on rg_regis_no = sed_regis_no ";
        strSQL += " WHERE (rg_approve = 'O' OR rg_approve = 'H') ";
        if (dataPolType != "")
        {
            if (dataPolType == "V")
            {
                strSQL += "     AND rg_pol_type in ('1','2','3','4','5') ";
                strSQL += "     AND mv_combine = 'N'";
            }
            else if (dataPolType == "VC")
            {
                strSQL += "     AND rg_pol_type in ('1','2','3','4','5') ";
                strSQL += "     AND mv_combine = 'Y'";
            }
            else
                strSQL += "     AND rg_pol_type = '" + dataPolType + "'";
        }
        strSQL += " ORDER BY rg_regis_dt desc, tm_regis_no desc";
        dsData = executeQuery(strSQL);
        return dsData;
    }
    public bool updateApprove(DataSet dsData)
    {
        IntsmkManager cmIntsmk;
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        SqlConnection cnn = getDbConnection();
        SqlTransaction trans = cnn.BeginTransaction();
        DataSet dsRegister;
        DataSet dsSendDoc;
        DataSet dsVoluntary;
        DataSet dsCompulsary;
        DataSet dsMtveh;
        DataSet dsPersonal;
        DataSet dsPACover;
        DataSet dsTACover;
        string strSQL = "";
        bool isRet = true;
        bool isIFXResult = true;
        string strDateTmp = "";
        try
        {
            for (int i = 0; i <= dsData.Tables[0].Rows.Count - 1; i++)
            {
                // update register
                strSQL = "UPDATE register ";
                strSQL += " SET rg_approve = '" + dsData.Tables[0].Rows[i]["approve_result"].ToString() + "'";
                strSQL += " WHERE rg_regis_no = '" + dsData.Tables[0].Rows[i]["regis_no"].ToString() + "'";
                TDS.Utility.MasterUtil.writeLog("CalculateService :: AdminCalculatorManager", strSQL);
                executeUpdate(strSQL, cnn, trans);
                if (dsData.Tables[0].Rows[i]["approve_result"].ToString() == "A")
                {
                    #region select data from register
                    strSQL = "SELECT * ,convert(varchar(10),rg_effect_dt,103) as tmp_effect_dt, ";
                    strSQL += "     convert(varchar(10),rg_expiry_dt,103) as tmp_expiry_dt, ";
                    strSQL += "     convert(varchar(10),rg_regis_dt,103) as tmp_regis_dt ";
                    strSQL += " FROM register ";
                    strSQL += " WHERE rg_regis_no = '" + dsData.Tables[0].Rows[i]["regis_no"].ToString() + "'";
                    TDS.Utility.MasterUtil.writeLog("CalculateService :: AdminCalculatorManager-1", strSQL);
                    dsRegister = executeQuery(strSQL, cnn, trans);
                    dsRegister.Tables[0].TableName = "REGISTER";
                    #endregion

                    #region select data from senddoc
                    strSQL = "SELECT * ";
                    strSQL += " FROM senddoc ";
                    strSQL += " WHERE sed_regis_no = '" + dsData.Tables[0].Rows[i]["regis_no"].ToString() + "'";
                    dsSendDoc = executeQuery(strSQL, cnn, trans);
                    dsRegister.Tables.Add(dsSendDoc.Tables[0].Copy());
                    dsRegister.Tables[1].TableName = "SENDDOC";
                    #endregion

                    #region select data from voluntary, compulsary, mtveh, personal, pa_cover, ta_cover
                    if (dsRegister.Tables.Count > 0 && dsRegister.Tables[0].Rows.Count > 0)
                    {
                        if (dsRegister.Tables[0].Rows[0]["rg_main_class"].ToString() == "M")
                        {
                            // select from mtveh
                            strSQL = "SELECT * ";
                            strSQL += " FROM mtveh ";
                            strSQL += " WHERE mv_regis_no = '" + dsData.Tables[0].Rows[i]["regis_no"].ToString() + "'";
                            dsMtveh = executeQuery(strSQL, cnn, trans);
                            dsRegister.Tables.Add(dsMtveh.Tables[0].Copy());
                            dsRegister.Tables[2].TableName = "MTVEH";
                            // select from compulsary / voluntary
                            if (dsRegister.Tables[0].Rows[0]["rg_pol_type"].ToString() == "C")
                            {
                                strSQL = "SELECT * ";
                                strSQL += " FROM compulsary ";
                                strSQL += " WHERE cp_regis_no = '" + dsData.Tables[0].Rows[i]["regis_no"].ToString() + "'";
                                dsCompulsary = executeQuery(strSQL, cnn, trans);
                                dsRegister.Tables.Add(dsCompulsary.Tables[0].Copy());
                                dsRegister.Tables[3].TableName = "COMPULSARY";
                            }
                            else
                            {
                                strSQL = "SELECT * ";
                                strSQL += " FROM voluntary ";
                                strSQL += " WHERE vl_regis_no = '" + dsData.Tables[0].Rows[i]["regis_no"].ToString() + "'";
                                dsVoluntary = executeQuery(strSQL, cnn, trans);
                                dsRegister.Tables.Add(dsVoluntary.Tables[0].Copy());
                                dsRegister.Tables[3].TableName = "VOLUNTARY";
                            }
                        }
                        else
                        {
                            //select from personal
                            strSQL = "SELECT * ,convert(varchar(10),pe_birth_dt,103) as tmp_birth_dt";
                            strSQL += " FROM personal ";
                            strSQL += " WHERE pe_regis_no = '" + dsData.Tables[0].Rows[i]["regis_no"].ToString() + "'";
                            dsPersonal = executeQuery(strSQL, cnn, trans);
                            dsRegister.Tables.Add(dsPersonal.Tables[0].Copy());
                            dsRegister.Tables[2].TableName = "PERSONAL";
                            // select from pa_cover / ta_cover
                            if (dsRegister.Tables[0].Rows[0]["rg_main_class"].ToString() == "P")
                            {
                                strSQL = "SELECT * ";
                                strSQL += " FROM pa_cover ";
                                strSQL += " WHERE pa_regis_no = '" + dsData.Tables[0].Rows[i]["regis_no"].ToString() + "'";
                                dsPACover = executeQuery(strSQL, cnn, trans);
                                dsRegister.Tables.Add(dsPACover.Tables[0].Copy());
                                dsRegister.Tables[3].TableName = "PACOVER";
                            }
                            else
                            {
                                strSQL = "SELECT * ";
                                strSQL += " FROM ta_cover ";
                                strSQL += " WHERE ta_regis_no = '" + dsData.Tables[0].Rows[i]["regis_no"].ToString() + "'";
                                dsTACover = executeQuery(strSQL, cnn, trans);
                                dsRegister.Tables.Add(dsTACover.Tables[0].Copy());
                                dsRegister.Tables[3].TableName = "TACOVER";
                            }
                        }
                    }

                    #endregion

                    if (dsRegister.Tables.Count > 0 && dsRegister.Tables[0].Rows.Count > 0)
                    {
                        #region insert into policy ใน SQL
                        strSQL = "INSERT INTO policy(pl_book_no, pl_inform_no, pl_regis_no, ";
                        strSQL += "     pl_main_class, pl_pol_type, pl_effect_dt, ";
                        strSQL += "     pl_expiry_dt, pl_regis_dt, pl_regis_time, pl_regis_type, ";
                        strSQL += "     pl_ins_fname, pl_ins_lname, pl_ins_addr1, pl_ins_addr2, ";
                        strSQL += "     pl_ins_amphor, pl_ins_changwat, pl_ins_postcode, pl_ins_tel, ";
                        strSQL += "     pl_ins_email, pl_old_policy, pl_sum_ins, pl_prmm, pl_tax, ";
                        strSQL += "     pl_stamp, pl_pmt_cd, pl_fleet_perc, pl_pay_type, pl_payment_stat, ";
                        strSQL += "     pl_ref_bank, pl_stat, pl_approve_dt) ";
                        strSQL += " VALUES(";
                        strSQL += "     '" + dsData.Tables[0].Rows[i]["book_no"].ToString() + "', ";
                        strSQL += "     '" + dsData.Tables[0].Rows[i]["inform_no"].ToString() + "', ";
                        strSQL += "     '" + dsData.Tables[0].Rows[i]["regis_no"].ToString() + "', ";
                        strSQL += "     '" + dsRegister.Tables[0].Rows[0]["rg_main_class"].ToString() + "', ";
                        strSQL += "     '" + dsRegister.Tables[0].Rows[0]["rg_pol_type"].ToString() + "', ";
                        strDateTmp = dsRegister.Tables[0].Rows[0]["tmp_effect_dt"].ToString().Substring(0, 10);
                        strSQL += "     '" + cmDate.convertDateStringForDBTH(strDateTmp) + "', ";
                        strDateTmp = dsRegister.Tables[0].Rows[0]["tmp_expiry_dt"].ToString().Substring(0, 10);
                        strSQL += "     '" + cmDate.convertDateStringForDBTH(strDateTmp) + "', ";
                        strDateTmp = dsRegister.Tables[0].Rows[0]["tmp_regis_dt"].ToString().Substring(0, 10);
                        strSQL += "     '" + cmDate.convertDateStringForDBTH(strDateTmp) + "', ";
                        strSQL += "     '" + dsRegister.Tables[0].Rows[0]["rg_regis_time"].ToString() + "', ";
                        strSQL += "     '" + dsRegister.Tables[0].Rows[0]["rg_regis_type"].ToString() + "', ";
                        strSQL += "     '" + dsRegister.Tables[0].Rows[0]["rg_ins_fname"].ToString() + "', ";
                        strSQL += "     '" + dsRegister.Tables[0].Rows[0]["rg_ins_lname"].ToString() + "', ";
                        if (Convert.IsDBNull(dsRegister.Tables[0].Rows[0]["rg_ins_addr1"]))
                        {
                            strSQL += "     NULL, ";
                        }
                        else
                        {
                            strSQL += "     '" + dsRegister.Tables[0].Rows[0]["rg_ins_addr1"].ToString() + "', ";
                        }
                        strSQL += "     '" + dsRegister.Tables[0].Rows[0]["rg_ins_addr2"].ToString() + "', ";
                        strSQL += "     '" + dsRegister.Tables[0].Rows[0]["rg_ins_amphor"].ToString() + "', ";
                        strSQL += "     '" + dsRegister.Tables[0].Rows[0]["rg_ins_changwat"].ToString() + "', ";
                        strSQL += "     '" + dsRegister.Tables[0].Rows[0]["rg_ins_postcode"].ToString() + "', ";
                        if (Convert.IsDBNull(dsRegister.Tables[0].Rows[0]["rg_ins_tel"]))
                        {
                            strSQL += "     NULL, ";
                        }
                        else
                        {
                            strSQL += "     '" + dsRegister.Tables[0].Rows[0]["rg_ins_tel"].ToString() + "', ";
                        }
                        if (Convert.IsDBNull(dsRegister.Tables[0].Rows[0]["rg_ins_email"]))
                        {
                            strSQL += "     NULL, ";
                        }
                        else
                        {
                            strSQL += "     '" + dsRegister.Tables[0].Rows[0]["rg_ins_email"].ToString() + "', ";
                        }
                        if (Convert.IsDBNull(dsRegister.Tables[0].Rows[0]["rg_old_policy"]))
                        {
                            strSQL += "     NULL, ";
                        }
                        else
                        {
                            strSQL += "     '" + dsRegister.Tables[0].Rows[0]["rg_old_policy"].ToString() + "', ";
                        }
                        strSQL += "     '" + dsRegister.Tables[0].Rows[0]["rg_sum_ins"].ToString() + "', ";
                        strSQL += "     '" + dsRegister.Tables[0].Rows[0]["rg_prmm"].ToString() + "', ";
                        strSQL += "     '" + dsRegister.Tables[0].Rows[0]["rg_tax"].ToString() + "', ";
                        strSQL += "     '" + dsRegister.Tables[0].Rows[0]["rg_stamp"].ToString() + "', ";
                        if (Convert.IsDBNull(dsRegister.Tables[0].Rows[0]["rg_pmt_cd"]))
                        {
                            strSQL += "     NULL, ";
                        }
                        else
                        {
                            strSQL += "     '" + dsRegister.Tables[0].Rows[0]["rg_pmt_cd"].ToString() + "', ";
                        }
                        if (Convert.IsDBNull(dsRegister.Tables[0].Rows[0]["rg_fleet_perc"]))
                        {
                            strSQL += "     NULL, ";
                        }
                        else
                        {
                            strSQL += "     '" + dsRegister.Tables[0].Rows[0]["rg_fleet_perc"].ToString() + "', ";
                        }
                        strSQL += "     '" + dsRegister.Tables[0].Rows[0]["rg_pay_type"].ToString() + "', ";
                        if (Convert.IsDBNull(dsRegister.Tables[0].Rows[0]["rg_payment_stat"]))
                        {
                            strSQL += "     NULL, ";
                        }
                        else
                        {
                            strSQL += "     '" + dsRegister.Tables[0].Rows[0]["rg_payment_stat"].ToString() + "', ";
                        }
                        if (Convert.IsDBNull(dsRegister.Tables[0].Rows[0]["rg_ref_bank"]))
                        {
                            strSQL += "     NULL, ";
                        }
                        else
                        {
                            strSQL += "     '" + dsRegister.Tables[0].Rows[0]["rg_ref_bank"].ToString() + "', ";
                        }
                        strSQL += "     'A', ";
                        strSQL += "     '" + cmDate.convertDateStringForDBTH(DateTime.Now.ToString("dd/MM/yyyy")) + "') ";
                        executeUpdate(strSQL, cnn, trans);
                        #endregion

                        #region insert data ใน Informix
                        TDS.Utility.MasterUtil.writeLog("CalculateService :: AdminCalculatorManager", "start informix");
                        cmIntsmk = new IntsmkManager();
                        if (dsRegister.Tables[0].Rows[0]["rg_main_class"].ToString() == "M")
                        {
                            if (dsRegister.Tables[0].Rows[0]["rg_pol_type"].ToString() == "C")
                                isIFXResult = cmIntsmk.insertPolicyComp(dsData.Tables[0].Rows[i]["book_no"].ToString(), dsData.Tables[0].Rows[i]["inform_no"].ToString(), dsData.Tables[0].Rows[i]["regis_no"].ToString(), dsRegister);
                            else
                                isIFXResult = cmIntsmk.insertPolicyVol(dsData.Tables[0].Rows[i]["book_no"].ToString(), dsData.Tables[0].Rows[i]["inform_no"].ToString(), dsData.Tables[0].Rows[i]["regis_no"].ToString(), dsRegister);
                        }
                        else if (dsRegister.Tables[0].Rows[0]["rg_main_class"].ToString() == "P")
                        {
                            isIFXResult = cmIntsmk.insertPolicyPA(dsData.Tables[0].Rows[i]["book_no"].ToString(), dsData.Tables[0].Rows[i]["inform_no"].ToString(), dsData.Tables[0].Rows[i]["regis_no"].ToString(), dsRegister);
                        }
                        else
                        {
                            isIFXResult = cmIntsmk.insertPolicyTA(dsData.Tables[0].Rows[i]["book_no"].ToString(), dsData.Tables[0].Rows[i]["inform_no"].ToString(), dsData.Tables[0].Rows[i]["regis_no"].ToString(), dsRegister);
                        }
                        #endregion

                        if (isIFXResult == false)
                            break;
                    }
                }
            }
            if (isIFXResult)
                trans.Commit();
            else
            {
                trans.Rollback();
                isRet = false;
            }
        }
        catch (Exception e)
        {
            isRet = false;
            trans.Rollback();
            TDS.Utility.MasterUtil.writeLog("CalculateService :: AdminCalculatorManager", e.ToString());
        }
        return isRet;
    }

    public DataSet searchContactData(string dataStatus)
    {
        DataSet ds;
        string strSQL = "";
        strSQL = "SELECT * ";
        strSQL += "FROM contact ";
        if (dataStatus != "")
            strSQL += " WHERE ct_cont_stat = '" + dataStatus + "' ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public bool editContactData(DataSet dsData)
    {
        bool isRet = true;
        string strSQL = "";
        SqlConnection cnn;
        SqlTransaction trans;
        cnn = getDbConnection();
        trans = cnn.BeginTransaction();
        try
        {
            for (int i = 0; i <= dsData.Tables[0].Rows.Count - 1; i++)
            {
                strSQL += "UPDATE CONTACT SET ";
                strSQL += " ct_cont_stat = '" + dsData.Tables[0].Rows[i]["status"].ToString() + "' ";
                strSQL += " WHERE ct_code = '" + dsData.Tables[0].Rows[i]["ct_code"].ToString() + "' ";
                executeUpdate(strSQL, cnn, trans);
            }
            trans.Commit();
        }
        catch (Exception e)
        {
            trans.Rollback();
            isRet = false;
        }
        finally
        {
            trans.Dispose();
            cnn.Close();
            cnn.Dispose();
        }
        return isRet;

    }
    public DataSet getDataPolicy(string dataPolType, string strRegisNo, string strPlNo)
    {
        DataSet dsData;
        string strSQL = "";
        strSQL = "SELECT register.*, tm_tmp_cd, tm_regis_no, sed_branch, ";
        strSQL += " rg_ins_fname + ' ' + rg_ins_lname as rg_ins_fullname, ";
        strSQL += " pl_book_no + '/' + pl_inform_no as pl_no ";
        strSQL += " FROM register inner join tmpreg on tm_regis_no = rg_regis_no";
        strSQL += "     left join mtveh on rg_regis_no = mv_regis_no";
        strSQL += "     left join senddoc on rg_regis_no = sed_regis_no ";
        strSQL += "		inner join policy on rg_regis_no = pl_regis_no ";
        strSQL += " WHERE 1 = 1 ";
        if (dataPolType != "")
        {
            if (dataPolType == "V")
            {
                strSQL += "     AND rg_pol_type in ('1','2','3','4','5') ";
                strSQL += "     AND mv_combine = 'N'";
            }
            else if (dataPolType == "VC")
            {
                strSQL += "     AND rg_pol_type in ('1','2','3','4','5') ";
                strSQL += "     AND mv_combine = 'Y'";
            }
            else
                strSQL += "     AND rg_pol_type = '" + dataPolType + "'";
        }
        if (strRegisNo != "")
        {
			strSQL += "     AND rg_regis_no like '%" + strRegisNo + "%'";
        }
        if (strPlNo != "")
        {
			strSQL += "     AND pl_book_no + '/' + pl_inform_no like '%" + strPlNo + "%'";
        }
        strSQL += " ORDER BY rg_regis_dt desc, tm_regis_no ";
        dsData = executeQuery(strSQL);
        return dsData;
    }
}
