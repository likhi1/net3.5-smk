﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;
using System.IO;

/// <summary>
/// Summary description for AuthenticateManager
/// </summary>
public class AuthenticateManager : TDS.BL.BLWorker_MSSQL
{
	public AuthenticateManager()
	{
		//
		// TODO: Add constructor logic here
		//
        strConnection = "strConnLocal";
	}
    #region for table aut_user    
    public DataSet searchDataUser(string strLogin, string strEmpCode, string strFName,
                                    string strLName, string strRoleID,
                                    string strStartDate1, string strStartDate2)
    {
        string strSQL = "";
        string strWhere = "";
        DataSet ds;
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        strSQL = "SELECT A.*, ISNULL(A.user_fname,'') + ' ' ++ ISNULL(A.user_lname,'') as user_fullname,";
        strSQL += " B.role_name ";
        strSQL += " FROM aut_user A INNER JOIN aut_role B ON A.role_id = B.role_id ";
        strSQL += " WHERE 1 = 1 ";
        if (strLogin != "")
            strWhere += " user_login = '" + strLogin + "' AND ";
        if (strEmpCode != "")
            strWhere += " user_empcode like '%" + strEmpCode + "%' AND ";
        if (strFName != "")
            strWhere += " user_fname like '%" + strFName + "%' AND ";
        if (strLName != "")
            strWhere += " user_lname like '%" + strLName + "%' AND ";
        if (strRoleID != "")
            strWhere += " A.role_id = '" + strRoleID + "' AND ";
        if (strStartDate1 != "__/__/____")
            strWhere += " A.start_date >= '" + cmDate.convertDateStringForDBTH(strStartDate1) + "' AND ";
        if (strStartDate2 != "__/__/____")
            strWhere += " A.start_date <= '" + cmDate.convertDateStringForDBTH(strStartDate2) + "' AND ";

        if (strWhere != "")
            strSQL += " AND " + strWhere.Substring(0, strWhere.Length - 4);
        ds = executeQuery(strSQL);
        return ds;
    }    
    public DataSet getDataUserByLogin(string strLogin)
    {
        string strSQL = "";
        DataSet ds;
        strSQL = "SELECT A.*, ISNULL(A.user_fname,'') + ' ' + ISNULL(A.user_lname,'') as user_fullname,";
        strSQL += "     B.role_name,";
        strSQL += "     UI.user_fname + ' ' + UI.user_lname as insert_name, ";
        strSQL += "     UU.user_fname + ' ' + UU.user_lname as update_name ";
        strSQL += " FROM aut_user  A INNER JOIN aut_role B ON A.role_id = B.role_id ";
        strSQL += "     LEFT JOIN aut_user UI ON A.insert_login = UI.user_login ";
        strSQL += "     LEFT JOIN aut_user UU ON A.update_login = UU.user_login ";
        strSQL += " WHERE A.user_login = '" + strLogin + "' ";
        ds = executeQuery(strSQL);
        return ds;
    }    
    public DataSet getAllDataUserActive()
    {
        string strSQL = "";
        DataSet ds;
        strSQL = "SELECT A.*, ISNULL(A.user_fname,'') + ' ' + ISNULL(A.user_lname,'') as user_fullname,";
        strSQL += "     B.role_name, B.level_id, ";
        strSQL += "     UI.user_fname + ' ' + UI.user_lname as insert_name, ";
        strSQL += "     UU.user_fname + ' ' + UU.user_lname as update_name, ";
        strSQL += "     UL.level_name ";
        strSQL += " FROM aut_user  A INNER JOIN aut_role B ON A.role_id = B.role_id ";
        strSQL += "     LEFT JOIN aut_user UI ON A.insert_login = UI.user_login ";
        strSQL += "     LEFT JOIN aut_user UU ON A.update_login = UU.user_login ";
        strSQL += "     INNER JOIN aut_level UL ON B.level_id = UL.level_id ";
        strSQL += " WHERE A.user_status = 'A' ";
        ds = executeQuery(strSQL);
        return ds;
    }    
    public string deleteDataUser(string strLogin, string strUserLogin)
    {
        string strRet = "";
        string strSQL = "";


        strSQL = "UPDATE aut_user SET ";
        strSQL += " user_status = 'C', ";
        strSQL += " user_canceldate = getDate(), ";
        strSQL += " user_cancellogin = '" + strUserLogin + "', ";
        strSQL += " update_date = getDate(), ";
        strSQL += " update_login = '" + strUserLogin + "' ";
        strSQL += " WHERE user_login = '" + strLogin + "' ";
        executeUpdate(strSQL);

        return strRet;
    }    
    public string addDataUser(DataSet dsData)
    {
        string strRet = "";
        string strSQL = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();

        strSQL = "INSERT INTO aut_user(user_login, user_pwd, user_empcode, user_fname, user_lname, ";
        strSQL += " role_id, user_status, user_canceldate, user_cancellogin, start_date, stop_date, ";
        strSQL += " user_remark, ";
        strSQL += " insert_date, insert_login ) ";
        strSQL += " VALUES ( ";
        strSQL += " '" + dsData.Tables[0].Rows[0]["user_login"].ToString() + "', ";
        strSQL += " '" + encode(dsData.Tables[0].Rows[0]["user_pwd"].ToString(), dsData.Tables[0].Rows[0]["user_login"].ToString()) + "', ";
        strSQL += " '" + dsData.Tables[0].Rows[0]["user_empcode"].ToString() + "', ";
        strSQL += " '" + TDS.Utility.MasterUtil.replaceSpecialSQL(dsData.Tables[0].Rows[0]["user_fname"].ToString()) + "', ";
        strSQL += " '" + TDS.Utility.MasterUtil.replaceSpecialSQL(dsData.Tables[0].Rows[0]["user_lname"].ToString()) + "', ";
        strSQL += " '" + dsData.Tables[0].Rows[0]["role_id"].ToString() + "', ";
        strSQL += " '" + dsData.Tables[0].Rows[0]["user_status"].ToString() + "', ";
        if (dsData.Tables[0].Rows[0]["user_status"].ToString() == "C")
        {
            strSQL += " getDate(), ";
            strSQL += " '" + dsData.Tables[0].Rows[0]["USERLOGIN"].ToString() + "', ";
        }
        else
        {
            strSQL += " null, ";
            strSQL += " '" + dsData.Tables[0].Rows[0]["USERLOGIN"].ToString() + "', ";
        }
        if (dsData.Tables[0].Rows[0]["start_date"].ToString() != "__/__/____")
            strSQL += " '" + cmDate.convertDateStringForDBTH(dsData.Tables[0].Rows[0]["start_date"].ToString()) + "', ";
        else
            strSQL += " null, ";
        if (dsData.Tables[0].Rows[0]["stop_date"].ToString() != "__/__/____")
            strSQL += " '" + cmDate.convertDateStringForDBTH(dsData.Tables[0].Rows[0]["stop_date"].ToString()) + "', ";
        else
            strSQL += " null, ";
        strSQL += " '" + dsData.Tables[0].Rows[0]["user_remark"].ToString() + "', ";
        strSQL += " getDate(), ";
        strSQL += " '" + dsData.Tables[0].Rows[0]["USERLOGIN"].ToString() + "' ";
        strSQL += " ) ";
        executeUpdate(strSQL);

        return strRet;
    }    
    public string editDataUser(DataSet dsData)
    {
        string strRet = "";
        string strSQL = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();

        strSQL = "UPDATE aut_user ";
        strSQL += " SET ";
        //strSQL += " user_pwd = '" + encode(dsData.Tables[0].Rows[0]["user_pwd"].ToString(), dsData.Tables[0].Rows[0]["user_login"].ToString()) + "', ";
        strSQL += " user_fname = '" + TDS.Utility.MasterUtil.replaceSpecialSQL(dsData.Tables[0].Rows[0]["user_fname"].ToString()) + "', ";
        strSQL += " user_lname = '" + TDS.Utility.MasterUtil.replaceSpecialSQL(dsData.Tables[0].Rows[0]["user_lname"].ToString()) + "', ";
        strSQL += " role_id = '" + dsData.Tables[0].Rows[0]["role_id"].ToString() + "', ";
        strSQL += " user_status = '" + dsData.Tables[0].Rows[0]["user_status"].ToString() + "', ";
        if (dsData.Tables[0].Rows[0]["user_status"].ToString() == "C")
        {
            strSQL += " user_canceldate = getDate(), ";
            strSQL += " user_cancellogin = '" + dsData.Tables[0].Rows[0]["USERLOGIN"].ToString() + "', ";
        }
        else
        {
            strSQL += " user_canceldate = null, ";
            strSQL += " user_cancellogin = null, ";
        }
        if (dsData.Tables[0].Rows[0]["start_date"].ToString() != "__/__/____")
            strSQL += " start_date = '" + cmDate.convertDateStringForDBTH(dsData.Tables[0].Rows[0]["start_date"].ToString()) + "', ";
        else
            strSQL += " start_date = null, ";
        if (dsData.Tables[0].Rows[0]["stop_date"].ToString() != "__/__/____")
            strSQL += " stop_date = '" + cmDate.convertDateStringForDBTH(dsData.Tables[0].Rows[0]["stop_date"].ToString()) + "', ";
        else
            strSQL += " stop_date = null, ";
        strSQL += " user_remark = '" + dsData.Tables[0].Rows[0]["user_remark"].ToString() + "', ";
        strSQL += " update_date = getDate(), ";
        strSQL += " update_login = '" + dsData.Tables[0].Rows[0]["USERLOGIN"].ToString() + "' ";
        strSQL += " WHERE  user_login = '" + dsData.Tables[0].Rows[0]["user_login"].ToString() + "' ";
        executeUpdate(strSQL);

        return strRet;
    }
    public string editDataPassword(string strLogin, string strNewPassword,
                                string strUserLogin, string strFlagLoginDate)
    {
        string strRet = "";
        string strSQL = "";
        DataSet ds;
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();

        strSQL = "SELECT * ";
        strSQL += " FROM aut_user ";
        strSQL += " WHERE user_login = '" + strLogin + "' AND ";
        strSQL += "     user_pwd = '" + encode(strNewPassword, strLogin) + "' ";
        ds = executeQuery(strSQL);
        if (ds.Tables[0].Rows.Count > 0)
        {
            strRet = "1 : รหัสผ่านซ้ำกับรหัสผ่านเดิม กรุณาเปลี่ยนรหัสผ่านอื่น";
        }
        else
        {
            strSQL = "UPDATE aut_user ";
            strSQL += " SET ";
            strSQL += " user_pwd = '" + encode(strNewPassword, strLogin) + "', ";
            if (strFlagLoginDate == "Y")
                strSQL += " user_lastlogindate = getDate(), ";
            strSQL += " user_status = 'A', ";
            strSQL += " user_changepwddate = getDate(), ";
            strSQL += " update_date = getDate(), ";
            strSQL += " update_login = '" + strUserLogin + "' ";
            strSQL += " WHERE  user_login = '" + strLogin + "' ";
            executeUpdate(strSQL);
        }
        return strRet;
    }    
    public string clearDataLastLogin(string strLogin, string strUserLogin)
    {
        string strRet = "";
        string strSQL = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();

        strSQL = "UPDATE aut_user ";
        strSQL += " SET ";
        strSQL += " user_lastlogindate = null, ";
        strSQL += " update_date = getDate(), ";
        strSQL += " update_login = '" + strUserLogin + "' ";
        strSQL += " WHERE  user_login = '" + strLogin + "' ";
        executeUpdate(strSQL);

        return strRet;
    }    
    public DataSet getDataUserByUserLogin(string strLogin)
    {
        string strSQL = string.Empty;
        DataSet ds;
        strSQL = "SELECT "
            + "user_login "
            + "FROM "
            + "aut_user "
            + " WHERE "
            + " user_login = '" + strLogin + "' "
            ;
        ds = executeQuery(strSQL);
        return ds;
    }
    #endregion

    #region for table aut_role && aut_role_menu && aut_roleassettype
    public DataSet getDataMenuByRoleID(string strRoleID)
    {
        string strSQL = "";
        DataSet ds;
        strSQL = "SELECT * ";
        strSQL += " FROM aut_role_menu A INNER JOIN aut_menu B ON A.menu_id = B.menu_id ";
        strSQL += " WHERE A.role_id = '" + strRoleID + "' AND ";
        strSQL += "     role_menu_status = 'A' ";

        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getAllDataRole()
    {
        string strSQL = "";
        DataSet ds;
        strSQL = "SELECT * ";
        strSQL += " FROM aut_role ";

        ds = executeQuery(strSQL);
        return ds;
    }
    #endregion

    #region for login method
    public string doLogin(string strUserLogin, string strPassword)
    {
        string strSQL = "";
        string strRet = "";
        DataSet ds;

        //1. ตรวจสอบ รหัสผู้ใช้งาน
        strSQL = "SELECT * ";
        strSQL += " FROM aut_user ";
        strSQL += " WHERE user_login = '" + TDS.Utility.MasterUtil.replaceSpecialSQL(strUserLogin) + "' AND ";
        strSQL += "     user_pwd = '" + encode(strPassword, strUserLogin) + "' ";
        ds = executeQuery(strSQL);
        if (ds.Tables[0].Rows.Count > 0)
        {
            if (ds.Tables[0].Rows[0]["user_status"].ToString() == "A")
            {
                //if (DateTime.Compare((DateTime)ds.Tables[0].Rows[0]["start_date"], DateTime.Now) > 0)
                //{
                //    strRet = "1 : รหัสผู้ใช้ยังไม่เปิดให้เข้าสู่ระบบได้ ท่านสามารถเข้าระบบได้ตั้งแต่วันที่ " + TDS.Utility.MasterUtil.getDateString((DateTime)ds.Tables[0].Rows[0]["start_date"], "dd/MM/yyyy", "th-TH");
                //}
                //else if ((!Convert.IsDBNull(ds.Tables[0].Rows[0]["stop_date"])) && (DateTime.Compare((DateTime)ds.Tables[0].Rows[0]["stop_date"], DateTime.Now.AddDays(-1)) < 0))
                //{
                //    strRet = "1 : รหัสผู้ใช้หมดอายุแล้ว ตั้งแต่วันที่ " + TDS.Utility.MasterUtil.getDateString((DateTime)ds.Tables[0].Rows[0]["stop_date"], "dd/MM/yyyy", "th-TH");
                //}
                //else if (ds.Tables[0].Rows[0]["user_flagexpriepwd"].ToString() == "Y")
                //{
                //    if ((!Convert.IsDBNull(ds.Tables[0].Rows[0]["user_changepwddate"])) &&
                //        (DateTime.Compare(((DateTime)ds.Tables[0].Rows[0]["user_changepwddate"]).AddDays(Convert.ToDouble(ds.Tables[0].Rows[0]["user_expirepwddays"])), DateTime.Now.AddDays(-1)) < 0))
                //    {
                //        strRet = "1 : รหัสผ่านหมดอายุแล้ว ตั้งแต่วันที่ " + TDS.Utility.MasterUtil.getDateString(((DateTime)ds.Tables[0].Rows[0]["user_changepwddate"]).AddDays(Convert.ToDouble(ds.Tables[0].Rows[0]["user_expirepwddays"])), "dd/MM/yyyy", "th-TH");
                //        strRet += "\\nกรุณาติดต่อผู้ดูแลระบบเพื่อทำการเปลี่ยนรหัสผ่านใหม่";
                //    }
                //}
                //if (strRet == "" &&
                //        (!Convert.IsDBNull(ds.Tables[0].Rows[0]["user_lastlogindate"])) &&
                //        (DateTime.Compare(((DateTime)ds.Tables[0].Rows[0]["user_lastlogindate"]).AddDays(Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings["loginDays"])), DateTime.Now.AddDays(-1)) < 0))
                //{
                //    strRet = "1 : ท่านไม่ได้เข้าใช้งานเกินกำหนด";
                //    strRet += "\\nกรุณาติดต่อผู้ดูแลระบบเพื่อทำการเคลียร์ค่าวันใช้งานล่าสุด";
                //}
            }
            else
            {
                strRet = "1 : รหัสผู้ใช้ของท่านได้ถูกยกเลิกไปแล้ว ";
            }
        }
        else
        {
            strSQL = "SELECT * ";
            strSQL += " FROM aut_user ";
            strSQL += " WHERE user_login = '" + TDS.Utility.MasterUtil.replaceSpecialSQL(strUserLogin) + "' ";
            ds = executeQuery(strSQL);
            if (ds.Tables[0].Rows.Count > 0)
            {
                strRet = "2 : รหัสผ่านไม่ถูกต้อง";
            }
            else
            {
                strRet = "3 : ไม่พบรหัสผู้ใช้งานที่ระบุ";
            }
        }
        if (strRet == "")
        {
            // 2. ตรวจสอบว่าเคยมีการเข้าใช้งานหรือยัง
            //if (Convert.IsDBNull(ds.Tables[0].Rows[0]["user_lastlogindate"]))
            //{
            //    strRet = "0 : คุณเข้าใช้งานระบบเป็นครั้งแรก จึงต้องทำการเปลี่ยนรหัสผ่านก่อน";
            //}
        }
        if (strRet == "")
        {
            // 3. กรณีสามารถเข้าใช้งานได้ ทำการ update ค่า user_lastlogindate ด้วย
            strSQL = "UPDATE aut_user SET";
            strSQL += " user_lastlogindate = getDate(),";
            strSQL += " update_date = getDate(), ";
            strSQL += " update_login = '" + strUserLogin + "' ";
            strSQL += " WHERE user_login = '" + TDS.Utility.MasterUtil.replaceSpecialSQL(strUserLogin) + "' ";
            executeUpdate(strSQL);
        }
        return strRet;
    }
    #endregion

    #region encode - decode
    public string encode(string strData, string strKey)
    {
        // Encode the string �message�
        // ScrambleKey and ScambleIV are randomly generated

        byte[] ScrambleIV = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7 };
        byte[] ScrambleKey = Encoding.Default.GetBytes(strKey);
        UTF8Encoding textConverter = new UTF8Encoding();
        RC2CryptoServiceProvider rc2CSP =
                         new RC2CryptoServiceProvider();

        //Convert the data to a byte array.
        byte[] toEncrypt = textConverter.GetBytes(strData);

        //Get an encryptor.
        ICryptoTransform encryptor =
           rc2CSP.CreateEncryptor(ScrambleKey, ScrambleIV);

        //Encrypt the data.
        MemoryStream msEncrypt = new MemoryStream();
        CryptoStream csEncrypt = new CryptoStream(msEncrypt,
                                 encryptor, CryptoStreamMode.Write);

        //Write all data to the crypto stream and flush it.
        // Encode length as first 4 bytes
        byte[] length = new byte[4];
        length[0] = (byte)(strData.Length & 0xFF);
        length[1] = (byte)((strData.Length >> 8) & 0xFF);
        length[2] = (byte)((strData.Length >> 16) & 0xFF);
        length[3] = (byte)((strData.Length >> 24) & 0xFF);
        csEncrypt.Write(length, 0, 4);
        csEncrypt.Write(toEncrypt, 0, toEncrypt.Length);
        csEncrypt.FlushFinalBlock();

        //Get encrypted array of bytes.
        byte[] encrypted = msEncrypt.ToArray();
        return Convert.ToBase64String(encrypted);
    }
    public string decode(string strData, string strKey)
    {
        // Decode the �encrypted� byte[]
        UTF8Encoding textConverter = new UTF8Encoding();
        RC2CryptoServiceProvider rc2CSP = new RC2CryptoServiceProvider();
        //Get a decryptor that uses the same key and IV as the encryptor.
        byte[] ScrambleIV = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7 };
        byte[] ScrambleKey = Encoding.Default.GetBytes(strKey);
        ICryptoTransform decryptor =
               rc2CSP.CreateDecryptor(ScrambleKey, ScrambleIV);

        //Now decrypt the previously encrypted message using the decryptor
        // obtained in the above step.
        MemoryStream msDecrypt = new MemoryStream(Convert.FromBase64String(strData));
        CryptoStream csDecrypt = new CryptoStream(msDecrypt,
                                 decryptor, CryptoStreamMode.Read);

        byte[] fromEncrypt = new byte[strData.Length - 4];

        //Read the data out of the crypto stream.
        byte[] length = new byte[4];
        csDecrypt.Read(length, 0, 4);
        csDecrypt.Read(fromEncrypt, 0, fromEncrypt.Length);
        int len = (int)length[0] | (length[1] << 8) |
                  (length[2] << 16) | (length[3] << 24);

        //Convert the byte array back into a string.
        return textConverter.GetString(fromEncrypt).Substring(0, len);
    }
    #endregion
}
