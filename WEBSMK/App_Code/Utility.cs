﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Utility
/// </summary>
public class Utility 
{
   string catId   ;
   public  string CatId { get { return catId; } set { catId = value; } }

	public  Utility()
	{
        
	} 

    public string SetPageName(string _CatId) {
        string CatName;
        switch (_CatId) {                               
            case "1": CatName = "โรงพยาบาล"; break;
            case "2": CatName = "อู่ในสัญญา"; break;
            case "3": CatName = "ศูนย์ซ่อมรถ"; break;
            case "4": CatName = "สาขา"; break;
            default: CatName = "ประเภทข้อมูลไม่ถูกต้อง"; break;
        }
        return CatName;
    }

    public string SetPagenameNews(string _CatId) {
        string CatName;
        switch (_CatId) {
            case "1": CatName = "ความรู้ประกันภัย"; break;
            case "2": CatName = "ข่าวสารประกันภัย"; break;
            case "3": CatName = "ข่าวกิจกรรม"; break;
            case "4": CatName = "TVC สินมั่นคง"; break;
            case "5": CatName = "ข่าวตัวแทน & คู่ค้า"; break;
            default: CatName = "ประเภทข้อมูลไม่ถูกต้อง"; break;
        }
        return CatName;
    }

    public string SetPagenameProducts(string _CatId) {
        string CatName;
        switch (_CatId) {
            case "1": CatName = "ประกันภัยสุขภาพ"; break;
            case "2": CatName = "ประกันภัยรถยนต์"; break;
            case "3": CatName = "ประกันภัยโรคมะเร็ง"; break;
            case "4": CatName = "ประกันภัยอุบัติเหตุ"; break;
            case "5": CatName = "ประกันภัยอัคคีภัย"; break;
            case "6": CatName = "ประกันภัยการเดินทาง"; break;
            case "7": CatName = "ประกันประเภทอื่นๆ"; break;
            default: CatName = "ประเภทข้อมูลไม่ถูกต้อง"; break;
        }
        return CatName;
    } 
     
    public string GetIpAddress() {
        string strIpAddress;
        strIpAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        if (strIpAddress == null) {
            strIpAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        }
        return strIpAddress;

    }

    public void CurrentSession() {
        object SessAdmin = HttpContext.Current.Session["SessAdmin"];
        string SessId = HttpContext.Current.Session.SessionID;
        // print Session Now
        HttpContext.Current.Response.Write(SessId);

        if (SessAdmin == null && (string)SessAdmin != SessId) {
            HttpContext.Current.Response.Redirect("main.aspx");
        }

    }

     
}