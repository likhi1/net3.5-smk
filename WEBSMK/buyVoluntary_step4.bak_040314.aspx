﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="buyVoluntary_step4.aspx.cs" Inherits="buyVoluntary_step4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div id="page-online-insCar">
        <h1 class="subject1">ทำประกันออนไลน์ </h1>
        การทำรายการประกันภัยของท่านเรียบร้อยแล้ว หากท่านมีข้อสงสัยเพิ่มเติม สามารถติดต่อสอบถามได้ที่ 02-378-7167<br />
        <br />        
        <div class="prc-row">        
        <h2>
        เลขที่อ้างอิง : <asp:Label ID="lblRefNo" runat="server" Text=""></asp:Label>
        </h2>
        <h2 class="subject-detail">
        รายละเอียดผู้เอาประกัน
        </h2>        
        <table width="630px">
            <tr>
                <td valign="top">
                    <table class="tb-online-insCar">              
                        <tr>
                            <td class="label" >
                                ชื่อ :
                            </td>
                            <td >
                                <asp:Label ID="lblFName" runat="server" Text=""></asp:Label>
                            </td>    
                            <td class="label">
                                นามสกุล :
                            </td>
                            <td>
                                <asp:Label ID="lblLName" runat="server" Text=""></asp:Label>
                            </td>                            
                        </tr>
                        <tr>
                            <td class="label">
                                ที่อยู่ :
                            </td>
                            <td colspan="3">
                                <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>                    
                            <td class="label"  >
                                ตำบล / แขวง :
                            </td>
                            <td colspan="3">
                                <asp:Label ID="lblSubDistrict" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" >
                                อำเภอ / เขต :
                            </td>
                            <td width="150px">
                                <asp:Label ID="lblDistrict" runat="server" Text=""></asp:Label>
                            </td>      
                            <td class="label">
                                จังหวัด :
                            </td>
                            <td  >
                                <asp:Label ID="lblProvince" runat="server" Text=""></asp:Label>
                            </td>                          
                        </tr>
                        <tr>
                            <td class="label">
                                รหัสไปรษณีย์ :
                            </td>
                            <td width="130px">
                                <asp:Label ID="lblPostCode" runat="server" Text=""></asp:Label>
                            </td>      
                            <td class="label">
                                เลขที่บัตรประชาชน :
                            </td>
                            <td  >
                                <asp:Label ID="lblIDCard" runat="server" Text=""></asp:Label>
                            </td>                         
                        </tr>
                        <tr>
                            <td class="label">
                                เบอร์โทรศัพท์มือถือ :
                            </td>
                            <td>
                                <asp:Label ID="lblMobile" runat="server" Text=""></asp:Label>
                            </td>  
                            <td class="label">
                                เบอร์โทรศัพท์บ้าน :
                            </td>
                            <td>
                                <asp:Label ID="lblTelephone" runat="server" Text=""></asp:Label>
                            </td>                                                  
                        </tr>
                        <tr>
                            <td class="label">
                                อีเมล์ :
                            </td>
                            <td>
                                <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label>
                            </td>                    
                        </tr>
                        <tr>
                            <td class="label"  >
                                ผู้ขับขี่คนที่ 1 :
                            </td>
                            <td  >
                                <asp:Label ID="lblDriver1Name" runat="server"></asp:Label>
                            </td>  
                            <td class="label"  >
                                วันเกิด :
                            </td>
                            <td  >
                                <asp:Label ID="lblDriver1BrithDate" runat="server"></asp:Label>
                            </td>                              
                        </tr>
                        <tr>
                            <td class="label"  >
                                ผู้ขับขี่คนที่ 2 :
                            </td>
                            <td  >
                                <asp:Label ID="lblDriver2Name" runat="server"></asp:Label>
                            </td>  
                            <td class="label"  >
                                วันเกิด :
                            </td>
                            <td  >
                                <asp:Label ID="lblDriver2BrithDate" runat="server"></asp:Label>
                            </td>                              
                        </tr>
                    </table>
                </td>
            </tr>            
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        
        <h2 class="subject-detail">รายละเอียดรถยนต์</h2>
        <table width="630px">
            <tr>
                <td valign="top">
                    <table class="tb-online-insCar">                
                        <tr>
                            <td class="label"  >
                                ยี่ห้อรถ :
                            </td>
                            <td  >
                                <asp:Label ID="lblCarName" runat="server" Text=""></asp:Label>
                            </td>                                
                        </tr>
                        <tr>
                            <td class="label">
                                รุ่นรถ :
                            </td>
                            <td>
                                <asp:Label ID="lblCarMark" runat="server" Text=""></asp:Label>
                            </td>                                
                        </tr>
                        <tr>
                            <td class="label">
                                ปีจดทะเบียน :
                            </td>
                            <td>
                                <asp:Label ID="lblCarRegisYear" runat="server" Text=""></asp:Label>
                            </td>                                                    
                        </tr>
                        <tr>
                            <td class="label">
                                ประเภทรถ :
                            </td>
                            <td>
                                <asp:Label ID="lblCarType" runat="server" Text=""></asp:Label>
                            </td>                                
                        </tr>
                        <tr>
                            <td class="label">
                                ลักษณะการใช้รถ :
                            </td>
                            <td class="style1">
                                <asp:Label ID="lblCarUseDesc" runat="server" Text=""></asp:Label>
                            </td>                                
                        </tr>
                        <tr>
                            <td class="label">
                                ขับรถวันละ :
                            </td>
                            <td>
                                <asp:Label ID="lblDistance" runat="server" Text=""></asp:Label>
                            </td>                                
                        </tr>
                        <tr>
                            <td class="label">
                                ปรกติใช้รถใน :
                            </td>
                            <td>
                                <asp:Label ID="lblDriveRegion" runat="server" Text=""></asp:Label>
                            </td>                                
                        </tr>
                        <tr>
                            <td class="label">
                                เลขเครื่องยนต์ :
                            </td>
                            <td  >
                                <asp:Label ID="txtEngineNo" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                     <table class="tb-online-insCar">
                        <tr>
                            <td class="label"  >
                                มีตารางกรมธรรม์เดิมหรือไม่ : 
                            </td>
                            <td  >
                                <asp:Label ID="lblFlagOldPolicy" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                ประเภทที่เคยทำประกันภัย :
                            </td>
                            <td  >
                                <asp:Label ID="lblOldPolType" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="label"  >
                                บริษัทที่เคยทำประกันภัย :
                            </td>
                            <td  >
                                <asp:Label ID="lblOldInsuraceComp" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                เลขทะเบียนรถ :
                            </td>
                            <td  >
                                <asp:Label ID="lblCarLicense" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                จังหวัดที่จดทะเบียน :
                            </td>
                            <td  >
                                <asp:Label ID="lblLicenseProvince" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                ขนาดซีซีเครื่องยนต์ :
                            </td>
                            <td >
                                <asp:Label ID="lblCarSize" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                เลขตัวถัง :
                            </td>
                            <td  >
                                <asp:Label ID="txtChasis" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>                        
                     </table>
                </td>
            </tr>
        </table>            
        </div>
        
        <h2 class="subject-detail">รายละเอียดความคุ้มครองหลัก</h2>
        <table width="630px">
            <tr>
                <td valign="top">
                    <table class="tb-online-insCar">   
                        <tr>
                            <td class="label">
                                ความเสียหายต่อร่างกายหรืออนามัย :
                            </td>
                            <td>
                                <asp:Label ID="lbltpbi1" runat="server" Text=""></asp:Label>
                                บาท/คน 
                            </td>
                            <td>
                                <asp:Label ID="lbltpbi2" runat="server" Text=""></asp:Label>
                                บาท/ครั้ง 
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                ความเสียหายต่อทรัพย์สิน :
                            </td>
                            <td>
                                <asp:Label ID="lbltppd" runat="server" Text=""></asp:Label>
                                บาท/ครั้ง
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                ความเสียหายส่วนแรก :
                            </td>
                            <td>
                                <asp:Label ID="lblDeduct" runat="server" Text=""></asp:Label>
                                บาท/ครั้ง
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                ทุนประกันภัย :
                            </td>
                            <td>
                                <asp:Label ID="lblCarOD" runat="server" Text=""></asp:Label>
                                บาท
                            </td>
                        </tr>
                </table>       
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>               
            </tr>
        </table>
        <h2 class="subject-detail">รายละเอียดความคุ้มครองแนบท้าย</h2>
        <table width="630px">
            <tr>
                <td valign="top">
                   <table class="tb-online-insCar">
                        <tr>
                            <td class="label">
                                เสียชีวิต สูญเสียอวัยวะ ทุพพลภาพ :
                            </td>
                            <td>ผู้ขับขี่  1    คน  
                                <asp:Label ID="lblperm_d_01" runat="server" Text=""></asp:Label>
                                บาท
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>ผู้โดยสาร 
                                <asp:Label ID="lblperm_p_num" runat="server" Text=""></asp:Label>
                                 คน   
                                <asp:Label ID="lblperm_p_01" runat="server" Text=""></asp:Label>
                                 บาท/คน
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                ทุพพลภาพชั่วคราว :
                            </td>
                            <td>ผู้ขับขี่  1    คน   
                                <asp:Label ID="lbltemp_d_01" runat="server" Text=""></asp:Label>
                                  บาท/สัปดาห์
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>ผู้โดยสาร  
                                <asp:Label ID="lbltemp_p_num" runat="server" Text=""></asp:Label>
                                  คน                          
                                <asp:Label ID="lbltemp_p_01" runat="server" Text=""></asp:Label>
                                 บาท/คน/สัปดาห์
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                ค่ารักษาพยาบาล :
                            </td>
                            <td>
                                <asp:Label ID="lblcover_02" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                ประกันตัวผู้ขับขี่ :
                            </td>
                            <td>
                                <asp:Label ID="lblcover_03" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        
        <h2 class="subject-detail">รายละเอียดการชำระเงินและการจัดส่งเอกสาร</h2>
        <table width="630px">
            <tr>
                <td valign="top">
                    <div id="divPremium" style="display:inline">
                    <table class="tb-online-insCar">
                        <tr>
                            <td>วันที่เริ่มคุ้มครอง</td>
                            <td>&nbsp;</td>
                            <td class="label"></td>
                            <td>
                                <asp:Label ID="lblStartDate" runat="server" Text=""></asp:Label>
                            </td>                            
                        </tr>
                        <tr>
                            <td>เบี้ยสมัครใจ</td>
                            <td>&nbsp;</td>
                            <td class="label"   >เบี้ยสุทธิ :</td>
                            <td>
                                <asp:Label ID="lblPremium" runat="server" Text=""></asp:Label>
                            </td>
                            <td class="label">อากร :</td>
                            <td>
                                <asp:Label ID="lblStamp" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td class="label"   >ภาษี :</td>
                            <td  >
                                <asp:Label ID="lblVat" runat="server" Text=""></asp:Label>
                            </td>
                            <td class="label">เบี้ยรวม :</td>
                            <td>
                                <asp:Label ID="lblGrossPremium" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>เบี้ย พรบ.</td>            
                            <td></td>
                            <td></td>
                            <td  >
                                <asp:Label ID="lblComPremium" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>เบี้ยรวมทั้งหมด</td>            
                            <td></td>
                            <td></td>
                            <td  >
                                <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>        
                    </div>
                    <div id="div013Premium" style="display:none">
                        <table   class="tb-online-insCar" >                           
                            <tr>
                                <td>เบี้ย Save&save(รวม พรบ.) :</td>            
                                <td></td>
                                <td  >
                                    <asp:Label ID="lblPaid2" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>   
                    </div>            
                </td>
            </tr>
            <tr>
                <td>
                    <table class="tb-online-insCar">                            
                        <tr>
                            <td colspan="6">
                                <table>
                                    <tr>
                                        <td class="label" valign="top">วิธีการชำระเบี้ยประกันภัย :</td>
                                        <td colspan="4">
                                            <asp:Label ID="lblPaidMethod" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label"><div style="display:none">สถานที่จัดส่งเอกสาร :</div></td>
                                        <td colspan="4">
                                            <asp:Label ID="lblSendDoc" runat="server" Text="" Visible="false"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="4">
                                            <asp:Label ID="lblDocAddress1" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>                            
                        </tr>                        
                    </table>
                </td>
            </tr>
        </table>
        
        <strong>ขอบคุณที่ท่านไว้วางใจเลือกใช้บริการกับ บริษัท สินมั่นคงประกันภัย จำกัด (มหาชน)</strong>
        <br />
        <strong>ในระหว่างรอตรวจสอบสภาพรถ บริษัทจะคุ้มครองกรณีอุบัติเหตุ ที่มีคู่กรณีเท่านั้น</strong>
        <br />
        <div style="margin: 0px auto; width: 190px;  margin-top: 15px;">
            <asp:LinkButton ID="LinkButton1" runat="server" OnClick="btnPrint_Click">รูปแบบ pdf</asp:LinkButton>&nbsp;           
            <a href="home.aspx">กลับสู่หน้าแรก</a> <br />
        </div>
    </div>
    <div style="display:none>
        <asp:TextBox ID="txtCampaign" runat="server"></asp:TextBox>
    </div>
    <!-- End  <div class="context page-app"> -->
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

