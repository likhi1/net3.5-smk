using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class UserControl_Calendar : System.Web.UI.Page
{
    public string txtDateObj = "";
    public string title = "";
    public string lang = "";
    public string firstYear = "";
    public string lastYear = "";
    public string postAction = "";
    public string mindate = "";
    public string maxdate = "";

    protected void Page_Load(object sender, EventArgs e)
    {        
        txtDateObj = Request.QueryString["txtDateObj"];
        title = Request.QueryString["title"];
        lang = Request.QueryString["lang"];
        firstYear = Request.QueryString["firstYear"];
        lastYear = Request.QueryString["lastYear"];
        postAction = Request.QueryString["postaction"];
        mindate = Request.QueryString["mindate"];
        maxdate = Request.QueryString["maxdate"];
    }
}
