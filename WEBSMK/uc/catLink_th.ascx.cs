﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---------- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;
using System.Collections;
 


public partial class catLink_th : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //DataSet ds = SelectData(3);
        //DataList3.DataSource = ds;
        //DataList3.DataBind();


        DataList[] arr = { DataList1, DataList2, DataList3 };
        int[] arrCatId = { 1, 2, 3 };
        for (int i = 0; i <= (arrCatId.Length - 1); i++) {
            DataSet ds = SelectData(arrCatId[i]);
            arr[i].DataSource = ds;
            arr[i].DataBind();
        }
        
    }
     
    protected DataSet  SelectData(int _CatId){ 
        string sql1 = "SELECT a.NewsConId , a.NewsCatId ,  a.NewsConTopic   , b.NewsCatName  "
        + "FROM  tbNewsCon a , tbNewsCat b  "
        +"WHERE  a.NewsCatId  = b.NewsCatId  "
        + "AND  a.NewsCatId ='" + _CatId + "'  "
        + "AND a.NewsConStatus='1'   " // check status content
		// + "AND lang = '0' "
        + "AND (GETDATE() between a.NewsConDtShow and a.NewsConDtHide ) "  // Preriod Online
        + "ORDER BY a.NewsConSort   ASC , a.NewsConId  DESC";
        DBClass obj = new DBClass();
        DataSet  ds = obj.SqlGet(sql1 , "tb");

        return ds;
    }

     

    protected void DataList1_ItemDataBound(object sender, DataListItemEventArgs e) {

        HyperLink hpl = (HyperLink)(e.Item.FindControl("hplTopic")) as HyperLink;
        if( hpl != null){
            string id = DataBinder.Eval(e.Item.DataItem, "NewsConId").ToString();
            string cat = DataBinder.Eval(e.Item.DataItem, "NewsCatId").ToString();
            string topic = DataBinder.Eval(e.Item.DataItem, "NewsConTopic").ToString(); 
            string strTopic = ((topic.Length ) <= 25) ? topic : topic.Substring(0, 25) + "..."; 

            hpl.NavigateUrl = "../newsView.aspx?cat=" + cat + "&id=" + id;
            hpl.Text = strTopic ;  


        }
    }

 





}//end class 















