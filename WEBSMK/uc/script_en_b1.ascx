﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="script_en.ascx.cs" Inherits="uc_script_en" %>
<link rel="shortcut icon" href="<%=Config.SiteUrl%>favicon.ico" /> 

<link href="<%=Config.SiteUrl%>css/globalFont_th.css" rel="stylesheet" />
<link href="<%=Config.SiteUrl%>css/globalFont_en.css" rel="stylesheet" />
<script src="<%=Config.SiteUrl%>scripts/jquery-1.9.1.js"></script>   
<script src="<%=Config.SiteUrl%>scripts/jqueryUI/jquery-ui-1.10.3.custom.js"></script>
<link href="<%=Config.SiteUrl%>scripts/jqueryUI/redmond/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
 
<!--================ bxSlider Plugin (SlideShow)  -->  
<script src="<%=Config.SiteUrl%>scripts/jquery.bxslider/jquery.bxslider.js"></script>
<link href="<%=Config.SiteUrl%>scripts/jquery.bxslider/jquery.bxslider.css" rel="stylesheet" /> 
<script>
    $(function () {
        $('.bxslider').bxSlider({
            mode: 'fade',
            auto: true,
            slideWidth: 857,
            speed: 500,
            randomStart: false,
            tickerHover: true,
            controls: false,
            autoHover: true
        })
    });
</script>
<style>
    .bx-controls { position: absolute;  width: 870px; z-index: 100; }
    .bxslider {height:182px; overflow:hidden ; }
</style>



<!--================ DatePicker -->   
<script src="<%=Config.SiteUrl%>scripts/datepicker/_BuddhistEra/scripts/jquery.ui.datepicker-th.js" type="text/javascript" charset="utf-8"></script>	    	
<script src="<%=Config.SiteUrl%>scripts/datepicker/_BuddhistEra/scripts/jquery.ui.datetimepicker.js" type="text/javascript" charset="utf-8"></script>	
<script src="<%=Config.SiteUrl%>scripts/datepicker/_BuddhistEra/scripts/jquery.ui.datepicker.ext.be.js" type="text/javascript" charset="utf-8"></script>	


<style>
#ui-datepicker-div { width: 12em; }
.ui-state-default { font-weight: normal; }
.ui-datepicker td a { padding: 0em; }
.ui-datepicker th { padding: 0em 0em; }
</style> 
