﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;


public partial class uc_slidePic_en : System.Web.UI.UserControl
{
       
        protected void Page_Load(object sender, EventArgs e) {

        BaindData();
        }
     
        protected void BaindData() {
       
        Repeater1.DataSource = SelectData();
        Repeater1.DataBind();
        }
 
        protected DataSet SelectData() {
        string sql1 = "SELECT *  FROM tbBanner "
        + "WHERE  BannerCat = '1' " // Catagory 1 => Banner Slide
        + "AND BannerStatus ='1'  " // check status content
        + "AND lang ='1'  " // TH = 0 ,  En =  1
        //    + "AND  (GETDATE() between  BannerStart and BannerStop ) "  // Preriod Online
        + "ORDER BY BannerSort  ASC , BannerId  DESC";

        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql1, "tb");

        return ds;
        }
     
        protected void RepeaterData_ItemDataBound(object sender, RepeaterItemEventArgs e) {
        Literal ltlPic = (Literal)(e.Item.FindControl("ltlPic"));
        if (ltlPic != null) {
        string BannerId = DataBinder.Eval(e.Item.DataItem, "BannerId").ToString();
        string BannerTitle = DataBinder.Eval(e.Item.DataItem, "BannerTitle").ToString();
        string BannerPic = DataBinder.Eval(e.Item.DataItem, "BannerPic").ToString();
        string BannerLink = DataBinder.Eval(e.Item.DataItem, "BannerLink").ToString();

        ltlPic.Text = "<li><a href='" +  BannerLink + "' target='_self' ><img src='" +  BannerPic + "'  alt='"  + BannerTitle + "'  ></a></li>";
        } 

        } 



}