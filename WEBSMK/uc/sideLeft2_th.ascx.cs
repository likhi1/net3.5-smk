﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;

public partial class uc_sideLeft2_th : System.Web.UI.UserControl
{
    //  protected string PrdId { get { return  Request.QueryString["id"] ; } }
    //protected string Section { get { return Request.QueryString["se"] ; } } 

    //public DataTable dtMinSubmenu ;

    protected int ConId { get {
            int Request = Convert.ToInt32(HttpContext.Current.Request.QueryString["id"]);
            return Request;
    } }
   
    public int CatId {
        get {
            int Request = Convert.ToInt32(HttpContext.Current.Request.QueryString["cat"]); 
            return Request;
        }
    }
     

    protected void Page_Load(object sender, EventArgs e) {  
        BaindData();
    }

    protected void BaindData() {
        //// Set All Banner
        RepeaterBanner.DataSource = SelectDataBanner();
        RepeaterBanner.DataBind();

        ////// Set All SunmenuInvestment  
        //  new SetNavigation(CatId);   // Ex1 - Run method from constructer 
        //  SetNavigation.SelectMinOfSubmenu(); // Ex2 - Run static method   
         
        /////////  
        Repeater repeaterSubNav = FindControl("RepeaterSubNav" + CatId) as Repeater; //  Get Repeater   By   Convert String to Controller 
        repeaterSubNav.DataSource = SetNavInvestment.SelectDataSubmenu(CatId ) ;
        repeaterSubNav.DataBind();  // Create All InvestmentMenu - SubMenu    
    }

    //=================  Select Data
    protected DataSet SelectDataBanner() {
        string sql1 = "SELECT *  FROM tbBanner "
        + "WHERE  BannerCat = '2' " // Catagory 1 => Banner Slide
        + "AND BannerStatus ='1'  " // check status content
        + "AND lang ='0'  " //  TH = 0 ,  En =  1
            // + "AND  (GETDATE() between  BannerStart and BannerStop ) "  // Preriod Online
        + "ORDER BY BannerSort ASC , BannerId  DESC";

        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql1, "tb");

        return ds;
    }
     

    ///////// ItemDataBound RepeaterBanner
    protected void RepeaterBanner_ItemDataBound(object sender, RepeaterItemEventArgs e) {
        Literal ltlBanner = (Literal)(e.Item.FindControl("ltlBanner"));
        if (ltlBanner != null) {
            string BannerId = DataBinder.Eval(e.Item.DataItem, "BannerId").ToString();
            string BannerTitle = DataBinder.Eval(e.Item.DataItem, "BannerTitle").ToString();
            string BannerPic = DataBinder.Eval(e.Item.DataItem, "BannerPic").ToString();
            string BannerLink = DataBinder.Eval(e.Item.DataItem, "BannerLink").ToString();

            ltlBanner.Text = "<li><a href='" + BannerLink + "' target='_self' ><img src='" + BannerPic + "'  alt='" + BannerTitle + "'  ></a></li>";
        }

    }

    ///////// ItemDataBound  Menu Sidebar   
    protected void RepeaterSubmenu_ItemDataBound(object sender, RepeaterItemEventArgs e) { 
        Literal ltlInvSubNav = (Literal)(e.Item.FindControl("ltlInvSubNav"));
        if (ltlInvSubNav != null) {
            int id = Convert.ToInt32( DataBinder.Eval(e.Item.DataItem, "InvId")  );
            int cat = Convert.ToInt32( DataBinder.Eval(e.Item.DataItem, "InvCatId")  );
            string topic = DataBinder.Eval(e.Item.DataItem, "InvTopic").ToString();

            ltlInvSubNav.Text = "<li><a href='" + Config.SiteUrl + "investment.aspx?cat=" + cat + "&id=" + id + "'>" + topic + "</a></li>";

            //////// Set Class Active  If this Submenu Selected
            //if (id == ConId &&  cat ==  CatId ) {
            //    ltlInvSubNav.Text = "<li><a href='#'>" + topic + "</a></li>";
            //}  else {
            //    ltlInvSubNav.Text = "<li><a href='investment.aspx?cat=" + cat + "&id=" + id + "'>" + topic + "</a></li>";
            //} 

        }
         

        //////////////////// EX USE HyperLink 
        //HyperLink hplInvSubNav = (HyperLink)(e.Item.FindControl("hplInvSubNav")) as HyperLink;s
        //if (hplInvSubNav != null) {
        //    int id = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "InvId"));
        //    int cat = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "InvCatId"));
        //    string topic = DataBinder.Eval(e.Item.DataItem, "InvTopic").ToString();

        //    hplInvSubNav.Text = topic;
        //    hplInvSubNav.NavigateUrl = "../investment.aspx?cat=" + cat + "&id=" + id;

        //    //// Set Empty Link  If this Submenu Selected
        //    if (id == Convert.ToInt32( GetFirstId(dtMinSubmenu, cat) ) && CatId == cat) {
        //        hplInvSubNav.NavigateUrl = "#";
        //        //  hplInvSubNav.Attributes.Add("class", "active");s
        //    }
        //    else {
        //        hplInvSubNav.NavigateUrl = "../investment.aspx?cat=" + cat + "&id=" + id;
        //    }
        //    //hplInvSubNav.Target = "_blank"; 
        //}
 

    }

 

}