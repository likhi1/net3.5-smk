﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="script_CKEditorCKFinder.ascx.cs" Inherits="uc_script_CKEditorCKFinder" %>

<!-- ==================  CKEditor -->
<script src="<%=Config.SiteUrl%>ckeditor/ckeditor.js"></script> 
 
<!-- ==================  CKFinder -->
<script src="<%=Config.SiteUrl%>ckfinder/ckfinder.js"></script> 
<script src="<%=Config.SiteUrl%>ckfinder_config/ckfinder_config_popups.js"></script>
