﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="sideLeft.ascx.cs" Inherits="sideLeft" %>

<style>
 #side-left .select-ins { border-radius: 9px; background-color: #f0f0f0; height: auto !important; height: 160px; min-height: 160px; width: 185px; margin-bottom: 10px; background : #f0f0f0 url('../images/icon-search-1.png') no-repeat 7px 3px; padding: 5px; }
#side-left .select-ins h1 { font-family: 'DBHelvethaicaX45Li'; font-size: 25px; color: #000; text-align: right; }
#side-left .select-ins h2 { font-family: 'DBHelvethaicaX45Li'; font-size: 21px; text-align: right; margin-bottom: 0px; padding-bottom: 0px; }
#side-left .online-ins { border-radius: 9px; background-color: #f0f0f0; height: auto !important; height: 160px; min-height: 160px; width: 185px; margin-bottom: 10px; background : #f0f0f0 url('../images/icon-search-2.png') no-repeat right 5px; padding: 5px; }
#side-left .online-ins h1 { font-family: 'DBHelvethaicaX45Li'; font-size: 24px; color: #000; }
#side-left .online-ins h2 { font-family: 'DBHelvethaicaX45Li'; font-size: 21px; text-align: left; margin-bottom: 0px; padding-bottom: 0px; }
#side-left select { width: 183px; } 
</style>

<div class="select-ins">
<h1>เลือกแบบประกันภัย</h1>
<h2>ประเภทประกันภัย</h2>
    <asp:DropDownList ID="DropDownList1" runat="server">
        <asp:ListItem>กรุณาเลือก</asp:ListItem>
    </asp:DropDownList>
<h2>ชนิดประกันภัย</h2>
    <asp:DropDownList ID="DropDownList2" runat="server">
        <asp:ListItem>กรุณาเลือก</asp:ListItem>
    </asp:DropDownList>
    <div><asp:Button ID="Button1" runat="server"   CssClass="btn-selectdata-th" OnClientClick="window.location='products.aspx' ;return false;"    /></div>
    
</div><!-- End class="select-ins" -->

<div class="online-ins">
<h1>ทำประกันรถยนต์ออนไลน์</h1>
<h2>ยี่ห้อรถยนต์</h2>
    <asp:DropDownList ID="DropDownList3" runat="server">
        <asp:ListItem>กรุณาเลือก</asp:ListItem>
    </asp:DropDownList>
<h2>รุ่นรถยนต์</h2>
    <asp:DropDownList ID="DropDownList4" runat="server">
        <asp:ListItem>กรุณาเลือก</asp:ListItem>
    </asp:DropDownList>
    <div><asp:Button ID="Button2" runat="server"   CssClass="btn-confirm-mini-th" OnClientClick="window.location='buyInsureCar_step1.aspx' ;return false;"  /></div>
</div><!-- End class="online-ins" -->
<div class="banner">
  <a href="<%=Config.SiteUrl%>callcenter.aspx">  <img src="images/banCall.png" /></a>
</div>

