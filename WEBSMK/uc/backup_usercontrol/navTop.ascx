﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="navTop.ascx.cs" Inherits="uc_navTop" %>

<script>
    $(function() {
        $("#nav-top ul.subNav").css({ "display": "none" });
        /*$(".subNav").hide();*/
        $("#nav-top ul  li ").hover(function(e) {
            /*
            console.log("already hover"); 
            var $kids = $(e.target.tagName);
            console.log($kids);  
            $(".subNav", this).css({ "border": "1px solid red" });
            $(this).find('ul').slideDown(300); //show its submenu
            */
            $('ul.subNav', this).slideDown(300);  //show its submenu
        },
		function() {
		    $('ul.subNav', this).slideUp(300);  //hide its submenu
		});

    });   // end ready
</script>

<div id="nav-top">
    <ul>
        <li><a href="<%=Config.SiteUrl%>home.aspx" class="first">บริการ &amp; ผลิตภัณฑ์</a>
            <ul class="subNav subNav-01" style="display: none;">
                <li><a href="<%=Config.SiteUrl%>insHealth.aspx">ประกันภัยสุขภาพ</a></li>
                <li><a href="<%=Config.SiteUrl%>insCar.aspx">ประกันภัยรถยนต์</a></li>
                <li><a href="<%=Config.SiteUrl%>insCancer.aspx">ประกันภัยโรคมะเร็ง</a></li>
                <li><a href="<%=Config.SiteUrl%>insAccident.aspx">ประกันภัยอุบัติเหตุ</a></li>
                <li><a href="<%=Config.SiteUrl%>insFire.aspx">ประกันภัยอัคคีภัย</a></li>
                <li><a href="<%=Config.SiteUrl%>insTravel.aspx">ประกันภัยการเดินทาง</a></li>
                <li><a href="<%=Config.SiteUrl%>insOther.aspx">ประกันประเภทอื่นๆ</a></li>
            </ul>
        </li>
        <li><a href="#">ซื้อประกัน</a>
            <ul class="subNav subNav-02" style="display: none;">
<li><a href="<%=Config.SiteUrl%>buyInsure.aspx">สุขภาพ</a></li>
<li><a href="<%=Config.SiteUrl%>buyInsureCar_step1.aspx">รถยนต์</a></li> 
<li><a href="<%=Config.SiteUrl%>buyInsure.aspx">มะเร็ง</a></li>
<li><a href="<%=Config.SiteUrl%>buyInsureAccident.aspx">อุบัติเหตุ</a></li> 
<li><a href="<%=Config.SiteUrl%>buyInsure.aspx">อัคคี</a></li> <!-- form เดิม -->
<li><a href="<%=Config.SiteUrl%>buyInsureTravel.aspx">เดินทาง</a></li> 
<li><a href="<%=Config.SiteUrl%>buyInsure.aspx">อื่นๆ</a></li> <!-- form เดิม -->
 
            </ul>
        </li>
       <li><a href="#">เครือข่าย</a> 
       <ul class="subNav subNav-03" style="display: none;">
            <li><a href="<%=Config.SiteUrl%>network_04.aspx">โรงพยาบาล</a></li>
            <li><a href="<%=Config.SiteUrl%>network_03.aspx">อู่ในสัญญา</a></li>
            <li><a href="<%=Config.SiteUrl%>network_02.aspx">ศูนย์ซ่อมรถ</a></li> 
        <li><a href="<%=Config.SiteUrl%>network_01.aspx">สาขา</a></li>  
         </ul>
        </li>


         <li><a href="#">สมัครงาน</a>
            <ul class="subNav subNav-04" style="display: none;">
            <li><a href="<%=Config.SiteUrl%>jobOfficerRequire.aspx">พนักงาน</a></li>
            <li><a href="<%=Config.SiteUrl%>jobAgentRequire.aspx">ตัวแทน</a></li> 
            </ul>
        </li>


         </li>
        <li><a href="#"   >ข้อมูลนักลงทุน </a>
            <ul class="subNav subNav-05" style="display: none;">
                <li><a href="<%=Config.SiteUrl%>investInfo_01.aspx">ประวัติบริษัทฯ</a></li>
                <li><a href="<%=Config.SiteUrl%>investInfo_02.aspx">คณะกรรมการและผู้บริหาร</a></li>
                <li><a href="<%=Config.SiteUrl%>investInfo_03.aspx">สัดส่วนผู้ถือหุ้นรายใหญ่</a></li>
                <li><a href="<%=Config.SiteUrl%>investInfo_04.aspx">จุดเด่นในรอบปี</a></li>
                <li><a href="<%=Config.SiteUrl%>investInfo_05.aspx">การกำกับดูแลกิจการ</a></li>
                <li><a href="<%=Config.SiteUrl%>investInfo_06.aspx">รายงานผู้สอบบัญชี</a></li>
                <li><a href="<%=Config.SiteUrl%>investInfo_07.aspx">ข้อมูลทางการเงิน</a></li>
                <li><a href="<%=Config.SiteUrl%>investInfo_08.aspx">งบดุล</a></li>
                <li><a href="<%=Config.SiteUrl%>investInfo_09.aspx">งบกำไรขาดทุน</a></li>
                <li><a href="<%=Config.SiteUrl%>investInfo_10.aspx">งบกระแสเงินสด</a></li>
                <li><a href="<%=Config.SiteUrl%>investInfo_11.aspx">สถิติงบการเงิน</a></li>
                <li><a href="<%=Config.SiteUrl%>investInfo_12.aspx">แบบแสดงรายการข้อมูลประจำปี </a></li>
                <li><a href="<%=Config.SiteUrl%>investInfo_13.aspx">มติที่ประชุมคณะกรรมการ/ผู้ถือหุ้น</a></li>
                <li><a href="<%=Config.SiteUrl%>investInfo_14.aspx">หนังสือเชิญประชุมผู้ถือหุ้น</a></li>
                <li><a href="<%=Config.SiteUrl%>investInfo_15.aspx">รายงานการประชุมผู้ถือหุ้น</a></li>
                <li><a href="<%=Config.SiteUrl%>investInfo_16.aspx">การเสนอวาระและชื่อกรรมการ</a></li>
                <li><a href="<%=Config.SiteUrl%>investInfo_17.aspx">รายงานประจำปี</a></li>
                <li><a href="<%=Config.SiteUrl%>investInfo_18.aspx">ฐานะทางการเงินและผลการดำเนินงาน</a></li>
            </ul>
        </li>
        <li><a href="<%=Config.SiteUrl%>faq.aspx"  class="last" >ถาม-ตอบ</a> </li>
  
    </ul>
</div>
<!-- End id="nav-top" -->
<div class="clear"></div>
