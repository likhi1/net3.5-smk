﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="footer.ascx.cs" Inherits="uc_footer" %>

    <div id="footer">
            <div class="footlink">
                <ul>
                    <li><a href="<%=Config.SiteUrl%>home.aspx">บริการ &amp; ผลิตภัณฑ์</a>| </li>
                    <li><a href="<%=Config.SiteUrl%>onlineInsCar.aspx">ซื้อประกัน</a>| </li>
                    <li><a href="<%=Config.SiteUrl%>onlineInsCar.aspx">เครือข่าย</a>| </li>  
                    <li><a href="<%=Config.SiteUrl%>faq.aspx">ถาม-ตอบ</a>| </li> 
                    <li><a href="#">แผนผังเว็บไซต์</a></li>
                </ul>
            </div><!--end class="footlink" -->
            <div class="copy">
                Copyright © 2013 Syn Mun Kong Public Co.,Ltd All Rights Reserved.
            </div>
            <div class="policy">
                <ul>
                    <li><a href="#">ข้อตกลงและเงื่อนไขการใช้บริการ </a>| </li>
                    <li><a href="#">นโยบายรักษาความปลอดภัย</a></li>
                </ul>
            </div>
        </div>
        <!-- End id="footer"-->