﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="catLink.ascx.cs" Inherits="catLink" %>
<div class="cat-link-warp">

<div class="cat-link cat-link-margin">
<h1 class="icon1" >สาระประกันภัย</h1>
<div class="row-list">
<ul> 
<li><a href="<%=Config.SiteUrl%>knowledge/knowledgeView_01.aspx">ประกันภัยรถยนต์ภาคสมัครใจ</a></li>
<li><a href="<%=Config.SiteUrl%>knowledge/knowledgeView_02.aspx">ประกันภัยรถยนต์ภาคบังคับ</a></li>
<li><a href="<%=Config.SiteUrl%>knowledge/knowledgeView_03.aspx">ประกันภัยเบ็ดเตล็ด</a></li>
<li><a href="<%=Config.SiteUrl%>knowledge/knowledgeView_04.aspx">ประกันภัยทางทะเลและการขนส่ง</a></li>
<li><a href="<%=Config.SiteUrl%>knowledge/knowledgeView_05.aspx">ประกันอัคคีภัย</a></li>
<li><a href="<%=Config.SiteUrl%>knowledge/knowledgeView_06.aspx">ประกันภัยรถยนต์มีมากกว่าชั้น1</a></li> 
</ul>
</div>
<div class="btn-more"><a href="<%=Config.SiteUrl%>knowledge/knowledgeList.aspx">&raquo; ดูทั้งหมด </a></div>
</div><!-- End class="cat-link" -->

<div class="cat-link  cat-link-margin">
<h1 class="icon2" >ข่าวสารประกันภัย</h1>
<div class="row-list">
<ul>
<li><a href="#">ข่าวจัดอบรมตัวแทน</a></li>
<li><a href="#">ข่าวกิจกรรม</a></li>
 
  
</ul>
</div>
<div class="btn-more"><a href="<%=Config.SiteUrl%>knowledge/knowledgeList.aspx">&raquo;  ดูทั้งหมด </a></div>
</div><!-- End class="cat-link" -->


<div class="cat-link  cat-link-margin">
<h1 class="icon3" >โฆษณาสินมั่นคง</h1>
<div class="row-list">
<ul> 
<li><a href="#">สินมั่นคงประกันตามฟิต</a></li>
<li><a href="#">สินมั่นคงประกันภัย - 3 บาท</a></li>
<li><a href="#">สินมั่นคงประกันภัยเร็ว</a></li> 
</ul>
</div>
<div class="btn-more"><a href="<%=Config.SiteUrl%>knowledge/knowledgeList.aspx">&raquo;  ดูทั้งหมด</a></div>
</div><!-- End class="cat-link" -->

<div class="cat-link">
<h1 class="icon4" >ลูกค้าสัมพันธ์</h1>
<div class="row-list">
<ul>
<li><a href="suppport_01.aspx">วิธีการชำระเบี้ย</a></li>
<li><a href="suppport_02.aspx">ร้องเรียน</a></li>
<li><a href="suppport_03.aspx">การจ่ายค่าสินไหม</a></li>

 
 
</ul>
</div>
<div class="btn-more"><a href="<%=Config.SiteUrl%>knowledge/knowledgeList.aspx">&raquo;  ดูทั้งหมด</a></div>
</div><!-- End class="cat-link" -->
<div class="clear"></div>
</div><!-- End class="cat-link-warp" -->
 