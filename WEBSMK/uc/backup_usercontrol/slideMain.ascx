﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="slideMain.ascx.cs" Inherits="uc_slideMain" %>

 <!--###### Key  Slide Show ######## -->
   <link rel='stylesheet' type='text/css' href='<%=Config.SiteUrl%>css/camera-sinmon.css''  >

    <script type='text/javascript' src="<%=Config.SiteUrl%>js/camera/scripts/jquery.mobile.customized.min.js"></script>

    <script type='text/javascript' src="<%=Config.SiteUrl%>js/camera/scripts/jquery.easing.1.3.js"></script>

    <script type='text/javascript' src="<%=Config.SiteUrl%>js/camera/scripts/camera.min.js"></script>

    <script>
        $(function () {
            $('#camera_wrap_1').camera({
                navigation: false,
                playPause: false,
                thumbnails: true,
                width: '870',
                height: '185',
                hover: true,
                thumbnails: true,
                fx: 'simpleFade',
                time: 5000,
                transPeriod: 1500,
                loader: 'none'
            });
        });
    </script>


 <div style="position: relative; ">
            <div class="camera_wrap camera_azure_skin" id="camera_wrap_1">
                <div data-thumb="images/key/key-4-tmb.jpg" data-src="images/key/key-4.jpg" data-link="speedApp.aspx">
                </div>
                <div data-thumb="images/key/key-1-tmb.jpg" data-src="images/key/key-1.jpg" class="camera_gold_skin"
                    data-link="insCarPrd_01.aspx">
                </div>
                <div data-thumb="images/key/key-2-tmb.jpg" data-src="images/key/key-2.jpg" data-link="insHealthPrd_01.aspx">
                </div>
                <div data-thumb="images/key/key-3-tmb.jpg" data-src="images/key/key-3.jpg" data-link="insHealthPrd_02.aspx">
                </div>
            </div>
        </div>
        <!-- End  #camera_wrap_1 -->

