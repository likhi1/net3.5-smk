﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class  uc_navTop_th : System.Web.UI.UserControl
{
    protected int ConId {
        get {
            int Request = Convert.ToInt32(HttpContext.Current.Request.QueryString["id"]);
            return Request;
        }
    }

    public int CatId {
        get {
            int Request = Convert.ToInt32(HttpContext.Current.Request.QueryString["cat"]);
            return Request;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    { 
        ///////// Set Datable for every where use 
        SetNavInvestment.Language = 0;
        SetNavInvestment.SelectMinIdByCat();  //  Select database to Datatable   by   Language 
        // new SetNavInvestment(CatId);   // Ex1 - If Want  Run method from constructer 

    }
}
