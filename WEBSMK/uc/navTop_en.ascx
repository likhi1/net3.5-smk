﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="navTop_en.ascx.cs" Inherits="uc_navTop_en" %>


<script>
    $(function () {
        //========== Set Event for Button  
        $("#nav-top > ul > li").each(function () { //  prevent li Level2
            var obj = $(this);
            var sub = obj.find('.subNav');
            var btLevel1 = obj.children('a');

            // command all sub hide
               sub.attr('style', "display: none;");

            obj.hover(function () {
                // obj.addClass('selected');
                btLevel1.addClass('selected');
                sub.slideDown();  //show its submenu 
            }, function () {
                //obj.removeClass('selected');
                btLevel1.removeClass('selected');
               myTimer = setTimeout(function () { sub.slideUp(); }, 100);
            })
        })

    });   // end ready
</script>
<div id="nav-top">
    <ul>
        <li class="first"><a href="<%=Config.SiteUrl%>en/home.aspx" class="first">Services &amp;  Products</a>
            <ul class="subNav subNav-01">
                <li><a href="<%=Config.SiteUrl%>en/product.aspx?cat=1">Health Insurance</a></li>
                <li><a href="<%=Config.SiteUrl%>en/product.aspx?cat=2">Car Insurance</a></li>
                <li><a href="<%=Config.SiteUrl%>en/product.aspx?cat=3">Cancer Insurance</a></li>
                <li><a href="<%=Config.SiteUrl%>en/product.aspx?cat=4">Accident Insurance</a></li>
                <li><a href="<%=Config.SiteUrl%>en/product.aspx?cat=5">Fire Insurance</a></li>
                <li><a href="<%=Config.SiteUrl%>en/product.aspx?cat=6">Travel Insurance</a></li>
                <li><a href="<%=Config.SiteUrl%>en/product.aspx?cat=7">Other Insurance</a></li> 
            </ul>
        </li>

        <li><a href="#">Buy Insurence</a>
            <ul class="subNav subNav-02"> 
                <li><a href="<%=Config.SiteUrl%>buyInsure.aspx?main_class=h">Health</a></li>
                <li><a href="<%=Config.SiteUrl%>buyCompulsary_step1.aspx">Compulsary</a></li>
                <li><a href="<%=Config.SiteUrl%>buyInsure.aspx?main_class=c">Cancer</a></li>
                <li><a href="<%=Config.SiteUrl%>buyVoluntary_step1.aspx">Voluntary</a></li>
                <li><a href="<%=Config.SiteUrl%>buyInsurePA_step1.aspx">Accident</a></li>
                <li><a href="<%=Config.SiteUrl%>buyInsure.aspx?main_class=f">Fire</a></li>
                <li><a href="<%=Config.SiteUrl%>buyInsureTravel_step1.aspx">Travel</a></li>
                <li><a href="<%=Config.SiteUrl%>buyInsure.aspx?main_class=o">Other</a></li>
            </ul>
        </li>

        <li><a href="#">Network</a>
            <ul class="subNav subNav-03">
                <li><a href="<%=Config.SiteUrl%>en/network.aspx?cat=1&zone=1">Hospitals</a></li>
                <li><a href="<%=Config.SiteUrl%>en/network.aspx?cat=2&zone=1">Garages</a></li>
                <li><a href="<%=Config.SiteUrl%>en/network.aspx?cat=3&zone=1">RepairCenters</a></li>
                <li><a href="<%=Config.SiteUrl%>en/network.aspx?cat=4&zone=0">Branchs</a></li>
            </ul>
        </li> 

        <li><a href="#">Apply Job & Agent</a>
            <ul class="subNav subNav-04" style="display: none;">
                <li><a href="<%=Config.SiteUrl%>en/jobRequire.aspx?cat=1">Officer</a></li>
                <li><a href="<%=Config.SiteUrl%>en/jobRequire.aspx?cat=2">Agent</a></li>
            </ul>
        </li>

<li><a href="#">Investor Relations </a> 
<ul class="subNav subNav-05">
<li><a href="<%=Config.SiteUrl%>en/investment.aspx?cat=1&id=<%= SetNavInvestment.GetFirstId( 1 )%>">About SMK </a></li>
<li><a href="<%=Config.SiteUrl%>en/investment.aspx?cat=2&id=<%= SetNavInvestment.GetFirstId( 2 )%>">Finance Information</a></li>
<li><a href="<%=Config.SiteUrl%>en/investment.aspx?cat=3&id=<%= SetNavInvestment.GetFirstId( 3 )%>">Annual Report</a></li>
<li><a href="<%=Config.SiteUrl%>en/investment.aspx?cat=4&id=<%= SetNavInvestment.GetFirstId( 4 )%>">Corporate Governance</a></li>
<li><a href="<%=Config.SiteUrl%>en/investment.aspx?cat=5&id=<%= SetNavInvestment.GetFirstId( 5 )%>">Investment Information</a></li>
</ul>
</li>

         
       
        <li class="last"><a href="<%=Config.SiteUrl%>en/faq.aspx" class="last">F.A.Q.</a> </li>

    </ul>
</div>
<!-- End id="nav-top" -->
<div class="clear"></div>
