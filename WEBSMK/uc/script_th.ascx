﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="script_th.ascx.cs" Inherits="uc_jsLibrary"   %>  
<link rel="shortcut icon" href="<%=Config.SiteUrl%>favicon.ico" /> 

<link  rel="stylesheet" href="<%=Config.SiteUrl%>css/globalFont_th.css"/>
<script src="<%=Config.SiteUrl%>scripts/jquery-1.9.1.js"></script>   

<script src="<%=Config.SiteUrl%>scripts/jqueryUI/jquery-ui-1.10.3.custom.js"></script>
<link href="<%=Config.SiteUrl%>scripts/jqueryUI/redmond/jquery-ui-1.10.3.custom.css" rel="stylesheet" />


<!--================ bxSlider Plugin (SlideShow)  -->  
<script src="<%=Config.SiteUrl%>scripts/jquery.bxslider/jquery.bxslider.js"></script>
<link href="<%=Config.SiteUrl%>scripts/jquery.bxslider/jquery.bxslider.css" rel="stylesheet" /> 
 
<script>

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)

  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
 
  ga('create', 'UA-51271540-1', 'smk.co.th');
  ga('send', 'pageview');

</script>

<script>
    $(function () {
        $('.bxslider').bxSlider({
            mode: 'fade',
            auto: true,
            slideWidth: 857,
            speed: 500,
            randomStart: false,
            tickerHover: true,
            controls: false,
            autoHover: true
        })
    });
</script>



<style>
.bx-controls { position:absolute;  width:870px ; z-index:100;   }
.bxslider {height:182px; overflow:hidden ; }
</style> 
 

<!--================ DatePicker -->  
<script src="<%=Config.SiteUrl%>scripts/datepicker/_BuddhistEra/scripts/jquery.ui.datepicker-th.js" type="text/javascript" charset="utf-8"></script>	    	
<script src="<%=Config.SiteUrl%>scripts/datepicker/_BuddhistEra/scripts/jquery.ui.datetimepicker.js" type="text/javascript" charset="utf-8"></script>	
<script src="<%=Config.SiteUrl%>scripts/datepicker/_BuddhistEra/scripts/jquery.ui.datepicker.ext.be.js" type="text/javascript" charset="utf-8"></script>	

<style>
#ui-datepicker-div { width: 12em; }
.ui-state-default { font-weight: normal; }
.ui-datepicker td a { padding: 0em; }
.ui-datepicker th { padding: 0em 0em; }
</style> 


