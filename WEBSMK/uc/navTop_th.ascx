﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="navTop_th.ascx.cs" Inherits="uc_navTop_th" %>


<script>
    $(function () {
        //========== Set Event for Button  
        $("#nav-top > ul > li").each(function () { //  prevent li Level2
            var obj = $(this);
            var sub = obj.find('.subNav');
            var btLevel1 = obj.children('a');

            // command all sub hide
              sub.attr('style', "display: none;");

            obj.hover(function () {
                // obj.addClass('selected');
                btLevel1.addClass('selected');
                sub.slideDown();  //show its submenu 
            }, function () {
                //obj.removeClass('selected');
                btLevel1.removeClass('selected');
                myTimer = setTimeout(function () { sub.slideUp(); }, 300);
            })
        })

    });   // end ready
</script>

<div id="nav-top">
    <ul>
        <li class="first"><a href="<%=Config.SiteUrl%>home.aspx" class="first">บริการ &amp; ผลิตภัณฑ์</a>
            <ul class="subNav subNav-01">
                <li><a href="<%=Config.SiteUrl%>product.aspx?cat=1">ประกันภัยสุขภาพ</a></li>
                <li><a href="<%=Config.SiteUrl%>product.aspx?cat=2">ประกันภัยรถยนต์</a></li>
                <li><a href="<%=Config.SiteUrl%>product.aspx?cat=3">ประกันภัยโรคมะเร็ง</a></li>
                <li><a href="<%=Config.SiteUrl%>product.aspx?cat=4">ประกันภัยอุบัติเหตุ</a></li>
                <li><a href="<%=Config.SiteUrl%>product.aspx?cat=5">ประกันภัยอัคคีภัย</a></li>
                <li><a href="<%=Config.SiteUrl%>product.aspx?cat=6">ประกันภัยการเดินทาง</a></li>
                <li><a href="<%=Config.SiteUrl%>product.aspx?cat=7">ประกันประเภทอื่นๆ</a></li>
            </ul>
        </li>
        <li><a href="#">ซื้อประกัน</a>
            <ul class="subNav subNav-02">
                <li><a href="<%=Config.SiteUrl%>buyInsure.aspx?main_class=h">สุขภาพ</a></li>
                <li><a href="<%=Config.SiteUrl%>buyCompulsary_step1.aspx">รถยนต์ ภาคบังคับ</a></li>
                <li><a href="<%=Config.SiteUrl%>buyInsure.aspx?main_class=c">มะเร็ง</a></li>
                <li><a href="<%=Config.SiteUrl%>buyVoluntary_step1.aspx">รถยนต์ ภาคสมัครใจ</a></li>
                <li><a href="<%=Config.SiteUrl%>buyInsurePA_step1.aspx">อุบัติเหตุ</a></li>
                <li><a href="<%=Config.SiteUrl%>buyInsure.aspx?main_class=f">อัคคีภัย</a></li>
                <li><a href="<%=Config.SiteUrl%>buyInsureTravel_step1.aspx">เดินทาง</a></li>
                <li><a href="<%=Config.SiteUrl%>buyInsure.aspx?main_class=o">อื่นๆ</a></li>

            </ul>
        </li>
        <li><a href="#">เครือข่าย</a>
            <ul class="subNav subNav-03">
                <li><a href="<%=Config.SiteUrl%>network.aspx?cat=1&zone=1">โรงพยาบาล</a></li>
                <li><a href="<%=Config.SiteUrl%>network.aspx?cat=2&zone=1">อู่ในสัญญา</a></li>
                <li><a href="<%=Config.SiteUrl%>network.aspx?cat=3&zone=1">ศูนย์ซ่อมรถ</a></li>
                <li><a href="<%=Config.SiteUrl%>network.aspx?cat=4&zone=0">สาขา</a></li>
            </ul>
        </li>



        <li><a href="#">สมัครงาน/ตัวแทน</a>
            <ul class="subNav subNav-04">
                <li><a href="<%=Config.SiteUrl%>jobRequire.aspx?cat=1">พนักงาน</a></li>
                <li><a href="<%=Config.SiteUrl%>jobRequire.aspx?cat=2">ตัวแทน</a></li>
            </ul>
        </li>


        <li><a href="#">ข้อมูลนักลงทุน </a> 
            <ul class="subNav subNav-05">
                <li><a href="<%=Config.SiteUrl%>investment.aspx?cat=1&id=<%= SetNavInvestment.GetFirstId( 1 )%>">รู้จัก SMK</a></li>
                <li><a href="<%=Config.SiteUrl%>investment.aspx?cat=2&id=<%= SetNavInvestment.GetFirstId( 2 )%>">ข้อมูลทางการเงิน</a></li>
                <li><a href="<%=Config.SiteUrl%>investment.aspx?cat=3&id=<%= SetNavInvestment.GetFirstId( 3 )%>">รายงานประจำปี</a></li>
                <li><a href="<%=Config.SiteUrl%>investment.aspx?cat=4&id=<%= SetNavInvestment.GetFirstId( 4 )%>">การกำกับดูแลกิจการที่ดี</a></li>
                <li><a href="<%=Config.SiteUrl%>investment.aspx?cat=5&id=<%= SetNavInvestment.GetFirstId( 5 )%>">ข้อมูลสำหรับผู้ถือหุ้น</a></li>

            </ul>
        </li>
        <li class="last"><a href="<%=Config.SiteUrl%>faq.aspx" class="last">ถาม-ตอบ</a> </li>

    </ul>
</div>
<!-- End id="nav-top" -->
<div class="clear"></div>
