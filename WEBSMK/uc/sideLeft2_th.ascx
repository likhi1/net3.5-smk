﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="sideLeft2_th.ascx.cs" Inherits="uc_sideLeft2_th" %>
 
<div class="banner">
<a href="<%=Config.SiteUrl%>buyVoluntary_step1.aspx">
<img src="<%=Config.SiteUrl%>images/banner-th/banner-insure-online.gif" /></a>
</div>

 
<div id="navInvestment">
    <div class="title">ข้อมูลนักลงทุน</div>
     

    <h3><% Response.Write(SetNavInvestment.SetLinkMenuLevelOne(1, "รู้จัก SMK")); %></h3>
    <% if (CatId == 1) { %>
    <ul>
        <asp:Repeater ID="RepeaterSubNav1" runat="server" OnItemDataBound="RepeaterSubmenu_ItemDataBound">
            <ItemTemplate> 
                  <asp:Literal ID="ltlInvSubNav" runat="server"></asp:Literal> 
            </ItemTemplate>
        </asp:Repeater>
    </ul>
    <% }  %>

    <h3><% Response.Write(SetNavInvestment.SetLinkMenuLevelOne(2, "ข้อมูลทางการเงิน")); %> </h3>
    <% if (CatId == 2) { %>
    <ul>
        <asp:Repeater ID="RepeaterSubNav2" runat="server" OnItemDataBound="RepeaterSubmenu_ItemDataBound">
            <ItemTemplate> 
                   <asp:Literal ID="ltlInvSubNav" runat="server"></asp:Literal> 
            </ItemTemplate>
        </asp:Repeater>
    </ul>
    <% }  %>
    <h3><% Response.Write(SetNavInvestment.SetLinkMenuLevelOne(3, "รายงานประจำปี")); %> </h3>
    <% if (CatId == 999) { %>
    <ul>
        <asp:Repeater ID="RepeaterSubNav3" runat="server" OnItemDataBound="RepeaterSubmenu_ItemDataBound">
            <ItemTemplate>
               <asp:Literal ID="ltlInvSubNav" runat="server"></asp:Literal> 
            </ItemTemplate>
        </asp:Repeater>
    </ul>
    <% }  %>

    <h3><% Response.Write(SetNavInvestment.SetLinkMenuLevelOne(4, "การกำกับดูแลกิจการที่ดี")); %> </h3>
    <% if (CatId == 999) { %>
    <ul>
        <asp:Repeater ID="RepeaterSubNav4" runat="server" OnItemDataBound="RepeaterSubmenu_ItemDataBound">
            <ItemTemplate>
              <asp:Literal ID="ltlInvSubNav" runat="server"></asp:Literal> 
            </ItemTemplate>
        </asp:Repeater>
    </ul>
    <% }  %>

    <h3><% Response.Write(SetNavInvestment.SetLinkMenuLevelOne(5, "ข้อมูลสำหรับผู้ถือหุ้น")); %> </h3>
    <% if (CatId == 999) { %>
    <ul>
        <asp:Repeater ID="RepeaterSubNav5" runat="server" OnItemDataBound="RepeaterSubmenu_ItemDataBound">
            <ItemTemplate>
                  <asp:Literal ID="ltlInvSubNav" runat="server"></asp:Literal> 
            </ItemTemplate>
        </asp:Repeater>
    </ul>
    <% }  %>
</div>
<!-- /#navInvestment -->


<div class="speedApp">
    <div class="speedApp-title">
        <div style="font-size: 21px; color: #B31044; margin-bottom: 3px;">SMK Speed App โหลดฟรี</div>
        <div style="font-size: 19px; line-height: 18px; margin-bottom: 2px;">
            ทั้ง
     iOS และ Andriod
        </div>
    </div>
    <div>
        <a href="https://itunes.apple.com/th/app/smk-speed-app/id621145200?mt=8" target="_blank">
            <img src="<%=Config.SiteUrl%>images/iconAppStore.png" width="85px" /></a> &nbsp;
 <a href="https://play.google.com/store/apps/details?id=app.android.assurance&hl=en" target="_blank">
     <img src="<%=Config.SiteUrl%>images/iconGooglePlay.png" width="85px" /></a>
    </div>
    <div style="font-size: 12px; line-height: 15px; font-family: Tahoma;">
        Tablet เวอร์ชั่น<br />
        อยู่ระหว่างการปรับปรุง
    </div>
</div>

<div class="banner">
    <a href="<%=Config.SiteUrl%>callcenter.aspx">
        <img src="<%=Config.SiteUrl%>images/banner-th/banner1.png" />
    </a>
</div>

<div class="banner">
    <a href="<%=Config.SiteUrl%>news.aspx?cat=4">
        <img src="<%=Config.SiteUrl%>images/banner-th/banner2.png" />
    </a>
</div>



<asp:Repeater ID="RepeaterBanner" runat="server" OnItemDataBound="RepeaterBanner_ItemDataBound">
    <ItemTemplate>

        <div>
            <asp:Literal ID="ltlBanner" runat="server"></asp:Literal>
        </div>

    </ItemTemplate>
</asp:Repeater>




<script>

    ////////////////// Get Paramiter from QueryString
    function getParameterByName(name, url) { 
        var match = RegExp('[?&]' + name + '=([^&]*)').exec(url); 
        return match && decodeURIComponent(match[1].replace(/\+/g, ' ')); 
    }

    ////////////////// Set Class Active In Navigation 
    $(function(){  
        $('#navInvestment li a').each(function() { // Loop
            var href = $(this).attr('href');
            var linkId = getParameterByName('id', href); // get id from link 

            <%  
              int intId = Convert.ToInt32(Request.QueryString["id"]); 
           %>  
           var querystringId =  <%= intId %> ; 
            if (linkId ==  querystringId ){ 
                $(this).addClass("active"); // add class after selected
                $(this).attr("href" , "#") ; // replace empty link after selected
            }
         }) // end each
    }); // end ready

</script> 
