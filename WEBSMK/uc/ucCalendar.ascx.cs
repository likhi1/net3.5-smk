using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class UserControl_ucCalendar : System.Web.UI.UserControl
{
    public string Title = "Calendar";
    public string lang = "th";
    public int firstYear = DateTime.Now.Year - 50;
    public int lastYear = DateTime.Now.Year + 50;
    public string Text = "";
    public string textboxName = "";
    public string postAction = "";
    public string mindate = "";
    public string maxdate = "";
    public string strAppName = System.Configuration.ConfigurationManager.AppSettings["appName"];

    private void Page_Init(object sender, EventArgs e)
    {
        if (Request.Form[textboxName] != null)
            Text = Request.Form[textboxName];

        if (Title == "")
            Title = "Calendar";

        if (lang == "")
            lang = "th";

        if (firstYear > lastYear)
            lastYear = firstYear;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        Page_Init(sender, e);
        string litDateCalendarCode = "";
        litDateCalendarCode += "<SCRIPT LANGUAGE=\"JavaScript\">\n";
        litDateCalendarCode += "<!--\n";
        litDateCalendarCode += "function openCalendar" + textboxName + "(txtCalendarName){\n";
        //litDateCalendarCode += " alert(\"/" + strAppName + "/adminweb/uc/Calendar.aspx?txtDateObj=document.all.\"+txtCalendarName+\"&title=" + this.Title + "&postaction=" + this.postAction + "&lang=" + this.lang + "&firstYear=" + this.firstYear + "&lastYear=" + this.lastYear + "&mindate=" + this.mindate + "&maxdate=" + this.maxdate + "\");\n";
        litDateCalendarCode += "	showModalDialog(\"/" + strAppName + "/uc/Calendar.aspx?txtDateObj=document.all.\"+txtCalendarName+\"&title=" + this.Title + "&postaction=" + this.postAction + "&lang=" + this.lang + "&firstYear=" + this.firstYear + "&lastYear=" + this.lastYear + "&mindate=" + this.mindate + "&maxdate=" + this.maxdate + "\",window,\"dialogHeight:250px;dialogWidth:270px;help:no;status:no;\");\n";
        litDateCalendarCode += "	//open(\"/" + strAppName + "/uc/Calendar.aspx?txtDateObj=document.all.\"+txtCalendarName+\"&title=" + this.Title + "&postaction=" + this.postAction + "&lang=" + this.lang + "&firstYear=" + this.firstYear + "&lastYear=" + this.lastYear + "&mindate=" + this.mindate + "&maxdate=" + this.maxdate + "\");\n";
        litDateCalendarCode += "}\n";
        litDateCalendarCode += "//-->\n";
        litDateCalendarCode += "</SCRIPT>\n";

        litDateCalendar.Text = litDateCalendarCode;
    }
}
