﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="catLink_th.ascx.cs" Inherits="catLink_th" %>
<div class="cat-link-warp">

<div class="cat-link cat-link-margin">
<h1 class="icon1" >สาระประกันภัย</h1>
<div class="row-list">
<ul> 
<asp:DataList ID="DataList1" runat="server" OnItemDataBound="DataList1_ItemDataBound"> 
<ItemTemplate> 
<li>
<asp:HyperLink ID="hplTopic" runat="server">HyperLink</asp:HyperLink> 
</li> 
</ItemTemplate> 
</asp:DataList>
</ul>

</div>
<div class="btn-more"><a href="<%=Config.SiteUrl%>news.aspx?cat=1">&raquo; ดูทั้งหมด </a></div>
</div><!-- End class="cat-link" -->

<div class="cat-link  cat-link-margin">
<h1 class="icon2" >ข่าวสาร</h1>
    

 
<div class="row-list">
<ul> 
<asp:DataList ID="DataList2" runat="server" OnItemDataBound="DataList1_ItemDataBound"> 
<ItemTemplate> 
<li>
<asp:HyperLink ID="hplTopic" runat="server">HyperLink</asp:HyperLink> 
</li> 
</ItemTemplate> 
</asp:DataList>
</ul>

</div>
<div class="btn-more"><a href="<%=Config.SiteUrl%>news.aspx?cat=2">&raquo;  ดูทั้งหมด </a></div>
</div><!-- End class="cat-link" -->


<div class="cat-link  cat-link-margin"">
<h1 class="icon3" >โฆษณาสินมั่นคง</h1>
<div class="row-list">
<ul> 
<asp:DataList ID="DataList3" runat="server" OnItemDataBound="DataList1_ItemDataBound"> 
<ItemTemplate> 
<li>
<asp:HyperLink ID="hplTopic" runat="server">HyperLink</asp:HyperLink> 
</li> 
</ItemTemplate> 
</asp:DataList>
</ul>
</div>
<div class="btn-more"><a href="<%=Config.SiteUrl%>news.aspx?cat=3">&raquo;  ดูทั้งหมด</a></div>
</div><!-- End class="cat-link" -->


<div class="cat-link  ">
<h1 class="icon4" >วิธีการชำระเงิน </h1>
<div class="row-list">

<ul style="color:#485ED1">  
<li>ชำระที่สำนักงานใหญ่ สินมั่นคง </li>
<li>ชำระที่สาขาสินมั่นคง </li> 
<li>โอนผ่านธนาคาร </li>
<li >บัตรเครดิต </li>
</ul>

</div>
<div class="btn-more"><a href="<%=Config.SiteUrl%>payment.aspx">&raquo;  ดูทั้งหมด</a></div>
</div><!-- End class="cat-link" -->


 

<div class="clear"></div>
</div><!-- End class="cat-link-warp" -->
 