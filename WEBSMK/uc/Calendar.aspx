<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Calendar.aspx.cs" Inherits="UserControl_Calendar" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml" >
	<head runat="server">
		<title>
			<%=title%>
		</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<STYLE type="text/css">
BODY {
	margin: 5px;
	font-family: ms sans serif;
	font-size: 10px;
	background-color: #FFFFFF;
}
TABLE,TD {
	background-color: #FFFFEE;
	border-collapse: collapse;
	border: solid 1px #6666FF;
	font-family: ms sans serif;
	font-size: 10px;
	text-align: center;
}
TR {
	height:20px;
}
SPAN {
	font-family: ms sans serif;
	font-size: 10px;
	font-weight: bold;
	color: #333399;
}
.header {
	background-color: #99CCFF;
	font-weight: bold;
	color: #993333;
}
.numText {
	background-color: #CCCCFF;
	cursor: hand;
}
.numTextOver {
	background-color: #99FFFF;
	border: outset 1px #6666FF;
	cursor: hand;
}
.selectedText {
	background-color: #9966FF;
	cursor: hand;
}
		</STYLE>
		<SCRIPT LANGUAGE="JavaScript">
<!--
var lang = "<%=lang%>";
var today = new Date();
var minDate = new Date();
var maxDate = new Date();
//var txtDateObj = eval("opener.<%=txtDateObj%>");// Normal window
var txtDateObj = eval("dialogArguments.<%=txtDateObj%>");// Normal window

// Year
var firstYear = <%=firstYear%>;
var lastYear = <%=lastYear%>;
var totalYear = lastYear-firstYear+1;
var yearValues = new Array(totalYear);
var yearNames = new Array(totalYear);
for(var i=firstYear,j=0; i<=lastYear; i++,j++){
	yearValues[j] = i;
	yearNames[j] = (lang == "en") ? i : ((lang == "th") ? i+543 : i );
}

// Month
var monthValues = ["1","2","3","4","5","6","7","8","9","10","11","12"];
var monthNames = new Array(12);
if (lang == "en") {
	monthNames = ["January","February","March","April","May","June","July","August","September","October","November","December"];
} else if (lang == "th") {
	monthNames = ["���Ҥ�","����Ҿѹ��","�չҤ�","����¹","����Ҥ�","�Զع�¹","�á�Ҥ�","�ԧ�Ҥ�","�ѹ��¹","���Ҥ�","��Ȩԡ�¹","�ѹ�Ҥ�"];
}

// Day
var daytNames = new Array(7);
if (lang == "en") {
	daytNames = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
} else if (lang == "th") {
	daytNames = ["��.","�.","�.","�.","��.","�.","�."];
}

// Initial data
if (txtDateObj.value != "__/__/____") {
	var dateArray = txtDateObj.value.split("/");
	dateArray[2] = lang == "en" ? dateArray[2] : ((lang == "th") ? ""+(parseInt(dateArray[2])-543) : dateArray[2]) ;
	if (parseFloat(dateArray[2]) > lastYear)
	    today = new Date(lastYear,parseFloat(dateArray[1])-1,dateArray[0]);
	else
	    today = new Date(dateArray[2],parseFloat(dateArray[1])-1,dateArray[0]);
}
if (today.getFullYear() > lastYear){
    today = new Date(lastYear,today.getMonth()+1,today.getDate());	
}

// Label
var monthYearLabel = new Array(2);
if (lang == "en") {
	monthYearLabel = ["Month","Year"];
} else if (lang == "th") {
	monthYearLabel = ["��͹","��"];
}

function initialData(){
	assignValueToDropdown(frmCalendar.cboMonth, monthValues, monthNames);
	assignValueToDropdown(frmCalendar.cboYear, yearValues, yearNames);

	frmCalendar.cboMonth.value = ""+(today.getMonth()+1);
	frmCalendar.cboYear.value = today.getFullYear();
}

function assignValueToDropdown(srcObj, values, texts){
	srcObj.length = values.length;
	for(var i=0; i<values.length; i++){
		srcObj.options[i].value = values[i];
		srcObj.options[i].text = texts[i];
	}
}

function getNumDaysInMonthYear(year, month){
	var numDaysInMonth = ["31","28","31","30","31","30","31","31","30","31","30","31"];
	var numDays;
	numDays = numDaysInMonth[month]
	if (month == 1) {
		if (year%400==0 || (year%4==0 && year%100!=0)) {
			numDays = 29;
		}
	}
	return numDays;
}

function toggleStyle(srcObj){
	srcObj.className = srcObj.className=="numTextOver" ? (srcObj.innerText == today.getDate() ? "selectedText" : "numText") : "numTextOver" ;
}

function returnValue(srcObj){
	var returnYear = lang == "en" ? frmCalendar.cboYear.value : ((lang == "th") ? ""+(parseInt(frmCalendar.cboYear.value)+543) : frmCalendar.cboYear.value) ;
	txtDateObj.value = "";
	txtDateObj.value += (srcObj.innerText.length == 1 ? "0"+srcObj.innerText : srcObj.innerText)+"/" ;
	txtDateObj.value += (frmCalendar.cboMonth.value.length == 1 ? "0"+frmCalendar.cboMonth.value : frmCalendar.cboMonth.value)+"/" ;
	txtDateObj.value += returnYear ;
	if ("<%=postAction %>" != "")
	    eval("dialogArguments.<%=postAction %>");
	//txtDateObj.value = srcObj.innerText+"/"+frmCalendar.cboMonth.value+"/"+returnYear;
	window.close();
}

function drawCalendar(){
	var firstDateOfMonth = new Date(parseInt(frmCalendar.cboYear.value), parseInt(frmCalendar.cboMonth.value)-1, 1);
	var numDaysInMonthYear = getNumDaysInMonthYear(parseInt(frmCalendar.cboYear.value), parseInt(frmCalendar.cboMonth.value)-1);
	var htmlStr = "";
	var dateStr = 1;
	var numWeeksHaveAllDay = 0;
	var numDaysOfLastWeek = 0;
	var testClass = "numText";	    
	htmlStr += "<TABLE width=\"100%\" cellpadding=1 cellspacing=1 border=1>";
	// Header
	htmlStr += "<TR>";
	for(var j=0; j<7; j++){
			htmlStr += "	<TD width=\"14%\" class=\"header\">"+daytNames[j]+"</TD>";
	}
	htmlStr += "</TR>";

	// First week
	htmlStr += "<TR>";
	minDate = "<%=mindate %>";
	maxdate = "<%=maxdate %>";
	for(var j=0; j<7; j++){	    
		if (firstDateOfMonth.getDay() <= j) {
			testClass = dateStr == today.getDate() ? "selectedText" : "numText";
			var dCalendarDate = parseFloat(((parseInt(frmCalendar.cboYear.value)+543)*10000)+(parseInt(frmCalendar.cboMonth.value)*100)+parseFloat(dateStr));
			if ("<%=mindate %>"!= "") {			    
			    var dMinDate = parseFloat( parseInt(minDate.substring(6,10)*10000) + parseInt(minDate.substring(3,5)*100) + parseInt(minDate.substring(1,2)) );			    
			    if ( dCalendarDate < dMinDate)
			    {
			        testClass = "disabledText";
			        htmlStr += "	<TD class=\""+testClass+"\" >"+(dateStr++)+"</TD>";
			    }
			    else
			    {
			        htmlStr += "	<TD class=\""+testClass+"\" onmouseover=\"toggleStyle(this)\" onmouseout=\"toggleStyle(this)\" onclick=\"returnValue(this)\">"+(dateStr++)+"</TD>";
			    }
			}
			else if ("<%=maxdate %>"!= "") {			    
			    var dMaxDate = parseFloat( parseInt(maxdate.substring(6,10)*10000) + parseInt(maxdate.substring(3,5)*100) + parseInt(maxdate.substring(1,2)) );
			    if ( dMaxDate < dCalendarDate)
			    {
			        testClass = "disabledText";
			        htmlStr += "	<TD class=\""+testClass+"\" >"+(dateStr++)+"</TD>";
			    }
			    else
			    {
			        htmlStr += "	<TD class=\""+testClass+"\" onmouseover=\"toggleStyle(this)\" onmouseout=\"toggleStyle(this)\" onclick=\"returnValue(this)\">"+(dateStr++)+"</TD>";
			    }
			}
			else {
			    htmlStr += "	<TD class=\""+testClass+"\" onmouseover=\"toggleStyle(this)\" onmouseout=\"toggleStyle(this)\" onclick=\"returnValue(this)\">"+(dateStr++)+"</TD>";
			}
		} else {
			htmlStr += "	<TD>&nbsp;</TD>";
		}
	}
	htmlStr += "</TR>";

	numWeeksHaveAllDay = Math.floor((numDaysInMonthYear-dateStr+1)/7);
	numDaysOfLastWeek = (numDaysInMonthYear-dateStr+1)%7;

	for(var i=0; i<numWeeksHaveAllDay; i++){
		htmlStr += "<TR>";
		for(var j=0; j<7; j++){
			testClass = dateStr == today.getDate() ? "selectedText" : "numText";
			if ("<%=mindate %>"!= "") {			    
			    var dMinDate = parseFloat( parseInt(minDate.substring(6,10)*10000) + parseInt(minDate.substring(3,5)*100) + parseInt(minDate.substring(1,2)) );
			    var dCalendarDate = parseFloat(((parseInt(frmCalendar.cboYear.value)+543)*10000)+(parseInt(frmCalendar.cboMonth.value)*100)+parseFloat(dateStr));			    
//			    alert("dMinDate" + dMinDate);
//		        alert("dCalendarDate" + dCalendarDate);
//		        alert ( dCalendarDate < dMinDate);
			    if ( dCalendarDate < dMinDate)
			    {
			        testClass = "disabledText";
			        htmlStr += "	<TD class=\""+testClass+"\" >"+(dateStr++)+"</TD>";
			    }
			    else
			    {
			        htmlStr += "	<TD class=\""+testClass+"\" onmouseover=\"toggleStyle(this)\" onmouseout=\"toggleStyle(this)\" onclick=\"returnValue(this)\">"+(dateStr++)+"</TD>";
			    }
			}
			else if ("<%=maxdate %>"!= "") {			    
			    var dMaxDate = parseFloat( parseInt(maxdate.substring(6,10)*10000) + parseInt(maxdate.substring(3,5)*100) + parseInt(maxdate.substring(1,2)) );
			    if ( dMaxDate < dCalendarDate)
			    {
			        testClass = "disabledText";
			        htmlStr += "	<TD class=\""+testClass+"\" >"+(dateStr++)+"</TD>";
			    }
			    else
			    {
			        htmlStr += "	<TD class=\""+testClass+"\" onmouseover=\"toggleStyle(this)\" onmouseout=\"toggleStyle(this)\" onclick=\"returnValue(this)\">"+(dateStr++)+"</TD>";
			    }
			}
			else {
			    htmlStr += "	<TD class=\""+testClass+"\" onmouseover=\"toggleStyle(this)\" onmouseout=\"toggleStyle(this)\" onclick=\"returnValue(this)\">"+(dateStr++)+"</TD>";
		    }
		}
		htmlStr += "</TR>";
	}

	// Last week
	if (numDaysOfLastWeek > 0) {
		htmlStr += "<TR>";
		for(var j=0; j<7; j++){
			if (numDaysOfLastWeek > j) {
				testClass = dateStr == today.getDate() ? "selectedText" : "numText";
				if ("<%=mindate %>"!= "") {			    
			        var dMinDate = parseFloat( parseInt(minDate.substring(6,10)*10000) + parseInt(minDate.substring(3,5)*100) + parseInt(minDate.substring(1,2)) );
			        var dCalendarDate = parseFloat(((parseInt(frmCalendar.cboYear.value)+543)*10000)+(parseInt(frmCalendar.cboMonth.value)*100)+parseFloat(dateStr));
//			        alert("dMinDate" + dMinDate);
//			        alert("dCalendarDate" + dCalendarDate);
//			        alert ( dCalendarDate < dMinDate);
			        if ( dCalendarDate < dMinDate)
			        {
			            testClass = "disabledText";
			            htmlStr += "	<TD class=\""+testClass+"\" >"+(dateStr++)+"</TD>";
			        }
			        else
			        {
			            htmlStr += "	<TD class=\""+testClass+"\" onmouseover=\"toggleStyle(this)\" onmouseout=\"toggleStyle(this)\" onclick=\"returnValue(this)\">"+(dateStr++)+"</TD>";
			        }
			    }
			    else if ("<%=maxdate %>"!= "") {			    
			        var dMaxDate = parseFloat( parseInt(maxdate.substring(6,10)*10000) + parseInt(maxdate.substring(3,5)*100) + parseInt(maxdate.substring(1,2)) );
			        if ( dMaxDate < dCalendarDate)
			        {
			            testClass = "disabledText";
			            htmlStr += "	<TD class=\""+testClass+"\" >"+(dateStr++)+"</TD>";
			        }
			        else
			        {
			            htmlStr += "	<TD class=\""+testClass+"\" onmouseover=\"toggleStyle(this)\" onmouseout=\"toggleStyle(this)\" onclick=\"returnValue(this)\">"+(dateStr++)+"</TD>";
			        }
			    }
			    else {
				    htmlStr += "	<TD class=\""+testClass+"\" onmouseover=\"toggleStyle(this)\" onmouseout=\"toggleStyle(this)\" onclick=\"returnValue(this)\">"+(dateStr++)+"</TD>";
			    }
			} else {
				htmlStr += "	<TD>&nbsp;</TD>";
			}
		}
		htmlStr += "</TR>";
	}

	htmlStr += "</TABLE>";
	calLayer.innerHTML = htmlStr;
	monthLabel.innerHTML = monthYearLabel[0];
	yearLabel.innerHTML = monthYearLabel[1];
}

//-->
		</SCRIPT>
	</head>
	<BODY onload="initialData();drawCalendar();" onselectstart="return false;" oncontextmenu="return false;">
		<FORM METHOD="POST" NAME="frmCalendar" ACTION="">
			<TABLE width="100%" cellpadding="1" cellspacing="0" border="1">
				<TR style="height:25px;">
					<TD style="text-align: left; border-right-width: 0px;"><SPAN id="monthLabel"></SPAN></TD>
					<TD style="text-align: left; border-left-width: 0px; border-right-width: 0px;"><SELECT NAME="cboMonth" onchange="drawCalendar()"></SELECT></TD>
					<TD style="text-align: right; border-left-width: 0px; border-right-width: 0px;"><SPAN id="yearLabel"></SPAN></TD>
					<TD style="text-align: right; border-left-width: 0px;"><SELECT NAME="cboYear" onchange="drawCalendar()"></SELECT></TD>
				</TR>
				<TR>
					<TD colspan="4"><div id="calLayer"></div>
					</TD>
				</TR>
			</TABLE>
		</FORM>
	</BODY>
</html>
