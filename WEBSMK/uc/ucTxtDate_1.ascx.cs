using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class UserControl_ucTxtDate : System.Web.UI.UserControl
{
    public string strScript;
    protected void Page_Load(object sender, EventArgs e)
    {
        UcCalendar1.textboxName = txtDate.UniqueID;
    }
    public string Text
    {
        set
        {
            txtDate.Text = value;
        }
        get
        {
            return txtDate.Text;
        }
    }
    public string Mode
    {
        set
        {
            if (value == "view")
            {
                UcCalendar1.Visible = false;
                txtDate.ReadOnly = true;
                txtDate.CssClass = "TEXTBOXDIS";
            }
            else if (value == "")
            {
                UcCalendar1.Visible = true;
                txtDate.ReadOnly = false;
                txtDate.CssClass = "TEXTBOX";
            }
        }
    }
    public string txtID
    {
        get
        {
            return txtDate.ClientID;
        }
    }
    public string divCalendarID
    {
        get
        {
            return divCalendar.ClientID;
        }
    }
    public string PostAction
    {
        set
        {
            strScript = value;
            txtDate.Attributes.Add("onblur", strScript);
            UcCalendar1.postAction = strScript;
        }
    }
    public int LastYear
    {
        set
        {
            txtDate.Attributes.Add("lastYear", value.ToString());
            UcCalendar1.lastYear = value;
        }
    }
    public string mindate
    {
        set
        {
            txtDate.Attributes.Add("mindate", value.ToString());
            UcCalendar1.mindate = value;
        }
    }
    public string maxdate
    {
        set
        {
            txtDate.Attributes.Add("maxdate", value.ToString());
            UcCalendar1.maxdate = value;
        }
    }
}
