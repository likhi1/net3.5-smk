﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="catLink_en.ascx.cs" Inherits="uc_catLink_en" %>

<div class="cat-link-warp">

<div class="cat-link cat-link-margin">
<h1 class="icon1" >Knowledge</h1>
<div class="row-list">
<ul> 
<asp:DataList ID="DataList1" runat="server" OnItemDataBound="DataList1_ItemDataBound"> 
<ItemTemplate> 
<li>
<asp:HyperLink ID="hplTopic" runat="server">HyperLink</asp:HyperLink> 
</li> 
</ItemTemplate> 
</asp:DataList>
</ul>

</div>
<div class="btn-more"><a href="<%=Config.SiteUrl%>en/news.aspx?cat=1">&raquo; More </a></div>
</div><!-- End class="cat-link" -->

<div class="cat-link  cat-link-margin">
<h1 class="icon2" >News</h1>
    

 
<div class="row-list">
<ul> 
<asp:DataList ID="DataList2" runat="server" OnItemDataBound="DataList1_ItemDataBound"> 
<ItemTemplate> 
<li>
<asp:HyperLink ID="hplTopic" runat="server">HyperLink</asp:HyperLink> 
</li> 
</ItemTemplate> 
</asp:DataList>
</ul>

</div>
<div class="btn-more"><a href="<%=Config.SiteUrl%>en/news.aspx?cat=2">&raquo;  More </a></div>
</div><!-- End class="cat-link" -->


<div class="cat-link  cat-link-margin"">
<h1 class="icon3" >TVC</h1>
<div class="row-list">
<ul> 
<asp:DataList ID="DataList3" runat="server" OnItemDataBound="DataList1_ItemDataBound"> 
<ItemTemplate> 
<li>
<asp:HyperLink ID="hplTopic" runat="server">HyperLink</asp:HyperLink> 
</li> 
</ItemTemplate> 
</asp:DataList>
</ul>
</div>
<div class="btn-more"><a href="<%=Config.SiteUrl%>en/news.aspx?cat=3">&raquo;  More</a></div>
</div><!-- End class="cat-link" -->


<div class="cat-link  ">
<h1 class="icon4" >Payment </h1>
<div class="row-list">

<ul style="color:#485ED1">  
<li >บัตรเครดิต </li>
<li>โอนผ่านธนาคาร </li>
<li>ชำระที่สำนักงานใหญ่ สินมั่นคง </li>
<li>ชำระที่สาขาสินมั่นคง </li> 
</ul>

</div>
<div class="btn-more"><a href="<%=Config.SiteUrl%>en/payment.aspx">&raquo;  More</a></div>
</div><!-- End class="cat-link" -->


 

<div class="clear"></div>
</div><!-- End class="cat-link-warp" -->
 