﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//--- connect
using System.Configuration;
using System.Data.SqlClient;
using System.Data;


public partial class buyInsureCarCover : System.Web.UI.Page
{
    //hplDetail.NavigateUrl = "buyInsureCarCover.aspx?cp=" + strCamPaign+ "&pl=" + strPolTyp+"&cc="+strCarCode+"&cs="+strCarSize;

    protected string strCampaign = HttpContext.Current.Request.QueryString["cp"];
    protected string strPolType = HttpContext.Current.Request.QueryString["pl"];
    protected string strCarCode = HttpContext.Current.Request.QueryString["cc"];
    protected string strCarSize = HttpContext.Current.Request.QueryString["cs"]; 
    protected string strCarBody = HttpContext.Current.Request.QueryString["cb"]; 
       

    protected void Page_Load(object sender, EventArgs e)
    {
        SelectData();
    }


    protected void SelectData() {
        DataSet ds;
        DataTable dt;
        OnlineVoluntaryManager cmOnline = new OnlineVoluntaryManager();
        ds = cmOnline.getDataCover(strCampaign, strPolType, strCarCode, strCarSize,strCarBody);
        dt = ds.Tables[0];
        if (dt.Rows.Count > 0)
        {
            if (dt.Rows[0]["tpbi1"].ToString() == "" ||
                Convert.ToDouble("0" + dt.Rows[0]["tpbi1"].ToString()) == 0)
                lbltpbi1.Text = " ไม่คุ้มครอง ";
            else
                lbltpbi1.Text = FormatStringApp.FormatInt(dt.Rows[0]["tpbi1"].ToString());
            if (dt.Rows[0]["tpbi2"].ToString() == "" ||
                 Convert.ToDouble("0" + dt.Rows[0]["tpbi2"].ToString()) == 0)
                lbltpbi2.Text = " ไม่คุ้มครอง ";
            else
                lbltpbi2.Text = FormatStringApp.FormatInt(dt.Rows[0]["tpbi2"].ToString());
            if (dt.Rows[0]["tppd"].ToString() == "" ||
                 Convert.ToDouble("0" + dt.Rows[0]["tppd"].ToString()) == 0)
                lbltppd.Text = " ไม่คุ้มครอง ";
            else
                lbltppd.Text = FormatStringApp.FormatInt(dt.Rows[0]["tppd"].ToString());
            if (dt.Rows[0]["od_dd"].ToString() == "" ||
                  Convert.ToDouble("0" + dt.Rows[0]["od_dd"].ToString()) == 0)
                lblDeduct.Text = " - ";
            else
                lblDeduct.Text = FormatStringApp.FormatInt(dt.Rows[0]["od_dd"].ToString());
            if (Request.QueryString["car_od"] == "" ||
                 Convert.ToDouble("0" + Request.QueryString["car_od"]) == 0)
                lblCarOD.Text = " - ";
            else
                lblCarOD.Text = FormatStringApp.FormatInt(Request.QueryString["car_od"]);
            if (dt.Rows[0]["perm_d_01"].ToString() == "" ||
                  Convert.ToDouble("0" + dt.Rows[0]["perm_d_01"].ToString()) == 0)
                lblperm_d_01.Text = " ไม่คุ้มครอง ";
            else
                lblperm_d_01.Text = FormatStringApp.FormatInt(dt.Rows[0]["perm_d_01"].ToString());
            if (dt.Rows[0]["perm_p_num"].ToString() == "" ||
                Convert.ToDouble("0" + dt.Rows[0]["perm_p_num"].ToString()) == 0)
                lblperm_p_num.Text = " - ";
            else
                lblperm_p_num.Text = dt.Rows[0]["perm_p_num"].ToString();
            if (dt.Rows[0]["perm_p_01"].ToString() == "" ||
                  Convert.ToDouble("0" + dt.Rows[0]["perm_p_01"].ToString()) == 0)
                lblperm_p_01.Text = " ไม่คุ้มครอง ";
            else
                lblperm_p_01.Text = FormatStringApp.FormatInt(dt.Rows[0]["perm_p_01"].ToString());
            if (dt.Rows[0]["temp_d_01"].ToString() == "" ||
                  Convert.ToDouble("0" + dt.Rows[0]["temp_d_01"].ToString()) == 0)
                lbltemp_d_01.Text = " ไม่คุ้มครอง ";
            else
                lbltemp_d_01.Text = FormatStringApp.FormatInt(dt.Rows[0]["temp_d_01"].ToString());
            if (dt.Rows[0]["temp_p_num"].ToString() == "" ||
               Convert.ToDouble("0" + dt.Rows[0]["temp_p_num"].ToString()) == 0)
                lbltemp_p_num.Text = " - ";
            else
                lbltemp_p_num.Text = dt.Rows[0]["temp_p_num"].ToString();
            if (dt.Rows[0]["temp_p_01"].ToString() == "" ||
                  Convert.ToDouble("0" + dt.Rows[0]["temp_p_01"].ToString()) == 0)
                lbltemp_p_01.Text = " ไม่คุ้มครอง ";
            else
                lbltemp_p_01.Text = FormatStringApp.FormatInt(dt.Rows[0]["temp_p_01"].ToString());
            if (dt.Rows[0]["cover_02"].ToString() == "" ||
                  Convert.ToDouble("0" + dt.Rows[0]["cover_02"].ToString()) == 0)
                lblcover_02.Text = " ไม่คุ้มครอง ";
            else
                lblcover_02.Text = FormatStringApp.FormatInt(dt.Rows[0]["cover_02"].ToString());
            if (dt.Rows[0]["cover_03"].ToString() == "" ||
                  Convert.ToDouble("0" + dt.Rows[0]["cover_03"].ToString()) == 0)
                lblcover_03.Text = " ไม่คุ้มครอง ";
            else
                lblcover_03.Text = FormatStringApp.FormatInt(dt.Rows[0]["cover_03"].ToString());
        }

    }
  



   
}