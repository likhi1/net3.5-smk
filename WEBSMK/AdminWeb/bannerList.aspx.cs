﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---------- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;
using System.Collections;

public partial class adminWeb_bannerList : System.Web.UI.Page
{
    protected string CatId { get { return Request.QueryString["cat"]; } }
    protected void Page_Load(object sender, EventArgs e) {
         

        if (!Page.IsPostBack) {
            btnAddContent.Attributes.Add("onclick" ,"location='bannerAdd.aspx?cat="+CatId+"';return false;" ) ; 

             BindData(); 
        }  
    }
    //=======================  Set Up  
    protected void BindData() {
        DataSet ds = SelectData();
        DataTable dt1 = ds.Tables[0];
        /// Check num roll befor Command Datasource 
        if (dt1.Rows.Count > 0) {
            //==== Set Pager From code Behind
            GridView1.AllowPaging = true;
            GridView1.PageSize = 20;
            //==== BindData
            GridView1.DataSource = dt1;
            GridView1.DataBind();
        }
    }

    protected DataSet SelectData() {
        string sql;
        sql = "SELECT  * FROM tbBanner WHERE BannerCat ='"+CatId+"'  ORDER BY BannerId   DESC  ";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "myTb");
        return ds;
    }

    //========================= Event   Other 
    /////  This Event Run After BindData 
    protected void GridView1_RowDataBound(object s, GridViewRowEventArgs e) {
        if (e.Row.RowType == DataControlRowType.DataRow) {
           
            //========  (Hidde For  Check   ddlCatName  Select  ) ================================  
            Label lblHdStatus = (Label)(e.Row.FindControl("lblHdStatus"));
            if (lblHdStatus != null) {
                lblHdStatus.Text = DataBinder.Eval(e.Row.DataItem, "BannerStatus").ToString();
            }

            Label lblHdSort = (Label)(e.Row.FindControl("lblHdSort"));
            if (lblHdSort != null) {
                int intHdSort = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "BannerSort"));
                if (intHdSort != 9999 && intHdSort != 0) {
                    lblHdSort.Text = intHdSort.ToString();
                } else {
                    lblHdSort.Text = "";
                }
            }

            //======== EditItemTemplate =================================================  
            DropDownList ddlStatus = (DropDownList)(e.Row.FindControl("ddlStatus"));
            if (ddlStatus != null) {
                ddlStatus.Items.Insert(0, new ListItem("ปิด", "0")); // add Empty Item 
                ddlStatus.Items.Insert(1, new ListItem("เปิด", "1")); // add Empty Item   
                ///// set select  in EditTemplate
                string x = (e.Row.FindControl("lblHdStatus") as Label).Text;
                ddlStatus.Items.FindByValue(x).Selected = true;
            }


            TextBox tbxSort = (TextBox)(e.Row.FindControl("tbxSort"));
            if (tbxSort != null) {
                string strValue = (e.Row.FindControl("lblHdSort") as Label).Text;
                tbxSort.Text = strValue;
                tbxSort.Style.Add("width", "40px");

            }
            //======== ItemTemplate ================================================= 
            Label lblId = (Label)(e.Row.FindControl("lblId"));
            if (lblId != null) {
                string strId = DataBinder.Eval(e.Row.DataItem, "BannerId").ToString();
                int intId = Convert.ToInt32(strId);
                string ConId = String.Format("{0:00000}", intId);
                lblId.Text = ConId;
            }
            HyperLink hplTitle = (HyperLink)(e.Row.FindControl("hplTitle"));
            if (hplTitle != null) { 
                string strId = DataBinder.Eval(e.Row.DataItem, "BannerId").ToString();
                hplTitle.Text = DataBinder.Eval(e.Row.DataItem, "BannerTitle").ToString();
                hplTitle.NavigateUrl = "bannerEdit.aspx?cat="+CatId+"&id=" + strId;
            }

            Image imgStatus = (Image)(e.Row.FindControl("imgStatus"));
            if (imgStatus != null) {
                int strStatus = (int)DataBinder.Eval(e.Row.DataItem, "BannerStatus");
                string picUrl = strStatus == 1 ? "images/check-right.png" : "images/check-wrong.png";
                imgStatus.Attributes.Add("src", picUrl);
            }


            Label lblSort = (Label)(e.Row.FindControl("lblSort"));
            if (lblSort != null) {
                Decimal decSort = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "BannerSort"));
                if (decSort != 9999 && decSort != 0) {
                    lblSort.Text = decSort.ToString();
                } else {
                    lblSort.Text = "";
                }
            }

        }//end if
    }
    protected void GridView1_Deleting(object s, GridViewDeleteEventArgs e) {
        int a = e.RowIndex;
        string sql = "DELETE  FROM  tbBanner WHERE BannerId = '" + GridView1.DataKeys[e.RowIndex].Value + "'";
        int i = new DBClass().SqlExecute(sql);
        GridView1.EditIndex = -1;
        BindData();
    }
    protected void btnSearch_Click(object sender, EventArgs e) {
        // Response.Redirect("newsSearch.aspx?cat=" + ddlSearchCat.Text + "&key=" + tbxKeySearch.Text);
    }
    //======================  Pager
    protected void GridView1_IndexChanging(object sender, GridViewPageEventArgs e) {
        GridView1.PageIndex = e.NewPageIndex;
        BindData();
    }
    //======================  Command Gridview
    protected void modEditCommand(object sender, GridViewEditEventArgs e) {
        GridView1.EditIndex = e.NewEditIndex;
        BindData();
    }
    protected void modCancelCommand(object sender, GridViewCancelEditEventArgs e) {
        GridView1.EditIndex = -1;
        BindData();
    }
    protected void modUpdateCommand(object sender, GridViewUpdateEventArgs e) {
        DropDownList ddlStatus = (DropDownList)GridView1.Rows[e.RowIndex].FindControl("ddlStatus");
        TextBox tbxSort = (TextBox)GridView1.Rows[e.RowIndex].FindControl("tbxSort");
        Decimal decSort;
        string strSQL = "UPDATE tbBanner SET " +
                  "BannerStatus = '" + ddlStatus.Text + "', " +
                  "BannerSort = '" + (decSort = (tbxSort.Text != "") ? Convert.ToDecimal(tbxSort.Text) : 0) + "' " +
                  " WHERE  BannerId = '" + GridView1.DataKeys[e.RowIndex].Value + "'";
        DBClass obj = new DBClass();
        int i = obj.SqlExecute(strSQL);
        GridView1.EditIndex = -1;
        BindData();
    }



}