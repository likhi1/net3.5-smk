﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data;

public partial class Module_Authenticate_UserList : System.Web.UI.Page
{
    public static string strAppName = System.Configuration.ConfigurationManager.AppSettings["AppName"].ToString();

    public string[] ColumnName = { "NO", "USER_LOGIN", "USER_FULLNAME", "ROLE_NAME", "START_DATE", "STOP_DATE", "USER_STATUS" };
    public string[] ColumnType = { "string", "string", "string", "string", "string", "string", "string" };
    public string[] ColumnTitle = { "ลำดับที่", "รหัสผู้ใช้งาน", "ชื่อผู้ใช้", "กลุ่มหน้าที่การใช้งาน", "วันที่เริ่มใช้งาน", "วันที่สิ้นสุด", "สถานะ" };
    string[] ColumnWidth = { "5%", "15%", "26%", "20%", "12%", "12%", "10%" };
    string[] ColumnAlign = { "center", "left", "left", "left", "center", "center", "center" };

    public string tranPicUrl = "";
    DataView dv = new DataView();
    protected void Page_Load(object sender, EventArgs e)
    {
        //UserData condition;
        if (!IsPostBack)
        {
            initialDropdownList();
            initialDataGrid();

            //if (Request.QueryString["back"] != null)
            //{
                //if (Session["SEARCHCONDITION"] != null)
                //{
                    //try
                    //{
                    //    condition = (UserData)Session["SEARCHCONDITION"];
                    //    txtLoginName.Text = condition.LOGINNAME;
                    //    txtEmpCode.Text = condition.EMPCODE;
                    //    txtFName.Text = condition.USERFNAME;
                    //    txtLName.Text = condition.USERLNAME;
                    //    ddlRole.SelectedValue = condition.ROLEID;
                    //    ucTxtDate1.Text = condition.STARTDATE;
                    //    ucTxtDate2.Text = condition.ENDDATE;

                    //    btnSearch_Click(null, null);
                    //}
                    //catch (Exception exp)
                    //{

                    //}
            //    }
            //}
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        AuthenticateManager cm = new AuthenticateManager();
        DataSet ds;
        //UserData condition = new UserData();
        ds = cm.searchDataUser(txtLoginName.Text, "", txtFName.Text,
                                txtLName.Text, ddlRole.SelectedValue, txtFromStartDate.Text,
                                txtToStartDate.Text);
        ViewState.Remove("SortDirection");
        ViewState.Remove("SortString");
        ViewState.Remove("SortIndex");
        ViewState.Add("DSMasterTable", ds);
        ShowData();

        // สำหรับกดปุ่มกลับมาหน้าค้นหา แล้วให้แสดงค่าเดิมไว้
        //condition.LOGINNAME = txtLoginName.Text;
        //condition.EMPCODE = txtEmpCode.Text;
        //condition.USERFNAME = txtFName.Text;
        //condition.USERLNAME = txtLName.Text;
        //condition.ROLEID = ddlRole.SelectedValue;
        //condition.STARTDATE = ucTxtDate1.Text;
        //condition.ENDDATE = ucTxtDate2.Text;
        //Session["SEARCHCONDITION"] = condition;
    }
    protected void PageIndexChanged(Object source, DataGridPageChangedEventArgs e)
    {
        DataView dv = new DataView();
        DataSet ds = new DataSet();
        if (!(ViewState["DSMasterTable"] == null))
        {
            ds = (DataSet)ViewState["DSMasterTable"];
            if (e.NewPageIndex > dtgData.PageCount - 1)
                dtgData.CurrentPageIndex = dtgData.PageCount - 1;
            else
                dtgData.CurrentPageIndex = e.NewPageIndex;

            String sortBy = "";
            if (!(ViewState["DSMasterTable"] == null))
            {
                sortBy = (string)(ViewState["SortString"]);
            }
            dv.Table = ds.Tables[0];
            dv.Sort = sortBy;
            ShowData();

        }
    }           

    public void initialDataGrid()
    {
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        int iPageSize = 0;
        dt = TDS.Utility.MasterUtil.AddColumnToDataTable(dt, ColumnType, ColumnName);
        ds.Tables.Add(dt);
        ViewState.Remove("SortDirection");
        ViewState.Remove("SortString");
        ViewState.Remove("SortIndex");
        ViewState.Add("DSMasterTable", ds);
        ShowData();
        GridViewUtil.setGridStyle(dtgData, ColumnWidth, ColumnAlign, ColumnTitle);
        try
        {
            iPageSize = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["pageSize"].ToString());
        }
        catch (Exception e)
        {
            iPageSize = 10;
        }
        dtgData.PageSize = iPageSize;   
    }
    public void initialDropdownList()
    {
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        AuthenticateManager cm = new AuthenticateManager();
        DataSet ds;
        ds = cm.getAllDataRole();

        ddlRole.DataSource = ds;
        ddlRole.DataTextField = "role_name";
        ddlRole.DataValueField = "role_id";
        ddlRole.DataBind();
        ddlRole.Items.Insert(0, new ListItem(" -- เลือก -- ", ""));
    }
    public void ShowData()
    {
        DataSet ds = new DataSet();
        if (ViewState["DSMasterTable"] != null)
            ds = (DataSet)ViewState["DSMasterTable"];

        string sortBy = "";
        if (ViewState["SortString"] != null)
            sortBy = (String)ViewState["SortString"];

        if (ds.Tables.Count > 0)
        {
            dv.Table = ds.Tables[0];
            dv.Sort = sortBy;

            int newPageCount = (Int32)(Math.Ceiling((double)dv.Table.Rows.Count / (double)dtgData.PageSize));
            if (dtgData.CurrentPageIndex >= newPageCount && newPageCount > 0)
                dtgData.CurrentPageIndex = newPageCount - 1;
        }
        dtgData.DataSource = dv;
        dtgData.DataBind();

        GridViewUtil.ItemDataBound(dtgData.Items);

        ucPageNavigator1.dvData = ds.Tables[0].DefaultView;
        ucPageNavigator1.dtgData = dtgData;
    }
    public void sortColumn(object Sender, EventArgs e)
    {
        string[] SortDirection = new string[ColumnTitle.Length];
        for (int i = 0; i < SortDirection.Length; i++)
        {
            SortDirection[i] = "";
        }
        if (ViewState["SortDirection"] != null)
            SortDirection = (string[])ViewState["SortDirection"];

        LinkButton lb = (LinkButton)Sender;
        string sortBy = "";
        int index = 0;
        while ((index < ColumnName.Length) && !((lb.Text).Equals(ColumnTitle[index])))
        {
            index++;
        }
        sortBy = ("" + ColumnName[index] + " " + SortDirection[index]);
        SortDirection[index] = (SortDirection[index].Equals("") ? "DESC" : "");
        ViewState.Add("SortDirection", SortDirection);
        ViewState.Add("SortString", sortBy);
        ViewState.Add("SortIndex", index);
        ShowData();
    }
    public string getPicUrl(int colNo)
    {
        string returnValue = Request.ApplicationPath + "/Images/tran.gif";
        if (ViewState["SortIndex"] != null)
        {
            DataGridItem dgi = (DataGridItem)(dtgData.Controls[0].Controls[1]);

            string[] SortDirection = new string[ColumnTitle.Length];
            SortDirection = (string[])ViewState["SortDirection"];

            if ((int)(ViewState["SortIndex"]) == colNo)
            {
                returnValue = Request.ApplicationPath + "/Images/" + (SortDirection[colNo].Equals("") ? "sortDown.gif" : "sortUp.gif");
            }
        }
        return returnValue;
    }    
}
