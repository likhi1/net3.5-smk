﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Module_Authenticate_UserForm : System.Web.UI.Page
{
    public static string strAppName = System.Configuration.ConfigurationManager.AppSettings["AppName"].ToString();
    public string strPostBack = "";
    public string[] ColumnPopupName = { "", "EMPCODE", "FNAMEE", "NAME" };
    public string[] ColumnPopupType = { "string", "string", "string", "string"};
    public string[] ColumnPopupTitle = { "No.", "รหัสพนักงาน", "ชื่อLogin", "ชื่อ-นามสกุล" };
    string[] ColumnPopupWidth = { "10%", "20%", "30%", "40%" };
    string[] ColumnPopupAlign = { "center", "center", "left", "left" };
    DataView dv = new DataView();

    protected void Page_Load(object sender, EventArgs e)
    {
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        AuthenticateManager cm = new AuthenticateManager();
        DataSet dsData;
        if (!IsPostBack)
        {
            initialDropdownList();
            ddlStatus.SelectedValue = "A";
            txtMode.Text = "A";
            txtStartDate.Text = TDS.Utility.MasterUtil.getDateString(DateTime.Now, "dd/MM/yyyy", "th-TH");
            if (Request.QueryString["user_login"] != null)
            {
                txtMode.Text = "E";
                txtLogin.Text = Request.QueryString["user_login"];
                dsData = cm.getDataUserByLogin(txtLogin.Text);
                docToUI(dsData);
            }
            setMode();
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string strRet = "";
        DataSet ds;
        AuthenticateManager cm = new AuthenticateManager();

        ds = UIToDoc();
        litJavaScript.Text = "";
       
            litJavaScript.Text = "<script>";
            if (txtMode.Text == "A")
            {
                if (validateForm())
                {
                    strRet = cm.addDataUser(ds);

                    if (strRet.StartsWith("ERROR"))
                    {
                        litJavaScript.Text += "alert('" + strRet.Split(':')[1] + "');";
                    }
                    else
                    {
                        txtMode.Text = "E";
                        litJavaScript.Text += "alert('บันทึกข้อมูลเรียบร้อย');";
                        setMode();

                        txtInsertLogin.Text = ds.Tables[0].Rows[0]["USERLOGIN"].ToString();
                        txtInsertDate.Text = FormatStringApp.FormatDate(DateTime.Now);
                    }
                }
                else
                {
                    litJavaScript.Text += "alert('!รหัสผู้ใช้งาน มีในระบบแล้ว');";
                }

            }

            else if (txtMode.Text == "E")
            {
                strRet = cm.editDataUser(ds);
                if (strRet.StartsWith("ERROR"))
                {
                    litJavaScript.Text += "alert('" + strRet.Split(':')[1] + "');";
                }
                else
                {
                    litJavaScript.Text += "alert('บันทึกข้อมูลเรียบร้อย');";

                    txtUpdateLogin.Text = ds.Tables[0].Rows[0]["USERLOGIN"].ToString();
                    txtUpdateDate.Text = FormatStringApp.FormatDate(DateTime.Now);
                }
            }
            litJavaScript.Text += "</script>";
        
    }
    protected void btnClearDate_Click(object sender, EventArgs e)
    {
        string strRet = "";
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        AuthenticateManager cm = new AuthenticateManager();

        litJavaScript.Text += "<script>";
        strRet = cm.clearDataLastLogin(txtLogin.Text, loginData.loginName);
        if (strRet.StartsWith("ERROR") || strRet.StartsWith("1 : "))
        {
            litJavaScript.Text += "alert('" + strRet.Split(':')[1] + "');";
        }
        else
        {
            litJavaScript.Text += "alert('บันทึกข้อมูลเรียบร้อย');";
            txtLastLogin.Text = "__/__/____";
        }
        litJavaScript.Text += "</script>";
    }
    protected void btnConfirmChangePwd_Click(object sender, EventArgs e)
    {
        string strRet = "";        
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        AuthenticateManager cm = new AuthenticateManager();

        litJavaScript.Text += "<script>";
        strRet = cm.editDataPassword(txtLogin.Text, txtNewPassword.Text, loginData.loginName,"N");
        if (strRet.StartsWith("ERROR"))
        {
            litJavaScript.Text += "alert('" + strRet.Split(':')[1] + "');";
        }
        else
        {
            litJavaScript.Text += "alert('บันทึกข้อมูลเรียบร้อย');";
        }
        litJavaScript.Text += "</script>";
    }     

    public void initialDropdownList()
    {
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        AuthenticateManager cm = new AuthenticateManager();
        DataSet ds;
        ds = cm.getAllDataRole();
        ddlRole.DataSource = ds;
        ddlRole.DataTextField = "role_name";
        ddlRole.DataValueField = "role_id";
        ddlRole.DataBind();
        ddlRole.Items.Insert(0,new ListItem(" -- เลือก -- ",""));
    }
    public void setMode()
    {
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        int iLoginDays = 90;
        if (txtMode.Text == "A")
        {
            txtLogin.CssClass = "TEXTBOX";
            txtLogin.ReadOnly = false;
            btnClearDate.Visible = false;
            btnChangePwd.Visible = false;
            btnAdd.Visible = false;
        }
        else if (txtMode.Text == "E")
        {
            txtLogin.CssClass = "TEXTBOXDIS";
            txtLogin.ReadOnly = true;
            btnClear.Visible = false;
            btnClearDate.Visible = false;
            btnAdd.Visible = true;
            iLoginDays = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["loginDays"]);
            if ((txtLastLogin.Text != "") && (txtLastLogin.Text != "__/__/____") && (Convert.ToDateTime(cmDate.convertDateStringForDBTH(txtLastLogin.Text)).AddDays(iLoginDays).CompareTo(DateTime.Now) < 0))
            {
                btnClearDate.Visible = true;
            }            
            btnChangePwd.Visible = true;
            divLablePassword.Visible = false;
            divLblRePassword.Visible = false;
            divtxtPassword.Visible = false;
            divtxtRePassword.Visible = false;
        }
    }
    public void docToUI(DataSet dsData)
    {
        hddPassword.Value = dsData.Tables[0].Rows[0]["user_pwd"].ToString();
        txtFName.Text = dsData.Tables[0].Rows[0]["user_fname"].ToString();
        txtLName.Text = dsData.Tables[0].Rows[0]["user_lname"].ToString();
        if (!Convert.IsDBNull(dsData.Tables[0].Rows[0]["user_lastlogindate"]))
            txtLastLogin.Text = TDS.Utility.MasterUtil.getDateString((DateTime)dsData.Tables[0].Rows[0]["user_lastlogindate"], "dd/MM/yyyy", "th-TH");
        if (!Convert.IsDBNull(dsData.Tables[0].Rows[0]["start_date"]))
            txtStartDate.Text = TDS.Utility.MasterUtil.getDateString((DateTime)dsData.Tables[0].Rows[0]["start_date"], "dd/MM/yyyy", "th-TH");
        if (!Convert.IsDBNull(dsData.Tables[0].Rows[0]["stop_date"]))
            txtEndDate.Text = TDS.Utility.MasterUtil.getDateString((DateTime)dsData.Tables[0].Rows[0]["stop_date"], "dd/MM/yyyy", "th-TH");
        try
        {
            ddlRole.SelectedValue = dsData.Tables[0].Rows[0]["role_id"].ToString();
        }
        catch (Exception ex)
        {
            ddlRole.SelectedIndex = 0;
        }
        try
        {
            ddlStatus.SelectedValue = dsData.Tables[0].Rows[0]["user_status"].ToString();
        }
        catch (Exception ex)
        {
            ddlStatus.SelectedIndex = 0;
        }
        txtRemark.Text = dsData.Tables[0].Rows[0]["user_remark"].ToString();

        txtInsertLogin.Text = dsData.Tables[0].Rows[0]["insert_name"].ToString();
        txtInsertDate.Text = FormatStringApp.FormatDateTime(dsData.Tables[0].Rows[0]["insert_date"]);
        if (!Convert.IsDBNull(dsData.Tables[0].Rows[0]["update_name"]))
        {
            txtUpdateLogin.Text = dsData.Tables[0].Rows[0]["update_name"].ToString();
            txtUpdateDate.Text = FormatStringApp.FormatDateTime(dsData.Tables[0].Rows[0]["update_date"]);
        }
    }
    public DataSet UIToDoc()
    {
        DataSet ds;
        DataTable dt;
        string[] columnName = {"user_login", "user_pwd", "user_fname", "user_lname", "role_id", 
                                  "user_status", "start_date", "stop_date", "user_email", "user_tel", 
                                  "user_fax", "user_remark", "USERLOGIN","user_flagexpriepwd", "user_expirepwddays",
                                  "broker_code", "user_empcode"};
        string[] columnType = {"string", "string", "string", "string", "string", 
                                  "string", "string", "string", "string", "string", 
                                  "string", "string", "string", "string", "string",
                                  "string", "string"};
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);

        ds = new DataSet();
        dt = new DataTable();
        dt = TDS.Utility.MasterUtil.AddColumnToDataTable(dt, columnType, columnName);
        ds.Tables.Add(dt);
        ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
        ds.Tables[0].Rows[0]["user_login"] = txtLogin.Text;
        if (txtMode.Text == "E")
            ds.Tables[0].Rows[0]["user_pwd"] = hddPassword.Value;
        else
            ds.Tables[0].Rows[0]["user_pwd"] = txtPassword.Text;
        ds.Tables[0].Rows[0]["user_fname"] = txtFName.Text;
        ds.Tables[0].Rows[0]["user_lname"] = txtLName.Text;
        ds.Tables[0].Rows[0]["role_id"] = ddlRole.SelectedValue;
        ds.Tables[0].Rows[0]["start_date"] = txtStartDate.Text;
        ds.Tables[0].Rows[0]["stop_date"] = txtEndDate.Text;
        ds.Tables[0].Rows[0]["user_remark"] = txtRemark.Text;
        ds.Tables[0].Rows[0]["user_status"] = ddlStatus.Text;
        ds.Tables[0].Rows[0]["USERLOGIN"] = loginData.loginName;
        return ds;
    }
    public bool validateForm()
    {
        bool isRet = true;
        DataSet ds;
        AuthenticateManager cm = new AuthenticateManager();
        ds = cm.getDataUserByUserLogin(txtLogin.Text);
        if (ds.Tables[0].Rows.Count > 0)
        {
            isRet = false;
        }
        return isRet;
    }
}
