﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---------- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;
using System.Collections;


public partial class AdminWeb_invSearch : System.Web.UI.Page
{
    //=======================  Global Varible  
    protected string CatId { get { return Request.QueryString["cat"]; } }
    protected string KeyWord { get { return Request.QueryString["key"]; } }
    

    //=======================  Global Varible  
    protected void Page_Load(object sender, EventArgs e) {
        //======== Chk  Query String  
        btnAddContent.Attributes.Add("OnClick", "location='invAdd.aspx';return false; ");

        if (!Page.IsPostBack) {
            //====== Set  Search First Time
            tbxKeySearch.Text = KeyWord;
            ddlSearchCat.SelectedValue = CatId;
            //=====  Set ddlConCat
            ddlSearchCatAddList(); 

            BindData(); 
        }else {
            //====== Set  Search  After Click  Again
            ddlSearchCat.SelectedValue = ddlSearchCat.SelectedValue;
            tbxKeySearch.Text = tbxKeySearch.Text; 
        }
    }
    //=======================  Set Up  
    //////// Set DropDown /////// 
    protected DataSet SelectConCat() {
        string sql = "SELECT * From tbInvCat ";
        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql, "tbName");
        return ds;
    }

    protected void ddlSearchCatAddList() {
        DataSet ds = SelectConCat();
        ddlSearchCat.DataSource = ds;
        ddlSearchCat.AppendDataBoundItems = true;
        ddlSearchCat.DataTextField = "InvCatNameTh";
        ddlSearchCat.DataValueField = "InvCatId";
        ddlSearchCat.DataBind();
    }

    //////// Get Content  /////// 
    protected void BindData() {
        DataSet ds = SelectSearchData();
        DataTable dt1 = ds.Tables[0];
        /// Check num roll befor Command Datasource 
        if (dt1.Rows.Count > 0) {
            //==== Set Pager From code Behind
            GridView1.AllowPaging = true;
            GridView1.PageSize = 20;
            //==== BindData
            GridView1.DataSource = dt1;
            GridView1.DataBind();
        }
    }

    protected DataSet SelectSearchData() {
    

        string sql1;
        //// Condition for user want to search Keyword  From  Global
        if (CatId != "") {
            sql1 = "SELECT  a.* , b.InvCatNameTh  FROM  tbInv  AS a "
            + " Join tbInvCat AS b ON( a.InvCatId = b.InvCatId ) "
            + " AND a.InvCatId = '" + CatId + "' "
            + " AND ( a.InvTopic  LIKE  '%" + KeyWord + "%' OR  a.InvDetailLong LIKE  '%" + KeyWord + "%' )  "
            + " ORDER BY a.InvId  DESC " ; 
        }
        else {
            sql1 = "SELECT  a.* , b.InvCatNameTh  FROM tbInv AS a "
            + " JOIN tbInvCat AS b   ON( a.InvCatId  =  b.InvCatId ) "
            + " AND ( a.InvTopic  LIKE  '%" + KeyWord + "%' OR  a.InvDetailLong LIKE  '%" + KeyWord + "%' )  "
            + " ORDER BY a.InvId  DESC  ";
        }

        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql1, "tableName");
        return ds;  

    }
     

    //========================= Event   Other 
    /////  This Event Run After BindData 
    protected void GridView1_RowDataBound(object s, GridViewRowEventArgs e) {
        if (e.Row.RowType == DataControlRowType.DataRow) {
            //========  (Hidde For  Check   ddlCatName  Select  ) ================================ 
            Label lblHdStatus = (Label)(e.Row.FindControl("lblHdStatus"));
            var InvStatus = DataBinder.Eval(e.Row.DataItem, "InvStatus");
            if (lblHdStatus != null && !Convert.IsDBNull(InvStatus)) {
                lblHdStatus.Text = InvStatus.ToString();
            }
            //======== EditItemTemplate ================================================= 
            DropDownList ddlStatus = (DropDownList)(e.Row.FindControl("ddlStatus"));

            if (ddlStatus != null) {
                ddlStatus.Items.Insert(0, new ListItem("ปิด", "0")); // add Empty Item 
                ddlStatus.Items.Insert(1, new ListItem("เปิด", "1")); // add Empty Item  
                // ddlStatus.Items.FindByValue(x).Selected = true;
                if (!Convert.IsDBNull(InvStatus)) {
                    string lblStatus = lblHdStatus.Text;
                    ddlStatus.Items.FindByValue(lblStatus).Selected = true;
                }
            }

            //===== Update2014 Multi Language
            Label lblLanguage = (Label)(e.Row.FindControl("lblLanguage"));
            if (lblLanguage != null) {
                string strLang = DataBinder.Eval(e.Row.DataItem, "lang").ToString();
                string strLangText = Convert.ToInt32(strLang) == 0 ? "ไทย" : "อังกฤษ";
                lblLanguage.Text = strLangText;
            }


            /// set select  in EditTemplate ////
            //DropDownList ddlStatus = (DropDownList)(e.Row.FindControl("ddlStatus"));
            //if (ddlStatus != null) {
            //    ddlStatus.Items.Insert(0, new ListItem("ปิด", "0")); // add Empty Item 
            //    ddlStatus.Items.Insert(1, new ListItem("เปิด", "1")); // add Empty Item  
            //}
            //if (!Convert.IsDBNull(lblHdStatus)) {
            //    string x = (e.Row.FindControl("lblHdStatus") as Label).Text;
            //    ddlStatus.Items.FindByValue(x).Selected = true;
            //}


            //======== ItemTemplate =================================================      
            Label lblId = (Label)(e.Row.FindControl("lblId"));
            if (lblId != null) {
                string strId = DataBinder.Eval(e.Row.DataItem, "InvId").ToString();
                int intId = Convert.ToInt32(strId);
                string ConId = String.Format("{0:00000}", intId);
                lblId.Text = ConId;
            }

            Label lblCat = (Label)(e.Row.FindControl("lblCat"));
            if (lblCat != null) {
                string strInvIdCat = DataBinder.Eval(e.Row.DataItem, "InvCatNameTh").ToString();
                // int intInvIdCat = Convert.ToInt32(strInvIdCat);
                lblCat.Text = strInvIdCat;
            }

            HyperLink hplTopic = (HyperLink)(e.Row.FindControl("hplTopic"));
            if (hplTopic != null) {
                string strId = DataBinder.Eval(e.Row.DataItem, "InvId").ToString();
                hplTopic.Text = DataBinder.Eval(e.Row.DataItem, "InvTopic").ToString();
                hplTopic.NavigateUrl = "invView.aspx?id=" + strId;
            }

            //Image imgStatus = (Image)(e.Row.FindControl("imgStatus")); 
            //if (imgStatus != null) {
            //    int strStatus = (int)DataBinder.Eval(e.Row.DataItem, "InvStatus");
            //    string picUrl = strStatus == 1 ? "images/check-right.png" : "images/check-wrong.png";
            //    imgStatus.Attributes.Add("src", picUrl);
            //}

            Label lblSort = (Label)(e.Row.FindControl("lblSort"));
            if (lblSort != null) {
                Decimal decSort = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "InvSort"));

                // Set value Show empty If value =  9999  
                if (decSort != 9999 && decSort != 0) {
                    lblSort.Text = decSort.ToString();
                }
                else {
                    lblSort.Text = "";
                }
            }


            Label lblDateInput = (Label)(e.Row.FindControl("lblDateInput"));
            if (lblDateInput != null) {

                DateTime input_date = (DateTime)DataBinder.Eval(e.Row.DataItem, "input_date");
                string strDate = input_date.ToString("dMMMyy HH:mm น.", new CultureInfo("th-TH"));
                lblDateInput.Text = strDate;

                ///// Add Style  
                //lblDtCreate.Style.Add("float", "right");
            }

            Image imgStatus = (Image)(e.Row.FindControl("imgStatus"));
            var strStatus = DataBinder.Eval(e.Row.DataItem, "InvStatus");
            string picUrl;

            if (imgStatus != null && !Convert.IsDBNull(strStatus)) {
                if (Convert.ToInt32(strStatus) == 1) {
                    picUrl = "images/check-right.png";
                }
                else {
                    picUrl = "images/check-wrong.png";
                }
                imgStatus.Attributes.Add("src", picUrl);
            }


        }//end if
    }
    protected void GridView1_Deleting(object s, GridViewDeleteEventArgs e) {
        int a = e.RowIndex;
        string sql = "DELETE  FROM  tbInv WHERE InvId = '" + GridView1.DataKeys[e.RowIndex].Value + "'";
        int i = new DBClass().SqlExecute(sql);
        GridView1.EditIndex = -1;
        BindData();
    }
    protected void btnSearch_Click(object sender, EventArgs e) {
        Response.Redirect("invSearch.aspx?cat=" + ddlSearchCat.Text + "&key=" + tbxKeySearch.Text);
    }
   
    //======================  Pager
    protected void GridView1_IndexChanging(object sender, GridViewPageEventArgs e) {
        GridView1.PageIndex = e.NewPageIndex;
        BindData();
    }
    //======================  Command Gridview
    protected void modEditCommand(object sender, GridViewEditEventArgs e) {
        GridView1.EditIndex = e.NewEditIndex;
        BindData();
    }
    protected void modCancelCommand(object sender, GridViewCancelEditEventArgs e) {
        GridView1.EditIndex = -1;
        BindData();
    }
    protected void modUpdateCommand(object sender, GridViewUpdateEventArgs e) {
        DropDownList ddlStatus = (DropDownList)GridView1.Rows[e.RowIndex].FindControl("ddlStatus");

        string strSQL = "UPDATE tbInv SET " +
                  "InvStatus = '" + ddlStatus.Text + "' " +
                  " WHERE  InvId = '" + GridView1.DataKeys[e.RowIndex].Value + "'";
        DBClass obj = new DBClass();
        int i = obj.SqlExecute(strSQL);
        GridView1.EditIndex = -1;
        BindData();
    }


}