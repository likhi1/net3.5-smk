﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;
using System.Collections;


public partial class adminWeb_prdSearch : System.Web.UI.Page {

    //=======================  Global Varible  
    protected string CatId { get { return Request.QueryString["cat"]; } }
    protected string KeyWord { get { return Request.QueryString["key"]; }  }
     

   protected void Page_Load(object sender, EventArgs e) {
       

       if (!Page.IsPostBack) {
           //====== Set  Search First Time
           tbxKeySearch.Text = KeyWord;
           ddlSearchCat.SelectedValue = CatId;

           //=====  Set ddlConCat
           ddlSearchCatAddList();

           BindData(); 

           
       } else {
           //====== Set  Search  After Click  Again
           ddlSearchCat.SelectedValue = ddlSearchCat.SelectedValue;
           tbxKeySearch.Text = tbxKeySearch.Text;

       }
    }

   
   //=======================  Set Up  
   protected void BindData() { 

        DataSet ds = SelectConData();
        DataTable dt1 = ds.Tables[0];

        /// Check num roll befor Command Datasource 
        if (dt1.Rows.Count > 0) {
            //==== Set Pager From code Behind
            GridView1.AllowPaging = true;
            GridView1.PageSize = 20;

            //==== BindData
            GridView1.DataSource = dt1;
            GridView1.DataBind();

            //GridView1.DataSource = dt1;
            //GridView1.DataBind();


        }

    }

   protected DataSet SelectConCat() {
        string sql = "SELECT * From tbPrdCat";
        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql, "_tbPrdCat"); 
        return ds;
    }
     
   protected DataSet  SelectConData() {

       string sql1;
       //// Condition for user want to search Keyword  From  Global
       if (CatId != "") {
           sql1 = "SELECT  a.* , b.PrdCatName   "
           + "FROM tbPrdCon a ,tbPrdCat b "
           + "WHERE "
           + "a.PrdCatId = b.PrdCatId "
           + "AND a.PrdCatId = '" + CatId + "' "
           + " AND ( PrdConTopic  LIKE  '%" + KeyWord + "%' OR  PrdConDetailShort  LIKE  '%" + KeyWord + "%' OR  PrdConDetailLong   LIKE  '%" + KeyWord + "%' )  "
           + "ORDER BY PrdConId   DESC   , PrdConSort ASC";
       } else {
           sql1 = "SELECT  a.* , b.PrdCatName   "
           + "FROM tbPrdCon a ,tbPrdCat b "
           + "WHERE "
           + "a.PrdCatId = b.PrdCatId "
           + " AND ( PrdConTopic  LIKE  '%" + KeyWord + "%' OR  PrdConDetailShort  LIKE  '%" + KeyWord + "%' OR  PrdConDetailLong   LIKE  '%" + KeyWord + "%' )  "
           + "ORDER BY PrdConId   DESC   , PrdConSort ASC ";
       } 
 
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql1, "tb_prdCon");  
        return ds;  
    }

   protected void ddlSearchCatAddList() {
       DataSet ds = SelectConCat();
       ddlSearchCat.DataSource = ds;
       ddlSearchCat.AppendDataBoundItems = true;
       ddlSearchCat.DataTextField = "PrdCatName";
       ddlSearchCat.DataValueField = "PrdCatId";
       ddlSearchCat.DataBind();
   }

   protected ArrayList ddlConSortAddList() {
       ArrayList arl = new ArrayList();
       for (int i = 1; i <= 10; i++) {
           arl.Add(i);
       }

       return arl;
   } 


   //========================= Event   Other 
    /////  This Event Run After BindData 


   //======================  Pager
    protected void GridView1_IndexChanging(object sender, GridViewPageEventArgs e) {
           GridView1.PageIndex = e.NewPageIndex;
           BindData();
       }

  
   //======================  Command Gridview

   protected void GridView1_DataBound(object s, GridViewRowEventArgs e) { 

       if (e.Row.RowType == DataControlRowType.DataRow) {
           //========  (Hidde For  Check   ddlCatName  Select  ) ===================================     
           Label lblHdCatId = (Label)(e.Row.FindControl("lblHdCatId"));
           if (lblHdCatId != null) {
               lblHdCatId.Text = DataBinder.Eval(e.Row.DataItem, "PrdCatId").ToString();
           }

           Label lblHdStatus = (Label)(e.Row.FindControl("lblHdStatus"));
           if (lblHdStatus != null) {
               lblHdStatus.Text = DataBinder.Eval(e.Row.DataItem, "PrdConStatus").ToString();
           }

           Label lblHdSort = (Label)(e.Row.FindControl("lblHdSort"));
           if (lblHdSort != null) { 
               int intHdSort = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "PrdConSort"));
               if (intHdSort != 9999 && intHdSort != 0) {
                   lblHdSort.Text = intHdSort.ToString();
               } else {
                   lblHdSort.Text = "";
               } 
           }

           //======== EditItemTemplate =================================================     
           DropDownList ddlCatName = (DropDownList)(e.Row.FindControl("ddlCatName"));
           if (ddlCatName != null) {
               DataSet ds = SelectConCat();
               ddlCatName.DataSource = ds;
               ddlCatName.AppendDataBoundItems = true;
               ddlCatName.DataTextField = "PrdCatName";
               ddlCatName.DataValueField = "PrdCatId";
               ddlCatName.DataBind();
               ///// set select  in EditTemplate
               string x = (e.Row.FindControl("lblHdCatId") as Label).Text;
               ddlCatName.Items.FindByValue(x).Selected = true;
           }

            

           TextBox tbxSort = (TextBox)(e.Row.FindControl("tbxSort"));
           if (tbxSort != null) {
               string strValue = (e.Row.FindControl("lblHdSort") as Label).Text;
               tbxSort.Text = strValue;
               tbxSort.Style.Add("width", "40px");
           }

           DropDownList ddlStatus = (DropDownList)(e.Row.FindControl("ddlStatus"));
           if (ddlStatus != null) {
               ddlStatus.Items.Insert(0, new ListItem("ปิด", "0")); // add Empty Item 
               ddlStatus.Items.Insert(1, new ListItem("เปิด", "1")); // add Empty Item   
               ///// set select  in EditTemplate
               string x = (e.Row.FindControl("lblHdStatus") as Label).Text;
               ddlStatus.Items.FindByValue(x).Selected = true;
           }



           //========  ItemTemplate =================================================         
           Label lblId = (Label)(e.Row.FindControl("lblId"));
           if (lblId != null) {
               string strId = DataBinder.Eval(e.Row.DataItem, "PrdConId").ToString();
               int intId = Convert.ToInt32(strId);
               string ConId = String.Format("{0:00000}", intId); 
               lblId.Text = ConId;

               lblId.Style.Add("text-align", "right");
               lblId.Style.Add("padding-right", "10px");
               ///// Add Style 
               lblId.Style.Add("width", "2%");
           }

           HyperLink hplTopic = (HyperLink)(e.Row.FindControl("hplTopic"));
           if (hplTopic != null) {
               string _id = DataBinder.Eval(e.Row.DataItem, "PrdConId").ToString();
               string _cat = DataBinder.Eval(e.Row.DataItem, "PrdCatId").ToString();

               hplTopic.Text = DataBinder.Eval(e.Row.DataItem, "PrdConTopic").ToString();

               hplTopic.NavigateUrl = "prdView.aspx?id=" + _id + "&cat=" + _cat;
               hplTopic.Style.Add("float", "left"); 
           }

           Label lblCatName = (Label)(e.Row.FindControl("lblCatName"));
           if (lblCatName != null) {
               lblCatName.Text = DataBinder.Eval(e.Row.DataItem, "PrdCatName").ToString();
           }


           Image imgStatus = (Image)(e.Row.FindControl("imgStatus"));
           if (imgStatus != null) {
               int strStatus = (int)DataBinder.Eval(e.Row.DataItem, "PrdConStatus");
               //lblConStatus.Text =  ( (strStatus == 1) ? '' : "ไม่แสดง"  ) ;
               string picUrl = strStatus == 1 ? "images/check-right.png" : "images/check-wrong.png";
               imgStatus.Attributes.Add("src", picUrl);  
           }

           Label lblSort = (Label)(e.Row.FindControl("lblSort"));
           if (lblSort != null) {
               Decimal decSort = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "PrdConSort"));

               if (decSort > 0) {
                   lblSort.Text = decSort.ToString();
               } else {
                   lblSort.Text = "";
               }

               
           }


           Label lblDtCreate = (Label)(e.Row.FindControl("lblDtCreate"));
           if (lblDtCreate != null) {

               DateTime strDtCreate = (DateTime)DataBinder.Eval(e.Row.DataItem, "PrdConDtShow");
               string strDtCreate2 = strDtCreate.ToString("dMMMyy", new CultureInfo("th-TH"));
               lblDtCreate.Text = strDtCreate2;

               ///// Add Style  
               //lblDtCreate.Style.Add("float", "right");
           }




           Label lblDtStop = (Label)(e.Row.FindControl("lblDtStop"));
           if (lblDtStop != null) {

               DateTime strDtStop = (DateTime)DataBinder.Eval(e.Row.DataItem, "PrdConDtHide");
               string strDtStop2 = strDtStop.ToString("dMMMyy", new CultureInfo("th-TH"));
               lblDtStop.Text = strDtStop2;

               ///// Add Style  
               lblDtCreate.Style.Add("float", "right");
           }


       }//end if

   }  

   protected void GridView1_Deleting(object s, GridViewDeleteEventArgs e) {
       int a = e.RowIndex;
       string sql2 = "DELETE  FROM  tbPrdCon WHERE PrdConId = '" + GridView1.DataKeys[e.RowIndex].Value + "'";
       int i = new DBClass().SqlExecute(sql2);
       GridView1.EditIndex = -1;
       BindData();
   }
     
   protected void btnSearch_Click(object sender, EventArgs e) {
       Response.Redirect("prdSearch.aspx?cat=" + ddlSearchCat.Text + "&key=" + tbxKeySearch.Text);
   }



   protected void modEditCommand(object sender, GridViewEditEventArgs e) {
       GridView1.EditIndex = e.NewEditIndex;
       BindData();
   }

   protected void modCancelCommand(object sender, GridViewCancelEditEventArgs e) {
       GridView1.EditIndex = -1;
       BindData();
   }

   protected void modUpdateCommand(object sender, GridViewUpdateEventArgs e) {
       DropDownList ddlCatName = (DropDownList)GridView1.Rows[e.RowIndex].FindControl("ddlCatName");
       DropDownList ddlStatus = (DropDownList)GridView1.Rows[e.RowIndex].FindControl("ddlStatus");
       TextBox tbxSort = (TextBox)GridView1.Rows[e.RowIndex].FindControl("tbxSort");
       Decimal decSort;

       string strSQL = "UPDATE tbPrdCon SET " +
                "PrdCatId= '" + ddlCatName.Text + "', " +
                 "PrdConStatus = '" + ddlStatus.Text + "', " +
                 "PrdConSort = '" + (decSort = (tbxSort.Text != "") ? Convert.ToDecimal(tbxSort.Text) : 0) + "' " +
                 " WHERE  PrdConId = '" + GridView1.DataKeys[e.RowIndex].Value + "'";

       DBClass obj = new DBClass();
       int i = obj.SqlExecute(strSQL);

       GridView1.EditIndex = -1;
       BindData();
   }



}