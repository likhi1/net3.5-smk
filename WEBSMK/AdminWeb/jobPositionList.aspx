﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminWeb/MasterPageAdmin1.master" AutoEventWireup="true" CodeFile="jobPositionList.aspx.cs" Inherits="adminWeb_jobPositionList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
           <div id="manageArea">
            <div  class="colLeft">
       <h1 class="subject1">
           ตำแหน่ง <asp:Label ID="lblPageName" runat="server" Text="Label"></asp:Label></h1>
   
                <div class="navManage">
            <asp:Button ID="btnAddContent" runat="server" Text="เพิ่มรายการ" class="btn-default"  Style="margin-right: 5px;" UseSubmitBehavior="false"   />
 
        </div>

                </div><!-- end  class="colLeft" --> 
<div class="colRight">
<div class="areaSearch">
<table width="100%" >
    <tr>
   
  
<td  align="right"    >
    
    
    <asp:TextBox ID="tbxSearch" runat="server"  Width="250px"></asp:TextBox>
</td>
                              
<td    width="5%"  > <asp:Button ID="btnSearch" runat="server" Text="ค้นหา" class="btn-default  btn-small"   OnClick="btnSearch_Click"  UseSubmitBehavior="False"  />
</td>              
</tr>
              
          
</table>
</div>
<!-- End  class="areaSearch" --> 
       <div class="notaion"> *** การแสดงหน้าเวบ 1.ต้องมีการเปิดการใช้งาน 2.วันที่ปัจจุบันไม่เกินวันที่หยุดแสดง</div>
</div><!-- end  class="colRight" -->
</div><!-- End  id="manageArea" -->

<style> 
/*===== Control  GridView1  Width (support IE9 & Over) =====*/ 
[id*=GridView] th:nth-child(1) {width: 5% ; }
[id*=GridView] th:nth-child(2) { }
[id*=GridView] th:nth-child(3) {width:8%; text-align:center; }
[id*=GridView] th:nth-child(4) {width: 13% } 
[id*=GridView] th:nth-child(5) {width:5% } 
[id*=GridView] th:nth-child(6) {width: 8% }
[id*=GridView] th:nth-child(7) {width: 8% } 
[id*=GridView] th:nth-child(8) {width: 8% } 
[id*=GridView] th:nth-child(9) {width:  13% } 
[id*=GridView] td { word-wrap: break-word; word-break:break-all;  text-align:center; }
[id*=GridView] td:nth-child(2) {  text-align:left ; }
[id*=GridView] td:nth-child(4) {  text-align:left ; }
</style>
 
       
    <asp:GridView ID="GridView1" runat="server" EnableModelValidation="True" AutoGenerateColumns="False" Width="850px"
        
       DataKeyNames="JobPosiId"
        OnRowDataBound="GridView1_RowDataBound" 
        OnRowDeleting="GridView1_Deleting"
        OnPageIndexChanging="GridView1_IndexChanging"
        OnRowEditing="modEditCommand"
        OnRowUpdating="modUpdateCommand"
        OnRowCancelingEdit="modCancelCommand">


        <Columns>
            <asp:TemplateField HeaderText="ID"> 
                <ItemTemplate>
                    <asp:Label ID="lblId" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ชื่อตำแหน่ง">
                <ItemTemplate>
                    <asp:HyperLink ID="hplName" runat="server"  ></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>

              <asp:TemplateField HeaderText="จำนวน"> 
                <ItemTemplate>
                    <asp:Label ID="lblQnt" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="ชื่อสาขา"> 
                <ItemTemplate>
                    <asp:Label ID="lblBranch" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="สถานะ">
                <ItemTemplate>
                        <asp:Literal ID="ltlStatus" runat="server"></asp:Literal >
                </ItemTemplate>

                   <EditItemTemplate>
                         <asp:Label ID="lblHdStatus" runat="server"   Visible="false"></asp:Label>
                          <asp:DropDownList ID="ddlStatus" runat="server"   > </asp:DropDownList>
                    </EditItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="แสดง<br>ลำดับ">
              
                <ItemTemplate>
                      <asp:Label ID="lblSort" runat="server"></asp:Label>
            
                </ItemTemplate>

                   <EditItemTemplate>
                         <asp:Label ID="lblHdSort" runat="server"   Visible="false"></asp:Label>
                        <asp:TextBox ID="tbxSort" runat="server" width="45px" ></asp:TextBox> 
                    </EditItemTemplate> 
            </asp:TemplateField>
            <asp:TemplateField HeaderText="วันที่เริ่ม<br>แสดง"> 
                <ItemTemplate>
                    <asp:Label ID="lblDtStart" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="วันที่หยุด<br>แสดง">
             
                <ItemTemplate>
                    <asp:Label ID="lblDtStop" runat="server"></asp:Label>
                </ItemTemplate>

                

            </asp:TemplateField>
         
            <asp:TemplateField  HeaderText="จัดการ">
                 <ItemTemplate>
                        <div style="text-align: center">
                            <asp:Button ID="btnRowEdit" runat="server" Text="ตั้งค่า" class="btn-default btn-small " CommandName="Edit" />
                            <asp:Button ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Delete" Text="ลบ"
                                class="btn-default btn-small " OnClientClick="return confirm('ยืนยันทำการ ลบข้อมูล')" />
                        </div>
                    </ItemTemplate>


                    <EditItemTemplate>
                        <div style="text-align: center">
                     
                        <asp:Button ID="btnRowUpdate" runat="server" Text="บันทึก" class="btn-default btn-small " CommandName="Update" />
                               <asp:Button ID="btnRowCancel" runat="server" Text="ยกเลิก" class="btn-default btn-small " CommandName="Cancel" />
                     
                        </div>
                    </EditItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    
</asp:Content>

