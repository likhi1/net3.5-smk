﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;


public partial class adminWeb_invView  : System.Web.UI.Page
{
    //=== global Var
    protected string ConId { get { return Request.QueryString["id"];  } }
    protected string CatId { get { return Request.QueryString["cat"]; }   }
 

    
    protected void Page_Load(object sender, EventArgs e) {
        ////////// Check  Session 
        // new CheckSess().CurrentSession(); 
         
        //////// Run Bind Data  
        if (!Page.IsPostBack) { 
            ddlCatBindData();
            BindData();
        }
    }


    protected void ddlCatBindData() {
        string sql = "SELECT * FROM tbInvCat ORDER BY InvCatId ";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "tbName");

        ddlCat.DataSource = ds;
        //**** Add frist Item of dropdownlist
        ddlCat.AppendDataBoundItems = true;
        ddlCat.DataTextField = "InvCatNameTh";
        ddlCat.DataValueField = "InvCatId";
        ddlCat.DataBind();
    }


    protected void BindData() {
        string sql  = "SELECT * FROM tbInv WHERE  InvId= '" + ConId + "'  ";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql , "myTb");
        DataTable dt = ds.Tables[0];

        if (dt.Rows.Count > 0) {    
            int  intId =  Convert.ToInt32( dt.Rows[0][0].ToString() ) ;
            lblId.Text = String.Format("{0:00000}", intId); 

            rblStatus.SelectedValue = dt.Rows[0]["InvStatus"].ToString(); 

            tbxTopic.Text = dt.Rows[0]["InvTopic"].ToString();

            tarDetailLong.Text = dt.Rows[0]["InvDetailLong"].ToString();

            ddlCat.SelectedValue = dt.Rows[0]["InvCatId"].ToString();

            string strSort = dt.Rows[0]["InvSort"].ToString(); 
            tbxSort.Text = Convert.ToDecimal(strSort) != 9999 ? strSort : ""; // Set value Show If value =  9999   


            //===== Update2014 Multi Language
            rblLanguage.SelectedValue = dt.Rows[0]["lang"].ToString();





        }
    }

    protected void btnSave_Click(object sender, EventArgs e) {
         
        /////// Update content
        string sql = "UPDATE  tbInv SET "
        + "InvTopic = @InvTopic, "
        + "InvStatus = @InvStatus,  "
        + "InvDetailLong = @InvDetailLong,  " 
         //===== AdminInfo Update   
		+ "input_date = @input_date,  "
		+ "input_user = @input_user ,  "
        + "InvCatId = @InvCatId , "
        + "InvSort = @InvSort ,  "  

        //=====  Update2014 Multi Language
         + "lang = @lang  "
        //=====  Increase Field For Category
        + "WHERE InvId = '" + ConId + "'   ";
 
        SqlParameterCollection objParam1 = new SqlCommand().Parameters;
        objParam1.AddWithValue("@InvTopic", tbxTopic.Text);
        objParam1.AddWithValue("@InvStatus", rblStatus.SelectedValue);
        objParam1.AddWithValue("@InvDetailLong", tarDetailLong.Text);
        objParam1.AddWithValue("@input_date",   DateTime.Now);
        objParam1.AddWithValue("@InvSort", tbxSort.Text);

        //===== Update2014  Multi Language
        objParam1.AddWithValue("@lang", rblLanguage.SelectedValue);

        //===========  Get UserName From Session Object 
        ProfileData loginData = (ProfileData)Session["SessAdmin"];
        objParam1.AddWithValue("@input_user", loginData.loginName); 

        //=====  Increase Field For Category
        objParam1.AddWithValue("@InvCatId ", ddlCat.SelectedValue);
        
 
         ///////  Get Value From Html Control 
        //objParam1.AddWithValue("@NewsPicSmall", SqlDbType.NVarChar).Value = Request.Form["PicSmall"].ToString();
        //objParam1.AddWithValue("@NewsPicBig", SqlDbType.NVarChar).Value = Request.Form["PicPic"].ToString();

        int i2 = new DBClass().SqlExecute(sql, objParam1); 
        //////// check Succes  
        if (i2 == 1) {
            Response.Write("<script>alert('แก้ไขข้อมูลเรียบร้อยแล้ว')</script>");
            Response.Redirect("invView.aspx?id=" + ConId );  
        } else {
            Response.Write("<script>alert('ไม่สามารถแก้ไขข้อมูลได้')</script>");
            Response.Write("<script>history.go(-1)</script>");
            //Response.Redirect("");
        }

    }

}