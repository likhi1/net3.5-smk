﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminWeb/MasterPageAdmin1.master" AutoEventWireup="true" CodeFile="WebPremiumForm.aspx.cs" Inherits="AdminWeb_WebPremiumForm" %>
<%@ Register src="uc/ucTxtDate.ascx" tagname="ucTxtDate" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div>
        <asp:Label ID="lblSessShow" runat="server"> </asp:Label> 
        <h1 class="subject1">Import ข้อมูลเข้า Web</h1>       
        <table style="width:100%;">
            <tr>
                <td class="label">                   
                    ผลิตภัณฑ์ :</td>
                <td>
                    <asp:DropDownList ID="ddlProduct" runat="server" style="margin-bottom: 0px">                        
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="label">
                    รหัสรถ :</td>
                <td>
                    <asp:DropDownList ID="ddlCarCode" runat="server" style="margin-bottom: 0px">                        
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="label">
                    ทุนประกันภัยตั้งแต่ :</td>
                <td>
                    <asp:TextBox ID="txtSumInsuredFrom" runat="server" CssClass="TEXTBOXMONEY" Width="100" Text="300,000"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label">
                    ทุนประกันภัยถึง :</td>
                <td>
                    <asp:TextBox ID="txtSumInsuredTo" runat="server" CssClass="TEXTBOXMONEY" Width="100" Text="400,000"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label">
                    ช่วงห่าง :</td>
                <td>
                    <asp:DropDownList ID="ddlPeriod" runat="server" style="margin-bottom: 0px">                        
                    </asp:DropDownList>
                </td>
            </tr>            
            <tr>
                <td class="label">
                    วันที่เริ่มใช้งาน :</td>
                <td>                    
                    <uc1:ucTxtDate ID="txtStartDate" runat="server" />                    
                </td>
            </tr>
            <tr>
                <td class="label">
                    วันที่สิ้นสุด :</td>
                <td>
                    <uc1:ucTxtDate ID="txtStopDate" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="label">
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btnSave" runat="server" Text="Generate" onclick="btnSave_Click" CssClass="btn-default" />
                    <asp:Button ID="Button2" runat="server" Text="Cancle"  CssClass="btn-default"/>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    * รถตู้ส่วนบุคคล(รหัส 210 body 22) ไม่ขาย 3 คุ้ม
                    <br />
                    ** รถอายุ 10 ปีขึ้นไป ขายประเภท 3 เท่านั้น
                </td>
            </tr>
        </table>      
        <br />
        <br />   
       <asp:DataGrid ID="dtgData" runat="server" AutoGenerateColumns="true" Width="100%"
            HeaderStyle-CssClass="gridhead" ItemStyle-CssClass="gridrow0" AlternatingItemStyle-CssClass="gridrow1"
            AllowPaging="false" PagerStyle-Visible="false">
            <Columns>
                <asp:TemplateColumn>                    
                    <ItemTemplate>
                        <%# Container.DataSetIndex +1%>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>       
        <br />
        <br />
        
        </div> <!-- end  id="page-prdList" -->
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

