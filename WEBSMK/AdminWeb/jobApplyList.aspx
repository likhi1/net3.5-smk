﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminWeb/MasterPageAdmin1.master" AutoEventWireup="true" CodeFile="jobApplyList.aspx.cs" Inherits="adminWeb_jobApplyList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div id="manageArea">
        <div class="colLeft">
            <h1 class="subject1">ผู้สมัครงาน <asp:Label ID="lblPageName" runat="server" Text="Label"></asp:Label> </h1>

            <div class="navManage">
            </div>

        </div>
        <!-- end  class="colLeft" -->
        <div class="colRight">
            <div class="areaSearch">
                <table width="100%">
                    <tr>

<td   align="right">
    <asp:DropDownList ID="ddlPosition" runat="server"></asp:DropDownList>
    </td>
</td>
                        <td align="right"  width="5%" >
                            <asp:TextBox ID="tbxSearch" runat="server"  ></asp:TextBox>


                        </td>

                        <td width="5%">
                            <asp:Button ID="btnSearch" runat="server" Text="ค้นหา" class="btn-default  btn-small" OnClick="btnSearch_Click" UseSubmitBehavior="False" />
                        </td>
                    </tr>


                </table>
            </div>
            <!-- End  class="areaSearch" -->
            
        </div>
        <!-- end  class="colRight" -->
    </div>
    <!-- End  id="manageArea" -->


    <style> 
/*===== Control  GridView1  Width (support IE9 & Over) =====*/ 
[id*=GridView] th:nth-child(1) {width: 5% ; }
[id*=GridView] th:nth-child(2) { }
[id*=GridView] th:nth-child(3) {width:5%; text-align:center; }
[id*=GridView] th:nth-child(4) {width: 20% } 
[id*=GridView] th:nth-child(5) {width:8% ;   } 
[id*=GridView] th:nth-child(6) {width: 13% }
[id*=GridView] th:nth-child(7) {width: 8% } 
[id*=GridView] th:nth-child(8) {width: 12% }  
[id*=GridView] td { word-wrap: break-word; word-break:break-all;  text-align:center; }
[id*=GridView] td:nth-child(2) {  text-align:left ; }
[id*=GridView] td:nth-child(4) { text-align: left; }
[id*=GridView] td:nth-child(5) {   }
</style>
 


    <asp:GridView ID="GridView1" runat="server" EnableModelValidation="True"  Width="100%" 
        AutoGenerateColumns="False" 
         DataKeyNames="JobPrfId"
        OnRowDataBound="GridView1_RowDataBound" 
        OnRowDeleting="GridView1_Deleting"
        OnPageIndexChanging="GridView1_IndexChanging"
        OnRowEditing="modEditCommand"
        OnRowUpdating="modUpdateCommand"
        OnRowCancelingEdit="modCancelCommand" 
        
        
        
        >
        <Columns>
            <asp:TemplateField HeaderText="ID">
                <ItemTemplate>
                    <asp:Label ID="lblId" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
           
            <asp:TemplateField HeaderText="ชื่อผู้สมัคร">
                <ItemTemplate>
                    <asp:HyperLink ID="hplName" runat="server" NavigateUrl="#" Text="ผู้สมัคร"></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="อายุ">

                <ItemTemplate>
                    <asp:Label ID="lblAge" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="สมัครตำแหน่ง">

                <ItemTemplate>
                    <asp:Label ID="lblPosition" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="วันเวลาสมัคร">
                <ItemTemplate>
                    <asp:Label ID="lblDatReg" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="เบอร์มือถือ"> 
                <ItemTemplate>
                    <asp:Label ID="lblTel" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
          
    <asp:TemplateField HeaderText="สถานะ<br>ติดต่อกลับ">
    <ItemTemplate> 
    <asp:Literal ID="ltlStatus" runat="server"></asp:Literal> 
    </ItemTemplate> 
    <EditItemTemplate>
    <asp:Label ID="lblHdStatus" runat="server"   Visible="false"></asp:Label>
    <asp:DropDownList ID="ddlStatus" runat="server"   > </asp:DropDownList>
    </EditItemTemplate>
    </asp:TemplateField> 



            <asp:TemplateField HeaderText="จัดการ">
               <ItemTemplate>
                        <div style="text-align: center">
                         <asp:Button ID="btnRowEdit" runat="server" Text="ตั้งค่า" class="btn-default btn-small " CommandName="Edit" />
                        
                            <asp:Button ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Delete" Text="ลบ"
                                class="btn-default btn-small " OnClientClick="return confirm('ยืนยันทำการ ลบข้อมูล')" />
                        </div>
                    </ItemTemplate> 

                   <EditItemTemplate>
                        <div style="text-align: center"> 
                        <asp:Button ID="btnRowUpdate" runat="server" Text="บันทึก" class="btn-default btn-small " CommandName="Update" />
                        <asp:Button ID="btnRowCancel" runat="server" Text="ยกเลิก" class="btn-default btn-small " CommandName="Cancel" /> 
                        </div>
                    </EditItemTemplate> 
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

</asp:Content>

