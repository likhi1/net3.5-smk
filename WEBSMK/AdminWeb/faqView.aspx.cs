﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;


public partial class adminWeb_faqView  : System.Web.UI.Page
{
    //=== global Var
    protected string ConId { get { return Request.QueryString["id"];  } }
    

    
    protected void Page_Load(object sender, EventArgs e) {
        ////////// Check  Session 
        // new CheckSess().CurrentSession(); 
 
        //////// Run Bind Data  
        if (!Page.IsPostBack) {
           
            BindData();
        }
    }

    protected void ddlCatBindData() {

        string sql = "SELECT * FROM tbFaq  ORDER BY FaqId ";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "myTb");
 
        
    }

    protected void BindData() {
        string sql1 = "SELECT * FROM tbFaq WHERE  FaqId= '" + ConId + "'  "; 
        DBClass obj1 = new DBClass();
        DataSet ds1 = obj1.SqlGet(sql1, "myTb");
        DataTable dt1 = ds1.Tables[0];

        if (dt1.Rows.Count > 0) {
            // foreach( string  x in dt1){ }  // *** see all value in  DataTable ***

            int  intId =  Convert.ToInt32( dt1.Rows[0][0].ToString() ) ;
            lblId.Text = String.Format("{0:00000}", intId);
            string strSort = dt1.Rows[0]["FaqSort"].ToString();
            tbxSort.Text =   Convert.ToDecimal(strSort) != 9999?strSort : "" ;

            rblStatus.SelectedValue = dt1.Rows[0]["FaqStatus"].ToString();

         
            tbxTopic.Text = dt1.Rows[0]["FaqQuiz"].ToString();
            tarDetailLong.Text = dt1.Rows[0]["FaqAnswer"].ToString();

        }
    }

    protected void btnSave_Click(object sender, EventArgs e) {
         
        /////// Update content
        string sql = "UPDATE  tbFaq SET "
        + "FaqSort = @FaqSort, "
        + "FaqQuiz = @FaqQuiz, "
        + "FaqAnswer = @FaqAnswer, "
        + "FaqStatus = @FaqStatus , "
            //===== AdminInfo Update  
        + "input_date = @input_date , "
        + "input_user = @input_user  "

        + "WHERE FaqId = '" + ConId + "'  ";
        
        
        Decimal decSort ;  
        SqlParameterCollection objParam1 = new SqlCommand().Parameters;
        objParam1.AddWithValue("@FaqSort", (decSort = (tbxSort.Text != "") ? Convert.ToDecimal(tbxSort.Text) : 9999));
        objParam1.AddWithValue("@FaqStatus", rblStatus.SelectedValue);
        objParam1.AddWithValue("@FaqQuiz", tbxTopic.Text);
        objParam1.AddWithValue("@FaqAnswer", tarDetailLong.Text);
       
        //===== AdminInfo Update  
        objParam1.AddWithValue("@input_date", DateTime.Now);

        //===========  Get UserName From Session Object 
        ProfileData loginData = (ProfileData)Session["SessAdmin"];
        objParam1.AddWithValue("@input_user", loginData.loginName);
        
 
        ///////  Get Value From Html Control 
        //objParam1.AddWithValue("@NewsPicSmall", SqlDbType.NVarChar).Value = Request.Form["PicSmall"].ToString();
        //objParam1.AddWithValue("@NewsPicBig", SqlDbType.NVarChar).Value = Request.Form["PicPic"].ToString();

        int i2 = new DBClass().SqlExecute(sql, objParam1); 
        //////// check Succes  
        if (i2 == 1) {
            Response.Write("<script>alert('แก้ไขข้อมูลเรียบร้อยแล้ว')</script>");
            Response.Redirect("faqView.aspx?id="+ ConId  );  
        } else {
            Response.Write("<script>alert('ไม่สามารถแก้ไขข้อมูลได้')</script>");
            Response.Write("<script>history.go(-1)</script>");
            //Response.Redirect("");
        }

    }

}