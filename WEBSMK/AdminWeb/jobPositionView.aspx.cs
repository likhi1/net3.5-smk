﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;
using System.Collections;



public partial class adminWeb_jobPositionView : System.Web.UI.Page
{

    public string Cat { get { return Request.QueryString["cat"]; } }
    public string JobId { get { return Request.QueryString["id"]; } }

    protected void Page_Load(object sender, EventArgs e) {
       
        if(!Page.IsPostBack){
          BindData();

        }
    }

    protected void BindData(){
     DataSet  ds = SelectData();
     DataTable dt =  ds.Tables[0] ;

     if (dt.Rows.Count > 0) {

       
        
        lblId.Text = dt.Rows[0]["JobPosiId"].ToString();
        tbxPosition.Text = dt.Rows[0]["JobPosiName"].ToString();
        tbxBranch.Text =  dt.Rows[0]["JobPosiBranch"].ToString(); 
        tbxDetail.Text = dt.Rows[0]["JobPosiDetail"].ToString();
        tbxQnt.Text = dt.Rows[0]["JobPosiQnt"].ToString();
        rblStatus.SelectedValue = dt.Rows[0]["JobPosiStatus"].ToString(); 
        rblPiority.SelectedValue = dt.Rows[0]["JobPiority"].ToString();


        Decimal dcSort = (Decimal)dt.Rows[0]["JobPosiSort"]   ;
        if (dcSort >= 9999) {
            tbxSort.Text = "";
        } else {
            tbxSort.Text = dcSort.ToString();
        }

 
         DateTime  dtStart =  (DateTime)dt.Rows[0]["JobPosiDtStart"] ;
         DateTime dtStop = (DateTime)dt.Rows[0]["JobPosiDtStop"];

         tbxDateStart.Text = dtStart.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
         tbxDateStop.Text = dtStop.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

       
        
      }

    }

    protected DataSet SelectData() {
        string sql = "SELECT * FROM tbJobPosi WHERE  JobPosiId ='" + JobId + "'  ";
        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql ,"tb");
        return ds;
    }


    protected void btnSave_Click(object sender, EventArgs e) {
        string sql = "UPDATE  tbJobPosi   SET  "
                        + " JobPosiName = @JobPosiName  , "
                        + " JobPosiBranch = @JobPosiBranch , "
                        + " JobPosiDetail =  @JobPosiDetail , "
                        + " JobPosiQnt = @JobPosiQnt , "
                        + " JobPosiStatus = @JobPosiStatus , "
                        + " JobPosiSort = @JobPosiSort , "
                        + " JobPosiDtStart = @JobPosiDtStart , "
                        + " JobPosiDtStop  = @JobPosiDtStop  , "
                        + " JobPiority = @JobPiority,  "
                          //===== AdminInfo Update  
         + "input_date = @input_date ,  "
         + "input_user = @input_user  " 
         
                        + " WHERE  JobPosiId ='" + JobId + "'  ";

        SqlParameterCollection objPm = new SqlCommand().Parameters;

        Decimal decSort;

        objPm.AddWithValue("@JobPosiName", tbxPosition.Text);
        objPm.AddWithValue("@JobPosiBranch", tbxBranch.Text);
        objPm.AddWithValue("@JobPosiDetail", tbxDetail.Text);
        objPm.AddWithValue("@JobPosiQnt", tbxQnt.Text);
        objPm.AddWithValue("@JobPosiStatus", rblStatus.SelectedValue);
        objPm.AddWithValue("@JobPosiSort", (decSort = (tbxSort.Text != "") ? Convert.ToDecimal(tbxSort.Text) : 9999));
        objPm.AddWithValue("@JobPosiDtStart", Convert.ToDateTime(tbxDateStart.Text, new CultureInfo("th-TH")));
        objPm.AddWithValue("@JobPosiDtStop", Convert.ToDateTime(tbxDateStop.Text, new CultureInfo("th-TH")));
        objPm.AddWithValue("@JobPiority", rblPiority.SelectedValue);
        
        //===== AdminInfo Update  
        objPm.AddWithValue("@input_date", DateTime.Now);
        //////Get UserName From Session Object
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        objPm.AddWithValue("@input_user", loginData.loginName);

       

        int i1 = new DBClass().SqlExecute(sql, objPm);

        if (i1 == 1) {
            Response.Write("<script>alert('แก้ไขข้อมูลเรียบร้อยแล้ว'); location='jobPositionView.aspx?cat=" + Cat + "&id="+JobId+"' ;</script>");
        } else {
            Response.Write("<script>alert('ไม่สามารถแก้ไขข้อมูลได้'); history.back() ;</script>");

        }
    }
 

}