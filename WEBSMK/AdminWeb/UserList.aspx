﻿<%@ Page Language="C#" MasterPageFile="~/AdminWeb/MasterPageAdmin1.master" AutoEventWireup="true" CodeFile="UserList.aspx.cs" Inherits="Module_Authenticate_UserList" Title="Untitled Page" %>
<%@ Register src="uc/ucPageNavigator.ascx" tagname="ucPageNavigator" tagprefix="uc1" %>
<%@ Register src="uc/ucTxtDate.ascx" tagname="ucTxtDate" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script language="javascript">
        function goPage(strPage) {
            window.location = "/<%=strAppName%>/" + strPage;
            return false;
        }
        function validateForm() {
            var isRet = true;
            var strMsg = "";
            var strMsg2 = "";
            var dateFrom;
            var dateTo;
            if (document.getElementById("<%=txtFromStartDate.txtID %>").value != "__/__/____" &&
                document.getElementById("<%=txtToStartDate.txtID %>").value != "__/__/____") {
                dateFrom = new Date(parseFloat(document.getElementById('ctl00_ContentPlaceHolder1_txtFromStartDate_txtDate').value.substring(6, 10)), parseFloat(document.getElementById('ctl00_ContentPlaceHolder1_ucTxtDate1_txtDate').value.substring(3, 5)) - 1, parseFloat(document.getElementById('ctl00_ContentPlaceHolder1_ucTxtDate1_txtDate').value.substring(0, 2)));
                dateTo = new Date(parseFloat(document.getElementById('ctl00_ContentPlaceHolder1_txtToStartDate_txtDate').value.substring(6, 10)), parseFloat(document.getElementById('ctl00_ContentPlaceHolder1_ucTxtDate2_txtDate').value.substring(3, 5)) - 1, parseFloat(document.getElementById('ctl00_ContentPlaceHolder1_ucTxtDate2_txtDate').value.substring(0, 2)));
                if (dateFrom > dateTo) {
                    isRet = false;
                    strMsg2 += "! กรุณาตรวจสอบวันที่เริ่มใช้งาน วันที่ตั้งแต่ต้องน้อยกว่าวันที่ถึง\n";
                }
            }

            if (isRet == false) {
                if (strMsg != "")
                    alert("! กรุณาระบุข้อมูล\n" + strMsg + strMsg2);
                else
                    alert(strMsg2);
            }
            return isRet;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div id="page-prdList">
              <div id="manageArea">
            <div  class="colLeft">
        <h1 class="subject1">ผู้ใช้งานระบบ</h1>        
        <div class="navManage">
        <asp:Button ID="btnAdd" runat="server" Text="เพิ่มรายการใหม่" OnClientClick="return goPage('Adminweb/UserForm.aspx');" CssClass="btn-default"/>                        
        </div>
                </div><!-- end  class="colLeft" --> 
<div class="colRight">
<div class="areaSearch">



                       
            <table width="100%">
                <tr>
                    <td class="label">รหัสผู้ใช้งาน :</td>
                    <td align="left">
                        <asp:TextBox ID="txtLoginName" runat="server" CssClass="TEXTBOX" Width="100" MaxLength="13"></asp:TextBox>
                    </td>  
                    
                     <td class="label">กลุ่มหน้าที่การใช้งาน :</td>
                    <td align="left">
                        <asp:DropDownList ID="ddlRole" runat="server">
                        </asp:DropDownList>
                    </td>
                                              
                </tr>
                <tr>
                    <td class="label">ชื่อผู้ใช้งาน :</td>
                    <td align="left">
                        <asp:TextBox ID="txtFName" runat="server" CssClass="TEXTBOX" Width="150" MaxLength="100"></asp:TextBox>
                    </td>
                    <td class="label">นามสกุล :</td>
                    <td align="left">
                        <asp:TextBox ID="txtLName" runat="server" CssClass="TEXTBOX" Width="150" MaxLength="100"></asp:TextBox>
                    </td>                            
                </tr>
                
                <tr>
                    <td class="label">วันที่เริ่มใช้งานตั้งแต่ :</td>
                    <td align="left">
                        <uc1:ucTxtDate ID="txtFromStartDate" runat="server" />
                    </td>
                    <td class="label">วันที่เริ่มใช้งานถึง :</td>
                    <td align="left">
                        <uc1:ucTxtDate ID="txtToStartDate" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td  ></td>  <td  ></td>  <td  ></td>
                    <td  >
                        <asp:Button ID="btnSearch" runat="server" Text="ค้นหา"
                            onclick="btnSearch_Click" OnClientClick="return validateForm();" CssClass="btn-default btn-small"/>
                        <asp:Button ID="btnClear" runat="server" Text="เคลียร์หน้าจอ" OnClientClick="return goPage('Adminweb/UserList.aspx');" CssClass="btn-default btn-small"/>
                    </td>
                </tr>
            </table>
        
    </div>
<!-- End  class="areaSearch" -->
    
    
    </div><!-- end  class="colRight" -->        
</div><!-- End  id="manageArea" -->

          <style> 
        /*===== Control  GridView1  Width (support IE9 & Over) =====*/ 
        [id*=dtgData] th:nth-child(1) {width: 8% ; }
        [id*=dtgData] th:nth-child(2) { }
        [id*=dtgData] th:nth-child(3) {width:10%; }
        [id*=dtgData] th:nth-child(4) {width: 7% } 
        [id*=dtgData] th:nth-child(5) {width: 7% } 
        [id*=dtgData] th:nth-child(6) {width: 7% }
        [id*=dtgData] th:nth-child(7) {width: 7% }  
        </style>
 



        <div class="list-data" >      
            <table width="100%">
                <tr>
                    <td>                            
                        <uc1:ucpagenavigator ID="ucPageNavigator1" runat="server" 
                            OnPageIndexChanged="PageIndexChanged"/>                            
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DataGrid ID="dtgData" runat="server" AutoGenerateColumns="false" Width="100%"
                            HeaderStyle-CssClass="gridhead" ItemStyle-CssClass="gridrow0" AlternatingItemStyle-CssClass="gridrow1"
                            AllowPaging="true" PagerStyle-Visible="false"
                        UseAccessibleHeader="true"
                            
                            >
                            <Columns>
                                <asp:TemplateColumn>
			                        <HeaderTemplate>
                                        <asp:Label ID="lblHeadNo" runat="server" Text="<%#ColumnTitle[0]%>"></asp:Label>							                
			                        </HeaderTemplate>
			                        <ItemTemplate>
			                            <%# Container.DataSetIndex +1%>
			                        </ItemTemplate>
		                        </asp:TemplateColumn>
                                <asp:TemplateColumn>
			                        <HeaderTemplate>
				                        <asp:LinkButton ID="lnkID1" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[1]%>">
				                        </asp:LinkButton>
				                        <asp:Image ID="imgSortCol1" Runat="server" ImageUrl="<%#getPicUrl(1)%>">
				                        </asp:Image>
			                        </HeaderTemplate>
			                        <ItemTemplate>
			                            <a href="#" onclick="goPage('AdminWeb/UserForm.aspx?user_login=<%#Eval(ColumnName[1])%>');"><%#Eval(ColumnName[1])%></a>
			                        </ItemTemplate>
		                        </asp:TemplateColumn>
		                        <asp:TemplateColumn>
			                        <HeaderTemplate>
				                        <asp:LinkButton ID="lnkID2" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[2]%>">
				                        </asp:LinkButton>
				                        <asp:Image ID="imgSortCol2" Runat="server" ImageUrl="<%#getPicUrl(2)%>">
				                        </asp:Image>
			                        </HeaderTemplate>
			                        <ItemTemplate>
			                            <%#Eval(ColumnName[2])%>
			                        </ItemTemplate>
		                        </asp:TemplateColumn>
		                        <asp:TemplateColumn>
			                        <HeaderTemplate>
				                        <asp:LinkButton ID="lnkID3" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[3]%>">
				                        </asp:LinkButton>
				                        <asp:Image ID="imgSortCol3" Runat="server" ImageUrl="<%#getPicUrl(3)%>">
				                        </asp:Image>
			                        </HeaderTemplate>
			                        <ItemTemplate>
			                            <%#Eval(ColumnName[3])%>
			                        </ItemTemplate>
		                        </asp:TemplateColumn>
		                        <asp:TemplateColumn>
			                        <HeaderTemplate>
				                        <asp:LinkButton ID="lnkID4" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[4]%>">
				                        </asp:LinkButton>
				                        <asp:Image ID="imgSortCol4" Runat="server" ImageUrl="<%#getPicUrl(4)%>">
				                        </asp:Image>
			                        </HeaderTemplate>
			                        <ItemTemplate>
			                            <%#FormatStringApp.FormatDate(Eval(ColumnName[4]))%>
			                        </ItemTemplate>
		                        </asp:TemplateColumn>
		                        <asp:TemplateColumn>
			                        <HeaderTemplate>
				                        <asp:LinkButton ID="lnkID5" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[5]%>">
				                        </asp:LinkButton>
				                        <asp:Image ID="imgSortCol5" Runat="server" ImageUrl="<%#getPicUrl(5)%>">
				                        </asp:Image>
			                        </HeaderTemplate>
			                        <ItemTemplate>
			                            <%#FormatStringApp.FormatDate(Eval(ColumnName[5]))%>
			                        </ItemTemplate>
		                        </asp:TemplateColumn>
		                        <asp:TemplateColumn>
			                        <HeaderTemplate>
				                        <asp:LinkButton ID="lnkID6" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[6]%>">
				                        </asp:LinkButton>
				                        <asp:Image ID="imgSortCol6" Runat="server" ImageUrl="<%#getPicUrl(6)%>">
				                        </asp:Image>
			                        </HeaderTemplate>
			                        <ItemTemplate>
			                            <%#FormatStringApp.getStatusName(Eval(ColumnName[6]))%>
			                        </ItemTemplate>
		                        </asp:TemplateColumn>
		                    </Columns>
                        </asp:DataGrid>
                    </td>
                </tr>
            </table>
        </div>                        
    </div> <!-- end  id="page-prdList" -->    
    <asp:Literal ID="litJavaScript" runat="server"></asp:Literal>                
</asp:Content>

