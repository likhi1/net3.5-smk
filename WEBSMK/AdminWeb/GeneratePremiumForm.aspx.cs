﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Web.Services;

public partial class AdminWeb_GeneratePremiumForm : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initialDropdownList();
            txtStartDate.Text = FormatStringApp.FormatDate(DateTime.Now);
            ddlProduct.Attributes.Add("onchange", "PopulateChild(this.value,document.getElementById(\'" + ddlPromotion.ClientID + "\'))");
        }
        else
        {
            litScript.Text = "<script>";
            litScript.Text += " PopulateChild('" + ddlProduct.SelectedValue + "',document.getElementById(\'" + ddlPromotion.ClientID + "\'));";
            litScript.Text += " document.getElementById('" + ddlPromotion.ClientID + "').value = '" + Request.Form[ddlPromotion.UniqueID] + "';";
            litScript.Text += "</script>";
        }
    }    
    protected void btnSave_Click(object sender, EventArgs e)
    {
        DataSet dsTariffKey = new DataSet();
        DataSet dsDistinct = new DataSet();
        DataSet dsDefaultValue = new DataSet();
        DataSet dsTariffInsure = new DataSet();
        DataSet dsReduceInsure = new DataSet();
        DataSet dsTmp = new DataSet();
        DataSet dsTaxRate = new DataSet();
        DataRow[] drDefaultAge;
        DataRow[] drDefaultGroup;        
        DataRow[] drDefaultSize;
        DataRow[] drDefaultBody;
        DataRow[] drDefaultdriver;
        DataRow[] drTmp;
        DataTable dtPolType;
        DataTable dtMotGar;
        DataTable dtCarMak;
        DataTable dtAgtCod;           
        string strPolTypeValue = "";
        string strMotGarValue = "";
        string strAgeValue = "";
        string strGroupValue = "";
        string strCarMakValue = "";
        string strSizeValue = "";
        string strBodyValue = "";
        string strAgtCodValue = "";
        string strDriverValue = "";
        double dMainPremium = 0;
        double dMainPremium1 = 0;
        double dCar01 = 0;
        double dCar02 = 0;
        double dCar03 = 0;
        int iPremium = 0;
        double dPeriod = 0;
        double dTax = 0;
        double dStamp = 0;
        string strPolTypeFactor = "";
        string strCampaignReduce = "";
        string[] ColumnName = { "campaign", "car_cod", "pol_typ", "key_carage", "key_cargroup", 
                                    "key_carsize", "key_carbody", "mot_gar", "car_age", "car_group", "car_size",
                                    "car_body", "car_driver", "sum_insure", "base_premium", "factor_caruse", 
                                    "factor_carsize", "factor_cardriver", "factor_carage", "factor_carinsure", "factor_cargroup", 
                                    "factor_carbody", "factor_tpbi1", "factor_tpbi2", "factor_tppd", "main_premium", 
                                    "perm_d_01", "perm_p_01", "perm_p_num", "temp_d_01", "temp_p_01", 
                                    "temp_p_num", "factor_permd01", "factor_permp01", "factor_tempd01", "factor_tempp01", 
                                    "car_01", "cover_02", "factor_car02", "car_02", "cover_03", 
                                    "factor_car03", "car_03", "od_dd", "net_premium", "stamp",
                                     "tax", "gross_premium"};
        string[] ColumnType = { "string", "string", "string", "string", "string", 
                                    "string", "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string"};
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        VoluntaryManager cm = new VoluntaryManager();
        PremiumManager cmPrem = new PremiumManager();
        SqlConnection conn;
        SqlTransaction trans;
        string strCoverID = "";
        string strHeadPremID = "";
        conn = cmPrem.getMyConnection();
        trans = conn.BeginTransaction();
        try
        {
            dPeriod = Convert.ToDouble(ddlPeriod.SelectedValue);
            dt = TDS.Utility.MasterUtil.AddColumnToDataTable(dt, ColumnType, ColumnName);
            ds.Tables.Add(dt);

            if (Request.Form[ddlPromotion.UniqueID] != "")
                strCampaignReduce = Request.Form[ddlPromotion.UniqueID];
            else
                strCampaignReduce = ddlProduct.SelectedValue;

            dsDistinct = cm.getDataDistinctTariffInsure(ddlProduct.SelectedValue, txtCarCode.Text);
            dtPolType = dsDistinct.Tables[0].DefaultView.ToTable(true, new string[] { "pol_typ" });
            dtMotGar = dsDistinct.Tables[0].DefaultView.ToTable(true, new string[] { "mot_gar" });
            dtCarMak = dsDistinct.Tables[0].DefaultView.ToTable(true, new string[] { "car_mak" });
            dtAgtCod = dsDistinct.Tables[0].DefaultView.ToTable(true, new string[] { "agt_cod" });
            dsTariffKey = cm.getDataTariffKey(ddlProduct.SelectedValue, txtCarCode.Text);
            dsDefaultValue = cmPrem.getDataDefaultValue(txtCarCode.Text, "");
            drDefaultAge = dsDefaultValue.Tables[0].Select("defv_type = 'CAR_AGE'");
            drDefaultGroup = dsDefaultValue.Tables[0].Select("defv_type = 'CAR_GROUP'");
            drDefaultSize = dsDefaultValue.Tables[0].Select("defv_type = 'CAR_SIZE'");
            drDefaultBody = dsDefaultValue.Tables[0].Select("defv_type = 'CAR_BODY'");
            drDefaultdriver = dsDefaultValue.Tables[0].Select("defv_type = 'DRIVER'");
            dsTaxRate = cm.getTaxRate(txtStartDate.Text);

            cmPrem.clearDataOnlinePremiumCover(ddlProduct.SelectedValue, txtCarCode.Text, 
                                                txtSumInsuredFrom.Text, txtSumInsuredTo.Text, 
                                                conn, trans);

            // add row in dsTariffKey if not found pol_typ then add Y value            
            for (int iPolType = 0; iPolType <= dtPolType.Rows.Count - 1; iPolType++)
            {
                if (dsTariffKey.Tables[0].Select("pol_typ = '" + dtPolType.Rows[iPolType]["pol_typ"].ToString() + "'").Length < 1)
                {
                    dsTariffKey.Tables[0].Rows.Add(dsTariffKey.Tables[0].NewRow());
                    dsTariffKey.Tables[0].Rows[dsTariffKey.Tables[0].Rows.Count - 1]["campaign"] = ddlProduct.SelectedValue;
                    dsTariffKey.Tables[0].Rows[dsTariffKey.Tables[0].Rows.Count - 1]["car_cod"] = txtCarCode.Text;
                    dsTariffKey.Tables[0].Rows[dsTariffKey.Tables[0].Rows.Count - 1]["pol_typ"] = dtPolType.Rows[iPolType]["pol_typ"].ToString();
                    dsTariffKey.Tables[0].Rows[dsTariffKey.Tables[0].Rows.Count - 1]["car_age"] = "Y";
                    dsTariffKey.Tables[0].Rows[dsTariffKey.Tables[0].Rows.Count - 1]["car_group"] = "Y";
                    dsTariffKey.Tables[0].Rows[dsTariffKey.Tables[0].Rows.Count - 1]["car_size"] = "Y";
                    dsTariffKey.Tables[0].Rows[dsTariffKey.Tables[0].Rows.Count - 1]["car_body"] = "Y";
                    dsTariffKey.Tables[0].Rows[dsTariffKey.Tables[0].Rows.Count - 1]["car_mak"] = "Y";
                    dsTariffKey.Tables[0].Rows[dsTariffKey.Tables[0].Rows.Count - 1]["car_od"] = "Y";
                }
            }
            strHeadPremID = cmPrem.addDataOnlineHeadPremium(ddlProduct.SelectedValue, strCampaignReduce,
                                            txtCarCode.Text, txtSumInsuredFrom.Text,
                                            txtSumInsuredTo.Text, "0",
                                            conn, trans);

            for (int iTariffKey = 0; iTariffKey <= dsTariffKey.Tables[0].Rows.Count - 1; iTariffKey++)
            {
                strPolTypeValue = dsTariffKey.Tables[0].Rows[iTariffKey]["pol_typ"].ToString();
                if (strPolTypeValue == "5")
                    strPolTypeFactor = "2";
                else
                    strPolTypeFactor = strPolTypeValue;
                for (int iMotGar = 0; iMotGar <= dtMotGar.Rows.Count - 1; iMotGar++)
                {
                    strMotGarValue = dtMotGar.Rows[iMotGar]["mot_gar"].ToString();
                    for (int iCarAge = 0; iCarAge <= drDefaultAge.Length - 1; iCarAge++)
                    {
                        if (dsTariffKey.Tables[0].Rows[iTariffKey]["car_age"].ToString() == "Y")
                            strAgeValue = "0";
                        else
                            strAgeValue = drDefaultAge[iCarAge]["defv_value"].ToString();
                        for (int iCarGroup = 0; iCarGroup <= drDefaultGroup.Length - 1; iCarGroup++)
                        {
                            if (dsTariffKey.Tables[0].Rows[iTariffKey]["car_group"].ToString() == "Y")
                                strGroupValue = "A";
                            else
                                strGroupValue = drDefaultGroup[iCarGroup]["defv_value"].ToString();
                            // add row in drDefaultCarMak
                            if (dsTariffKey.Tables[0].Rows[iTariffKey]["car_mak"].ToString() == "Y")
                            {
                                dtCarMak.Rows.Clear();
                                dtCarMak.Rows.Add(dtCarMak.NewRow());
                                dtCarMak.Rows[dtCarMak.Rows.Count - 1]["car_mak"] = "A";
                            }
                            else if (dsTariffKey.Tables[0].Rows[iTariffKey]["car_mak"].ToString() == "N")
                            {
                                DataRow[] drs = dtCarMak.Select("car_mark = 'A'", "");
                                if (drs.Length > 0)
                                    dtCarMak.Rows.Remove(drs[0]);
                            }
                            for (int iCarMak = 0; iCarMak <= dtCarMak.Rows.Count - 1; iCarMak++)
                            {
                                strCarMakValue = dtCarMak.Rows[iCarMak]["car_mak"].ToString();
                                for (int iCarSize = 0; iCarSize <= drDefaultSize.Length - 1; iCarSize++)
                                {
                                    if (dsTariffKey.Tables[0].Rows[iTariffKey]["car_size"].ToString() == "Y")
                                        strSizeValue = "0";
                                    else
                                        strSizeValue = drDefaultSize[iCarSize]["defv_value"].ToString();
                                    for (int iCarBody = 0; iCarBody <= drDefaultBody.Length - 1; iCarBody++)
                                    {
                                        if (dsTariffKey.Tables[0].Rows[iTariffKey]["car_body"].ToString() == "Y")
                                            strBodyValue = "A";
                                        else
                                            strBodyValue = drDefaultBody[iCarBody]["defv_value"].ToString();
                                        for (int iAgtCod = 0; iAgtCod <= dtAgtCod.Rows.Count - 1; iAgtCod++)
                                        {
                                            strAgtCodValue = dtAgtCod.Rows[iAgtCod]["agt_cod"].ToString();
                                            dsTariffInsure = cm.getDataTariffInsure(ddlProduct.SelectedValue, txtCarCode.Text,
                                                                     strPolTypeValue, strMotGarValue,
                                                                     strAgeValue, strGroupValue,
                                                                     strCarMakValue, strSizeValue,
                                                                     strBodyValue, strAgtCodValue);
                                            dsReduceInsure = cm.getDataReduceInsure(strCampaignReduce, txtCarCode.Text,
                                                                     strPolTypeValue, strMotGarValue,
                                                                     strAgeValue, strGroupValue,
                                                                     strCarMakValue, strSizeValue,
                                                                     strBodyValue, strAgtCodValue);
                                            if (dsTariffInsure.Tables[0].Rows.Count > 0 &&
                                                Convert.ToDouble(dsTariffInsure.Tables[0].Rows[0]["rate_use"].ToString()) > 0)
                                            {
                                                strCoverID = cmPrem.addDataOnlineCover(ddlProduct.SelectedValue, txtCarCode.Text,
                                                                            strPolTypeValue, dtMotGar.Rows[iMotGar]["mot_gar"].ToString(),
                                                                            drDefaultAge[iCarAge]["defv_value"].ToString(), drDefaultGroup[iCarGroup]["defv_value"].ToString(),
                                                                            dtCarMak.Rows[iCarMak]["car_mak"].ToString(), drDefaultSize[iCarSize]["defv_value"].ToString(),
                                                                            drDefaultBody[iCarBody]["defv_value"].ToString(), dtAgtCod.Rows[iAgtCod]["agt_cod"].ToString(),
                                                                            dsTariffInsure.Tables[0].Rows[0]["tpbi1"].ToString(),dsTariffInsure.Tables[0].Rows[0]["tpbi2"].ToString(),
                                                                            dsTariffInsure.Tables[0].Rows[0]["tppd"].ToString(),"0",
                                                                            dsTariffInsure.Tables[0].Rows[0]["od_dd"].ToString(), dsTariffInsure.Tables[0].Rows[0]["perm_d_01"].ToString(),
                                                                            dsTariffInsure.Tables[0].Rows[0]["perm_p_num"].ToString(),dsTariffInsure.Tables[0].Rows[0]["perm_p_01"].ToString(),
                                                                            dsTariffInsure.Tables[0].Rows[0]["temp_d_01"].ToString(), dsTariffInsure.Tables[0].Rows[0]["temp_p_num"].ToString(),
                                                                            dsTariffInsure.Tables[0].Rows[0]["temp_p_01"].ToString(), dsTariffInsure.Tables[0].Rows[0]["cover_02"].ToString(),
                                                                            dsTariffInsure.Tables[0].Rows[0]["cover_03"].ToString(),
                                                                            conn, trans);
                                                for (int iDriver = 0; iDriver <= drDefaultdriver.Length - 1; iDriver++)
                                                {
                                                    strDriverValue = drDefaultdriver[iDriver]["defv_value"].ToString();
                                                    
                                                    for (double dSumIns = Convert.ToDouble(txtSumInsuredFrom.Text); dSumIns <= Convert.ToDouble(txtSumInsuredTo.Text); dSumIns = dSumIns + dPeriod)
                                                    {
                                                        if (strPolTypeValue == "3" && 
                                                            (dsTariffKey.Tables[0].Rows[iTariffKey]["campaign"].ToString() == "000" ||
                                                             dsTariffKey.Tables[0].Rows[iTariffKey]["campaign"].ToString() == "013"))
                                                            dSumIns = 0;
                                                        ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                                                        iPremium = ds.Tables[0].Rows.Count - 1;
                                                        ds.Tables[0].Rows[iPremium]["campaign"] = dsTariffKey.Tables[0].Rows[iTariffKey]["campaign"].ToString();
                                                        ds.Tables[0].Rows[iPremium]["car_cod"] = dsTariffKey.Tables[0].Rows[iTariffKey]["car_cod"].ToString();
                                                        ds.Tables[0].Rows[iPremium]["pol_typ"] = dsTariffKey.Tables[0].Rows[iTariffKey]["pol_typ"].ToString();
                                                        ds.Tables[0].Rows[iPremium]["key_carage"] = dsTariffKey.Tables[0].Rows[iTariffKey]["car_age"].ToString();
                                                        ds.Tables[0].Rows[iPremium]["key_cargroup"] = dsTariffKey.Tables[0].Rows[iTariffKey]["car_group"].ToString();
                                                        ds.Tables[0].Rows[iPremium]["key_carsize"] = dsTariffKey.Tables[0].Rows[iTariffKey]["car_size"].ToString();
                                                        ds.Tables[0].Rows[iPremium]["key_carbody"] = dsTariffKey.Tables[0].Rows[iTariffKey]["car_body"].ToString();
                                                        ds.Tables[0].Rows[iPremium]["mot_gar"] = dtMotGar.Rows[iMotGar]["mot_gar"].ToString();
                                                        ds.Tables[0].Rows[iPremium]["car_age"] = drDefaultAge[iCarAge]["defv_value"].ToString();
                                                        ds.Tables[0].Rows[iPremium]["car_group"] = drDefaultGroup[iCarGroup]["defv_value"].ToString();
                                                        ds.Tables[0].Rows[iPremium]["car_size"] = drDefaultSize[iCarSize]["defv_value"].ToString();
                                                        ds.Tables[0].Rows[iPremium]["car_body"] = drDefaultBody[iCarBody]["defv_value"].ToString();
                                                        ds.Tables[0].Rows[iPremium]["car_driver"] = strDriverValue;
                                                        ds.Tables[0].Rows[iPremium]["sum_insure"] = FormatStringApp.FormatInt(dSumIns.ToString());
                                                        if (strPolTypeValue == "5")
                                                        {
                                                            ds.Tables[0].Rows[iPremium]["base_premium"] = dsTariffInsure.Tables[0].Rows[0]["rate_use_ft"].ToString();
                                                        }
                                                        else
                                                        {
                                                            ds.Tables[0].Rows[iPremium]["base_premium"] = dsTariffInsure.Tables[0].Rows[0]["rate_use"].ToString();
                                                        }

                                                        dsTmp = cm.getDataFactorCarUse(txtCarCode.Text, strPolTypeFactor);
                                                        if (dsTmp.Tables[0].Rows.Count > 0)
                                                        {
                                                            ds.Tables[0].Rows[iPremium]["factor_caruse"] = dsTmp.Tables[0].Rows[0]["rate"].ToString();
                                                        }
                                                        else
                                                        {
                                                            ds.Tables[0].Rows[iPremium]["factor_caruse"] = "100";
                                                        }
                                                        dsTmp = cm.getDataFactorCarSize(txtCarCode.Text, strPolTypeFactor, drDefaultSize[iCarSize]["defv_value"].ToString());
                                                        if (dsTmp.Tables[0].Rows.Count > 0)
                                                        {
                                                            ds.Tables[0].Rows[iPremium]["factor_carsize"] = dsTmp.Tables[0].Rows[0]["rate"].ToString();
                                                        }
                                                        else
                                                        {
                                                            ds.Tables[0].Rows[iPremium]["factor_carsize"] = "100";
                                                        }
                                                        dsTmp = cm.getDataFactorDriver(txtCarCode.Text, strPolTypeFactor, strDriverValue);
                                                        if (dsTmp.Tables[0].Rows.Count > 0)
                                                        {
                                                            ds.Tables[0].Rows[iPremium]["factor_cardriver"] = dsTmp.Tables[0].Rows[0]["rate"].ToString();
                                                        }
                                                        else
                                                        {
                                                            ds.Tables[0].Rows[iPremium]["factor_cardriver"] = "100";
                                                        }
                                                        dsTmp = cm.getDataFactorCarAge(txtCarCode.Text, strPolTypeFactor, drDefaultAge[iCarAge]["defv_value"].ToString());
                                                        if (dsTmp.Tables[0].Rows.Count > 0)
                                                        {
                                                            ds.Tables[0].Rows[iPremium]["factor_carage"] = dsTmp.Tables[0].Rows[0]["rate"].ToString();
                                                        }
                                                        else
                                                        {
                                                            ds.Tables[0].Rows[iPremium]["factor_carage"] = "100";
                                                        }
                                                        dsTmp = cm.getDataFactorCarSumInsure(txtCarCode.Text, strPolTypeFactor, dSumIns.ToString());
                                                        if (dsTmp.Tables[0].Rows.Count > 0)
                                                        {
                                                            ds.Tables[0].Rows[iPremium]["factor_carinsure"] = dsTmp.Tables[0].Rows[0]["rate"].ToString();
                                                        }
                                                        else
                                                        {
                                                            ds.Tables[0].Rows[iPremium]["factor_carinsure"] = "100";
                                                        }
                                                        dsTmp = cm.getDataFactorCarGroup(txtCarCode.Text, strPolTypeFactor, drDefaultGroup[iCarGroup]["defv_value"].ToString());
                                                        if (dsTmp.Tables[0].Rows.Count > 0)
                                                        {
                                                            ds.Tables[0].Rows[iPremium]["factor_cargroup"] = dsTmp.Tables[0].Rows[0]["rate"].ToString();
                                                        }
                                                        else
                                                            ds.Tables[0].Rows[iPremium]["factor_cargroup"] = "100";

                                                        ds.Tables[0].Rows[iPremium]["factor_carbody"] = "100";
                                                        dsTmp = cm.getDataTpbiTppd(txtCarCode.Text, strPolTypeFactor,
                                                                                    dsTariffInsure.Tables[0].Rows[0]["tppd"].ToString(),
                                                                                    dsTariffInsure.Tables[0].Rows[0]["tpbi1"].ToString(),
                                                                                    dsTariffInsure.Tables[0].Rows[0]["tpbi2"].ToString());
                                                        if (dsTmp.Tables[0].Rows.Count > 0)
                                                        {
                                                            drTmp = dsTmp.Tables[0].Select("cover_typ = '2'", "");
                                                            if (drTmp.Length > 0)
                                                            {
                                                                if (Convert.IsDBNull(drTmp[0]["rate"]))
                                                                    ds.Tables[0].Rows[iPremium]["factor_tpbi1"] = "1";
                                                                else
                                                                    ds.Tables[0].Rows[iPremium]["factor_tpbi1"] = drTmp[0]["rate"].ToString();
                                                            }
                                                            else
                                                                ds.Tables[0].Rows[iPremium]["factor_tpbi1"] = "1";
                                                            drTmp = dsTmp.Tables[0].Select("cover_typ = '3'", "");
                                                            if (drTmp.Length > 0)
                                                            {
                                                                if (Convert.IsDBNull(drTmp[0]["rate"]))
                                                                    ds.Tables[0].Rows[iPremium]["factor_tpbi2"] = "1";
                                                                else
                                                                    ds.Tables[0].Rows[iPremium]["factor_tpbi2"] = drTmp[0]["rate"].ToString();
                                                            }
                                                            else
                                                                ds.Tables[0].Rows[iPremium]["factor_tpbi2"] = "1";
                                                            drTmp = dsTmp.Tables[0].Select("cover_typ = '1'", "");
                                                            if (drTmp.Length > 0)
                                                            {
                                                                if (Convert.IsDBNull(drTmp[0]["rate"]))
                                                                    ds.Tables[0].Rows[iPremium]["factor_tppd"] = "1";
                                                                else
                                                                    ds.Tables[0].Rows[iPremium]["factor_tppd"] = drTmp[0]["rate"].ToString();
                                                            }
                                                            else
                                                                ds.Tables[0].Rows[iPremium]["factor_tppd"] = "1";
                                                        }
                                                        else
                                                        {
                                                            ds.Tables[0].Rows[iPremium]["factor_tpbi1"] = "1";
                                                            ds.Tables[0].Rows[iPremium]["factor_tpbi2"] = "1";
                                                            ds.Tables[0].Rows[iPremium]["factor_tppd"] = "1";
                                                        }
                                                        // หาเบี้ยหลัก
                                                        try
                                                        {
                                                            dMainPremium = Convert.ToDouble(ds.Tables[0].Rows[iPremium]["base_premium"]);
                                                            dMainPremium = dMainPremium * Convert.ToDouble(ds.Tables[0].Rows[iPremium]["factor_caruse"]) / 100;
                                                            dMainPremium = dMainPremium * Convert.ToDouble(ds.Tables[0].Rows[iPremium]["factor_carsize"]) / 100;
                                                            dMainPremium = dMainPremium * Convert.ToDouble(ds.Tables[0].Rows[iPremium]["factor_cardriver"]) / 100;
                                                            dMainPremium = dMainPremium * Convert.ToDouble(ds.Tables[0].Rows[iPremium]["factor_carage"]) / 100;
                                                            dMainPremium = dMainPremium * Convert.ToDouble(ds.Tables[0].Rows[iPremium]["factor_carinsure"]) / 100;
                                                            dMainPremium = dMainPremium * Convert.ToDouble(ds.Tables[0].Rows[iPremium]["factor_cargroup"]) / 100;
                                                            dMainPremium = dMainPremium * Convert.ToDouble(ds.Tables[0].Rows[iPremium]["factor_tpbi1"]);
                                                            dMainPremium = dMainPremium * Convert.ToDouble(ds.Tables[0].Rows[iPremium]["factor_tpbi2"]);                                                            
                                                            dMainPremium = dMainPremium * Math.Round(Convert.ToDouble(ds.Tables[0].Rows[iPremium]["factor_tppd"]),4);   
                                                            //dMainPremium1 = dMainPremium;
                                                            
                                                            // ตรวจสอบกรณี 3 คุ้ม
                                                            if (cm.is3K(ddlProduct.SelectedValue))
                                                            {
                                                                dsTmp = cm.getDataTariffRate(ddlProduct.SelectedValue, txtCarCode.Text, strSizeValue);
                                                                if (dsTmp.Tables[0].Rows.Count > 0)
                                                                {
                                                                    dMainPremium = dMainPremium + Math.Round((Convert.ToDouble(dsTmp.Tables[0].Rows[0]["od"].ToString()) * Convert.ToDouble(dsTmp.Tables[0].Rows[0]["rate_num"].ToString()) / 100),0,MidpointRounding.AwayFromZero);//.5 ปัดขึ้น
                                                                    //dMainPremium = dMainPremium + ((Convert.ToDouble(dsTmp.Tables[0].Rows[0]["od"].ToString()) * Convert.ToDouble(dsTmp.Tables[0].Rows[0]["rate_num"].ToString()) / 100));
                                                                }
                                                            }
                                                            dMainPremium = Math.Round(dMainPremium);
                                                            //TDS.Utility.MasterUtil.writeLog("dMainPremium1", Convert.ToString(Math.Round(dMainPremium, 5)));
                                                            // กรณีกรมธรรม์ประเภท 5
                                                            if (strPolTypeValue == "5")
                                                            {
                                                                double dMainPremium2 = 0;
                                                                dsTmp = cm.getDataFactorCarUse(txtCarCode.Text, strPolTypeValue);
                                                                if (dsTmp.Tables[0].Rows.Count > 0)
                                                                {
                                                                    dMainPremium2 = Convert.ToDouble(dsTmp.Tables[0].Rows[0]["rate"].ToString()) /100;
                                                                }
                                                                else
                                                                {
                                                                    dMainPremium2 = 1;
                                                                }
                                                                dsTmp = cm.getDataFactorCarSumInsure(txtCarCode.Text, strPolTypeValue, dSumIns.ToString());
                                                                if (dsTmp.Tables[0].Rows.Count > 0)
                                                                {
                                                                    dMainPremium2 = dMainPremium2 * Convert.ToDouble(dsTmp.Tables[0].Rows[0]["rate"].ToString()) /100;
                                                                }
                                                                else
                                                                {
                                                                    dMainPremium2 = dMainPremium2 * 1;
                                                                }
                                                                dsTmp = cm.getDataTariffInsure(ddlProduct.SelectedValue, txtCarCode.Text,
                                                                     strPolTypeValue, strMotGarValue,
                                                                     strAgeValue, strGroupValue,
                                                                     strCarMakValue, strSizeValue,
                                                                     strBodyValue, strAgtCodValue);
                                                                if (dsTmp.Tables[0].Rows.Count > 0)
                                                                {
                                                                    dMainPremium2 = dMainPremium2 * Convert.ToDouble(dsTmp.Tables[0].Rows[0]["rate_use"].ToString());
                                                                }
                                                                else
                                                                {
                                                                    dMainPremium2 = dMainPremium2 * 1;
                                                                }
                                                                dMainPremium = dMainPremium + dMainPremium2;
                                                            }
                                                        }
                                                        catch (Exception e1)
                                                        {
                                                            TDS.Utility.MasterUtil.writeError("GeneratePremiumForm.btnSave_Click", e1.ToString());
                                                            //break;
                                                        }
                                                        ds.Tables[0].Rows[iPremium]["main_premium"] = dMainPremium.ToString();
                                                        //รย01
                                                        ds.Tables[0].Rows[iPremium]["perm_d_01"] = dsTariffInsure.Tables[0].Rows[0]["perm_d_01"].ToString() == "" ? "0" : dsTariffInsure.Tables[0].Rows[0]["perm_d_01"].ToString();
                                                        ds.Tables[0].Rows[iPremium]["perm_p_01"] = dsTariffInsure.Tables[0].Rows[0]["perm_p_01"].ToString() == "" ? "0" : dsTariffInsure.Tables[0].Rows[0]["perm_p_01"].ToString();
                                                        ds.Tables[0].Rows[iPremium]["perm_p_num"] = dsTariffInsure.Tables[0].Rows[0]["perm_p_num"].ToString() == "" ? "0" : dsTariffInsure.Tables[0].Rows[0]["perm_p_num"].ToString();
                                                        ds.Tables[0].Rows[iPremium]["temp_d_01"] = dsTariffInsure.Tables[0].Rows[0]["temp_d_01"].ToString() == "" ? "0" : dsTariffInsure.Tables[0].Rows[0]["temp_d_01"].ToString();
                                                        ds.Tables[0].Rows[iPremium]["temp_p_01"] = dsTariffInsure.Tables[0].Rows[0]["temp_p_01"].ToString() == "" ? "0" : dsTariffInsure.Tables[0].Rows[0]["temp_p_01"].ToString();
                                                        ds.Tables[0].Rows[iPremium]["temp_p_num"] = dsTariffInsure.Tables[0].Rows[0]["temp_p_num"].ToString() == "" ? "0" : dsTariffInsure.Tables[0].Rows[0]["temp_p_num"].ToString();
                                                        dsTmp = cm.getDataCar01(ddlProduct.SelectedValue, txtCarCode.Text, strPolTypeValue, drDefaultSize[iCarSize]["defv_value"].ToString(), drDefaultBody[iCarBody]["defv_value"].ToString());
                                                        if (dsTmp.Tables[0].Rows.Count > 0)
                                                        {
                                                            drTmp = dsTmp.Tables[0].Select("add01_typ = 'D'", "");
                                                            if (drTmp.Length > 0)
                                                            {
                                                                ds.Tables[0].Rows[iPremium]["factor_permd01"] = drTmp[0]["rate1_use"].ToString();
                                                                ds.Tables[0].Rows[iPremium]["factor_tempd01"] = drTmp[0]["rate2_use"].ToString();
                                                            }
                                                            else
                                                            {
                                                                ds.Tables[0].Rows[iPremium]["factor_permd01"] = "0";
                                                                ds.Tables[0].Rows[iPremium]["factor_tempd01"] = "0";
                                                            }
                                                            drTmp = dsTmp.Tables[0].Select("add01_typ = 'M'", "");
                                                            if (drTmp.Length > 0)
                                                            {
                                                                ds.Tables[0].Rows[iPremium]["factor_permp01"] = drTmp[0]["rate1_use"].ToString();
                                                                ds.Tables[0].Rows[iPremium]["factor_tempp01"] = drTmp[0]["rate2_use"].ToString();
                                                            }
                                                            else
                                                            {
                                                                ds.Tables[0].Rows[iPremium]["factor_permp01"] = "0";
                                                                ds.Tables[0].Rows[iPremium]["factor_tempp01"] = "0";
                                                            }
                                                        }
                                                        else
                                                        {
                                                            ds.Tables[0].Rows[iPremium]["factor_permd01"] = "0";
                                                            ds.Tables[0].Rows[iPremium]["factor_tempd01"] = "0";
                                                            ds.Tables[0].Rows[iPremium]["factor_permp01"] = "0";
                                                            ds.Tables[0].Rows[iPremium]["factor_tempp01"] = "0";
                                                        }
                                                        dCar01 = ((Convert.ToDouble(ds.Tables[0].Rows[iPremium]["perm_d_01"]) * Convert.ToDouble(ds.Tables[0].Rows[iPremium]["factor_permd01"])) +
                                                                    (Convert.ToDouble(ds.Tables[0].Rows[iPremium]["perm_p_01"]) * Convert.ToDouble(ds.Tables[0].Rows[iPremium]["factor_permp01"]) * Convert.ToDouble(ds.Tables[0].Rows[iPremium]["perm_p_num"])) +
                                                                    (Convert.ToDouble(ds.Tables[0].Rows[iPremium]["temp_d_01"]) * Convert.ToDouble(ds.Tables[0].Rows[iPremium]["factor_tempd01"])) +
                                                                    (Convert.ToDouble(ds.Tables[0].Rows[iPremium]["temp_p_01"]) * Convert.ToDouble(ds.Tables[0].Rows[iPremium]["factor_tempp01"]) * Convert.ToDouble(ds.Tables[0].Rows[iPremium]["temp_p_num"])));
                                                        ds.Tables[0].Rows[iPremium]["car_01"] = dCar01.ToString();

                                                        // รย02
                                                        ds.Tables[0].Rows[iPremium]["cover_02"] = dsTariffInsure.Tables[0].Rows[0]["cover_02"].ToString();
                                                        //if (strSizeValue == "2000" && dsTariffInsure.Tables[0].Rows[0]["cover_02"].ToString() == "200000")
                                                        //{
                                                        //    dCar01 = 0;
                                                        //}
                                                        dsTmp = cm.getDataCar02(ddlProduct.SelectedValue, txtCarCode.Text, strPolTypeValue, drDefaultSize[iCarSize]["defv_value"].ToString(), drDefaultBody[iCarBody]["defv_value"].ToString(), dsTariffInsure.Tables[0].Rows[0]["cover_02"].ToString());
                                                        if (dsTmp.Tables[0].Rows.Count > 0)
                                                        {
                                                            ds.Tables[0].Rows[iPremium]["factor_car02"] = dsTmp.Tables[0].Rows[0]["rate_use"].ToString();
                                                        }
                                                        else
                                                            ds.Tables[0].Rows[iPremium]["factor_car02"] = "0";
                                                        dCar02 = (Convert.ToDouble(ds.Tables[0].Rows[iPremium]["perm_p_num"].ToString()) + 1) * Convert.ToDouble(ds.Tables[0].Rows[iPremium]["factor_car02"].ToString());
                                                        ds.Tables[0].Rows[iPremium]["car_02"] = dCar02.ToString();

                                                        // รย03
                                                        ds.Tables[0].Rows[iPremium]["cover_03"] = dsTariffInsure.Tables[0].Rows[0]["cover_03"].ToString();
                                                        dsTmp = cm.getDataCar03(ddlProduct.SelectedValue, txtCarCode.Text, strPolTypeValue, drDefaultSize[iCarSize]["defv_value"].ToString(), drDefaultBody[iCarBody]["defv_value"].ToString());
                                                        if (dsTmp.Tables[0].Rows.Count > 0)
                                                        {
                                                            ds.Tables[0].Rows[iPremium]["factor_car03"] = dsTmp.Tables[0].Rows[0]["rate_use"].ToString();
                                                        }
                                                        else
                                                            ds.Tables[0].Rows[iPremium]["factor_car03"] = "0";
                                                        dCar03 = (Convert.ToDouble(ds.Tables[0].Rows[iPremium]["cover_03"])) * Convert.ToDouble(ds.Tables[0].Rows[iPremium]["factor_car03"].ToString()) / 100;
                                                        ds.Tables[0].Rows[iPremium]["car_03"] = dCar03.ToString();

                                                        ds.Tables[0].Rows[iPremium]["od_dd"] = dsTariffInsure.Tables[0].Rows[0]["od_dd"].ToString();
                                                        dMainPremium = 0;
                                                        if (dsReduceInsure.Tables[0].Rows.Count > 0)
                                                        {
                                                            // ส่วนลดเบี้ยหลัก
                                                            if (Convert.ToDouble(dsReduceInsure.Tables[0].Rows[0]["pre_oth"]) > 0)
                                                            {
                                                                dMainPremium = Convert.ToDouble(ds.Tables[0].Rows[iPremium]["main_premium"].ToString());
                                                                dMainPremium = dMainPremium - Convert.ToDouble(dsReduceInsure.Tables[0].Rows[0]["pre_oth"]);
                                                                ds.Tables[0].Rows[iPremium]["main_premium"] = dMainPremium.ToString();
                                                            }
                                                        }
                                                        dMainPremium = 0;
                                                        dMainPremium = Convert.ToDouble(ds.Tables[0].Rows[iPremium]["main_premium"].ToString()) +
                                                            Convert.ToDouble(ds.Tables[0].Rows[iPremium]["car_01"].ToString() == "" ? "0" : ds.Tables[0].Rows[iPremium]["car_01"].ToString()) +
                                                            Convert.ToDouble(ds.Tables[0].Rows[iPremium]["car_02"].ToString() == "" ? "0" : ds.Tables[0].Rows[iPremium]["car_02"].ToString()) +
                                                            Convert.ToDouble(ds.Tables[0].Rows[iPremium]["car_03"].ToString() == "" ? "0" : ds.Tables[0].Rows[iPremium]["car_03"].ToString()) -
                                                            Convert.ToDouble(ds.Tables[0].Rows[iPremium]["od_dd"].ToString() == "" ? "0" : ds.Tables[0].Rows[iPremium]["od_dd"].ToString());
                                                        

                                                        if (dsReduceInsure.Tables[0].Rows.Count > 0)
                                                        {
                                                            // ประวัติเพิ่ม
                                                            if (Convert.ToDouble(dsReduceInsure.Tables[0].Rows[0]["pre_add_per"]) > 0)
                                                            {
                                                                dMainPremium = dMainPremium + (dMainPremium * Convert.ToDouble(dsReduceInsure.Tables[0].Rows[0]["pre_add_per"]) / 100);
                                                            }
                                                            // ส่วนลดหมู่
                                                            if (Convert.ToDouble(dsReduceInsure.Tables[0].Rows[0]["pre_fle_per"]) > 0)
                                                            {
                                                                dMainPremium = dMainPremium - (dMainPremium * Convert.ToDouble(dsReduceInsure.Tables[0].Rows[0]["pre_fle_per"]) / 100);
                                                            }
                                                            // ส่วนลดประวัติดี
                                                            if (Convert.ToDouble(dsReduceInsure.Tables[0].Rows[0]["pre_exp_per"]) > 0)
                                                            {
                                                                dMainPremium = dMainPremium - (dMainPremium * Convert.ToDouble(dsReduceInsure.Tables[0].Rows[0]["pre_exp_per"]) / 100);
                                                            }
                                                            // ส่วนลดอื่นๆ
                                                            if (Convert.ToDouble(dsReduceInsure.Tables[0].Rows[0]["pre_oth_per"]) > 0)
                                                            {
                                                                dMainPremium = dMainPremium - (dMainPremium * Convert.ToDouble(dsReduceInsure.Tables[0].Rows[0]["pre_oth_per"]) / 100);
                                                            }
                                                            else if (Convert.ToDouble(dsReduceInsure.Tables[0].Rows[0]["pre_oth_baht"]) > 0)
                                                            {
                                                                dMainPremium = dMainPremium - Convert.ToDouble(dsReduceInsure.Tables[0].Rows[0]["pre_oth_baht"]) ;
                                                            }
                                                        }
                                                        dMainPremium = Math.Round(dMainPremium);
                                                        ds.Tables[0].Rows[iPremium]["net_premium"] = dMainPremium.ToString();
                                                        // Stamp + Tax
                                                        dTax = 0;
                                                        dStamp = 0;
                                                        if (dsTaxRate.Tables[0].Rows.Count > 0)
                                                        {                                                            
                                                            dStamp = dMainPremium * Convert.ToDouble(dsTaxRate.Tables[0].Rows[0]["stm_rate"]) / 100;
                                                            dStamp = Math.Ceiling(dStamp);
                                                            dTax = (dMainPremium + dStamp) * Convert.ToDouble(dsTaxRate.Tables[0].Rows[0]["tax_rate"]) / 100;
                                                        }
                                                        ds.Tables[0].Rows[iPremium]["stamp"] = dStamp.ToString();
                                                        ds.Tables[0].Rows[iPremium]["tax"] = dTax.ToString();
                                                        double dCompul = 0;
                                                        double dPA = 0;
                                                        if (ddlProduct.SelectedValue == "013")
                                                        {
                                                            // หาเบี้ยพรบ.
                                                            dsTmp = cm.getCompulPremium(txtCarCode.Text, drDefaultBody[iCarBody]["defv_value"].ToString());
                                                            if (dsTmp.Tables[0].Rows.Count > 0)
                                                                dCompul = Convert.ToDouble(dsTmp.Tables[0].Rows[0]["pre_grs"].ToString());
                                                            // หาเบี้ย PA
                                                            dPA = Convert.ToDouble(dsReduceInsure.Tables[0].Rows[0]["pa_pre"].ToString());
                                                        }
                                                        else
                                                        {
                                                            dCompul = 0;
                                                            dPA = 0;
                                                        }
                                                        ds.Tables[0].Rows[iPremium]["gross_premium"] = Convert.ToString(dMainPremium + dStamp + dTax+dCompul + dPA);
                                                        cmPrem.addDataOnlinePremium(strCoverID, strHeadPremID, ddlProduct.SelectedValue, txtCarCode.Text,
                                                                            strPolTypeValue, dtMotGar.Rows[iMotGar]["mot_gar"].ToString(),
                                                                            drDefaultAge[iCarAge]["defv_value"].ToString(), drDefaultGroup[iCarGroup]["defv_value"].ToString(),
                                                                            dtCarMak.Rows[iCarMak]["car_mak"].ToString(), drDefaultSize[iCarSize]["defv_value"].ToString(),
                                                                            drDefaultBody[iCarBody]["defv_value"].ToString(), dtAgtCod.Rows[iAgtCod]["agt_cod"].ToString(),
                                                                            ds.Tables[0].Rows[iPremium]["car_driver"].ToString(), ds.Tables[0].Rows[iPremium]["sum_insure"].ToString(),
                                                                            ds.Tables[0].Rows[iPremium]["main_premium"].ToString(),Convert.ToString(dCar01 + dCar02 + dCar03),
                                                                            ds.Tables[0].Rows[iPremium]["net_premium"].ToString(),ds.Tables[0].Rows[iPremium]["stamp"].ToString(),
                                                                            ds.Tables[0].Rows[iPremium]["tax"].ToString(), ds.Tables[0].Rows[iPremium]["gross_premium"].ToString(),
                                                                            conn, trans);
                                                        if (strPolTypeValue == "3")
                                                        {
                                                            dSumIns = Convert.ToDouble(txtSumInsuredTo.Text) + 1;
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                litScript.Text += "";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            cmPrem.updateDataOnlineHeadPremium(strHeadPremID, ds.Tables[0].Rows.Count.ToString(),
                                            conn, trans);
            trans.Commit();
            litScript.Text += "<script>alert('Save complete!');";            
            litScript.Text += "</script>";
        }
        catch (Exception e2)
        {
            trans.Rollback();
            TDS.Utility.MasterUtil.writeError("GeneratePremiumForm.btnSave_Click-e2", e2.ToString());
            litScript.Text += "<script>alert('Error!');</script>";
        }
        finally
        {
            trans.Dispose();
            conn.Close();
            conn.Dispose();
        }
        //dtgData.DataSource = ds;
        //dtgData.DataBind();
    }
    [System.Web.Services.WebMethod]
    public static ArrayList GetPromotionData(string strCampaign)
    {
        DataSet ds = new DataSet();
        ArrayList list = new ArrayList();
        VoluntaryManager cm = new VoluntaryManager();
        //TDS.Utility.MasterUtil.writeLog("ddd", strCampaign);
        ds = cm.getAllDataPromotion(strCampaign);
        ds.Tables[0].Rows.InsertAt(ds.Tables[0].NewRow(), 0);
        ds.Tables[0].Rows[0]["campaign"] = "";
        ds.Tables[0].Rows[0]["name"] = " - เลือก - ";
        ds.Tables[0].Rows[0]["pattern"] = "";
        for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
        {
            list.Add(new ListItem(
           ds.Tables[0].Rows[i]["name"].ToString(),
           ds.Tables[0].Rows[i]["campaign"].ToString()
            ));
        }
        return list;
    }

    public void initialDropdownList()
    {
        DataSet ds;
        VoluntaryManager cm = new VoluntaryManager();
        PremiumManager cmPrem = new PremiumManager();
        ds = cm.getAllDataProduct();
        ddlProduct.DataSource = ds;
        ddlProduct.DataTextField = "name";
        ddlProduct.DataValueField = "campaign";
        ddlProduct.DataBind();
        ddlProduct.Items.Insert(0, new ListItem(" - เลือก - ", ""));

        ds = cm.getAllDataPromotion(ddlProduct.SelectedValue);
        ddlPromotion.DataSource = ds;
        ddlPromotion.DataTextField = "name";
        ddlPromotion.DataValueField = "campaign";
        ddlPromotion.DataBind();
        ddlPromotion.Items.Insert(0, new ListItem(" - เลือก - ", ""));

        ds = cmPrem.getPeriodPremium("GEN_PREMIUM");
        ddlPeriod.DataSource = ds;
        ddlPeriod.DataTextField = "peri_text";
        ddlPeriod.DataValueField = "peri_value";
        ddlPeriod.DataBind();
        ddlPeriod.Items.Insert(0, new ListItem(" - เลือก - ", ""));
    }    
}
