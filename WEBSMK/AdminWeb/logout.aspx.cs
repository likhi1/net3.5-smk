﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminWeb_logout : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
            
        //***** Destroy  Session *****
         Session["SessAdmin"] =   null  ; 
         Response.Redirect("main.aspx"); 
    }
}
