﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class AdminWeb_ApproveForm : System.Web.UI.Page
{
    public string[] ColumnName = { "flag_H", "flag_A", "flag_C", "", "tm_regis_no", 
                                     "rg_ins_fullname", "rg_ins_tel", "rg_regis_dt", "rg_prmmgross", "rg_pay_type"};
    public string[] ColumnType = { "string", "string", "string", "string", "string", 
                                     "string", "string", "string", "string", "string"};
    public string[] ColumnTitle = { "H", "A", "C", "เล่มที่ / เลขที่รับแจ้ง", "เลขที่อ้างอิง", 
                                    "ชื่อ-นามสกุล", "เบอร์โทรศัพท์", "วันที่บันทึกข้อมูล", "เบี้ยประกัน", "วิธีการชำระเงิน",
                                    "ดูรายละเอียด"};
    string[] ColumnWidth = { "5%", "5%", "5%", "15%", "10%",
                               "10%", "10%", "10%", "10%", "10%",
                                "10%" };
    string[] ColumnAlign = { "center", "center", "center", "center", "center",
                                "left", "left", "center", "right", "left",
                                "center"};

    public string tranPicUrl = "";
    DataView dv = new DataView();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ddlPolType.DataSource = FormatStringApp.getAllPolType();
            ddlPolType.DataTextField = "Text_th";
            ddlPolType.DataValueField = "Code";
            ddlPolType.DataBind();
            ddlPolType.Items.Insert(0, "-- ระบุความคุ้มครอง --");
            ddlPolType.Items[0].Value = "";
            Button1_Click(Button1, e);
        } 
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        DataSet dsData;
        RequestPolicyManager cm = new RequestPolicyManager();
        dsData = cm.getDataForApprove(ddlPolType.SelectedValue);
        ViewState["DSMasterTable"] = dsData;
        ViewState["poltype"] = ddlPolType.SelectedValue;
        ShowData();
    }
    protected void PageIndexChanged(Object source, DataGridPageChangedEventArgs e)
    {
        DataView dv = new DataView();
        DataSet ds = new DataSet();
        if (!(ViewState["DSMasterTable"] == null))
        {
            ds = (DataSet)ViewState["DSMasterTable"];
            if (e.NewPageIndex > dtgData.PageCount - 1)
                dtgData.CurrentPageIndex = dtgData.PageCount - 1;
            else
                dtgData.CurrentPageIndex = e.NewPageIndex;

            String sortBy = "";
            if (!(ViewState["DSMasterTable"] == null))
            {
                sortBy = (string)(ViewState["SortString"]);
            }
            dv.Table = ds.Tables[0];
            dv.Sort = sortBy;
            ShowData();
        }
    }

    private void ShowData()
    {
        DataSet ds = new DataSet();
        if (ViewState["DSMasterTable"] != null)
            ds = (DataSet)ViewState["DSMasterTable"];

        string sortBy = "";
        if (ViewState["SortString"] != null)
            sortBy = (String)ViewState["SortString"];

        if (ds.Tables.Count > 0)
        {
            dv.Table = ds.Tables[0];
            dv.Sort = sortBy;

            int newPageCount = (Int32)(Math.Ceiling((double)dv.Table.Rows.Count / (double)dtgData.PageSize));
            if (dtgData.CurrentPageIndex >= newPageCount && newPageCount > 0)
                dtgData.CurrentPageIndex = newPageCount - 1;
        }
        dtgData.DataSource = dv;
        dtgData.DataBind();

        GridViewUtil.ItemDataBound(dtgData.Items);

        ucPageNavigator1.dvData = ds.Tables[0].DefaultView;
        ucPageNavigator1.dtgData = dtgData;
    }
    private bool validateForm(DataSet dsData)
    {
        bool isRet = true;
        string errMsg = "";
        for (int i = 0; i < dsData.Tables[0].Rows.Count; i++)
        {
            if (dsData.Tables[0].Rows[i]["approve_result"].ToString() == "A" &&
                dsData.Tables[0].Rows[i]["book_no"].ToString() == "")
            {
                isRet = false;
                errMsg += "    - เล่มที่รับแจ้ง ของรายการเลขที่อ้างอิง=" + dsData.Tables[0].Rows[i]["tmp_cd"].ToString() + "\\n";
            }
            if (dsData.Tables[0].Rows[i]["approve_result"].ToString() == "A" &&
                dsData.Tables[0].Rows[i]["inform_no"].ToString() == "")
            {
                isRet = false;
                errMsg += "    - เลขที่รับแจ้ง ของรายการเลขที่อ้างอิง=" + dsData.Tables[0].Rows[i]["tmp_cd"].ToString() + "\\n";
            }
        }
        if (isRet == false)
        {
            //UcError1.showMessage(ViewState["err000001"].ToString() + "\\n" + errMsg);
        }
        return isRet;
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        RequestPolicyManager cm = new RequestPolicyManager();
        DataSet dsData;
        DataRow drData;
        string[] columnNames = { "approve_result", "regis_no", "book_no", "inform_no", "tmp_cd" };
        string[] columnTypes = { "string", "string", "string", "string", "string" };
        RadioButton rdoHold;
        RadioButton rdoApprove;
        RadioButton rdoReject;
        TextBox txtBook;
        TextBox txtNo;

        dsData = new DataSet();
        dsData.Tables.Add();
        TDS.Utility.MasterUtil.AddColumnToDataTable(dsData.Tables[0], columnTypes, columnNames);
        for (int i = 0; i <= dtgData.Items.Count - 1; i++)
        {
            drData = dsData.Tables[0].NewRow();
            drData["regis_no"] = dtgData.Items[i].Cells[0].Text;
            rdoHold = (RadioButton)dtgData.Items[i].Cells[1].FindControl("rdoHold");
            rdoApprove = (RadioButton)dtgData.Items[i].Cells[2].FindControl("rdoApprove");
            rdoReject = (RadioButton)dtgData.Items[i].Cells[3].FindControl("rdoReject");
            if (rdoHold != null && rdoApprove != null && rdoReject != null)
            {
                if (rdoHold.Checked)
                    drData["approve_result"] = "H";
                else if (rdoApprove.Checked)
                    drData["approve_result"] = "A";
                else
                    drData["approve_result"] = "C";
            }
            txtBook = (TextBox)dtgData.Items[i].Cells[4].FindControl("txtBookNo");
            txtNo = (TextBox)dtgData.Items[i].Cells[4].FindControl("txtNo");
            if (txtBook != null)
            {
                drData["book_no"] = txtBook.Text;
            }
            else
                drData["book_no"] = "";
            if (txtNo != null)
                drData["inform_no"] = txtNo.Text;
            else
                drData["inform_no"] = "";
            drData["regis_no"] = ((DataBoundLiteralControl)dtgData.Items[i].Cells[4].Controls[0]).Text.Trim();
            dsData.Tables[0].Rows.Add(drData);
        }
        if (validateForm(dsData))
        {
            if (cm.updateApprove(dsData))
            {
                litScript.Text = "<script>alert(\"บันทึกข้อมูลเรียบร้อย\")</script>'"; 
            }
            else
            {
                litScript.Text = "<script>alert(\"ไม่สามารถบันทึกข้อมูลได้\")</script>'";
            }            
            if (ViewState["poltype"] != null)
            {
                dsData = cm.getDataForApprove(ViewState["poltype"].ToString());
                ViewState["DSMasterTable"] = dsData;
                ShowData();
            }
        }
    }
}
