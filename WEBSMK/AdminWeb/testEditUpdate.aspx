﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="testEditUpdate.aspx.cs" Inherits="adminWeb_testEditUpdate" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            DataKeyNames="newsConId" DataSourceID="SqlDataSource1" Width="110px">
            <Columns>
                <asp:BoundField DataField="newsConId" HeaderText="newsConId" 
                    InsertVisible="False" ReadOnly="True" SortExpression="newsConId" />
                <asp:BoundField DataField="newsCatIdRef" HeaderText="newsCatIdRef" 
                    SortExpression="newsCatIdRef" />
                <asp:BoundField DataField="newsConTopic" HeaderText="newsConTopic" 
                    SortExpression="newsConTopic" />
             
                <asp:CommandField ShowEditButton="True" />
             
            </Columns>
        </asp:GridView>
        
        
        
        
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:smkConnectionString %>" 
            SelectCommand="SELECT * FROM [tb_newsCon]" UpdateCommand="UPDATE  tb_newsCon      SET 
 newsCatIdRef =@newsCatIdRef, 
 newsConTopic=@newsConTopic 
WHERE newsConId = @newsConId"  
            >
            <UpdateParameters>
                <asp:Parameter Name="newsCatIdRef" />
                <asp:Parameter />
                <asp:Parameter Name="newsConId" />
            </UpdateParameters>
        </asp:SqlDataSource>
    
    </div>
    </form>
</body>
</html>
