﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---------- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;
using System.Collections;
 
public partial class adminWeb_faqList : System.Web.UI.Page {


    //=======================  Global Varible 
    protected string CatId {  get { return Request.QueryString["cat"];  }    }
     

    protected void Page_Load(object sender, EventArgs e) {

        //======== Chk  Query String 
       

        btnAddContent.Attributes.Add("OnClick", "location='faqAdd.aspx' ;return false; ");

    

        if (!Page.IsPostBack) {
           
            //ddlSearchCat.SelectedValue = CatId;

            BindData();
             
            
        } else {
           // ddlSearchCat.SelectedValue = ddlSearchCat.SelectedValue;
           

        }
    }


    //=======================  Set Up  
    protected void BindData() {

        DataSet ds = SelectData();
        DataTable dt1 = ds.Tables[0];

        /// Check num roll befor Command Datasource 
        if (dt1.Rows.Count > 0) { 
            GridView1.AllowPaging = true;
            GridView1.PageSize = 20; 
            GridView1.DataSource = dt1;
            GridView1.DataBind();
             


        }

    }
     
    protected DataSet SelectData() {

        string sql1; 
            sql1 = "SELECT * FROM tbfaq " 
             + "ORDER BY FaqId   DESC   "; 

        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql1, "myTb");
        return ds;
    }
     

    //======================  Pager
    protected void GridView1_IndexChanging(object sender, GridViewPageEventArgs e) {
        GridView1.PageIndex = e.NewPageIndex;
        BindData();
    }


    //======================  Command Gridview 
    protected void GridView1_RowDataBound(object s, GridViewRowEventArgs e) {

        if (e.Row.RowType == DataControlRowType.DataRow) {
            //========  (Hidde For  Check   ddlCatName  Select  ) ===================================     
            Label lblHdStatus = (Label)(e.Row.FindControl("lblHdStatus"));
            if (lblHdStatus != null) {
                lblHdStatus.Text = DataBinder.Eval(e.Row.DataItem, "FaqStatus").ToString();
            }

         
            Label lblHdSort = (Label)(e.Row.FindControl("lblHdSort"));
            if (lblHdSort != null) { 
                int intHdSort = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "FaqSort"));
                if (intHdSort != 9999 && intHdSort != 0) {
                    lblHdSort.Text = intHdSort.ToString();
                } else {
                    lblHdSort.Text = "";
                }
            }



            //======== EditItemTemplate =================================================   
            TextBox tbxSort = (TextBox)(e.Row.FindControl("tbxSort"));
            if (tbxSort != null) {
                string strValue = (e.Row.FindControl("lblHdSort") as Label).Text;
                tbxSort.Text = strValue;
                tbxSort.Style.Add("width", "40px");
            }

           

            DropDownList ddlStatus = (DropDownList)(e.Row.FindControl("ddlStatus"));
            if (ddlStatus != null) {
                ddlStatus.Items.Insert(0, new ListItem("ปิด", "0")); // add Empty Item 
                ddlStatus.Items.Insert(1, new ListItem("เปิด", "1")); // add Empty Item   
                ///// set select  in EditTemplate
                string x = (e.Row.FindControl("lblHdStatus") as Label).Text;
                ddlStatus.Items.FindByValue(x).Selected = true;
            }



            //======== ItemTemplate =================================================   
            Label lblId = (Label)(e.Row.FindControl("lblId"));
            if (lblId != null) {
                string strId = DataBinder.Eval(e.Row.DataItem, "FaqId").ToString();
                int intId = Convert.ToInt32(strId);
                string ConId = String.Format("{0:00000}", intId);
                lblId.Text = ConId;

                lblId.Style.Add("text-align", "right");
                lblId.Style.Add("padding-right", "10px");
                ///// Add Style 
                lblId.Style.Add("width", "2%");
            }

            HyperLink hplTopic = (HyperLink)(e.Row.FindControl("hplTopic"));
            if (hplTopic != null) {
                string _id = DataBinder.Eval(e.Row.DataItem, "FaqId").ToString();
                hplTopic.Text = DataBinder.Eval(e.Row.DataItem, "FaqQuiz").ToString();

                hplTopic.NavigateUrl = "faqView.aspx?id=" + _id;
                hplTopic.Style.Add("float", "left");
            }

          

            Image imgStatus = (Image)(e.Row.FindControl("imgStatus"));
            if (imgStatus != null) {
                int strStatus = (int)DataBinder.Eval(e.Row.DataItem, "FaqStatus");
                //lblConStatus.Text =  ( (strStatus == 1) ? '' : "ไม่แสดง"  ) ;
                string picUrl = strStatus == 1 ? "images/check-right.png" : "images/check-wrong.png";
                imgStatus.Attributes.Add("src", picUrl);
            }

            Label lblSort = (Label)(e.Row.FindControl("lblSort"));
            if (lblSort != null) {
                Decimal decSort = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "FaqSort"));
                if (decSort != 9999) {
                    lblSort.Text = decSort.ToString();
                } else {
                    lblSort.Text = "";
                } 
            }
             

        }//end if

    }

    protected void GridView1_Deleting(object s, GridViewDeleteEventArgs e) {
        int a = e.RowIndex;
        string sql2 = "DELETE  FROM  tbFaq WHERE FaqId = '" + GridView1.DataKeys[e.RowIndex].Value + "'";
        int i = new DBClass().SqlExecute(sql2);
        GridView1.EditIndex = -1;
        BindData();
    }

    protected void btnSearch_Click(object sender, EventArgs e) {
      //  Response.Redirect("newsSearch.aspx?cat=" + ddlSearchCat.Text + "&key=" + tbxKeySearch.Text);
    }

    protected void modEditCommand(object sender, GridViewEditEventArgs e) {
        GridView1.EditIndex = e.NewEditIndex;
        BindData();
    }

    protected void modCancelCommand(object sender, GridViewCancelEditEventArgs e) {
        GridView1.EditIndex = -1;
        BindData();
    }

    protected void modUpdateCommand(object sender, GridViewUpdateEventArgs e) { 
        DropDownList ddlStatus = (DropDownList)GridView1.Rows[e.RowIndex].FindControl("ddlStatus");
        TextBox tbxSort = (TextBox)GridView1.Rows[e.RowIndex].FindControl("tbxSort");
        Decimal decSort;

        string strSQL = "UPDATE tbFaq SET " +
                  "FaqStatus = '" + ddlStatus.Text + "', " +
                  "FaqSort = '" + (decSort = (tbxSort.Text != "") ? Convert.ToDecimal(tbxSort.Text) : 0) + "' " +
                  " WHERE  FaqId = '" + GridView1.DataKeys[e.RowIndex].Value + "'";

        DBClass obj = new DBClass();
        int i = obj.SqlExecute(strSQL);

        GridView1.EditIndex = -1;
        BindData();
    }




}// end class
