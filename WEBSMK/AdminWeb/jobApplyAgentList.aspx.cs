﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;
using System.Collections;

public partial class adminWeb_jobApplyAgentList : System.Web.UI.Page 
{
    public string Cat { get { return Request.QueryString["cat"]; } } 

    public DataTable dtPosition { get; set; } // Decare Global Varibal  For Use in Posi
    protected DataTable Posi { get { DataTable dt = dtPosition; return dt; } }

    protected void Page_Load(object sender, EventArgs e)
    { 
        if (!Page.IsPostBack) {
            SelectPosition();
            BindData();
        } else {

        }
 
    }
     
    protected void BindData(){
        GridView1.DataSource = SelectData();
        GridView1.DataBind();

    }
     
    protected DataSet SelectData() { 

       string sql = "SELECT * FROM  tbJobPrf  WHERE  JobPrfCat ='"+Cat+"'  ";


        DBClass obj = new DBClass() ;
        DataSet ds = obj.SqlGet(sql ,"tb");

        return ds;
    }

    //================== Position Manage
    protected DataTable SelectPosition() {
        string sql = "Select JobPosiId ,JobPosiName  FROM tbJobPosi WHERE JobPrfCat= '" + Cat + "'    ORDER BY  JobPosiId DESC  ";

        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql, "myTb");
        dtPosition = ds.Tables[0];
        return dtPosition;
    }
  
    string  posiName ;  
    protected string SetPositionName(string index) {
        foreach (DataRow r in Posi.Rows) {
            if (r[0].ToString() == index) {
                posiName = r[1].ToString();
            } else {
                posiName = "";
            }
        }
        return posiName;
    }



     
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e) {
        
        Label lblId = (Label)e.Row.FindControl("lblId");
        if (lblId != null) {
            int CntId = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "JobPrfId"));
            string strId = String.Format("{0:0000}", CntId);
            lblId.Text = strId;
        }

        HyperLink hplName = (HyperLink)(e.Row.FindControl("hplName"));
        if (hplName != null) {
            string strJobPrfSex = DataBinder.Eval(e.Row.DataItem, "JobPrfSex").ToString();
            string strJobPrfName1  = DataBinder.Eval(e.Row.DataItem, "JobPrfName1").ToString();
            string strJobPrfName2 = DataBinder.Eval(e.Row.DataItem, "JobPrfName2").ToString();
            string strId = DataBinder.Eval(e.Row.DataItem, "JobPrfId").ToString();

            hplName.Text = strJobPrfSex+" "+strJobPrfName1 + "  " + strJobPrfName2;
            hplName.NavigateUrl = "jobApplyAgentView.aspx?cat=" + Cat + "&id=" + strId;  
        }

        Label lblAge = (Label)(e.Row.FindControl("lblAge"));
        if (lblAge != null) {
            lblAge.Text = DataBinder.Eval(e.Row.DataItem, "JobPrfAge").ToString();
        }
         
          
        Label lblPosition = (Label)(e.Row.FindControl("lblPosition"));
        if (lblPosition != null) {
           string  strPosition  = DataBinder.Eval(e.Row.DataItem, "JobPrfPosition").ToString(); 
           lblPosition.Text = SetPositionName(strPosition);

        }


        Label lblDatReg = (Label)(e.Row.FindControl("lblDatReg"));
        if (lblDatReg != null) {
            DateTime dtDateSet = (DateTime)DataBinder.Eval(e.Row.DataItem, "JobPrfDateReg");
            string strDateSet = dtDateSet.ToString("ddMMMyy");
            string strTimeSet = dtDateSet.ToString("H:m");

            lblDatReg.Text = strDateSet+"<br>"+strTimeSet+" น.";
        }


      

        Label lblTel = (Label)(e.Row.FindControl("lblTel"));
        if (lblTel != null) {
            lblTel.Text = DataBinder.Eval(e.Row.DataItem, "JobPrfTel1").ToString();
        }
        

    } 
    
    protected void btnSearch_Click(object sender, EventArgs e) {
     //   Response.Redirect("newsSearch.aspx?cat=" + ddlSearchCat.Text + "&key=" + tbxKeySearch.Text);
    } 

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e) {

    }


}