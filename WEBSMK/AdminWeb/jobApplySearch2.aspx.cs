﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;
using System.Collections;

public partial class adminWeb_jobApplySearch : System.Web.UI.Page
{

    public string CatId { get { return Request.QueryString["cat"]; } }
    public DataTable _dtPosi;  // Decare Global Varibal  For Use in Posi
    protected DataTable DtPosi { get { return _dtPosi; } }
    string posiName;

    public string SearchPosi { get {    return Session["SearchPosi"].ToString();  }  }
    public string SearchKey { get { return  Session["SearchKey"].ToString();   }  }



    protected void Page_Load(object sender, EventArgs e) {
        lblPageName.Text = (Convert.ToInt32(CatId) == 1 ? "พนักงาน" : "ตัวแทน");

        if (!Page.IsPostBack) { 
            BindData();
           
        }
    }

    protected void BindData() {
       
        SelectPosition();

        DataTable dt = SearchData(CatId , SearchPosi, SearchKey).Tables[0];

        //======  Check Num Befor DataBind
        if (dt.Rows.Count > 0) {
            GridView1.AllowPaging = true;
            GridView1.PageSize = 20;

            GridView1.DataSource = dt;
            GridView1.DataBind();
        }

        PositionBind();

    }


    protected DataSet SelectData() {
        string sql = "SELECT * FROM  tbJobPrf  WHERE  JobCatId ='" + CatId + "' ORDER BY  JobPrfId  DESC ";
        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql, "tb");
        return ds;
    }


    protected DataSet SearchData(  string catId   , string  searchPosi, string searchKey ) {
        string sql = "SELECT *  FROM tbJobPrf  "

        + "WHERE  JobPrfCat = '" + catId + "'  ";

        if (searchPosi != "") {
            sql += " AND  ( JobPrfPosition1 = '" + searchPosi + "'  OR  JobPrfPosition2 = '" + searchPosi + "'   OR JobPrfPosition3 = '" + searchPosi + "'  )  ";

        }

        if (searchKey != "") {
            sql += "AND ( "
                + " JobPrfName1  LIKE  '%" + searchKey + "%'  "
                + " OR  JobPrfAddress1  LIKE  '%" + searchKey + "%'  " 
                + " OR  JobPrfAddress2  LIKE  '%" + searchKey + "%'  "
                + " OR  JobPrfEdu1  LIKE  '%" + searchKey + "%'  "
                + " OR  JobPrfEdu2  LIKE  '%" + searchKey + "%'  "
                + " OR  JobPrfEdu3  LIKE  '%" + searchKey + "%'  "
                + " OR  JobPrfSkCom  LIKE  '%" + searchKey + "%'  "
                + " )  ";
        }

        sql += "ORDER BY JobPrfId DESC ";


        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql, "tb");
        return ds;

    }

 
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e) {

        //========  (Hidde For  Check   ddlCatName  Select  ) ===================================     

        Label lblStatusHd = (Label)e.Row.FindControl("lblHdStatus");
        if (lblStatusHd != null) {
            lblStatusHd.Text = DataBinder.Eval(e.Row.DataItem, "JobPrfStatus").ToString();
        }

        //======== EditItemTemplate ================================================= 
        DropDownList ddlStatus = (DropDownList)e.Row.FindControl("ddlStatus");
        if (ddlStatus != null) {
            ddlStatus.Items.Insert(0, new ListItem("ยังไม่ติดต่อ", "0")); // add Empty Item 
            ddlStatus.Items.Insert(1, new ListItem("ติดต่อแล้ว", "1")); // add Empty Item   

            ///// set select  in EditTemplate
            string x = (e.Row.FindControl("lblHdStatus") as Label).Text;
            ddlStatus.Items.FindByValue(x).Selected = true;
        }

        //========  ItemTemplate ================================================= 

        Label lblId = (Label)e.Row.FindControl("lblId");
        if (lblId != null) {
            int CntId = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "JobPrfId"));
            string strId = String.Format("{0:0000}", CntId);
            lblId.Text = strId;
        }

        HyperLink hplName = (HyperLink)(e.Row.FindControl("hplName"));
        if (hplName != null) {
            string strJobPrfSex = DataBinder.Eval(e.Row.DataItem, "JobPrfSex").ToString();
            string strJobPrfName1 = DataBinder.Eval(e.Row.DataItem, "JobPrfName1").ToString();
            string strJobPrfName2 = DataBinder.Eval(e.Row.DataItem, "JobPrfName2").ToString();
            string strId = DataBinder.Eval(e.Row.DataItem, "JobPrfId").ToString();

            hplName.Text = strJobPrfSex + " " + strJobPrfName1 + "  " + strJobPrfName2;
            if (CatId == "1") {
                hplName.NavigateUrl = "jobApplyView.aspx?cat=" + CatId + "&id=" + strId;
            } else {
                hplName.NavigateUrl = "jobApplyAgentView.aspx?cat=" + CatId + "&id=" + strId;
            }
        }

        Label lblAge = (Label)(e.Row.FindControl("lblAge"));
        if (lblAge != null) {
            lblAge.Text = DataBinder.Eval(e.Row.DataItem, "JobPrfAge").ToString();
        }

        /*
        Label lblEdu = (Label)(e.Row.FindControl("lblEdu"));
        if (lblEdu != null) {
            string strEdu1 = DataBinder.Eval(e.Row.DataItem, "JobPrfEdu1").ToString();
            string[] arrEdu1 = strEdu1.Split('|');

            string strEdu2 = DataBinder.Eval(e.Row.DataItem, "JobPrfEdu2").ToString();
            string[] arrEdu2 = strEdu2.Split('|');

            string strEdu3 = DataBinder.Eval(e.Row.DataItem, "JobPrfEdu3").ToString();
            string[] arrEdu3 = strEdu3.Split('|');

            lblEdu.Text = arrEdu1[1] + "<br>" + arrEdu2[1] + "<br>" + arrEdu3[1];
        }
        */

        /*
        Label lblPosition = (Label)(e.Row.FindControl("lblPosition"));
        if (lblPosition != null) {

            string strPosition = DataBinder.Eval(e.Row.DataItem, "JobPrfPosition").ToString();
            string[] arrPosition = strPosition.Split('|');

            if (arrPosition.Length > 1) {
                lblPosition.Text = "";
                if (arrPosition[0] != "") { lblPosition.Text += "<p>1. " + SetPositionName(arrPosition[0]) + "</p>"; }
                if (arrPosition[1] != "") { lblPosition.Text += "<p>2. " + SetPositionName(arrPosition[1]) + "</p>"; }
                if (arrPosition[2] != "") { lblPosition.Text += "<p>3. " + SetPositionName(arrPosition[2]) + "</p>"; }
            } else {
                lblPosition.Text = SetPositionName(arrPosition[0]);
            }
        } 
         */ 

        Label lblPosition = (Label)(e.Row.FindControl("lblPosition"));
        if (lblPosition != null) {

            string strPosition1 = DataBinder.Eval(e.Row.DataItem, "JobPrfPosition1").ToString();
            string strPosition2 = DataBinder.Eval(e.Row.DataItem, "JobPrfPosition2").ToString();
            string strPosition3 = DataBinder.Eval(e.Row.DataItem, "JobPrfPosition3").ToString();

            lblPosition.Text = !string.IsNullOrEmpty(strPosition1) ? "<p>1. " + SetPositionName(strPosition1) + "</p>" : "";
            lblPosition.Text += !string.IsNullOrEmpty(strPosition2) ? "<p>2. " + SetPositionName(strPosition2) + "</p>" : "";
            lblPosition.Text += !string.IsNullOrEmpty(strPosition3) ? "<p>3. " + SetPositionName(strPosition3) + "</p>" : "";


        }


        Label lblDatReg = (Label)(e.Row.FindControl("lblDatReg"));
        if (lblDatReg != null) {
            DateTime dtDateSet = (DateTime)DataBinder.Eval(e.Row.DataItem, "JobPrfDateReg");
            string strDateSet = dtDateSet.ToString("ddMMMyy");
            string strTimeSet = dtDateSet.ToString("H:m");

            lblDatReg.Text = strDateSet + "<br>" + strTimeSet + " น.";
        }


        //Label lblDateTimeSet = (Label)e.Row.FindControl("lblDateTimeSet");
        //if (lblDateTimeSet != null) {
        //    DateTime dtDateSet = (DateTime)DataBinder.Eval(e.Row.DataItem, "CntDateSet");
        //    string strDateSet = dtDateSet.ToString("dMMMyy");

        //    string CntTimeSet = DataBinder.Eval(e.Row.DataItem, "CntTimeSet").ToString();
        //    lblDateTimeSet.Text = (strDateSet + "<br>" + CntTimeSet + " น.");
        //}


        Label lblTel = (Label)(e.Row.FindControl("lblTel"));
        if (lblTel != null) {
            lblTel.Text = DataBinder.Eval(e.Row.DataItem, "JobPrfTel1").ToString();
        }


        Literal ltlStatus = (Literal)(e.Row.FindControl("ltlStatus"));
        if (ltlStatus != null) {
            int intStatus = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "JobPrfStatus"));
            if (intStatus == 0)
                ltlStatus.Text = "<img src='images/check-wrong.png' > ";
            else
                ltlStatus.Text = "<img src='images/check-right.png' > ";

        }

    }

    protected void btnSearch_Click(object sender, EventArgs e) {
        Session["SearchPosi"] = ddlPosition.SelectedValue;
        Session["SearchKey"] =  tbxSearch.Text;
    
        Response.Redirect("jobApplySearch.aspx?cat=" + CatId  );
    }


    //================== Position Manage
    protected DataTable SelectPosition() {
        string sql = "Select JobPosiId ,JobPosiName  FROM tbJobPosi WHERE JobPrfCat= '" + CatId + "'    ORDER BY  JobPosiId DESC  ";
        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql, "myTb");
        _dtPosi = ds.Tables[0];
        return _dtPosi;
    }

    protected void PositionBind() {
        ddlPosition.DataSource = DtPosi;
        ddlPosition.AppendDataBoundItems = true;
        ddlPosition.Items.Insert(0, new ListItem("-- กรุณาระบุ --", ""));

        ddlPosition.DataTextField = "JobPosiName";
        ddlPosition.DataValueField = "JobPosiId";
        ddlPosition.DataBind();
    }

    protected string SetPositionName(string indexPosition) {

        foreach (DataRow r in DtPosi.Rows) {
            if (r[0].ToString() == indexPosition)
                posiName = r[1].ToString();  //new Utility().SetPositionName(); 
        }
        return posiName;
    }



    //======================  Pager
    protected void GridView1_IndexChanging(object sender, GridViewPageEventArgs e) {
        GridView1.PageIndex = e.NewPageIndex;
        BindData();
    }


    //======================  Command Gridview  

    protected void GridView1_Deleting(object s, GridViewDeleteEventArgs e) {
        int a = e.RowIndex;
        string sql2 = "DELETE  FROM  tbJobPrf WHERE JobPrfId = '" + GridView1.DataKeys[e.RowIndex].Value + "'";
        int i = new DBClass().SqlExecute(sql2);
        GridView1.EditIndex = -1;
        BindData();
    }

    protected void modEditCommand(object sender, GridViewEditEventArgs e) {
        SelectPosition();
        GridView1.EditIndex = e.NewEditIndex;
        BindData();
    }

    protected void modCancelCommand(object sender, GridViewCancelEditEventArgs e) {
        GridView1.EditIndex = -1;
        BindData();
    }

    protected void modUpdateCommand(object sender, GridViewUpdateEventArgs e) {

        DropDownList ddlStatus = (DropDownList)GridView1.Rows[e.RowIndex].FindControl("ddlStatus");

        string strSQL = "UPDATE tbJobPrf  SET " +
        "JobPrfStatus= '" + ddlStatus.SelectedValue + "'  " +
        " WHERE  JobPrfId = '" + GridView1.DataKeys[e.RowIndex].Value + "'";

        DBClass obj = new DBClass();
        int i = obj.SqlExecute(strSQL);

        GridView1.EditIndex = -1;
        BindData();
    }
 

}