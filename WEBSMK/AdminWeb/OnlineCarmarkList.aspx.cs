﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data;

public partial class Module_Authenticate_OnlineCarmarkList : System.Web.UI.Page
{
    public static string strAppName = System.Configuration.ConfigurationManager.AppSettings["AppName"].ToString();

	public string[] ColumnName = { "NO", "carmakcod", "carnamcod", "group_code", "desc_t", 
									"desc_e", "car_type", "od_min", "od_max", "car_cc" };
    public string[] ColumnType = { "string", "string", "string", "string", "string", 
									"string", "string", "string", "string", "string" };
    public string[] ColumnTitle = { "ลำดับที่", "รหัสรุ่นรถ", "ยี่ห้อรถ", "กลุ่มรถ", "ชื่อไทย", 
									"ชื่ออังกฤษ", "ประเภทรถ", "ทุนต่ำ", "ทุนสูง", "ขนาดรถ", "" };
    string[] ColumnWidth = { "5%", "10%", "8%", "8%", "13%", 
									"13%", "8%", "10%", "10%", "10%", "5%" };
    string[] ColumnAlign = { "center", "left", "left", "left", "left", 
									"left", "right", "right", "right", "right", "center" };
									    
    public string tranPicUrl = "";
    DataView dv = new DataView();
    protected void Page_Load(object sender, EventArgs e)
    {
        //UserData condition;
        if (!IsPostBack)
        {
            initialDropdownList();
            initialDataGrid();

            //if (Request.QueryString["back"] != null)
            //{
                //if (Session["SEARCHCONDITION"] != null)
                //{
                    //try
                    //{
                    //    condition = (UserData)Session["SEARCHCONDITION"];
                    //    txtLoginName.Text = condition.LOGINNAME;
                    //    txtEmpCode.Text = condition.EMPCODE;
                    //    txtFName.Text = condition.USERFNAME;
                    //    txtLName.Text = condition.USERLNAME;
                    //    ddlRole.SelectedValue = condition.ROLEID;
                    //    ucTxtDate1.Text = condition.STARTDATE;
                    //    ucTxtDate2.Text = condition.ENDDATE;

                    //    btnSearch_Click(null, null);
                    //}
                    //catch (Exception exp)
                    //{

                    //}
            //    }
            //}
        }
        else
        {
			litJavaScript.Text = "";
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        PremiumManager cm = new PremiumManager();
        DataSet ds;
        //UserData condition = new UserData();
        ds = cm.searchDataOnlineCarMark(txtCarMark.Text, txtCarName.Text, txtCarGroup.Text);
        ViewState.Remove("SortDirection");
        ViewState.Remove("SortString");
        ViewState.Remove("SortIndex");
        ViewState.Add("DSMasterTable", ds);
        ShowData();

        // สำหรับกดปุ่มกลับมาหน้าค้นหา แล้วให้แสดงค่าเดิมไว้
        //condition.LOGINNAME = txtLoginName.Text;
        //condition.EMPCODE = txtEmpCode.Text;
        //condition.USERFNAME = txtFName.Text;
        //condition.USERLNAME = txtLName.Text;
        //condition.ROLEID = ddlRole.SelectedValue;
        //condition.STARTDATE = ucTxtDate1.Text;
        //condition.ENDDATE = ucTxtDate2.Text;
        //Session["SEARCHCONDITION"] = condition;
    }
    protected void PageIndexChanged(Object source, DataGridPageChangedEventArgs e)
    {
        DataView dv = new DataView();
        DataSet ds = new DataSet();
        if (!(ViewState["DSMasterTable"] == null))
        {
            ds = (DataSet)ViewState["DSMasterTable"];
            if (e.NewPageIndex > dtgData.PageCount - 1)
                dtgData.CurrentPageIndex = dtgData.PageCount - 1;
            else
                dtgData.CurrentPageIndex = e.NewPageIndex;

            String sortBy = "";
            if (!(ViewState["DSMasterTable"] == null))
            {
                sortBy = (string)(ViewState["SortString"]);
            }
            dv.Table = ds.Tables[0];
            dv.Sort = sortBy;
            ShowData();

        }
    }   
    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataSet ds;
        ds = (DataSet)ViewState["DSMasterTable"];
        ds.Tables[0].Rows.InsertAt(ds.Tables[0].NewRow(), 0);
        ViewState["DSMasterTable"] = ds;
        dtgData.EditItemIndex = 0;
        ShowData();
        txtMode.Text = "A";
    }
    protected void lnkEdit_Click(object sender, EventArgs e)
    {
        LinkButton imgClick = (LinkButton)sender;
        int iIndex = Convert.ToInt32(imgClick.Attributes["dsindex"]);
        iIndex = iIndex % 10;
        dtgData.EditItemIndex = iIndex;
        ShowData();
        txtMode.Text = "E";
    }
    protected void lnkCancel_Click(object sender, EventArgs e)
    {
        dtgData.EditItemIndex = -1;
        ShowData();
    }
    protected void lnkSave_Click(object sender, EventArgs e)
    {
        LinkButton imgClick = (LinkButton)sender;
        DataSet ds;
        TextBox txtDtgCarMark;
        TextBox txtDtgCarName;
        TextBox txtDtgCarGroup;
        TextBox txtDtgDescT;
        TextBox txtDtgDescE;
        TextBox txtDtgCarType;
        TextBox txtDtgODMin;
        TextBox txtDtgODMax;
        TextBox txtDtgCarCC;
        int iIndex = Convert.ToInt32(imgClick.Attributes["dsindex"]);
        int iDtg = iIndex % 10;
        bool isRet = true;
        string strRet = "";
        PremiumManager cm = new PremiumManager();
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);

        ds = (DataSet)ViewState["DSMasterTable"];
        txtDtgCarMark = (TextBox)dtgData.Items[iDtg].FindControl("txtCarMark");
        txtDtgCarName = (TextBox)dtgData.Items[iDtg].FindControl("txtCarName");
        txtDtgCarGroup = (TextBox)dtgData.Items[iDtg].FindControl("txtCarGroup");
        txtDtgDescT = (TextBox)dtgData.Items[iDtg].FindControl("txtDescT");
        txtDtgDescE = (TextBox)dtgData.Items[iDtg].FindControl("txtDescE");
        txtDtgCarType = (TextBox)dtgData.Items[iDtg].FindControl("txtCarType");
        txtDtgODMin = (TextBox)dtgData.Items[iDtg].FindControl("txtODMin");
        txtDtgODMax = (TextBox)dtgData.Items[iDtg].FindControl("txtODMax");
        txtDtgCarCC = (TextBox)dtgData.Items[iDtg].FindControl("txtCarCC");
 
        ds.Tables[0].Rows[iIndex]["carmakcod"] = txtDtgCarMark.Text;
        ds.Tables[0].Rows[iIndex]["carnamcod"] = txtDtgCarName.Text;
        ds.Tables[0].Rows[iIndex]["group_code"] = txtDtgCarGroup.Text;
        ds.Tables[0].Rows[iIndex]["desc_t"] = txtDtgDescT.Text;
        ds.Tables[0].Rows[iIndex]["desc_e"] = txtDtgDescE.Text;
        ds.Tables[0].Rows[iIndex]["car_type"] = txtDtgCarType.Text;
        ds.Tables[0].Rows[iIndex]["od_min"] = txtDtgODMin.Text;
        ds.Tables[0].Rows[iIndex]["od_max"] = txtDtgODMax.Text;
        ds.Tables[0].Rows[iIndex]["car_cc"] = txtDtgCarCC.Text;

        litJavaScript.Text = "<script>";
        if (txtMode.Text == "A")
        {
			if (cm.isDuplicateOnlineCarMark(ds.Tables[0].Rows[iIndex]) == true)
			{
				strRet = cm.addDataOnlineCarMark(ds.Tables[0].Rows[iIndex], loginData.loginName);
				if (strRet == "")
				{
					litJavaScript.Text += "alert('บันทึกข้อมูลเรียบร้อย');";
					dtgData.EditItemIndex = -1;					
				}
				else
				{
					litJavaScript.Text += "alert('ไม่สามารถบันทึกข้อมูลได้');";
				}
			}
			else
			{
				litJavaScript.Text += "alert('มีข้อมูลที่ระบุแล้ว');";
			}
        }
        else
        {
            isRet = cm.editDataOnlineCarMark(ds.Tables[0].Rows[iIndex], loginData.loginName);
            if (isRet == false)
            {
                litJavaScript.Text += "alert('ไม่สามารถบันทึกข้อมูลได้');";
            }
            else
            {
                litJavaScript.Text += "alert('บันทึกข้อมูลเรียบร้อย');";
                dtgData.EditItemIndex = -1;
            }
        }
        litJavaScript.Text += "</script>";

        ViewState["DSMasterTable"] = ds;
        ShowData();
    }    

    public void initialDataGrid()
    {
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        int iPageSize = 0;
        dt = TDS.Utility.MasterUtil.AddColumnToDataTable(dt, ColumnType, ColumnName);
        ds.Tables.Add(dt);
        ViewState.Remove("SortDirection");
        ViewState.Remove("SortString");
        ViewState.Remove("SortIndex");
        ViewState.Add("DSMasterTable", ds);
        ShowData();
        GridViewUtil.setGridStyle(dtgData, ColumnWidth, ColumnAlign, ColumnTitle);
        try
        {
            iPageSize = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["pageSize"].ToString());
        }
        catch (Exception e)
        {
            iPageSize = 10;
        }
        dtgData.PageSize = iPageSize;   
    }
    public void initialDropdownList()
    {
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        AuthenticateManager cm = new AuthenticateManager();
        
    }
    public void ShowData()
    {
        DataSet ds = new DataSet();
        if (ViewState["DSMasterTable"] != null)
            ds = (DataSet)ViewState["DSMasterTable"];

        string sortBy = "";
        if (ViewState["SortString"] != null)
            sortBy = (String)ViewState["SortString"];

        if (ds.Tables.Count > 0)
        {
            dv.Table = ds.Tables[0];
            dv.Sort = sortBy;

            int newPageCount = (Int32)(Math.Ceiling((double)dv.Table.Rows.Count / (double)dtgData.PageSize));
            if (dtgData.CurrentPageIndex >= newPageCount && newPageCount > 0)
                dtgData.CurrentPageIndex = newPageCount - 1;
        }
        dtgData.DataSource = dv;
        dtgData.DataBind();

        GridViewUtil.ItemDataBound(dtgData.Items);

        ucPageNavigator1.dvData = ds.Tables[0].DefaultView;
        ucPageNavigator1.dtgData = dtgData;
    }
    public void sortColumn(object Sender, EventArgs e)
    {
        string[] SortDirection = new string[ColumnTitle.Length];
        for (int i = 0; i < SortDirection.Length; i++)
        {
            SortDirection[i] = "";
        }
        if (ViewState["SortDirection"] != null)
            SortDirection = (string[])ViewState["SortDirection"];

        LinkButton lb = (LinkButton)Sender;
        string sortBy = "";
        int index = 0;
        while ((index < ColumnName.Length) && !((lb.Text).Equals(ColumnTitle[index])))
        {
            index++;
        }
        sortBy = ("" + ColumnName[index] + " " + SortDirection[index]);
        SortDirection[index] = (SortDirection[index].Equals("") ? "DESC" : "");
        ViewState.Add("SortDirection", SortDirection);
        ViewState.Add("SortString", sortBy);
        ViewState.Add("SortIndex", index);
        ShowData();
    }
    public string getPicUrl(int colNo)
    {
        string returnValue = Request.ApplicationPath + "/Images/tran.gif";
        if (ViewState["SortIndex"] != null)
        {
            DataGridItem dgi = (DataGridItem)(dtgData.Controls[0].Controls[1]);

            string[] SortDirection = new string[ColumnTitle.Length];
            SortDirection = (string[])ViewState["SortDirection"];

            if ((int)(ViewState["SortIndex"]) == colNo)
            {
                returnValue = Request.ApplicationPath + "/Images/" + (SortDirection[colNo].Equals("") ? "sortDown.gif" : "sortUp.gif");
            }
        }
        return returnValue;
    }    
}
