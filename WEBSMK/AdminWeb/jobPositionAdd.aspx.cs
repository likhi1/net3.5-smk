﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---------- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;
using System.Collections;

public partial class adminWeb_jobPositionAdd : System.Web.UI.Page
{

    public string CatId { get { return Request.QueryString["cat"];  } }

    protected void Page_Load(object sender, EventArgs e)
    {
         
     
    }
     
    protected void btnSave_Click(object sender, EventArgs e) {
        string sql = "INSERT INTO  tbJobPosi    VALUES ( " 
      //  +" VALUES ( @JobPosiName  ,  @JobPosiBranch  , @JobPosiDetail  , @JobPosiQnt  , @JobPosiStatus ,  @JobPosiSort  ,   @JobPosiDtStart  ,   @JobPosiDtStop  ,  @JobPiority  ,   @JobPosiDateReg  )" ;
                        + " @JobCatId , "  
                        + " @JobPosiName , " 
                        + " @JobPosiBranch , "
                        + " @JobPosiDetail , "
                        + " @JobPosiQnt , "
                        + " @JobPosiStatus , "
                        + " @JobPosiSort , "
                        + " @JobPosiDtStart , "
                        + " @JobPosiDtStop  , "
                        + " @JobPiority  , "
                        + " @JobPosiDateReg ,   "
                        //=====AdminInfo Add
                        + " @lang , "
                        + " @input_date ,   "
                        + " @input_user  "
                        + " ) ";

        SqlParameterCollection objPm  = new SqlCommand().Parameters ;
         
        Decimal decSort;
        objPm.AddWithValue("@JobCatId", CatId);
        objPm.AddWithValue("@JobPosiName", tbxPosition.Text  );
        objPm.AddWithValue("@JobPosiBranch", tbxBranch.Text);
        objPm.AddWithValue("@JobPosiDetail", tbxDetail.Text);
        objPm.AddWithValue("@JobPosiQnt",   tbxQnt.Text   );
        objPm.AddWithValue("@JobPosiStatus", rblStatus.SelectedValue );
        objPm.AddWithValue("@JobPosiSort", ( decSort = (tbxSort.Text != "") ? Convert.ToDecimal(tbxSort.Text) : 9999));
        objPm.AddWithValue("@JobPosiDtStart", Convert.ToDateTime( tbxDateStart.Text , new CultureInfo("th-TH" ) ) );
        objPm.AddWithValue("@JobPosiDtStop",  Convert.ToDateTime( tbxDateStop.Text, new CultureInfo("th-TH") ) );
        objPm.AddWithValue("@JobPiority", rblPiority.SelectedValue);
        objPm.AddWithValue("@JobPosiDateReg", DateTime.Now);
        
        //===== AdminInfo Add 
        objPm.AddWithValue("@lang", '0' ); // TH = 0 ,  En =  1
        objPm.AddWithValue("@input_date", DateTime.Now);
        //////Get UserName From Session Object
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        objPm.AddWithValue("@input_user", loginData.loginName);

        //Response.Write(loginData.loginName); 
        int i1 = new DBClass().SqlExecute(sql, objPm);

        if (i1 == 1) {
            Response.Write("<script>alert('เพิ่มข้อมูลเรียบร้อยแล้ว'); location='jobPositionList.aspx?cat=" + CatId + "' ;</script>");
        } else {
            Response.Write("<script>alert('ไม่สามารถเพิ่มข้อมูลได้'); history.back() ;</script>");

        }          
    }
 

}