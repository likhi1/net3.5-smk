﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminWeb/MasterPageAdmin1.master" AutoEventWireup="true" CodeFile="jobPositionView.aspx.cs" Inherits="adminWeb_jobPositionView"
    ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- ===============  Date  Picker - Config -->
    <script>
        $(function () {
            $("[id*=tbxDate]").attr('readOnly', 'true');
            $("[id*=tbxDate]").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "-0:+10",
                isBE: true,
                autoConversionField: false
            })

        });
    </script>

    <!-- ===============  CKEditor Config &  CKEditor With CKFinder Config  --> 
    <script>
        $(function () {
            /// CKFinder  
            //    CKEDITOR.replace('<%=tbxBranch.ClientID%>', varToolbarSmall);
            /// CKFinder With CKEditer
            if (typeof CKEDITOR != 'undefined') {
                var editor = CKEDITOR.replace('<%=tbxDetail.ClientID%>', { customConfig: '../ckeditor_config/fullMenu.js' } );
                ////////  Fix This When change location of this file  (This path for refer to file in folder  ckfinder )
                CKFinder.setupCKEditor(editor, '../ckfinder/');
            }
        });// end onload
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <h1 class="subject1">แก้ไข ตำแหน่งพนักงาน </h1>

    <style>
        .label { width: 100px; vertical-align: top; margin-right: 5px; }
        input[type=text] { }
        select { }
        textarea { }
        table td { vertical-align: top; }
    </style>
     

    <table style="width: 100%;">
        <tr>
            <td class="label">ID :</td>
            <td>
                <asp:Label ID="lblId" runat="server"></asp:Label> 
            </td>
            <td class="label">วันที่เริ่มแสดง :</td>
            <td>
                <asp:TextBox ID="tbxDateStart" runat="server"></asp:TextBox>
            </td>


        </tr>
        <tr>
            <td class="label">cสถานะการแสดง :</td>
            <td>
                <asp:RadioButtonList ID="rblStatus" runat="server"
                    RepeatDirection="Horizontal" TextAlign="Left">
                    <asp:ListItem Value="0">ปิด</asp:ListItem>
                    <asp:ListItem Value="1" Selected="True">เปิด</asp:ListItem>
                </asp:RadioButtonList>
            </td>





            <td class="label">วันที่หยุดแสดง :</td>
            <td>
                <asp:TextBox ID="tbxDateStop" runat="server"></asp:TextBox>
            </td>


        </tr>
        <tr>
            <td class="label">ลำดับแสดง :</td>
            <td>
                <asp:TextBox ID="tbxSort" runat="server"></asp:TextBox>
            </td>





            <td class="label">&nbsp;</td>
            <td>&nbsp;</td>


        </tr>

    </table>


    <table style="width: 100%;">
        <tr>
            <td class="label" width="150px">ตำแหน่งงาน :</td>
            <td>
                <asp:TextBox ID="tbxPosition" runat="server" Width="700px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="label">ความเร่งด่วน :</td>
            <td>
                <div style="float: left">
                    <asp:RadioButtonList ID="rblPiority" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Selected="True" Value="0">ไม่ใช่</asp:ListItem>
                        <asp:ListItem Value="1">ใช่</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
                <span class="star1">แสดง Icon ด่วน หลังชื่อตำแหน่ง </span>
            </td>
        </tr>
        <tr>
            <td class="label">ประจำสาขา :</td>
            <td>
                <asp:TextBox ID="tbxBranch" runat="server" Width="700px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="label">จำนวนที่รับ :</td>
            <td>
                <asp:TextBox ID="tbxQnt" runat="server"></asp:TextBox>
                <span class="star1">สามารถพิมพ์เป็นเลข หรือตัวหนังสือก็ได้</span>
            </td>
        </tr>


        <tr>
            <td class="label">รายละเอียดงาน :</td>
            <td>
                <asp:TextBox ID="tbxDetail" runat="server" TextMode="MultiLine" Width="700px"></asp:TextBox>
            </td>
        </tr>




        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Button ID="btnSave" runat="server" Text="ยืนยัน" class="btn-default  btn-small" Style="margin-right: 5px;" OnClick="btnSave_Click" />
                <asp:Button ID="Button2" runat="server" Text="ยกเลิก" class="btn-default  btn-small" OnClientClick="history.back();return false;" />
            </td>
        </tr>




        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>

    </table>



</asp:Content>

