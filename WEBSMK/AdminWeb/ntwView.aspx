﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminWeb/MasterPageAdmin1.master" AutoEventWireup="true" CodeFile="ntwView.aspx.cs"
     Inherits="adminWeb_branchView" ValidateRequest="false"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <!-- ===============  Date  Picker - Config -->
    <script>
        //================ On Load
        $(function(){ 
            ////////// Set Varbles  
            btnSave  = $("#<%=btnSave.ClientID%>");
            ddlCat = $("#<%=ddlCat.ClientID%>") ;
            ddlZone = $("select[id*=ddlZone]") ;
            ddlProvince =  $("#<%=ddlProvince.ClientID%>") ;
            ddlAmphoe =  $("#<%=ddlAmphoe.ClientID%>") ;
            tbxName = $("#<%=tbxName.ClientID%>") ;
            tbxSort = $("#<%=tbxSort.ClientID%>");

            var ddlNetworkType = $("#<%=ddlNetworkType.ClientID%>");
            cat = parseInt( <%=CatId%>);

            limitControler(); 
             tbxSort.keypress(validateNumber);
             btnSave.click(function(){ 
                 return CheckNull();
             })
        }); 

        //=============== Set DatePicker
        function setDatePicker(){
            $("[id*=tbxDate]").attr('readOnly', 'true');
            $("[id*=tbxDate]").datepicker({ 
                changeMonth: true,
                changeYear: true,
                yearRange: "-0:+10",
                isBE:true,
                autoConversionField: false 
            })
        }  
        //=============== Validate  
        function isDecimal(_val){
            patt = /^[0-9]+([.,][0-9]{1,3})?$/ ;
            re =  patt.test(_val);
            return re;
        }
        function validateNumber(event) {
            var key = window.event ? event.keyCode : event.which;
            if (event.keyCode == 8 || event.keyCode == 46
             || event.keyCode == 37 || event.keyCode == 39) {
                return true;
            }
            else if (key < 48 || key > 57) {
                return false;
            }
            else return true;
        }
        function CheckNull(){
            if(ddlCat.val() == "" || ddlCat.val()  ==  null ){
                alert("กรุณาเลือกเครือข่าย");  return false ;
            }else if (ddlZone.val()  == "" || ddlZone.val()  == null   ){
                alert("กรุณาเลือกภูมิภาค");   return false ;
            }else if (ddlProvince.val()  == "" || ddlProvince.val()  == null ){
                alert("กรุณาเลือกจังหวัด");  return false ;
            }else if (ddlProvince.val()  == "กรุงเทพมหานคร" && ddlAmphoe.val()  == "" ){
                alert("กรุณาเลือกเขต/อำเภอ");  return false ;
            }else if ( ( ddlCat.val()  == "4" && ddlNetworkType.val()  == "" )  || (ddlCat.val()  == "2" && ddlNetworkType.val()  == "")  ){
                alert("กรุณาเลือกประเภท");  return false ;
            }else if (tbxName.val()  == "" || tbxName.val()  == null ){
                alert("กรุณาระบุ ชื่อเครือข่าย");  return false ;
            } else if(!tbxSort.val() == "" ){
                // alert("กรุณาระบุ ลำดับด้วย");  
                if (isDecimal(tbxSort.val() ) == false ){ 
                    alert("กรุณาระบุ ให้ถูกต้อง และมีทศนิยมได้ไม่เกิน 3 ตำแหน่ง");   return false ;
                }
            } 
            return true ;
        }    
        //====== Limit  Select DropDownList
        function limitControler() {
            var ddlProvince = $("#<%=ddlProvince.ClientID%>");
            var ddlAmphoe = $("#<%=ddlAmphoe.ClientID%>");
           
          
            // ddlNetworkType.attr("disabled", "disabled");
            if(ddlProvince.val() != "กรุงเทพมหานคร"){
                ddlAmphoe.val('');
                ddlAmphoe.attr("disabled", "disabled");
            }
            ddlProvince.change(function () {
                if (ddlProvince.val() == "กรุงเทพมหานคร")
                    ddlAmphoe.removeAttr("disabled");
                else
                    ddlAmphoe.attr("disabled", "disabled");
                    ddlAmphoe.val('');
            });
            //if (cat == 2 || cat == 4) {
            //    ddlNetworkType.removeAttr("disabled");
            //} else {
            //    ddlNetworkType.attr("disabled", "disabled");
            //}
        }
   </script> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
      <h1 class="subject1"> 
          แก้ไข <asp:Label ID="lblPageName" runat="server"  ></asp:Label> 
      </h1>
    <table style="width: 100%;">
            <tr>
                <td class="label" width="110px">ID :</td>
                <td>
                    <asp:Label ID="lblId" runat="server"  ></asp:Label> 
                </td>
                <td class="label">&nbsp;</td>
                <td>
                    &nbsp;</td> 
            </tr>
            <tr>
                <td class="label" width="110px">เครือข่าย :</td>
                <td>
                    <asp:DropDownList ID="ddlCat" runat="server" Style="margin-bottom: 0px"    >
                        <asp:ListItem Value="">-- โปรดระบุ  --</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="label">สถานะการแสดง :</td>
                <td>
                    <asp:RadioButtonList ID="rblStatus" runat="server"
                        RepeatDirection="Horizontal" TextAlign="Left">
                        <asp:ListItem Value="0" Selected="True">ปิด</asp:ListItem>
                        <asp:ListItem Value="1">เปิด</asp:ListItem>  
                    </asp:RadioButtonList>
                </td> 
            </tr>
            <tr> 
                 <td class="label">ภูมิภาค : </td>
                <td>
                    <myUc:ucZone runat="server" ID="ucZone" />
                </td>
                <td class="label">ลำดับการแสดง :</td>
                <td>
                    <asp:TextBox ID="tbxSort" runat="server" ></asp:TextBox> <span class="star1">สามารถกำหนดเป็นทศนิยมได้</span>
                    </td>  
            </tr> 
            <tr>
                <td class="label">จังหวัด : </td>
                <td>
                      <asp:DropDownList ID="ddlProvince" runat="server"> 
                          <asp:ListItem Value="">-- โปรดระบุ  --</asp:ListItem> 
                    </asp:DropDownList>
                </td> 
                <td class="label">เบอร์โทรศัพท์ :</td>
                <td>
                    <asp:TextBox ID="tbxTelPhone" runat="server" class="datepicker"  Width="230px"></asp:TextBox>
                    </td> 
            </tr> 
            <tr>
               <td class="label">เขต/อำเภอ : </td>
                <td>
                    <asp:DropDownList ID="ddlAmphoe" runat="server"> 
                          <asp:ListItem Value="">-- โปรดระบุ  --</asp:ListItem> 
                    </asp:DropDownList>
                </td> 
                <td class="label">เบอร์มือถือ :</td>
                <td>
                    <asp:TextBox ID="tbxTelMobile" runat="server" Width="230px"  ></asp:TextBox>
                    </td> 
            </tr> 
        <tr>
                <td class="label">ประเภท  :</td>
                <td>
                <asp:DropDownList ID="ddlNetworkType" runat="server">
                <asp:ListItem Value="">-- โปรดระบุ  --</asp:ListItem>
                </asp:DropDownList>
                </td>
            <td class="label">เบอร์แฟกต์ : </td>
            <td>
                <asp:TextBox ID="tbxNtwFax" runat="server" Width="230px"></asp:TextBox>
            </td>
        </tr>
        <tr>
                <%if (CatId== 4) { %>
            <td class="label">รหัสสาขา :</td>
            <td>
                <asp:TextBox ID="tbxNumCode" runat="server"></asp:TextBox></td>
               <%}else { %> 
            <td class="label"> </td>
            <td></td>
            <%}  %> 
            <td class="label">อีเมล์ : </td>
            <td>
            <asp:TextBox ID="tbxEmail" runat="server" Width="230px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="label">&nbsp;</td>
            <td>&nbsp;</td>
            <td class="label">เวบไซต์ : </td>
            <td>
                <asp:TextBox ID="tbxWebsite" runat="server" Width="230px"></asp:TextBox>
            </td>
        </tr>
        </table>
       <table style="width: 100%;">
            <tr>
                <td class="label"  width="110px">ชื่อเครือข่าย :</td>
                <td>
                    <asp:TextBox ID="tbxName" runat="server"   class="ckeditor" Width="700" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label " valign="top">ที่อยู่ :</td>
                <td>
                   <%--  <textarea id="txaConDetailShort" name="txaDetailShort" cols="20" rows="2"></textarea>--%>
                   <asp:TextBox ID="tbxAddress" TextMode="MultiLine" runat="server" Width="700" Height="50px"  ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label " valign="top">ลักษณะบริการ :</td>
                <td>
                  <asp:TextBox ID="tbxService" TextMode="MultiLine" runat="server" Width="700" Height="50px"  ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label" valign="top">หมายเหตุ :</td>
                <td>
                   <%-- <textarea id="txaConDetailLong" name="txaDetailLong" rows="10" cols="80"></textarea>--%>
                    <asp:TextBox ID="tbxNotation" TextMode="MultiLine" runat="server" Width="700"  Height="50px"  ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label" valign="top">ลิงค์ฝังแผนที่ :</td>
                <td>
                    <asp:TextBox ID="tbxMap" TextMode="MultiLine"  runat="server" Width="700"  Height="130px"  ></asp:TextBox>
 <p class="star1">
  ใส่โค๊ด Google Map จากช่อง  =>  วาง HTML เพื่อฝังในเว็บไซต์ ( แท๊ก iframe )  โดยให้กำหนดขนาดแผนที่เป็น 640 * 450 px   


 </p>
                </td>
            </tr>

                 <tr>
            <td  class="label" valign="top" >ลิงค์เปิดแผนที่ :</td>
            <td class="auto-style4"> 
                <asp:TextBox ID="tbxMapLink" runat="server"  TextMode="MultiLine"   Width="700" Height="50px"></asp:TextBox>
                  <p class="star1">
                      
 ใส่โค๊ด Google Map จากช่อง  => วางลิงค์์ใน อีเมล หรือ IM  

                  </p>
                   </td>
                </tr>
      


            <tr>
                <td  class="label" valign="top">รูปสถานที่ 1:</td>
                <td>   
                    <asp:TextBox ID="tbxPic1" runat="server" Width="400px"  ></asp:TextBox> 
                    <input type="button" value="Browse Server" onclick="BrowseServer('Images:/', '<%=tbxPic1.ClientID%>');" /></tr>
            </tr>
            <tr>
                <td  class="label" valign="top">รูปสถานที่ 2:</td>
                <td> 
                    <asp:TextBox ID="tbxPic2" runat="server" Width="400px"  ></asp:TextBox> 
                    <input type="button" value="Browse Server" onclick="BrowseServer('Images:/', '<%=tbxPic2.ClientID%>');" /></tr>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="label">&nbsp;</td>
                <td>
<%--OnClientClick="return ChkNullPrd();"--%>
                    <asp:Button ID="btnSave" runat="server" Text="บันทึก" OnClick="btnSave_Click"  CssClass="btn-default"
                        Style="margin-right: 10px;" />
                    <asp:Button ID="btnClear" runat="server" Text="ยกเลิก" CssClass="btn-default"  OnClientClick="history.back();return false;"  UseSubmitBehavior="false"  />
                </td>
            </tr>
        </table>   
</asp:Content>