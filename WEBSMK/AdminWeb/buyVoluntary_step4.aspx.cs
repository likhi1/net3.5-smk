﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class buyVoluntary_step4 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        RegisterManager cmRegister = new RegisterManager();
        if (Request.QueryString["regis_no"] != "")
        {
            ds = cmRegister.getDataVoluntary(Request.QueryString["regis_no"]);
        }
        
        if (!IsPostBack)
        {
            docToUI(ds);
        }
    }

    public void docToUI(DataSet ds)
    {
        string strMotGar = "";
        DataSet dsPrem = new DataSet();
        DataSet dsTmp = new DataSet();
        OnlineVoluntaryManager cmOnline = new OnlineVoluntaryManager();

        DataSet dsRegister = new DataSet();
        if (Session["DSRegister"] != null)
            dsRegister = (DataSet)Session["DSRegister"];
        
        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            lblRefNo.Text = ds.Tables[0].Rows[0]["rg_regis_no"].ToString();
            lblFName.Text = ds.Tables[0].Rows[0]["rg_ins_fname"].ToString();
            lblLName.Text = ds.Tables[0].Rows[0]["rg_ins_lname"].ToString();
            lblAddress.Text = ds.Tables[0].Rows[0]["rg_ins_addr1"].ToString();
            lblSubDistrict.Text = ds.Tables[0].Rows[0]["rg_ins_addr2"].ToString();
            lblDistrict.Text = ds.Tables[0].Rows[0]["rg_ins_amphor"].ToString();
            dsTmp = cmOnline.getDataProvinceByCode(ds.Tables[0].Rows[0]["rg_ins_changwat"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                lblProvince.Text = dsTmp.Tables[0].Rows[0]["ds_desc"].ToString();            
            lblPostCode.Text = ds.Tables[0].Rows[0]["rg_ins_postcode"].ToString();
            lblIDCard.Text = ds.Tables[0].Rows[0]["rg_ins_idcard"].ToString();
            lblMobile.Text = ds.Tables[0].Rows[0]["rg_ins_mobile"].ToString();
            lblTelephone.Text = ds.Tables[0].Rows[0]["rg_ins_tel"].ToString();
            lblEmail.Text = ds.Tables[0].Rows[0]["rg_ins_email"].ToString();
            if (ds.Tables[0].Rows[0]["vl_drv1"].ToString() == "")
            {
                lblDriver1Name.Text = "-";
                lblDriver1BrithDate.Text = "-";
            }
            else
            {
                lblDriver1Name.Text = ds.Tables[0].Rows[0]["vl_drv1"].ToString();
                lblDriver1BrithDate.Text = ds.Tables[0].Rows[0]["vl_birth_drv1"].ToString();
            }
            if (ds.Tables[0].Rows[0]["vl_drv2"].ToString() == "")
            {
                lblDriver2Name.Text = "-";
                lblDriver2BrithDate.Text = "-";
            }
            else
            {
                lblDriver2Name.Text = ds.Tables[0].Rows[0]["vl_drv2"].ToString();
                lblDriver2BrithDate.Text = ds.Tables[0].Rows[0]["vl_birth_drv2"].ToString();
            }

            dsTmp = cmOnline.getDataCarNameByCode(ds.Tables[0].Rows[0]["mv_major_cd"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                lblCarName.Text = dsTmp.Tables[0].Rows[0]["desc_t"].ToString();

            dsTmp = cmOnline.getDataCarMarkByCode(ds.Tables[0].Rows[0]["mv_minor_cd"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                lblCarMark.Text = dsTmp.Tables[0].Rows[0]["desc_t"].ToString();
            lblCarRegisYear.Text = ds.Tables[0].Rows[0]["mv_veh_year"].ToString();
            if (ds.Tables[0].Rows[0]["vl_veh_cd"].ToString().Trim() == "110")
            {
                lblCarType.Text = "รถยนต์นั่ง";
                lblCarUseDesc.Text = "ส่วนบุคคล";
            }
            else if (ds.Tables[0].Rows[0]["vl_veh_cd"].ToString().Trim() == "210")
            {
                lblCarType.Text = "รถยนต์โดยสาร";
                lblCarUseDesc.Text = "ส่วนบุคคล";
            }
            else if (ds.Tables[0].Rows[0]["vl_veh_cd"].ToString().Trim() == "320")
            {
                lblCarType.Text = "รถยนต์บรรทุก";
                lblCarUseDesc.Text = "เพื่อการพาณิชย์";
            }
            dsTmp = cmOnline.getDataDriveDistanceByID(ds.Tables[0].Rows[0]["rg_oth_distance"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                lblDistance.Text = dsTmp.Tables[0].Rows[0]["ddis_name"].ToString();
            else
                lblDistance.Text = "-";
            lblDriveRegion.Text = (ds.Tables[0].Rows[0]["rg_oth_region"].ToString() == "1" ? "กรุงเทพฯ" : "ต่างจังหวัด");
            if (ds.Tables[0].Rows[0]["rg_oth_oldpolicy"].ToString() == "Y")
                lblFlagOldPolicy.Text = "มี";
            else
                lblFlagOldPolicy.Text = "ไม่มี";
            if (ds.Tables[0].Rows[0]["rg_insc_id"].ToString() != "")
            {
                dsTmp = cmOnline.getDataInsuranceCompanyByID(ds.Tables[0].Rows[0]["rg_insc_id"].ToString());
                if (dsTmp.Tables[0].Rows.Count > 0)
                    lblOldInsuraceComp.Text = dsTmp.Tables[0].Rows[0]["insc_name"].ToString();
            }
            else
                lblOldInsuraceComp.Text = "-";
            if (ds.Tables[0].Rows[0]["rg_mott_id"].ToString() != "")
            {
                dsTmp = cmOnline.getDataMotorTypeByID(ds.Tables[0].Rows[0]["rg_mott_id"].ToString());
                if (dsTmp.Tables[0].Rows.Count > 0)
                    lblOldPolType.Text = dsTmp.Tables[0].Rows[0]["mott_name"].ToString();
            }
            else
                lblOldPolType.Text = "-";
            lblCarLicense.Text = ds.Tables[0].Rows[0]["mv_license_no"].ToString();
            dsTmp = cmOnline.getDataProvinceByCode(ds.Tables[0].Rows[0]["mv_license_area"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                lblLicenseProvince.Text = dsTmp.Tables[0].Rows[0]["ds_desc"].ToString();

            if (ds.Tables[0].Rows[0]["vl_veh_cd"].ToString().Trim() == "110")
            {
                lblCarSize.Text = ds.Tables[0].Rows[0]["mv_veh_cc"].ToString();
            }
            else if (ds.Tables[0].Rows[0]["vl_veh_cd"].ToString().Trim() == "210")
            {
                lblCarSize.Text = ds.Tables[0].Rows[0]["mv_veh_seat"].ToString();
            }
            else if (ds.Tables[0].Rows[0]["vl_veh_cd"].ToString().Trim() == "320")
            {
                lblCarSize.Text = ds.Tables[0].Rows[0]["mv_veh_weight"].ToString();
            }            
            txtChasis.Text = ds.Tables[0].Rows[0]["mv_chas_no"].ToString();
            txtEngineNo.Text = ds.Tables[0].Rows[0]["mv_engin_no"].ToString();

            if (ds.Tables[0].Rows[0]["vl_tpbi_person"].ToString() == "" ||
                 Convert.ToDouble("0" + ds.Tables[0].Rows[0]["vl_tpbi_person"].ToString()) == 0)
                lbltpbi1.Text = " ไม่คุ้มครอง ";
            else
                lbltpbi1.Text = FormatStringApp.FormatInt(ds.Tables[0].Rows[0]["vl_tpbi_person"].ToString());
            if (ds.Tables[0].Rows[0]["vl_tpbi_time"].ToString() == "" ||
                 Convert.ToDouble("0" + ds.Tables[0].Rows[0]["vl_tpbi_time"].ToString()) == 0)
                lbltpbi2.Text = " ไม่คุ้มครอง ";
            else
                lbltpbi2.Text = FormatStringApp.FormatInt(ds.Tables[0].Rows[0]["vl_tpbi_time"].ToString());
            if (ds.Tables[0].Rows[0]["vl_tppd_time"].ToString() == "" ||
                 Convert.ToDouble("0" + ds.Tables[0].Rows[0]["vl_tppd_time"].ToString()) == 0)
                lbltppd.Text = " ไม่คุ้มครอง ";
            else
                lbltppd.Text = FormatStringApp.FormatInt(ds.Tables[0].Rows[0]["vl_tppd_time"].ToString());
            if (ds.Tables[0].Rows[0]["vl_tppd_exc"].ToString() == "" ||
                   Convert.ToDouble("0" + ds.Tables[0].Rows[0]["vl_tppd_exc"].ToString()) == 0)
                lblDeduct.Text = " - ";
            else
                lblDeduct.Text = FormatStringApp.FormatInt(ds.Tables[0].Rows[0]["vl_tppd_exc"].ToString());
            if (ds.Tables[0].Rows[0]["rg_sum_ins"].ToString() == "" ||
                   Convert.ToDouble("0" + ds.Tables[0].Rows[0]["rg_sum_ins"].ToString()) == 0)
                lblCarOD.Text = " - ";
            else
                lblCarOD.Text = FormatStringApp.FormatInt(ds.Tables[0].Rows[0]["rg_sum_ins"].ToString());
            if (ds.Tables[0].Rows[0]["vl_01_11"].ToString() == "" ||
                 Convert.ToDouble("0" + ds.Tables[0].Rows[0]["vl_01_11"].ToString()) == 0)
                lblperm_d_01.Text = " ไม่คุ้มครอง ";
            else
                lblperm_d_01.Text = FormatStringApp.FormatInt(ds.Tables[0].Rows[0]["vl_01_11"].ToString());
            if (ds.Tables[0].Rows[0]["vl_01_121"].ToString() == "" ||
                   Convert.ToDouble("0" + ds.Tables[0].Rows[0]["vl_01_121"].ToString()) == 0)
                lblperm_p_num.Text = " - ";
            else
                lblperm_p_num.Text = FormatStringApp.FormatInt(ds.Tables[0].Rows[0]["vl_01_121"].ToString());
            if (ds.Tables[0].Rows[0]["vl_01_122"].ToString() == "" ||
                 Convert.ToDouble("0" + ds.Tables[0].Rows[0]["vl_01_122"].ToString()) == 0)
                lblperm_p_01.Text = " ไม่คุ้มครอง ";
            else
                lblperm_p_01.Text = FormatStringApp.FormatInt(ds.Tables[0].Rows[0]["vl_01_122"].ToString());
            if (ds.Tables[0].Rows[0]["vl_01_21"].ToString() == "" ||
                 Convert.ToDouble("0" + ds.Tables[0].Rows[0]["vl_01_21"].ToString()) == 0)
                lbltemp_d_01.Text = " ไม่คุ้มครอง ";
            else
                lbltemp_d_01.Text = FormatStringApp.FormatInt(ds.Tables[0].Rows[0]["vl_01_21"].ToString());
            if (ds.Tables[0].Rows[0]["vl_02_person"].ToString() == "" ||
                   Convert.ToDouble("0" + ds.Tables[0].Rows[0]["vl_02_person"].ToString()) == 0)
                lbltemp_p_num.Text = " - ";
            else
                lbltemp_p_num.Text = FormatStringApp.FormatInt(ds.Tables[0].Rows[0]["vl_02_person"].ToString());
            if (ds.Tables[0].Rows[0]["vl_01_212"].ToString() == "" ||
                 Convert.ToDouble("0" + ds.Tables[0].Rows[0]["vl_01_212"].ToString()) == 0)
                lbltemp_p_01.Text = " ไม่คุ้มครอง ";
            else
                lbltemp_p_01.Text = FormatStringApp.FormatInt(ds.Tables[0].Rows[0]["vl_01_212"].ToString());
            if (ds.Tables[0].Rows[0]["vl_02"].ToString() == "" ||
                 Convert.ToDouble("0" + ds.Tables[0].Rows[0]["vl_02"].ToString()) == 0)
                lblcover_02.Text = " ไม่คุ้มครอง ";
            else
                lblcover_02.Text = FormatStringApp.FormatInt(ds.Tables[0].Rows[0]["vl_02"].ToString());
            if (ds.Tables[0].Rows[0]["vl_03"].ToString() == "" ||
                 Convert.ToDouble("0" + ds.Tables[0].Rows[0]["vl_03"].ToString()) == 0)
                lblcover_03.Text = " ไม่คุ้มครอง ";
            else
                lblcover_03.Text = FormatStringApp.FormatInt(ds.Tables[0].Rows[0]["vl_03"].ToString());

			lblStartDate.Text = FormatStringApp.FormatDate(ds.Tables[0].Rows[0]["rg_effect_dt"]);
            double dPrem = 0;
            double dStamp = 0;
            double dTax = 0;
            double dGross = 0;
            double dPaid = 0;
            dPrem = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["rg_prmm"]);
            dStamp = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["rg_stamp"]);
            dTax = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["rg_tax"]);
            dGross = dPrem + dStamp + dTax;
            dPaid = dGross;
            lblPremium.Text = FormatStringApp.FormatNDigit(ds.Tables[0].Rows[0]["rg_prmm"], 2) + " บาท";
            lblStamp.Text = FormatStringApp.FormatNDigit(ds.Tables[0].Rows[0]["rg_stamp"], 2) + " บาท";
            lblVat.Text = FormatStringApp.FormatNDigit(ds.Tables[0].Rows[0]["rg_tax"], 2) + " บาท";
            lblGrossPremium.Text = FormatStringApp.FormatNDigit(dGross, 2) + " บาท";
            dPrem = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["cp_comp_prmm"]);
            dStamp = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["cp_comp_stamp"]);
            dTax = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["cp_comp_tax"]);
            dGross = dPrem + dStamp + dTax;
            dPaid = dPaid + dGross;
            lblComPremium.Text = FormatStringApp.FormatNDigit(dGross, 2) + " บาท";
            lblPaid.Text = FormatStringApp.FormatNDigit(dPaid, 2) + " บาท";

            if (ds.Tables[0].Rows[0]["rg_pay_type"].ToString() == "1")
            {
                lblPaidMethod.Text = "ชำระเงินด้วยเงินสดหรือเช็ค ที่บริษัทสินมั่นคงประกันภัยจำกัด (มหาชน) ที่</br>";
                if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "100")
                    lblPaidMethod.Text += "สำนักงานใหญ่ (ถนนศรีนครินทร์)";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "100")
                    lblPaidMethod.Text += "สำนักงานใหญ่ (ถนนศรีนครินทร์)";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "101")
                    lblPaidMethod.Text += "สาขาสวนมะลิ (ยสเส)";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "102")
                    lblPaidMethod.Text += "สาขาดอนเมือง";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "102")
                    lblPaidMethod.Text += "สาขาบางแค";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "130")
                    lblPaidMethod.Text += "สาขารัตนาธิเบศร์";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "500")
                    lblPaidMethod.Text += "สาขาชลบุรี";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "201")
                    lblPaidMethod.Text += "สาขาพิษณุโลก";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "200")
                    lblPaidMethod.Text += "สาขาเชียงใหม่";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "601")
                    lblPaidMethod.Text += "สาขาหาดใหญ่";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "600")
                    lblPaidMethod.Text += "สาขาสุราษฎร์ธานี";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "300")
                    lblPaidMethod.Text += "สาขานครปฐม";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "401")
                    lblPaidMethod.Text += "สาขาขอนแก่น";
            }
            else if (ds.Tables[0].Rows[0]["rg_pay_type"].ToString() == "2")
            {
                lblPaidMethod.Text = "ชำระเงิน ด้วยวิธีการโอนเข้าบัญชีของบริษัทสินมั่นคงประกันภัย จำกัด (มหาชน)";
            }
            else if (ds.Tables[0].Rows[0]["rg_pay_type"].ToString() == "3")
            {
                lblPaidMethod.Text = "ชำระเงิน ด้วยวิธีการโอนเข้าบัญชีของบริษัทสินมั่นคงประกันภัย จำกัด(มหาชน) ผ่าน www.scbeasy.com";
            }
            else if (ds.Tables[0].Rows[0]["rg_pay_type"].ToString() == "4")
            {
                lblPaidMethod.Text = "บริการจัดส่งและจัดเก็บเบี้ยประกันถึงบ้าน (เฉพาะภายในเขตกรุงเทพฯ)";
            }
            else if (ds.Tables[0].Rows[0]["rg_pay_type"].ToString() == "5")
            {
                lblPaidMethod.Text = "ชำระเงิน ด้วยบัตรเครดิต";
            }
            if (ds.Tables[0].Rows[0]["rg_ref_bank"].ToString() == "1")
                lblSendDoc.Text = "ที่อยู่ตามกรมธรรม์";
            else
            {
            }

        }
    }
    
    protected void btnExport_Click(object sender, EventArgs e)
    {
        System.IO.StringWriter tw = new System.IO.StringWriter();
        HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
       // dtgData.RenderControl(hw);
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("Content-Disposition", "attachment;filename=PriceApproval.xls");
        Response.Clear();
        Response.Write(tw.ToString());
        Response.Flush();
        Response.End();
    }
}
