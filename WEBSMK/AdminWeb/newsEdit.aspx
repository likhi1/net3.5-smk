﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminWeb/MasterPageAdmin1.master" AutoEventWireup="true" CodeFile="newsEdit.aspx.cs" Inherits="adminWeb_newsEdit"   ValidateRequest="false"   %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server"> 
        <!-- ===============  Date  Picker  - Config  -->
    <script>
        $(function(){
            $("[id*=tbxDate]").attr('readOnly', 'true');
            $("[id*=tbxDate]").datepicker({
                // $('#<%=tbxDateHide.ClientID%>').datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "-0:+10",
                isBE:true,
                autoConversionField: false 
            })
        }); 
    </script>
    
    <!-- ===============  CKEditor Config &  CKEditor With CKFinder Config  -->
    <script>
        $(function () {
            /// CKFinder  
            <%if ( CatId != "5"  ) { %>
            CKEDITOR.replace('<%=tarDetailShort.ClientID%>'   ,  { 
                height : 100 , // กำหนดความสูง 
                forcePasteAsPlainText : true,   // Paste Pain Text
                toolbarCanCollapse : false ,  //not allow toolbar collapse
                resize_enabled : false, //remove resize  
                toolbar: [
                ['Source'],
                ['Bold', 'Italic', 'Underline', 'Strike', 'Outdent', 'Indent'], ['TextColor', 'BGColor'],
                ['FontSize', 'Styles', ] 
                ] 
            }) 
            <%} %>

            /// CKFinder & CKEditer
            if (typeof CKEDITOR != 'undefined') {
                var editor = CKEDITOR.replace('<%=tarDetailLong.ClientID%>' ,  {  customConfig : '../ckeditor_config/fullMenu.js' } );
                ////////  Fix This When change location of this file  (This path for refer to file in folder  ckfinder )
                CKFinder.setupCKEditor(editor, '../ckfinder/');
            }

        });// end onload
    </script>

        <!-- ===============  Validate  -->
    <script>
        
        $(function () {
            //// Set Varbles
            ddlPrdCat = $("#<%=ddlCat.ClientID%>") ; 
            tbxDateShow = $("[id*=<%=tbxDateShow.ClientID%>]") ;
            tbxDateHide = $("[id*=<%=tbxDateHide.ClientID%>]") ; 
            tbxTopic = $("#<%=tbxTopic.ClientID%>").val();
            tarDetailShort = $("#<%=tarDetailShort.ClientID%>") ;
            tarDetailLong = $("#<%=tarDetailLong.ClientID%>") ;
            btnSave = $("#<%=btnSave.ClientID%>");
            tbxSort = $("#<%=tbxSort.ClientID%>");

            //// Check Number
            tbxSort.keypress(validateNumber);
         

            btnSave.click(function ( ) { 
                return ChkNull();   
            });
  
        });

        
        function isDecimal(_val){
            patt = /^[0-9]+([.,][0-9]{1,3})?$/ ;
            re =  patt.test(_val);
            return re;
        }

        function validateNumber(event) {
            var key = window.event ? event.keyCode : event.which;
            if (event.keyCode == 8 || event.keyCode == 46
             || event.keyCode == 37 || event.keyCode == 39) {
                return true;
            }
            else if (key < 48 || key > 57) {
                return false;
            }
            else return true;
        }

        function ChkNull( ) {  
                if (ddlPrdCat == "" || ddlPrdCat == null) {
                    alert("กรุณากรอกกลุ่มสินค้า");  
                    return false;   
                } else if (tbxDateShow == "" || tbxDateShow == null) {
                    alert("กรุณากรอกวันที่แสดง"); 
                    return false;  
                } else if (tbxDateHide == "" || tbxDateHide == null) {
                    alert("กรุณากรอกวันที่หยุดแสดง"); 
                    return false;   
                } else if (tbxTopic == "" || tbxTopic == null) {
                    alert("กรุณากรอกหัวข้อด้วย"); 
                    return false;  
                } else if(!tbxSort.val() == "" ){
                    // alert("กรุณาระบุ ลำดับด้วย");  
                    if (isDecimal(tbxSort.val() ) == false ){ 
                        alert("กรุณาระบุ ให้ถูกต้อง และมีทศนิยมได้ไม่เกิน 3 ตำแหน่ง");   return false ;
                    }
                } 
      
                return true;  
        }


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div id="page-prdAdd">

        <h1 class="subject1">แก้ไข <asp:Label ID="lblPageName" runat="server"></asp:Label></h1>

        <table style="width: 100%;">
            <tr>
                 
                <td class="label" width="110px">ลำดับ :</td>
                <td>


                    <asp:Label ID="lblId" runat="server" Text="Label"></asp:Label>


                </td>


                <td class="label">สถานะการแสดง :</td>
                <td>
                    <asp:RadioButtonList ID="rblStatus" runat="server"
                        RepeatDirection="Horizontal" TextAlign="Left">
                        <asp:ListItem Value="0" Selected="True">ปิด</asp:ListItem>
                        <asp:ListItem Value="1">เปิด</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>

                <td class="label">ประเภทประกัน :</td>
                <td>
                    <asp:DropDownList ID="ddlCat" runat="server" Style="margin-bottom: 0px"  >
                        <asp:ListItem Value="">-- โปรดระบุ  --</asp:ListItem>
                    </asp:DropDownList>
                </td> 
                <td class="label">วันที่เริ่มต้นแสดงเนื้อหา :</td>
                <td>
                    <asp:TextBox ID="tbxDateShow" runat="server" class="datepicker"></asp:TextBox>
                </td> 

            </tr>
            <tr>
            
                
                <td class="label">ลำดับการแสดง :</td>
                <td>
                    <asp:TextBox ID="tbxSort" runat="server" MaxLength="8" ></asp:TextBox>  <span class="star1">สามารถกำหนดเป็นทศนิยมได้</span>

                    
                    </td>  
                <td class="label">วันที่สุดท้ายแสดงเนื้อหา :</td>
                <td>
                    <asp:TextBox ID="tbxDateHide" runat="server" class="datepicker"></asp:TextBox>
                </td>
            </tr>
        </table>

        <table style="width: 100%;">

            <tr>
                <td class="label" width="100px">&nbsp;</td>
                <td>
                &nbsp;
            </tr>


            <tr>
                <td class="label">หัวข้อ :</td>
                <td>
                    <asp:TextBox ID="tbxTopic" runat="server" Width="730px" class="ckeditor"  ></asp:TextBox>
                </td>
            </tr>
     <%if (CatId != "5") { // CatId  5 As Static Content  %> 
            <tr>
                <td class="label " valign="top">รายละเอียดย่อ :</td>
                <td>
                    <%--  <textarea id="txaConDetailShort" name="txaDetailShort" cols="20" rows="2"></textarea>--%>
                    <asp:TextBox ID="tarDetailShort" TextMode="MultiLine" runat="server"></asp:TextBox>
                </td> 

            </tr>
         
                <%} %>
        
                <%if ((CatId != "3") && (CatId != "5")) { // CatId  3 As TVC   ,   CatId  5 As Static Content   %>
            <tr>
                <td class="label">รูปภาพใหญ่ :</td>
                <td> 
                    <asp:TextBox ID="tbxPicBig" runat="server" Width="400px"  ></asp:TextBox>
                    <input type="button" value="Browse Server" onclick="BrowseServer('Images:/', '<%=tbxPicBig.ClientID%>');" />

<span class="star1"> รูปในส่วนที่แสดงรายละเอียดเต็ม </span>

<%--    
///////  Get Value From Html Control              
<input id="tbxPicBig" name="PicBig" type="text" size="60" /> 
<input type="button" value="Browse Server" onclick="BrowseServer('Images:/', 'tbxPicBig');" />
--%>
      
      

            </tr>
              <%}  %> 

  <%if (CatId == "3") {  // CatId  3 As TVC  %> 
              <tr>
                <td class="label" valign="top">VDO ที่จะแสดง  :</td>
                <td>    
                  <asp:TextBox ID="tarVdo" TextMode="MultiLine" runat="server"  Width="730px"  Height="55px" ></asp:TextBox> 
                    <br />
                 <p class="star1">   ใส่แท๊ก iframe จาก YouTube  โดยให้กำหนดขนาดแผนที่เป็น 560 * 315 px </p>
                     
            </tr> 
           <%}  %>   




            <tr>
                <td class="label" valign="top">รายละเอียดเต็ม :</td>
                <td>

                    <%-- <textarea id="txaConDetailLong" name="txaDetailLong" rows="10" cols="80"></textarea>--%>
                    <asp:TextBox ID="tarDetailLong" TextMode="MultiLine" runat="server"></asp:TextBox>


                </td>
            </tr>

            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="label">&nbsp;</td>
                <td>
                    <asp:Button ID="btnSave" runat="server" Text="บันทึก" OnClick="btnSave_Click"   CssClass="btn-default"
                        Style="margin-right: 10px;" />
                    <asp:Button ID="btnClear" runat="server" Text="ยกเลิก" CssClass="btn-default"  UseSubmitBehavior="false"   OnClientClick="history.back();return false;"  />
                </td>
            </tr>
        </table>
    </div>
    <!-- end id="page-prdAdd" -->

</asp:Content>

