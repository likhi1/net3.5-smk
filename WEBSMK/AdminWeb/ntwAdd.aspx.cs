﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization; 

public partial class adminWeb_branchAdd : System.Web.UI.Page
{  
     // protected  string Cat = HttpContext.Current.Request.QueryString["cat"];
    protected int CatId { 
        get { 
                int cat ;
                   // =  int.Parse(Request.QueryString["cat"]) ; 
                if( string.IsNullOrEmpty(Request.QueryString["cat"])  ) {
                    cat =  1;
                }else{
                    cat = int.Parse(Request.QueryString["cat"]);
                }
                return  cat ;

            }
    }
   
    protected string PageName;
    protected DataTable DtCat;

    protected void Page_Load(object sender, EventArgs e) {
        if (!Page.IsPostBack) {
          
            SelectCat();
            ddlProvinceBindData();
            ddlBranchTypeBindData(CatId);
            ddlAmphoeBindData();
            ddlCatBindData();
            /////// Set Pagename
            GetNamePage();
            lblPageName.Text = PageName;

            //======= set read only on Cat DropDownlist
            ddlCat.SelectedValue = CatId.ToString();
            ddlCat.Attributes.Add("disabled", "true");  
        }
    }

    protected void ddlCatBindData() {
        string sql = "SELECT * FROM tbNtwCat  ORDER BY NtwCatId ";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "_tbNtwCaT");

        ddlCat.DataSource = ds;
        //**** Add frist Item of dropdownlist
        ddlCat.AppendDataBoundItems = true;
        ddlCat.DataTextField = "NtwCatName";
        ddlCat.DataValueField = "NtwCatId";
        ddlCat.DataBind();

        //  ddlCat.SelectedValue = CatId;


    }

    //======================  Set DropDown 
    protected DataTable SelectCat() {
        string sql = "SELECT  NtwCatId  ,  NtwCatName  FROM tbNtwCat  ORDER BY NtwCatId ";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "_tb");
        DtCat = ds.Tables[0];
        return DtCat;
    }

    protected void ddlProvinceBindData() {
        string sql = "SELECT ds_major_cd , RTRIM(ds_desc_t) AS ds_desc_t  FROM setcode  WHERE (ds_major_cd = 'CH')  Order by ds_desc_t ";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "tb");

        ddlProvince.DataSource = ds;
        //**** Add frist Item of dropdownlist
        ddlProvince.AppendDataBoundItems = true;
        ddlProvince.DataTextField = "ds_desc_t";
        //ddlProvince.DataValueField = "ds_desc_t";
        ddlProvince.DataBind();

    }

    protected void ddlAmphoeBindData() {
        string sql = "SELECT * FROM setcode  WHERE (ds_major_cd = 'AM') AND (ds_desc = '0') ";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "tb");

        ddlAmphoe.DataSource = ds;
        //**** Add frist Item of dropdownlist
        ddlAmphoe.AppendDataBoundItems = true;
        ddlAmphoe.DataTextField = "ds_desc_t";
       // ddlAmphoe.DataValueField = "ds_desc_t";
        ddlAmphoe.DataBind();

    }

    protected void ddlBranchTypeBindData(int CatId)
    {
        string sql = "SELECT * FROM tbNtwType WHERE NtwTypeCat='" + CatId + "'  ORDER BY NtwTypeId";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "_tb");

        ddlNetworkType.DataSource = ds;
        //**** Add frist Item of dropdownlist
        ddlNetworkType.AppendDataBoundItems = true;
        ddlNetworkType.DataTextField = "NtwTypeNameTh";
        ddlNetworkType.DataValueField = "NtwTypeId";
        ddlNetworkType.DataBind();
    }

    //=======================  Set PageName
    protected string GetNamePage() {
        foreach (DataRow r in DtCat.Rows) {
            if ((int)r[0] == CatId) {
                PageName = r[1].ToString();
            }
        }
        return PageName;
    }

   //======================== Set  Event 
    protected void btnSave_Click(object sender, EventArgs e) {

        string sql1 = "INSERT INTO tbNtw "
            + "( NtwZoneId, NtwCatId, NtwName, NtwAddress, NtwTelPhone, NtwTelMobile, NtwProvince, NtwEmail,  NtwNotation, "
            + " NtwMap, NtwPic1, NtwPic2, NtwStatus, NtwSort, NtwDateTime,  NtwNumCode,  NtwTypeId, NtwAmphoe, NtwService, lang , input_date ,input_user, "
            + " NtwWebsite,  NtwFax , NtwMapLink , Spare2 , Spare3 , Spare4 , Spare5 )"

            + " VALUES "
            +"( @NtwZoneId,  @NtwCatId,  @NtwName,  @NtwAddress, @NtwTelPhone,  @NtwTelMobile,  @NtwProvince,  @NtwEmail,  @NtwNotation, "
            + " @NtwMap, @NtwPic1, @NtwPic2, @NtwStatus, @NtwSort, @NtwDateTime,  @NtwNumCode, @NtwTypeId, @NtwAmphoe, @NtwService, @lang , @input_date ,@input_user, "
            + " @NtwWebsite,   @NtwFax , @NtwMapLink , @Spare2 , @Spare3 , @Spare4 , @Spare5 )";
         
        Decimal decSort;  
         
        SqlParameterCollection objParam1 =   new SqlCommand().Parameters;
        objParam1.AddWithValue("@NtwZoneId", ucZone.StrZoneName);// Get Value from User Control ( เรียกโดย IdUserControl.FieldnameUserControl )     
        objParam1.AddWithValue("@NtwCatId", CatId);
        objParam1.AddWithValue("@NtwName", tbxName.Text);
        objParam1.AddWithValue("@NtwAddress", tbxAddress.Text);
        objParam1.AddWithValue("@NtwTelPhone", tbxTelPhone.Text);
        objParam1.AddWithValue("@NtwTelMobile", tbxTelMobile.Text);
        objParam1.AddWithValue("@NtwProvince", ddlProvince.SelectedValue);
        objParam1.AddWithValue("@NtwEmail", tbxEmail.Text);
        objParam1.AddWithValue("@NtwNotation", tbxNotation.Text);
        objParam1.AddWithValue("@NtwMap", tbxMap.Text);
        objParam1.AddWithValue("@NtwPic1", tbxPic1.Text );
        objParam1.AddWithValue("@NtwPic2", tbxPic2.Text);
        objParam1.AddWithValue("@NtwStatus", rblStatus.SelectedValue);
        objParam1.AddWithValue("@NtwSort",  (decSort = (tbxSort.Text != "") ? Convert.ToDecimal(tbxSort.Text) : 9999)  );
        objParam1.AddWithValue("@NtwDateTime", DateTime.Today );
        objParam1.AddWithValue("@NtwAmphoe", ddlAmphoe.SelectedValue);
        objParam1.AddWithValue("@NtwService", tbxService.Text);
        //=========== AdminInfo Add 
        objParam1.AddWithValue("@lang", '0'); // TH = 0 ,  En =  1
        objParam1.AddWithValue("@input_date", DateTime.Now);

        //===========  Get UserName From Session Object 
        ProfileData loginData = (ProfileData)Session["SessAdmin"];
        objParam1.AddWithValue("@input_user", loginData.loginName);
         
        //=========== Increase Field
        string strWebsite1  =  tbxWebsite.Text ;
        string strWebsite2 = (!strWebsite1.Contains("http://"))?"http://" + strWebsite1:  strWebsite1 ;
        objParam1.AddWithValue("@NtwWebsite", strWebsite2); 

        objParam1.AddWithValue("@NtwFax", tbxNtwFax.Text);
        objParam1.AddWithValue("@NtwMapLink", tbxMapLink.Text);
        objParam1.AddWithValue("@Spare2", "");
        objParam1.AddWithValue("@Spare3", "");
        objParam1.AddWithValue("@Spare4", "");
        objParam1.AddWithValue("@Spare5", "");
         
        //=========== Check Insert From Category
        if (CatId == 4) { // Catagory 4 =  Branch  ****
            objParam1.AddWithValue("@NtwNumCode", tbxNumCode.Text);
            objParam1.AddWithValue("@NtwTypeId", ddlNetworkType.SelectedValue);
        } else  {// Catagory 2 =  Garage  ****
            objParam1.AddWithValue("@NtwNumCode", "");
            objParam1.AddWithValue("@NtwTypeId", ddlNetworkType.SelectedValue);
        } 
         

        int i1 = new DBClass().SqlExecute(sql1, objParam1);

        if (i1 == 1) {
            Response.Write("<script>alert('เพิ่มข้อมูลเรียบร้อยแล้ว'); location='ntwList.aspx?cat=" + CatId + "' ;</script>"); 
        } else {
            Response.Write("<script>alert('ไม่สามารถเพิ่มข้อมูลได้'); history.back() ;</script>");
           
        }
         

    }

     
}