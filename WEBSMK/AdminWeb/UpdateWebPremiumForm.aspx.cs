﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


public partial class AdminWeb_UpdateWebPremiumForm : System.Web.UI.Page
{
    public string[] ColumnName = { "", "campaign", "car_cod", "pol_typ", "car_age", 
                                     "car_group", "car_mak", "car_size", "car_body", "agt_cod",
                                     "car_od", "car_driver", "start_dte", "end_dte", "pre_grsgar",
                                     "pre_grsservice"};
    public string[] ColumnType = { "string", "string", "string", "string", "string", 
                                     "string", "string", "string", "string", "string",
                                     "string", "string", "string", "string", "string",
                                     "string"};
    public string[] ColumnTitle = { "No.", "Cam paign", "รหัสรถ", "ประเภท", "อายุรถ", 
                                    "กลุ่มรถ", "car mak", "ขนาด", "body", "ตัวแทน",
                                    "ทุนประกัน", "อายุผู้ขับขี่", "วันเริ่ม", "วันสิ้นสุด", "เบี้ยอู่",
                                    "เบี้ยห้าง"};
    string[] ColumnWidth = { "3%", "5%", "5%", "5%", "5%",
                               "5%", "5%", "5%", "5%", "5%",
                               "10%", "5%", "10%", "10%", "9%",
                                "8%" };
    string[] ColumnAlign = { "center", "center", "center", "center", "center",
                                "center", "center", "right", "center", "left",
                                "right", "right", "center", "center", "right",
                                "right"};

    public string tranPicUrl = "";
    DataView dv = new DataView();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initialDropdownList();
            initialDataGrid();
        }
        else
        {
            litScript.Text = "";
        }
    }    
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        DataSet ds;
        PremiumManager cmPrem = new PremiumManager();
        ds = cmPrem.searchDataOnlineWeb(ddlProduct.SelectedValue, ddlCarCode.SelectedValue,
                                        ddlPolType.SelectedValue, txtFromCarAge.Text,
                                        txtToCarAge.Text, ddlCarGroup.SelectedValue,
                                        txtCarMak.Text, txtCarSize.Text,
                                        txtCarBody.Text, txtAgentCode.Text,
                                        txtFromSumInsured.Text, txtToSumInsured.Text,
                                        ddlDriverAge.SelectedValue,
                                        txtFromStartDate.Text, txtToStartDate.Text,
                                        txtFromStopDate.Text, txtToStopDate.Text);
        ViewState.Remove("SortDirection");
        ViewState.Remove("SortString");
        ViewState.Remove("SortIndex");
        ViewState.Add("DSMasterTable", ds);
        ShowData();

        litScript.Text += "<script>";
        litScript.Text += " document.getElementById('divResult').style.display = 'inline';";
        litScript.Text += " location.hash = '#aDtgData';\n";
        litScript.Text += "</script>";
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {

    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        PremiumManager cmPrem = new PremiumManager();
        bool isSave = true;
        isSave = cmPrem.updatStartEndDateeDataOnlineWeb(ddlProduct.SelectedValue, ddlCarCode.SelectedValue,
                                        ddlPolType.SelectedValue, txtFromCarAge.Text,
                                        txtToCarAge.Text, ddlCarGroup.SelectedValue,
                                        txtCarMak.Text, txtCarSize.Text,
                                        txtCarBody.Text, txtAgentCode.Text,
                                        txtFromSumInsured.Text, txtToSumInsured.Text,
                                        ddlDriverAge.SelectedValue,
                                        txtFromStartDate.Text, txtToStartDate.Text,
                                        txtFromStopDate.Text, txtToStopDate.Text,
                                        txtUpdateStartDate.Text, txtUpdateStopDate.Text);
        if (isSave)
        {
            litScript.Text += "<script>";
            litScript.Text += " alert('Save complete');";
            litScript.Text += "</script>";
        }
        else
        {
            litScript.Text += "<script>";
            litScript.Text += " alert('Error');";
            litScript.Text += "</script>";
        }
    }
    protected void PageIndexChanged(Object source, DataGridPageChangedEventArgs e)
    {
        DataView dv = new DataView();
        DataSet ds = new DataSet();
        if (!(ViewState["DSMasterTable"] == null))
        {
            ds = (DataSet)ViewState["DSMasterTable"];
            if (e.NewPageIndex > dtgData.PageCount - 1)
                dtgData.CurrentPageIndex = dtgData.PageCount - 1;
            else
                dtgData.CurrentPageIndex = e.NewPageIndex;

            String sortBy = "";
            if (!(ViewState["DSMasterTable"] == null))
            {
                sortBy = (string)(ViewState["SortString"]);
            }
            dv.Table = ds.Tables[0];
            dv.Sort = sortBy;
            ShowData();
        }
    }

    public void initialDropdownList()
    {
        DataSet ds;
        VoluntaryManager cm = new VoluntaryManager();
        PremiumManager cmPrem = new PremiumManager();
        ds = cm.getAllDataProduct();
        ddlProduct.DataSource = ds;
        ddlProduct.DataTextField = "name";
        ddlProduct.DataValueField = "campaign";
        ddlProduct.DataBind();
        ddlProduct.Items.Insert(0, new ListItem(" - เลือก - ", ""));

        ds = cmPrem.getDistinctCarCode();
        ddlCarCode.DataSource = ds;
        ddlCarCode.DataTextField = "car_cod";
        ddlCarCode.DataValueField = "car_cod";
        ddlCarCode.DataBind();
        ddlCarCode.Items.Insert(0, new ListItem(" - เลือก - ", ""));

        ds = cmPrem.getDistinctPolType();
        ddlPolType.DataSource = ds;
        ddlPolType.DataTextField = "pol_typ";
        ddlPolType.DataValueField = "pol_typ";
        ddlPolType.DataBind();
        ddlPolType.Items.Insert(0, new ListItem(" - เลือก - ", ""));

        ds = cmPrem.getDistinctCarGroup();
        ddlCarGroup.DataSource = ds;
        ddlCarGroup.DataTextField = "car_group";
        ddlCarGroup.DataValueField = "car_group";
        ddlCarGroup.DataBind();
        ddlCarGroup.Items.Insert(0, new ListItem(" - เลือก - ", ""));

        ds = cmPrem.getDistinctDriverAge();
        ddlDriverAge.DataSource = ds;
        ddlDriverAge.DataTextField = "car_driver";
        ddlDriverAge.DataValueField = "car_driver";
        ddlDriverAge.DataBind();
        ddlDriverAge.Items.Insert(0, new ListItem(" - เลือก - ", ""));
    }
    public void initialDataGrid()
    {
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        int iPageSize = 0;
        dt = TDS.Utility.MasterUtil.AddColumnToDataTable(dt, ColumnType, ColumnName);
        ds.Tables.Add(dt);
        ViewState.Remove("SortDirection");
        ViewState.Remove("SortString");
        ViewState.Remove("SortIndex");
        ViewState.Add("DSMasterTable", ds);
        ShowData();
        GridViewUtil.setGridStyle(dtgData, ColumnWidth, ColumnAlign, ColumnTitle);
        try
        {
            iPageSize = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["pageSize"].ToString());
        }
        catch (Exception e)
        {
            iPageSize = 10;
        }
        dtgData.PageSize = iPageSize;
    }
    public void ShowData()
    {
        DataSet ds = new DataSet();
        if (ViewState["DSMasterTable"] != null)
            ds = (DataSet)ViewState["DSMasterTable"];

        string sortBy = "";
        if (ViewState["SortString"] != null)
            sortBy = (String)ViewState["SortString"];

        if (ds.Tables.Count > 0)
        {
            dv.Table = ds.Tables[0];
            dv.Sort = sortBy;

            int newPageCount = (Int32)(Math.Ceiling((double)dv.Table.Rows.Count / (double)dtgData.PageSize));
            if (dtgData.CurrentPageIndex >= newPageCount && newPageCount > 0)
                dtgData.CurrentPageIndex = newPageCount - 1;
        }
        dtgData.DataSource = dv;
        dtgData.DataBind();

        GridViewUtil.ItemDataBound(dtgData.Items);

        ucPageNavigator1.dvData = ds.Tables[0].DefaultView;
        ucPageNavigator1.dtgData = dtgData;
    }
    public void sortColumn(object Sender, EventArgs e)
    {
        string[] SortDirection = new string[ColumnTitle.Length];
        for (int i = 0; i < SortDirection.Length; i++)
        {
            SortDirection[i] = "";
        }
        if (ViewState["SortDirection"] != null)
            SortDirection = (string[])ViewState["SortDirection"];

        LinkButton lb = (LinkButton)Sender;
        string sortBy = "";
        int index = 0;
        while ((index < ColumnName.Length) && !((lb.Text).Equals(ColumnTitle[index])))
        {
            index++;
        }
        sortBy = ("" + ColumnName[index] + " " + SortDirection[index]);
        SortDirection[index] = (SortDirection[index].Equals("") ? "DESC" : "");
        ViewState.Add("SortDirection", SortDirection);
        ViewState.Add("SortString", sortBy);
        ViewState.Add("SortIndex", index);
        ShowData();
    }
    public string getPicUrl(int colNo)
    {
        string returnValue = Request.ApplicationPath + "/Images/tran.gif";
        if (ViewState["SortIndex"] != null)
        {
            DataGridItem dgi = (DataGridItem)(dtgData.Controls[0].Controls[1]);

            string[] SortDirection = new string[Title.Length];
            SortDirection = (string[])ViewState["SortDirection"];

            if ((int)(ViewState["SortIndex"]) == colNo)
            {
                returnValue = Request.ApplicationPath + "/Images/" + (SortDirection[colNo].Equals("") ? "sortDown.gif" : "sortUp.gif");
            }
        }
        return returnValue;
    }
}
