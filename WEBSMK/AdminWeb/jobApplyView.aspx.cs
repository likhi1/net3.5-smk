﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---------- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;
using System.Collections;

public partial class adminWeb_jobApplyView : System.Web.UI.Page {

    protected string _catId;
    protected string CatId { get { _catId = Request.QueryString["cat"]; return _catId; } }
    protected string ConId { get { return Request.QueryString["id"]; } }

    protected DataTable dtPosition { get; set; } // Decare Global Varibal  For Use in Posi
    protected DataTable Posi { get { DataTable dt = dtPosition; return dt; } }

  

    protected void Page_Load(object sender, EventArgs e) {  
        if (!Page.IsPostBack) {
            SelectPosition();
            BindData();
        }
    } 

    void BindData() {

        if( Convert.ToInt32(CatId) == 1){
            FormView1.DataSource = SelectData();
            FormView1.DataBind();  

           // ManualDataBound1();

        }else{
            FormView2.DataSource = SelectData();
            FormView2.DataBind();  
        }
    }



    protected DataSet SelectData() {
        string sql = "SELECT * FROM  tbJobPrf  WHERE  JobCatId ='" + CatId + "' "
                   + "AND JobPrfId='" + ConId + "'  ";

        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql, "tb");
        return ds;
    }
     

    //================== Position Manage
    protected DataTable SelectPosition() {
        string sql = "Select JobPosiId ,JobPosiName  FROM tbJobPosi WHERE JobCatId= '" + CatId + "'    ORDER BY  JobPosiId DESC  ";

        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql, "myTb");
          dtPosition = ds.Tables[0];
        return dtPosition ;
    }
   
    string posiName;
  
    protected string SetPositionName(string index ) {
        foreach (DataRow r in  Posi.Rows ) {
            if ( r[0].ToString()  ==   index ){
                posiName = r[1].ToString() ; //new Utility().SetPositionName(); 
              }
        }
        return posiName;
    }
 


    protected void FormView1_DataBound(object sender, EventArgs e) {


        Label lblId = (Label)FormView1.FindControl("lblId");
        if (lblId != null) {
            int JobPrfId = Convert.ToInt32(DataBinder.Eval(FormView1.DataItem, "JobPrfId"));
            string strId = String.Format("{0:0000}", JobPrfId);
            lblId.Text = strId;
        }

        Label lblDateReg = (Label)FormView1.FindControl("lblDateReg");
        if (lblDateReg != null) {
            DateTime dtDateReg = Convert.ToDateTime(DataBinder.Eval(FormView1.DataItem, "JobPrfDateReg"));
            
            string strDate = dtDateReg.ToString("d MMMM yyyy");
            string strTime = dtDateReg.ToString("HH:mm")+" น.";

            lblDateReg.Text = strDate + " (" + strTime + ")";

        }
         

        //Label lblPosition = (Label)FormView1.FindControl("lblPosition");
        //if (lblPosition != null) {
        //    string strPosition = Convert.ToString(DataBinder.Eval(FormView1.DataItem, "JobPrfPosition"));
        //    string[] arrPosition = strPosition.Split('|'); 
           
        //    if (arrPosition.Length > 1) {
        //        lblPosition.Text = "";
        //        if (arrPosition[0] != "") { lblPosition.Text += "<p>1. " + SetPositionName(arrPosition[0]) + "</p>"; }
        //        if (arrPosition[1] != "") { lblPosition.Text += "<p>2. " + SetPositionName(arrPosition[1]) + "</p>"; }
        //        if (arrPosition[2] != "") { lblPosition.Text += "<p>3. " + SetPositionName(arrPosition[2]) + "</p>"; }
        //    } else {
        //        lblPosition.Text = SetPositionName(arrPosition[0]);
        //    }
             
        //}

        Label lblPosition = (Label)FormView1.FindControl("lblPosition");
        if (lblPosition != null) {
          
        string strPosition1 = DataBinder.Eval(FormView1.DataItem, "JobPrfPosition1").ToString();
        string strPosition2 = DataBinder.Eval(FormView1.DataItem, "JobPrfPosition2").ToString();
        string strPosition3 = DataBinder.Eval(FormView1.DataItem, "JobPrfPosition3").ToString();

        lblPosition.Text = !string.IsNullOrEmpty(strPosition1) ? "<p>1. " + SetPositionName(strPosition1) + "</p>" : "";
        lblPosition.Text += !string.IsNullOrEmpty(strPosition2) ? "<p>2. " + SetPositionName(strPosition2) + "</p>" : "";
        lblPosition.Text += !string.IsNullOrEmpty(strPosition3) ? "<p>3. " + SetPositionName(strPosition3) + "</p>" : "";
         
        }


        Label lblSalary = (Label)FormView1.FindControl("lblSalary");
        if (lblSalary != null) {
            string strSalary = Convert.ToString(DataBinder.Eval(FormView1.DataItem, "JobPrfSalary"));
            lblSalary.Text =  (!string.IsNullOrEmpty( strSalary)? strSalary+" บาท" :"" ) ;
     //  lblTopic.Text = ((strTopic.Length) <= 50) ? strTopic : strTopic.Substring(0, 50) + "...";
          }

            Label lblName = (Label)FormView1.FindControl("lblName");
            if (lblName != null) {
             
               string   strPrfSex= Convert.ToString(DataBinder.Eval(FormView1.DataItem, "JobPrfSex"));  
            string   strName1 = Convert.ToString(DataBinder.Eval(FormView1.DataItem, "JobPrfName1"));
            string   strName2 = Convert.ToString(DataBinder.Eval(FormView1.DataItem, "JobPrfName2"));
                 
            lblName.Text = strPrfSex + " " + strName1 + " " + strName2 ;
            }

            Label lblBirth = (Label)FormView1.FindControl("lblBirth");
            if (lblBirth != null) {
              DateTime  dtBirth = Convert.ToDateTime(DataBinder.Eval(FormView1.DataItem, "JobPrfBirth"));
              string strDate = dtBirth.ToString("d MMMM yyyy" , new CultureInfo("th-TH") );
              lblBirth.Text = strDate;
            }

            Label lblAge = (Label)FormView1.FindControl("lblAge");
            if (lblAge != null) {
                
               string   JobPrfAge =  Convert.ToString(DataBinder.Eval(FormView1.DataItem, "JobPrfAge"));
                lblAge.Text = ( !string.IsNullOrEmpty( JobPrfAge )?JobPrfAge +" ปี": "")  ;
            }

            Label lblIdCard = (Label)FormView1.FindControl("lblIdCard");
            if (lblIdCard != null) {
                lblIdCard.Text = Convert.ToString(DataBinder.Eval(FormView1.DataItem, "JobPrfIdCard")); 
            }


            Label lblNationality = (Label)FormView1.FindControl("lblNationality");
            if (lblNationality != null) {
                lblNationality.Text = Convert.ToString(DataBinder.Eval(FormView1.DataItem, "JobPrfNationality"));
            }
            Label lblReligion = (Label)FormView1.FindControl("lblReligion");
            if (lblReligion != null) {
                lblReligion.Text = Convert.ToString(DataBinder.Eval(FormView1.DataItem, "JobPrfReligion"));
            }
         

            Label lblWeight = (Label)FormView1.FindControl("lblWeight");
            if (lblWeight != null) {
                lblWeight.Text = Convert.ToString(DataBinder.Eval(FormView1.DataItem, "JobPrfWeight"));
            }
         
            Label lblHeight = (Label)FormView1.FindControl("lblHeight");
            if (lblHeight != null) {
                lblHeight.Text = Convert.ToString(DataBinder.Eval(FormView1.DataItem, "JobPrfHeight"));
            }
         
            Label lblMilitary = (Label)FormView1.FindControl("lblMilitary");
            if (lblMilitary != null) { 
                 string strMilitaly =  Convert.ToString(DataBinder.Eval(FormView1.DataItem, "JobPrfMilitary"));
                 lblMilitary.Text = (Convert.ToInt32(strMilitaly) == 0 ? "ยังไม่พ้น" : "พ้นแล้ว");
            }

            Label lblPrfTel1 = (Label)FormView1.FindControl("lblPrfTel1");
            if (lblPrfTel1 != null) {
                string JobPrfTel1 = Convert.ToString(DataBinder.Eval(FormView1.DataItem, "JobPrfTel1")); 
                lblPrfTel1.Text = JobPrfTel1  ;
            }

            Label lblPrfTel2= (Label)FormView1.FindControl("lblPrfTel2");
            if (lblPrfTel2 != null) { 
                string JobPrfTel2 = Convert.ToString(DataBinder.Eval(FormView1.DataItem, "JobPrfTel2"));
                lblPrfTel2.Text = JobPrfTel2;
            }
         
            Label lblPrfEmail = (Label)FormView1.FindControl("lblPrfEmail");
            if (lblPrfEmail != null) {
                lblPrfEmail.Text = Convert.ToString(DataBinder.Eval(FormView1.DataItem, "JobPrfEmail"));
            } 

            Label lblAddress1 = (Label)FormView1.FindControl("lblAddress1");
            if (lblAddress1 != null) {
                lblAddress1.Text = Convert.ToString(DataBinder.Eval(FormView1.DataItem, "JobPrfAddress1"));
            }

            Label lblAddress2 = (Label)FormView1.FindControl("lblAddress2");
            if (lblAddress2 != null) {
                lblAddress2.Text = Convert.ToString(DataBinder.Eval(FormView1.DataItem, "JobPrfAddress2"));
            } 

         Label lblEduAll = (Label)FormView1.FindControl("lblEduAll");
            if (lblEduAll != null) {
                string  strEdu1  = Convert.ToString(DataBinder.Eval(FormView1.DataItem, "JobPrfEdu1"));
                string  strEdu2 = Convert.ToString(DataBinder.Eval(FormView1.DataItem, "JobPrfEdu2"));
                string  strEdu3  = Convert.ToString(DataBinder.Eval(FormView1.DataItem, "JobPrfEdu3")); 
                string [] arrEdu1  = strEdu1.Split('|');
                string [] arrEdu2  = strEdu2.Split('|');
                string [] arrEdu3  = strEdu3.Split('|');

                lblEduAll.Text =  "" 
                           + (!string.IsNullOrEmpty(arrEdu1[0]) ? arrEdu1[0] + " " : "")
                           + (!string.IsNullOrEmpty(arrEdu1[1]) ? "(" + arrEdu1[1] + ") " : "") + "<br>"
                           + (!string.IsNullOrEmpty(arrEdu1[2]) ? "วุฒิ " + arrEdu1[2] + " / " : "")
                           + (!string.IsNullOrEmpty(arrEdu1[3]) ? "วิชาเอก " + arrEdu1[3] + " / " : "")
                           + (!string.IsNullOrEmpty(arrEdu1[4]) ? "เกรดเฉลี่ย " + arrEdu1[4] + " / " : "")
                           + (!string.IsNullOrEmpty(arrEdu1[5]) ? "ปีจบ " + arrEdu1[5] : "")
        + "<div style='border-bottom:1px dashed #ddd; width:100%; padding-top: 5px ;margin-bottom:5px;' ></div>"

                           + (!string.IsNullOrEmpty(arrEdu2[0]) ? arrEdu2[0] + " " : "")
                           + (!string.IsNullOrEmpty(arrEdu2[1]) ? "(" + arrEdu2[1] + ") " : "") + "<br>"
                           + (!string.IsNullOrEmpty(arrEdu2[2]) ? "วุฒิ " + arrEdu2[2] + " / " : "") 
                           + (!string.IsNullOrEmpty(arrEdu2[3]) ? "วิชาเอก " + arrEdu2[3] + " / " : "")  
                           + (!string.IsNullOrEmpty(arrEdu2[4]) ? "เกรดเฉลี่ย " + arrEdu2[4] + " / " : "")  
                           +  (!string.IsNullOrEmpty(arrEdu2[5]) ? "ปีจบ " + arrEdu2[5] : "")
          + "<div style='border-bottom:1px dashed  #ddd; width:100%; padding-top: 5px ;margin-bottom:5px;' ></div>"

                          + (!string.IsNullOrEmpty(arrEdu3[0]) ? arrEdu3[0] : "") + " "
                          + (!string.IsNullOrEmpty(arrEdu3[1]) ? "(" + arrEdu3[1] + ") " : "")  +"<br>"
                          + (!string.IsNullOrEmpty(arrEdu3[2]) ? "วุฒิ " + arrEdu3[2]+ " / " : "") 
                          + (!string.IsNullOrEmpty(arrEdu3[3]) ? "วิชาเอก " + arrEdu3[3]+ " / " : "") 
                          + (!string.IsNullOrEmpty(arrEdu3[4]) ? "เกรดเฉลี่ย " + arrEdu3[4]+ " / " : "") 
                          + (!string.IsNullOrEmpty(arrEdu3[5]) ? "ปีที่จบ " + arrEdu3[5] : "")
         + "<div style='border-bottom:1px dashed  #ddd; width:100%; padding-top: 5px ;margin-bottom:5px;' ></div>";
            
            }

       
         Label lblEduActivity = (Label)FormView1.FindControl("lblEduActivity");
            if (lblEduActivity != null) {
                lblEduActivity.Text = Convert.ToString(DataBinder.Eval(FormView1.DataItem, "JobPrfEduActivity"));
            }


Label lblPastWork = (Label)FormView1.FindControl("lblPastWork");
if (lblPastWork != null) {
    string strPastWork1 = Convert.ToString(DataBinder.Eval(FormView1.DataItem, "JobPrfPastWork1"));
    string strPastWork2 = Convert.ToString(DataBinder.Eval(FormView1.DataItem, "JobPrfPastWork2"));
    string[] arrPw1 = strPastWork1.Split('|');
    string[] arrPw2 = strPastWork2.Split('|');

    //lblPastWork.Text = (!string.IsNullOrEmpty(arrPw1[0]) ? "บริษัท : " + arrPw1[0] : "") + "<br> "
    //            + "<p>"+(!string.IsNullOrEmpty(arrPw1[2]) ? "ตำแหน่ง : " + arrPw1[2]  : "") + "</p> "
    //            + "<p>" + (!string.IsNullOrEmpty(arrPw1[3]) ? "ระยะเวลา : " + arrPw1[3] + " ปี" : "") + "</p>"
    //            + "<p>" + (!string.IsNullOrEmpty(arrPw1[4]) ? "เงินเดือน : " + arrPw1[4] + " บาท" : "") + "</p> "
    //            + "<p>" + (!string.IsNullOrEmpty(arrPw1[5]) ? "หน้าที่ : " + arrPw1[5] : "") + "<p>  "
    //            + "<p>" + (!string.IsNullOrEmpty(arrPw1[1]) ? "ที่อยู่ : " + arrPw1[1] : "") + " </p>  "
    //            + "<p>"+ (!string.IsNullOrEmpty(arrPw1[6]) ? "สาเหตุที่ออก : " + arrPw1[6] : "") + " </p>  "
    //            + "<div style='border-bottom:1px solid #ddd; width:100%; padding-top: 5px ;margin-bottom:5px;' ></div>"

    //            + "<p>" + (!string.IsNullOrEmpty(arrPw2[0]) ? "บริษัท : " + arrPw2[0] : "") + "<br> "
    //            + "<p>" + (!string.IsNullOrEmpty(arrPw2[2]) ? "ตำแหน่ง : " + arrPw2[2] : "") + "</p> "
    //            + "<p>" + (!string.IsNullOrEmpty(arrPw2[3]) ? "ระยะเวลา : " + arrPw2[3] + " ปี" : "") + "</p> "
    //            + "<p>" + (!string.IsNullOrEmpty(arrPw2[4]) ? "เงินเดือน : " + arrPw2[4] + " บาท" : "") + " </p> "
    //            + "<p>" + (!string.IsNullOrEmpty(arrPw2[5]) ? "หน้าที่ : " + arrPw2[5] : "") + " </p>  "
    //            + "<p>" + (!string.IsNullOrEmpty(arrPw2[1]) ? "ที่อยู่ : " + arrPw2[1] : "") + " </p> "
    //            + "<p>" + (!string.IsNullOrEmpty(arrPw2[6]) ? "สาเหตุที่ออก : " + arrPw2[6] : "") + " </p> "
    // + "<div style='border-bottom:1px solid #ddd; width:100%; padding-top: 5px ;margin-bottom:5px;' ></div>";

    lblPastWork.Text =
    "<table   id='tbWorkXp'>"
    + "<tr>"
    + "<td class='label'> บริษัท :</td>"
    + "<td   >" + (!string.IsNullOrEmpty(arrPw1[0]) ? arrPw1[0] : "") + "</td>"
    + "<td class='label' > บริษัท :</td>"
    + "<td  >" + (!string.IsNullOrEmpty(arrPw2[0]) ? arrPw2[0] : "") + "</td>"
    + "</tr>"

    + "<tr>"
    + "<td class='label'> ตำแหน่ง : </td>"
    + "<td>" + (!string.IsNullOrEmpty(arrPw1[2]) ? arrPw1[2] : "") + "</td>"
    + "<td class='label'> ตำแหน่ง : </td>"
    + "<td>" + (!string.IsNullOrEmpty(arrPw2[2]) ? arrPw2[2] : "") + "</td>"
    + "</tr>"

    + "<tr>"
    + "<td class='label'> ปีที่ทำ:</td>"
    + "<td>" + (!string.IsNullOrEmpty(arrPw1[3]) ? arrPw1[3] : "") + "</td>"
    + "<td class='label'> ปีที่ทำ:</td>"
    + "<td>" + (!string.IsNullOrEmpty(arrPw2[3]) ? arrPw2[3] : "") + "</td>"
    + "</tr>"

    + "<tr>"
    + "<td class='label'> เงินเดือน :</td>"
    + "<td>" + (!string.IsNullOrEmpty(arrPw1[4]) ? arrPw1[4] + " บาท" : "") + "</td>"
    + "<td class='label'> เงินเดือน :</td>"
    + "<td>" + (!string.IsNullOrEmpty(arrPw2[4]) ? arrPw2[4] + " บาท" : "") + "</td>"
    + "</tr>"

    + "<tr>"
    + "<td class='label'>หน้าที่ :</td>"
    + "<td>" + (!string.IsNullOrEmpty(arrPw1[5]) ? arrPw1[5] : "") + "</td>"
    + "<td class='label'>หน้าที่ :</td>"
    + "<td>" + (!string.IsNullOrEmpty(arrPw2[5]) ? arrPw2[5] : "") + "</td>"
    + "</tr>"

    + "<tr>"
    + "<td class='label'>ที่อยู่ :</td>"
    + "<td>" + (!string.IsNullOrEmpty(arrPw1[1]) ? arrPw1[1] : "") + "</td>"
    + "<td class='label'>ที่อยู่ :</td>"
    + "<td>" + (!string.IsNullOrEmpty(arrPw2[1]) ? arrPw2[1] : "") + "</td>"
    + "</tr>"

    + "<tr>"
    + "<td class='label'>สาเหตุที่ออก :</td>"
    + "<td>" + (!string.IsNullOrEmpty(arrPw1[6]) ? arrPw1[6] : "") + "</td>"
    + "<td class='label'>สาเหตุที่ออก :</td>"
    + "<td>" + (!string.IsNullOrEmpty(arrPw2[6]) ? arrPw2[6] : "") + "</td>"
    + "</tr>" 

    + "</table >"; 

}


Label lblSkLanguage = (Label)FormView1.FindControl("lblSkLanguage");
if (lblSkLanguage != null) {
    string strSkLanguage1 = Convert.ToString(DataBinder.Eval(FormView1.DataItem, "JobPrfSkLang1"));
    string strSkLanguage2 = Convert.ToString(DataBinder.Eval(FormView1.DataItem, "JobPrfSkLang2"));
    string[] arrSkLang1 = strSkLanguage1.Split('|');
    string[] arrSkLang2 = strSkLanguage2.Split('|');

    lblSkLanguage.Text = (!string.IsNullOrEmpty(arrSkLang1[0]) ? "" + arrSkLang1[0] : "") + "<br>"
                         + (!string.IsNullOrEmpty(arrSkLang1[1]) ? "การฟัง " + arrSkLang1[1] : "") + " / "
                         + (!string.IsNullOrEmpty(arrSkLang1[2]) ? "การพูด " + arrSkLang1[2] : "") + " / "
                         + (!string.IsNullOrEmpty(arrSkLang1[3]) ? "การอ่าน " + arrSkLang1[3] : "") + " / "
                         + (!string.IsNullOrEmpty(arrSkLang1[4]) ? "การเขียน " + arrSkLang1[4] : "")
           + "<div style='border-bottom:1px dashed #ddd; width:100%; padding-top: 5px ;margin-bottom:5px;' ></div>"

                         + (!string.IsNullOrEmpty(arrSkLang2[0]) ? "" + arrSkLang2[0] : "") + "<br>"
                         + (!string.IsNullOrEmpty(arrSkLang2[1]) ? "การฟัง " + arrSkLang2[1] : "") + " / "
                         + (!string.IsNullOrEmpty(arrSkLang2[2]) ? "การพูด " + arrSkLang2[2] : "") + " / "
                         + (!string.IsNullOrEmpty(arrSkLang2[3]) ? "การอ่าน " + arrSkLang2[3] : "") + " / "
                         + (!string.IsNullOrEmpty(arrSkLang2[4]) ? "การเขียน " + arrSkLang2[4] : "")
          + "<div style='border-bottom:1px dashed #ddd; width:100%; padding-top: 5px ;margin-bottom:5px;' ></div>";

}

         
         Label lblSkCom = (Label)FormView1.FindControl("lblSkCom");
            if (lblSkCom != null) {
                lblSkCom.Text = Convert.ToString(DataBinder.Eval(FormView1.DataItem, "JobPrfSkCom"));
            }
         Label lblSkOther = (Label)FormView1.FindControl("lblSkOther");
            if (lblSkOther != null) {
                lblSkOther.Text = Convert.ToString(DataBinder.Eval(FormView1.DataItem, "JobPrfSkOther"));
            }

             Label lblMorInfo = (Label)FormView1.FindControl("lblMorInfo");
            if (lblMorInfo != null) {
                lblMorInfo.Text = Convert.ToString(DataBinder.Eval(FormView1.DataItem, "JobPrfMoreInfo"));
            }
         
          


        }



    protected void FormView2_DataBound(object sender, EventArgs e) {

        Label lblId = (Label)FormView2.FindControl("lblId");
        if (lblId != null) {
            int JobPrfId = Convert.ToInt32(DataBinder.Eval(FormView2.DataItem, "JobPrfId"));
            string strId = String.Format("{0:0000}", JobPrfId);
            lblId.Text = strId;
        }

        Label lblDateReg = (Label)FormView2.FindControl("lblDateReg");
        if (lblDateReg != null) {
            DateTime dtDateReg = Convert.ToDateTime(DataBinder.Eval(FormView2.DataItem, "JobPrfDateReg"));

            string strDate = dtDateReg.ToString("dd MMMM yyyy");
            string strTime = dtDateReg.ToString("HH:mm") + " น.";

            lblDateReg.Text = strDate + " (" + strTime + ")";
        }

        Label lblPosition = (Label)FormView2.FindControl("lblPosition");
        if (lblPosition != null) {
            string strPosition1 = DataBinder.Eval(FormView2.DataItem, "JobPrfPosition1").ToString();


            lblPosition.Text = !string.IsNullOrEmpty(strPosition1) ? "<p> " + SetPositionName(strPosition1) + "</p>" : "";


        }


        Label lblName = (Label)FormView2.FindControl("lblName");
        if (lblName != null) {
            string strPrfSex = Convert.ToString(DataBinder.Eval(FormView2.DataItem, "JobPrfSex"));
            string strName1 = Convert.ToString(DataBinder.Eval(FormView2.DataItem, "JobPrfName1"));
            string strName2 = Convert.ToString(DataBinder.Eval(FormView2.DataItem, "JobPrfName2"));

            lblName.Text = strPrfSex + " " + strName1 + " " + strName2;
        }

        Label lblBirth = (Label)FormView2.FindControl("lblBirth");
        if (lblBirth != null) {
            DateTime dtBirth = Convert.ToDateTime(DataBinder.Eval(FormView1.DataItem, "JobPrfBirth"));
            string strDate = dtBirth.ToString("d MMMM yyyy", new CultureInfo("th-TH"));
            lblBirth.Text = strDate;
        }

        Label lblAge = (Label)FormView2.FindControl("lblAge");
        if (lblAge != null) {

            string JobPrfAge = Convert.ToString(DataBinder.Eval(FormView2.DataItem, "JobPrfAge"));
            lblAge.Text = (!string.IsNullOrEmpty(JobPrfAge) ? JobPrfAge + " ปี" : "");
        }

        Label lblIdCard = (Label)FormView2.FindControl("lblIdCard");
        if (lblIdCard != null) {
            lblIdCard.Text = Convert.ToString(DataBinder.Eval(FormView2.DataItem, "JobPrfIdCard"));
        }

        Label lblPrfTel1 = (Label)FormView2.FindControl("lblPrfTel1");
        if (lblPrfTel1 != null) {
            string JobPrfTel1 = Convert.ToString(DataBinder.Eval(FormView2.DataItem, "JobPrfTel1"));
            lblPrfTel1.Text = JobPrfTel1;
        }

        Label lblPrfTel2 = (Label)FormView2.FindControl("lblPrfTel2");
        if (lblPrfTel2 != null) {
            string JobPrfTel2 = Convert.ToString(DataBinder.Eval(FormView2.DataItem, "JobPrfTel2"));
            lblPrfTel2.Text = JobPrfTel2;
        }

        Label lblPrfEmail = (Label)FormView2.FindControl("lblPrfEmail");
        if (lblPrfEmail != null) {
            lblPrfEmail.Text = Convert.ToString(DataBinder.Eval(FormView2.DataItem, "JobPrfEmail"));
        } 

        Label lblAddress1 = (Label)FormView2.FindControl("lblAddress1");
        if (lblAddress1 != null) {
            lblAddress1.Text = Convert.ToString(DataBinder.Eval(FormView2.DataItem, "JobPrfAddress1"));
        }

        Label lblAddress2 = (Label)FormView2.FindControl("lblAddress2");
        if (lblAddress2 != null) {
            lblAddress2.Text = Convert.ToString(DataBinder.Eval(FormView2.DataItem, "JobPrfAddress2"));
        }



        Label lblMorInfo = (Label)FormView2.FindControl("lblMorInfo");
        if (lblMorInfo != null) {
            lblMorInfo.Text = Convert.ToString(DataBinder.Eval(FormView2.DataItem, "JobPrfMoreInfo"));
        }



    }







    
}
 



 