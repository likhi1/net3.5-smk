﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class AdminWeb_WebPremiumForm : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initialDropdownList();
        }
    }
    public void initialDropdownList()
    {
        DataSet ds;
        VoluntaryManager cm = new VoluntaryManager();
        PremiumManager cmPrem = new PremiumManager();
        ds = cm.getAllDataProduct();
        ddlProduct.DataSource = ds;
        ddlProduct.DataTextField = "name";
        ddlProduct.DataValueField = "campaign";
        ddlProduct.DataBind();
        ddlProduct.Items.Insert(0, new ListItem(" - เลือก - ", ""));

        ds = cmPrem.getDistinctCarCode();
        ddlCarCode.DataSource = ds;
        ddlCarCode.DataTextField = "car_cod";
        ddlCarCode.DataValueField = "car_cod";
        ddlCarCode.DataBind();
        ddlCarCode.Items.Insert(0, new ListItem(" - เลือก - ", ""));

        ds = cmPrem.getPeriodPremium("IMPORT_WEB");
        ddlPeriod.DataSource = ds;
        ddlPeriod.DataTextField = "peri_text";
        ddlPeriod.DataValueField = "peri_value";
        ddlPeriod.DataBind();
        ddlPeriod.Items.Insert(0, new ListItem(" - เลือก - ", ""));
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        PremiumManager cmPrem = new PremiumManager();
        bool isSave = true;

        isSave = cmPrem.addDataOnlineWeb(ddlProduct.SelectedValue, ddlCarCode.SelectedValue,
                                            txtSumInsuredFrom.Text, txtSumInsuredTo.Text,
                                            ddlPeriod.SelectedValue, txtStartDate.Text,
                                            txtStopDate.Text);
        if (isSave)
        {
            litScript.Text = "<script>alert('Save complete.');</script>";
        }
        else
        {
            litScript.Text = "<script>alert('Error.');</script>";
        }
    }
}
