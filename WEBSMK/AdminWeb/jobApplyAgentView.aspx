﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminWeb/MasterPageAdmin1.master" AutoEventWireup="true" CodeFile="jobApplyAgentView.aspx.cs" Inherits="adminWeb_jobApplyAgentView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">


    
    <style>
 [id*=DetailsView] table { border: 1px solid #fff; }
 [id*=DetailsView] td { padding:5px;height: 25px; border: none; border-bottom: 1px dotted #ddd;  }
 [id*=DetailsView] td:nth-child(1) { width: 25%; text-align: right;   vertical-align: top; }
 [id*=DetailsView] td:nth-child(2) { text-align: left; vertical-align: top; }
    </style>
    <asp:DetailsView ID="DetailsView1" runat="server" Height="50px" Width="100%" OnDataBound="DetailsView1_DataBound" AutoGenerateRows="False" EnableModelValidation="True" BorderStyle="None" OnPageIndexChanging="DetailsView1_PageIndexChanging" >

        <Fields>
            <asp:TemplateField HeaderText="ID :">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblId"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

             <asp:TemplateField HeaderText="วันที่สมัคร :">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblDateReg"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>


            <asp:TemplateField HeaderText="ตำแหน่งงาน :">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblPosition"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
 
            <asp:TemplateField HeaderText="ชื่อ :">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblName"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="วันเกิด  :">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblBirth"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="อายุ  :">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblAge"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="เลขบัตรประชาชน   :">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblIdCard"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
     
     

 
            <asp:TemplateField HeaderText="ที่อยู่ปัจจุบันที่ติดต่อได้ :">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblAddress1"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>



            <asp:TemplateField HeaderText="ที่อยู่ตามบัตรประชาชน :">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblAddress2"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
 
 
        

                <asp:TemplateField HeaderText="ข้อมูลอื่นๆ :">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblMorInfo"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

          



        </Fields>






    </asp:DetailsView>

    <div style="margin: 0px auto; width: 100px; margin-top: 20px;">
        <asp:Button ID="Button1" runat="server" Text="ย้อนกลับ" class="btn-default btn-small" OnClientClick="history.back();return false;" />

    </div>




</asp:Content>

