﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminWeb_adminHome : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //***** Check  Session *****
        if (Session["SessAdmin"] == null) {
            Response.Redirect("main.aspx"); 
        }

        //***** Show all session ***** 
        for (int i = 0; i < Session.Count; i++) {
            var numSession = Session.Keys[i]; 

            Label2.Text = "<br/>" + string.Concat(numSession, " = ", Session[numSession]);
       
        }
    }
}
