﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---------- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;
using System.Collections;

 
public partial class adminWeb_jobApplyAgentView : System.Web.UI.Page
{
    public string Cat { get { return Request.QueryString["cat"]; } }
    public string Id { get { return Request.QueryString["id"]; } }

    public DataTable dtPosition { get; set; } // Decare Global Varibal  For Use in Posi
    protected DataTable Posi { get { DataTable dt = dtPosition; return dt; } }


    protected void Page_Load(object sender, EventArgs e) {
        SelectPosition() ;
        BindData(); 
    }
       
      
    void BindData() {
        DetailsView1.DataSource = SelectData();
        DetailsView1.DataBind();
    }

    protected DataSet SelectData() {
        string sql = "SELECT * FROM  tbJobPrf  WHERE  JobPrfCat ='" + Cat + "' "
                   + "AND JobPrfId='" + Id + "'  ";

        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql, "tb");
        return ds;
    }

    //================== Position Manage
    protected DataTable SelectPosition() {
        string sql = "Select JobPosiId ,JobPosiName  FROM tbJobPosi WHERE JobPrfCat= '" + Cat + "'    ORDER BY  JobPosiId DESC  ";

        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql, "myTb");
        dtPosition = ds.Tables[0];
        return dtPosition;
    }

    string posiName;
    protected string SetPositionName(string index) {
        foreach (DataRow r in Posi.Rows) {
            if (r[0].ToString() == index) {
                posiName = r[1].ToString();
            } else {
                posiName = "";
            }
        }
        return posiName;
    }


    protected void DetailsView1_DataBound(object sender, EventArgs e) {
         
        Label lblId = (Label)DetailsView1.FindControl("lblId");
        if (lblId != null) {
            int JobPrfId = Convert.ToInt32(DataBinder.Eval(DetailsView1.DataItem, "JobPrfId"));
            string strId = String.Format("{0:0000}", JobPrfId);
            lblId.Text = strId;
        }

        Label lblDateReg = (Label)DetailsView1.FindControl("lblDateReg");
        if (lblDateReg != null) {
            DateTime dtDateReg = Convert.ToDateTime(DataBinder.Eval(DetailsView1.DataItem, "JobPrfDateReg"));

            string strDate = dtDateReg.ToString("dd MMMM yyyy");
            string strTime = dtDateReg.ToString("HH:mm") + " น.";

            lblDateReg.Text = strDate + " (" + strTime + ")"; 
        } 

        Label lblPosition = (Label)DetailsView1.FindControl("lblPosition");
        if (lblPosition != null) {
            string strPosition = Convert.ToString(DataBinder.Eval(DetailsView1.DataItem, "JobPrfPosition"));
            lblPosition.Text = SetPositionName(strPosition);
        }
         

        Label lblName = (Label)DetailsView1.FindControl("lblName");
        if (lblName != null) { 
            string strPrfSex = Convert.ToString(DataBinder.Eval(DetailsView1.DataItem, "JobPrfSex"));
            string strName1 = Convert.ToString(DataBinder.Eval(DetailsView1.DataItem, "JobPrfName1"));
            string strName2 = Convert.ToString(DataBinder.Eval(DetailsView1.DataItem, "JobPrfName2"));

            lblName.Text = strPrfSex + " " + strName1 + " " + strName2;
        }

        Label lblBirth = (Label)DetailsView1.FindControl("lblBirth");
        if (lblBirth != null) {
            DateTime dtBirth = Convert.ToDateTime(DataBinder.Eval(DetailsView1.DataItem, "JobPrfBirth"));
            string strDate = dtBirth.ToString("d MMMM yyyy", new CultureInfo("th-TH"));
            lblBirth.Text = strDate;
        }

        Label lblAge = (Label)DetailsView1.FindControl("lblAge");
        if (lblAge != null) {

            string JobPrfAge = Convert.ToString(DataBinder.Eval(DetailsView1.DataItem, "JobPrfAge"));
            lblAge.Text = string.IsNullOrEmpty(JobPrfAge) ? JobPrfAge + " ปี" : "";
        }

        Label lblIdCard = (Label)DetailsView1.FindControl("lblIdCard");
        if (lblIdCard != null) {
            lblIdCard.Text = Convert.ToString(DataBinder.Eval(DetailsView1.DataItem, "JobPrfIdCard"));
        }
         
         

        Label lblAddress1 = (Label)DetailsView1.FindControl("lblAddress1");
        if (lblAddress1 != null) {
            lblAddress1.Text = Convert.ToString(DataBinder.Eval(DetailsView1.DataItem, "JobPrfAddress1"));
        }

        Label lblAddress2 = (Label)DetailsView1.FindControl("lblAddress2");
        if (lblAddress2 != null) {
            lblAddress2.Text = Convert.ToString(DataBinder.Eval(DetailsView1.DataItem, "JobPrfAddress2"));
        }

          

 
         




    }
     
    protected void DetailsView1_PageIndexChanging(object sender, DetailsViewPageEventArgs e) {

    }
}