﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminWeb/MasterPageAdmin1.master" AutoEventWireup="true" CodeFile="bannerList.aspx.cs" Inherits="adminWeb_bannerList" %>
 

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <script>
          function CheckNull(tbx) {
              console.log(tbx.id);
              tbxSort = $("#" + tbx.id);

              if (!tbxSort.val() == "") {
                  // alert("กรุณาระบุ ลำดับด้วย");  
                  if (isDecimal(tbxSort.val()) == false) {
                      alert("กรุณาระบุ ให้ถูกต้อง และมีทศนิยมได้ไม่เกิน 3 ตำแหน่ง"); return false;
                  }
              }
              return true;
          }


          function isDecimal(_val) {
              patt = /^[0-9]+([.,][0-9]{1,3})?$/;
              re = patt.test(_val);
              return re;
          }

          function validateNumber(event) {
              var key = window.event ? event.keyCode : event.which;
              if (event.keyCode == 8 || event.keyCode == 46
               || event.keyCode == 37 || event.keyCode == 39) {
                  return true;
              }
              else if (key < 48 || key > 57) {
                  return false;
              }
              else return true;
          }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
      <div id="page-prdList">
     <div id="manageArea">
            <div  class="colLeft">
       <h1 class="subject1">
         แบนเนอร์<%
string  strPageName = ( Convert.ToInt32 (CatId) == 1 ?"- ด้านบน": "- ด้านข้าง" ) ;
Response.Write(strPageName);  
%>

       </h1>
   
                <div class="navManage">
            <asp:Button ID="btnAddContent" runat="server" Text="เพิ่มรายการ" class="btn-default"    UseSubmitBehavior="false"   />
 
        </div>

                </div><!-- end  class="colLeft" --> 
<div class="colRight">
<div class="areaSearch">
 

</div>
<!-- End  class="areaSearch" --> 
       <div class="notaion"> *** การแสดงหน้าเวบ 1.ต้องมีการเปิดการใช้งาน </div>
</div><!-- end  class="colRight" -->
</div><!-- End  id="manageArea" -->

    <style> 
    /*===== Control  GridView1  Width (support IE9 & Over) =====*/ 
    [id*=GridView] th:nth-child(1) {width: 5% ; }
    [id*=GridView] th:nth-child(2) { }
    [id*=GridView] th:nth-child(3) {width:8%; }
      [id*=GridView] th:nth-child(4) {width:8%; }
    [id*=GridView] th:nth-child(5) {width: 13% }  
    </style>

<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False"  Width="100%" EnableModelValidation="True" 
    DataKeyNames="BannerId"
    OnRowDataBound="GridView1_RowDataBound" 
    OnRowDeleting="GridView1_Deleting" 
    OnPageIndexChanging="GridView1_IndexChanging" 
    OnRowEditing="modEditCommand"
    OnRowUpdating="modUpdateCommand"
    OnRowCancelingEdit="modCancelCommand"
>

 
  
            <Columns>

                <asp:TemplateField HeaderText="ID">
                    <%-- แสดงเวลาปรกติ --%>
                    <ItemTemplate >
                        <div style="float:right">
                        <asp:Label ID="lblId" runat="server"  ></asp:Label></div>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="หัวข้อ"  >
                    <ItemTemplate  >
                        <asp:HyperLink ID="hplTitle" runat="server"  > 
                        </asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>

                

    <asp:TemplateField HeaderText="สถานะ">
    <ItemTemplate>
    <div style="text-align:center"> 
    <asp:Image ID="imgStatus" runat="server" /> 
    </div>
    </ItemTemplate> 
    <EditItemTemplate>
    <asp:Label ID="lblHdStatus" runat="server"   Visible="false"></asp:Label>
    <asp:DropDownList ID="ddlStatus" runat="server"   > </asp:DropDownList>
    </EditItemTemplate>  
    </asp:TemplateField>  

    <asp:TemplateField HeaderText="แสดง<br>ลำดับ ">
    <ItemTemplate>
    <div style="text-align:center">
    <asp:Label ID="lblSort" runat="server"></asp:Label> 
    </div>
    </ItemTemplate> 
    <EditItemTemplate>
    <asp:Label ID="lblHdSort" runat="server"   Visible="false"></asp:Label>
    <asp:TextBox ID="tbxSort" runat="server"></asp:TextBox>
    </EditItemTemplate>

                </asp:TemplateField>


                <asp:TemplateField HeaderText="จัดการ" ShowHeader="False">
                    <ItemTemplate>
                        <div style="text-align: center">
                            <asp:Button ID="btnRowEdit" runat="server" Text="ตั้งค่า" class="btn-default btn-small " CommandName="Edit" />
                            <asp:Button ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Delete" Text="ลบ"
                                class="btn-default btn-small " OnClientClick="return confirm('ยืนยันทำการ ลบข้อมูล')" />
                        </div>
                    </ItemTemplate> 

                    <EditItemTemplate>
                        <div style="text-align: center"> 
                        <asp:Button ID="btnRowUpdate" runat="server" Text="บันทึก" class="btn-default btn-small " CommandName="Update" />
                        <asp:Button ID="btnRowCancel" runat="server" Text="ยกเลิก" class="btn-default btn-small " CommandName="Cancel" /> 
                        </div>
                    </EditItemTemplate>
          </asp:TemplateField>
            


            </Columns>
            <PagerSettings Mode="NumericFirstLast" />
            <RowStyle Height="20px" />
        </asp:GridView>


 

     

    </div>
    <!-- end  id="page-prdList" --> 


</asp:Content>

