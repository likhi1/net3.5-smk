﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;


public partial class adminWeb_prdView  : System.Web.UI.Page
{
    protected string PrdId { get { return Request.QueryString["id"];  } }
    protected string CatId  { get { return Request.QueryString["cat"];  } }
   // protected string CatSelect  { get { return ddlCat.SelectedValue;   } }
     

    
    protected void Page_Load(object sender, EventArgs e) {
        ////////// Check  Session 
        // new CheckSess().CurrentSession(); 

        /////////  Chk  Query String 
        //if (PrdId == null || CatId == null) {
        //   Response.Redirect("prdList.aspx");
        //}
            
        //////// Run Bind Data  
            if (!Page.IsPostBack) {
                ddlCatBindData();
                BindData(); 
            } 

    }


 

    protected void ddlCatBindData() { 
        string sql = "SELECT * FROM tbPrdCat  ORDER BY PrdCatId ";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "tbPrdCaT");
 
        //**** Add frist Item of dropdownlist
        ddlCat.DataSource = ds;
        ddlCat.AppendDataBoundItems = true;
        ddlCat.DataTextField = "PrdCatName";
        ddlCat.DataValueField = "PrdCatId";
        ddlCat.DataBind();

        //***** Add frist Item of dropdownlist
        /* 
        ddlPrdCat.Items.Insert( 0 , new ListItem("-- Please Select --", ""));  
        ddlPrdCat.SelectedIndex = 0;
        */
    }

    protected DataSet SelectData() {
        string sql1 = "SELECT * FROM tbPrdCon WHERE  PrdConId= '" + PrdId + "'  "
            + "AND PrdCatId  ='" + CatId + "' ";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql1, "tblPrdCon"); 
        return ds;
    }

    protected void BindData() {
        
        DataSet ds =  SelectData();
        DataTable dt1 = ds.Tables[0];

        if (dt1.Rows.Count > 0) {
            // foreach( string  x in dt1){ }  // *** see all value in  DataTable ***

            int  intId =  Convert.ToInt32( dt1.Rows[0][0].ToString() ) ;
            lblId.Text = String.Format("{0:00000}", intId);  

            ddlCat.SelectedValue = dt1.Rows[0]["PrdCatId"].ToString();
          //  ddlCat.Attributes.Add("disabled", "true"); 

            string  strSort =    dt1.Rows[0]["PrdConSort"].ToString()   ;
            tbxSort.Text =   Convert.ToDecimal(strSort) !=9999?strSort : "" ;

            rblStatus.SelectedValue = dt1.Rows[0]["PrdConStatus"].ToString();

            //------ Split Date  to Array  
            DateTime strDtShow = (DateTime)dt1.Rows[0]["PrdConDtShow"];
            tbxDateShow.Text = strDtShow.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

            DateTime strDtHide = (DateTime)dt1.Rows[0]["PrdConDtHide"];
            tbxDateHide.Text = strDtHide.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

            tbxTopic.Text = dt1.Rows[0]["PrdConTopic"].ToString();
            tbxPicSmall.Text = dt1.Rows[0]["PrdPicSmall"].ToString();
            tbxPicBig.Text = dt1.Rows[0]["PrdPicBig"].ToString();
            tarDetailShort.Text = dt1.Rows[0]["PrdConDetailShort"].ToString();
            tarDetailLong.Text = dt1.Rows[0]["PrdConDetailLong"].ToString();

            string  strConType = Convert.ToString(dt1.Rows[0]["PrdConType"]);
            if (!string.IsNullOrEmpty(strConType)) {
                ddlType.SelectedValue = strConType;
            }

        }
    }


    protected void btnSave_Click(object sender, EventArgs e) {

        /////// Update content
        string sql = "UPDATE  tbPrdCon SET "
        + "PrdCatId = @PrdCatId, "
        + "PrdConSort = @PrdConSort, "
        + "PrdConDtShow =  @PrdConDtShow, "
        + "PrdConDtHide  =  @PrdConDtHide, "
        + "PrdPicSmall = @PrdPicSmall, "
        + "PrdPicBig = @PrdPicBig, "
        + "PrdConTopic = @PrdConTopic, "
        + "PrdConDetailShort = @PrdConDetailShort , "
        + "PrdConDetailLong = @PrdConDetailLong, "
        + "PrdConStatus = @PrdConStatus,  "
         + "PrdConType = @PrdConType, "
              //===== AdminInfo Update  
         + "input_date = @input_date ,  "
         + "input_user = @input_user  " 

        + "WHERE PrdConId = @PrdId  AND PrdCatId = @CatId  ";

        DateTime dtDateShow = Convert.ToDateTime(tbxDateShow.Text, new CultureInfo("th-TH"));
        DateTime dtDateHide = Convert.ToDateTime(tbxDateHide.Text, new CultureInfo("th-TH"));

        Decimal decSort;

        SqlParameterCollection objParam1 = new SqlCommand().Parameters;
        objParam1.AddWithValue("@PrdCatId", CatId ); //  Request.Form["ddlCat"] 
        objParam1.AddWithValue("@PrdConSort", (decSort = (tbxSort.Text != "") ? Convert.ToDecimal(tbxSort.Text) : 9999));
        objParam1.AddWithValue("@PrdConStatus", rblStatus.SelectedValue);
        objParam1.AddWithValue("@PrdConDtShow", dtDateShow);
        objParam1.AddWithValue("@PrdConDtHide", dtDateHide);
        objParam1.AddWithValue("@PrdConTopic", tbxTopic.Text);
        objParam1.AddWithValue("@PrdConDetailShort", tarDetailShort.Text);
        objParam1.AddWithValue("@PrdConDetailLong", tarDetailLong.Text);
        objParam1.AddWithValue("@PrdId", PrdId);
        objParam1.AddWithValue("@CatId", CatId);
        objParam1.AddWithValue("@PrdConType", ddlType.SelectedValue);

		//===== AdminInfo Update  
        objParam1.AddWithValue("@input_date", DateTime.Now);
        //////Get UserName From Session Object
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        objParam1.AddWithValue("@input_user", loginData.loginName);

        /////// Get Value From Web Control
        objParam1.AddWithValue("@PrdPicSmall", tbxPicSmall.Text);
        objParam1.AddWithValue("@PrdPicBig", tbxPicBig.Text);

        /////  Get Value From Html Control   
        ////objParam1.AddWithValue("@PrdPicSmall", SqlDbType.NVarChar).Value =  Request.Form["PicSmall"].ToString();
        ////objParam1.AddWithValue("@PrdPicBig", SqlDbType.NVarChar).Value = Request.Form["PicPic"].ToString();

        int i2 = new DBClass().SqlExecute(sql, objParam1);

        if (i2 == 1) {
            Response.Write("<script>alert('แก้ไขข้อมูลเรียบร้อยแล้ว')</script>");
            Response.Redirect("prdView.aspx?id=" + PrdId + "&cat=" + CatId);
        }
        else {
            Response.Write("<script>alert('ไม่สามารถแก้ไขข้อมูลได้')</script>");
            Response.Write("<script>history.go(-1)</script>");
        }

    }



}