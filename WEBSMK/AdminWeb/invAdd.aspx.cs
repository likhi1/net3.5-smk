﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization; 


public partial class adminWeb_invAdd : System.Web.UI.Page
{  
    protected void Page_Load(object sender, EventArgs e) {

        //***** Check  Session *****
        // new CheckSess().CurrentSession();   

        if (!Page.IsPostBack) {
            ddlCatBindData();
        } 
    }


     
    protected void btnSave_Click(object sender, EventArgs e) {
         
        string sql1 = "INSERT INTO tbInv "
                    + "VALUES ( @InvTopic, @InvStatus,@NewsDetailLong,@InvDtReg, "
                      
                      //=====  AdminInfo Add
                        + " @lang , "
                        + " @input_date ,   "
                        + " @input_user,  "

                     //=====  Increase Field For Category
                       + " @InvCatId ,  "
                       + " @InvSort " 
                       + " ) ";

        Decimal decSort;
        SqlParameterCollection objParam1 = new SqlCommand().Parameters;
        objParam1.AddWithValue("@InvTopic", tbxTopic.Text);
        objParam1.AddWithValue("@InvStatus", rblStatus.SelectedValue); 
        objParam1.AddWithValue("@NewsDetailLong",   tarDetailLong.Text );
        objParam1.AddWithValue("@InvDtReg", DateTime.Today);
        objParam1.AddWithValue("@input_date", DateTime.Now);
        objParam1.AddWithValue("@InvSort", (decSort = (tbxSort.Text != "")? Convert.ToDecimal(tbxSort.Text) : 9999));//Set Default value = 99999  If User no Assign
         
        //===== Update2014  Multi Language
       objParam1.AddWithValue("@lang", rblLanguage.SelectedValue ); // TH = 0 ,  En =  1 

       //===========  Get UserName From Session Object 
       ProfileData loginData = (ProfileData)Session["SessAdmin"];
       objParam1.AddWithValue("@input_user", loginData.loginName); 

       //=====  Increase Field For Category
       objParam1.AddWithValue("@InvCatId ", ddlCat.SelectedValue);
        
         
        int i1 = new DBClass().SqlExecute(sql1, objParam1);  
        if (i1 == 1) {
            Response.Write("<script>alert('เพิ่มข้อมูลเรียบร้อยแล้ว'); location='invList.aspx' ;</script>");
        } else {
            Response.Write("<script>alert('ไม่สามารถเพิ่มข้อมูลได้'); history.back() ;</script>"); 
        }



    }

    protected void ddlCatBindData() {
        string sql = "SELECT * FROM tbInvCat ORDER BY InvCatId ";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "tbName");

        ddlCat.DataSource = ds;
        //**** Add frist Item of dropdownlist
        ddlCat.AppendDataBoundItems = true;
        ddlCat.DataTextField = "InvCatNameTh";
        ddlCat.DataValueField = "InvCatId";
        ddlCat.DataBind();
    }


}