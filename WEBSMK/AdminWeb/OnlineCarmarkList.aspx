﻿<%@ Page Language="C#" MasterPageFile="~/AdminWeb/MasterPageAdmin1.master" AutoEventWireup="true" CodeFile="OnlineCarmarkList.aspx.cs" Inherits="Module_Authenticate_OnlineCarmarkList" Title="Untitled Page" %>
<%@ Register src="uc/ucPageNavigator.ascx" tagname="ucPageNavigator" tagprefix="uc1" %>
<%@ Register src="uc/ucTxtDate.ascx" tagname="ucTxtDate" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script language="javascript">
        function goPage(strPage) {
            window.location = "/<%=strAppName%>/" + strPage;
            return false;
        }
        function validateForm() {
            var isRet = true;
            var strMsg = "";
            var strMsg2 = "";
            var dateFrom;
            var dateTo;
            if (document.getElementById("<%=txtCarMark.ClientID %>").value == "" ) {
                isRet = false;
                strMsg += "! รุ่นรถ\n";
            }
            if (document.getElementById("<%=txtCarName.ClientID %>").value == "") {
                isRet = false;
                strMsg += "! ยี่รถ\n";
            }

            if (isRet == false) {
                if (strMsg != "")
                    alert("! กรุณาระบุข้อมูล\n" + strMsg + strMsg2);
                else
                    alert(strMsg2);
            }
            return isRet;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div id="page-prdList">
              <div id="manageArea">
            <div  class="colLeft">
        <h1 class="subject1">จัดการรุ่นรถ</h1>        
        <div class="navManage">
        <asp:Button ID="btnAdd" runat="server" Text="เพิ่มรายการใหม่" CssClass="btn-default" onclick="btnAddItem_Click"/>
        </div>
                </div><!-- end  class="colLeft" --> 
<div class="colRight">
<div class="areaSearch">



                       
            <table width="100%">
                <tr>
                    <td class="label">ยี่ห้อรถยนต์ :</td>
                    <td align="left">
                        <asp:TextBox ID="txtCarMark" runat="server" CssClass="TEXTBOX" Width="100"></asp:TextBox>
                    </td>                      
                    <td class="label">รุ่นรถยนต์ :</td>
                    <td align="left">
                        <asp:TextBox ID="txtCarName" runat="server" CssClass="TEXTBOX" Width="100"></asp:TextBox>
                    </td>                                              
                </tr>
                <tr>
                    <td class="label">กลุ่มรถ :</td>
                    <td align="left">
                        <asp:TextBox ID="txtCarGroup" runat="server" CssClass="TEXTBOX" Width="100"></asp:TextBox>
                    </td>                                              
                </tr>
                <tr>
                    <td></td>  
                    <td></td>  
                    <td></td>
                    <td>
                        <asp:Button ID="btnSearch" runat="server" Text="ค้นหา"
                            onclick="btnSearch_Click" CssClass="btn-default btn-small"/>
                        <asp:Button ID="btnClear" runat="server" Text="เคลียร์หน้าจอ" OnClientClick="return goPage('Adminweb/OnlineCarmarkList.aspx');" CssClass="btn-default btn-small"/>                        
                    </td>
                </tr>
            </table>
        
    </div>
<!-- End  class="areaSearch" -->
    
    
    </div><!-- end  class="colRight" -->        
</div><!-- End  id="manageArea" -->

          <style> 
        /*===== Control  GridView1  Width (support IE9 & Over) =====*/ 
        [id*=dtgData] th:nth-child(1) {width: 8% ; }
        [id*=dtgData] th:nth-child(2) { }
        [id*=dtgData] th:nth-child(3) {width:10%; }
        [id*=dtgData] th:nth-child(4) {width: 7% } 
        [id*=dtgData] th:nth-child(5) {width: 7% } 
        [id*=dtgData] th:nth-child(6) {width: 7% }
        [id*=dtgData] th:nth-child(7) {width: 7% }  
        </style>
 



        <div class="list-data" >      
            <table width="100%">
                <tr>
                    <td>                            
                        <uc1:ucpagenavigator ID="ucPageNavigator1" runat="server" 
                            OnPageIndexChanged="PageIndexChanged"/>                            
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DataGrid ID="dtgData" runat="server" AutoGenerateColumns="false" Width="100%"
                            HeaderStyle-CssClass="gridhead" ItemStyle-CssClass="gridrow0" AlternatingItemStyle-CssClass="gridrow1"
                            AllowPaging="true" PagerStyle-Visible="false"
                        UseAccessibleHeader="true"                            
                            >
                            <Columns>
                                <asp:TemplateColumn>
			                        <HeaderTemplate>
                                        <asp:Label ID="lblHeadNo" runat="server" Text="<%#ColumnTitle[0]%>"></asp:Label>							                
			                        </HeaderTemplate>
			                        <ItemTemplate>
			                            <%# Container.DataSetIndex +1%>
			                        </ItemTemplate>
		                        </asp:TemplateColumn>
                                <asp:TemplateColumn>
			                        <HeaderTemplate>
				                        <asp:LinkButton ID="lnkID1" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[1]%>">
				                        </asp:LinkButton>
				                        <asp:Image ID="imgSortCol1" Runat="server" ImageUrl="<%#getPicUrl(1)%>">
				                        </asp:Image>
			                        </HeaderTemplate>
			                        <ItemTemplate>
			                            <%#Eval(ColumnName[1])%>
			                            <div style="display:none">
			                                <asp:TextBox ID="txtCarMark" runat="server" Text="<%#Eval(ColumnName[1])%>"></asp:TextBox>
			                            </div>
			                        </ItemTemplate>
			                        <EditItemTemplate>
                                        <asp:TextBox ID="txtCarMark" runat="server" Text="<%#Eval(ColumnName[1])%>" CssClass="TEXTBOX" Width="80"></asp:TextBox>			                            
			                        </EditItemTemplate>
		                        </asp:TemplateColumn>
		                        <asp:TemplateColumn>
			                        <HeaderTemplate>
				                        <asp:LinkButton ID="lnkID2" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[2]%>">
				                        </asp:LinkButton>
				                        <asp:Image ID="imgSortCol2" Runat="server" ImageUrl="<%#getPicUrl(2)%>">
				                        </asp:Image>
			                        </HeaderTemplate>
			                        <ItemTemplate>
			                            <%#Eval(ColumnName[2])%>
			                        </ItemTemplate>
			                        <EditItemTemplate>
                                        <asp:TextBox ID="txtCarName" runat="server" Text="<%#Eval(ColumnName[2])%>" CssClass="TEXTBOX" Width="40"></asp:TextBox>			                            
			                        </EditItemTemplate>
		                        </asp:TemplateColumn>
		                        <asp:TemplateColumn>
			                        <HeaderTemplate>
				                        <asp:LinkButton ID="lnkID3" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[3]%>">
				                        </asp:LinkButton>
				                        <asp:Image ID="imgSortCol3" Runat="server" ImageUrl="<%#getPicUrl(3)%>">
				                        </asp:Image>
			                        </HeaderTemplate>
			                        <ItemTemplate>
			                            <%#Eval(ColumnName[3])%>
			                        </ItemTemplate>
			                        <EditItemTemplate>
                                        <asp:TextBox ID="txtCarGroup" runat="server" Text="<%#Eval(ColumnName[3])%>" CssClass="TEXTBOX" Width="20"></asp:TextBox>			                            
			                        </EditItemTemplate>
		                        </asp:TemplateColumn>
		                        <asp:TemplateColumn>
			                        <HeaderTemplate>
				                        <asp:LinkButton ID="lnkID4" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[4]%>">
				                        </asp:LinkButton>
				                        <asp:Image ID="imgSortCol4" Runat="server" ImageUrl="<%#getPicUrl(4)%>">
				                        </asp:Image>
			                        </HeaderTemplate>
			                        <ItemTemplate>
			                            <%#(Eval(ColumnName[4]))%>
			                        </ItemTemplate>
			                        <EditItemTemplate>
                                        <asp:TextBox ID="txtDescT" runat="server" Text="<%#Eval(ColumnName[4])%>" CssClass="TEXTBOX"></asp:TextBox>			                            
			                        </EditItemTemplate>
		                        </asp:TemplateColumn>
		                        <asp:TemplateColumn>
			                        <HeaderTemplate>
				                        <asp:LinkButton ID="lnkID5" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[5]%>">
				                        </asp:LinkButton>
				                        <asp:Image ID="imgSortCol5" Runat="server" ImageUrl="<%#getPicUrl(5)%>">
				                        </asp:Image>
			                        </HeaderTemplate>
			                        <ItemTemplate>
			                            <%#(Eval(ColumnName[5]))%>
			                        </ItemTemplate>
			                        <EditItemTemplate>
                                        <asp:TextBox ID="txtDescE" runat="server" Text="<%#Eval(ColumnName[5])%>" CssClass="TEXTBOX"></asp:TextBox>			                            
			                        </EditItemTemplate>
		                        </asp:TemplateColumn>
		                        <asp:TemplateColumn>
			                        <HeaderTemplate>
				                        <asp:LinkButton ID="lnkID6" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[6]%>">
				                        </asp:LinkButton>
				                        <asp:Image ID="imgSortCol6" Runat="server" ImageUrl="<%#getPicUrl(6)%>">
				                        </asp:Image>
			                        </HeaderTemplate>
			                        <ItemTemplate>
			                            <%#(Eval(ColumnName[6]))%>
			                        </ItemTemplate>
			                        <EditItemTemplate>
                                        <asp:TextBox ID="txtCarType" runat="server" Text="<%#Eval(ColumnName[6])%>" CssClass="TEXTBOX" Width="40"></asp:TextBox>			                            
			                        </EditItemTemplate>
		                        </asp:TemplateColumn>
		                        <asp:TemplateColumn>
			                        <HeaderTemplate>
				                        <asp:LinkButton ID="lnkID7" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[7]%>">
				                        </asp:LinkButton>
				                        <asp:Image ID="imgSortCol7" Runat="server" ImageUrl="<%#getPicUrl(7)%>">
				                        </asp:Image>
			                        </HeaderTemplate>
			                        <ItemTemplate>
			                            <%#FormatStringApp.FormatInt(Eval(ColumnName[7]))%>
			                        </ItemTemplate>
			                        <EditItemTemplate>
                                        <asp:TextBox ID="txtODMin" runat="server" Text="<%#FormatStringApp.FormatInt(Eval(ColumnName[7]))%>" CssClass="TEXTBOXMONEY" width="60"></asp:TextBox>			                            
			                        </EditItemTemplate>
		                        </asp:TemplateColumn>
		                        <asp:TemplateColumn>
			                        <HeaderTemplate>
				                        <asp:LinkButton ID="lnkID8" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[8]%>">
				                        </asp:LinkButton>
				                        <asp:Image ID="imgSortCol8" Runat="server" ImageUrl="<%#getPicUrl(8)%>">
				                        </asp:Image>
			                        </HeaderTemplate>
			                        <ItemTemplate>
			                            <%#FormatStringApp.FormatInt(Eval(ColumnName[8]))%>
			                        </ItemTemplate>
			                        <EditItemTemplate>
                                        <asp:TextBox ID="txtODMax" runat="server" Text="<%#FormatStringApp.FormatInt(Eval(ColumnName[8]))%>" CssClass="TEXTBOXMONEY"></asp:TextBox>			                            
			                        </EditItemTemplate>
		                        </asp:TemplateColumn>
		                        <asp:TemplateColumn>
			                        <HeaderTemplate>
				                        <asp:LinkButton ID="lnkID9" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[9]%>">
				                        </asp:LinkButton>
				                        <asp:Image ID="imgSortCol9" Runat="server" ImageUrl="<%#getPicUrl(9)%>">
				                        </asp:Image>
			                        </HeaderTemplate>
			                        <ItemTemplate>
			                            <%#(Eval(ColumnName[9]))%>
			                        </ItemTemplate>
			                        <EditItemTemplate>
                                        <asp:TextBox ID="txtCarCC" runat="server" Text="<%#Eval(ColumnName[9])%>" CssClass="TEXTBOXMONEY"></asp:TextBox>			                            
			                        </EditItemTemplate>
		                        </asp:TemplateColumn>
		                        <asp:TemplateColumn>
			                        <HeaderTemplate>
				                        
			                        </HeaderTemplate>
			                        <ItemTemplate>
			                            <asp:LinkButton ID="lnkEdit" runat="server" OnClick="lnkEdit_Click" dsindex="<%# Container.DataSetIndex%>">Edit</asp:LinkButton>
			                        </ItemTemplate>
			                        <EditItemTemplate>
			                            <asp:LinkButton ID="lnkSave" runat="server" OnClick="lnkSave_Click" dsindex="<%# Container.DataSetIndex%>">Save</asp:LinkButton>
			                            <asp:LinkButton ID="lnkCancel" runat="server" OnClick="lnkCancel_Click" dsindex="<%# Container.DataSetIndex%>">Cancel</asp:LinkButton>
			                        </EditItemTemplate>
		                        </asp:TemplateColumn>
		                    </Columns>
                        </asp:DataGrid>
                    </td>
                </tr>
            </table>
        </div>                        
    </div> <!-- end  id="page-prdList" -->    
    <asp:Literal ID="litJavaScript" runat="server"></asp:Literal>
    <div style="display:none">
        <asp:TextBox ID="txtMode" runat="server"></asp:TextBox>
    </div>                
</asp:Content>

