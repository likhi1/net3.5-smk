﻿<%@ Page Language="C#" MasterPageFile="~/AdminWeb/MasterPageAdmin1.master" AutoEventWireup="true" CodeFile="ChangePasswordForm.aspx.cs" Inherits="Module_Authenticate_ChangePasswordForm" Title="Untitled Page" %>
<%@ Register src="uc/ucTxtDate.ascx" tagname="ucTxtDate" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script>
        function goPage(strPage)
        {
            window.location= "/<%=strAppName%>/" + strPage;
            return false;
        }
        function validateChangePassword()
        {
            var isRet = true;
            var strMsg = "";
            if (document.getElementById("<%=txtNewPassword.ClientID %>").value == ""){
                isRet = false;
                strMsg += "\t-  รหัสผ่านใหม่\n";
            }
            if (document.getElementById("<%=txtRenewPassword.ClientID %>").value == ""){
                isRet = false;
                strMsg += "\t-  ยืนยันรหัสผ่านใหม่\n";
            }
            if (document.getElementById("<%=txtNewPassword.ClientID %>").value.length < 8)
            {
                isRet = false;
                strMsg += "! รหัสผ่านต้องมีความยาวอย่างน้อย 8 ตัวอักษร\n";
            }            
            if (document.getElementById("<%=txtNewPassword.ClientID %>").value != document.getElementById("<%=txtRenewPassword.ClientID %>").value)
            {
                isRet = false;
                strMsg += "! รหัสผ่านใหม่ ไม่ตรงกับ ยืนยันรหัสผ่านใหม่\n";
            }

            if (isRet == false)
                alert("! กรุณาระบุข้อมูล\n" + strMsg);            
            return isRet;
        }
        function btnCloseSearchPopupClick() {
            Popup.hide('divPopup');
            return false;
        }   
        function submitWhenEnterKey(e)
        {
            if ((event.which && event.which == 13) || (event.keyCode && event.keyCode == 13))
            {
                formpostback();
            }
        }    
        function formpostback()
        {
            var theform;
            theform = document.forms[0];
            eval(<%=strPostBack %>);
        }     
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">    
    <div id="page-prdAdd">
    <h1 class="subject1">เปลี่ยนพาสเวิส</h1>      
    <table width="100%" border="0">
        <tr>
            <td width="20px">&nbsp;</td>
            <td width="150px" align="right"><div class="labelRequireField">รหัสผ่านใหม่ :</div></td>
            <td>
                <asp:TextBox ID="txtNewPassword" runat="server" CssClass="TEXTBOX" Width="100" MaxLength="13" TextMode="Password"></asp:TextBox>
            </td>
            <td width="20px">&nbsp;</td>
        </tr>
        <tr>
            <td width="20px">&nbsp;</td>
            <td align="right"><div class="labelRequireField">ยืนยันรหัสผ่านใหม่ :</div></td>
            <td>
                <asp:TextBox ID="txtRenewPassword" runat="server" CssClass="TEXTBOX" Width="100" MaxLength="13" TextMode="Password"></asp:TextBox>
            </td>
            <td width="20px">&nbsp;</td>
        </tr>
        <tr>
            <td></td>        
            <td colspan="2">
                <asp:Button ID="btnConfirmChangePwd" runat="server" Text="ตกลง" 
                    CssClass="btn-default" OnClientClick="return validateChangePassword();" 
                    onclick="btnConfirmChangePwd_Click" />                
            </td>
            <td></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>                                         
    <div style="display:none">
        <asp:TextBox ID="txtMode" runat="server"></asp:TextBox>
        <asp:HiddenField ID="hddPassword" runat="server" />
    </div>           
    </div>
    <asp:Literal ID="litJavaScript" runat="server"></asp:Literal>        
</asp:Content>

