﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---------- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;
using System.Collections;

public partial class adminWeb_cntView : System.Web.UI.Page
{
    protected string CntId {  get{  return Request.QueryString["id"]; } } 
 

    protected void Page_Load(object sender, EventArgs e) { 
            BindData();
         
    }

    void BindData() {
        DetailsView1.DataSource = SelectData();
        DetailsView1.DataBind();
    }

    protected DataSet SelectData() {
        string sql = "SELECT *  FROM tbCnt  WHERE CntId=" + CntId;
        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql, "tb");
        return ds;
    }
     

    protected void DetailsView1_DataBound(object sender, EventArgs e) {

       
        Label lblId = (Label)DetailsView1.FindControl("lblId");
        if (lblId != null) {
        int CntId = Convert.ToInt32(DataBinder.Eval(DetailsView1.DataItem, "CntId"));
        string strId = String.Format("{0:0000}", CntId);
        lblId.Text = strId; 
        }


        Label lblStatus= (Label)DetailsView1.FindControl("lblStatus");
        if (lblStatus != null) {
            lblStatus.Text = Convert.ToString(DataBinder.Eval(DetailsView1.DataItem, "CntStatus"));

        }



        Label lblTopic = (Label)DetailsView1.FindControl("lblTopic");
        if (lblId != null) {
            string  strTopic = Convert.ToString(DataBinder.Eval(DetailsView1.DataItem, "CntDetail"));

            lblTopic.Text = ((strTopic.Length) <= 50) ? strTopic : strTopic.Substring(0, 50) + "...";
            
        }

        Label lblName = (Label)DetailsView1.FindControl("lblName");
        if (lblName != null) {
            lblName.Text  = Convert.ToString(DataBinder.Eval(DetailsView1.DataItem, "CntName"));
 
        }

        Label lblTel = (Label)DetailsView1.FindControl("lblTel");
        if (lblTel != null) {
            lblTel.Text = Convert.ToString(DataBinder.Eval(DetailsView1.DataItem, "CntTel")); 
        }

        Label lblEmail = (Label)DetailsView1.FindControl("lblEmail");
        if (lblEmail != null) {
            lblEmail.Text = Convert.ToString(DataBinder.Eval(DetailsView1.DataItem, "CntEmail"));
        }

        Label lblDetail = (Label)DetailsView1.FindControl("lblDetail");
        if (lblDetail != null) {
            lblDetail.Text = Convert.ToString(DataBinder.Eval(DetailsView1.DataItem, "CntDetail"));
        }


        Label lblDateGet = (Label)DetailsView1.FindControl("lblDateGet");
        if (lblDateGet != null) {
            DateTime dtDateTimeGet = Convert.ToDateTime(DataBinder.Eval(DetailsView1.DataItem, "CntDateTimeGet"));
             string strDate = dtDateTimeGet.ToString("dMMMyy");
             string strTime = dtDateTimeGet.ToString("H:m");

             lblDateGet.Text = strDate + " (" + strTime + ")";
        }


        Label lblDateSet = (Label)DetailsView1.FindControl("lblDateSet");
        if (lblDateSet != null) {

              DateTime dtDateSet =  Convert.ToDateTime(DataBinder.Eval(DetailsView1.DataItem, "CntDateSet"));
              string strTimeSet = Convert.ToString(DataBinder.Eval(DetailsView1.DataItem, "CntTimeSet"));
              string strDate = dtDateSet.ToString("dMMMyy");

              lblDateSet.Text = strDate + " (" + strTimeSet + ")";


        }



   }
    
}

