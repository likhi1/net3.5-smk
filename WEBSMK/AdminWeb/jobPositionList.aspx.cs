﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;
using System.Collections;



public partial class adminWeb_jobPositionList : System.Web.UI.Page
{
    public string CatId { get { return Request.QueryString["cat"]; } }
    public string SearchKey { get { return tbxSearch.Text; } } 

    protected void Page_Load(object sender, EventArgs e) {
        btnAddContent.Attributes.Add("OnClick", "location='jobPositionAdd.aspx?cat=" + CatId + "';return false; ");
        lblPageName.Text = (Convert.ToInt32( CatId) == 1 ? "พนักงาน" : "ตัวแทน" ) ;

       

        if (!Page.IsPostBack) {
           
            BindData();
        } 

    }

    protected void BindData() {
      

        //====== Set Paging
        GridView1.AllowPaging = true;
        GridView1.PageSize = 20; 

        GridView1.DataSource = SelectData();
        GridView1.DataBind();

    }


    protected DataSet SelectData() {
        string sql = "SELECT * FROM  tbJobPosi WHERE JobCatId='"+CatId+"' ";
        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql, "tb");

        return ds;
    }


    protected DataSet SearchData(string catId, string searchKey) {
        string sql = "SELECT *  FROM tbJobPosi  "

        + "WHERE  JobCatId = '" + catId + "'  ";

        if (searchKey != "") {
            sql += "AND ( "
                + " JobPosiName  LIKE  '%" + searchKey + "%'  "
                + " OR  JobPosiBranch  LIKE  '%" + searchKey + "%'  "
                + " OR  JobPosiDetail  LIKE  '%" + searchKey + "%'  "
                + " )  ";
        }

        sql += "ORDER BY JobPosiId  DESC ";


        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql, "tb");
        return ds;

    }

 
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e) {

        //========  (Hidde For  Check   ddlCatName  Select  ) ===================================
          Label lblHdStatus = (Label)e.Row.FindControl("lblHdStatus");
        if (lblHdStatus != null) {
            lblHdStatus.Text = DataBinder.Eval(e.Row.DataItem, "JobPosiStatus").ToString();
        }

        Label lblHdSort = (Label)(e.Row.FindControl("lblHdSort"));
        if (lblHdSort != null) { 
            int intHdSort = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "JobPosiSort"));
            if (intHdSort != 9999 && intHdSort != 0) {
                lblHdSort.Text = intHdSort.ToString();
            } else {
                lblHdSort.Text = "";
            }
        }

        //======== EditItemTemplate ================================================= 
        DropDownList ddlStatus = (DropDownList)e.Row.FindControl("ddlStatus");
        if (ddlStatus != null) {
            ddlStatus.Items.Insert(0, new ListItem("ปิด", "0")); // add Empty Item 
            ddlStatus.Items.Insert(1, new ListItem("เปิด", "1")); // add Empty Item   
       
            ///// set select  in EditTemplate
            string x = (e.Row.FindControl("lblHdStatus") as Label).Text;
            ddlStatus.Items.FindByValue(x).Selected = true; 
        }


        //========  ItemTemplate ================================================= 
        Label lblId = (Label)(e.Row.FindControl("lblId"));
        if (lblId != null) {
          int intJobPosiId   =  Convert.ToInt32( DataBinder.Eval(e.Row.DataItem, "JobPosiId") ); 
          lblId.Text = String.Format("{0:00000}", intJobPosiId);
 
        }

        HyperLink hplName = (HyperLink)(e.Row.FindControl("hplName"));
        if (hplName != null) {
            string strId = DataBinder.Eval(e.Row.DataItem, "JobPosiId").ToString();
            hplName.Text = DataBinder.Eval(e.Row.DataItem, "JobPosiName").ToString();
            hplName.NavigateUrl = "jobPositionView.aspx?cat="+CatId+"&id=" + strId;

        }

        Label lblQnt = (Label)(e.Row.FindControl("lblQnt"));
        if (lblQnt != null) {
            lblQnt.Text = DataBinder.Eval(e.Row.DataItem, "JobPosiQnt").ToString();
        }


        Label lblBranch = (Label)(e.Row.FindControl("lblBranch"));
        if (lblBranch != null) {
            lblBranch.Text = DataBinder.Eval(e.Row.DataItem, "JobPosiBranch").ToString();
        }




        Literal ltlStatus = (Literal)(e.Row.FindControl("ltlStatus"));
        if (ltlStatus != null) {
            int intStatus = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "JobPosiStatus"));
            if (intStatus == 0)
                ltlStatus.Text = "<img src='images/check-wrong.png' > ";
            else
                ltlStatus.Text = "<img src='images/check-right.png' > ";

        }


        Label lblSort = (Label)(e.Row.FindControl("lblSort"));
        if (lblSort != null) {
            Decimal decSort = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "JobPosiSort"));
            if (decSort != 9999) {
                lblSort.Text = decSort.ToString();
            } else {
                lblSort.Text = "";
            }
        }


        Label lblDtStart = (Label)(e.Row.FindControl("lblDtStart"));
        if (lblDtStart != null) {
          DateTime dtStart =  (DateTime)DataBinder.Eval(e.Row.DataItem, "JobPosiDtStart");
          string strdtStart = dtStart.ToString("ddMMMyy", new CultureInfo("th-TH"));
            lblDtStart.Text = strdtStart;
        }


        Label lblDtStop = (Label)(e.Row.FindControl("lblDtStop"));
        if (lblDtStop != null) { 
            DateTime dtStop = (DateTime)DataBinder.Eval(e.Row.DataItem, "JobPosiDtStop");
            string strdtStop = dtStop.ToString("ddMMMyy", new CultureInfo("th-TH"));
            lblDtStop.Text = strdtStop;
        }
        

    }

    protected void btnSearch_Click(object sender, EventArgs e) {

        //======= Set Session for Search
        Session["SearchKey"] = tbxSearch.Text ; 

        Response.Redirect("jobPositionSearch.aspx?cat=" + CatId  );
 
    }
     

    //======================  Pager
    protected void GridView1_IndexChanging(object sender, GridViewPageEventArgs e) {
        GridView1.PageIndex = e.NewPageIndex;
        BindData();
    }


    //======================  Command Gridview

    protected void GridView1_Deleting(object s, GridViewDeleteEventArgs e) {
        int a = e.RowIndex;
        string sql = "DELETE  FROM  tbJobPosi WHERE JobPosiId = '" + GridView1.DataKeys[e.RowIndex].Value + "'";
        int i = new DBClass().SqlExecute(sql);
        GridView1.EditIndex = -1;
        BindData();
    } 


    protected void modEditCommand(object sender, GridViewEditEventArgs e) {
        GridView1.EditIndex = e.NewEditIndex;
        BindData();
    }

    protected void modCancelCommand(object sender, GridViewCancelEditEventArgs e) {
        GridView1.EditIndex = -1;
        BindData();
    }

    protected void modUpdateCommand(object sender, GridViewUpdateEventArgs e) { 
        DropDownList ddlStatus = (DropDownList)GridView1.Rows[e.RowIndex].FindControl("ddlStatus");
        TextBox tbxSort = (TextBox)GridView1.Rows[e.RowIndex].FindControl("tbxSort");
        Decimal decSort;


        string strSQL = "UPDATE tbJobPosi SET " +
             "JobPosiStatus = '" + ddlStatus.SelectedValue + "', " +
             "JobPosiSort = '" + (decSort = (tbxSort.Text != "") ? Convert.ToDecimal(tbxSort.Text) : 9999) + "' " +
             " WHERE  JobPosiId = '" + GridView1.DataKeys[e.RowIndex].Value + "'";

        DBClass obj = new DBClass();
        int i = obj.SqlExecute(strSQL);

        GridView1.EditIndex = -1;
        BindData();
    }


}