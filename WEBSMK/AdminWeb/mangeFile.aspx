﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminWeb/MasterPageAdmin2.master" AutoEventWireup="true" CodeFile="mangeFile.aspx.cs" Inherits="adminWeb_mangeFile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">  
    <link rel="shortcut icon" href="../favicon.ico" />   
    <link href="css/reset.css" rel="stylesheet" />
    <link href="css/layout.css" rel="stylesheet" />
<style>
.subject1 { font-size: 23px;}
/*------ nav-backend ------------------------------------------------------*/
#menuArea { position: relative; height: 38px; border-radius: 7px; padding: 0px 10px;
border-radius:5px;   /* IE9 &  Over */  
background : linear-gradient( 0deg, #0e3ce7   0%, #2f75f1 50%  );   /* IE9 &  Over */  
background-color: #2f75f1; /* IE8 & Below  */ 
behavior: url(css/PIE-1/PIE.htc); top: 0px; left: 0px; /* IE8 CSS3 HACK */
} 

/*====  mainmenu ======*/
#nav-backend { clear: both; width: 870px; height: 38px; font-family: 'RSULight'; font-size: 15px;  margin-bottom:10px; }
#nav-backend ul {  height: 38px; }
#nav-backend ul li {   position:relative ; float:left;  border-radius: 0px;   }
#nav-backend ul li a { float: left; text-decoration: none;   border-right: 1px solid #fff; color: #fff;  height: 30px;  padding:8px 5px 0px 5px  ;  }
.subNav a { float: left; text-decoration: none;  height : 28px;  border-right: 1px solid #fff; color: #fff;  height: 38px;  padding:8px 5px 0px 5px  ; 
background-image: linear-gradient(  0deg, #0e3ce7  0%, #2f75f1 50% );  /* IE9 &  Over */  
background-color: #2f75f1; /* IE8 & Below  */ }
.subNav li a:hover, .subNav  li .active { 
background-image: linear-gradient(  0deg, #1472fa  0%, #5bbdfc 85% );  /* IE9 &  Over */  
background-color:#5bbdfc;   /* IE8 & Below  */ }
#nav-backend ul li.selected {  background-color:#5793FF;  /* IE8 & Below  */} 

/*====  submenu ======*/
#nav-backend  .subNav{position:absolute; left:0px; top:38px; z-index:111; border:1px solid #ccc ; 
background-color:#fff ;   padding:10px;  padding-right: 0px; font-family: 'Tahoma' ; font-size:13px; 
padding-bottom:10px; width:140px; height:auto !important;  height: 40px; min-height:40px; } 
#nav-backend   .subNav li { clear:both;display :block; float:none; }
#nav-backend   .subNav li a{  background: url('../images/bullet1.png') no-repeat left 3px;  margin: 0px ; padding:0px; font-size:13px;  text-indent:15px; height : 20px;  color:#666;    } 
#nav-backend   .subNav li a:hover{ color:Blue; background: url('../images/bullet1.png') no-repeat left -17px;  } 
#nav-backend   .subNav-05  {width:180px;   }   
</style>
<script src="../scripts/jquery-1.7.1.js"></script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
   
     <h1 class="subject1"> จัดการไฟล์  </h1>  
    <script src="../ckfinder/ckfinder.js"></script>

	<script type="text/javascript">

	    // This is a sample function which is called when a file is selected in CKFinder.
	    function showFileInfo(fileUrl, data, allFiles) {
	        var msg = 'The last selected file is: <a href="' + fileUrl + '">' + fileUrl + '</a><br /><br />';
	        // Display additional information available in the "data" object.
	        // For example, the size of a file (in KB) is available in the data["fileSize"] variable.
	        if (fileUrl != data['fileUrl'])
	            msg += '<b>File url:</b> ' + data['fileUrl'] + '<br />';
	        msg += '<b>File size:</b> ' + data['fileSize'] + 'KB<br />';
	        msg += '<b>Last modified:</b> ' + data['fileDate'];

	        if (allFiles.length > 1) {
	            msg += '<br /><br /><b>Selected files:</b><br /><br />';
	            msg += '<ul style="padding-left:20px">';
	            for (var i = 0 ; i < allFiles.length ; i++) {
	                // See also allFiles[i].url
	                msg += '<li>' + allFiles[i].data['fileUrl'] + ' (' + allFiles[i].data['fileSize'] + 'KB)</li>';
	            }
	            msg += '</ul>';
	        }
	        // this = CKFinderAPI object
	        this.openMsgDialog("Selected file", msg);
	    }

	    // You can use the "CKFinder" class to render CKFinder in a page:
	    var finder = new CKFinder();
	    // The path for the installation of CKFinder (default = "/ckfinder/").
	    finder.basePath = '../';
	    // The default height is 400.
	    finder.height = 600;
	    // This is a sample function which is called when a file is selected in CKFinder.
	    finder.selectActionFunction = showFileInfo;
	    finder.create();

	    // It can also be done in a single line, calling the "static"
	    // create( basePath, width, height, selectActionFunction ) function:
	    // CKFinder.create( '../', null, null, showFileInfo );

	    // The "create" function can also accept an object as the only argument.
	    // CKFinder.create( { basePath : '../', selectActionFunction : showFileInfo } );

		</script>


</asp:Content>




