using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class TestQuery : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtSQL.Text = "";
        }
    }
    protected void btnExe_Click(object sender, EventArgs e)
    {
        string strError = "";
        PremiumManager cm = new PremiumManager();
        DataSet ds ;
        try
        {
            ds = cm.TestQuery(txtSQL.Text, ref strError);

            if (ds.Tables.Count < 1)
            {
                lblError.Text = strError;
                ds = new DataSet();
                ds.Tables.Add("");
                gvData.DataSource = ds;
                gvData.DataBind();

                if (!txtSQL.Text.ToLower().StartsWith("select"))
                {
                    lblError.Text = txtSQL.Text.Split(' ')[0] + " completed";
                }
            }
            else
            {
                lblError.Text = "";
                if (ds.Tables[0].Rows.Count == 0)
                {
                    lblError.Text = "Not found data.";
                    ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                }
                gvData.DataSource = ds;
                gvData.DataBind();
            }
        }
        catch(Exception ex)
        {
            lblError.Text = ex.ToString();
        }
        
    }
}
