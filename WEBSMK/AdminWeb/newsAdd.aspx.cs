﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization; 


public partial class adminWeb_newsAdd : System.Web.UI.Page
{ 
   protected string  CatId { get { return Request.QueryString["cat"]; } }
   protected DataTable _dtCat  ;
   protected DataTable DtCat  {  get { return _dtCat ; } }
   protected string PageName;

    protected void Page_Load(object sender, EventArgs e) {

       //======= Check Session 
       //new CheckSess().CurrentSession();   
       //=======  Set PageName 
       //Utility obj = new Utility();
       //string strPageName = obj.SetPagenameNews(CatId); 
      
       if (!Page.IsPostBack) {
            SelectCat();
            DdlCatBindData(); 
            GetNamePage(); 
            lblPageName.Text = PageName; //  Set page name 
        }
        
    }

    protected DataTable  SelectCat() {
        string sql = "SELECT  NewsCatId  ,  NewsCatName  FROM tbNewsCat  ORDER BY NewsCatId ";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "tbNewsCaT");
                _dtCat = ds.Tables[0];
        return  _dtCat;

    }

    protected void  DdlCatBindData() {
        ddlCat.DataSource = DtCat;
        //**** Add frist Item of dropdownlist
        ddlCat.AppendDataBoundItems = true;
        ddlCat.DataTextField = "NewsCatName";
        ddlCat.DataValueField = "NewsCatId";
        ddlCat.DataBind();

        //======= set read only on Cat DropDownlist
        ddlCat.SelectedValue = CatId;
        ddlCat.Attributes.Add("disabled", "true");   
    }


    protected string GetNamePage() {  
        foreach ( DataRow r in DtCat.Rows ){
            if (r[0].ToString() == CatId) {
                PageName = r[1].ToString();
            }
        } 
        return PageName;
    }

    

    protected void btnSave_Click(object sender, EventArgs e) {

        //string ip = System.Web.HttpContext.Current.Request.UserHostAddress;  

        ////// Use If  datePicker send format = 25/06/2013
        DateTime dtDateShow = Convert.ToDateTime(tbxDateShow.Text, new CultureInfo("th-TH")); 
        DateTime dtDateHide = Convert.ToDateTime(tbxDateHide.Text, new CultureInfo("th-TH"));
         
        string sql1 = "INSERT INTO tbNewsCon "
                    + "VALUES (@NewsCatId,@NewsConTopic,@NewsConDetailShort,@NewsConDetailLong,"
                    + "@NewsConDtCreate,@NewsConDtShow,@NewsConDtHide,"
                    + "@NewsConStatus,@NewsConSort,@NewsConIp,@NewsPicSmall,@NewsPicBig,@NewsVdo, "
					 //===== AdminInfo Add
					 + " @lang , "
					 + " @input_date ,   "
					 + " @input_user  "
					 + " ) ";
       
        Decimal decSort;

        SqlParameterCollection objParam1 = new SqlCommand().Parameters;
        objParam1.AddWithValue("@NewsCatId",  CatId );  // CatId
        objParam1.AddWithValue("@NewsConSort",  ( decSort = (tbxSort.Text != "")?Convert.ToDecimal(tbxSort.Text):9999 ) );
        objParam1.AddWithValue("@NewsConStatus",  rblStatus.SelectedValue ); 
        objParam1.AddWithValue("@NewsConDtCreate",  DateTime.Today  );
        objParam1.AddWithValue("@NewsConDtShow",  dtDateShow );
        objParam1.AddWithValue("@NewsConDtHide", dtDateHide );
        objParam1.AddWithValue("@NewsConTopic",  tbxTopic.Text );
        objParam1.AddWithValue("@NewsConDetailShort", tarDetailShort.Text );
        objParam1.AddWithValue("@NewsConDetailLong",   tarDetailLong.Text );
        objParam1.AddWithValue("@NewsConIp",  "");
        
        //===== AdminInfo Add 
        objParam1.AddWithValue("@lang", '0' ); // TH = 0 ,  En =  1
        objParam1.AddWithValue("@input_date", DateTime.Now);
        //////Get UserName From Session Object
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        objParam1.AddWithValue("@input_user", loginData.loginName);
        

        /////// Get Value From Web Control
        objParam1.AddWithValue("@NewsPicSmall",  "" );
        if (CatId != "3") {  // CatId  3 As TVC
            objParam1.AddWithValue("@NewsPicBig", tbxPicBig.Text);
            objParam1.AddWithValue("@NewsVdo", "");
        } else {
            objParam1.AddWithValue("@NewsPicBig", "");
            objParam1.AddWithValue("@NewsVdo", tarVdo.Text);
        }
 
         
        /////// Get Value From Html Control 
        //objParam1.AddWithValue("@NewsPicSmall", SqlDbType.NVarChar).Value = Request.Form["PicSmall"].ToString();
        //objParam1.AddWithValue("@NewsPicBig", SqlDbType.NVarChar).Value = Request.Form["PicPic"].ToString();
        

        int i1 = new DBClass().SqlExecute(sql1, objParam1);

 

        if (i1 == 1) {
            Response.Write("<script>alert('เพิ่มข้อมูลเรียบร้อยแล้ว'); location='newsList.aspx?cat=" + CatId + "' ;</script>");
        } else {
            Response.Write("<script>alert('ไม่สามารถเพิ่มข้อมูลได้'); history.back() ;</script>");

        }



    }

}