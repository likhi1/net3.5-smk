<%@ Page Title="" Language="C#" MasterPageFile="~/AdminWeb/MasterPageAdmin1.master" AutoEventWireup="true" CodeFile="UpdateWebPremiumForm.aspx.cs" Inherits="AdminWeb_UpdateWebPremiumForm" %>
<%@ Register src="uc/ucTxtDate.ascx" tagname="ucTxtDate" tagprefix="uc1" %>
<%@ Register src="uc/ucPageNavigator.ascx" tagname="ucPageNavigator" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div>
        <asp:Label ID="lblSessShow" runat="server"> </asp:Label> 
        <h1 class="subject1">�Ѵ������� Web</h1>       
        <table style="width:100%;">
            <tr>
                <td class="label">                   
                    ��Ե�ѳ�� :</td>
                <td colspan="2">
                    <asp:DropDownList ID="ddlProduct" runat="server" style="margin-bottom: 0px">                        
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="label">
                    ����ö :</td>
                <td>
                    <asp:DropDownList ID="ddlCarCode" runat="server" style="margin-bottom: 0px">                        
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="label">
                    ��������û�Сѹ��� :</td>
                <td>
                    <asp:DropDownList ID="ddlPolType" runat="server" style="margin-bottom: 0px">                        
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="label">
                    ����ö����� :</td>
                <td>
                    <asp:TextBox ID="txtFromCarAge" runat="server" CssClass="TEXTBOXMONEY" Width="40" Text="1"></asp:TextBox>
                </td>
                <td class="label">
                    �֧ :</td>
                <td>
                    <asp:TextBox ID="txtToCarAge" runat="server" CssClass="TEXTBOXMONEY" Width="40" Text="10"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label">
                    �����ö :</td>
                <td>
                    <asp:DropDownList ID="ddlCarGroup" runat="server" style="margin-bottom: 0px">                        
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="label">
                    car mak :</td>
                <td>
                    <asp:TextBox ID="txtCarMak" runat="server" CssClass="TEXTBOX" Width="100"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label">
                    ��Ҵö¹�� :</td>
                <td>
                    <asp:TextBox ID="txtCarSize" runat="server" CssClass="TEXTBOXMONEY" Width="100"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label">
                    Car body :</td>
                <td>
                    <asp:TextBox ID="txtCarBody" runat="server" CssClass="TEXTBOXMONEY" Width="100"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label">
                    ���ʵ��᷹ :</td>
                <td>
                    <asp:TextBox ID="txtAgentCode" runat="server" CssClass="TEXTBOX" Width="100"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label">
                    �ع��Сѹ��µ���� :</td>
                <td>
                    <asp:TextBox ID="txtFromSumInsured" runat="server" CssClass="TEXTBOXMONEY" Width="100" Text="300,000"></asp:TextBox>
                </td>
                <td class="label">
                    �ع��Сѹ��¶֧ :</td>
                <td>
                    <asp:TextBox ID="txtToSumInsured" runat="server" CssClass="TEXTBOXMONEY" Width="100" Text="400,000"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label">
                    ���ؼ��Ѻ��� :</td>
                <td>
                    <asp:DropDownList ID="ddlDriverAge" runat="server" style="margin-bottom: 0px">                        
                    </asp:DropDownList>
                </td>
            </tr>            
            <tr>
                <td class="label">
                    �ѹ����������ҹ����� :</td>
                <td>                    
                    <uc1:ucTxtDate ID="txtFromStartDate" runat="server" />                    
                </td>
                <td class="label">
                    �֧ :</td>
                <td>                    
                    <uc1:ucTxtDate ID="txtToStartDate" runat="server" />                    
                </td>
            </tr>
            <tr>
                <td class="label">
                    �ѹ�������ش����� :</td>
                <td>
                    <uc1:ucTxtDate ID="txtFromStopDate" runat="server" />
                </td>
                <td class="label">
                    �֧ :</td>
                <td>
                    <uc1:ucTxtDate ID="txtToStopDate" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="label">
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btnSearch" runat="server" Text="Search" onclick="btnSearch_Click" />
                    <asp:Button ID="Button2" runat="server" Text="Cancle" />
                </td>
            </tr>
        </table>      
        <br />
        <br />  
        <div id="divResult" style="display:none">
        <table width="100%"> 
            <tr>
                <td colspan="4">                    
                    <uc2:ucPageNavigator ID="ucPageNavigator1" runat="server" OnPageIndexChanged="PageIndexChanged"/>                    
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <a id="aDtgData"></a>
                    <asp:DataGrid ID="dtgData" runat="server" AutoGenerateColumns="false" Width="100%"
                        HeaderStyle-CssClass="gridhead" ItemStyle-CssClass="gridrow0" AlternatingItemStyle-CssClass="gridrow1"
                        AllowPaging="true" PagerStyle-Visible="false">
                        <Columns>
                            <asp:TemplateColumn>  
                                <HeaderTemplate><%#ColumnTitle[0]%></HeaderTemplate>
                                <ItemTemplate>
                                    <%# Container.DataSetIndex +1%>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>
				                <HeaderTemplate>
					                <asp:LinkButton ID="lnkID1" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[1]%>">
					                </asp:LinkButton>
					                <asp:Image ID="imgSortCol1" Runat="server" ImageUrl="<%#getPicUrl(1)%>">
					                </asp:Image>
				                </HeaderTemplate>
				                <ItemTemplate>
				                    <%#(Eval(ColumnName[1]))%>
				                </ItemTemplate>
				            </asp:TemplateColumn>
                            <asp:TemplateColumn>
				                <HeaderTemplate>
					                <asp:LinkButton ID="lnkID2" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[2]%>">
					                </asp:LinkButton>
					                <asp:Image ID="imgSortCol2" Runat="server" ImageUrl="<%#getPicUrl(2)%>">
					                </asp:Image>
				                </HeaderTemplate>
				                <ItemTemplate>
				                    <%#(Eval(ColumnName[2]))%>
				                </ItemTemplate>
			                </asp:TemplateColumn>
			                <asp:TemplateColumn>
				                <HeaderTemplate>
					                <asp:LinkButton ID="lnkID3" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[3]%>">
					                </asp:LinkButton>
					                <asp:Image ID="imgSortCol3" Runat="server" ImageUrl="<%#getPicUrl(3)%>">
					                </asp:Image>
				                </HeaderTemplate>
				                <ItemTemplate>
				                    <%#(Eval(ColumnName[3]))%>
				                </ItemTemplate>			                
			                </asp:TemplateColumn>
			                <asp:TemplateColumn>
				                <HeaderTemplate>
					                <asp:LinkButton ID="lnkID4" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[4]%>">
					                </asp:LinkButton>
					                <asp:Image ID="imgSortCol4" Runat="server" ImageUrl="<%#getPicUrl(4)%>">
					                </asp:Image>
				                </HeaderTemplate>
				                <ItemTemplate>
				                    <%#(Eval(ColumnName[4]))%>
				                </ItemTemplate>			                
			                </asp:TemplateColumn>
			                <asp:TemplateColumn>
				                <HeaderTemplate>
					                <asp:LinkButton ID="lnkID5" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[5]%>">
					                </asp:LinkButton>
					                <asp:Image ID="imgSortCol5" Runat="server" ImageUrl="<%#getPicUrl(5)%>">
					                </asp:Image>
				                </HeaderTemplate>
				                <ItemTemplate>
				                    <%#(Eval(ColumnName[5]))%>
				                </ItemTemplate>			                
			                </asp:TemplateColumn>
			                <asp:TemplateColumn>
				                <HeaderTemplate>
					                <asp:LinkButton ID="lnkID6" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[6]%>">
					                </asp:LinkButton>
					                <asp:Image ID="imgSortCol6" Runat="server" ImageUrl="<%#getPicUrl(6)%>">
					                </asp:Image>
				                </HeaderTemplate>
				                <ItemTemplate>
				                    <%#(Eval(ColumnName[6]))%>
				                </ItemTemplate>			                
			                </asp:TemplateColumn>
			                <asp:TemplateColumn>
				                <HeaderTemplate>
					                <asp:LinkButton ID="lnkID7" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[7]%>">
					                </asp:LinkButton>
					                <asp:Image ID="imgSortCol7" Runat="server" ImageUrl="<%#getPicUrl(7)%>">
					                </asp:Image>
				                </HeaderTemplate>
				                <ItemTemplate>
				                    <%#FormatStringApp.FormatInt(Eval(ColumnName[7]))%>
				                </ItemTemplate>			                
			                </asp:TemplateColumn>
			                <asp:TemplateColumn>
				                <HeaderTemplate>
					                <asp:LinkButton ID="lnkID8" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[8]%>">
					                </asp:LinkButton>
					                <asp:Image ID="imgSortCol8" Runat="server" ImageUrl="<%#getPicUrl(8)%>">
					                </asp:Image>
				                </HeaderTemplate>
				                <ItemTemplate>
				                    <%#(Eval(ColumnName[8]))%>
				                </ItemTemplate>			                
			                </asp:TemplateColumn>
			                <asp:TemplateColumn>
				                <HeaderTemplate>
					                <asp:LinkButton ID="lnkID9" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[9]%>">
					                </asp:LinkButton>
					                <asp:Image ID="imgSortCol9" Runat="server" ImageUrl="<%#getPicUrl(9)%>">
					                </asp:Image>
				                </HeaderTemplate>
				                <ItemTemplate>
				                    <%#(Eval(ColumnName[9]))%>
				                </ItemTemplate>			                
			                </asp:TemplateColumn>
			                <asp:TemplateColumn>
				                <HeaderTemplate>
					                <asp:LinkButton ID="lnkID10" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[10]%>">
					                </asp:LinkButton>
					                <asp:Image ID="imgSortCol10" Runat="server" ImageUrl="<%#getPicUrl(10)%>">
					                </asp:Image>
				                </HeaderTemplate>
				                <ItemTemplate>
				                    <%#FormatStringApp.FormatInt(Eval(ColumnName[10]))%>
				                </ItemTemplate>			                
			                </asp:TemplateColumn>
			                <asp:TemplateColumn>
				                <HeaderTemplate>
					                <asp:LinkButton ID="lnkID11" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[11]%>">
					                </asp:LinkButton>
					                <asp:Image ID="imgSortCol11" Runat="server" ImageUrl="<%#getPicUrl(11)%>">
					                </asp:Image>
				                </HeaderTemplate>
				                <ItemTemplate>
				                    <%#(Eval(ColumnName[11]))%>
				                </ItemTemplate>			                
			                </asp:TemplateColumn>
			                <asp:TemplateColumn>
				                <HeaderTemplate>
					                <asp:LinkButton ID="lnkID12" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[12]%>">
					                </asp:LinkButton>
					                <asp:Image ID="imgSortCol12" Runat="server" ImageUrl="<%#getPicUrl(12)%>">
					                </asp:Image>
				                </HeaderTemplate>
				                <ItemTemplate>
				                    <%#FormatStringApp.FormatDate(Eval(ColumnName[12]))%>
				                </ItemTemplate>			                
			                </asp:TemplateColumn>
			                <asp:TemplateColumn>
				                <HeaderTemplate>
					                <asp:LinkButton ID="lnkID13" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[13]%>">
					                </asp:LinkButton>
					                <asp:Image ID="imgSortCol13" Runat="server" ImageUrl="<%#getPicUrl(13)%>">
					                </asp:Image>
				                </HeaderTemplate>
				                <ItemTemplate>
				                    <%#FormatStringApp.FormatDate(Eval(ColumnName[13]))%>
				                </ItemTemplate>			                
			                </asp:TemplateColumn>
			                <asp:TemplateColumn>
				                <HeaderTemplate>
					                <asp:LinkButton ID="lnkID14" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[14]%>">
					                </asp:LinkButton>
					                <asp:Image ID="imgSortCol14" Runat="server" ImageUrl="<%#getPicUrl(14)%>">
					                </asp:Image>
				                </HeaderTemplate>
				                <ItemTemplate>
				                    <%#FormatStringApp.Format2Digit(Eval(ColumnName[14]))%>
				                </ItemTemplate>			                
			                </asp:TemplateColumn>
			                <asp:TemplateColumn>
				                <HeaderTemplate>
					                <asp:LinkButton ID="lnkID15" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[15]%>">
					                </asp:LinkButton>
					                <asp:Image ID="imgSortCol15" Runat="server" ImageUrl="<%#getPicUrl(15)%>">
					                </asp:Image>
				                </HeaderTemplate>
				                <ItemTemplate>
				                    <%#FormatStringApp.Format2Digit(Eval(ColumnName[15]))%>
				                </ItemTemplate>			                
			                </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
            <tr>
                <td class="label">
                    �ѹ����������ҹ :</td>
                <td>                    
                    <uc1:ucTxtDate ID="txtUpdateStartDate" runat="server" />                    
                </td>
                <td class="label"></td>
                <td></td>
            </tr>
            <tr>
                <td class="label">
                    �ѹ�������ش :</td>
                <td>
                    <uc1:ucTxtDate ID="txtUpdateStopDate" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="label">
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" onclick="btnDelete_Click" />
                    <asp:Button ID="btnUpdate" runat="server" Text="Update" onclick="btnUpdate_Click"/>
                    <asp:Button ID="Button1" runat="server" Text="Cancle" />
                </td>
            </tr>
        </table>
        </div>
        <br />
        <br />
        
        </div> <!-- end  id="page-prdList" -->
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>


