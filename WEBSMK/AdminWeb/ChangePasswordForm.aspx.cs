﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Module_Authenticate_ChangePasswordForm : System.Web.UI.Page
{
    public static string strAppName = System.Configuration.ConfigurationManager.AppSettings["AppName"].ToString();
    public string strPostBack = "";
    public string[] ColumnPopupName = { "", "EMPCODE", "FNAMEE", "NAME" };
    public string[] ColumnPopupType = { "string", "string", "string", "string"};
    public string[] ColumnPopupTitle = { "No.", "รหัสพนักงาน", "ชื่อLogin", "ชื่อ-นามสกุล" };
    string[] ColumnPopupWidth = { "10%", "20%", "30%", "40%" };
    string[] ColumnPopupAlign = { "center", "center", "left", "left" };
    DataView dv = new DataView();

    protected void Page_Load(object sender, EventArgs e)
    {
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        AuthenticateManager cm = new AuthenticateManager();
        DataSet dsData;
        if (!IsPostBack)
        {
        }
    }    
    protected void btnConfirmChangePwd_Click(object sender, EventArgs e)
    {
        string strRet = "";        
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        AuthenticateManager cm = new AuthenticateManager();

        litJavaScript.Text += "<script>";
        strRet = cm.editDataPassword(loginData.loginName, txtNewPassword.Text, loginData.loginName,"N");
        if (strRet.StartsWith("ERROR"))
        {
            litJavaScript.Text += "alert('" + strRet.Split(':')[1] + "');";
        }
        else
        {
            litJavaScript.Text += "alert('บันทึกข้อมูลเรียบร้อย');";
        }
        litJavaScript.Text += "</script>";
    }         
    public bool validateForm()
    {
        bool isRet = true;
        DataSet ds;
        AuthenticateManager cm = new AuthenticateManager();
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        ds = cm.getDataUserByUserLogin(loginData.loginName);
        if (ds.Tables[0].Rows.Count > 0)
        {
            isRet = false;
        }
        return isRet;
    }
}
