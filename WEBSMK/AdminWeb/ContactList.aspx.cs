﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class AdminWeb_ContactList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
        }
        else
        {
            litScript.Text = "";
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        DataSet dsData = new DataSet();
        RequestPolicyManager cm = new RequestPolicyManager();
        dsData = cm.searchContactData(ddlStatus.SelectedValue);
        ViewState["dsDataGrid"] = dsData;
        ViewState["status"] = ddlStatus.SelectedValue;
        ShowData();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        RequestPolicyManager cm = new RequestPolicyManager();
        DataSet dsData;
        DataRow drData;
        string[] columnNames = { "status", "ct_code" };
        string[] columnTypes = { "string", "string" };
        RadioButton rdoHold;
        RadioButton rdoApprove;

        dsData = new DataSet();
        dsData.Tables.Add();
        TDS.Utility.MasterUtil.AddColumnToDataTable(dsData.Tables[0], columnTypes, columnNames);
        for (int i = 0; i <= dtgData.Items.Count - 1; i++)
        {
            drData = dsData.Tables[0].NewRow();
            drData["ct_code"] = dtgData.Items[i].Cells[0].Text;
            rdoHold = (RadioButton)dtgData.Items[i].Cells[1].FindControl("rdoHold");
            rdoApprove = (RadioButton)dtgData.Items[i].Cells[2].FindControl("rdoApprove");
            if (rdoHold != null && rdoApprove != null)
            {
                if (rdoHold.Checked)
                    drData["status"] = "O";
                else if (rdoApprove.Checked)
                    drData["status"] = "F";
            }
            dsData.Tables[0].Rows.Add(drData);
        }
        if (cm.editContactData(dsData))
        {
            litScript.Text = "<script>alert(\"บันทึกข้อมูลเรียบร้อย\")</script>'"; 
        }
        else
        {
            litScript.Text = "<script>alert(\"ไม่สามารถบันทึกข้อมูลได้\")</script>'";
        }
        ViewState["dsApprove"] = dsData;
        if (ViewState["status"] != null)
        {
            dsData = cm.searchContactData(ViewState["status"].ToString());
            ViewState["dsDataGrid"] = dsData;
            ShowData();
        }
    }
    private void ShowData()
    {
        DataSet ds = new DataSet();
        DataRow dr;
        DataView dv;
        if (ViewState["dsDataGrid"] != null)
        {
            ds = (DataSet)ViewState["dsDataGrid"];
        }


        string sortBy = "";
        if (ViewState["SortString"] != null)
        {
            sortBy = (string)ViewState["SortString"];
        }

        dv = new DataView();
        if (ds.Tables.Count > 0)
        {
            dv.Table = ds.Tables[0];
            dv.Sort = sortBy;

            int newPageCount;
            newPageCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(dv.Table.Rows.Count) / Convert.ToDouble(dtgData.PageSize)));
            if (dtgData.CurrentPageIndex >= newPageCount && newPageCount > 0)
            {
                dtgData.CurrentPageIndex = newPageCount - 1;
            }
        }
        dtgData.DataSource = dv;
        dtgData.DataBind();
        // set lastpage, curpage, fromitem, toitem, allitem to ucPage
        //ucPageNavigator1.LastPage = dtgData.PageCount.ToString();
        //ucPageNavigator1.CurPage = Convert.ToString(dtgData.CurrentPageIndex + 1);
        //ucPageNavigator1.FromItem = Convert.ToString((dtgData.CurrentPageIndex * dtgData.PageSize) + 1);
        //ucPageNavigator1.ToItem = Convert.ToString((dtgData.CurrentPageIndex * dtgData.PageSize) + dtgData.PageSize);
        //if ((dtgData.CurrentPageIndex * dtgData.PageSize) + dtgData.PageSize > dv.Count)
        //{
        //    ucPageNavigator1.ToItem = dv.Count.ToString();
        //}
        //ucPageNavigator1.AllItem = dv.Count.ToString();
    }
    public bool showStatusHold(string dataStatus)
    {
        bool isRet = false;
        if (dataStatus == "O")
            isRet = true;
        return isRet;
    }
    public bool showStatusApprove(string dataStatus)
    {
        bool isRet = false;
        if (dataStatus == "F")
        {
            isRet = true;
        }
        return isRet;
    }
    public bool showEnabled(string dataStatus)
    {
        bool isRet = true;
        if (dataStatus == "F")
        {
            isRet = false;
        }
        return isRet;
    }
}
