﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


public partial class adminWeb_main  : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e) {

        if (!Page.IsPostBack)
        {
            txtUserName.Focus();
            Session.RemoveAll();
            ViewState.Clear();
        }
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        string strRet = "";
        string menuList = "";
        ProfileData loginData;
        DataSet ds;
        DataSet dsMenu;
        DataSet dsTmp;
        DataSet dsBranch;
        string strBranch = "";
        AuthenticateManager cm = new AuthenticateManager();
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        strRet = cm.doLogin(txtUserName.Text, txtPassword.Text);
        litScript.Text = "";
        if (strRet.StartsWith("1"))
        {
            litScript.Text += "alert('" + strRet.Split(':')[1] + "');\n";
        }
        else if (strRet.StartsWith("2"))
        {
            litScript.Text += "alert('" + strRet.Split(':')[1] + "');\n";
            txtPassword.Focus();
        }
        else if (strRet.StartsWith("3"))
        {
            litScript.Text += "alert('" + strRet.Split(':')[1] + "');\n";
            txtUserName.Focus();
        }
        else if (strRet.StartsWith("0"))
        {
            litScript.Text += "alert('" + strRet.Split(':')[1] + "');\n";
            litScript.Text += "document.getElementById('divChangePassword').style.display = 'inline';\n";
        }

        if (litScript.Text == "")
        {
            loginData = new ProfileData();
            ds = cm.getDataUserByLogin(txtUserName.Text);
            loginData = new ProfileData();
            if (ds.Tables[0].Rows.Count > 0)
            {
                loginData.loginName = ds.Tables[0].Rows[0]["user_login"].ToString();
                loginData.userName = ds.Tables[0].Rows[0]["user_fullname"].ToString();
                loginData.roleID = ds.Tables[0].Rows[0]["role_id"].ToString();
                loginData.roleName = ds.Tables[0].Rows[0]["role_name"].ToString();

                Session.Add("SessAdmin", loginData);
                //if ((!Convert.IsDBNull(ds.Tables[0].Rows[0]["user_changepwddate"])) &&
                //    (ds.Tables[0].Rows[0]["user_changepwddate"].ToString() != "") &&
                //    (ds.Tables[0].Rows[0]["user_flagexpriepwd"].ToString() == "Y") &&
                //    (cmDate.daysDiff(TDS.Utility.MasterUtil.getDateString(DateTime.Now, "dd/MM/yyyy", "th-TH"), TDS.Utility.MasterUtil.getDateString(((DateTime)ds.Tables[0].Rows[0]["user_changepwddate"]).AddDays(Convert.ToDouble(ds.Tables[0].Rows[0]["user_expirepwddays"])), "dd/M/yyyy", "th-TH")) <= 10))
                //{
                //    litScript.Text = "<script>\n";
                //    litScript.Text += "alert('รหัสผ่านเหลือเวลาอีก " + cmDate.daysDiff(TDS.Utility.MasterUtil.getDateString(DateTime.Now, "dd/MM/yyyy", "th-TH"), TDS.Utility.MasterUtil.getDateString(((DateTime)ds.Tables[0].Rows[0]["user_changepwddate"]).AddDays(Convert.ToDouble(ds.Tables[0].Rows[0]["user_expirepwddays"])), "dd/M/yyyy", "th-TH")).ToString() + "วัน \\nกรุณาเปลี่ยนรหัสผ่านใหม่');\n";
                //    litScript.Text += "window.location = 'mainPage.aspx'\n";
                //    litScript.Text += "</script>\n";
                //}
                //else
                //{
                Response.Redirect("adminHome.aspx");
                //}
            }
            else
            {
                litScript.Text += "alert('ไม่สามารถเข้าใช้งานระบบได้');\n";
                litScript.Text = "<script>\n" + litScript.Text;
                litScript.Text += "</script>\n";
            }
        }
        else
        {
            litScript.Text = "<script>\n" + litScript.Text;
            litScript.Text += "</script>\n";
        }
    }

  
}//end class
