﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class buyInsurePA_step4 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        RegisterManager cmRegister = new RegisterManager();
        if (Request.QueryString["regis_no"] != "")
        {
            ds = cmRegister.getDataPersonelAccident(Request.QueryString["regis_no"]);
        }
        else
        {
            Response.Redirect("buyInsurePA_step1.aspx", true);
        }

        if (!IsPostBack)
        {
            docToUI(ds);
        }
    }
    public void docToUI(DataSet ds)
    {
        DataSet dsPrem = new DataSet();
        DataSet dsTmp = new DataSet();
        OnlineVoluntaryManager cmOnline = new OnlineVoluntaryManager();
        NonMotorManager cmNon = new NonMotorManager();

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            lblRefNo.Text = ds.Tables[0].Rows[0]["rg_regis_no"].ToString();
            // รายละเอียดผู้เอาประกัน
            lblFName.Text = ds.Tables[0].Rows[0]["rg_ins_fname"].ToString();
            lblLName.Text = ds.Tables[0].Rows[0]["rg_ins_lname"].ToString();
            lblAddress.Text = ds.Tables[0].Rows[0]["rg_ins_addr1"].ToString();
            lblSubDistrict.Text = ds.Tables[0].Rows[0]["rg_ins_addr2"].ToString();
            lblDistrict.Text = ds.Tables[0].Rows[0]["rg_ins_amphor"].ToString();
            dsTmp = cmOnline.getDataProvinceByCode(ds.Tables[0].Rows[0]["rg_ins_changwat"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                lblProvince.Text = dsTmp.Tables[0].Rows[0]["ds_desc"].ToString();
            lblPostCode.Text = ds.Tables[0].Rows[0]["rg_ins_postcode"].ToString();
            //lblIDCard.Text = ds.Tables[0].Rows[0]["rg_ins_idcard"].ToString();
            //lblMobile.Text = ds.Tables[0].Rows[0]["rg_ins_mobile"].ToString();
            lblTelephone.Text = ds.Tables[0].Rows[0]["rg_ins_tel"].ToString();
            lblEmail.Text = ds.Tables[0].Rows[0]["rg_ins_email"].ToString();
            dsTmp = cmNon.getDataOccupationByCode(ds.Tables[0].Rows[0]["pe_occup"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                lblOccupy.Text = dsTmp.Tables[0].Rows[0]["ds_desc"].ToString();
            lblBirthDate.Text = FormatStringApp.FormatDate(ds.Tables[0].Rows[0]["pe_birth_dt"]);
            lblStartDate.Text = FormatStringApp.FormatDate(ds.Tables[0].Rows[0]["rg_effect_dt"]);
            lblStopDate.Text = FormatStringApp.FormatDate(ds.Tables[0].Rows[0]["rg_expiry_dt"]);

            // รายละเอียดผู้รับประโยชน์
            lblBefFName.Text = ds.Tables[0].Rows[0]["pe_ben_fname"].ToString();
            lblBefLName.Text = ds.Tables[0].Rows[0]["pe_ben_lname"].ToString();
            dsTmp = cmNon.getDataRelationByCode(ds.Tables[0].Rows[0]["pe_relation"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                lblRelationship.Text = dsTmp.Tables[0].Rows[0]["ds_desc"].ToString();
            lblBefAddress1.Text = ds.Tables[0].Rows[0]["pe_ben_addr1"].ToString();
            lblBefAddress2.Text = ds.Tables[0].Rows[0]["pe_ben_addr2"].ToString();
            lblBefDistrict.Text = ds.Tables[0].Rows[0]["pe_ben_amphor"].ToString();
            dsTmp = cmOnline.getDataProvinceByCode(ds.Tables[0].Rows[0]["pe_ben_changwat"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                lblBefProvince.Text = dsTmp.Tables[0].Rows[0]["ds_desc"].ToString();
            lblBefPostCode.Text = ds.Tables[0].Rows[0]["pe_ben_postcode"].ToString();
            lblBefTelephone.Text = ds.Tables[0].Rows[0]["pe_ben_tel"].ToString();
            lblBefEmail.Text = ds.Tables[0].Rows[0]["pe_ben_email"].ToString();

            // รายละเอียดความคุ้มครองหลัก
            if (ds.Tables[0].Rows[0]["pa_type"].ToString() == "PA1")
            {
                lblPa1Si.Text = FormatStringApp.FormatMoney(ds.Tables[0].Rows[0]["pa_death_si"].ToString());
                lblPa2Si.Text = "ไม่คุ้มครอง";
            }
            else if (ds.Tables[0].Rows[0]["pa_type"].ToString() == "PA1")
            {
                lblPa2Si.Text = FormatStringApp.FormatMoney(ds.Tables[0].Rows[0]["pa_death_si"].ToString());
                lblPa1Si.Text = "ไม่คุ้มครอง";
            }
            lblTtdWeek.Text = ds.Tables[0].Rows[0]["pa_ttd_week"].ToString();
            lblTtdSi.Text = FormatStringApp.FormatMoney(ds.Tables[0].Rows[0]["pa_ttd_si"].ToString());
            lblPtdWeek.Text = ds.Tables[0].Rows[0]["pa_ttd_week"].ToString();
            lblPtdSi.Text = FormatStringApp.FormatMoney(ds.Tables[0].Rows[0]["pa_ptd_si"].ToString());
            lblMedicine.Text = FormatStringApp.FormatMoney(ds.Tables[0].Rows[0]["pa_med_si"].ToString());
            //รายละเอียดความคุ้มครองเพิ่ม 
            if (ds.Tables[0].Rows[0]["pa_war"].ToString() == "Y")
                lblWar.Visible = true;
            else
                lblWar.Visible = false;
            if (ds.Tables[0].Rows[0]["pa_str"].ToString() == "Y")
                lblStr.Visible = true;
            else
                lblStr.Visible = false;
            if (ds.Tables[0].Rows[0]["pa_spt"].ToString() == "Y")
                lblSpt.Visible = true;
            else
                lblSpt.Visible = false;
            if (ds.Tables[0].Rows[0]["pa_mtr"].ToString() == "Y")
                lblMtr.Visible = true;
            else
                lblMtr.Visible = false;
            if (ds.Tables[0].Rows[0]["pa_air"].ToString() == "Y")
                lblAir.Visible = true;
            else
                lblAir.Visible = false;
            if (ds.Tables[0].Rows[0]["pa_murder"].ToString() == "Y")
                lblMurder.Visible = true;
            else
                lblMurder.Visible = false;

            // เบี้ย 
            double dPrem = 0;
            double dStamp = 0;
            double dTax = 0;
            double dGross = 0;
            double dPaid = 0;
            dPrem = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["rg_prmm"]);
            dStamp = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["rg_stamp"]);
            dTax = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["rg_tax"]);
            dGross = dPrem + dStamp + dTax;
            dPaid = dGross;
            lblPremium.Text = FormatStringApp.FormatNDigit(ds.Tables[0].Rows[0]["rg_prmm"], 2) + " บาท";
            lblStamp.Text = FormatStringApp.FormatNDigit(ds.Tables[0].Rows[0]["rg_stamp"], 2) + " บาท";
            lblVat.Text = FormatStringApp.FormatNDigit(ds.Tables[0].Rows[0]["rg_tax"], 2) + " บาท";
            lblGrossPremium.Text = FormatStringApp.FormatNDigit(dGross, 2) + " บาท";
            dPrem = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["rg_prmm"]);
            dStamp = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["rg_stamp"]);
            dTax = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["rg_tax"]);
            dGross = dPrem + dStamp + dTax;
            dPaid = dPaid + dGross;
            //lblComPremium.Text = FormatStringApp.FormatNDigit(dGross, 2) + " บาท";
            //lblPaid.Text = FormatStringApp.FormatNDigit(dPaid, 2) + " บาท";

            // การชำระเงิน
            if (ds.Tables[0].Rows[0]["rg_pay_type"].ToString() == "1")
            {
                lblPaidMethod.Text = "ชำระเงินด้วยเงินสดหรือเช็ค ที่บริษัทสินมั่นคงประกันภัยจำกัด (มหาชน) ที่</br>";
                if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "100")
                    lblPaidMethod.Text += "สำนักงานใหญ่ (ถนนศรีนครินทร์)";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "100")
                    lblPaidMethod.Text += "สำนักงานใหญ่ (ถนนศรีนครินทร์)";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "101")
                    lblPaidMethod.Text += "สาขาสวนมะลิ (ยสเส)";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "102")
                    lblPaidMethod.Text += "สาขาดอนเมือง";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "102")
                    lblPaidMethod.Text += "สาขาบางแค";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "130")
                    lblPaidMethod.Text += "สาขารัตนาธิเบศร์";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "500")
                    lblPaidMethod.Text += "สาขาชลบุรี";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "201")
                    lblPaidMethod.Text += "สาขาพิษณุโลก";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "200")
                    lblPaidMethod.Text += "สาขาเชียงใหม่";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "601")
                    lblPaidMethod.Text += "สาขาหาดใหญ่";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "600")
                    lblPaidMethod.Text += "สาขาสุราษฎร์ธานี";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "300")
                    lblPaidMethod.Text += "สาขานครปฐม";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "401")
                    lblPaidMethod.Text += "สาขาขอนแก่น";
            }
            else if (ds.Tables[0].Rows[0]["rg_pay_type"].ToString() == "2")
            {
                lblPaidMethod.Text = "ชำระเงิน ด้วยวิธีการโอนเข้าบัญชีของบริษัทสินมั่นคงประกันภัย จำกัด (มหาชน)";
            }
            else if (ds.Tables[0].Rows[0]["rg_pay_type"].ToString() == "3")
            {
                lblPaidMethod.Text = "ชำระเงิน ด้วยวิธีการโอนเข้าบัญชีของบริษัทสินมั่นคงประกันภัย จำกัด(มหาชน) ผ่าน www.scbeasy.com";
            }
            else if (ds.Tables[0].Rows[0]["rg_pay_type"].ToString() == "4")
            {
                lblPaidMethod.Text = "บริการจัดส่งและจัดเก็บเบี้ยประกันถึงบ้าน (เฉพาะภายในเขตกรุงเทพฯ)";
            }
            else if (ds.Tables[0].Rows[0]["rg_pay_type"].ToString() == "5")
            {
                lblPaidMethod.Text = "ชำระเงิน ด้วยบัตรเครดิต";
            }
            // จัดส่งเอกสาร
            if (ds.Tables[0].Rows[0]["rg_ref_bank"].ToString() == "1")
                lblSendDoc.Text = "ที่อยู่ตามกรมธรรม์";
            else
            {
            }

        }
    }
}
