﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminWeb/MasterPageAdmin1.master" AutoEventWireup="true" CodeFile="ApproveForm.aspx.cs" Inherits="AdminWeb_ApproveForm" %>
<%@ Register src="uc/ucTxtDate.ascx" tagname="ucTxtDate" tagprefix="uc1" %>
<%@ Register src="uc/ucPageNavigator.ascx" tagname="ucPageNavigator" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script language="javascript">
        function print_click(objBtn){        
            var url = "";
            if (objBtn.getAttribute("pol_type") == "C") {
                url = "buyCompulsary_step3.aspx";
            }else if (objBtn.getAttribute("pol_type") == "P"){
                url = "buyInsurePA_step4.aspx";
            }else if (objBtn.getAttribute("pol_type") == "T"){
                url = "buyInsureTravel_step3.aspx";
            }else{
                url = "buyVoluntary_step4.aspx";
            }
            url = url + "?regis_no=" + objBtn.getAttribute("regis_no");
            url = url + "&paid_at=" + objBtn.getAttribute("paid_at");
            var owin = window.open(url,"detail","height=650,width=650,scrollbars=yes,top=20");
            owin.focus();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div>
        <asp:Label ID="lblSessShow" runat="server"> </asp:Label> 
        <h1 class="subject1">ตรวจสอบการรับแจ้งประกันภัย</h1>   
        <table width="100%" cellpadding="0px" cellspacing="0px" border="0px">
            <tr>
                <td class="globalPadding">
                    <asp:Label ID="Label2" runat="server" Text="ประเภทความคุ้มครอง"></asp:Label>
                </td>
                <td class="globalPadding">
                    <asp:DropDownList ID="ddlPolType" runat="server" CssClass="inputText">
                    </asp:DropDownList>
                    <asp:Button ID="Button1" runat="server" Text="ค้นหา" OnClick="Button1_Click" CssClass="btn-default btn-small"/></td>
            </tr>            
            <tr>
                <td class="globalPadding" colspan="2">
                    <uc2:ucPageNavigator ID="ucPageNavigator1" runat="server" OnPageIndexChanged="PageIndexChanged"/>
                </td>
            </tr>
            <tr>
                <td class="globalPadding" colspan="2">
                    <asp:DataGrid ID="dtgData" runat="server" AutoGenerateColumns="False" 
                            HeaderStyle-CssClass="gridhead" ItemStyle-CssClass="gridrow0" AlternatingItemStyle-CssClass="gridrow1"
                            BorderWidth="0px" AllowPaging="True" PageSize="10" width="98%">
                        <Columns>
                            <asp:TemplateColumn>
				                <HeaderTemplate>
					                <%#ColumnTitle[0]%>					                
				                </HeaderTemplate>
				                <ItemTemplate>
				                    <asp:RadioButton runat="server" ID="rdoHold"  GroupName="Approve" Checked=true />				                    
				                </ItemTemplate>
				            </asp:TemplateColumn>                         
				            <asp:TemplateColumn>
				                <HeaderTemplate>
					                <%#ColumnTitle[1]%>					                
				                </HeaderTemplate>
				                <ItemTemplate>
				                    <asp:RadioButton runat="server" ID="rdoApprove"  GroupName="Approve"/>
				                </ItemTemplate>
				            </asp:TemplateColumn>
				            <asp:TemplateColumn>
				                <HeaderTemplate>
					                <%#ColumnTitle[2]%>					                
				                </HeaderTemplate>
				                <ItemTemplate>
				                    <asp:RadioButton runat="server" ID="rdoReject"  GroupName="Approve" />
				                </ItemTemplate>
				            </asp:TemplateColumn>
				            <asp:TemplateColumn>
				                <HeaderTemplate>
					                <%#ColumnTitle[3]%>					                
				                </HeaderTemplate>
				                <ItemTemplate>
				                    <asp:TextBox runat="server" ID="txtBookNo" Width="30" CssClass="inputText" MaxLength="5"></asp:TextBox>
                                    <asp:TextBox runat="server" ID="txtNo" Width="38" CssClass="inputText" MaxLength="6"></asp:TextBox>
				                </ItemTemplate>
				            </asp:TemplateColumn>
				            <asp:TemplateColumn>
				                <HeaderTemplate>
					                <%#ColumnTitle[4]%>					                
				                </HeaderTemplate>
				                <ItemTemplate>
				                    <%#(Eval(ColumnName[4]))%>
				                </ItemTemplate>
				            </asp:TemplateColumn>
				            <asp:TemplateColumn>
				                <HeaderTemplate>
					                <%#ColumnTitle[5]%>					                
				                </HeaderTemplate>
				                <ItemTemplate>
				                    <%#(Eval(ColumnName[5]))%>
				                </ItemTemplate>
				            </asp:TemplateColumn>
				            <asp:TemplateColumn>
				                <HeaderTemplate>
					                <%#ColumnTitle[6]%>					                
				                </HeaderTemplate>
				                <ItemTemplate>
				                    <%#(Eval(ColumnName[6]))%>
				                </ItemTemplate>
				            </asp:TemplateColumn>
				            <asp:TemplateColumn>
				                <HeaderTemplate>
					                <%#ColumnTitle[7]%>					                
				                </HeaderTemplate>
				                <ItemTemplate>
				                    <%#FormatStringApp.FormatDate(Eval(ColumnName[7]))%>
				                </ItemTemplate>
				                <ItemStyle HorizontalAlign="Center" />
				            </asp:TemplateColumn>
                            <asp:TemplateColumn>
				                <HeaderTemplate>
					                <%#ColumnTitle[8]%>					                
				                </HeaderTemplate>
				                <ItemTemplate>
				                    <%#FormatStringApp.FormatNDigit(Eval(ColumnName[8]),2)%>
				                </ItemTemplate>
				                <ItemStyle HorizontalAlign="Right" />
				            </asp:TemplateColumn>
				            <asp:TemplateColumn>
				                <HeaderTemplate>
					                <%#ColumnTitle[9]%>					                
				                </HeaderTemplate>
				                <ItemTemplate>
				                    <%#FormatStringApp.ShowPaymentType(Eval(ColumnName[9]))%>
				                </ItemTemplate>
				            </asp:TemplateColumn>
                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <asp:Button ID="btnPrint" runat="server" Text='ดูรายละเอียด' OnClientClick="print_click(this);" paid_at='<%# DataBinder.Eval(Container, "DataItem.rg_pay_type")%>' regis_no='<%# DataBinder.Eval(Container, "DataItem.rg_regis_no")%>' pol_type='<%#DataBinder.Eval(Container, "DataItem.rg_pol_type") %>' CssClass="btn-default btn-small"></asp:Button>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <HeaderStyle CssClass="gridhead" />
                        <PagerStyle Visible="False" />
                    </asp:DataGrid>
                </td>
            </tr>
            <tr>
                <td class="globalPadding" colspan="2">
                    <div width="600px" align="right">
                        <asp:Button ID="btnSave" runat="server"  Text="ตกลง" OnClick="btnSave_Click" CssClass="btn-default btn-small"/>
                        <asp:Button ID="btnReset" runat="server"  Text="ยกเลิก" CssClass="btn-default btn-small"/>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

