﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;


public partial class adminWeb_bannerEdit : System.Web.UI.Page
{
    //=== global Var
    protected string ConId { get { return Request.QueryString["id"];  } }
    protected string CatId { get { return Request.QueryString["cat"]; }   }
  
    
    protected void Page_Load(object sender, EventArgs e) {  

        //////// Run Bind Data  
        if (!Page.IsPostBack) { 
            btnClear.Attributes.Add("Onclick" , "location='bannerList.aspx?cat="+CatId+"'; return false;" ) ; 
            BindData();
        }
    }

    

    protected void BindData() {
        string sql  = "SELECT * FROM tbBanner WHERE  BannerId= '" + ConId + "'  ";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql , "myTb");
        DataTable dt = ds.Tables[0];

        if (dt.Rows.Count > 0) {    
            int  intId =  Convert.ToInt32( dt.Rows[0][0].ToString() ) ;
            lblId.Text = String.Format("{0:00000}", intId);
             
            rblStatus.SelectedValue = dt.Rows[0]["BannerStatus"].ToString();
             
            int intSort =  Convert.ToInt32( dt.Rows[0]["BannerSort"] );  
            tbxSort.Text =  (intSort >=9999? "":intSort.ToString() ) ;

            tbxTitle.Text =   dt.Rows[0]["BannerTitle"].ToString();

            tbxPic.Text = dt.Rows[0]["BannerPic"].ToString();
            tarLink.Text = dt.Rows[0]["BannerLink"].ToString();

            ddlLang.SelectedValue = dt.Rows[0]["lang"].ToString();

        }
    }

    protected void btnSave_Click(object sender, EventArgs e) {
         
        /////// Update content
        string sql = "UPDATE  tbBanner SET "
        + "BannerTitle = @BannerTitle, "
        + "BannerStatus = @BannerStatus,  "
        + "BannerPic = @BannerPic,  "
        + "BannerSort = @BannerSort ,   "
        + "BannerLink = @BannerLink,   "  
		//===== AdminInfo Update 
		+ "lang = @lang ,  " 
		+ "input_date = @input_date ,  "
		+ "input_user = @input_user  " 
         
        + "WHERE BannerId = '" + ConId + "'   ";
 
        SqlParameterCollection objParam1 = new SqlCommand().Parameters;
        objParam1.AddWithValue("@BannerTitle", tbxTitle.Text);
        objParam1.AddWithValue("@BannerStatus", rblStatus.SelectedValue);
        objParam1.AddWithValue("@BannerPic", tbxPic.Text);
        objParam1.AddWithValue("@BannerSort", (!String.IsNullOrEmpty(tbxSort.Text)) ? Convert.ToDecimal(tbxSort.Text) : Convert.ToDecimal(9999));
        objParam1.AddWithValue("@BannerLink", tarLink.Text);
        
        
          //===== AdminInfo Update  
       objParam1.AddWithValue("@lang", ddlLang.SelectedValue);
       objParam1.AddWithValue("@input_date", DateTime.Now);
        //////Get UserName From Session Object
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        objParam1.AddWithValue("@input_user", loginData.loginName);
 
         ///////  Get Value From Html Control 
        //objParam1.AddWithValue("@NewsPicSmall", SqlDbType.NVarChar).Value = Request.Form["PicSmall"].ToString();
        //objParam1.AddWithValue("@NewsPicBig", SqlDbType.NVarChar).Value = Request.Form["PicPic"].ToString();

        int i2 = new DBClass().SqlExecute(sql, objParam1); 
        //////// check Succes  
        if (i2 == 1) {
            Response.Write("<script>alert('แก้ไขข้อมูลเรียบร้อยแล้ว')</script>");
            Response.Redirect("bannerEdit.aspx?cat="+CatId+"&id=" + ConId );  
        } else {
            Response.Write("<script>alert('ไม่สามารถแก้ไขข้อมูลได้')</script>");
            Response.Write("<script>history.go(-1)</script>");
            //Response.Redirect("");
        }

    }

}