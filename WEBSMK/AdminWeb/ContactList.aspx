﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminWeb/MasterPageAdmin1.master" AutoEventWireup="true" CodeFile="ContactList.aspx.cs" Inherits="AdminWeb_ContactList" %>
<%@ Register src="uc/ucPageNavigator.ascx" tagname="ucPageNavigator" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div>
        <asp:Label ID="lblSessShow" runat="server"> </asp:Label> 
        <h1 class="subject1">ซื้อประกันภัย</h1>   
        <table width="100%" cellpadding="0px" cellspacing="0px" border="0px">
            <tr>
                <td class="globalPadding">
                    <asp:Label ID="Label2" runat="server" Text="สถานะการตรวจสอบ"></asp:Label>
                </td>
                <td class="globalPadding">
                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="inputText">
                        <asp:ListItem Value="">-- ทั้งหมด --</asp:ListItem>
                        <asp:ListItem Value="O">รอการติดต่อกลับ</asp:ListItem>
                        <asp:ListItem Value="F">ติดต่อกลับแล้ว</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Button ID="Button1" runat="server" CssClass="btn" Text="ค้นหา" OnClick="Button1_Click" /></td>
            </tr>            
            <tr>
                <td class="globalPadding" colspan="2">
                    <uc2:ucPageNavigator ID="ucPageNavigator1" runat="server"/>
                </td>
            </tr>
            <tr>
                <td class="globalPadding" colspan="2">
                    <asp:DataGrid ID="dtgData" runat="server" AutoGenerateColumns="False" 
                            HeaderStyle-CssClass="gridhead" ItemStyle-CssClass="gridrow0" AlternatingItemStyle-CssClass="gridrow1"
                            BorderWidth="0px" AllowPaging="True" PageSize="10" width="98%">
                        <Columns>
                            <asp:BoundColumn DataField="ct_code" ReadOnly="True" Visible="False">
                                <ItemStyle CssClass="WPC_table_heading" />
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="H">
                                <ItemTemplate>
                                    <asp:RadioButton runat="server" ID="rdoHold"  GroupName="Approve" Checked=<%# showStatusHold(DataBinder.Eval(Container, "DataItem.ct_cont_stat").ToString()) %>  Enabled=<%#showEnabled(DataBinder.Eval(Container, "DataItem.ct_cont_stat").ToString()) %>/>
                                </ItemTemplate>
                                <HeaderStyle CssClass="WPC_table_heading" HorizontalAlign="Center" />
                            </asp:TemplateColumn>                            
                            <asp:TemplateColumn HeaderText="A">
                                <ItemTemplate>
                                    <asp:RadioButton runat="server" ID="rdoApprove"  GroupName="Approve" Checked=<%#showStatusApprove(DataBinder.Eval(Container, "DataItem.ct_cont_stat").ToString()) %> Enabled=<%#showEnabled(DataBinder.Eval(Container, "DataItem.ct_cont_stat").ToString()) %> />
                                </ItemTemplate>
                                <HeaderStyle CssClass="WPC_table_heading" HorizontalAlign="Center" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ชื่อ-นามสกุล">
                                <ItemTemplate>
                                    <asp:Label ID="lblFName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ct_name") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="WPC_table_heading" HorizontalAlign="Center" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="เบอร์โทรศัพท์">
                                <ItemTemplate>
                                    <asp:Label ID="lblTel" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ct_telephone") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="WPC_table_heading" HorizontalAlign="Center" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="E-mail">
                                <ItemTemplate>
                                    <asp:Label ID="lblEMail" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ct_email") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="WPC_table_heading" HorizontalAlign="Center" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="เวลาสะดวกติดต่อกลับ" ItemStyle-HorizontalAlign="center">
                                <ItemTemplate>
                                    <asp:Label ID="lblContactTime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ct_con_time") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="WPC_table_heading" HorizontalAlign="Center" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ประกันภัยรถยนต์">
                                <ItemTemplate>
                                    <asp:Label ID="lblMotor" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ct_motor") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="WPC_table_heading" HorizontalAlign="Center" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ประกันอัคคีภัย">
                                <ItemTemplate>
                                    <asp:Label ID="lblFire" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ct_fire") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="WPC_table_heading" HorizontalAlign="Center" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ประกันภัยทางทะเลและขนส่ง">
                                <ItemTemplate>
                                    <asp:Label ID="lblMarine" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ct_marine") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="WPC_table_heading" HorizontalAlign="Center" />                                
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ประกันภัยอุบัติเหตุส่วนบุคคลและเดินทาง">
                                <ItemTemplate>
                                    <asp:Label ID="lblPA" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ct_pa") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="WPC_table_heading" HorizontalAlign="Center" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ประกันภัยอื่นๆ">
                                <ItemTemplate>
                                    <asp:Label ID="lblOther" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ct_other") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="WPC_table_heading" HorizontalAlign="Center" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ประกันภัยสุขภาพ">
                                <ItemTemplate>
                                    <asp:Label ID="lblHealth" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ct_health") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="WPC_table_heading" HorizontalAlign="Center" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ประกันภัยมะเร็ง">
                                <ItemTemplate>
                                    <asp:Label ID="lblCancer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ct_cancer") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="WPC_table_heading" HorizontalAlign="Center" />
                            </asp:TemplateColumn>
                        </Columns>
                        <HeaderStyle CssClass="gridhead" />
                        <PagerStyle Visible="False" />
                    </asp:DataGrid>
                </td>
            </tr>
            <tr>
                <td class="globalPadding" colspan="2">
                    <div width="600px" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="btn" Text="ตกลง" OnClick="btnSave_Click"/>
                        <asp:Button ID="btnReset" runat="server" CssClass="btn" Text="ยกเลิก" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

