﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="main.aspx.cs" Inherits="adminWeb_main" %>
<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=IE9" />
    <meta charset="utf-8" />
    <title>Untitled Document</title>
    <link href="css/globalBackend.css" rel="stylesheet" />
    <link rel="stylesheet" href='<%=Config.BackendUrl%>css/globalBackend.css'' />

    <script src="<%=Config.BackendUrl%>scripts/jquery-1.4.1.js"></script>

    <script>
        function validateForm() {
            var isRet = true;
            var strMsg = "";
            var strFocus = "";

            if (document.getElementById("<%=txtUserName.ClientID %>").value == "") {
                isRet = false;
                strMsg += "    - ชื่อผู้ใช้งาน\n";
                if (strFocus == "")
                    strFocus = document.getElementById("<%=txtUserName.ClientID %>");
            }
            if (document.getElementById("<%=txtPassword.ClientID %>").value == "") {
                isRet = false;
                strMsg += "    - รหัสผ่าน\n";
                if (strFocus == "")
                    strFocus = document.getElementById("<%=txtPassword.ClientID %>");
            }


            if (isRet == false) {
                alert("! กรุณาระบุข้อมูล : \n" + strMsg);
                if (strFocus != "")
                    eval(strFocus.focus());
            }

            return isRet;
        }
        
            
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div id="container">
        <div id="page-login" >

            
            <div class="logoWeb">
            </div>
            <div class="section-login">

               


                <div class="rowLogin">
                    <div class="label">
                        User :</div>
                    <div>
                        <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox></div>
                </div>
                <div class="rowLogin">
                    <div class="label">
                        Password:</div>
                    <div>
                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox></div>
                </div>
                <div class="btnLogin">
                    <asp:Button ID="btnLogin" runat="server" Text="Login" OnClick="btnLogin_Click" OnClientClick="return validateForm();" CssClass="btn-default"/>
                </div>
                <div><br /></div>
            </div>
            
            <!-- end  class="section-login" --->
        </div>
        <!-- end  id="page-login" --->
        
           <asp:Label ID="lblSessShow" runat="server"> </asp:Label> 


        <div style="margin: 0px auto; text-align: center; width: 750px;">
            Copyright &#169; 2013 Syn Mun Kong Co.th.Ltd All Rights Reserved.
        </div>
    </div>
    <!-- End id="container"-->
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
    </form>
</body>
</html>
