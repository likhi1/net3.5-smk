﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminWeb/MasterPageAdmin1.master" AutoEventWireup="true" CodeFile="ntwList.aspx.cs" Inherits="adminWeb_ntwList" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- ===============  validate  -->
    <script>
        //============ On Load
        $(function () {
            //===== Set Global var
            cat = parseInt( <%=CatId%>);

            limitControler();
        });

        //============  Validate
        function CheckNull(tbx) {
            console.log(tbx.id);
            tbxSort = $("#" + tbx.id);
            if (!tbxSort.val() == "") {
                // alert("กรุณาระบุ ลำดับด้วย");  
                if (isDecimal(tbxSort.val()) == false) {
                    alert("กรุณาระบุ ให้ถูกต้อง และมีทศนิยมได้ไม่เกิน 3 ตำแหน่ง"); return false;
                }
            }
            return true;
        }
        function isDecimal(_val) {
            patt = /^[0-9]+([.,][0-9]{1,3})?$/;
            re = patt.test(_val);
            return re;
        }
        function validateNumber(event) {
            var key = window.event ? event.keyCode : event.which;
            if (event.keyCode == 8 || event.keyCode == 46
            || event.keyCode == 37 || event.keyCode == 39) {
                return true;
            }
            else if (key < 48 || key > 57) {
                return false;
            }
            else return true;
        }

        //====== Limit  Select DropDownList
        function limitControler() {
            var ddlProvince = $("#<%=ddlProvince.ClientID%>");
            var ddlAmphoe = $("#<%=ddlAmphoe.ClientID%>");
            var ddlNetworkType = $("#<%=ddlNetworkType.ClientID%>");

            //ddlNetworkType.attr("disabled", "disabled");
            ddlAmphoe.attr("disabled", "disabled");

            ///// Check for show NetworkType 
            if (ddlProvince.val() == "กรุงเทพมหานคร") {
                ddlAmphoe.removeAttr("disabled");
            }

            ///// Check for show NetworkType 
            //if (cat == 2 || cat == 4) {
            //    ddlNetworkType.removeAttr("disabled");
            //} else {
            //    ddlNetworkType.attr("disabled", "disabled");
            //}

            ddlProvince.change(function () {
                if (ddlProvince.val() == "กรุงเทพมหานคร")
                    ddlAmphoe.removeAttr("disabled");
                else
                    ddlAmphoe.attr("disabled", "disabled");
                    ddlAmphoe.val('');
            });
 
        }

         
   </script> 
    <style>
        #manageArea select { width:100px; }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div id="page-prdList">
        <div id="manageArea">
            <div  class="colLeft">
 <h1 class="subject1"> เครือข่าย <asp:Label ID="lblPageName" runat="server"></asp:Label></h1>
            <div class="navManage">
                <a class="btn-default" onclick="location ='ntwAdd.aspx?cat=<%=CatId%>' ; " style="margin-right: 10px;" />เพิ่มรายการ</a>
            </div>
</div><!-- end  class="colLeft" -->
           <div class="colRight">
        <table style="width: 100%;">
            <tr>
                <td><label>เลือกภาค :</label> <myUc:ucZone runat="server" ID="ucZone"  /> </td>
                <td> <label>เลือกจังหวัด :</label> </span><asp:DropDownList ID="ddlProvince" runat="server">
                    <asp:ListItem Value="">-- โปรดระบุ  --</asp:ListItem>
                </asp:DropDownList></td>
                <td><label>เลือกเขต :</label>
                    <asp:DropDownList ID="ddlAmphoe" runat="server">
                        <asp:ListItem Value="">-- โปรดระบุ  --</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr><td>
<label>ประเภท :</label> </span><asp:DropDownList ID="ddlNetworkType" runat="server">
                    <asp:ListItem Value="">-- โปรดระบุ  --</asp:ListItem>
                </asp:DropDownList>
                </td>
                <td colspan="2">
                    <div  > 
                    <span  style="float:left;margin-right:5px;"  > 
                    <label>เลือกคำค้น :</label>   <asp:TextBox ID="tbxKeyWord" runat="server" Width="220px"></asp:TextBox>
                    </span> 
  <span >
                    <asp:Button ID="BtnSearch" runat="server" Text="ค้นหา" class="btn-default" Style="height: 22px;" OnClick="BtnSearch_Click" />
 </span>
                    </div>   
</td>
               </div>  
            </tr>
        </table>
           
 <div class="notaion">*** การแสดงหน้าเวบ 1.ต้องมีการเปิดการใช้งาน </div>
</div><!-- end  class="colRight" -->
        </div>
        <!-- End  id="manageArea" -->
        <style>
            /*===== Control GridView1  Width (support IE9 & Over) =====*/
        [id*=GridView] th:nth-child(1) { width: 5%; }
        [id*=GridView] th:nth-child(2) { }
        [id*=GridView] th:nth-child(3) { width: 10%; }
        [id*=GridView] th:nth-child(4) { width: 7%; }
        [id*=GridView] th:nth-child(5) { width: 10%; }
        [id*=GridView] th:nth-child(6) { width: 13%; }
        [id*=GridView] th:nth-child(7) { width: 5%; }
        [id*=GridView] th:nth-child(8) { width: 7%; }
        [id*=GridView] th:nth-child(9) { width: 13%; }
        </style>
        <asp:GridView ID="networkGridView" runat="server" AutoGenerateColumns="false" Width="100%" EnableModelValidation="True"
            DataKeyNames="NtwId"
            OnRowDataBound="networkGridView_RowDataBound" 
            OnRowDeleting="networkGridView_Deleting"
            OnPageIndexChanging="networkGridView_IndexChanging"
            OnRowEditing="modEditCommand"
            OnRowUpdating="modUpdateCommand"
            OnRowCancelingEdit="modCancelCommand"    >
            <Columns>
                <asp:TemplateField HeaderText="No.">
                    <%-- แสดงเวลาปรกติ --%>
                    <ItemTemplate>
                        <div style="float: right">
                            <asp:Label ID="lblId" runat="server"></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ชื่อสาขา">
                    <ItemTemplate>
                        <asp:HyperLink ID="hplName" runat="server"> 
                        </asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                     <asp:TemplateField HeaderText="เขต/อำเภอ">
                    <ItemTemplate>
                        <div style="text-align: center">
                            <asp:Label ID="lblAmphoe" runat="server"></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="จังหวัด">
                    <ItemTemplate>
                        <div style="text-align: center">
                            <asp:Label ID="lblProvince" runat="server"></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ภาค">
                    <ItemTemplate>
                        <div style="text-align: center">
                            <asp:Label ID="lblZone" runat="server"></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="เบอร์โทร">
                    <ItemTemplate>
                        <div style="text-align: center">
                            <asp:Label ID="lblTelPhone" runat="server"></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="สถานะ">
                    <ItemTemplate>
                        <div style="text-align: center">
                            <asp:Image ID="imgStatus" runat="server" />
                        </div>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:Label ID="lblHdStatus" runat="server" Visible="false"></asp:Label>
                        <asp:DropDownList ID="ddlStatus" runat="server"></asp:DropDownList>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="แสดง<br>ลำดับ">
                    <ItemTemplate>
                        <div style="text-align: center">
                            <asp:Label ID="lblSort" runat="server"></asp:Label>
                        </div>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:Label ID="lblHdSort" runat="server" Visible="false"></asp:Label>
 <asp:TextBox ID="tbxSort" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="จัดการ" ShowHeader="False">
                    <ItemTemplate>
                        <div style="text-align: center">
                            <asp:Button ID="btnRowEdit" runat="server" Text="ตั้งค่า" class="btn-default btn-small " CommandName="Edit" />
                            <asp:Button ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Delete" Text="ลบ"
                                class="btn-default btn-small " OnClientClick="return confirm('ยืนยันทำการ ลบข้อมูล')" />
                        </div>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <div style="text-align: center">
                            <asp:Button ID="btnRowUpdate" runat="server" Text="บันทึก" class="btn-default btn-small " CommandName="Update" />
                            <asp:Button ID="btnRowCancel" runat="server" Text="ยกเลิก" class="btn-default btn-small " CommandName="Cancel" />
                        </div>
                    </EditItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerSettings Mode="NumericFirstLast" />
            <RowStyle Height="20px" />
        </asp:GridView>
        <br />
        <br />
    </div>
    <!-- end  id="page-prdList" -->
</asp:Content>