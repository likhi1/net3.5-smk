﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;
using System.Collections;


public partial class adminWeb_newsSearch : System.Web.UI.Page {

    //=======================  Global Varible 
    private string _CatId;// Declear Varible for Referance inSide  get{} set{}
    private string _KeyWord  ;// Declear Varible for Referance inSide  get{} set{}
 
    protected string CatId {
        get {
            if (Request.QueryString["cat"] != "") {
                _CatId = Request.QueryString["cat"];
            } else {
                _CatId = "";
            }
            return _CatId;
        }

    }
    protected string KeyWord {
        get {
            if (Request.QueryString["key"] != "") {
                _KeyWord = Request.QueryString["key"];
            } else {
                _KeyWord = "";
            }
            return _KeyWord;
        }

    }

    protected DataTable _dtCat;
    protected DataTable DtCat { get { return _dtCat; } }
    protected string PageName;
  
    
    protected void Page_Load(object sender, EventArgs e) {

        btnAddContent.Attributes.Add("OnClick", "location='newsAdd.aspx?cat=" + CatId + "';return false; "); 

       if (!Page.IsPostBack) {  
           BindData(); 
           ddlSearchCatBindData();
           GetNamePage();
           lblPageName.Text = PageName;

           //====== Set  Search First Time
           tbxKeySearch.Text = KeyWord;
           ddlSearchCat.SelectedValue = CatId; 
           
       } else {
           //====== Set  Search  After Click  Again
           ddlSearchCat.SelectedValue = ddlSearchCat.SelectedValue;
           tbxKeySearch.Text = tbxKeySearch.Text; 
           
       }
    }

    
   protected void BindData() {
        SelectCat(); 

        DataTable dt = SelectData().Tables[0]; 

        //======  Check Num Befor DataBind
        if (dt.Rows.Count > 0) { 
            GridView1.AllowPaging = true;
            GridView1.PageSize = 20; 
            GridView1.DataSource = dt;
            GridView1.DataBind();  
        }

    }

   protected DataSet SelectData() {

       string sql1;
       //// Condition for user want to search Keyword  From  Global
       if (CatId != "") {
           sql1 = "SELECT  a.* , b.NewsCatName   "
           + "FROM tbNewsCon a ,tbNewsCat b "
           + "WHERE "
           + "a.NewsCatId = b.NewsCatId "
           + "AND a.NewsCatId = '" + CatId + "' "
           + " AND ( NewsConTopic  LIKE  '%" + KeyWord + "%' OR  NewsConDetailShort  LIKE  '%" + KeyWord + "%' OR  NewsConDetailLong   LIKE  '%" + KeyWord + "%' )  "
           + "ORDER BY NewsConId   DESC    , NewsConStatus DESC  ";
       } else {
           sql1 = "SELECT  a.* , b.NewsCatName   "
           + "FROM tbNewsCon a ,tbNewsCat b "
           + "WHERE "
           + "a.NewsCatId = b.NewsCatId " 
           + " AND ( NewsConTopic  LIKE  '%" + KeyWord + "%' OR  NewsConDetailShort  LIKE  '%" + KeyWord + "%' OR  NewsConDetailLong   LIKE  '%" + KeyWord + "%' )  "
           + "ORDER BY NewsConId   DESC   , NewsConStatus DESC  "; 
       }
        
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql1, "tb_NewsCon");  
        return ds;  
    } 


   protected DataTable SelectCat() {
       string sql = "SELECT  NewsCatId  ,  NewsCatName  FROM tbNewsCat  ORDER BY NewsCatId ";
       DBClass obj1 = new DBClass();
       DataSet ds = obj1.SqlGet(sql, "tbNewsCaT");
       _dtCat = ds.Tables[0];
       return _dtCat;
   }

   protected void ddlSearchCatBindData() {
       ddlSearchCat.DataSource = DtCat;
       //**** Add frist Item of dropdownlist 
       ddlSearchCat.AppendDataBoundItems = true;
       ddlSearchCat.DataTextField = "NewsCatName";
       ddlSearchCat.DataValueField = "NewsCatId";
       ddlSearchCat.DataBind();
   }

   protected string GetNamePage() {
       foreach (DataRow r in DtCat.Rows) {
           if (r[0].ToString() == CatId) {
               PageName = r[1].ToString();
           }
       }
       return PageName;
   }
      

   protected ArrayList ddlConSortAddList() {
       ArrayList arl = new ArrayList();
       for (int i = 1; i <= 10; i++) {
           arl.Add(i);
       }

       return arl;
   } 

  
   //======================  Pager
    protected void GridView1_IndexChanging(object sender, GridViewPageEventArgs e) {
           GridView1.PageIndex = e.NewPageIndex;
           DataBind();
       }

  
   //======================  Command Gridview
    protected void GridView1_RowDataBound(object s, GridViewRowEventArgs e) {

        if (e.Row.RowType == DataControlRowType.DataRow) { 

            //========  (Hidde For  Check   ddlCatName  Select  ) ===================================     
            Label lblHdCatId = (Label)(e.Row.FindControl("lblHdCatId"));
            if (lblHdCatId != null) {
                lblHdCatId.Text = DataBinder.Eval(e.Row.DataItem, "NewsCatId").ToString();
            }

            Label lblHdStatus = (Label)(e.Row.FindControl("lblHdStatus"));
            if (lblHdStatus != null) {
                lblHdStatus.Text = DataBinder.Eval(e.Row.DataItem, "NewsConStatus").ToString();
            }

            Label lblHdSort = (Label)(e.Row.FindControl("lblHdSort"));
            if (lblHdSort != null) { 
                int intHdSort = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "NewsConSort"));
                if (intHdSort != 9999 && intHdSort != 0) {
                    lblHdSort.Text = intHdSort.ToString();
                } else {
                    lblHdSort.Text = "";
                } 

            }

            //======== EditItemTemplate =================================================     
            DropDownList ddlCatName = (DropDownList)(e.Row.FindControl("ddlCatName"));
            if (ddlCatName != null) {

                ddlCatName.DataSource = DtCat;
                ddlCatName.AppendDataBoundItems = true;
                ddlCatName.DataTextField = "NewsCatName";
                ddlCatName.DataValueField = "NewsCatId";
                ddlCatName.DataBind();
                ///// set select  in EditTemplate
                string x = (e.Row.FindControl("lblHdCatId") as Label).Text;
                ddlCatName.Items.FindByValue(x).Selected = true;
            }
 
            TextBox tbxSort = (TextBox)(e.Row.FindControl("tbxSort"));
            if (tbxSort != null) {
                string strValue = (e.Row.FindControl("lblHdSort") as Label).Text;
                tbxSort.Text = strValue;
                tbxSort.Style.Add("width", "40px");
            }

            DropDownList ddlStatus = (DropDownList)(e.Row.FindControl("ddlStatus"));
            if (ddlStatus != null) {
                ddlStatus.Items.Insert(0, new ListItem("ปิด", "0")); // add Empty Item 
                ddlStatus.Items.Insert(1, new ListItem("เปิด", "1")); // add Empty Item   
                ///// set select  in EditTemplate
                string x = (e.Row.FindControl("lblHdStatus") as Label).Text;
                ddlStatus.Items.FindByValue(x).Selected = true;
            }
             

            //======== ItemTemplate =================================================     
            Label lblId = (Label)(e.Row.FindControl("lblId"));
            if (lblId != null) {
                string strId = DataBinder.Eval(e.Row.DataItem, "NewsConId").ToString();
                int intId = Convert.ToInt32(strId);
                string ConId = String.Format("{0:00000}", intId);
                lblId.Text = ConId;

                lblId.Style.Add("text-align", "right");
                lblId.Style.Add("padding-right", "10px");
                ///// Add Style 
                lblId.Style.Add("width", "2%");
            }

            HyperLink hplTopic = (HyperLink)(e.Row.FindControl("hplTopic"));
            if (hplTopic != null) {
                string _id = DataBinder.Eval(e.Row.DataItem, "NewsConId").ToString();
                string _cat = DataBinder.Eval(e.Row.DataItem, "NewsCatId").ToString();

                hplTopic.Text = DataBinder.Eval(e.Row.DataItem, "NewsConTopic").ToString();

                hplTopic.NavigateUrl = "newsView.aspx?id=" + _id + "&cat=" + _cat;
                hplTopic.Style.Add("float", "left");
            }

            Label lblCatName = (Label)(e.Row.FindControl("lblCatName"));
            if (lblCatName != null) {
                lblCatName.Text = DataBinder.Eval(e.Row.DataItem, "NewsCatName").ToString();
            }


            Image imgStatus = (Image)(e.Row.FindControl("imgStatus"));
            if (imgStatus != null) {
                int strStatus = (int)DataBinder.Eval(e.Row.DataItem, "NewsConStatus");
                //lblConStatus.Text =  ( (strStatus == 1) ? '' : "ไม่แสดง"  ) ;
                string picUrl = strStatus == 1 ? "images/check-right.png" : "images/check-wrong.png";
                imgStatus.Attributes.Add("src", picUrl);
            }

            Label lblSort = (Label)(e.Row.FindControl("lblSort"));
            if (lblSort != null) {
                Decimal decSort = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "NewsConSort"));
                if (decSort > 0) {
                    lblSort.Text = decSort.ToString();
                } else {
                    lblSort.Text = "";
                }




            }

            Label lblDtCreate = (Label)(e.Row.FindControl("lblDtCreate"));
            if (lblDtCreate != null) {

                //DateTime strDtCreate = (DateTime)DataBinder.Eval(e.Row.DataItem, "NewsConDtCreate");
                //string strDtCreate2 = strDtCreate.ToString("dMMMyy", new CultureInfo("th-TH"));
                //lblDtCreate.Text = strDtCreate2;

                object obj = DataBinder.Eval(e.Row.DataItem, "NewsConDtCreate");
                if (!Convert.IsDBNull(obj)) {
                    obj = ((DateTime)obj).ToString("dMMMyy", new CultureInfo("th-TH"));
                } else {
                    obj = "";
                }
                lblDtCreate.Text = obj.ToString(); ;

                ///// Add Style  
                lblDtCreate.Style.Add("float", "right");
            }



            Label lblDtStop = (Label)(e.Row.FindControl("lblDtStop"));
            if (lblDtStop != null) {
                //DateTime strDtStop = (DateTime)DataBinder.Eval(e.Row.DataItem, "NewsConDtHide");
                //string strDtStop2 = strDtStop.ToString("dMMMyy", new CultureInfo("th-TH"));
                //lblDtStop.Text = strDtStop2;

                object obj = DataBinder.Eval(e.Row.DataItem, "NewsConDtHide");
                if (!Convert.IsDBNull(obj)) {
                    obj = ((DateTime)obj).ToString("dMMMyy", new CultureInfo("th-TH"));
                } else {
                    obj = "";
                }

                lblDtStop.Text = obj.ToString(); ;

                ///// Add Style  
                lblDtCreate.Style.Add("float", "right");
            }


        }//end if

    }

    protected void GridView1_Deleting(object s, GridViewDeleteEventArgs e) {
        int a = e.RowIndex;
        string sql2 = "DELETE  FROM  tbNewsCon WHERE NewsConId = '" + GridView1.DataKeys[e.RowIndex].Value + "'";
        int i = new DBClass().SqlExecute(sql2);
        GridView1.EditIndex = -1;
        BindData();
    }

    protected void btnSearch_Click(object sender, EventArgs e) {
        Response.Redirect("newsSearch.aspx?cat=" + ddlSearchCat.Text + "&key=" + tbxKeySearch.Text);
    }
 
   protected void modEditCommand(object sender, GridViewEditEventArgs e) {
         
       GridView1.EditIndex = e.NewEditIndex;
       BindData();
   }

   protected void modCancelCommand(object sender, GridViewCancelEditEventArgs e) {
       GridView1.EditIndex = -1;
       BindData();
   }

   protected void modUpdateCommand(object sender, GridViewUpdateEventArgs e) {
       DropDownList ddlCatName = (DropDownList)GridView1.Rows[e.RowIndex].FindControl("ddlCatName");
       DropDownList ddlStatus = (DropDownList)GridView1.Rows[e.RowIndex].FindControl("ddlStatus");
       TextBox tbxSort = (TextBox)GridView1.Rows[e.RowIndex].FindControl("tbxSort");
       Decimal decSort;

       string strSQL = "UPDATE tbNewsCon SET " +
                "NewsCatId= '" + ddlCatName.Text + "', " +
                 "NewsConStatus = '" + ddlStatus.Text + "', " +
                 "NewsConSort = '" + (decSort = (tbxSort.Text != "") ? Convert.ToDecimal(tbxSort.Text) : 0) + "' " +
                 " WHERE  NewsConId = '" + GridView1.DataKeys[e.RowIndex].Value + "'";

       DBClass obj = new DBClass();
       int i = obj.SqlExecute(strSQL);

       GridView1.EditIndex = -1;
       BindData();
   }



}