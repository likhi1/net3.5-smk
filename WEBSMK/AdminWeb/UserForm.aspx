﻿<%@ Page Language="C#" MasterPageFile="~/AdminWeb/MasterPageAdmin1.master" AutoEventWireup="true" CodeFile="UserForm.aspx.cs" Inherits="Module_Authenticate_UserForm" Title="Untitled Page" %>
<%@ Register src="uc/ucTxtDate.ascx" tagname="ucTxtDate" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script>
        function goPage(strPage)
        {
            window.location= "/<%=strAppName%>/" + strPage;
            return false;
        }
        function validateForm()
        {
            var isRet = true;
            var strMsg = "";
            if (document.getElementById("<%=txtLogin.ClientID %>").value == ""){
                isRet = false;
                strMsg += "\t-  รหัสผู้ใช้งาน\n";
            }
            if (document.getElementById("<%=txtMode.ClientID %>").value == "A" && document.getElementById("<%=txtPassword.ClientID %>").value == ""){
                isRet = false;
                strMsg += "\t-  รหัสผ่าน\n";
            }
            if (document.getElementById("<%=txtFName.ClientID %>").value == ""){
                isRet = false;
                strMsg += "\t-  ชื่อผู้ใช้งาน\n";
            }
            if (document.getElementById("<%=txtLName.ClientID %>").value == ""){
                isRet = false;
                strMsg += "\t-  นามสกุล\n";
            }
            if (document.getElementById("<%=txtStartDate.txtID %>").value == ""){
                isRet = false;
                strMsg += "\t-  วันที่เริ่มต้นใช้งาน\n";
            }
            if (document.getElementById("<%=ddlRole.ClientID %>").value == ""){
                isRet = false;
                strMsg += "\t-  กลุ่มหน้าที่การใช้งาน\n";
            }
            if (document.getElementById("<%=ddlStatus.ClientID %>").value == ""){
                isRet = false;
                strMsg += "\t-  สถานะ\n";
            }            
            if (document.getElementById("<%=txtMode.ClientID %>").value == "A" )
            {
                if (document.getElementById("<%=txtPassword.ClientID %>").value.length < 8)
                {
                    isRet = false;
                    strMsg += "! รหัสผ่านต้องมีความยาวอย่างน้อย 8 ตัวอักษร\n";
                }
                if (document.getElementById("<%=txtPassword.ClientID %>").value != document.getElementById("<%=txtRePassword.ClientID %>").value)
                {
                    isRet = false;
                    strMsg += "! รหัสผ่าน ไม่ตรงกับ ยืนยันรหัสผ่าน\n";
                }
            }

            if (isRet == false)
                alert("! กรุณาระบุข้อมูล\n" + strMsg);            
            return isRet;
        }
        function validateChangePassword()
        {
            var isRet = true;
            var strMsg = "";
            if (document.getElementById("<%=txtNewPassword.ClientID %>").value == ""){
                isRet = false;
                strMsg += "\t-  รหัสผ่านใหม่\n";
            }
            if (document.getElementById("<%=txtRenewPassword.ClientID %>").value == ""){
                isRet = false;
                strMsg += "\t-  ยืนยันรหัสผ่านใหม่\n";
            }
            if (document.getElementById("<%=txtNewPassword.ClientID %>").value.length < 8)
            {
                isRet = false;
                strMsg += "! รหัสผ่านต้องมีความยาวอย่างน้อย 8 ตัวอักษร\n";
            }            
            if (document.getElementById("<%=txtNewPassword.ClientID %>").value != document.getElementById("<%=txtRenewPassword.ClientID %>").value)
            {
                isRet = false;
                strMsg += "! รหัสผ่านใหม่ ไม่ตรงกับ ยืนยันรหัสผ่านใหม่\n";
            }

            if (isRet == false)
                alert("! กรุณาระบุข้อมูล\n" + strMsg);            
            return isRet;
        }
        function btnCloseSearchPopupClick() {
            Popup.hide('divPopup');
            return false;
        }   
        function submitWhenEnterKey(e)
        {
            if ((event.which && event.which == 13) || (event.keyCode && event.keyCode == 13))
            {
                formpostback();
            }
        }    
        function formpostback()
        {
            var theform;
            theform = document.forms[0];
            eval(<%=strPostBack %>);
        }     
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">    
    <div id="page-prdAdd">
    <h1 class="subject1">ผู้ใช้งานระบบ</h1>
    
                

                    <table width="100%" border="0">
                        <tr>                            
                            <td align="right"><div class="labelRequireField">รหัสผู้ใช้งาน :</div></td>
                            <td>
                                <asp:TextBox ID="txtLogin" runat="server" CssClass="TEXTBOX" Width="100" MaxLength="13" ></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><div id="divLablePassword" class="labelRequireField" runat="server">รหัสผ่าน :</div></td>
                            <td>
                                <div id="divtxtPassword" runat="server">
                                <asp:TextBox ID="txtPassword" runat="server" CssClass="TEXTBOX" Width="100" MaxLength="13" TextMode="Password"></asp:TextBox>
                                </div>
                            </td>
                            <td align="right"><div id="divLblRePassword" class="labelRequireField" runat="server">ยืนยันรหัสผ่าน :</div></td>
                            <td>
                                <div id="divtxtRePassword" runat="server">
                                <asp:TextBox ID="txtRePassword" runat="server" CssClass="TEXTBOX" Width="100" MaxLength="13" TextMode="Password"></asp:TextBox>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><div class="labelRequireField">ชื่อผู้ใช้งาน :</div></td>
                            <td>
                                <asp:TextBox ID="txtFName" runat="server" CssClass="TEXTBOX" Width="150" MaxLength="100"></asp:TextBox>
                            </td>
                            <td align="right"><div class="labelRequireField">นามสกุล :</div></td>
                            <td>
                                <asp:TextBox ID="txtLName" runat="server" CssClass="TEXTBOX" Width="150" MaxLength="100"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><div class="labelRequireField">วันที่เริ่มต้นใช้งาน :</div></td>
                            <td>                                
                                <uc1:ucTxtDate ID="txtStartDate" runat="server" />                                
                            </td>
                            <td align="right">วันที่สิ้นสุด : </td>
                            <td>                                
                                <uc1:ucTxtDate ID="txtEndDate" runat="server" />                                
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><div class="labelRequireField">กลุ่มหน้าที่การใช้งาน :</div></td>
                            <td>                                
                                <asp:DropDownList ID="ddlRole" runat="server">
                                </asp:DropDownList>                            
                            </td>
                            <td align="right"><div class="labelRequireField">สถานะ :</div></td>
                            <td>                                
                                <asp:DropDownList ID="ddlStatus" runat="server">
                                    <asp:ListItem Value=""> -- เลือก -- </asp:ListItem>
                                    <asp:ListItem Value="A">ใช้งาน</asp:ListItem>
                                    <asp:ListItem Value="C">ไม่ใช้งาน</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>                        
                        <tr>
                            <td align="right">วันที่เข้าใช้งานล่าสุด : </td>
                            <td>
                                <uc1:ucTxtDate ID="txtLastLogin" runat="server" Mode="view" />
                            </td>                                    
                        </tr>                        
                        <tr>
                            <td align="right" valign="top">หมายเหตุ : </td>
                            <td colspan="3">
                                <asp:TextBox ID="txtRemark" runat="server" CssClass="TEXTAREA" Width="450" MaxLength="200" TextMode="MultiLine" Rows="3"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">ผู้ที่บันทึกรายการ :</td>
                            <td>
                                <asp:TextBox ID="txtInsertLogin" runat="server" CssClass="TEXTBOXDIS" ReadOnly="true"></asp:TextBox>
                            </td>
                            <td align="right">วันที่บันทึกรายการ :</td>
                            <td>
                                <asp:TextBox ID="txtInsertDate" runat="server" CssClass="TEXTBOXDIS" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">ผู้ที่แก้ไขรายการล่าสุด :</td>
                            <td>
                                <asp:TextBox ID="txtUpdateLogin" runat="server" CssClass="TEXTBOXDIS" ReadOnly="true"></asp:TextBox>
                            </td>
                            <td align="right">วันที่แก้ไขรายการล่าสุด :</td>
                            <td>
                                <asp:TextBox ID="txtUpdateDate" runat="server" CssClass="TEXTBOXDIS" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>                        
                        <tr>
                            <td></td>
                            <td>
                                <asp:Button ID="btnBack" runat="server" Text="กลับ" CssClass="btn-default" OnClientClick="return goPage('AdminWeb/UserList.aspx?back=Y');"/>
                                <asp:Button ID="btnSave" runat="server" Text="บันทึกข้อมูล" CssClass="btn-default" 
                                    onclick="btnSave_Click" OnClientClick="return validateForm();"/>
                                <asp:Button ID="btnChangePwd" runat="server" Text="เปลี่ยนรหัสผ่าน" CssClass="btn-default" 
                                    OnClientClick="document.getElementById('divChangePassword').style.display = 'block'; return false;"/>
                                <asp:Button ID="btnClearDate" runat="server" Text="เคลียร์วันใช้งานล่าสุด" 
                                    CssClass="btn-default" onclick="btnClearDate_Click" Visible="false"/>
                                <asp:Button ID="btnClear" runat="server" Text="เคลียร์หน้าจอ" CssClass="btn-default" OnClientClick="return goPage('AdminWeb/UserForm.aspx');"/>
                                <asp:Button ID="btnAdd" runat="server" Text="เพิ่มรายการใหม่" CssClass="btn-default" OnClientClick="return goPage('AdminWeb/UserForm.aspx');"/>
                                <asp:Button ID="btnExit" runat="server" Text="ออก" CssClass="btn-default" OnClientClick="return goPage('AdminWeb/adminHome.aspx');"/>
                            </td>
                        </tr>
                    </table>                                    
            
            <div style="display:none">
                <asp:TextBox ID="txtMode" runat="server"></asp:TextBox>
                <asp:HiddenField ID="hddPassword" runat="server" />
            </div>
           
    </div>
    <div style="position:absolute; display:none; top:300px; left:400px; z-index:999;border: 2px solid;width:350px;background-color:White" id="divChangePassword">
    <table class="bgPopupLayer" width="100%">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td width="20px">&nbsp;</td>
            <td>รหัสผ่านใหม่ :</td>
            <td>
                <asp:TextBox ID="txtNewPassword" runat="server" CssClass="TEXTBOX" Width="100" MaxLength="13" TextMode="Password"></asp:TextBox>
            </td>
            <td width="20px">&nbsp;</td>
        </tr>
        <tr>
            <td width="20px">&nbsp;</td>
            <td>ยืนยันรหัสผ่านใหม่ :</td>
            <td>
                <asp:TextBox ID="txtRenewPassword" runat="server" CssClass="TEXTBOX" Width="100" MaxLength="13" TextMode="Password"></asp:TextBox>
            </td>
            <td width="20px">&nbsp;</td>
        </tr>
        <tr>
            <td></td>        
            <td colspan="2">
                <asp:Button ID="btnConfirmChangePwd" runat="server" Text="ตกลง" 
                    CssClass="BUTTON" OnClientClick="return validateChangePassword();" 
                    onclick="btnConfirmChangePwd_Click" />
                <input id="Button1" name="btnClear" type="button" value="ยกเลิก" class="BUTTON" onclick="document.getElementById('divChangePassword').style.display = 'none';" />
            </td>
            <td></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
    </div>
    <asp:Literal ID="litJavaScript" runat="server"></asp:Literal>        
</asp:Content>

