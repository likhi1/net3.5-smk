﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminWeb/MasterPageAdmin1.master" AutoEventWireup="true" CodeFile="faqView.aspx.cs" Inherits="adminWeb_faqView"   ValidateRequest="false"   %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server"> 
        
    
    <!-- ===============  CKEditor Config &  CKEditor With CKFinder Config  -->
    <script>
        $(function () {
            
            /// CKFinder & CKEditer
            if (typeof CKEDITOR != 'undefined') {
                var editor = CKEDITOR.replace('<%=tarDetailLong.ClientID%>', { customConfig: '../ckeditor_config/fullMenu.js' } );
                ////////  Fix This When change location of this file  (This path for refer to file in folder  ckfinder )
                CKFinder.setupCKEditor(editor, '../ckfinder/');
            }

        });// end onload
    </script>

        <!-- ===============  Validate  -->
    <script>
        
        $(function () {
            //// Set Varbles
            
            tbxTopic = $("#<%=tbxTopic.ClientID%>").val();
            
            tarDetailLong = $("#<%=tarDetailLong.ClientID%>") ;
            btnSave = $("#<%=btnSave.ClientID%>");
            tbxSort = $("#<%=tbxSort.ClientID%>");

            //// Check Number
            tbxSort.keypress(validateNumber);
         

            btnSave.click(function ( ) { 
                return ChkNull();   
            });
  
        });

        
        function isDecimal(_val){
            patt = /^[0-9]+([.,][0-9]{1,3})?$/ ;
            re =  patt.test(_val);
            return re;
        }

        function validateNumber(event) {
            var key = window.event ? event.keyCode : event.which;
            if (event.keyCode == 8 || event.keyCode == 46
             || event.keyCode == 37 || event.keyCode == 39) {
                return true;
            }
            else if (key < 48 || key > 57) {
                return false;
            }
            else return true;
        }

        function ChkNull( ) {  
                if (ddlPrdCat == "" || ddlPrdCat == null) {
                    alert("กรุณากรอกกลุ่มสินค้า");  
                    return false;   
                } else if (tbxDateShow == "" || tbxDateShow == null) {
                    alert("กรุณากรอกวันที่แสดง"); 
                    return false;  
                } else if (tbxDateHide == "" || tbxDateHide == null) {
                    alert("กรุณากรอกวันที่หยุดแสดง"); 
                    return false;   
                } else if (tbxTopic == "" || tbxTopic == null) {
                    alert("กรุณากรอกหัวข้อด้วย"); 
                    return false;  
                } else if(!tbxSort.val() == "" ){
                    // alert("กรุณาระบุ ลำดับด้วย");  
                    if (isDecimal(tbxSort.val() ) == false ){ 
                        alert("กรุณาระบุ ให้ถูกต้อง และมีทศนิยมได้ไม่เกิน 3 ตำแหน่ง");   return false ;
                    }
                } 
      
                return true;  
        }


    </script>

        <style type="text/css">
            .auto-style1 { text-align: right; height: 19px; }
            </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

       <div id="page-prdAdd">

        <h1 class="subject1">เพิ่มรายการ ถาม-ตอบ</h1>

        <table style="width: 100%;">
            <tr>

                <td class="label" width="100px">ID :</td>
                <td >
               <asp:Label ID="lblId" runat="server" ></asp:Label>
                    </td>
              

                <td class="auto-style1"></td>
                <td >
                  
                </td> 
            </tr>
            <tr>

                <td class="label" width="100px">สถานะการแสดง :</td>
                <td>
             
                    <asp:RadioButtonList ID="rblStatus" runat="server"
                        RepeatDirection="Horizontal" TextAlign="Left">
                        <asp:ListItem Value="0">ปิด</asp:ListItem>
                        <asp:ListItem Value="1" Selected="True">เปิด</asp:ListItem>  
                    </asp:RadioButtonList></td>

                <td class="label">&nbsp;</td>
                <td>
                  
                </td> 
            </tr>
            <tr> 
                
                   <td class="label">ลำดับการแสดง :</td>
                <td>
                    <asp:TextBox ID="tbxSort" runat="server" MaxLength="8" ></asp:TextBox>  <span class="star1">สามารถกำหนดเป็นทศนิยมได้</span>

                    
                    </td>  

                <td class="label">&nbsp;</td>
                <td>
                    &nbsp;</td>  
            </tr> 
            </table>

        <table style="width: 100%;">

        
            <tr>
                <td class="label">หัวข้อ :</td>
                <td>
                    <asp:TextBox ID="tbxTopic" runat="server" Width="730px" class="ckeditor"></asp:TextBox>
                </td>
            </tr>

    
           
            <tr>
                <td class="label" valign="top">รายละเอียดเต็ม :</td>
                <td>
 
                    <asp:TextBox ID="tarDetailLong" TextMode="MultiLine" runat="server"></asp:TextBox>


                </td>
            </tr>

            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="label">&nbsp;</td>
                <td> 

                    <asp:Button ID="btnSave" runat="server" Text="บันทึก" OnClick="btnSave_Click"  CssClass="btn-default"
                        Style="margin-right: 10px;" />
                    <asp:Button ID="btnClear" runat="server" Text="ยกเลิก" CssClass="btn-default"   OnClientClick="history.back();return false;"  UseSubmitBehavior="false"  />
                </td>
            </tr>
        </table>
    </div>
    <!-- end id="page-prdAdd" -->


</asp:Content>

