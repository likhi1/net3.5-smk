﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization; 


public partial class adminWeb_prdAdd : System.Web.UI.Page
{
    protected string  PrdId { get {return Request.QueryString["id"]; }  }
    protected string  CatId { get { return Request.QueryString["cat"]; } }
     

    protected void Page_Load(object sender, EventArgs e) { 
        //***** Check  Session *****
        // new CheckSess().CurrentSession();  

        if (!Page.IsPostBack) {
            ddlCatBindData();
        }
    }

    protected void ddlCatBindData() {  
        string sql = "SELECT * FROM tbPrdCat  ORDER BY PrdCatId ";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "tbPrdCaT");

        ddlCat.DataSource = ds;
        //**** Add frist Item of dropdownlist
        ddlCat.AppendDataBoundItems = true;
        ddlCat.DataTextField = "PrdCatName";
        ddlCat.DataValueField = "PrdCatId";
        ddlCat.DataBind();
    }

     /*
    protected string SetYearUs(string _dateTime) {
        string year;
        string dateFormat;
        // if(year == "Thai"){
        //// change year culture
        string[] arrDate = _dateTime.Split('/');
        string strYear = (int.Parse(arrDate[2]) - 543).ToString();
        dateFormat = arrDate[0] + "/" + arrDate[1] + "/" + strYear;
        // }// end if
        return dateFormat;
    }
    */

    protected void btnSave_Click(object sender, EventArgs e) {

        //string ip = System.Web.HttpContext.Current.Request.UserHostAddress;  

        ////// Use If  datePicker send format = 25/06/2013
        DateTime dtDateShow = Convert.ToDateTime(tbxDateShow.Text, new CultureInfo("th-TH")); 
        DateTime dtDateHide = Convert.ToDateTime(tbxDateHide.Text, new CultureInfo("th-TH"));
 
        string sql1 = "INSERT INTO tbPrdCon "
                    + "VALUES (@PrdCatId,@PrdConTopic,@PrdConDetailShort,@PrdConDetailLong,"
                    + "@PrdConDtCreate,@PrdConDtShow,@PrdConDtHide,"
                    + "@PrdConStatus,@PrdConSort,@PrdConIp,@PrdPicSmall,@PrdPicBig  ,@PrdConType, "
					//===== AdminInfo Add
					+ " @lang , "
					+ " @input_date ,   "
					+ " @input_user  "
					+ " ) ";
                        
       
        Decimal decSort;
         
        SqlParameterCollection objParam1 = new SqlCommand().Parameters;
        objParam1.AddWithValue("@PrdCatId",  ddlCat.SelectedValue ); 
        objParam1.AddWithValue("@PrdConSort",  ( decSort = (tbxSort.Text != "")?Convert.ToDecimal(tbxSort.Text):9999 ) );
        objParam1.AddWithValue("@PrdConStatus",  rblStatus.SelectedValue );
        objParam1.AddWithValue("@PrdConDtCreate", DateTime.Today ); 
        //objParam1.AddWithValue("@PrdConDtCreate",  DateTime.Today.ToString("MM/dd/yy") ); // Change Date Time Format
        objParam1.AddWithValue("@PrdConDtShow",  dtDateShow );
        objParam1.AddWithValue("@PrdConDtHide", dtDateHide );
        objParam1.AddWithValue("@PrdConTopic",  tbxTopic.Text );
        objParam1.AddWithValue("@PrdConDetailShort", tarDetailShort.Text );
        objParam1.AddWithValue("@PrdConDetailLong",   tarDetailLong.Text );
        objParam1.AddWithValue("@PrdConIp",  "" );
        objParam1.AddWithValue("@PrdConType", ddlType.SelectedValue );
        
		//===== AdminInfo Add 
		objParam1.AddWithValue("@lang", '0' ); // TH = 0 ,  En =  1
		objParam1.AddWithValue("@input_date", DateTime.Now);
		//////Get UserName From Session Object
		ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
		objParam1.AddWithValue("@input_user", loginData.loginName);
          

        /////// Get Value From Web Control
        objParam1.AddWithValue("@PrdPicSmall",  tbxPicSmall.Text );
        objParam1.AddWithValue("@PrdPicBig",  tbxPicBig.Text ) ;

        /////// Get Value From Html Control 
        //objParam1.AddWithValue("@PrdPicSmall", SqlDbType.NVarChar).Value = Request.Form["PicSmall"].ToString();
        //objParam1.AddWithValue("@PrdPicBig", SqlDbType.NVarChar).Value = Request.Form["PicPic"].ToString();
         
        int i1 = new DBClass().SqlExecute(sql1, objParam1);

        if (i1 == 1) {
            Response.Write("<script>alert('เพิ่มข้อมูลเรียบร้อยแล้ว')</script>");
            Response.Redirect("prdList.aspx");
        } else {
            Response.Write("<script>alert('ไม่สามารถเพิ่มข้อมูลได้')</script>");
            Response.Redirect("prdAdd.aspx");
        }
    }

}