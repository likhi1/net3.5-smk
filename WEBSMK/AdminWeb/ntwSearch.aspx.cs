﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;
using System.Collections;


public partial class adminWeb_ntwSearch : System.Web.UI.Page
{
    protected int CatId {
        get {
            int cat;
            // =  int.Parse(Request.QueryString["cat"]) ; 
            if (string.IsNullOrEmpty(Request.QueryString["cat"])) {
                cat = 1;
            } else {
                cat = int.Parse(Request.QueryString["cat"]);
            }
            return cat;

        }
    }
    protected string ZoneId;
     
    protected string PageName;
    protected DataTable DtCat;

    //==============  Set Create varible ;
    protected string Type;
    protected string Pro;
    protected string Amp;
    protected string Key;

    protected void Page_Load(object sender, EventArgs e) {
        ///// BindDropDown 
        SelectCat();
        ddlAmphoeBindData();
        ddlProvinceBindData();
        ddlBranchTypeBindData(CatId);
        /////// Set Pagename
        GetNamePage();
        lblPageName.Text = PageName;

        if (!Page.IsPostBack) {
            ///// Set Varible from QueryString  => from Page ntwList.aspx
            ZoneId = Request.QueryString["zone"];
            Type = Request.QueryString["type"];
            Pro = Server.HtmlDecode(Request.QueryString["pro"]);
            Amp = Server.HtmlDecode(Request.QueryString["amp"]);
            Key = Server.HtmlDecode(Request.QueryString["key"]);

            BindData();
        } else {
            ///// Set Varible from => Page ntwSearch.aspx
            ZoneId = ucZone.StrZoneName; 
            Type = ddlNetworkType.SelectedValue;
            Pro = ddlProvince.SelectedValue;
            Amp = ddlAmphoe.SelectedValue;
            Key = tbxKeyWord.Text;
        }

        ///// Set select & Show Data
                   //////////// // ddlTip.Items.FindByValue(tocs).Selected = true;
        DropDownList ddlZone = (DropDownList)ucZone.FindControl("ddlZone");
        ddlZone.SelectedValue = ZoneId; 
 
        ddlNetworkType.SelectedValue = Type;
        ddlProvince.SelectedValue = Pro;
        ddlAmphoe.SelectedValue = Amp;
        tbxKeyWord.Text = Key;

    }

    protected void BindData() {
        DataSet ds = SelectData();
        DataTable dt1 = ds.Tables[0];



        //=== Set Pager From code Behind
        networkGridView.AllowPaging = true;
        networkGridView.PageSize = 20;
        networkGridView.PagerStyle.CssClass = "cssPager";

        //===== Bind  Data
        networkGridView.DataSource = dt1;
        networkGridView.DataBind();

    }

    protected DataSet SelectData( ) {
        string sql1 = "SELECT tbNtw.* ,  tbNtwZone.NtwZoneName "
       + "FROM  tbNtw "
       + "LEFT JOIN tbNtwCat ON tbNtw.NtwCatId  = tbNtwCat.NtwCatId "
       + "LEFT JOIN tbNtwZone ON tbNtw.NtwZoneId  = tbNtwZone.NtwZoneId  "
       + "WHERE "
      // + "tbNtw.NtwStatus ='1'   "
       + "  tbNtw.NtwCatId  ='"+CatId+"'  ";
        if (ZoneId != "") {
          sql1 += "AND  tbNtw.NtwZoneId  ='" + ZoneId + "'  " ;
        }

        if (Type != "") {
            sql1 += "AND   tbNtw.NtwTypeId = '" + Type + "'  ";
        }


        if (Pro != "") {
            sql1 += "AND   tbNtw.NtwProvince  = '" + Pro + "'  ";
        }

        if (Amp != "") {
            sql1 +=   "AND   tbNtw.NtwAmphoe  = '" + Amp + "'  ";
        }

        if (Key != "") {
            sql1 += "AND (NtwName   LIKE  '%" + Key + "%'  OR   "
                    + "NtwProvince   LIKE  '%" + Key + "%'  OR   "
                    + "NtwAmphoe    LIKE  '%" + Key + "%'  OR   "
                    + "NtwAddress   LIKE  '%" + Key + "%'  OR   "
                    + "NtwService   LIKE  '%" + Key + "%' )   "; 
        }
         

        sql1 += " ORDER BY  tbNtw.NtwSort ASC  ,  tbNtw.NtwId   DESC    ";


        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql1, "tbNtwData");

        return ds;


    }

    //======================  Set DropDown 
    protected DataTable SelectCat() {
        string sql = "SELECT  NtwCatId  ,  NtwCatName  FROM tbNtwCat  ORDER BY NtwCatId ";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "_tb");
        DtCat = ds.Tables[0];
        return DtCat;
    }

    protected void ddlProvinceBindData() {
        string sql = "SELECT ds_major_cd , RTRIM(ds_desc_t) AS ds_desc_t  FROM setcode  WHERE (ds_major_cd = 'CH') ";

        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "tb");

        ddlProvince.DataSource = ds;
        //**** Add frist Item of dropdownlist
        ddlProvince.AppendDataBoundItems = true;
        ddlProvince.DataTextField = "ds_desc_t";
        //ddlProvince.DataValueField = "ds_desc_t";
        ddlProvince.DataBind();

    }

    protected void ddlAmphoeBindData() {
        string sql = "SELECT ds_major_cd , ds_desc , RTRIM(ds_desc_t) AS ds_desc_t  FROM setcode  WHERE (ds_major_cd = 'AM') AND (ds_desc = '0') ";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "tb");

        ddlAmphoe.DataSource = ds;
        //**** Add frist Item of dropdownlist
        ddlAmphoe.AppendDataBoundItems = true;
        ddlAmphoe.DataTextField = "ds_desc_t";
        // ddlAmphoe.DataValueField = "ds_desc_t";
        ddlAmphoe.DataBind();

    }

    protected void ddlBranchTypeBindData(int CatId) {
        string sql = "SELECT * FROM tbNtwType WHERE NtwTypeCat='" + CatId + "'  ORDER BY NtwTypeId";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "_tb");

        ddlNetworkType.DataSource = ds;
        //**** Add frist Item of dropdownlist
        ddlNetworkType.AppendDataBoundItems = true;
        ddlNetworkType.DataTextField = "NtwTypeNameTh";
        ddlNetworkType.DataValueField = "NtwTypeId";
        ddlNetworkType.DataBind();
    }


    //======================  ????
    protected DataSet SelectZone() {
        string sql = "SELECT * From tbNtwZone";
        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql, "_tbNtwZone");
        return ds;
    }

    protected void ddlSearchCatAddList() {
        //DataSet ds = SelectZone();
        //ddlSearchCat.DataSource = ds;
        //ddlSearchCat.AppendDataBoundItems = true;
        //ddlSearchCat.DataTextField = "NtwZoneName";
        //ddlSearchCat.DataValueField = "NtwZoneId";
        //ddlSearchCat.DataBind();
    }

    protected ArrayList ddlConSortAddList() {
        ArrayList arl = new ArrayList();
        for (int i = 1; i <= 10; i++) {
            arl.Add(i);
        }

        return arl;
    }

    //=======================  Set PageName
    protected string GetNamePage() {
        foreach (DataRow r in DtCat.Rows) {
            if ((int)r[0] == CatId) {
                PageName = r[1].ToString();
            }
        }
        return PageName;
    }


    //======================== Set  Event 
    protected void networkGridView_RowDataBound(object sender, GridViewRowEventArgs e) {

        if (e.Row.RowType == DataControlRowType.DataRow) {



            //+++++++++++++++++ TemplateField +++++++++++++++++++++++++++++++++++++++++++++// 
            //======= Control Hidden ( For  Check   ddlCatName  Select  ) =======//
            Label lblHdStatus = (Label)(e.Row.FindControl("lblHdStatus"));
            if (lblHdStatus != null) {
                lblHdStatus.Text = DataBinder.Eval(e.Row.DataItem, "NtwStatus").ToString();
            }

            Label lblHdSort = (Label)(e.Row.FindControl("lblHdSort"));
            if (lblHdSort != null) {
                lblHdSort.Text = DataBinder.Eval(e.Row.DataItem, "NtwSort").ToString();
            }


            //======= Control Edit =======//
            TextBox tbxSort = (TextBox)(e.Row.FindControl("tbxSort"));
            if (tbxSort != null) {
                string strValue = (e.Row.FindControl("lblHdSort") as Label).Text;
                tbxSort.Text = strValue;
                tbxSort.Style.Add("width", "40px");
            }
            DropDownList ddlStatus = (DropDownList)(e.Row.FindControl("ddlStatus"));
            if (ddlStatus != null) {
                ddlStatus.Items.Insert(0, new ListItem("ปิด", "0")); // add Empty Item 
                ddlStatus.Items.Insert(1, new ListItem("เปิด", "1")); // add Empty Item   
                ///// set select  in EditTemplate
                string x = (e.Row.FindControl("lblHdStatus") as Label).Text;
                ddlStatus.Items.FindByValue(x).Selected = true;
            }

            Button btnRowUpdate = (Button)(e.Row.FindControl("btnRowUpdate"));
            if (btnRowUpdate != null) {
                btnRowUpdate.Attributes.Add("onclick", "return CheckNull(" + tbxSort.ClientID + ")");
            }


            //=======  Control Show ========//  
            //Label lblId = (Label)(e.Row.FindControl("lblId")); 
            //if (lblId != null) {

            //    //int intId = networkGridView.Rows.Count ; // count each row
            //    int intId = RowCount - 1; 
            //    string strId = String.Format("{0:0000}", intId);
            //    lblId.Text = strId;

            //} 

            //////  Show ID From DB /////// 
            Label lblId = (Label)(e.Row.FindControl("lblId"));
            if (lblId != null) {
                string strConId = DataBinder.Eval(e.Row.DataItem, "NtwId").ToString();
                int intId = Convert.ToInt32(strConId);
                string strId = String.Format("{0:0000}", intId);
                lblId.Text = strId;

                //lblId.Style.Add("text-align", "right");
                //lblId.Style.Add("padding-right", "10px"); 
                //lblId.Style.Add("width", "2%");
            }

            HyperLink hplName = (HyperLink)(e.Row.FindControl("hplName"));
            if (hplName != null) {
                string _id = DataBinder.Eval(e.Row.DataItem, "NtwId").ToString();
                string _cat = DataBinder.Eval(e.Row.DataItem, "NtwCatId").ToString();
                hplName.Text = DataBinder.Eval(e.Row.DataItem, "NtwName").ToString();

                hplName.NavigateUrl = "ntwView.aspx?id=" + _id + "&cat=" + _cat;
                hplName.Style.Add("float", "left");
            }

            Label lblProvince = (Label)(e.Row.FindControl("lblProvince"));
            if (lblProvince != null) {
                lblProvince.Text = DataBinder.Eval(e.Row.DataItem, "NtwProvince").ToString();

            }
            Label lblZone = (Label)(e.Row.FindControl("lblZone"));
            if (lblZone != null) {
                lblZone.Text = DataBinder.Eval(e.Row.DataItem, "NtwZoneName").ToString();
            }
            Label lblAmphoe = (Label)(e.Row.FindControl("lblAmphoe"));
            if (lblAmphoe != null) {
                lblAmphoe.Text = DataBinder.Eval(e.Row.DataItem, "NtwAmphoe").ToString();
            }

            Label lblTelPhone = (Label)(e.Row.FindControl("lblTelPhone"));
            if (lblTelPhone != null) {
                lblTelPhone.Text = DataBinder.Eval(e.Row.DataItem, "NtwTelPhone").ToString();
            }

            Image imgStatus = (Image)(e.Row.FindControl("imgStatus"));
            if (imgStatus != null) {
                int intStatus = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "NtwStatus"));
                //lblConStatus.Text =  ( (strStatus == 1) ? '' : "ไม่แสดง"  ) ;
                string picUrl = intStatus == 1 ? "images/check-right.png" : "images/check-wrong.png";
                imgStatus.Attributes.Add("src", picUrl);
            }

            Label lblSort = (Label)(e.Row.FindControl("lblSort"));
            if (lblSort != null) {
                Decimal decSort = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "NtwSort"));

                if (decSort > 0) {
                    lblSort.Text = decSort.ToString();
                }
                else {
                    lblSort.Text = "";
                }
            }




        }//end if

    } // end method 

    protected void networkGridView_Deleting(object s, GridViewDeleteEventArgs e) {
        int a = e.RowIndex;
        string sql2 = "DELETE  FROM  tbNtw  WHERE NtwId = '" + networkGridView.DataKeys[e.RowIndex].Value + "'";
        int i = new DBClass().SqlExecute(sql2);
        networkGridView.EditIndex = -1;
        BindData();
    }

    protected void BtnSearch_Click(object sender, EventArgs e) {

        //=========  Set  Renew  Seession 
        //Session["zone"] = ucZone.StrZoneName;
        //Session["pro"] = ddlProvince.SelectedValue;
        //Session["amp"] = ddlAmphoe.SelectedValue;
        //Session["key"] = tbxKeyWord.Text;
        //Session["type"] = ddlNetworkType.SelectedValue;
         

        //======= Serach By QueryString  
 
        ZoneId = ucZone.StrZoneName;
        Type = ddlNetworkType.SelectedValue;
        Pro = Server.HtmlEncode(ddlProvince.SelectedValue);
        Amp = Server.HtmlEncode(ddlAmphoe.SelectedValue);
        Key = Server.HtmlEncode(tbxKeyWord.Text);

        Response.Redirect("ntwSearch.aspx?cat=" + CatId + "&zone=" + ZoneId + "&type=" + Type + "&pro=" + Pro + "&amp=" + Amp + "&key=" + Key);


    }

    //====================== Set Pager
    protected void networkGridView_IndexChanging(object sender, GridViewPageEventArgs e) {
        networkGridView.PageIndex = e.NewPageIndex;
        BindData();
    }

    //======================  Set  Gridview
    protected void modEditCommand(object sender, GridViewEditEventArgs e) {
        networkGridView.EditIndex = e.NewEditIndex;
        BindData();
    }

    protected void modCancelCommand(object sender, GridViewCancelEditEventArgs e) {
        networkGridView.EditIndex = -1;
        BindData();
    }

    protected void modUpdateCommand(object sender, GridViewUpdateEventArgs e) {


        DropDownList ddlStatus = (DropDownList)networkGridView.Rows[e.RowIndex].FindControl("ddlStatus");
        TextBox tbxSort = (TextBox)networkGridView.Rows[e.RowIndex].FindControl("tbxSort");
        Decimal decSort;

        string strSQL = "UPDATE tbNtw SET " +

             "NtwStatus = '" + ddlStatus.Text + "', " +
             "NtwSort = '" + (decSort = (tbxSort.Text != "") ? Convert.ToDecimal(tbxSort.Text) : 0) + "' " +
             " WHERE  NtwId = '" + networkGridView.DataKeys[e.RowIndex].Value + "'";

        DBClass obj = new DBClass();
        int i = obj.SqlExecute(strSQL);

        networkGridView.EditIndex = -1;
        BindData();
    }


}