﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;
using System.Collections;


public partial class adminWeb_branchView : System.Web.UI.Page 
{
    public int CatId {
        get {
            int catId;
            if (string.IsNullOrEmpty(Request.QueryString["cat"])) {
                catId = 1;
            } else {
                catId = Convert.ToInt32(Request.QueryString["cat"]);
            }
            return catId;
        }
    }
    protected string NtwId { get { return Request.QueryString["id"]; }   }

    protected string PageName;
    protected DataTable DtCat;

    protected void Page_Load(object sender, EventArgs e) {
        if (!Page.IsPostBack) {
            SelectCat();
            ddlCatBindData();
            ddlProvinceBindData();
            ddlAmphoeBindData(); 
            ddlBranchTypeBindData(CatId);
            /////// Set Pagename
            GetNamePage();
            lblPageName.Text = PageName;

            BindData();  
        }
    }

    protected void BindData() {
        string sql1 = @"SELECT a.*, b.NtwCatName   , c.NtwZoneName , d.NtwTypeNameTh 
                        FROM tbNtw  a
                        left outer join tbNtwCat b on a.NtwCatId  = b.NtwCatId
                        left outer join tbNtwZone c on a.NtwZoneId  = c.NtwZoneId 
                        left outer join tbNtwType d  on a.NtwTypeId  = d.NtwTypeId 
                        WHERE  a.NtwId= '" + NtwId + "'  AND b.NtwCatId  ='" + CatId + "' ";

        //string sql1 = "SELECT a.*, b.NtwCatName   , c.NtwZoneName , d.NtwTypeNameTh  " //, e.ds_desc_t
        //             + "FROM tbNtw  a, tbNtwCat b , tbNtwZone c " // , setcode e 
        //             + " left outer join tbNtwType d  on a.NtwTypeId  = d.NtwTypeId "
        //             + "WHERE "
        //             + "a.NtwCatId  = b.NtwCatId AND "
        //             + "a.NtwZoneId  = c.NtwZoneId AND "
        //             //+ " AND "
        //             + "a.NtwId= '" + NtwId + "'  AND b.NtwCatId  ='" + CatId + "' ";

        DBClass obj1 = new DBClass();
        DataSet ds1 = obj1.SqlGet(sql1, "tbSelect");
        DataTable dt1 = ds1.Tables[0];

        if (dt1.Rows.Count > 0) {
            // foreach( string  x in dt1){ }  // *** see all value in  DataTable ***

            int intConId = Convert.ToInt32(dt1.Rows[0]["NtwId"].ToString());
            lblId.Text = String.Format("{0:00000}", intConId);
            ddlCat.SelectedValue = dt1.Rows[0]["NtwCatId"].ToString();
            ddlCat.Attributes.Add("disabled", "true");

            //==== Selected DropDownList in User Control   =>  by FindControl( Id )   in User Control ===//
            DropDownList Zone = (DropDownList)ucZone.FindControl("ddlZone");
            Zone.SelectedValue = dt1.Rows[0]["NtwZoneId"].ToString();
            //DropDownList Province = (DropDownList)ucProvince.FindControl("ddlProvince");  
            //Province.SelectedValue = dt1.Rows[0]["NtwProvince"].ToString();

            ddlProvince.SelectedValue = dt1.Rows[0]["NtwProvince"].ToString();
            ddlAmphoe.SelectedValue = dt1.Rows[0]["NtwAmphoe"].ToString();

            tbxEmail.Text = dt1.Rows[0]["NtwEmail"].ToString();
            rblStatus.SelectedValue = dt1.Rows[0]["NtwStatus"].ToString();

            string strSort = dt1.Rows[0]["NtwSort"].ToString();
            tbxSort.Text = Convert.ToDecimal(strSort) != 9999 ? strSort : "";

            tbxTelPhone.Text = dt1.Rows[0]["NtwTelPhone"].ToString();
            tbxTelMobile.Text = dt1.Rows[0]["NtwTelMobile"].ToString();
            tbxName.Text = dt1.Rows[0]["NtwName"].ToString();
            tbxAddress.Text = dt1.Rows[0]["NtwAddress"].ToString();
            tbxService.Text = dt1.Rows[0]["NtwService"].ToString();
            tbxNotation.Text = dt1.Rows[0]["NtwNotation"].ToString();
            tbxMap.Text = dt1.Rows[0]["NtwMap"].ToString();
            tbxPic1.Text = dt1.Rows[0]["NtwPic1"].ToString();
            tbxPic2.Text = dt1.Rows[0]["NtwPic2"].ToString();
            tbxNumCode.Text = dt1.Rows[0]["NtwNumCode"].ToString();
            ddlNetworkType.SelectedValue = dt1.Rows[0]["NtwTypeId"].ToString();
            tbxMapLink.Text = dt1.Rows[0]["NtwMapLink"].ToString();

            //============ Edit 
            tbxNtwFax.Text = dt1.Rows[0]["NtwFax"].ToString();
            tbxWebsite.Text = dt1.Rows[0]["NtwWebsite"].ToString();



        }
    }

    protected void ddlCatBindData() {
        string sql = "SELECT * FROM tbNtwCat  ORDER BY NtwCatId ";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "_tbNtwCaT");

        ddlCat.DataSource = ds;
        //**** Add frist Item of dropdownlist
        ddlCat.AppendDataBoundItems = true;
        ddlCat.DataTextField = "NtwCatName";
        ddlCat.DataValueField = "NtwCatId";
        ddlCat.DataBind();

      //  ddlCat.SelectedValue = CatId;
      
        
    }

    //======================  Set DropDown  
    protected DataTable SelectCat() {
        string sql = "SELECT  NtwCatId  ,  NtwCatName  FROM tbNtwCat  ORDER BY NtwCatId ";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "_tb");
        DtCat = ds.Tables[0];
        return DtCat;
    }

    protected void ddlProvinceBindData() {
        string sql = "SELECT ds_major_cd , RTRIM(ds_desc_t) AS ds_desc_t  FROM setcode  WHERE (ds_major_cd = 'CH')  Order by ds_desc_t ";
       
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "tb");

        ddlProvince.DataSource = ds;
        //**** Add frist Item of dropdownlist
        ddlProvince.AppendDataBoundItems = true;
        ddlProvince.DataTextField = "ds_desc_t";
        //ddlProvince.DataValueField = "ds_desc_t";
        ddlProvince.DataBind();

    }

    protected void ddlAmphoeBindData() {
        string sql = "SELECT ds_major_cd , ds_desc , RTRIM(ds_desc_t) AS ds_desc_t  FROM setcode  WHERE (ds_major_cd = 'AM') AND (ds_desc = '0') ";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "tb");

        ddlAmphoe.DataSource = ds;
        //**** Add frist Item of dropdownlist
        ddlAmphoe.AppendDataBoundItems = true;
        ddlAmphoe.DataTextField = "ds_desc_t";
        // ddlAmphoe.DataValueField = "ds_desc_t";
        ddlAmphoe.DataBind();

    }

    protected void ddlBranchTypeBindData(int CatId) {
        string sql = "SELECT * FROM tbNtwType WHERE NtwTypeCat='"+CatId+"'  ORDER BY NtwTypeId";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "_tb");

        ddlNetworkType.DataSource = ds;
        //**** Add frist Item of dropdownlist
        ddlNetworkType.AppendDataBoundItems = true;
        ddlNetworkType.DataTextField = "NtwTypeNameTh";
        ddlNetworkType.DataValueField = "NtwTypeId";
        ddlNetworkType.DataBind();
    }

    //=======================  Set PageName
    protected string GetNamePage() {
        foreach (DataRow r in DtCat.Rows) {
            if ((int)r[0] == CatId) {
                PageName = r[1].ToString();
            }
        }
        return PageName;
    }

    //======================== Set  Event 
    protected void btnSave_Click(object sender, EventArgs e) {

        /////// Update content
        string sql1 = "UPDATE  tbNtw SET "
        + "NtwName = @NtwName, " 
        + "NtwAddress = @NtwAddress, "
         + "NtwService = @NtwService, " 
        + "NtwTelPhone = @NtwTelPhone, "
        + "NtwTelMobile = @NtwTelMobile, " 
        + "NtwEmail = @NtwEmail, "
        + "NtwNotation = @NtwNotation, "
        + "NtwMap = @NtwMap, "
        + "NtwPic1 = @NtwPic1,"
        + "NtwPic2 = @NtwPic2,"
        + "NtwStatus = @NtwStatus, "
        + "NtwSort = @NtwSort, "
        + "NtwZoneId = @NtwZoneId, "
        + "NtwProvince = @NtwProvince,  "
        + "NtwAmphoe = @NtwAmphoe,  "
        + "NtwNumCode = @NtwNumCode,  "
        + "NtwTypeId = @NtwTypeId,  "
        + "NtwFax = @NtwFax,  "
        + "NtwWebsite = @NtwWebsite,  "
        + "NtwMapLink = @NtwMapLink  " 
  
        + "WHERE NtwId ='" + NtwId + "' AND NtwCatId ='" + CatId + "'  ";

        Decimal decSort;
       
        
        SqlParameterCollection objParam1 = new SqlCommand().Parameters; 
        objParam1.AddWithValue("@NtwName", tbxName.Text);
        objParam1.AddWithValue("@NtwAddress", tbxAddress.Text);
        objParam1.AddWithValue("@NtwService", tbxService.Text);
        objParam1.AddWithValue("@NtwTelPhone", tbxTelPhone.Text);
        objParam1.AddWithValue("@NtwTelMobile", tbxTelMobile.Text);
        objParam1.AddWithValue("@NtwEmail", tbxEmail.Text);
        objParam1.AddWithValue("@NtwNotation", tbxNotation.Text);
        objParam1.AddWithValue("@NtwMap", tbxMap.Text);
        objParam1.AddWithValue("@NtwPic1", tbxPic1.Text);
        objParam1.AddWithValue("@NtwPic2", tbxPic2.Text);
        objParam1.AddWithValue("@NtwStatus", rblStatus.SelectedValue);
        objParam1.AddWithValue("@NtwSort", (decSort = (tbxSort.Text != "") ? Convert.ToDecimal(tbxSort.Text) : 9999) );
        objParam1.AddWithValue("@NtwProvince", ddlProvince.SelectedValue);
        objParam1.AddWithValue("@NtwAmphoe", ddlAmphoe.SelectedValue);

        objParam1.AddWithValue("@NtwFax", tbxNtwFax.Text);
        objParam1.AddWithValue("@NtwWebsite", tbxWebsite.Text);
        objParam1.AddWithValue("@NtwMapLink", tbxMapLink.Text);

        //===== AdminInfo Update  
        objParam1.AddWithValue("@input_date", DateTime.Now);

        //===========  Get UserName From Session Object 
        ProfileData loginData = (ProfileData)Session["SessAdmin"];
        objParam1.AddWithValue("@input_user", loginData.loginName);
          
         
        //=========== Check Insert From Category
        if (CatId == 4) { // Catagory 4 =  Branch  ****
            objParam1.AddWithValue("@NtwNumCode", tbxNumCode.Text);
            objParam1.AddWithValue("@NtwTypeId", ddlNetworkType.SelectedValue);
        } else   {// Catagory 2 =  Garage  ****
            objParam1.AddWithValue("@NtwNumCode", "");
            objParam1.AddWithValue("@NtwTypeId", ddlNetworkType.SelectedValue);
        }  

        objParam1.AddWithValue("@NtwZoneId", ucZone.StrZoneName); // Get Value from User Control ( เรียกโดย IdUserControl.FieldnameUserControl )
         
        int i1 = new DBClass().SqlExecute(sql1, objParam1);

        if (i1 == 1) {
            Response.Write("<script>alert('แก้ไขข้อมูลเรียบร้อยแล้ว')</script>");
            Response.Redirect("ntwView.aspx?id=" + NtwId + "&cat=" + CatId );
        } else {
            Response.Write("<script>alert('ไม่สามารถแก้ไขข้อมูลได้')</script>");
            Response.Write("<script>history.back()</script>");
            //Response.Redirect("");
        }
    }


}