﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---------- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;
using System.Collections;

public partial class adminWeb_cnt : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {// ก่อนกด Submit
            BindData();
        } else { // หลังกด Submit

        }


    }

    void BindData() {
        //_key = Session["key"] != null ? Session["key"].ToString() : String.Empty ;
        //_status = Session["status"] != null ? Session["status"].ToString() : String.Empty;
         
        //if (!String.IsNullOrEmpty(Key)  || !String.IsNullOrEmpty(Status) ) {
        //    GridView1.DataSource = SelectData(_key , _status );
        //    GridView1.DataBind(); 
        //    //======= Clear Session
        //    Session["key"] = "";
        //    Session["status"] = "";

        //} else {   //  String.IsNullOrEmpty(Key)  && String.IsNullOrEmpty(Status)  // ความหมายบรรทัดนี้
          
        //} 

        //======= Set Start Session
        Session["key"] = "";
        Session["status"] = "";

        //====== Set Paging
        GridView1.AllowPaging = true;
        GridView1.PageSize = 20; 
        //=======  Bind Data
        GridView1.DataSource = SelectData();
        GridView1.DataBind();
       
    }
 
    protected DataSet SelectData() {
        string sql = "SELECT *  FROM tbCnt ORDER BY CntId DESC ";
        DBClass obj = new DBClass();
        DataSet ds = obj.SqlGet(sql, "tb");
        return ds;
    }

   

     protected void btnSearch_Click(object sender, EventArgs e) {
        Session["SearchStatus"] = ddlSearchStatus.SelectedValue;
        Session["SearchKey"] =  tbxSearchKey.Text   ; 
        
        Response.Redirect("cntSearch.aspx");
    }
     
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e) {
        //========  (Hidde For  Check   ddlCatName  Select  ) ================================ 
        Label lblStatusHd = (Label)e.Row.FindControl("lblHdStatus");
        if (lblStatusHd != null) {
            lblStatusHd.Text = DataBinder.Eval(e.Row.DataItem, "CntStatus").ToString();
        }

        //======== EditItemTemplate ================================================= 
        DropDownList ddlStatus = (DropDownList)e.Row.FindControl("ddlStatus");
        if (ddlStatus != null) {
            ddlStatus.Items.Insert(0, new ListItem("ยังไม่ติดต่อ", "0")); // add Empty Item 
            ddlStatus.Items.Insert(1, new ListItem("ติดต่อแล้ว", "1")); // add Empty Item   
       
            ///// set select  in EditTemplate
            string x = (e.Row.FindControl("lblHdStatus") as Label).Text;
            ddlStatus.Items.FindByValue(x).Selected = true; 
        }


        //========  ItemTemplate ================================================= 
        Label lblId = (Label)e.Row.FindControl("lblId");
        if (lblId != null) {
            int CntId = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "CntId"));
          string strId = String.Format("{0:0000}", CntId);
          lblId.Text = strId; 
        }

        HyperLink hplDetail = (HyperLink)e.Row.FindControl("hplDetail");
        if (hplDetail != null) {
            string strId = DataBinder.Eval(e.Row.DataItem, "CntId").ToString(); ;
            string strDetail = DataBinder.Eval(e.Row.DataItem, "CntDetail").ToString();

            hplDetail.Text = ((strDetail.Length) <= 50) ? strDetail : strDetail.Substring(0, 50) + "...";
            hplDetail.NavigateUrl = "cntView.aspx?Id=" + strId;  
        }

        Label lblName = (Label)e.Row.FindControl("lblName");
        if (lblName != null) {
            lblName.Text = DataBinder.Eval(e.Row.DataItem, "CntName").ToString();
        }

        Label lblDateTimeGet = (Label)e.Row.FindControl("lblDateTimeGet");
        if (lblDateTimeGet != null) {
            DateTime dtDateTimeGet = (DateTime)DataBinder.Eval(e.Row.DataItem, "CntDateTimeGet");
            string strDateGet = dtDateTimeGet.ToString("dMMMyy");
            string strTimeGet = dtDateTimeGet.ToString("H:m");

            lblDateTimeGet.Text = strDateGet + "<br>" + strTimeGet+" น."; 
        }

        Label lblDateTimeSet = (Label)e.Row.FindControl("lblDateTimeSet");
        if (lblDateTimeSet  != null) {
           DateTime dtDateSet =  ( DateTime ) DataBinder.Eval(e.Row.DataItem, "CntDateSet")  ;
           string strDateSet = dtDateSet.ToString("dMMMyy"); 

            string CntTimeSet = DataBinder.Eval(e.Row.DataItem, "CntTimeSet").ToString();
            lblDateTimeSet.Text =( strDateSet +"<br>"+ CntTimeSet+" น." ) ;
        }

        Literal ltlStatus = (Literal)(e.Row.FindControl("ltlStatus"));
        if (ltlStatus != null) {  
                int intStatus = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "CntStatus"));
                if (intStatus == 0)
                    ltlStatus.Text = "<img src='images/check-wrong.png' > "; 
                else 
                   ltlStatus.Text = "<img src='images/check-right.png' > ";
                   
        } 

    }
     
    //======================  Pager
    protected void GridView1_IndexChanging(object sender, GridViewPageEventArgs e) {
        GridView1.PageIndex = e.NewPageIndex;
        BindData();
    }
     
    //======================  Command Gridview  
    protected void GridView1_Deleting(object s, GridViewDeleteEventArgs e) {
        int a = e.RowIndex;
        string sql2 = "DELETE  FROM  tbCnt WHERE CntId = '" + GridView1.DataKeys[e.RowIndex].Value + "'";
        int i = new DBClass().SqlExecute(sql2);
        GridView1.EditIndex = -1;
        BindData();
    }

    protected void modEditCommand(object sender, GridViewEditEventArgs e) {
        GridView1.EditIndex = e.NewEditIndex;
        BindData();
    }

    protected void modCancelCommand(object sender, GridViewCancelEditEventArgs e) {
        GridView1.EditIndex = -1;
        BindData();
    }

    protected void modUpdateCommand(object sender, GridViewUpdateEventArgs e) {
       
            DropDownList ddlStatus = (DropDownList)GridView1.Rows[e.RowIndex].FindControl("ddlStatus");  
         
            string strSQL = "UPDATE tbCnt SET " +
            "CntStatus= '" + ddlStatus.SelectedValue + "'  " + 
            " WHERE  CntId = '" + GridView1.DataKeys[e.RowIndex].Value + "'";

            DBClass obj = new DBClass();
            int i = obj.SqlExecute(strSQL);

            GridView1.EditIndex = -1;
            BindData();
    }
     

}