<%@ Page Title="" Language="C#" MasterPageFile="~/AdminWeb/MasterPageAdmin1.master" AutoEventWireup="true" CodeFile="GeneratePremiumForm.aspx.cs" Inherits="AdminWeb_GeneratePremiumForm" EnableEventValidation="false" %>
<%@ Register src="uc/ucTxtDate.ascx" tagname="ucTxtDate" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script>
        function PopulateChild(strCampaign, ddlChild) {
            var pageUrl = '<%=ResolveUrl("~/AdminWeb/GeneratePremiumForm.aspx")%>';            
            //$(ddlChild).attr("disabled", "disabled");
            //alert(strCampaign);
            $.ajax({
                async: false,
                type: "POST",
                url: pageUrl + '/GetPromotionData',
                data: '{strCampaign:"' + strCampaign + '"}',                
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(response) {
                    PopulateControl(response, $(ddlChild))
                },
                error: function(response) {
                    alert("error " + response.d);
                }
            });
        }
        function PopulateControl(response, ddlChild) {
            //alert("PopulateControl"+response.d);
            PopulateDropdownControl(response.d, ddlChild);
        }
        function PopulateDropdownControl(list, control) {
            //control.removeAttr("disabled");
            if (list.length > 0) {
                //control.empty().append('<option selected="selected" value="0">Please select</option>');
                control.empty();
                $.each(list, function() {
                control.append($("<option></option>").val(this['Value']).html(this['Text']));
            });
            control.removeAttr("disabled");            
            }
            else {
                control.empty().append('<option selected="selected" value=""> - ���͡ - <option>');
                control.attr("disabled", "disabled");    
            }
        }   
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div>
        <asp:Label ID="lblSessShow" runat="server"> </asp:Label> 
        <h1 class="subject1">Generate ���ҧ���»�Сѹ���</h1>       
        <table style="width:100%;">
            <tr>
                <td class="label">                   
                    ��Ե�ѳ�� :</td>
                <td>
                    <asp:DropDownList ID="ddlProduct" runat="server" style="margin-bottom: 0px">                        
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="label">                   
                    ������� :</td>
                <td>
                    <asp:DropDownList ID="ddlPromotion" runat="server" style="margin-bottom: 0px">                        
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="label">
                    ����ö :</td>
                <td>
                    <asp:TextBox ID="txtCarCode" runat="server" Width="40" MaxLength="3" CssClass="TEXTBOX" Text="110"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label">
                    �ع��Сѹ��µ���� :</td>
                <td>
                    <asp:TextBox ID="txtSumInsuredFrom" runat="server" CssClass="TEXTBOXMONEY" Width="100" Text="300000"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label">
                    �ع��Сѹ��¶֧ :</td>
                <td>
                    <asp:TextBox ID="txtSumInsuredTo" runat="server" CssClass="TEXTBOXMONEY" Width="100" Text="400000"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label">
                    ��ǧ��ҧ�ع��Сѹ��� :</td>
                <td>
                    <asp:DropDownList ID="ddlPeriod" runat="server" style="margin-bottom: 0px">                        
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="label">
                    �ѹ���Tax rate :</td>
                <td>
                    <uc1:ucTxtDate ID="txtStartDate" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="label">
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btnSave" runat="server" Text="Generate" onclick="btnSave_Click" CssClass="btn-default" />
                    <asp:Button ID="Button2" runat="server" Text="Cancle" CssClass="btn-default"/>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    �ó� 3 ���� �кطع੾�з���� ������ҧ�� P77 �кطع����� 150000 �֧  150000 
                </td>
            </tr>
        </table>      
        <br />
        <br />   
       <asp:DataGrid ID="dtgData" runat="server" AutoGenerateColumns="true" Width="100%"
            HeaderStyle-CssClass="gridhead" ItemStyle-CssClass="gridrow0" AlternatingItemStyle-CssClass="gridrow1"
            AllowPaging="false" PagerStyle-Visible="false">
            <Columns>
                <asp:TemplateColumn>                    
                    <ItemTemplate>
                        <%# Container.DataSetIndex +1%>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>       
        <br />
        <br />
        
        </div> <!-- end  id="page-prdList" -->
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

