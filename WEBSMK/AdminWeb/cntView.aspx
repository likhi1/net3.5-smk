﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminWeb/MasterPageAdmin1.master" AutoEventWireup="true" CodeFile="cntView.aspx.cs" Inherits="adminWeb_cntView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div id="manageArea">
        <div class="colLeft">
            <h1 class="subject1">ข้อมูลติดต่อกลับ</h1>


        </div>
        <!-- end  class="colLeft" -->
        <div class="colRight">
        </div>
        <!-- end  class="colRight" -->


    </div>
    <!-- End  id="manageArea" -->

<style>
/*===== Control  DetailsView1  Width (support IE9 & Over) =====*/
[id*=DetailsView] table { border: 1px solid #fff; }
[id*=DetailsView] td { height: 25px; border: none; border-bottom: 1px dotted #ccc; }
[id*=DetailsView] td:nth-child(1) { width: 20%; text-align: right; padding-right: 10px; vertical-align: top; }
[id*=DetailsView] td:nth-child(2) { text-align: left; padding-left: 10px; vertical-align: top;}
</style>
    <asp:DetailsView ID="DetailsView1" runat="server" Height="50px" Width="100%" OnDataBound="DetailsView1_DataBound" AutoGenerateRows="False" EnableModelValidation="True" BorderStyle="None">


        <Fields>
            <asp:TemplateField HeaderText="ID :">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblId"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="สถานะติดต่อกลับ :">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblStatus"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="เรื่อง :">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblTopic"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ชื่อลูกค้า :">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblName"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="เบอร์โทร :">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblTel"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="อีเมล์ :">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblEmail"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ข้อความ :">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblDetail"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="เวลาติดต่อมา :">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblDateGet"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="เวลาติดต่อกลับ :">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblDateSet"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Fields>


    </asp:DetailsView>

    <div style="margin: 0px auto; width: 100px; margin-top: 20px;">
        <asp:Button ID="Button1" runat="server" Text="ย้อนกลับ" class="btn-default btn-small" OnClientClick="history.back();return false;" />

    </div>
</asp:Content>

