﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminWeb/MasterPageAdmin1.master" AutoEventWireup="true" CodeFile="invView.aspx.cs" Inherits="adminWeb_invView" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    

    <!-- ===============  CKEditor Config &  CKEditor With CKFinder Config  -->
    <script>
        $(function () {
             
            /// CKFinder & CKEditer
            if (typeof CKEDITOR != 'undefined') {
                var editor = CKEDITOR.replace('<%=tarDetailLong.ClientID%>', { customConfig: '../ckeditor_config/fullMenu.js' } );
                ////////  Fix This When change location of this file  (This path for refer to file in folder  ckfinder )
                CKFinder.setupCKEditor(editor, '../ckfinder/');
            }

        });// end onload
    </script>

    <!-- ===============  Validate  -->
    <script>
        
        $(function () {
            //// Set Varbles
            tbxTopic = $("#<%=tbxTopic.ClientID%>").val(); 
            tarDetailLong = $("#<%=tarDetailLong.ClientID%>") ;
            ddlCat = $("#<%=ddlCat.ClientID%>");
            tbxSort = $("#<%=tbxSort.ClientID%>");
            btnSave = $("#<%=btnSave.ClientID%>");

            //// Check Number
            tbxSort.keypress(validateNumber); 

            btnSave.click(function ( ) { 
                return ChkNull();   
            });
  
        });

        
        function isDecimal(_val){
            patt = /(^[0-9]{4})+([.,][0-9]{1,3})?$/;     // Limit  Length = 4   And  Decimal  =  3
            re =  patt.test(_val);
            return re;
        }

        function validateNumber(event) {
            var key = window.event ? event.keyCode : event.which;
            if (event.keyCode == 8 || event.keyCode == 46
             || event.keyCode == 37 || event.keyCode == 39) {
                return true;
            }
            else if (key < 48 || key > 57) {
                return false;
            }
            else return true;
        }


        function ChkNull() {
            if (ddlCat.val() == "" || ddlCat.val() == null) {
                alert("กรุณาเลือกประเภท ");
                return false;
            } else if (tbxTopic.val() == "" || tbxTopic.val() == null) {
                alert("กรุณากรอกหัวข้อด้วย");
                return false;
            } else if (!tbxSort.val() == "") { 
                if (isDecimal(tbxSort.val()) == false) { // Limit  Length = 4   And  Decimal  =  3
                    alert("กรุณาระบุด้วย ตัวเลขแค่ 4 หลัก และมีทศนิยมได้ไม่เกิน 3 ตำแหน่ง"); return false;    
                }
            }

            return true;
        }



    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

   <div id="page-prdAdd">

        <h1 class="subject1">เพิ่มรายการ <asp:Label ID="lblPageName" runat="server"  ></asp:Label></h1>

   

        <table style="width: 100%;">

    <tr>

                <td class="label" width="100px">ID :</td>
                <td>
             <asp:Label ID="lblId" runat="server"  ></asp:Label>        

                </td> 
            </tr>
        
              <tr> 
                <td class="label" width="100px">สถานะการแสดง :</td>
                <td>
                    <asp:RadioButtonList ID="rblStatus" runat="server"
                        RepeatDirection="Horizontal" TextAlign="Left">
                        <asp:ListItem Value="0">ปิด</asp:ListItem>
                        <asp:ListItem Value="1" Selected="True">เปิด</asp:ListItem>  
                    </asp:RadioButtonList>
                </td> 
            </tr>

            <tr> 
<td class="label" >ลำดับการแสดง :</td>
<td>
<asp:TextBox ID="tbxSort" runat="server" MaxLength="8" ></asp:TextBox>  <span class="star1">สามารถกำหนดเป็นทศนิยมได้</span> 
</td>   
</tr> 
       
              <!--  2014 Update Multi Language -->
           <tr> 
                <td class="label"  >ภาษา :</td>
                <td>
                    <asp:RadioButtonList ID="rblLanguage" runat="server"
                        RepeatDirection="Horizontal" TextAlign="Left">
                        <asp:ListItem Value="0"  Selected="True">ไทย</asp:ListItem>
                        <asp:ListItem Value="1">อังกฤษ</asp:ListItem>  
                    </asp:RadioButtonList>
                </td> 
            </tr>
             
               
               <tr>
                <td class="label">ประเภท :</td>
                <td>
                    <asp:DropDownList ID="ddlCat" runat="server"   >
                           <asp:ListItem Value="">-- โปรดระบุ  --</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>



            <tr>
                <td class="label">หัวข้อ :</td>
                <td>
                    <asp:TextBox ID="tbxTopic" runat="server" Width="730px" class="ckeditor"></asp:TextBox>
                </td>
            </tr>

    
    
            <tr>
                <td class="label" valign="top">รายละเอียดเต็ม :</td>
                <td>
 
                    <asp:TextBox ID="tarDetailLong" TextMode="MultiLine" runat="server"></asp:TextBox>


                </td>
            </tr>

            <tr>
                <td class="label" valign="top">&nbsp;</td>
                <td>

                    &nbsp;</td>
            </tr>

            <tr>
                <td class="label">&nbsp;</td>
                <td>

 

                    <asp:Button ID="btnSave" runat="server" Text="บันทึก" OnClick="btnSave_Click"  CssClass="btn-default"
                        Style="margin-right: 10px;" />
                    <asp:Button ID="btnClear" runat="server" Text="ยกเลิก" CssClass="btn-default"   OnClientClick="history.back();return false;" UseSubmitBehavior="false"  />
                </td>
            </tr>
        </table>
    </div>
    <!-- end id="page-prdAdd" -->


</asp:Content>

