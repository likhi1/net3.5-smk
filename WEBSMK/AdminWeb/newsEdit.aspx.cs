﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization;


public partial class adminWeb_newsEdit  : System.Web.UI.Page
{
    //=== global Var
    protected string NewsId { get { return Request.QueryString["id"];  } }
    protected string CatId { get { return Request.QueryString["cat"]; }   }

    protected DataTable _dtCat;
    protected DataTable DtCat { get { return _dtCat; } }
    protected string PageName;
    

    protected void Page_Load(object sender, EventArgs e) {
        ////////// Check  Session 
        // new CheckSess().CurrentSession(); 
  
         
        //////// Run Bind Data  
        if (!Page.IsPostBack) {
            SelectCat(); 
            ddlCatBindData();
            GetNamePage();
            lblPageName.Text = PageName; 
            BindData();
            
        }
    }


    protected DataTable SelectCat() {
        string sql = "SELECT  NewsCatId  ,  NewsCatName  FROM tbNewsCat  ORDER BY NewsCatId ";
        DBClass obj1 = new DBClass();
        DataSet ds = obj1.SqlGet(sql, "tbNewsCaT");
        _dtCat = ds.Tables[0];
        return _dtCat; 
    }

    protected void ddlCatBindData() {
        ddlCat.DataSource = DtCat;  
        //**** Add frist Item of dropdownlist 
        ddlCat.AppendDataBoundItems = true;
        ddlCat.DataTextField = "NewsCatName";
        ddlCat.DataValueField = "NewsCatId";
        ddlCat.DataBind(); 
    }

    protected string GetNamePage() {
        foreach (DataRow r in DtCat.Rows) {
            if (r[0].ToString() == CatId) {
                PageName = r[1].ToString();
            }
        }
        return PageName;
    }
     

    protected void BindData() {
        string sql1 = "SELECT * FROM tbNewsCon WHERE  NewsConId= '" + NewsId + "'  "
            + "AND NewsCatId  ='" + CatId + "' ";
        DBClass obj1 = new DBClass();
        DataSet ds1 = obj1.SqlGet(sql1, "tblNewsCon");
        DataTable dt1 = ds1.Tables[0];

        if (dt1.Rows.Count > 0) {
            // foreach( string  x in dt1){ }  // *** see all value in  DataTable ***

            int  intId =  Convert.ToInt32( dt1.Rows[0][0].ToString() ) ;
            lblId.Text = String.Format("{0:00000}", intId);  

            ddlCat.SelectedValue = dt1.Rows[0]["NewsCatId"].ToString();
            ddlCat.Attributes.Add("disabled", "true"); 

            string  strSort =    dt1.Rows[0]["NewsConSort"].ToString()   ;
            tbxSort.Text =   Convert.ToDecimal(strSort) != 9999?strSort : "" ;

            rblStatus.SelectedValue = dt1.Rows[0]["NewsConStatus"].ToString();

            //------ Convert DateTime Format 
            object strDtShow =  dt1.Rows[0]["NewsConDtShow"];
            if (!Convert.IsDBNull(strDtShow)) {
                tbxDateShow.Text = ((DateTime)strDtShow).ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            }

            object strDtHide = dt1.Rows[0]["NewsConDtHide"];
            if (!Convert.IsDBNull(strDtHide)) {
                tbxDateHide.Text = ((DateTime)strDtHide).ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            }

             
            tbxTopic.Text = dt1.Rows[0]["NewsConTopic"].ToString();
            if (CatId != "3") { // CatId  3 As  TVC
                tbxPicBig.Text = dt1.Rows[0]["NewsPicBig"].ToString();
            } else {
                tarVdo.Text = dt1.Rows[0]["NewsVdo"].ToString(); 
            }
            
            tarDetailShort.Text = dt1.Rows[0]["NewsConDetailShort"].ToString();
            tarDetailLong.Text = dt1.Rows[0]["NewsConDetailLong"].ToString();

        }
    }

    protected void btnSave_Click(object sender, EventArgs e) {
         
        /////// Update content
        string sql = "UPDATE  tbNewsCon SET "
        + "NewsCatId = @NewsCatId, "
        + "NewsConSort = @NewsConSort, "
        + "NewsConDtShow =  @NewsConDtShow, "
        + "NewsConDtHide  =  @NewsConDtHide, " 
        + "NewsConTopic = @NewsConTopic, "
        + "NewsConDetailShort = @NewsConDetailShort , "
        + "NewsConDetailLong = @NewsConDetailLong, " 
        + "NewsConStatus = @NewsConStatus, " 
        + "NewsPicBig = @NewsPicBig, "
        + "NewsVdo = @NewsVdo, " 
         //===== AdminInfo Update  
         + "input_date = @input_date ,  "
         + "input_user = @input_user  " 
         
        + "WHERE NewsConId = @NewsId  AND NewsCatId = @CatId  ";
        

        DateTime dtDateShow = Convert.ToDateTime(tbxDateShow.Text, new CultureInfo("th-TH")); 
        DateTime dtDateHide = Convert.ToDateTime(tbxDateHide.Text, new CultureInfo("th-TH"));
        
        Decimal decSort ; 
       
        SqlParameterCollection objParam1 = new SqlCommand().Parameters;
        objParam1.AddWithValue("@NewsCatId", CatId );
        objParam1.AddWithValue("@NewsConSort", ( decSort = (tbxSort.Text != "")?Convert.ToDecimal(tbxSort.Text):9999 ) );  
        objParam1.AddWithValue("@NewsConStatus", rblStatus.SelectedValue );

        objParam1.AddWithValue("@NewsConDtShow", dtDateShow ) ;
        objParam1.AddWithValue("@NewsConDtHide", dtDateHide ) ; 

        objParam1.AddWithValue("@NewsConTopic", tbxTopic.Text ) ;
        objParam1.AddWithValue("@NewsConDetailShort", tarDetailShort.Text);
        objParam1.AddWithValue("@NewsConDetailLong", tarDetailLong.Text );
        objParam1.AddWithValue("@NewsId",  NewsId ) ;
        objParam1.AddWithValue("@CatId",  CatId) ;
        
        //===== AdminInfo Update  
        objParam1.AddWithValue("@input_date", DateTime.Now);
        //////Get UserName From Session Object
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        objParam1.AddWithValue("@input_user", loginData.loginName);

        /////// Get Value From Web Control 
        if (CatId != "3") { // CatId  3 As  TVC
            objParam1.AddWithValue("@NewsPicBig", tbxPicBig.Text);
            objParam1.AddWithValue("@NewsVdo", "");
        } else {
            objParam1.AddWithValue("@NewsPicBig", ""); 
            objParam1.AddWithValue("@NewsVdo", tarVdo.Text);
        }

         ///////  Get Value From Html Control 
        //objParam1.AddWithValue("@NewsPicSmall", SqlDbType.NVarChar).Value = Request.Form["PicSmall"].ToString();
        //objParam1.AddWithValue("@NewsPicBig", SqlDbType.NVarChar).Value = Request.Form["PicPic"].ToString();

        int i2 = new DBClass().SqlExecute(sql, objParam1); 
        //////// check Succes  
        if (i2 == 1) {
            Response.Write("<script>alert('แก้ไขข้อมูลเรียบร้อยแล้ว')</script>");
            Response.Redirect("newsEdit.aspx?id=" + NewsId + "&cat=" + CatId);  
        } else {
            Response.Write("<script>alert('ไม่สามารถแก้ไขข้อมูลได้')</script>");
            Response.Write("<script>history.go(-1)</script>");
            //Response.Redirect("");
        }

    }

}