﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization; 


public partial class adminWeb_faqAdd : System.Web.UI.Page
{ 
   protected string  CatId { get { return Request.QueryString["cat"]; } }
     

    protected void Page_Load(object sender, EventArgs e) {

        //***** Check  Session *****
        // new CheckSess().CurrentSession();  
         
        if (!Page.IsPostBack) {  

        }
        
    }

     

    protected void btnSave_Click(object sender, EventArgs e) { 
         
        string sql1 = "INSERT INTO tbFaq  VALUES  ( "
                       + "@FaqQuiz, @FaqAnswer, @FaqSort, @FaqStatus , @FaqDateReg, "
                        //=====  AdminInfo Add
                        + " @lang , "
                        + " @input_date ,   "
                        + " @input_user  "
                        + " ) ";

       
        Decimal decSort;

        SqlParameterCollection objParam1 = new SqlCommand().Parameters;
        objParam1.AddWithValue("@FaqQuiz", tbxTopic.Text);
        objParam1.AddWithValue("@FaqAnswer", tarDetailLong.Text);
        objParam1.AddWithValue("@FaqSort", (decSort = (tbxSort.Text != "") ? Convert.ToDecimal(tbxSort.Text) : 9999));
        objParam1.AddWithValue("@FaqStatus", rblStatus.SelectedValue);
        objParam1.AddWithValue("@FaqDateReg", DateTime.Today); 
         
        //=====  AdminInfo Add
        objParam1.AddWithValue("@lang", '0' ); // TH = 0 ,  En =  1
        objParam1.AddWithValue("@input_date", DateTime.Now);

        //===========  Get UserName From Session Object 
        ProfileData loginData = (ProfileData)Session["SessAdmin"];
        objParam1.AddWithValue("@input_user", loginData.loginName);

  
        int i1 = new DBClass().SqlExecute(sql1, objParam1); 

        if (i1 == 1) {
            Response.Write("<script>alert('เพิ่มข้อมูลเรียบร้อยแล้ว'); location='faqList.aspx' ;</script>");
        } else {
            Response.Write("<script>alert('ไม่สามารถเพิ่มข้อมูลได้'); history.back() ;</script>");

        }



    }

}