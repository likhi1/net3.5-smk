<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TestQuery.aspx.cs" Inherits="TestQuery" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
       
        <asp:TextBox ID="txtSQL" runat="server" Rows="10" TextMode="MultiLine" Width="588px"></asp:TextBox>
        <asp:Button ID="btnExe" runat="server" OnClick="btnExe_Click" Text="Execute" /><br />
        <br />
        <br />
        <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
        <asp:GridView ID="gvData" runat="server">            
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>No.</HeaderTemplate>
                    <ItemTemplate>
                        <%# Container.DataItemIndex +1%>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    </form>
</body>
</html>
