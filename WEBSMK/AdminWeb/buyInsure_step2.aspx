﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminWeb/MasterPagePopUp.master" AutoEventWireup="true" CodeFile="buyInsure_step2.aspx.cs" Inherits="buyInsure_step2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div id="page-online-insCar">
        <div class="prc-row">        
        <h2>
        เลขที่อ้างอิง : <asp:Label ID="lblRefNo" runat="server" Text=""></asp:Label>
        </h2>
        <h2 class="subject-detail">รายละเอียดผู้เอาประกัน</h2>
        <table width="630px">
            <tr>
                <td valign="top"  class="label">
                    ท่านสนใจการประกันภัย :
                </td>
                <td colspan="2">
                    <asp:Label ID="lblPolType" runat="server" Text="" CssClass="data"></asp:Label>
                </td>        
            </tr>
            <tr>
                <td  class="label">
                    ชื่อ-นามสกุล :
                </td>
                <td colspan="2">
                    <asp:Label ID="lblName" runat="server" Text="" CssClass="data"></asp:Label>
                </td>
            </tr>
            <tr>
                <td  class="label">
                    เบอร์โทรศัพท์/มือถือ :
                </td>
                <td colspan="2">
                    <asp:Label ID="lblTelNo" runat="server" Text="" CssClass="data"></asp:Label>
                </td>
            </tr>
            <tr>
                <td  class="label">
                    Email :
                </td>
                <td colspan="2">
                    <asp:Label ID="lblEmail" runat="server" Text="" CssClass="data"></asp:Label>
                </td>
            </tr>
            <tr>
                <td  class="label">
                    เวลาสะดวกติดต่อกลับ :
                </td>
                <td colspan="2">
                    <asp:Label ID="lblTime" runat="server" Text="" CssClass="data"></asp:Label>
                </td>        
            </tr>
            <tr>
                <td valign="top" class="label">
                    ข้อมูลเพิ่มเติม :
                </td>
                <td colspan="2" valign="top">
                    <asp:Label ID="lblRemark" runat="server" Text="" CssClass="data"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>
    <!-- End  <div class="context page-app"> -->
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

