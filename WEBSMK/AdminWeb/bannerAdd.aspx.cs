﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//---- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//---------- DateTime Region
using System.Globalization; 


public partial class adminWeb_bannerAdd : System.Web.UI.Page
{
    protected string CatId { get { return Request.QueryString["cat"]; } }


    protected void Page_Load(object sender, EventArgs e) { 

        if (!Page.IsPostBack) {  

        }
        
    }

     

    protected void btnSave_Click(object sender, EventArgs e) {
        string sql1 = "INSERT INTO tbBanner "
                         + "VALUES ( "
                         + "@BannerCat,  "
                         + "@BannerTitle,  "
                         + "@BannerPic,  "
                         + "@BannerStatus,  "
                         + "@BannerStart,  "
                         + "@BannerStop,  "
                         + "@BannerSort,  "
                         + "@BannerLink,  " 
                        //====  AdminInfo Add 
                        + " @lang,   "
                        + " @input_date,  "
                        + " @input_user   "
                        + " ) ";

         
        SqlParameterCollection objParam1 = new SqlCommand().Parameters;
        objParam1.AddWithValue("@BannerCat", CatId );
        objParam1.AddWithValue("@BannerTitle", tbxTitle.Text);
        objParam1.AddWithValue("@BannerPic", tbxPic.Text);
        objParam1.AddWithValue("@BannerStatus", rblStatus.SelectedValue);
        objParam1.AddWithValue("@BannerStart", "");
        objParam1.AddWithValue("@BannerStop", "");
        objParam1.AddWithValue("@BannerSort",  (!String.IsNullOrEmpty (tbxSort.Text))? Convert.ToDecimal(tbxSort.Text) : Convert.ToDecimal(9999)  );
       
       // string strTatget = ( ddlLang.SelectedValue  == "0")? tarLink.Text : "en/"+tarLink.Text ;
        objParam1.AddWithValue("@BannerLink", tarLink.Text); 

        //====  AdminInfo Add 
        objParam1.AddWithValue("@lang", ddlLang.SelectedValue); // Th = 0 , En = 1
        objParam1.AddWithValue("@input_date", DateTime.Now);
        ///////// Get UserName From Session Object
        //   ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);   
        objParam1.AddWithValue("@input_user", "");



        int i1 = new DBClass().SqlExecute(sql1, objParam1); 

        if (i1 == 1) {
            Response.Write("<script>alert('เพิ่มข้อมูลเรียบร้อยแล้ว'); location='bannerList.aspx?cat="+CatId+"' ;</script>");
        } else {
            Response.Write("<script>alert('ไม่สามารถเพิ่มข้อมูลได้'); history.back() ;</script>");

        }



    }

}