﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminWeb/MasterPageAdmin1.master" AutoEventWireup="true" CodeFile="jobApplyView.aspx.cs" Inherits="adminWeb_jobApplyView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <style>
[id*=FormView] { width:100% ;}
#viewData   { border: 1px solid #fff;    }
#viewData   .title { font-weight:bold; text-align:left; border-bottom : 1px solid  #ddd; }
#viewData  td { padding:2px;height: 20px; border: none; /*border : 1px solid  #ddd;*/ /*border-bottom: 1px dotted #ddd;*/ vertical-align: top;  } 
#viewData  .label { width: 25%; text-align:right; }
 

#tbWorkXp { width:  100%; }
#tbWorkXp  td { padding:2px;height: 20px; border: none;  
                vertical-align: top; text-align:left; width:35% ;  } 
#tbWorkXp .label { width: 15%; text-align:right;  }
 


    </style> 


    <%if( Convert.ToInt32( _catId) == 1 ){ %>
     
    
     
<asp:FormView ID="FormView1" runat="server"  OnDataBound="FormView1_DataBound"  >

<ItemTemplate>



      
<table style="width: 100%;" id="viewData"    > 
<tr> 
<td  colspan="4" class="title" > ข้อมูลการสมัคร   </td > 
</tr> 
      
<tr>
<td  class="label" >ID :</td>
<td colspan="3"  >   <asp:Label runat="server" ID="lblId"></asp:Label></td>  
    
</tr>


    <tr>

<td  class="label" >วันที่สมัคร :</td>
<td colspan="3" >  <asp:Label runat="server" ID="lblDateReg"></asp:Label></td>   

    </tr>
     <tr>
                <td  class="label">เงินเดือนที่ต้องการ :</td>
            <td colspan="3" > <asp:Label runat="server" ID="lblSalary"></asp:Label></td> 
        </tr>

      <tr>
            <td  class="label" >ตำแหน่งงาน :</td>
            <td  colspan="3" > <asp:Label runat="server" ID="lblPosition"></asp:Label></td>
         
        </tr>

<tr> 
    <td  colspan="4" class="title" > ข้อมูลส่วนตัว  </td > 
</tr> 
      
    
    
      
 
         <tr>
            <td  class="label" >ชื่อ  :</td>
            <td>   <asp:Label runat="server" ID="lblName"></asp:Label></td>  
<td  class="label" >วันเกิด : </td>
            <td>  <asp:Label runat="server" ID="lblBirth"></asp:Label></td> 
   

        </tr>
        
     <tr>
<td  class="label" >เลขบัตรประชาชน : </td>
<td>   <asp:Label runat="server" ID="lblIdCard"></asp:Label> </td>  

  
<td  class="label" >น้ำหนัก : </td>
        <td>  <asp:Label runat="server" ID="lblWeight"></asp:Label> </td> 

 
            </tr>




<tr>

          <td  class="label" >อายุ :</td>
            <td>  <asp:Label runat="server" ID="lblAge"></asp:Label> </td> 
<td  class="label" >ส่วนสูง : </td>
<td> <asp:Label runat="server" ID="lblHeight"></asp:Label></td> 
</tr>  
        
  

<tr>
<td class="label" >เบอร์โทรศัพท์ : </td>
<td ><asp:Label runat="server" ID="lblPrfTel1"></asp:Label> </td>          
<td class="label" >ศาสนา : </td>
<td><asp:Label runat="server" ID="lblReligion"></asp:Label> </td>   
</tr>
     
<tr>
<td  class="label" >เบอร์มือถือ : </td>
<td > <asp:Label runat="server" ID="lblPrfTel2"></asp:Label> </td>            
<td  class="label" >สัญชาติ: </td> 
<td>  <asp:Label runat="server" ID="lblNationality"></asp:Label> </td>  
</tr>

<tr>
<td  class="label" >อีเมล : </td>
<td  >   <asp:Label runat="server" ID="lblPrfEmail"></asp:Label> </td> 
<td  class="label" >พันธะทางทหาร : </td>
<td> <asp:Label runat="server" ID="lblMilitary"></asp:Label> </td>  
</tr>
      

        <tr>
        <td  class="label" >ที่อยู่ปัจจุบันที่ติดต่อได้ : </td>
        <td colspan="3">   <asp:Label runat="server" ID="lblAddress1"></asp:Label> </td> 
        </tr>

         <tr>
        <td  class="label" >ที่อยู่ตามบัตรประชาชน : </td>
        <td colspan="3">  <asp:Label runat="server" ID="lblAddress2"></asp:Label></td> 
        </tr>
 

 
    <tr> 
           <td  colspan="4" class="title" >การศึกษา </td > 
</tr>

        <tr>
        <td  class="label" >ประวัติการศึกษา : </td>
        <td colspan="3">  <asp:Label runat="server" ID="lblEduAll"></asp:Label> </td> 
        </tr>

        <tr>
        <td  class="label" >กิจกรรมระหว่างศึกษา : </td>
        <td  colspan="3">  <asp:Label runat="server" ID="lblEduActivity"></asp:Label>  </td> 
        </tr>
<tr>
  
              <td  colspan="4" class="title" > ประสบการณ์ </td >  
</tr>
<tr>
<td  class="label" colspan="4" > <asp:Label runat="server" ID="lblPastWork"></asp:Label> </td> 
</tr>

   


    <tr>
  
              <td  colspan="4" class="title" >ทักษะ</td > 
</tr>
         <tr>
        <td  class="label" >ความสามารถทางภาษา : </td>
        <td  colspan="3">     <asp:Label runat="server" ID="lblSkLanguage"></asp:Label> </td> 
        </tr>

         <tr>
        <td  class="label" >ความสามารถทางคอมพิวเตอร์  : </td>
        <td  colspan="3">   <asp:Label runat="server" ID="lblSkCom"></asp:Label> </td> 
        </tr>

          <tr>
        <td  class="label" >ความสามารถอื่นๆ  : </td>
        <td  colspan="3">     <asp:Label runat="server" ID="lblSkOther"></asp:Label> </td> 
        </tr>

          <tr>
        <td  class="label" > ข้อมูลอื่นๆ : </td>
        <td  colspan="3">    <asp:Label runat="server" ID="lblMorInfo"></asp:Label> </td> 
        </tr>
         
    </table>

</ItemTemplate>

     

    </asp:FormView>


    <%}else{ %>

 <asp:FormView ID="FormView2" runat="server"  OnDataBound="FormView2_DataBound"   >
<ItemTemplate>
 
    
      
<table style="width: 100%;" id="viewData"    > 
<tr> 
<td  colspan="4" class="title" > ข้อมูลการสมัคร   </td > 
</tr> 
      
<tr>
<td  class="label" >ID :</td>
<td colspan="3"  >   <asp:Label runat="server" ID="lblId"></asp:Label></td>  
    
</tr>


    <tr>

<td  class="label" >วันที่สมัคร :</td>
<td colspan="3" >  <asp:Label runat="server" ID="lblDateReg"></asp:Label></td>   

    </tr>
    

      <tr>
            <td  class="label" >ตำแหน่งงาน :</td>
            <td  colspan="3" > <asp:Label runat="server" ID="lblPosition"></asp:Label></td>
         
        </tr>

<tr> 
    <td  colspan="4" class="title" > ข้อมูลส่วนตัว  </td > 
</tr> 
      
    
    
      
 
         <tr>
            <td  class="label" >ชื่อ  :</td>
            <td colspan="3" >   <asp:Label runat="server" ID="lblName"></asp:Label></td> 
        </tr>


 

    <tr>
<td  class="label" >เลขบัตรประชาชน : </td>
<td colspan="3" >   <asp:Label runat="server" ID="lblIdCard"></asp:Label> </td> 
</tr>  
        
        
     <tr>
            <td  class="label" >อายุ :</td>
            <td colspan="3" >  <asp:Label runat="server" ID="lblAge"></asp:Label> </td> 
            </tr>

   <tr>
    <td  class="label" >วันเกิด : </td>
            <td colspan="3" >  <asp:Label runat="server" ID="lblBirth"></asp:Label></td>  
    </tr>



  

<tr>
<td class="label" >เบอร์โทรศัพท์ : </td>
<td colspan="3" ><asp:Label runat="server" ID="lblPrfTel1"></asp:Label> </td>          
 </tr>
     
<tr>
<td  class="label" >เบอร์มือถือ : </td>
<td colspan="3" > <asp:Label runat="server" ID="lblPrfTel2"></asp:Label> </td>            
 
</tr>

<tr>
<td  class="label" >อีเมล : </td>
<td colspan="3"  >   <asp:Label runat="server" ID="lblPrfEmail"></asp:Label> </td> 
 
</tr>
      

        <tr>
        <td  class="label" >ที่อยู่ปัจจุบันที่ติดต่อได้ : </td>
        <td colspan="3">   <asp:Label runat="server" ID="lblAddress1"></asp:Label> </td> 
        </tr>

         <tr>
        <td  class="label" >ที่อยู่ตามบัตรประชาชน : </td>
        <td colspan="3">  <asp:Label runat="server" ID="lblAddress2"></asp:Label></td> 
        </tr>
 

  


     

          <tr>
        <td  class="label" > ข้อมูลอื่นๆ : </td>
        <td  colspan="3">    <asp:Label runat="server" ID="lblMorInfo"></asp:Label> </td> 
        </tr>
         
    </table>


</ItemTemplate> 

</asp:FormView>


    <%} %>



    <div id="areaButton"  style="margin: 0px auto; width: 100px; margin-top: 20px;">
        <asp:Button ID="Button1" runat="server" Text="ย้อนกลับ" class="btn-default btn-small" OnClientClick="history.back();return false;" />

    </div>

</asp:Content>

