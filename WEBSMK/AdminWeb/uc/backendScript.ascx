﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="backendScript.ascx.cs" Inherits="uc_scriptBackend" %>
<link rel="shortcut icon" href="<%=Config.SiteUrl%>favicon.ico" /> 

<link rel="stylesheet" href='<%=Config.BackendUrl%>css/globalBackend.css'   /> 
<script src="<%=Config.SiteUrl%>scripts/jquery-1.9.1.js"></script>    
<script src="<%=Config.SiteUrl%>scripts/jqueryUI/jquery-ui-1.10.3.custom.js"></script>
<link href="<%=Config.SiteUrl%>scripts/jqueryUI/redmond/jquery-ui-1.10.3.custom.css" rel="stylesheet" />

<!--================ DatePicker -->   
<script src="<%=Config.SiteUrl%>scripts/datepicker/_BuddhistEra/scripts/jquery.ui.datepicker-th.js" type="text/javascript" charset="utf-8"></script>	    	
<script src="<%=Config.SiteUrl%>scripts/datepicker/_BuddhistEra/scripts/jquery.ui.datetimepicker.js" type="text/javascript" charset="utf-8"></script>	
<script src="<%=Config.SiteUrl%>scripts/datepicker/_BuddhistEra/scripts/jquery.ui.datepicker.ext.be.js" type="text/javascript" charset="utf-8"></script>	

<style>
#ui-datepicker-div { width: 12em; }
.ui-state-default { font-weight: normal; }
.ui-datepicker td a { padding: 0em; }
.ui-datepicker th { padding: 0em 0em; }
</style> 

 