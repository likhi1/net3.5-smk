﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucZone.ascx.cs" Inherits="adminWeb_uc_ucZone" %>

<asp:DropDownList ID="ddlZone" runat="server" OnSelectedIndexChanged="ddlZone_SelectedIndexChanged"  >
<asp:ListItem Value="" Selected="True" >-- โปรดระบุ  --</asp:ListItem>
<asp:ListItem Value="1">กรุงเทพฯ & ปริมณฑล</asp:ListItem>
<asp:ListItem Value="2">ภาคกลาง</asp:ListItem>
<asp:ListItem Value="3">ภาคตะวันออก</asp:ListItem>
<asp:ListItem Value="4">ภาคตะวันออกเฉียงเหนือ</asp:ListItem>
<asp:ListItem Value="5">ภาคเหนือ</asp:ListItem>
<asp:ListItem Value="6">ภาคใต้</asp:ListItem>
    <asp:ListItem Value="7">ภาคตะวันตก</asp:ListItem>
    
</asp:DropDownList>