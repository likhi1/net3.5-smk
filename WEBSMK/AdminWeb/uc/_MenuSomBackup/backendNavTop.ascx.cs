﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminWeb_backendNavTop  : System.Web.UI.UserControl
{
    public ProfileData loginData;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["SessAdmin"] != null)
        {
            loginData = (ProfileData)Session["SessAdmin"];
        }
        else
        {
            Page.Response.Redirect("main.aspx");
        }
    }
}
