﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="backendNavTop.ascx.cs" Inherits="adminWeb_backendNavTop" %>


<!-- ====== Navigation Top ====== -->
<script>
    $(function () {
//$(".subNav").hide();
        $("#nav-backend ul  li ").hover(function (e) {
            $('ul.subNav', this).slideDown(300);  //show its submenu
        },
		function () {
		    $('ul.subNav', this).slideUp(300);  //hide its submenu
		})
    });
</script>

 
<div id="menuArea"  >
<div id="nav-backend" >
    <ul>  
        <% if (loginData.roleID == "001" )
           {%>
        <li><a href="userList.aspx" class="first">จัดการผู้ใช้</a> </li>
        <%} %>
        <% if (loginData.roleID == "001" ||
               loginData.roleID == "002")
           {%>
       <li><a href="prdList.aspx" class="first">สินค้าประกัน</a> </li>
        <li><a href="#">CMS อื่นๆ</a> 
        <ul class="subNav subNav-01"   style="display:none"  >
        <li><a href="newsList.aspx?cat=1">สาระน่ารู้</a></li>
        <li><a href="newsList.aspx?cat=2">ข่าวสาร</a></li>  
	   <li><a href="newsList.aspx?cat=3">Static Page</a></li>
       <li><a href="newsList.aspx?cat=4">TVC สินมั่นคง</a></li>  
  <li><a href="newsList.aspx?cat=5">ข่าวตัวแทน & คู่ค้า</a></li> 
            
        </ul>
        </li>

         <li><a href="#"  >เครือข่าย</a>
         <ul class="subNav subNav-02"  style="display:none"  >
        <li><a href="ntwList.aspx?cat=1">โรงพยาบาล</a></li>
        <li><a href="ntwList.aspx?cat=2">อู่ในสัญญา</a></li>
        <li><a href="ntwList.aspx?cat=3">ศูนย์ซ่อมรถ</a></li> 
        <li><a href="ntwList.aspx?cat=4">สาขา</a></li> 
        </ul>
        </li>
        
       <li><a href="cntList.aspx"  >ติดต่อกลับ</a></li> 
        <li><a href="invList.aspx"  >นักลงทุน</a></li> 
          <li><a href="faqList.aspx"  >ถามตอบ</a></li> 
        <li><a href="#"  >สมัครงาน</a>
        <ul class="subNav subNav-03"   style="display:none"   >
            <li><a href="jobPositionList.aspx?cat=1">ตำแหน่งพนักงาน</a></li>
             <li><a href="jobPositionList.aspx?cat=2">ตำแหน่งตัวแทน</a></li> 
                   <li><a href="jobApplyList.aspx?cat=1">ผู้สมัครพนักงาน</a></li> 
        <li><a href="jobApplyAgentList.aspx?cat=2">ผู้สมัครตัวแทน</a></li>   </ul>
        </li> 

	
         <ul class="subNav subNav-04"   style="display:none"   >
        <li><a href="ntwList.aspx?cat=1">โรงพยาบาล</a></li>
        <li><a href="ntwList.aspx?cat=2">อู่ในสัญญา</a></li>
        <li><a href="ntwList.aspx?cat=3">ศูนย์ซ่อมรถ</a></li> 
        <li><a href="ntwList.aspx?cat=4">สาขา</a></li> 
        </ul>
        </li>
        <%} %>
        <% if (loginData.roleID == "001" ||
               loginData.roleID == "003")
           {%>
        <li><a href="#">คำนวณเบี้ย</a>         
        <ul class="subNav subNav-05"  style="display:none">
        <li><a href="GeneratePremiumForm.aspx">Generate ตารางเบี้ย</a></li>
        <li><a href="WebPremiumForm.aspx">Import ข้อมูลเข้าเบี้ย Web</a></li>
        <li><a href="UpdateWebPremiumForm.aspx">จัดการเบี้ย Web</a></li>                 
        </ul>
        </li>
        <%} %>
        <% if (loginData.roleID == "001" ||
               loginData.roleID == "004")
           {%>
        <li><a href="ApproveForm.aspx" class="first">ตรวจรับแจ้ง</a> </li>
        <%} %>                      
        <li ><a href="#">เปลี่ยนพาสเวิส</a></li>
          <li><a href="<%=Config.BackendUrl%>logout.aspx" class="last" >Logout</a></li> 
         
    </ul>
</div>
<!-- End id="nav-top" -->


 

</div><!--  End id="menuArea" ------>