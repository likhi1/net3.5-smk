﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="backendNavTop.ascx.cs" Inherits="adminWeb_backendNavTop" %>



<!-- ====== Navigation Top ====== -->
 
 
<script>
    $(function () {
        //========== Set Event for Button  
        $("#nav-backend > ul > li").each(function () { //  prevent li Level2
            var obj = $(this);
            var sub = obj.find('.subNav');
            var btLevel1 = obj.children('a');

            obj.hover(function () {
                // obj.addClass('selected');
                btLevel1.addClass('selected');
                sub.slideDown();  //show its submenu 
            }, function () {
                //obj.removeClass('selected');
                btLevel1.removeClass('selected');
                myTimer = setTimeout(function () { sub.slideUp(); }, 300);
            })
        })

    });   // end ready
</script>

 
<div id="menuArea"  >
<div id="nav-backend" >
    <ul>  
        <% if (loginData.roleID == "001" )
           {%>
        <li><a href="userList.aspx" class="first">จัดการผู้ใช้</a> </li>
        <%} %>
        <% if (loginData.roleID == "001" ||
               loginData.roleID == "002")
           {%>
        <li><a href="#">จัดการไฟล์</a> 
			<ul class="subNav subNav-07"  style="display:none"  >
			<li><a href="mangeFile.aspx">จัดการไฟล์</a></li>
			<li><a href="bannerList.aspx?cat=1">แบนเนอร์-ด้านบน</a></li>
			<li><a href="bannerList.aspx?cat=2">แบนเนอร์-ด้านข้าง</a></li>
			</ul>
       </li>
       <li><a href="prdList.aspx" class="first">สินค้าประกัน</a> </li>
       
<li><a href="#">CMS อื่นๆ</a> 
<ul class="subNav subNav-01"   style="display:none"  >
<li><a href="newsList.aspx?cat=1">สาระน่ารู้</a></li>
<li><a href="newsList.aspx?cat=2">ข่าวสาร</a></li> 
<li><a href="newsList.aspx?cat=3">TVC สินมั่นคง</a></li> 
<li><a href="newsList.aspx?cat=4">ตัวแทน & คู่ค้า</a></li>  
<li><a href="newsList.aspx?cat=5">เนื้อหา Static</a></li> 
<li><a href="invList.aspx"  >นักลงทุน</a></li> 
<li><a href="faqList.aspx"  >ถามตอบ</a></li> 
</ul>
</li>

         <li><a href="#"  >เครือข่าย</a>
         <ul class="subNav subNav-02"  style="display:none"  >
        <li><a href="ntwList.aspx?cat=1">โรงพยาบาล</a></li>
        <li><a href="ntwList.aspx?cat=2">อู่ในสัญญา</a></li>
        <li><a href="ntwList.aspx?cat=3">ศูนย์ซ่อมรถ</a></li> 
        <li><a href="ntwList.aspx?cat=4">สาขา</a></li> 
        </ul>
        </li>
        
    
     <li><a href="cntList.aspx"  >ร้องเรียน</a></li>    
	
       <!--- <ul class="subNav subNav-04"   style="display:none"   >
        <li><a href="ntwList.aspx?cat=1">โรงพยาบาล</a></li>
        <li><a href="ntwList.aspx?cat=2">อู่ในสัญญา</a></li>
        <li><a href="ntwList.aspx?cat=3">ศูนย์ซ่อมรถ</a></li> 
        <li><a href="ntwList.aspx?cat=4">สาขา</a></li> 
        </ul>
        </li>-->
        
        
        <%} %>
<% if (loginData.roleID == "006" )
{%>
            <li><a href="#"  >เครือข่าย</a>
         <ul class="subNav subNav-02"  style="display:none"  >
        <li><a href="ntwList.aspx?cat=1">โรงพยาบาล</a></li>
        <li><a href="ntwList.aspx?cat=2">อู่ในสัญญา</a></li>
        <li><a href="ntwList.aspx?cat=3">ศูนย์ซ่อมรถ</a></li> 
        <li><a href="ntwList.aspx?cat=4">สาขา</a></li> 
        </ul>
        </li>
 <%} %> 
 
 <% if (loginData.roleID == "007" )
{%>
   <li><a href="cntList.aspx"  >ร้องเรียน</a></li>  
 <%} %>
        
<% if (loginData.roleID == "001" || loginData.roleID == "005")
{%>    
     <li><a href="#"  >สมัครงาน</a>
        <ul class="subNav subNav-03"   style="display:none"   >
            <li><a href="jobPositionList.aspx?cat=1">ตำแหน่งพนักงาน</a></li>
                   <li><a href="jobApplyList.aspx?cat=1">ผู้สมัครพนักงาน</a></li></ul>
        </li> 
		
  <%} %> 
  <% if (loginData.roleID == "001" || loginData.roleID == "008")
{%>    
     <li><a href="#"  >ตัวแทน</a>
        <ul class="subNav subNav-03"   style="display:none"   >
            <li><a href="jobPositionList.aspx?cat=2">ตำแหน่งตัวแทน</a></li> 
        <li><a href="jobApplyList.aspx?cat=2">ผู้สมัครตัวแทน</a></li>   </ul>
        </li> 
  <%} %> 
  
 <% if (loginData.roleID == "001" ||
               loginData.roleID == "003")
{%>
        <li><a href="#">คำนวณเบี้ย</a>         
        <ul class="subNav subNav-05"  style="display:none">
        <li><a href="GeneratePremiumForm.aspx">Generate ตารางเบี้ย</a></li>
        <li><a href="WebPremiumForm.aspx">Import ข้อมูลเข้าเบี้ย Web</a></li>
        <li><a href="UpdateWebPremiumForm.aspx">จัดการเบี้ย Web</a></li>                 
        <li><a href="OnlineCarmarkList.aspx">จัดการรุ่นรถ</a></li>
        </ul>
        </li>
        <%} %>
        <% if (loginData.roleID == "001" ||
               loginData.roleID == "004")
           {%>
        <li>
            <a href="#">ตรวจสอบการรับแจ้ง</a> 
            <ul class="subNav subNav-06"   style="display:none" >
            <li><a href="ApproveForm.aspx">ซื้อประกัน</a></li>
            <li><a href="ContactList.aspx">ติดต่อกลับ</a></li>
            <li><a href="PolicyList.aspx">รายการประกันที่อนุมัติ</a></li>
            </ul>
        </li>
        <%} %>                      
        <li ><a href="<%=Config.BackendUrl%>ChangePasswordForm.aspx">เปลี่ยนพาสเวิส</a></li>
          <li><a href="<%=Config.BackendUrl%>logout.aspx" class="last" >Logout</a></li> 
         
    </ul>
</div>
<!-- End id="nav-top" -->


 

</div><!--  End id="menuArea" ------>