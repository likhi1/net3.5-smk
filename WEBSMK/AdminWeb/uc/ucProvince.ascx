﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucProvince.ascx.cs" Inherits="adminWeb_uc_ucProvince" %>

<asp:DropDownList ID="ddlProvince" runat="server" OnSelectedIndexChanged="ddlProvince_SelectedIndexChanged">
     <asp:ListItem Value="" Selected="True" >-- โปรดระบุ  --</asp:ListItem>
    <asp:ListItem>กรุงเทพฯ</asp:ListItem>
    <asp:ListItem>กระบี่ </asp:ListItem>
    <asp:ListItem>กาญจนบุรี </asp:ListItem>
    <asp:ListItem>กาฬสินธุ์ </asp:ListItem>
    <asp:ListItem>กำแพงเพชร </asp:ListItem>
    <asp:ListItem>ขอนแก่น</asp:ListItem>
    <asp:ListItem>จันทบุรี</asp:ListItem>
    <asp:ListItem>ฉะเชิงเทรา </asp:ListItem>
    <asp:ListItem>ชัยนาท </asp:ListItem>
    <asp:ListItem>ชัยภูมิ </asp:ListItem>
    <asp:ListItem>ชุมพร </asp:ListItem>
    <asp:ListItem>ชลบุรี </asp:ListItem>
    <asp:ListItem>เชียงใหม่ </asp:ListItem>
    <asp:ListItem>เชียงราย </asp:ListItem>
    <asp:ListItem>ตรัง </asp:ListItem>
    <asp:ListItem>ตราด </asp:ListItem>
    <asp:ListItem>ตาก </asp:ListItem>
    <asp:ListItem>นครนายก </asp:ListItem>
    <asp:ListItem>นครปฐม </asp:ListItem>
    <asp:ListItem>นครพนม </asp:ListItem>
    <asp:ListItem>นครราชสีมา </asp:ListItem>
    <asp:ListItem>นครศรีธรรมราช </asp:ListItem>
    <asp:ListItem>นครสวรรค์ </asp:ListItem>
    <asp:ListItem>นราธิวาส </asp:ListItem>
    <asp:ListItem>น่าน </asp:ListItem>
    <asp:ListItem>นนทบุรี </asp:ListItem>
    <asp:ListItem>บึงกาฬ</asp:ListItem>
    <asp:ListItem>บุรีรัมย์</asp:ListItem>
    <asp:ListItem>ประจวบคีรีขันธ์ </asp:ListItem>
    <asp:ListItem>ปทุมธานี </asp:ListItem>
    <asp:ListItem>ปราจีนบุรี </asp:ListItem>
    <asp:ListItem>ปัตตานี </asp:ListItem>
    <asp:ListItem>พะเยา </asp:ListItem>
    <asp:ListItem>พระนครศรีอยุธยา </asp:ListItem>
    <asp:ListItem>พังงา </asp:ListItem>
    <asp:ListItem>พิจิตร </asp:ListItem>
    <asp:ListItem>พิษณุโลก </asp:ListItem>
    <asp:ListItem>เพชรบุรี </asp:ListItem>
    <asp:ListItem>เพชรบูรณ์ </asp:ListItem>
    <asp:ListItem>แพร่ </asp:ListItem>
    <asp:ListItem>พัทลุง </asp:ListItem>
    <asp:ListItem>ภูเก็ต </asp:ListItem>
    <asp:ListItem>มหาสารคาม </asp:ListItem>
    <asp:ListItem>มุกดาหาร </asp:ListItem>
    <asp:ListItem>แม่ฮ่องสอน </asp:ListItem>
    <asp:ListItem>ยโสธร </asp:ListItem>
    <asp:ListItem>ยะลา </asp:ListItem>
    <asp:ListItem>ร้อยเอ็ด </asp:ListItem>
    <asp:ListItem>ระนอง </asp:ListItem>
    <asp:ListItem>ระยอง </asp:ListItem>
    <asp:ListItem>ราชบุรี</asp:ListItem>
    <asp:ListItem>ลพบุรี </asp:ListItem>
    <asp:ListItem>ลำปาง </asp:ListItem>
    <asp:ListItem>ลำพูน </asp:ListItem>
    <asp:ListItem>เลย </asp:ListItem>
    <asp:ListItem>ศรีสะเกษ</asp:ListItem>
    <asp:ListItem>สกลนคร</asp:ListItem>
    <asp:ListItem>สงขลา </asp:ListItem>
    <asp:ListItem>สมุทรสาคร </asp:ListItem>
    <asp:ListItem>สมุทรปราการ </asp:ListItem>
    <asp:ListItem>สมุทรสงคราม </asp:ListItem>
    <asp:ListItem>สระแก้ว </asp:ListItem>
    <asp:ListItem>สระบุรี </asp:ListItem>
    <asp:ListItem>สิงห์บุรี </asp:ListItem>
    <asp:ListItem>สุโขทัย </asp:ListItem>
    <asp:ListItem>สุพรรณบุรี </asp:ListItem>
    <asp:ListItem>สุราษฎร์ธานี </asp:ListItem>
    <asp:ListItem>สุรินทร์ </asp:ListItem>
    <asp:ListItem>สตูล </asp:ListItem>
    <asp:ListItem>หนองคาย </asp:ListItem>
    <asp:ListItem>หนองบัวลำภู </asp:ListItem>
    <asp:ListItem>อำนาจเจริญ </asp:ListItem>
    <asp:ListItem>อุดรธานี </asp:ListItem>
    <asp:ListItem>อุตรดิตถ์ </asp:ListItem>
    <asp:ListItem>อุทัยธานี </asp:ListItem>
    <asp:ListItem>อุบลราชธานี</asp:ListItem>
    <asp:ListItem>อ่างทอง </asp:ListItem>

</asp:DropDownList>


