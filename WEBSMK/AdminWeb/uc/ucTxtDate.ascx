<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucTxtDate.ascx.cs" Inherits="UserControl_ucTxtDate" %>
<%@ Register Src="ucCalendar.ascx" TagName="ucCalendar" TagPrefix="uc1" %>
<table cellpadding="0" cellspacing="0">
    <tr valign="middle">
        <td><asp:TextBox ID="txtDate" runat="server" Width="80" CssClass="TEXTBOX"
                    mindate="" maxdate=""
                    maxlength="10" onkeydown="dateOnKeyDown()" 
                    onpaste="dateOnPaste(this)" onbeforedeactivate="return isDate(this)" value="__/__/____"></asp:TextBox></td>
        <td>&nbsp;</td>
        <td>
            <div id="divCalendar" runat="server">
            <uc1:ucCalendar ID="UcCalendar1" runat="server" />
            </div>
        </td>
    </tr>
</table>


