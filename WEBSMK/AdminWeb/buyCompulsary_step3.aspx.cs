﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class buyCompulsary_step3 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        RegisterManager cmRegister = new RegisterManager();
        //if (Session["DSRegister"] != null)
        //    ds = (DataSet)Session["DSRegister"];
        //else
        //    Response.Redirect("buyVoluntary_step1.aspx", true);
        if (Request.QueryString["regis_no"] != "")
        {
            ds = cmRegister.getDataCompulsary(Request.QueryString["regis_no"]);
        }
        else
        {
            Response.Redirect("buyCompulsary_step1.aspx", true);
        }

        if (!IsPostBack)
        {
            docToUI(ds);
        }
    }
    public void docToUI(DataSet ds)
    {
        string strMotGar = "";
        DataSet dsPrem = new DataSet();
        DataSet dsTmp = new DataSet();
        OnlineVoluntaryManager cmOnlineVol = new OnlineVoluntaryManager();        
        OnlineCompulsaryManager cmOnline = new OnlineCompulsaryManager();        

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            lblRefNo.Text = ds.Tables[0].Rows[0]["rg_regis_no"].ToString();
            // รายละเอียดผู้เอาประกัน
            lblFName.Text = ds.Tables[0].Rows[0]["rg_ins_fname"].ToString();
            lblLName.Text = ds.Tables[0].Rows[0]["rg_ins_lname"].ToString();
            lblAddress.Text = ds.Tables[0].Rows[0]["rg_ins_addr1"].ToString();
            lblSubDistrict.Text = ds.Tables[0].Rows[0]["rg_ins_addr2"].ToString();
            lblDistrict.Text = ds.Tables[0].Rows[0]["rg_ins_amphor"].ToString();
            dsTmp = cmOnlineVol.getDataProvinceByCode(ds.Tables[0].Rows[0]["rg_ins_changwat"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                lblProvince.Text = dsTmp.Tables[0].Rows[0]["ds_desc"].ToString();
            lblPostCode.Text = ds.Tables[0].Rows[0]["rg_ins_postcode"].ToString();
            //lblIDCard.Text = ds.Tables[0].Rows[0]["rg_ins_idcard"].ToString();
            //lblMobile.Text = ds.Tables[0].Rows[0]["rg_ins_mobile"].ToString();
            lblTelephone.Text = ds.Tables[0].Rows[0]["rg_ins_tel"].ToString();
            lblEmail.Text = ds.Tables[0].Rows[0]["rg_ins_email"].ToString();            

            // รายละเอียดรถยนต์
            dsTmp = cmOnline.getBrandCompByCode(ds.Tables[0].Rows[0]["mv_major_cd"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                lblCarName.Text = dsTmp.Tables[0].Rows[0]["mdc_desc"].ToString();
            dsTmp = cmOnline.getCategoryCompByCode(ds.Tables[0].Rows[0]["mv_major_cd"].ToString(), ds.Tables[0].Rows[0]["mv_minor_cd"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                lblCarType.Text = dsTmp.Tables[0].Rows[0]["mdc_desc"].ToString();
            lblCarUseDesc.Text = ds.Tables[0].Rows[0]["cp_veh_cd"].ToString();
            dsTmp = cmOnline.getSizeCompByCategoryByCode(ds.Tables[0].Rows[0]["mv_major_cd"].ToString(), 
                                                            ds.Tables[0].Rows[0]["mv_minor_cd"].ToString(),
                                                            "",
                                                            ds.Tables[0].Rows[0]["cp_veh_cd"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                lblCarSize.Text = dsTmp.Tables[0].Rows[0]["mdcv_desc"].ToString();            
            dsTmp = cmOnlineVol.getDataProvinceByCode(ds.Tables[0].Rows[0]["mv_license_area"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                lblLicenseProvice.Text = dsTmp.Tables[0].Rows[0]["ds_desc"].ToString();
            lblLicenseNo.Text = ds.Tables[0].Rows[0]["mv_license_no"].ToString();
            lblCarRegisYear.Text = Convert.ToString(Convert.ToDouble(ds.Tables[0].Rows[0]["mv_veh_year"].ToString()) + 543);
            lblEngineNo.Text = ds.Tables[0].Rows[0]["mv_engin_no"].ToString();
            lblChasis.Text = ds.Tables[0].Rows[0]["mv_chas_no"].ToString();
            lblStartDate.Text = FormatStringApp.FormatDate(ds.Tables[0].Rows[0]["rg_effect_dt"].ToString());
            // เบี้ย 
            double dPrem = 0;
            double dStamp = 0;
            double dTax = 0;
            double dGross = 0;
            double dPaid = 0;
            dPrem = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["rg_prmm"]);
            dStamp = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["rg_stamp"]);
            dTax = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["rg_tax"]);
            dGross = dPrem + dStamp + dTax;
            dPaid = dGross;
            lblPremium.Text = FormatStringApp.FormatNDigit(ds.Tables[0].Rows[0]["rg_prmm"], 2) + " บาท";
            lblStamp.Text = FormatStringApp.FormatNDigit(ds.Tables[0].Rows[0]["rg_stamp"], 2) + " บาท";
            lblVat.Text = FormatStringApp.FormatNDigit(ds.Tables[0].Rows[0]["rg_tax"], 2) + " บาท";
            lblGrossPremium.Text = FormatStringApp.FormatNDigit(dGross, 2) + " บาท";
            dPrem = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["cp_comp_prmm"]);
            dStamp = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["cp_comp_stamp"]);
            dTax = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["cp_comp_tax"]);
            dGross = dPrem + dStamp + dTax;
            dPaid = dPaid + dGross;
            //lblComPremium.Text = FormatStringApp.FormatNDigit(dGross, 2) + " บาท";
            //lblPaid.Text = FormatStringApp.FormatNDigit(dPaid, 2) + " บาท";

            // การชำระเงิน
            if (ds.Tables[0].Rows[0]["rg_pay_type"].ToString() == "1")
            {
                lblPaidMethod.Text = "ชำระเงินด้วยเงินสดหรือเช็ค ที่บริษัทสินมั่นคงประกันภัยจำกัด (มหาชน) ที่</br>";
                if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "100")
                    lblPaidMethod.Text += "สำนักงานใหญ่ (ถนนศรีนครินทร์)";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "100")
                    lblPaidMethod.Text += "สำนักงานใหญ่ (ถนนศรีนครินทร์)";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "101")
                    lblPaidMethod.Text += "สาขาสวนมะลิ (ยสเส)";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "102")
                    lblPaidMethod.Text += "สาขาดอนเมือง";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "102")
                    lblPaidMethod.Text += "สาขาบางแค";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "130")
                    lblPaidMethod.Text += "สาขารัตนาธิเบศร์";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "500")
                    lblPaidMethod.Text += "สาขาชลบุรี";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "201")
                    lblPaidMethod.Text += "สาขาพิษณุโลก";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "200")
                    lblPaidMethod.Text += "สาขาเชียงใหม่";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "601")
                    lblPaidMethod.Text += "สาขาหาดใหญ่";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "600")
                    lblPaidMethod.Text += "สาขาสุราษฎร์ธานี";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "300")
                    lblPaidMethod.Text += "สาขานครปฐม";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "401")
                    lblPaidMethod.Text += "สาขาขอนแก่น";
            }
            else if (ds.Tables[0].Rows[0]["rg_pay_type"].ToString() == "2")
            {
                lblPaidMethod.Text = "ชำระเงิน ด้วยวิธีการโอนเข้าบัญชีของบริษัทสินมั่นคงประกันภัย จำกัด (มหาชน)";
            }
            else if (ds.Tables[0].Rows[0]["rg_pay_type"].ToString() == "3")
            {
                lblPaidMethod.Text = "ชำระเงิน ด้วยวิธีการโอนเข้าบัญชีของบริษัทสินมั่นคงประกันภัย จำกัด(มหาชน) ผ่าน www.scbeasy.com";
            }
            else if (ds.Tables[0].Rows[0]["rg_pay_type"].ToString() == "4")
            {
                lblPaidMethod.Text = "บริการจัดส่งและจัดเก็บเบี้ยประกันถึงบ้าน (เฉพาะภายในเขตกรุงเทพฯ)";
            }
            else if (ds.Tables[0].Rows[0]["rg_pay_type"].ToString() == "5")
            {
                lblPaidMethod.Text = "ชำระเงิน ด้วยบัตรเครดิต";
            }
            // จัดส่งเอกสาร
            if (ds.Tables[0].Rows[0]["rg_ref_bank"].ToString() == "1")
                lblSendDoc.Text = "ที่อยู่ตามกรมธรรม์";
            else
            {
            }

        }
    }
}
