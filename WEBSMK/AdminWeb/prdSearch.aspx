﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/adminWeb/MasterPageAdmin1.master" CodeFile="prdSearch.aspx.cs" Inherits="adminWeb_prdSearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
            <script>


                function CheckNull(tbx) {
                    console.log(tbx.id);
                    tbxSort = $("#" + tbx.id);

                    if (!tbxSort.val() == "") {
                        // alert("กรุณาระบุ ลำดับด้วย");  
                        if (isDecimal(tbxSort.val()) == false) {
                            alert("กรุณาระบุ ให้ถูกต้อง และมีทศนิยมได้ไม่เกิน 3 ตำแหน่ง"); return false;
                        }
                    }
                    return true;
                }


                function isDecimal(_val) {
                    patt = /^[0-9]+([.,][0-9]{1,3})?$/;
                    re = patt.test(_val);
                    return re;
                }

                function validateNumber(event) {
                    var key = window.event ? event.keyCode : event.which;
                    if (event.keyCode == 8 || event.keyCode == 46
                     || event.keyCode == 37 || event.keyCode == 39) {
                        return true;
                    }
                    else if (key < 48 || key > 57) {
                        return false;
                    }
                    else return true;
                }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div id="page-prdList">

<div id="manageArea"> 
<div  class="colLeft">
         
<h1 class="subject1">สินค้าประกันภัย</h1>
<div class="navManage">
<asp:Button ID="Button2" runat="server" Text="เพิ่มรายการ" class="btn-default" OnClientClick="location ='prdAdd.aspx' ;return false;" Style="margin-right: 5px;" />
</div>

</div><!-- end  class="colLeft" --> 
<div class="colRight">
<div class="areaSearch">
<table width="100%" >
<tr>
    <td  width="110px"  >

        </td>
<td  align="right">ประเภท : </td>
<td     width="5%"    >
<asp:DropDownList ID="ddlSearchCat" runat="server">
<asp:ListItem Value="">-- โปรดระบุ  --</asp:ListItem>
</asp:DropDownList> 
</td>     
<td    width="5%"     ><asp:TextBox ID="tbxKeySearch" runat="server"></asp:TextBox>
</td>
                              
<td    width="5%"     > <asp:Button ID="btnSearch" runat="server" Text="ค้นหา" class="btn-default btn-small"   OnClick="btnSearch_Click"  UseSubmitBehavior="False"  />
</td>              
</tr>
              
          
</table>
      
    </div>
<!-- End  class="areaSearch" -->

<div class="notaion"> *** การแสดงหน้าเวบ 1.ต้องมีการเปิดการใช้งาน 2.วันที่ปัจจุบันไม่เกินวันที่หยุดแสดง</div>
</div><!-- end  class="colRight" -->
</div><!-- End  id="manageArea" -->
 



<style> 
        /*===== Control  GridView1  Width (support IE9 & Over) =====*/ 
    

         [id*=GridView] th:nth-child(1) {width: 5% ; }
        [id*=GridView] th:nth-child(2) { }
        [id*=GridView] th:nth-child(3) {width:10%; }
        [id*=GridView] th:nth-child(4) {width: 7% } 
        [id*=GridView] th:nth-child(5) {width:7% } 
        [id*=GridView] th:nth-child(6) {width: 7% }
        [id*=GridView] th:nth-child(7) {width:  7% } 
        [id*=GridView] th:nth-child(8) {width:  14% }  
        
          
        </style>

<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False"  Width="100%" EnableModelValidation="True" 
    DataKeyNames="PrdConId"
    OnRowDataBound="GridView1_DataBound" 
    OnRowDeleting="GridView1_Deleting" 
    OnPageIndexChanging="GridView1_IndexChanging" 
    OnRowEditing="modEditCommand"
    OnRowUpdating="modUpdateCommand"
    OnRowCancelingEdit="modCancelCommand"
>

 
  
            <Columns>

                <asp:TemplateField HeaderText="ID">
                    <%-- แสดงเวลาปรกติ --%>
                    <ItemTemplate >
                        <div style="float:right">
                        <asp:Label ID="lblId" runat="server"  ></asp:Label></div>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="หัวข้อ"  >
                    <ItemTemplate  >
                        <asp:HyperLink ID="hplTopic" runat="server"  > 
                        </asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="หมวดหมู่">
                    <ItemTemplate>
                          <div style="text-align:center">
                        <asp:Label ID="lblCatName" runat="server"></asp:Label></div>
                    </ItemTemplate>

                      <EditItemTemplate>
                          <asp:Label ID="lblHdCatId" runat="server"   Visible="false"></asp:Label>
                          <asp:DropDownList ID="ddlCatName" runat="server"   >   </asp:DropDownList>
                    </EditItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="สถานะ">
                    <ItemTemplate>
                          <div style="text-align:center"> 
                        <asp:Image ID="imgStatus" runat="server" /> 
                          </div>
                    </ItemTemplate>

                     <EditItemTemplate>
                            <asp:Label ID="lblHdStatus" runat="server"   Visible="false"></asp:Label>
                          <asp:DropDownList ID="ddlStatus" runat="server"   > </asp:DropDownList>
                    </EditItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="แสดง<br>ลำดับ ">
                    <ItemTemplate>
                           <div style="text-align:center">
                        <asp:Label ID="lblSort" runat="server"></asp:Label></div>
                    </ItemTemplate>

                  <EditItemTemplate>
                               <asp:Label ID="lblHdSort" runat="server"   Visible="false"></asp:Label>
                      <asp:TextBox ID="tbxSort" runat="server"></asp:TextBox>
                    </EditItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="วันที่เริ่ม"> 
                    <ItemTemplate><div style="text-align:center">
                        <asp:Label ID="lblDtCreate" runat="server"  ></asp:Label></div>
                    </ItemTemplate>
                </asp:TemplateField>

                     <asp:TemplateField HeaderText="วันที่หยุด"> 
                    <ItemTemplate><div style="text-align:center">
                        <asp:Label ID="lblDtStop" runat="server"  ></asp:Label></div>
                    </ItemTemplate>
                </asp:TemplateField>
                 

       <asp:TemplateField HeaderText="จัดการ" ShowHeader="False">
                    <ItemTemplate>
                        <div style="text-align: center">
                            <asp:Button ID="btnRowEdit" runat="server" Text="แก้ไข" class="btn-default btn-small " CommandName="Edit" />
                            <asp:Button ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Delete" Text="ลบ"
                                class="btn-default btn-small " OnClientClick="return confirm('ยืนยันทำการ ลบข้อมูล')" />
                        </div>
                    </ItemTemplate>


                    <EditItemTemplate>
                        <div style="text-align: center">
                     
                        <asp:Button ID="btnRowUpdate" runat="server" Text="บันทึก" class="btn-default btn-small " CommandName="Update" />
                               <asp:Button ID="btnRowCancel" runat="server" Text="ยกเลิก" class="btn-default btn-small " CommandName="Cancel" />
                     
                        </div>
                    </EditItemTemplate>
                </asp:TemplateField>


            </Columns>
            <PagerSettings Mode="NumericFirstLast" />
            <RowStyle Height="20px" />
        </asp:GridView>



        <br />
        <br />

          <div class="areaBtnBottom"style="position:absolute; bottom:0px; ">
            <asp:Button ID="Button1" runat="server" Text="ย้อนกลับ" class="btn-default" OnClientClick="location ='prdList.aspx' ;return false;"  Style="margin-right:10px;" UseSubmitBehavior="False" />
             
        </div>

    </div>
    <!-- end  id="page-prdList" -->

 

</asp:Content>
