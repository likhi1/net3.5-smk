﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminWeb/MasterPageAdmin1.master" AutoEventWireup="true"
    CodeFile="bannerAdd.aspx.cs" Inherits="adminWeb_bannerAdd" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
     <!-- ===============  Date  Picker - Config -->
    <script>
        $(function(){
            $("[id*=tbxDate]").attr('readOnly', 'true');
            $("[id*=tbxDate]").datepicker({ 
                changeMonth: true,
                changeYear: true,
                yearRange: "-0:+10",
                isBE:true,
                autoConversionField: false 
            })
             
        }); 
    </script>
     
   
    <!-- ===============  validate  --> 
    <script>
        
        $(function () { 

          
            //===Check Number
            $("#<%=tbxSort.ClientID%>").keypress(validateNumber);

            

            $("#<%=btnSave.ClientID%>").click(function ( ) {  
            //// Set Varbles 
            tbxTitle = $("#<%=tbxTitle.ClientID%>").val(); 
            tbxPic = $("#<%=tbxPic.ClientID%>").val();
            
         
                if (tbxTitle == "" || tbxPic == "" ) {
                    alert("กรุณาป้อนข้อมูลให้ครบ");  
                    return false;   
                }else{  
                    return true; 
                } 

            return false;   
            })
  
        });

        
        function isDecimal(_val){
            patt = /^[0-9]+([.,][0-9]{1,3})?$/ ;
            re =  patt.test(_val);
            return re;
        }

        function validateNumber(event) {
            var key = window.event ? event.keyCode : event.which;
            if (event.keyCode == 8 || event.keyCode == 46
             || event.keyCode == 37 || event.keyCode == 39) {
                return true;
            }
            else if (key < 48 || key > 57) {
                return false;
            }
            else return true;
        }
 

    </script>
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div id="page-prdAdd">

        <h1 class="subject1">เพิ่มรายการ แบนเนอร์
<%
string  strPageName = ( Convert.ToInt32 (CatId) == 1 ?"- ด้านบน": "- ด้านข้าง" ) ;
Response.Write(strPageName);  
%>
     
        </h1>

        <table style="width: 100%;">
            <tr>

                <td class="label" width="100px">สถานะการแสดง :</td>
                <td>
                    <asp:RadioButtonList ID="rblStatus" runat="server"
                        RepeatDirection="Horizontal" TextAlign="Left">
                        <asp:ListItem Value="0">ปิด</asp:ListItem>
                        <asp:ListItem Value="1" Selected="True">เปิด</asp:ListItem>  
                    </asp:RadioButtonList>
                </td> 
            </tr>
            </table>

        <table style="width: 100%;">

                    <tr> 
                
                   <td class="label">ลำดับการแสดง :</td>
                <td>
                    <asp:TextBox ID="tbxSort" runat="server" MaxLength="8" ></asp:TextBox>  <span class="star1">สามารถกำหนดเป็นทศนิยมได้</span>

                    
                    </td>
            </tr>    
             

            <tr>
                
                   <td class="label">ภาษา :</td>
                <td>
                    <asp:DropDownList ID="ddlLang" runat="server">
                        <asp:ListItem Selected="True" Value="0">TH</asp:ListItem>
                        <asp:ListItem Value="1">EN</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>

    
    
            <tr>
                <td class="label">ชื่อแบนเนอร์ :</td>
                <td>
                    <asp:TextBox ID="tbxTitle" runat="server" Width="730px" class="ckeditor"></asp:TextBox>
                </td>
            </tr>

    
    
            <tr>
                <td class="label" valign="top">รูปภาพ :</td>
                <td>
 
      <asp:TextBox ID="tbxPic" runat="server"   Width="400px"    ></asp:TextBox> 
<input type="button" value="Browse Server" onclick="BrowseServer('Images:/', '<%=tbxPic.ClientID%>');" /> 

<%
if ( Convert.ToInt32 (CatId) == 1) {
    Response.Write("<span class='star1'>รูปภาพขนาด 870 พิกเซล x 185 พิกเซล</span>");
}else{
    Response.Write("<span class='star1'>รูปภาพขนาด 203 พิกเซล x 76 พิกเซล</span>");
}
%>
                    
                </td>
            </tr>

            <tr>
                <td class="label" valign="top">ลิงค์ไปหน้า :</td>
                <td>

                    <asp:TextBox ID="tarLink" runat="server" Width="760px" Columns="2" TextMode="MultiLine"></asp:TextBox>
                    <div class="star1">ลิงค์ที่ใส่ต้องเป็น Path เต็มและต้องมี http:// นำหน้า</div>
                </td>
            </tr>

            <tr>
                <td class="label">&nbsp;</td>
                <td>

 

                    &nbsp;</td>
            </tr>

            <tr>
                <td class="label">&nbsp;</td>
                <td>

 

                    <asp:Button ID="btnSave" runat="server" Text="บันทึก" OnClick="btnSave_Click"  CssClass="btn-default"
                        Style="margin-right: 10px;" />
                    <asp:Button ID="btnClear" runat="server" Text="ยกเลิก" CssClass="btn-default"  OnClientClick="history.back();return false;"  UseSubmitBehavior="false"  />
                </td>
            </tr>
        </table>
    </div>
    <!-- end id="page-prdAdd" -->

</asp:Content>

