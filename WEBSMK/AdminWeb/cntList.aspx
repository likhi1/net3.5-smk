﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminWeb/MasterPageAdmin1.master" AutoEventWireup="true" CodeFile="cntList.aspx.cs" Inherits="adminWeb_cnt"  EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server"> 
    
        <div id="manageArea">
            <div  class="colLeft">
        <h1 class="subject1">ข้อมูลร้องเรียน</h1>

        <div class="navManage">
          
        </div>
                </div><!-- end  class="colLeft" -->
            <div class="colRight">
<div class="areaSearch">
 
 
            <table  width="100%"  >
                <tr>   <td  width="110px"  >

        </td>
                    <td class="label"  >สถานะการติดต่อ : </td>
                    <td  > 
                        <asp:DropDownList ID="ddlSearchStatus"   runat="server"     width="90px"  >
                            <asp:ListItem Value="">-- โปรดระบุ  --</asp:ListItem>
                              <asp:ListItem Value="0">ยังไม่ติดต่อ</asp:ListItem>
                              <asp:ListItem Value="1">ติดต่อแล้ว</asp:ListItem>

                        </asp:DropDownList>
                    </td>
                    <td  >
                        <asp:TextBox ID="tbxSearchKey" runat="server"  AutoPostBack="true" ></asp:TextBox>
                    </td>


                    <td>
                        <asp:Button ID="btnSearch" runat="server" Text="ค้นหา" class="btn-default btn-small" OnClick="btnSearch_Click"  UseSubmitBehavior="false" />
                    </td>
                </tr>


            </table>


</div>
<!-- End  class="areaSearch" -->

 

</div><!-- end  class="colRight" -->


</div><!-- End  id="manageArea" -->

      <style> 
    /*===== Control  GridView1  Width (support IE9 & Over) =====*/ 
    [id*=GridView] th:nth-child(1) {width: 5% ;   }
    [id*=GridView] th:nth-child(2) { }
    [id*=GridView] th:nth-child(3) {width:10%;   }
    [id*=GridView] th:nth-child(4) {width: 10% ; } 
    [id*=GridView] th:nth-child(5) {width:10% ;  } 
    [id*=GridView] th:nth-child(6) {width: 8% ;   }
    [id*=GridView] th:nth-child(7) {width:  13% ;   }  
    [id*=GridView] td:nth-child(1) { text-align:right; }
    [id*=GridView] td:nth-child(3) { text-align:center; }
    [id*=GridView] td:nth-child(4) { text-align:center; } 
    [id*=GridView] td:nth-child(5) { text-align:center; } 
    [id*=GridView] td:nth-child(6) { text-align:center; }
    [id*=GridView] td:nth-child(7) { text-align:center; }  
    </style>


<asp:GridView ID="GridView1" runat="server" EnableModelValidation="True"  AutoGenerateColumns="False"  Width="100%"  
        DataKeyNames="CntId"
        OnRowDataBound="GridView1_RowDataBound" 
        OnRowDeleting="GridView1_Deleting"
        OnPageIndexChanging="GridView1_IndexChanging"
        OnRowEditing="modEditCommand"
        OnRowUpdating="modUpdateCommand"
        OnRowCancelingEdit="modCancelCommand">
     
    
        <Columns>

            <asp:TemplateField HeaderText="ID">
            <ItemTemplate> 
            <asp:Label ID="lblId" runat="server" ></asp:Label>
            </ItemTemplate> 
            </asp:TemplateField>
             
            <asp:TemplateField HeaderText="เรื่องติดต่อ">
            <ItemTemplate> 
                <asp:HyperLink ID="hplDetail" runat="server"></asp:HyperLink> 
            </ItemTemplate> 
            </asp:TemplateField>

           <asp:TemplateField HeaderText="ชื่อลูกค้า">
            <ItemTemplate> 
            <asp:Label ID="lblName" runat="server" ></asp:Label>
            </ItemTemplate> 
            </asp:TemplateField>

            <asp:TemplateField HeaderText="เวลา<br>ติดต่อมา">
            <ItemTemplate> 
            <asp:Label ID="lblDateTimeGet" runat="server"  ></asp:Label>
            </ItemTemplate> 
            </asp:TemplateField>

             <asp:TemplateField HeaderText="เวลา<br>ติดต่อกลับ">
            <ItemTemplate> 
            <asp:Label ID="lblDateTimeSet" runat="server" ></asp:Label>
            </ItemTemplate> 
            </asp:TemplateField>

          
              <asp:TemplateField HeaderText="สถานะ<br>ติดต่อกลับ">
                <ItemTemplate> 
                     <asp:Literal ID="ltlStatus" runat="server"></asp:Literal>

                
                </ItemTemplate>
               
                    <EditItemTemplate>
                         <asp:Label ID="lblHdStatus" runat="server"   Visible="false"></asp:Label>
                          <asp:DropDownList ID="ddlStatus" runat="server"   > </asp:DropDownList>
                    </EditItemTemplate>
            </asp:TemplateField> 
         

               <asp:TemplateField HeaderText="จัดการ" ShowHeader="False">
                    <ItemTemplate>
                        <div style="text-align: center">
                            <asp:Button ID="btnRowEdit" runat="server" Text="ตั้งค่า" class="btn-default btn-small " CommandName="Edit" />
                            <asp:Button ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Delete" Text="ลบ"
                                class="btn-default btn-small " OnClientClick="return confirm('ยืนยันทำการ ลบข้อมูล')" />
                        </div>
                    </ItemTemplate> 

                    <EditItemTemplate>
                        <div style="text-align: center"> 
                        <asp:Button ID="btnRowUpdate" runat="server" Text="บันทึก" class="btn-default btn-small " CommandName="Update" />
                        <asp:Button ID="btnRowCancel" runat="server" Text="ยกเลิก" class="btn-default btn-small " CommandName="Cancel" /> 
                        </div>
                    </EditItemTemplate>
          </asp:TemplateField>
            

        </Columns>

       <PagerSettings Mode="NumericFirstLast" />
            <RowStyle Height="20px" />

    </asp:GridView>



</asp:Content>

