﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class onlineIns : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initialDropdownList();
            ddlHour.SelectedValue = DateTime.Now.Hour.ToString("00");
            ddlMinute.SelectedValue = DateTime.Now.Minute.ToString("00");
            string strMainClass = "";
            if (Request.QueryString["main_class"] != null)
            {
                strMainClass = Request.QueryString["main_class"].ToString();
                if (strMainClass.ToUpper() == "M")
                    chkMotor.Checked = true;
                else if (strMainClass.ToUpper() == "S")
                    chkSea.Checked = true;
                else if (strMainClass.ToUpper() == "F")
                    chkFire.Checked = true;
                else if (strMainClass.ToUpper() == "P")
                    chkPersonal.Checked = true;
                else if (strMainClass.ToUpper() == "H")
                    chkHealth.Checked = true;
                else if (strMainClass.ToUpper() == "C")
                    chkCancer.Checked = true;
                else if (strMainClass.ToUpper() == "O")
                    chkOther.Checked = true;
            }
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        string strTmpCD = "";
        RegisterManager cmRegister = new RegisterManager();
        ds = UIToDoc();
        strTmpCD = cmRegister.insertDataContact(ds);
        ds.Tables[0].Rows[0]["ct_code"] = strTmpCD;
        Session.Add("DSRegister", ds);
        if (strTmpCD != "")
            Response.Redirect("buyInsure_step2.aspx?ct_code=" + strTmpCD, true);
    }

    public void initialDropdownList()
    {
        DataSet ds;
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        string[] columnDaysNames = { "text", "value" };
        string[] columnDaysTypes = { "string", "string" };
        
        ds = new DataSet();
        ds.Tables.Add("");        
        TDS.Utility.MasterUtil.AddColumnToDataTable(ds.Tables[0], columnDaysTypes, columnDaysNames);       
        for (int i = 0; i <= 24; i++)
        {
            ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());          
            if(i != 24){
				ds.Tables[0].Rows[i]["text"] = (i + 1).ToString("00");
				ds.Tables[0].Rows[i]["value"] = (i + 1).ToString("00");
            }else{
			    ds.Tables[0].Rows[i]["text"] = (0).ToString("00");
				ds.Tables[0].Rows[i]["value"] = (0).ToString("00");
            }
        }
        ddlHour.DataSource = ds;
        ddlHour.DataTextField = "text";
        ddlHour.DataValueField = "value";
        ddlHour.DataBind();

        ds = new DataSet();
        ds.Tables.Add("");
        TDS.Utility.MasterUtil.AddColumnToDataTable(ds.Tables[0], columnDaysTypes, columnDaysNames);
        for (int i = 0; i <= 60; i++)
        {
            ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
            if(i != 60){
				ds.Tables[0].Rows[i]["text"] = (i + 1).ToString("00");
				ds.Tables[0].Rows[i]["value"] = (i + 1).ToString("00");
            }else{
				ds.Tables[0].Rows[i]["text"] = (0).ToString("00");
				ds.Tables[0].Rows[i]["value"] = (0).ToString("00");
            }
        }
        ddlMinute.DataSource = ds;
        ddlMinute.DataTextField = "text";
        ddlMinute.DataValueField = "value";
        ddlMinute.DataBind();
    }
    public DataSet UIToDoc()
    {
        DataSet dsData;
        DataRow drData;
        string[] columnNames = { "ct_code", "name", "tel_no", "email", "contact_time", "f_motor", "f_fire", "f_sea", "f_pa", "f_health", "f_cancer", "f_other", "remark" };
        string[] columnTypes = { "string", "string", "string", "string", "string", "string", "string", "string", "string", "string", "string", "string", "string" };

        dsData = new DataSet();
        dsData.Tables.Add();
        TDS.Utility.MasterUtil.AddColumnToDataTable(dsData.Tables[0], columnTypes, columnNames);
        drData = dsData.Tables[0].NewRow();
        drData["name"] = txtName.Text;
        drData["tel_no"] = txtTelephone.Text;
        drData["email"] = txtEmail.Text;
        drData["contact_time"] = ddlHour.SelectedValue + ":" + ddlMinute.SelectedValue;
        if (chkMotor.Checked)
            drData["f_motor"] = "Y";
        else
            drData["f_motor"] = "N";
        if (chkFire.Checked)
            drData["f_fire"] = "Y";
        else
            drData["f_fire"] = "N";
        if (chkSea.Checked)
            drData["f_sea"] = "Y";
        else
            drData["f_sea"] = "N";
        if (chkPersonal.Checked)
            drData["f_pa"] = "Y";
        else
            drData["f_pa"] = "N";
        if (chkOther.Checked)
            drData["f_other"] = "Y";        
        else
            drData["f_other"] = "N";
        if (chkHealth.Checked)
            drData["f_health"] = "Y";
        else
            drData["f_health"] = "N";
        if (chkCancer.Checked)
            drData["f_cancer"] = "Y";
        else
            drData["f_cancer"] = "N";
        drData["remark"] = txtRemark.Text;
        dsData.Tables[0].Rows.Add(drData);
        return dsData;
    }
}