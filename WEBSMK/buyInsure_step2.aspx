﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="buyInsure_step2.aspx.cs" Inherits="buyInsure_step2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div id="page-online-insCar">
        <h1 class="subject1">ซื้อประกัน</h1>
        ระบบบันทึกข้อมูลการติดต่อกลับของท่านเรียบร้อยแล้ว หากท่านมีข้อสงสัยเพิ่มเติม สามารถติดต่อสอบถามได้ที่ 02-378-7167<br />
        <br />        
        <div class="prc-row">                
        <h2 class="subject-detail">รายละเอียดผู้เอาประกัน</h2>
        <table width="630px">
            <tr>
                <td>
                     <table class="tb-online-insCar">
                        <tr>
                            <td valign="top"  class="label" nowrap>
                                ท่านสนใจการประกันภัย :
                            </td>
                            <td>
                                <asp:Label ID="lblPolType" runat="server" Text="" CssClass="data"></asp:Label>
                            </td>        
                        </tr>
                        <tr>
                            <td  class="label">
                                ชื่อ-นามสกุล :
                            </td>
                            <td>
                                <asp:Label ID="lblName" runat="server" Text="" CssClass="data"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td  class="label">
                                เบอร์โทรศัพท์/มือถือ :
                            </td>
                            <td>
                                <asp:Label ID="lblTelNo" runat="server" Text="" CssClass="data"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td  class="label">
                                Email :
                            </td>
                            <td>
                                <asp:Label ID="lblEmail" runat="server" Text="" CssClass="data"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td  class="label">
                                เวลาสะดวกติดต่อกลับ :
                            </td>
                            <td>
                                <asp:Label ID="lblTime" runat="server" Text="" CssClass="data"></asp:Label>
                            </td>        
                        </tr>
                        <tr>
                            <td valign="top" class="label">
                                ข้อมูลเพิ่มเติม :
                            </td>
                            <td valign="top">
                                <asp:Label ID="lblRemark" runat="server" Text="" CssClass="data"></asp:Label>
                            </td>
                        </tr>
                     </table>
                </td>
            </tr>            
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </div>
        <strong>ขอบคุณที่ท่านไว้วางใจเลือกใช้บริการกับ บริษัท สินมั่นคงประกันภัย จำกัด</strong>
        <br />
        <br />
        <div style="margin: 0px auto; width: 190px;  margin-top: 15px;">
            <a href="home.aspx">กลับสู่หน้าแรก</a><br />
        </div>
    </div>
    <div style="display:none">
        <h2>
        เลขที่อ้างอิง : <asp:Label ID="lblRefNo" runat="server" Text=""></asp:Label>
        </h2>
    </div>
    <!-- End  <div class="context page-app"> -->
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

