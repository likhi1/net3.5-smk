﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data;
using System.Web.Services;

public partial class buyCompulsary_step1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initialDropdownList();
            ddlBrand.Attributes.Add("onchange", "PopulateCategory()");
            ddlCategory.Attributes.Add("onchange", "PopulateSize()");
            rdoPersonal.Attributes.Add("onclick", "PopulateSize()");
            rdoWork.Attributes.Add("onclick", "PopulateSize()");
            rdoRent.Attributes.Add("onclick", "PopulateSize()");            
        }
    }
    protected void btnCalculate_Click(object sender, EventArgs e)
    {
		TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        double dStartDate = 0;
        double dMinDate = 0;
        string strMinDate = "";
        string strScript = "";

        if (DateTime.Now.Hour >= 16 && DateTime.Now.Minute >= 30)
        {
            dMinDate = Convert.ToDouble(cmDate.convertDateStringForDBTH(FormatStringApp.FormatDate(DateTime.Now.AddDays(1))).Replace("/",""));
            strMinDate = FormatStringApp.FormatDate(DateTime.Now.AddDays(1));
        }
        else
        {
            dMinDate = Convert.ToDouble(cmDate.convertDateStringForDBTH(FormatStringApp.FormatDate(DateTime.Now)).Replace("/",""));
            strMinDate = FormatStringApp.FormatDate(DateTime.Now);
        }
        dStartDate = Convert.ToDouble(cmDate.convertDateStringForDBTH(txtStartDate.Text).Replace("/",""));        
        if (dStartDate < dMinDate)
        {
            strScript = "<script>";
            strScript += " document.getElementById('divLoading').style.display = 'none';";
			strScript += "showMessagePage('วันที่เริ่มคุ้มครองต้องมีค่าตั้งแต่วันที่ " + strMinDate + "');";
			strScript += " </script>";
			ScriptManager.RegisterClientScriptBlock(Literal1, typeof(Literal), "s", strScript, false);
        }
		else
		{	
			calPremium();
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        string strTmpCD = "";
        RegisterManager cmRegister = new RegisterManager();
		TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        double dStartDate = 0;
        double dMinDate = 0;
        string strMinDate = "";
        string strScript = "";

        if (DateTime.Now.Hour >= 16 && DateTime.Now.Minute >= 30)
        {
            dMinDate = Convert.ToDouble(cmDate.convertDateStringForDBTH(FormatStringApp.FormatDate(DateTime.Now.AddDays(1))).Replace("/",""));
            strMinDate = FormatStringApp.FormatDate(DateTime.Now.AddDays(1));
        }
        else
        {
            dMinDate = Convert.ToDouble(cmDate.convertDateStringForDBTH(FormatStringApp.FormatDate(DateTime.Now)).Replace("/",""));
            strMinDate = FormatStringApp.FormatDate(DateTime.Now);
        }
        dStartDate = Convert.ToDouble(cmDate.convertDateStringForDBTH(txtStartDate.Text).Replace("/",""));
        if (dStartDate < dMinDate)
        {
            strScript = "<script>";
            strScript += " document.getElementById('divLoading').style.display = 'none';";
			strScript += "showMessagePage('วันที่เริ่มคุ้มครองต้องมีค่าตั้งแต่วันที่ " + strMinDate + "');";
			strScript += " </script>";
			ScriptManager.RegisterClientScriptBlock(Literal1, typeof(Literal), "s", strScript, false);
        }
		else
		{	
			if (Session["DSRegister"] != null)
				ds = (DataSet)Session["DSRegister"];

	        
			strTmpCD = cmRegister.insertTmpCompulsary(ds);
			ds.Tables[0].Rows[0]["tm_tmp_cd"] = strTmpCD;
			Session.Add("DSRegister", ds);
			if (strTmpCD != "" )
				Response.Redirect("buyCompulsary_step2.aspx", true);

			//if (ViewState["KeyTmp"] != null)
			//    Response.Redirect("../Paid/Paid.aspx?tmp_cd=" + ViewState["KeyTmp"].ToString() + "&pol_type=C");
        }
    }

    public void initialDropdownList()
    {
        DataSet ds;
        DataView dvTmp;
        OnlineCompulsaryManager cmOnline = new OnlineCompulsaryManager();
        OnlineVoluntaryManager cmOnlineVol = new OnlineVoluntaryManager();
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        
        ds = cmOnline.getAllBrandComp();
        ddlBrand.DataSource = ds;
        ddlBrand.DataTextField = "mdc_desc";
        ddlBrand.DataValueField = "mdc_major_cd";
        ddlBrand.DataBind();
        ddlBrand.Items.Insert(0, new ListItem("-- กรุณาระบุ --", ""));

        ds = cmOnline.getCategoryCompByBrand(ddlBrand.SelectedValue);
        ddlCategory.DataSource = ds;
        ddlCategory.DataTextField = "mdc_desc";
        ddlCategory.DataValueField = "mdc_minor_cd";
        ddlCategory.DataBind();
        ddlCategory.Items.Insert(0, new ListItem("-- กรุณาระบุ --", ""));

        string strVehUse;
        if (rdoPersonal.Checked)
            strVehUse = "P";
        else if (rdoWork.Checked)
            strVehUse = "S";
        else
            strVehUse = "R";
        ds = cmOnline.getSizeCompByCategory(ddlBrand.SelectedValue, ddlCategory.SelectedValue, strVehUse);
        ddlSize.DataSource = ds;
        ddlSize.DataTextField = "mdcv_desc";
        ddlSize.DataValueField = "mdcv_veh_cd";
        ddlSize.DataBind();
        ddlSize.Items.Insert(0, new ListItem("-- กรุณาระบุ --", ""));

        ds = cmOnlineVol.getAllProvince();
        ddlProvince.DataSource = ds;
        ddlProvince.DataTextField = "ds_desc";
        ddlProvince.DataValueField = "ds_minor_cd";
        ddlProvince.DataBind();
        ddlProvince.Items.Insert(0, new ListItem("-- กรุณาระบุ --", ""));

        ddlLicenseProvince.DataSource = ds;
        ddlLicenseProvince.DataTextField = "ds_desc";
        ddlLicenseProvince.DataValueField = "ds_minor_cd";
        ddlLicenseProvince.DataBind();
        ddlLicenseProvince.Items.Insert(0, new ListItem("-- กรุณาระบุ --", ""));

        ds = cmDate.getDDLYear(DateTime.Now.Year + 543 - 20, DateTime.Now.Year + 543);
        dvTmp = ds.Tables[0].DefaultView;
        dvTmp.Sort = "value desc ";
        ddlYear.DataSource = dvTmp;
        ddlYear.DataTextField = "value";
        ddlYear.DataValueField = "value";
        ddlYear.DataBind();
        ddlYear.Items.Insert(0, new ListItem("-- กรุณาระบุ --", ""));
    }
    private void calPremium()
    {
        OnlineCompulsaryManager cmComp = new OnlineCompulsaryManager();
        RegisterManager cmRegister = new RegisterManager();
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        DataSet dsData;
        DataSet dsPremium;
        string strTmpCD;
        string strScript = "";
        dsPremium = cmComp.getNewPremium(Request.Form[ddlSize.UniqueID]);
        lblPremium.Text = FormatStringApp.Format2Digit(dsPremium.Tables[0].Rows[0]["com_prmm"].ToString());
        lblStamp.Text = FormatStringApp.Format2Digit(dsPremium.Tables[0].Rows[0]["stamp_prmm"].ToString());
        lblVat.Text = FormatStringApp.Format2Digit(dsPremium.Tables[0].Rows[0]["tax_prmm"].ToString());
        lblGrossPremium.Text = FormatStringApp.Format2Digit(dsPremium.Tables[0].Rows[0]["total_prmm"].ToString());
        dsData = UIToDoc();        
        
        litScript.Text += "<script>";
        litScript.Text += " document.getElementById('divPremium').style.display = 'inline';";
        litScript.Text += "</script>";
        strTmpCD = cmRegister.insertTmpCompulsary(dsData);
        txtTmpCD.Text = strTmpCD;
        dsData.Tables[0].Rows[0]["tm_tmp_cd"] = strTmpCD;
        if (strTmpCD == "")
        {
            //dsErrLabel = (DataSet)Session["dsLabelError"];
            //UcError1.showMessage(cmLabel.getLabel(dsErrLabel, "err000002", Session["Language"].ToString()));
        }
        ViewState.Add("KeyTmp", strTmpCD);
        Session.Add("DSRegister", dsData);

        strScript = "<script>";
        strScript += " document.getElementById('divPremium').style.display = 'block';";
        strScript += " document.getElementById('divLoading').style.display = 'none';";
        strScript += " </script>";
        ScriptManager.RegisterClientScriptBlock(Literal1, typeof(Literal), "s", strScript, false);
    }
    private DataSet UIToDoc()
    {
        DataSet dsData = new DataSet();
        // insert tmpreg, tmpvolun, tmpmtveh, tmpcompul
        string[] ColumnName = { "tm_tmp_cd",
                                    //register
                                    "rg_regis_no", "rg_member_cd", "rg_member_ref", "rg_main_class", "rg_pol_type", 
                                    "rg_effect_dt", "rg_expiry_dt", "rg_regis_dt", "rg_regis_time", "rg_regis_type", 
                                    "rg_ins_fname", "rg_ins_lname", "rg_ins_addr1", "rg_ins_addr2", "rg_ins_amphor",
                                    "rg_ins_changwat", "rg_ins_postcode", "rg_ins_tel", "rg_ins_email", "rg_sum_ins", 
                                    "rg_prmm", "rg_tax", "rg_stamp", "rg_pmt_cd", "rg_fleet_perc", 
                                    "rg_pay_type", "rg_payment_stat", "rg_ref_bank", "rg_approve", "rg_prmmgross",
                                    // register - new column
                                    "rg_ins_mobile", "rg_ins_idcard", "oth_distance", "oth_region", "oth_flagdeduct", 
                                    "oth_oldpolicy", "insc_id", "mott_id",
                                    //mtveh
                                    "mv_regis_no", "mv_major_cd", "mv_minor_cd", "mv_veh_year", "mv_veh_cc", 
                                    "mv_veh_seat", "mv_veh_weight", "mv_license_no", "mv_license_area", "mv_engin_no", 
                                    "mv_chas_no", "mv_combine", "mv_no_claim", "mv_no_claim_perc",
                                    // compulsary
                                    "cp_regis_no", "cp_veh_cd", "cp_comp_prmm", "cp_comp_tax", "cp_comp_stamp",
                                    "cp_comp_grs",
                                    // senddoc
                                    "sed_regis_no", "sed_branch", "sed_name", "sed_ins_addr", "sed_amphor",
                                    "sed_changwat", "sed_postcode", "sed_tel", "sed_at"
                                    };
        string[] ColumnType = { "string", 
                                  //
                                    "string", "string", "string", "string", "string", 
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    //
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string",                                    
                                    //
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string",
                                    //
                                    "string", "string", "string", "string", "string",
                                    "string",
                                    //
                                    "string", "string", "string", "string", "string",
                                    "string", "string", "string", "string"};
        DataTable dt = new DataTable();
        dt = TDS.Utility.MasterUtil.AddColumnToDataTable(dt, ColumnType, ColumnName);
        dsData.Tables.Add(dt);
        dsData.Tables[0].Rows.Add(dsData.Tables[0].NewRow());

        //dsData.Tables[0].Rows[0]["brand"] = ddlBrand.SelectedValue;
        //dsData.Tables[0].Rows[0]["category"] = dsCar.Tables[0].Rows[0]["category"];
        //dsData.Tables[0].Rows[0]["use"] = dsCar.Tables[0].Rows[0]["use"];
        //dsData.Tables[0].Rows[0]["size"] = dsCar.Tables[0].Rows[0]["size"];
        //dsData.Tables[0].Rows[0]["license_province"] = dsCar.Tables[0].Rows[0]["license_province"];
        //dsData.Tables[0].Rows[0]["regis_no"] = dsCar.Tables[0].Rows[0]["regis_no"];
        //dsData.Tables[0].Rows[0]["year"] = dsCar.Tables[0].Rows[0]["year"];
        //dsData.Tables[0].Rows[0]["engin_no"] = dsCar.Tables[0].Rows[0]["engin_no"];
        //dsData.Tables[0].Rows[0]["chas_no"] = dsCar.Tables[0].Rows[0]["chas_no"];
        //dsData.Tables[0].Rows[0]["cover_date"] = dsCar.Tables[0].Rows[0]["cover_date"].ToString();
        //dsData.Tables[0].Rows[0]["FName"] = dsCust.Tables[0].Rows[0]["FName"];
        //dsData.Tables[0].Rows[0]["LName"] = dsCust.Tables[0].Rows[0]["LName"];
        //dsData.Tables[0].Rows[0]["Address1"] = dsCust.Tables[0].Rows[0]["Address1"];
        //dsData.Tables[0].Rows[0]["Address2"] = dsCust.Tables[0].Rows[0]["Address2"];
        //dsData.Tables[0].Rows[0]["District"] = dsCust.Tables[0].Rows[0]["District"];
        //dsData.Tables[0].Rows[0]["Province"] = dsCust.Tables[0].Rows[0]["Province"];
        //dsData.Tables[0].Rows[0]["PostCode"] = dsCust.Tables[0].Rows[0]["PostCode"];
        //dsData.Tables[0].Rows[0]["TelNo"] = dsCust.Tables[0].Rows[0]["TelNo"];
        //dsData.Tables[0].Rows[0]["EMail"] = dsCust.Tables[0].Rows[0]["EMail"];

        // register
        dsData.Tables[0].Rows[0]["rg_main_class"] = "M";
        dsData.Tables[0].Rows[0]["rg_pol_type"] = "C";
        dsData.Tables[0].Rows[0]["rg_regis_dt"] = FormatStringApp.FormatDate(DateTime.Now);
        dsData.Tables[0].Rows[0]["rg_regis_time"] = FormatStringApp.FormatTime(DateTime.Now); 
        dsData.Tables[0].Rows[0]["rg_effect_dt"] = txtStartDate.Text;
        DateTime dte = new DateTime(Convert.ToInt32(txtStartDate.Text.Substring(6, 4)) - 543, Convert.ToInt32(txtStartDate.Text.Substring(3, 2)), Convert.ToInt32(txtStartDate.Text.Substring(0, 2)));
        dte = dte.AddYears(1);
        dsData.Tables[0].Rows[0]["rg_expiry_dt"] = FormatStringApp.FormatDate(dte);
        dsData.Tables[0].Rows[0]["rg_ins_fname"] = txtFName.Text;
        dsData.Tables[0].Rows[0]["rg_ins_lname"] = txtLName.Text;
        dsData.Tables[0].Rows[0]["rg_ins_addr1"] = txtAddress.Text;
        dsData.Tables[0].Rows[0]["rg_ins_addr2"] = txtSubDistrict.Text;
        dsData.Tables[0].Rows[0]["rg_ins_amphor"] = txtDistrict.Text;
        dsData.Tables[0].Rows[0]["rg_ins_changwat"] = ddlProvince.SelectedValue;
        dsData.Tables[0].Rows[0]["rg_ins_postcode"] = txtPostCode.Text;
        dsData.Tables[0].Rows[0]["rg_ins_tel"] = txtTelephone.Text;        
        dsData.Tables[0].Rows[0]["rg_ins_mobile"] = txtMobile.Text;
        dsData.Tables[0].Rows[0]["rg_ins_email"] = txtEmail.Text;
        dsData.Tables[0].Rows[0]["rg_sum_ins"] = "0.00";
        dsData.Tables[0].Rows[0]["rg_prmm"] = lblPremium.Text.Replace(",", "");
        dsData.Tables[0].Rows[0]["rg_tax"] = lblVat.Text.Replace(",", "");
        dsData.Tables[0].Rows[0]["rg_stamp"] = lblStamp.Text.Replace(",", "");
        dsData.Tables[0].Rows[0]["rg_prmmgross"] = lblGrossPremium.Text.Replace(",", "");
        // mtveh
        dsData.Tables[0].Rows[0]["mv_major_cd"] = ddlBrand.SelectedValue;
        dsData.Tables[0].Rows[0]["mv_minor_cd"] = Request.Form[ddlCategory.UniqueID];
        dsData.Tables[0].Rows[0]["mv_veh_year"] = Convert.ToString(Convert.ToInt32(ddlYear.SelectedValue) - 543);
        dsData.Tables[0].Rows[0]["mv_veh_cc"] = "0";
        dsData.Tables[0].Rows[0]["mv_veh_seat"] = "0";
        dsData.Tables[0].Rows[0]["mv_veh_weight"] = "0";
        dsData.Tables[0].Rows[0]["mv_license_no"] = txtRegisNo.Text;
        dsData.Tables[0].Rows[0]["mv_license_area"] = ddlLicenseProvince.SelectedValue;
        dsData.Tables[0].Rows[0]["mv_engin_no"] = txtEngineNo.Text;
        dsData.Tables[0].Rows[0]["mv_chas_no"] = txtChasNo.Text;
        dsData.Tables[0].Rows[0]["mv_combine"] = "";
        dsData.Tables[0].Rows[0]["mv_no_claim"] = "";
        dsData.Tables[0].Rows[0]["mv_no_claim_perc"] = "0";
        // compulsary
        dsData.Tables[0].Rows[0]["cp_veh_cd"] = Request.Form[ddlSize.UniqueID];
        dsData.Tables[0].Rows[0]["cp_comp_prmm"] = lblPremium.Text.Replace(",","");
        dsData.Tables[0].Rows[0]["cp_comp_tax"] = lblVat.Text.Replace(",","");
        dsData.Tables[0].Rows[0]["cp_comp_stamp"] = lblStamp.Text.Replace(",","");
        dsData.Tables[0].Rows[0]["cp_comp_grs"] = lblGrossPremium.Text.Replace(",","");
        // new column
        dsData.Tables[0].Rows[0]["oth_distance"] = "";
        dsData.Tables[0].Rows[0]["oth_region"] = "";
        dsData.Tables[0].Rows[0]["oth_flagdeduct"] = "";
        dsData.Tables[0].Rows[0]["oth_oldpolicy"] = "";
        dsData.Tables[0].Rows[0]["insc_id"] = "";
        dsData.Tables[0].Rows[0]["mott_id"] = "";
        
        return dsData;
    }

    [WebMethod]
    public static ArrayList GetCarCategoryData(string strCarName)
    {
        DataSet ds = new DataSet();
        ArrayList list = new ArrayList();
        OnlineCompulsaryManager cmOnline = new OnlineCompulsaryManager();
        ds = cmOnline.getCategoryCompByBrand(strCarName);
        ds.Tables[0].Rows.InsertAt(ds.Tables[0].NewRow(), 0);
        ds.Tables[0].Rows[0]["mdc_minor_cd"] = "";
        ds.Tables[0].Rows[0]["mdc_desc"] = "-- กรุณาระบุ --";
        for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
        {
            list.Add(new ListItem(
           ds.Tables[0].Rows[i]["mdc_desc"].ToString(),
           ds.Tables[0].Rows[i]["mdc_minor_cd"].ToString()
            ));
        }
        return list;
    }
    [WebMethod]
    public static ArrayList GetCarSizeData(string strCarName, string strCarCategory, string strVehUse)
    {
        DataSet ds = new DataSet();
        ArrayList list = new ArrayList();
        OnlineCompulsaryManager cmOnline = new OnlineCompulsaryManager();        
        ds = cmOnline.getSizeCompByCategory(strCarName, strCarCategory, strVehUse);
        ds.Tables[0].Rows.InsertAt(ds.Tables[0].NewRow(), 0);
        ds.Tables[0].Rows[0]["mdcv_veh_cd"] = "";
        ds.Tables[0].Rows[0]["mdcv_desc"] = "-- กรุณาระบุ --";
        for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
        {
            list.Add(new ListItem(
           ds.Tables[0].Rows[i]["mdcv_desc"].ToString(),
           ds.Tables[0].Rows[i]["mdcv_veh_cd"].ToString()
            ));
        }
        return list;
    }
}
