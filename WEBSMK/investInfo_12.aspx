﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFront2_th.master" AutoEventWireup="true" CodeFile="investInfo_12.aspx.cs" Inherits="investInfo_12" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
 <div class="subject1">แบบแสดงรายการข้อมูลประจำปี (แบบ 56-1)</div>
    
     <table class="tb-quarter"  width="80%">
        <tr>
            <th class="no-borderleft"  >ปี</th>
            <th colspan="2">
             แบบแสดงรายการข้อมูลประจำปี </th>
        </tr>
        <tr>
            <td width="218" class="no-borderleft">
                2555
            </td>
            <td width="64"  class="no-border-leftright" ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"  /> 
            </td>
            <td width="341" class="no-border-leftright   align-left" >   <a href="downloads/investment/f56-1-2555-25.pdf">สิ้นสุดวันที่ 31 ธันวาคม 2555</a></td>
        </tr>
        <tr>
            <td class="no-borderleft">
                2554
            </td>
            <td class="no-border-leftright" ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"  /> 
              </td>
            <td class="no-border-leftright   align-left" > 
            <a href="downloads/investment/f56-1-2554-24.pdf">สิ้นสุดวันที่ 31 ธันวาคม 2554</a>
            </td>
        </tr>
        <tr>
            <td class="no-borderleft">
                2553
            </td>
            <td class="no-border-leftright" ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"  /> 
              </td>
            <td class="no-border-leftright  align-left" >  
            <a href="downloads/investment/f56-1-2553-23.pdf">สิ้นสุดวันที่ 31 ธันวาคม 2553</a>
            </td>
        </tr>
        <tr>
          <td class="no-borderleft"> 2552</td>
          <td class="no-border-leftright" ><img src="<%=Config.SiteUrl%>images/icon_Acrobat_Document2.png"  /></td>
          <td class="no-border-leftright   align-left" >  
            <a href="downloads/investment/f56-1-2552-21.pdf">สิ้นสุดวันที่ 31 ธันวาคม 2552(1/2)</a><br /> 
                        <a href="downloads/investment/f56-1-2552-22.pdf">สิ้นสุดวันที่ 31 ธันวาคม 2552(2/2)</a>
          </td>
        </tr>
     </table> 
   

   <div class="notation2">
เอกสารเป็นไฟล์ PDF การเปิดดูจำเป็นต้องมีโปรแกรม  Adobe Reader คลิ๊กที่ไอคอนโปรแกรมเพื่อติดตั้ง   
<a href="http://www.adobe.com/products/acrobat/readstep2.html"
target="_blank"><img src="<%=Config.SiteUrl%>images/icon-pdf-download.gif"  />
</a>
</div>
  
   
 
</asp:Content>

