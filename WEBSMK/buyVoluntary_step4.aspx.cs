﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using TDSLib.Libs.net.mail;
using System.Net.Mail;

public partial class buyVoluntary_step4 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        RegisterManager cmRegister = new RegisterManager();
        //if (Session["DSRegister"] != null)
        //    ds = (DataSet)Session["DSRegister"];
        //else 
        if (Request.QueryString["regis_no"] != null)
            ds = cmRegister.getDataVoluntary(Request.QueryString["regis_no"]);
        else
            Response.Redirect("buyVoluntary_step1.aspx", true);

        if (!IsPostBack)
        {
            docToUI(ds);
        }
    }
    protected void btnPrint_Click(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        string strPrintURL = "";
        DataSet dsTmp = new DataSet();
        OnlineVoluntaryManager cmOnline = new OnlineVoluntaryManager();
        RegisterManager cmRegister = new RegisterManager();

        ds = cmRegister.getDataVoluntary(Request.QueryString["regis_no"]);
        //if (Session["DSRegister"] != null)
        if (ds.Tables[0].Rows.Count > 0)            
        {
            //ds = (DataSet)Session["DSRegister"];
            ds.Tables[0].Columns.Add("carname_desc");
            ds.Tables[0].Columns.Add("carmak_desc");
            ds.Tables[0].Columns.Add("distance_desc");
            ds.Tables[0].Columns.Add("insurecomp_desc");
            ds.Tables[0].Columns.Add("motortype_desc");
            ds.Tables[0].Columns.Add("licenscearea_desc");

            dsTmp = cmOnline.getDataProvinceByCode(ds.Tables[0].Rows[0]["rg_ins_changwat"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                ds.Tables[0].Rows[0]["rg_ins_changwat"] = dsTmp.Tables[0].Rows[0]["ds_desc"].ToString();

            dsTmp = cmOnline.getDataCarNameByCode(ds.Tables[0].Rows[0]["mv_major_cd"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                ds.Tables[0].Rows[0]["carname_desc"] = dsTmp.Tables[0].Rows[0]["desc_t"].ToString();

            dsTmp = cmOnline.getDataCarMarkByCode(ds.Tables[0].Rows[0]["mv_minor_cd"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                ds.Tables[0].Rows[0]["carmak_desc"] = dsTmp.Tables[0].Rows[0]["desc_t"].ToString();

            dsTmp = cmOnline.getDataDriveDistanceByID(ds.Tables[0].Rows[0]["oth_distance"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                ds.Tables[0].Rows[0]["distance_desc"] = dsTmp.Tables[0].Rows[0]["ddis_name"].ToString();
            else
                ds.Tables[0].Rows[0]["distance_desc"] = "-";

            dsTmp = cmOnline.getDataInsuranceCompanyByID(ds.Tables[0].Rows[0]["insc_id"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                ds.Tables[0].Rows[0]["insurecomp_desc"] = dsTmp.Tables[0].Rows[0]["insc_name"].ToString();

            dsTmp = cmOnline.getDataMotorTypeByID(ds.Tables[0].Rows[0]["mott_id"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                ds.Tables[0].Rows[0]["motortype_desc"] = dsTmp.Tables[0].Rows[0]["mott_name"].ToString();

            dsTmp = cmOnline.getDataProvinceByCode(ds.Tables[0].Rows[0]["mv_license_area"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                ds.Tables[0].Rows[0]["licenscearea_desc"] = dsTmp.Tables[0].Rows[0]["ds_desc"].ToString();                
                
			dsTmp = cmOnline.getDataProvinceByCode(ds.Tables[0].Rows[0]["sed_changwat"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                ds.Tables[0].Rows[0]["sed_changwat"] = dsTmp.Tables[0].Rows[0]["ds_desc"].ToString();
        }
        Session["DsReport"] = ds;

        strPrintURL = "CommonReportForm.aspx?rptName=VoluntaryDocument.rpt&sid=DsReport";
        litScript.Text += "<script>window.open('" + strPrintURL + "','Document');</script>";
    }

    public void docToUI(DataSet ds)
    {
        string strMotGar = "";
        DataSet dsPrem = new DataSet();
        DataSet dsTmp = new DataSet();
        OnlineVoluntaryManager cmOnline = new OnlineVoluntaryManager();

        DataSet dsRegister = new DataSet();
        if (Session["DSRegister"] != null)
            dsRegister = (DataSet)Session["DSRegister"];

        sendMail(ds);
        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            lblRefNo.Text = ds.Tables[0].Rows[0]["rg_regis_no"].ToString();
            lblFName.Text = ds.Tables[0].Rows[0]["rg_ins_fname"].ToString();
            lblLName.Text = ds.Tables[0].Rows[0]["rg_ins_lname"].ToString();
            lblAddress.Text = ds.Tables[0].Rows[0]["rg_ins_addr1"].ToString();
            lblSubDistrict.Text = ds.Tables[0].Rows[0]["rg_ins_addr2"].ToString();
            lblDistrict.Text = ds.Tables[0].Rows[0]["rg_ins_amphor"].ToString();
            dsTmp = cmOnline.getDataProvinceByCode(ds.Tables[0].Rows[0]["rg_ins_changwat"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                lblProvince.Text = dsTmp.Tables[0].Rows[0]["ds_desc"].ToString();            
            lblPostCode.Text = ds.Tables[0].Rows[0]["rg_ins_postcode"].ToString();
            lblIDCard.Text = ds.Tables[0].Rows[0]["rg_ins_idcard"].ToString();
            lblMobile.Text = ds.Tables[0].Rows[0]["rg_ins_mobile"].ToString();
            lblTelephone.Text = ds.Tables[0].Rows[0]["rg_ins_tel"].ToString();
            lblEmail.Text = ds.Tables[0].Rows[0]["rg_ins_email"].ToString();
            if (ds.Tables[0].Rows[0]["vl_drv1"].ToString() == "")
            {
                lblDriver1Name.Text = "-";
                lblDriver1BrithDate.Text = "-";
            }
            else
            {
                lblDriver1Name.Text = ds.Tables[0].Rows[0]["vl_drv1"].ToString();
                lblDriver1BrithDate.Text = ds.Tables[0].Rows[0]["vl_birth_drv1"].ToString();
            }
            if (ds.Tables[0].Rows[0]["vl_drv2"].ToString() == "")
            {
                lblDriver2Name.Text = "-";
                lblDriver2BrithDate.Text = "-";
            }
            else
            {
                lblDriver2Name.Text = ds.Tables[0].Rows[0]["vl_drv2"].ToString();
                lblDriver2BrithDate.Text = ds.Tables[0].Rows[0]["vl_birth_drv2"].ToString();
            }

            dsTmp = cmOnline.getDataCarNameByCode(ds.Tables[0].Rows[0]["mv_major_cd"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                lblCarName.Text = dsTmp.Tables[0].Rows[0]["desc_t"].ToString();

            dsTmp = cmOnline.getDataCarMarkByCode(ds.Tables[0].Rows[0]["mv_minor_cd"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                lblCarMark.Text = dsTmp.Tables[0].Rows[0]["desc_t"].ToString();
            lblCarRegisYear.Text = ds.Tables[0].Rows[0]["mv_veh_year"].ToString();
            if (ds.Tables[0].Rows[0]["vl_veh_cd"].ToString().Trim() == "110")
            {
                lblCarType.Text = "รถยนต์นั่ง";
                lblCarUseDesc.Text = "ส่วนบุคคล";
            }
            else if (ds.Tables[0].Rows[0]["vl_veh_cd"].ToString().Trim() == "210")
            {
                lblCarType.Text = "รถยนต์โดยสาร";
                lblCarUseDesc.Text = "ส่วนบุคคล";
            }
            else if (ds.Tables[0].Rows[0]["vl_veh_cd"].ToString().Trim() == "320")
            {
                lblCarType.Text = "รถยนต์บรรทุก";
                lblCarUseDesc.Text = "เพื่อการพาณิชย์";
            }
            dsTmp = cmOnline.getDataDriveDistanceByID(ds.Tables[0].Rows[0]["oth_distance"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                lblDistance.Text = dsTmp.Tables[0].Rows[0]["ddis_name"].ToString();
            else
                lblDistance.Text = "-";
            lblDriveRegion.Text = (ds.Tables[0].Rows[0]["oth_region"].ToString() == "1" ? "กรุงเทพฯ" : "ต่างจังหวัด");
            if (ds.Tables[0].Rows[0]["oth_oldpolicy"].ToString() == "Y")
                lblFlagOldPolicy.Text = "มี";
            else
                lblFlagOldPolicy.Text = "ไม่มี";
            if (ds.Tables[0].Rows[0]["insc_id"].ToString() != "")
            {
                dsTmp = cmOnline.getDataInsuranceCompanyByID(ds.Tables[0].Rows[0]["insc_id"].ToString());
                if (dsTmp.Tables[0].Rows.Count > 0)
                    lblOldInsuraceComp.Text = dsTmp.Tables[0].Rows[0]["insc_name"].ToString();
            }
            else
                lblOldInsuraceComp.Text = "-";
            if (ds.Tables[0].Rows[0]["mott_id"].ToString() != "")
            {
                dsTmp = cmOnline.getDataMotorTypeByID(ds.Tables[0].Rows[0]["mott_id"].ToString());
                if (dsTmp.Tables[0].Rows.Count > 0)
                    lblOldPolType.Text = dsTmp.Tables[0].Rows[0]["mott_name"].ToString();
            }
            else
                lblOldPolType.Text = "-";
            lblCarLicense.Text = ds.Tables[0].Rows[0]["mv_license_no"].ToString();
            dsTmp = cmOnline.getDataProvinceByCode(ds.Tables[0].Rows[0]["mv_license_area"].ToString());
            if (dsTmp.Tables[0].Rows.Count > 0)
                lblLicenseProvince.Text = dsTmp.Tables[0].Rows[0]["ds_desc"].ToString();

            if (ds.Tables[0].Rows[0]["vl_veh_cd"].ToString().Trim() == "110")
            {
                lblCarSize.Text = ds.Tables[0].Rows[0]["mv_veh_cc"].ToString();
            }
            else if (ds.Tables[0].Rows[0]["vl_veh_cd"].ToString().Trim() == "210")
            {
                lblCarSize.Text = ds.Tables[0].Rows[0]["mv_veh_seat"].ToString();
            }
            else if (ds.Tables[0].Rows[0]["vl_veh_cd"].ToString().Trim() == "320")
            {
                lblCarSize.Text = ds.Tables[0].Rows[0]["mv_veh_weight"].ToString();
            }            
            txtChasis.Text = ds.Tables[0].Rows[0]["mv_chas_no"].ToString();
            txtEngineNo.Text = ds.Tables[0].Rows[0]["mv_engin_no"].ToString();
            lblMotGar.Text = (ds.Tables[0].Rows[0]["vl_repair"].ToString() == "Y" ? "ซ่อมศูนย์" : "ซ่อมอู่");

            if (ds.Tables[0].Rows[0]["vl_tpbi_person"].ToString() == "" ||
                 Convert.ToDouble("0" + ds.Tables[0].Rows[0]["vl_tpbi_person"].ToString()) == 0)
                lbltpbi1.Text = " ไม่คุ้มครอง ";
            else
                lbltpbi1.Text = FormatStringApp.FormatInt(ds.Tables[0].Rows[0]["vl_tpbi_person"].ToString());
            if (ds.Tables[0].Rows[0]["vl_tpbi_time"].ToString() == "" ||
                 Convert.ToDouble("0" + ds.Tables[0].Rows[0]["vl_tpbi_time"].ToString()) == 0)
                lbltpbi2.Text = " ไม่คุ้มครอง ";
            else
                lbltpbi2.Text = FormatStringApp.FormatInt(ds.Tables[0].Rows[0]["vl_tpbi_time"].ToString());
            if (ds.Tables[0].Rows[0]["vl_tppd_time"].ToString() == "" ||
                 Convert.ToDouble("0" + ds.Tables[0].Rows[0]["vl_tppd_time"].ToString()) == 0)
                lbltppd.Text = " ไม่คุ้มครอง ";
            else
                lbltppd.Text = FormatStringApp.FormatInt(ds.Tables[0].Rows[0]["vl_tppd_time"].ToString());
            if (ds.Tables[0].Rows[0]["vl_tppd_exc"].ToString() == "" ||
                   Convert.ToDouble("0" + ds.Tables[0].Rows[0]["vl_tppd_exc"].ToString()) == 0)
                lblDeduct.Text = " - ";
            else
                lblDeduct.Text = FormatStringApp.FormatInt(ds.Tables[0].Rows[0]["vl_tppd_exc"].ToString());
            if (ds.Tables[0].Rows[0]["rg_sum_ins"].ToString() == "" ||
                   Convert.ToDouble("0" + ds.Tables[0].Rows[0]["rg_sum_ins"].ToString()) == 0)
                lblCarOD.Text = " - ";
            else
                lblCarOD.Text = FormatStringApp.FormatInt(ds.Tables[0].Rows[0]["rg_sum_ins"].ToString());
            if (ds.Tables[0].Rows[0]["vl_01_11"].ToString() == "" ||
                 Convert.ToDouble("0" + ds.Tables[0].Rows[0]["vl_01_11"].ToString()) == 0)
                lblperm_d_01.Text = " ไม่คุ้มครอง ";
            else
                lblperm_d_01.Text = FormatStringApp.FormatInt(ds.Tables[0].Rows[0]["vl_01_11"].ToString());
            if (ds.Tables[0].Rows[0]["vl_01_121"].ToString() == "" ||
                   Convert.ToDouble("0" + ds.Tables[0].Rows[0]["vl_01_121"].ToString()) == 0)
                lblperm_p_num.Text = " - ";
            else
                lblperm_p_num.Text = FormatStringApp.FormatInt(ds.Tables[0].Rows[0]["vl_01_121"].ToString());
            if (ds.Tables[0].Rows[0]["vl_01_122"].ToString() == "" ||
                 Convert.ToDouble("0" + ds.Tables[0].Rows[0]["vl_01_122"].ToString()) == 0)
                lblperm_p_01.Text = " ไม่คุ้มครอง ";
            else
                lblperm_p_01.Text = FormatStringApp.FormatInt(ds.Tables[0].Rows[0]["vl_01_122"].ToString());
            if (ds.Tables[0].Rows[0]["vl_01_21"].ToString() == "" ||
                 Convert.ToDouble("0" + ds.Tables[0].Rows[0]["vl_01_21"].ToString()) == 0)
                lbltemp_d_01.Text = " ไม่คุ้มครอง ";
            else
                lbltemp_d_01.Text = FormatStringApp.FormatInt(ds.Tables[0].Rows[0]["vl_01_21"].ToString());
            if (ds.Tables[0].Rows[0]["vl_02_person"].ToString() == "" ||
                   Convert.ToDouble("0" + ds.Tables[0].Rows[0]["vl_02_person"].ToString()) == 0)
                lbltemp_p_num.Text = " - ";
            else
                lbltemp_p_num.Text = FormatStringApp.FormatInt(ds.Tables[0].Rows[0]["vl_02_person"].ToString());
            if (ds.Tables[0].Rows[0]["vl_01_212"].ToString() == "" ||
                 Convert.ToDouble("0" + ds.Tables[0].Rows[0]["vl_01_212"].ToString()) == 0)
                lbltemp_p_01.Text = " ไม่คุ้มครอง ";
            else
                lbltemp_p_01.Text = FormatStringApp.FormatInt(ds.Tables[0].Rows[0]["vl_01_212"].ToString());
            if (ds.Tables[0].Rows[0]["vl_02"].ToString() == "" ||
                 Convert.ToDouble("0" + ds.Tables[0].Rows[0]["vl_02"].ToString()) == 0)
                lblcover_02.Text = " ไม่คุ้มครอง ";
            else
                lblcover_02.Text = FormatStringApp.FormatInt(ds.Tables[0].Rows[0]["vl_02"].ToString());
            if (ds.Tables[0].Rows[0]["vl_03"].ToString() == "" ||
                 Convert.ToDouble("0" + ds.Tables[0].Rows[0]["vl_03"].ToString()) == 0)
                lblcover_03.Text = " ไม่คุ้มครอง ";
            else
                lblcover_03.Text = FormatStringApp.FormatInt(ds.Tables[0].Rows[0]["vl_03"].ToString());

			lblStartDate.Text = FormatStringApp.FormatDate(ds.Tables[0].Rows[0]["rg_effect_dt"]);
            double dPrem = 0;
            double dStamp = 0;
            double dTax = 0;
            double dGross = 0;
            double dPaid = 0;
            double dTotal = 0;
            dPrem = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["rg_prmm"]);
            dStamp = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["rg_stamp"]);
            dTax = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["rg_tax"]);
            dGross = dPrem + dStamp + dTax;
            dPaid = dGross;
            dTotal = dGross;
            lblPremium.Text = FormatStringApp.FormatNDigit(ds.Tables[0].Rows[0]["rg_prmm"], 2) + " บาท";
            lblStamp.Text = FormatStringApp.FormatNDigit(ds.Tables[0].Rows[0]["rg_stamp"], 2) + " บาท";
            lblVat.Text = FormatStringApp.FormatNDigit(ds.Tables[0].Rows[0]["rg_tax"], 2) + " บาท";
            lblGrossPremium.Text = FormatStringApp.FormatNDigit(dGross, 2) + " บาท";
            dPrem = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["cp_comp_prmm"]);
            dStamp = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["cp_comp_stamp"]);
            dTax = Convert.ToDouble("0" + ds.Tables[0].Rows[0]["cp_comp_tax"]);
            dGross = dPrem + dStamp + dTax;
            dPaid = dPaid + dGross;
            dTotal = dTotal + dGross;
            lblComPremium.Text = FormatStringApp.FormatNDigit(dGross, 2) + " บาท";
            //lblPaid.Text = FormatStringApp.FormatNDigit(dPaid, 2) + " บาท";            
            lblTotal.Text = FormatStringApp.FormatNDigit(dTotal, 2) + " บาท";

            if (ds.Tables[0].Rows[0]["rg_pay_type"].ToString() == "1")
            {
                lblPaidMethod.Text = "ชำระเงินด้วยเงินสดหรือเช็คที่ บริษัท สินมั่นคงประกันภัยจำกัด (มหาชน) ที่</br>";
                if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "100")
                    lblPaidMethod.Text += "สำนักงานใหญ่ (ถนนศรีนครินทร์)";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "100")
                    lblPaidMethod.Text += "สำนักงานใหญ่ (ถนนศรีนครินทร์)";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "101")
                    lblPaidMethod.Text += "สาขาสวนมะลิ (ยสเส)";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "102")
                    lblPaidMethod.Text += "สาขาดอนเมือง";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "102")
                    lblPaidMethod.Text += "สาขาบางแค";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "130")
                    lblPaidMethod.Text += "สาขารัตนาธิเบศร์";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "500")
                    lblPaidMethod.Text += "สาขาชลบุรี";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "201")
                    lblPaidMethod.Text += "สาขาพิษณุโลก";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "200")
                    lblPaidMethod.Text += "สาขาเชียงใหม่";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "601")
                    lblPaidMethod.Text += "สาขาหาดใหญ่";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "600")
                    lblPaidMethod.Text += "สาขาสุราษฎร์ธานี";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "300")
                    lblPaidMethod.Text += "สาขานครปฐม";
                else if (ds.Tables[0].Rows[0]["sed_branch"].ToString() == "401")
                    lblPaidMethod.Text += "สาขาขอนแก่น";
            }
            else if (ds.Tables[0].Rows[0]["rg_pay_type"].ToString() == "2")
            {
                lblPaidMethod.Text = "ชำระเงิน ด้วยวิธีการโอนเข้าบัญชีของ บริษัท สินมั่นคงประกันภัย จำกัด (มหาชน)";
            }
            else if (ds.Tables[0].Rows[0]["rg_pay_type"].ToString() == "3")
            {
                lblPaidMethod.Text = "ชำระเงิน ด้วยวิธีการโอนเข้าบัญชีของ บริษัท สินมั่นคงประกันภัย จำกัด(มหาชน) ผ่าน www.scbeasy.com";
            }
            else if (ds.Tables[0].Rows[0]["rg_pay_type"].ToString() == "4")
            {
                lblPaidMethod.Text = "บริการจัดส่งและจัดเก็บเบี้ยประกันถึงบ้าน (เฉพาะภายในเขตกรุงเทพฯ)";
            }
            else if (ds.Tables[0].Rows[0]["rg_pay_type"].ToString() == "5")
            {
                lblPaidMethod.Text = "ชำระเงิน ด้วยบัตรเครดิต";
            }
            if (ds.Tables[0].Rows[0]["rg_ref_bank"].ToString() == "1")
                lblSendDoc.Text = "ที่อยู่ตามกรมธรรม์";
            else
            {
            }


            lblPaid2.Text = FormatStringApp.FormatNDigit(FormatStringApp.FormatNDigit(ds.Tables[0].Rows[0]["rg_prmmgross"], 2), 2) + " บาท";
            txtCampaign.Text = ds.Tables[0].Rows[0]["campaign"].ToString();
            if (txtCampaign.Text == "013")
            {
                litScript.Text = "<script>";
                litScript.Text += "document.getElementById('divPremium').style.display = 'none';";
                litScript.Text += "document.getElementById('div013Premium').style.display = 'inline';";
                litScript.Text += "</script>";
            }
        }
    }    
    private void sendMail(DataSet dsContact)
    {
        string strSMTPDomainName = "";
        TDSMail mail;
        string strBody = "";
        string strType = "";
        MailAddressCollection mAddrs = new MailAddressCollection();
        MailManager cmMail = new MailManager();
        cmMail.setXmlFilePath(System.Configuration.ConfigurationManager.AppSettings["appPath"] + "/Adminweb/xml/ContactUs.xml");
        // ส่งเมล์ไปให้บุคคลที่มีหน้าทีรับผิดชอบ
        strSMTPDomainName = cmMail.getDataByKey("SMTPMail");
        mail = new TDSMail(strSMTPDomainName,25);
        mail.TO = new MailAddressCollection();
        mail.Subject = "ลูกค้าจาก Internet สั่งซื้อประกันภัยออนไลน์ตามเลขอ้างอิง " + dsContact.Tables[0].Rows[0]["rg_regis_no"].ToString();
        mail.IsBodyHtml = true;
        strBody = " Dear เจ้าหน้าที่รับแจ้งประกัน ," + "<br>";
        strBody += " ลูกค้าได้ทำการบันทึกข้อมูลสั่งซื้อประกันภัยออนไลน์ตามเลขอ้างอิง " + dsContact.Tables[0].Rows[0]["rg_regis_no"].ToString() + "<br>";
        mAddrs.Add(new MailAddress(cmMail.getDataByKey("EmailBuyOnline")));        
        strBody += " Best Regards," + "<br>";
        strBody += " &nbsp;&nbsp;&nbsp;บริษัท สินมั่นคงประกันภัย จำกัด(มหาชน)." + "<br>";
        mail.Body = strBody;
        mail.TO = mAddrs;
        for (int i = 0; i <= mAddrs.Count - 1; i++)
        {
            
        }
        try
		{
            string strFrom = cmMail.getDataByKey("EmailAdmin");
            string strPassword = cmMail.getDataByKey("EmailAdminPassword");
            mail.Send(new MailAddress(strFrom), strPassword, false);
		}
		catch (Exception ex)
		{
			TDS.Utility.MasterUtil.writeError("buyVoluntary_step4.sendMail() ", ex.ToString());
		}
    }
}
