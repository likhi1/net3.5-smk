USE [WEBSMKDB]
GO
/****** Object:  Table [dbo].[short_rate]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[short_rate](
	[sr_day] [int] NOT NULL,
	[sr_rate] [numeric](5, 2) NOT NULL,
	[sr_effect_dt] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[setcode]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[setcode](
	[ds_major_cd] [char](2) NOT NULL,
	[ds_minor_cd] [char](8) NOT NULL,
	[ds_desc_t] [char](100) NOT NULL,
	[ds_desc_e] [char](100) NULL,
	[ds_desc] [varchar](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[senddoc]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[senddoc](
	[sed_regis_no] [varchar](20) NOT NULL,
	[sed_branch] [varchar](3) NULL,
	[sed_name] [varchar](50) NOT NULL,
	[sed_ins_addr] [varchar](100) NULL,
	[sed_amphor] [varchar](30) NULL,
	[sed_changwat] [varchar](2) NULL,
	[sed_postcode] [varchar](5) NULL,
	[sed_tel] [varchar](30) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[register]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[register](
	[rg_regis_no] [varchar](20) NULL,
	[rg_member_cd] [varchar](10) NULL,
	[rg_member_ref] [varchar](10) NULL,
	[rg_main_class] [varchar](1) NULL,
	[rg_pol_type] [varchar](1) NULL,
	[rg_effect_dt] [datetime] NULL,
	[rg_expiry_dt] [datetime] NULL,
	[rg_regis_dt] [datetime] NULL,
	[rg_regis_time] [char](5) NULL,
	[rg_regis_type] [varchar](1) NULL,
	[rg_ins_fname] [varchar](50) NULL,
	[rg_ins_lname] [varchar](50) NULL,
	[rg_ins_addr1] [varchar](50) NULL,
	[rg_ins_addr2] [varchar](50) NULL,
	[rg_ins_amphor] [varchar](30) NULL,
	[rg_ins_changwat] [varchar](2) NULL,
	[rg_ins_postcode] [varchar](5) NULL,
	[rg_ins_tel] [varchar](30) NULL,
	[rg_ins_email] [varchar](50) NULL,
	[rg_old_policy] [varchar](20) NULL,
	[rg_sum_ins] [numeric](12, 2) NULL,
	[rg_prmm] [numeric](10, 2) NULL,
	[rg_tax] [numeric](10, 2) NULL,
	[rg_stamp] [numeric](10, 2) NULL,
	[rg_pmt_cd] [varchar](10) NULL,
	[rg_fleet_perc] [numeric](5, 2) NULL,
	[rg_pay_type] [varchar](1) NULL,
	[rg_payment_stat] [varchar](3) NULL,
	[rg_ref_bank] [varchar](20) NULL,
	[rg_approve] [varchar](1) NULL,
	[rg_ins_idcard] [varchar](50) NULL,
	[rg_ins_mobile] [varchar](50) NULL,
	[rg_oth_distance] [varchar](50) NULL,
	[rg_oth_region] [varchar](50) NULL,
	[rg_oth_oldpolicy] [varchar](50) NULL,
	[rg_oth_flagdeduct] [varchar](1) NULL,
	[rg_mott_id] [varchar](10) NULL,
	[rg_insc_id] [varchar](10) NULL,
	[rg_prmmgross] [numeric](18, 2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rate]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rate](
	[pc_cd] [int] NOT NULL,
	[pc_name] [varchar](15) NULL,
	[pc_value] [decimal](5, 2) NULL,
	[pc_status] [char](1) NULL,
 CONSTRAINT [PK_percent] PRIMARY KEY CLUSTERED 
(
	[pc_cd] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[policy]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[policy](
	[pl_book_no] [varchar](5) NOT NULL,
	[pl_inform_no] [varchar](6) NOT NULL,
	[pl_regis_no] [varchar](20) NOT NULL,
	[pl_member_cd] [varchar](10) NULL,
	[pl_member_ref] [varchar](10) NULL,
	[pl_main_class] [varchar](1) NOT NULL,
	[pl_pol_type] [varchar](1) NOT NULL,
	[pl_policy_no] [varchar](25) NULL,
	[pl_effect_dt] [datetime] NOT NULL,
	[pl_expiry_dt] [datetime] NOT NULL,
	[pl_regis_dt] [datetime] NOT NULL,
	[pl_regis_time] [datetime] NOT NULL,
	[pl_regis_type] [varchar](1) NOT NULL,
	[pl_ins_fname] [varchar](50) NOT NULL,
	[pl_ins_lname] [varchar](50) NOT NULL,
	[pl_ins_addr1] [varchar](50) NULL,
	[pl_ins_addr2] [varchar](50) NOT NULL,
	[pl_ins_amphor] [varchar](30) NOT NULL,
	[pl_ins_changwat] [varchar](2) NOT NULL,
	[pl_ins_postcode] [varchar](5) NOT NULL,
	[pl_ins_tel] [varchar](30) NULL,
	[pl_ins_email] [varchar](50) NULL,
	[pl_old_policy] [varchar](20) NULL,
	[pl_sum_ins] [numeric](12, 2) NOT NULL,
	[pl_prmm] [numeric](10, 2) NOT NULL,
	[pl_tax] [numeric](10, 2) NOT NULL,
	[pl_stamp] [numeric](10, 2) NOT NULL,
	[pl_pmt_cd] [varchar](10) NULL,
	[pl_fleet_perc] [numeric](5, 2) NULL,
	[pl_pay_type] [varchar](1) NOT NULL,
	[pl_payment_stat] [varchar](3) NULL,
	[pl_ref_bank] [varchar](20) NULL,
	[pl_stat] [varchar](1) NOT NULL,
	[pl_approve_dt] [datetime] NULL,
	[pl_post_dt] [datetime] NULL,
	[pl_cancel_dt] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[personal]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[personal](
	[pe_regis_no] [char](20) NOT NULL,
	[pe_eff_time] [char](5) NOT NULL,
	[pe_card_type] [char](1) NULL,
	[pe_card_no] [varchar](20) NULL,
	[pe_birth_dt] [datetime] NULL,
	[pe_occup] [char](2) NULL,
	[pe_ben_fname] [varchar](50) NULL,
	[pe_ben_lname] [varchar](50) NULL,
	[pe_ben_addr1] [varchar](50) NULL,
	[pe_ben_addr2] [varchar](50) NULL,
	[pe_ben_amphor] [varchar](30) NULL,
	[pe_ben_changwat] [char](2) NULL,
	[pe_ben_postcode] [char](5) NULL,
	[pe_ben_tel] [varchar](30) NULL,
	[pe_ben_email] [varchar](50) NULL,
	[pe_relation] [char](8) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pa_tariff]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pa_tariff](
	[pa_code] [char](4) NOT NULL,
	[pa_rate] [numeric](5, 2) NULL,
	[pa_sum_ins] [numeric](12, 2) NULL,
	[pa_prmm] [numeric](10, 2) NULL,
	[pa_effect_dt] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pa_promotion]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[pa_promotion](
	[pm_major_cd] [char](4) NOT NULL,
	[pm_minor_cd] [char](4) NOT NULL,
	[pm_desc] [varchar](255) NULL,
	[pm_desc2] [varchar](255) NULL,
	[pm_pa1] [char](1) NOT NULL,
	[pm_pa1_si] [numeric](12, 2) NULL,
	[pm_pa2] [char](1) NOT NULL,
	[pm_pa2_si] [numeric](12, 2) NULL,
	[pm_ttd] [char](1) NOT NULL,
	[pm_ttd_week] [int] NULL,
	[pm_ttd_si] [numeric](12, 2) NULL,
	[pm_ptd] [char](1) NOT NULL,
	[pm_ptd_week] [int] NULL,
	[pm_ptd_si] [numeric](12, 2) NULL,
	[pm_med] [char](1) NOT NULL,
	[pm_med_si] [numeric](12, 2) NULL,
	[pm_war] [char](1) NOT NULL,
	[pm_str] [char](1) NOT NULL,
	[pm_spt] [char](1) NOT NULL,
	[pm_mtr] [char](1) NOT NULL,
	[pm_air] [char](1) NOT NULL,
	[pm_murder] [char](1) NULL,
	[pm_prmm] [numeric](10, 2) NULL,
	[pm_tax] [numeric](10, 2) NULL,
	[pm_stamp] [numeric](10, 2) NULL,
	[pm_effect_dt] [datetime] NOT NULL,
	[pm_expiry_dt] [datetime] NOT NULL,
	[pm_visitor] [char](1) NOT NULL,
	[pm_customer] [char](1) NOT NULL,
	[pm_agent] [char](1) NOT NULL,
	[pm_discount] [decimal](5, 2) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pa_medicine]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pa_medicine](
	[pam_sum_ins] [numeric](12, 2) NOT NULL,
	[pam_med_si_from] [numeric](12, 2) NULL,
	[pam_med_si_to] [numeric](12, 2) NULL,
	[pam_med_si_step] [numeric](12, 2) NULL,
 CONSTRAINT [PK_PA_MEDICINE] PRIMARY KEY CLUSTERED 
(
	[pam_sum_ins] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pa_cover]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pa_cover](
	[pa_regis_no] [char](20) NOT NULL,
	[pa_type] [char](3) NOT NULL,
	[pa_death_si] [numeric](12, 2) NOT NULL,
	[pa_death_exc] [numeric](10, 2) NULL,
	[pa_death_prmm] [numeric](10, 2) NULL,
	[pa_ttd_si] [numeric](12, 2) NULL,
	[pa_ttd_week] [int] NULL,
	[pa_ttd_exc] [numeric](10, 2) NULL,
	[pa_ttd_prmm] [numeric](10, 2) NULL,
	[pa_ptd_si] [numeric](12, 2) NULL,
	[pa_ptd_week] [int] NULL,
	[pa_ptd_exc] [numeric](10, 2) NULL,
	[pa_ptd_prmm] [numeric](10, 2) NULL,
	[pa_med_si] [numeric](12, 2) NULL,
	[pa_med_exc] [numeric](10, 2) NULL,
	[pa_med_prmm] [numeric](10, 2) NULL,
	[pa_war] [char](1) NOT NULL,
	[pa_str] [char](1) NOT NULL,
	[pa_spt] [char](1) NOT NULL,
	[pa_mtr] [char](1) NOT NULL,
	[pa_air] [char](1) NOT NULL,
	[pa_murder] [char](1) NOT NULL,
	[pa_add_prmm] [numeric](10, 2) NOT NULL,
	[pa_disc_prmm] [numeric](10, 2) NOT NULL,
	[pa_net_prmm] [numeric](10, 2) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[online_web_bk]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[online_web_bk](
	[web_id] [int] IDENTITY(1,1) NOT NULL,
	[campaign] [char](3) NOT NULL,
	[car_cod] [char](4) NOT NULL,
	[pol_typ] [char](1) NOT NULL,
	[car_age] [int] NOT NULL,
	[car_group] [char](1) NOT NULL,
	[car_mak] [char](8) NOT NULL,
	[car_size] [int] NOT NULL,
	[car_body] [char](2) NOT NULL,
	[agt_cod] [char](7) NOT NULL,
	[car_od] [decimal](10, 0) NULL,
	[car_driver] [int] NULL,
	[od_dd] [decimal](8, 0) NULL,
	[start_dte] [datetime] NOT NULL,
	[end_dte] [datetime] NULL,
	[pre_pregar] [decimal](12, 2) NULL,
	[pre_extgar] [decimal](12, 2) NULL,
	[pre_netgar] [decimal](12, 2) NULL,
	[pre_stmgar] [decimal](6, 2) NULL,
	[pre_taxgar] [decimal](10, 2) NULL,
	[pre_grsgar] [decimal](12, 2) NULL,
	[pre_preservice] [decimal](12, 2) NULL,
	[pre_extservice] [decimal](12, 2) NULL,
	[pre_netservice] [decimal](12, 2) NULL,
	[pre_stmservice] [decimal](6, 2) NULL,
	[pre_taxservice] [decimal](10, 2) NULL,
	[pre_grsservice] [decimal](12, 2) NULL,
	[sys_dte] [datetime] NULL,
	[pre_netgar20] [decimal](20, 6) NULL,
	[pre_netgar30] [decimal](20, 6) NULL,
	[pre_netgar40] [decimal](20, 6) NULL,
	[pre_netgar50] [decimal](20, 6) NULL,
	[pre_netservice20] [decimal](20, 6) NULL,
	[pre_netservice30] [decimal](20, 6) NULL,
	[pre_netservice40] [decimal](20, 6) NULL,
	[pre_netservice50] [decimal](20, 6) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[online_web]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[online_web](
	[web_id] [int] IDENTITY(1,1) NOT NULL,
	[campaign] [char](3) NOT NULL,
	[car_cod] [char](4) NOT NULL,
	[pol_typ] [char](1) NOT NULL,
	[car_age] [int] NOT NULL,
	[car_group] [char](1) NOT NULL,
	[car_mak] [char](8) NOT NULL,
	[car_size] [int] NOT NULL,
	[car_body] [char](2) NOT NULL,
	[agt_cod] [char](7) NOT NULL,
	[car_od] [decimal](10, 0) NULL,
	[car_driver] [int] NULL,
	[od_dd] [decimal](8, 0) NULL,
	[start_dte] [datetime] NOT NULL,
	[end_dte] [datetime] NULL,
	[pre_pregar] [decimal](12, 2) NULL,
	[pre_extgar] [decimal](12, 2) NULL,
	[pre_netgar] [decimal](12, 2) NULL,
	[pre_stmgar] [decimal](6, 2) NULL,
	[pre_taxgar] [decimal](10, 2) NULL,
	[pre_grsgar] [decimal](12, 2) NULL,
	[pre_preservice] [decimal](12, 2) NULL,
	[pre_extservice] [decimal](12, 2) NULL,
	[pre_netservice] [decimal](12, 2) NULL,
	[pre_stmservice] [decimal](6, 2) NULL,
	[pre_taxservice] [decimal](10, 2) NULL,
	[pre_grsservice] [decimal](12, 2) NULL,
	[sys_dte] [datetime] NULL,
	[pre_netgar20]  AS ([pre_netgar]-([pre_netgar]*(20))/(100)),
	[pre_netgar30]  AS ([pre_netgar]-([pre_netgar]*(30))/(100)) PERSISTED,
	[pre_netgar40]  AS ([pre_netgar]-([pre_netgar]*(40))/(100)),
	[pre_netgar50]  AS ([pre_netgar]-([pre_netgar]*(50))/(100)),
	[pre_netservice20]  AS ([pre_netservice]-([pre_netservice]*(20))/(100)),
	[pre_netservice30]  AS ([pre_netservice]-([pre_netservice]*(30))/(100)) PERSISTED,
	[pre_netservice40]  AS ([pre_netservice]-([pre_netservice]*(40))/(100)),
	[pre_netservice50]  AS ([pre_netservice]-([pre_netservice]*(50))/(100)),
 CONSTRAINT [PK_online_web] PRIMARY KEY CLUSTERED 
(
	[web_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[online_premium-test]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[online_premium-test](
	[prem_id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[cover_id] [varchar](10) NULL,
	[hprem_id] [varchar](10) NULL,
	[campaign] [char](3) NOT NULL,
	[car_cod] [char](4) NOT NULL,
	[pol_typ] [char](1) NOT NULL,
	[mot_gar] [char](1) NOT NULL,
	[car_age] [int] NOT NULL,
	[car_group] [char](1) NOT NULL,
	[car_mak] [char](8) NOT NULL,
	[car_size] [int] NOT NULL,
	[car_body] [char](2) NOT NULL,
	[agt_cod] [char](7) NOT NULL,
	[car_od] [decimal](10, 0) NULL,
	[car_driver] [int] NULL,
	[start_dte] [datetime] NULL,
	[end_dte] [datetime] NULL,
	[pre_pre] [decimal](12, 2) NULL,
	[pre_ext] [decimal](12, 2) NULL,
	[pre_net] [decimal](12, 2) NULL,
	[pre_stm] [decimal](6, 2) NULL,
	[pre_tax] [decimal](10, 2) NULL,
	[pre_grs] [decimal](12, 2) NULL,
	[sys_dte] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[online_premium_bk]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[online_premium_bk](
	[prem_id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[cover_id] [varchar](10) NULL,
	[hprem_id] [varchar](10) NULL,
	[campaign] [char](3) NOT NULL,
	[car_cod] [char](4) NOT NULL,
	[pol_typ] [char](1) NOT NULL,
	[mot_gar] [char](1) NOT NULL,
	[car_age] [int] NOT NULL,
	[car_group] [char](1) NOT NULL,
	[car_mak] [char](8) NOT NULL,
	[car_size] [int] NOT NULL,
	[car_body] [char](2) NOT NULL,
	[agt_cod] [char](7) NOT NULL,
	[car_od] [decimal](10, 0) NULL,
	[car_driver] [int] NULL,
	[start_dte] [datetime] NULL,
	[end_dte] [datetime] NULL,
	[pre_pre] [decimal](12, 2) NULL,
	[pre_ext] [decimal](12, 2) NULL,
	[pre_net] [decimal](12, 2) NULL,
	[pre_stm] [decimal](6, 2) NULL,
	[pre_tax] [decimal](10, 2) NULL,
	[pre_grs] [decimal](12, 2) NULL,
	[sys_dte] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[online_premium]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[online_premium](
	[prem_id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[cover_id] [varchar](10) NULL,
	[hprem_id] [varchar](10) NULL,
	[campaign] [char](3) NOT NULL,
	[car_cod] [char](4) NOT NULL,
	[pol_typ] [char](1) NOT NULL,
	[mot_gar] [char](1) NOT NULL,
	[car_age] [int] NOT NULL,
	[car_group] [char](1) NOT NULL,
	[car_mak] [char](8) NOT NULL,
	[car_size] [int] NOT NULL,
	[car_body] [char](2) NOT NULL,
	[agt_cod] [char](7) NOT NULL,
	[car_od] [decimal](10, 0) NULL,
	[car_driver] [int] NULL,
	[start_dte] [datetime] NULL,
	[end_dte] [datetime] NULL,
	[pre_pre] [decimal](12, 2) NULL,
	[pre_ext] [decimal](12, 2) NULL,
	[pre_net] [decimal](12, 2) NULL,
	[pre_stm] [decimal](6, 2) NULL,
	[pre_tax] [decimal](10, 2) NULL,
	[pre_grs] [decimal](12, 2) NULL,
	[sys_dte] [datetime] NULL,
 CONSTRAINT [PK_online_premium] PRIMARY KEY CLUSTERED 
(
	[prem_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[online_headpremium-test]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[online_headpremium-test](
	[hprem_id] [varchar](10) NOT NULL,
	[campaign] [char](3) NOT NULL,
	[promotion] [char](3) NULL,
	[car_cod] [char](4) NOT NULL,
	[car_odfrom] [decimal](10, 0) NULL,
	[car_odto] [decimal](10, 0) NULL,
	[start_dte] [datetime] NULL,
	[end_dte] [datetime] NULL,
	[row_num] [int] NULL,
	[sys_dte] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[online_headpremium_bk]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[online_headpremium_bk](
	[hprem_id] [varchar](10) NOT NULL,
	[campaign] [char](3) NOT NULL,
	[promotion] [char](3) NULL,
	[car_cod] [char](4) NOT NULL,
	[car_odfrom] [decimal](10, 0) NULL,
	[car_odto] [decimal](10, 0) NULL,
	[start_dte] [datetime] NULL,
	[end_dte] [datetime] NULL,
	[row_num] [int] NULL,
	[sys_dte] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[online_headpremium]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[online_headpremium](
	[hprem_id] [varchar](10) NOT NULL,
	[campaign] [char](3) NOT NULL,
	[promotion] [char](3) NULL,
	[car_cod] [char](4) NOT NULL,
	[car_odfrom] [decimal](10, 0) NULL,
	[car_odto] [decimal](10, 0) NULL,
	[start_dte] [datetime] NULL,
	[end_dte] [datetime] NULL,
	[row_num] [int] NULL,
	[sys_dte] [datetime] NULL,
 CONSTRAINT [PK_online_headpremium] PRIMARY KEY CLUSTERED 
(
	[hprem_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[online_cover-test]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[online_cover-test](
	[cover_id] [varchar](10) NOT NULL,
	[campaign] [char](3) NOT NULL,
	[car_cod] [char](4) NOT NULL,
	[pol_typ] [char](1) NOT NULL,
	[mot_gar] [char](1) NOT NULL,
	[car_age] [int] NOT NULL,
	[car_group] [char](1) NOT NULL,
	[car_mak] [char](8) NOT NULL,
	[car_size] [int] NOT NULL,
	[car_body] [char](2) NOT NULL,
	[agt_cod] [char](7) NOT NULL,
	[start_dte] [datetime] NULL,
	[end_dte] [datetime] NULL,
	[tpbi1] [decimal](9, 0) NULL,
	[tpbi2] [decimal](9, 0) NULL,
	[tppd] [decimal](8, 0) NULL,
	[tppd_dd] [decimal](8, 0) NULL,
	[od_dd] [decimal](8, 0) NULL,
	[perm_d_01] [decimal](8, 0) NULL,
	[perm_p_num] [int] NULL,
	[perm_p_01] [decimal](8, 0) NULL,
	[temp_d_01] [decimal](8, 0) NULL,
	[temp_p_num] [int] NULL,
	[temp_p_01] [decimal](8, 0) NULL,
	[cover_02] [decimal](8, 0) NULL,
	[cover_03] [decimal](8, 0) NULL,
	[sys_dte] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[online_cover_bk]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[online_cover_bk](
	[cover_id] [varchar](10) NOT NULL,
	[campaign] [char](3) NOT NULL,
	[car_cod] [char](4) NOT NULL,
	[pol_typ] [char](1) NOT NULL,
	[mot_gar] [char](1) NOT NULL,
	[car_age] [int] NOT NULL,
	[car_group] [char](1) NOT NULL,
	[car_mak] [char](8) NOT NULL,
	[car_size] [int] NOT NULL,
	[car_body] [char](2) NOT NULL,
	[agt_cod] [char](7) NOT NULL,
	[start_dte] [datetime] NULL,
	[end_dte] [datetime] NULL,
	[tpbi1] [decimal](9, 0) NULL,
	[tpbi2] [decimal](9, 0) NULL,
	[tppd] [decimal](8, 0) NULL,
	[tppd_dd] [decimal](8, 0) NULL,
	[od_dd] [decimal](8, 0) NULL,
	[perm_d_01] [decimal](8, 0) NULL,
	[perm_p_num] [int] NULL,
	[perm_p_01] [decimal](8, 0) NULL,
	[temp_d_01] [decimal](8, 0) NULL,
	[temp_p_num] [int] NULL,
	[temp_p_01] [decimal](8, 0) NULL,
	[cover_02] [decimal](8, 0) NULL,
	[cover_03] [decimal](8, 0) NULL,
	[sys_dte] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[online_cover]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[online_cover](
	[cover_id] [varchar](10) NOT NULL,
	[campaign] [char](3) NOT NULL,
	[car_cod] [char](4) NOT NULL,
	[pol_typ] [char](1) NOT NULL,
	[mot_gar] [char](1) NOT NULL,
	[car_age] [int] NOT NULL,
	[car_group] [char](1) NOT NULL,
	[car_mak] [char](8) NOT NULL,
	[car_size] [int] NOT NULL,
	[car_body] [char](2) NOT NULL,
	[agt_cod] [char](7) NOT NULL,
	[start_dte] [datetime] NULL,
	[end_dte] [datetime] NULL,
	[tpbi1] [decimal](9, 0) NULL,
	[tpbi2] [decimal](9, 0) NULL,
	[tppd] [decimal](8, 0) NULL,
	[tppd_dd] [decimal](8, 0) NULL,
	[od_dd] [decimal](8, 0) NULL,
	[perm_d_01] [decimal](8, 0) NULL,
	[perm_p_num] [int] NULL,
	[perm_p_01] [decimal](8, 0) NULL,
	[temp_d_01] [decimal](8, 0) NULL,
	[temp_p_num] [int] NULL,
	[temp_p_01] [decimal](8, 0) NULL,
	[cover_02] [decimal](8, 0) NULL,
	[cover_03] [decimal](8, 0) NULL,
	[sys_dte] [datetime] NULL,
 CONSTRAINT [PK_online_cover] PRIMARY KEY CLUSTERED 
(
	[cover_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[online_compprem]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[online_compprem](
	[car_cod] [char](3) NOT NULL,
	[car_body] [char](2) NOT NULL,
	[pre_net] [decimal](12, 2) NOT NULL,
	[pre_stm] [decimal](6, 2) NOT NULL,
	[pre_tax] [decimal](10, 2) NOT NULL,
	[pre_grs] [decimal](12, 2) NOT NULL,
 CONSTRAINT [PK_online_compprem] PRIMARY KEY CLUSTERED 
(
	[car_cod] ASC,
	[car_body] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[online_carname]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[online_carname](
	[carnamcod] [char](6) NOT NULL,
	[desc_t] [varchar](40) NULL,
	[desc_e] [varchar](40) NULL,
 CONSTRAINT [PK_online_carname] PRIMARY KEY CLUSTERED 
(
	[carnamcod] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[online_carmark_bk]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[online_carmark_bk](
	[carmakcod] [char](8) NOT NULL,
	[carnamcod] [char](6) NULL,
	[group_code] [char](2) NULL,
	[desc_t] [varchar](40) NULL,
	[desc_e] [varchar](40) NULL,
	[car_type] [char](1) NULL,
	[od_min] [decimal](18, 0) NULL,
	[od_max] [decimal](18, 0) NULL,
	[car_cc] [int] NULL,
	[input_date] [datetime] NULL,
	[input_user] [varchar](13) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[online_carmark]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[online_carmark](
	[carmakcod] [char](8) NOT NULL,
	[carnamcod] [char](6) NULL,
	[group_code] [char](2) NULL,
	[desc_t] [varchar](40) NULL,
	[desc_e] [varchar](40) NULL,
	[car_type] [char](1) NULL,
	[od_min] [decimal](18, 0) NULL,
	[od_max] [decimal](18, 0) NULL,
	[car_cc] [int] NULL,
	[input_date] [datetime] NULL,
	[input_user] [varchar](13) NULL,
 CONSTRAINT [PK_online_carmark] PRIMARY KEY CLUSTERED 
(
	[carmakcod] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[online_campaign]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[online_campaign](
	[campaign] [char](3) NOT NULL,
	[pol_typ] [char](1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[pattern] [char](2) NOT NULL,
	[cp_sts] [varchar](10) NOT NULL,
	[cp_seq] [int] NULL,
 CONSTRAINT [PK_online_campaign_1] PRIMARY KEY CLUSTERED 
(
	[campaign] ASC,
	[pol_typ] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NtwZone]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NtwZone](
	[NtwZoneId] [int] NOT NULL,
	[NtwZoneName] [varchar](50) NULL,
	[Spare1] [varchar](50) NULL,
	[Spare2] [varchar](50) NULL,
	[Spare3] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NtwType]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NtwType](
	[NtwTypeId] [int] NOT NULL,
	[NtwTypeName] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mtveh]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mtveh](
	[mv_regis_no] [char](20) NOT NULL,
	[mv_major_cd] [char](4) NOT NULL,
	[mv_minor_cd] [char](8) NOT NULL,
	[mv_veh_year] [smallint] NOT NULL,
	[mv_veh_cc] [smallint] NULL,
	[mv_veh_seat] [smallint] NULL,
	[mv_veh_weight] [smallint] NULL,
	[mv_license_no] [char](20) NULL,
	[mv_license_area] [char](2) NULL,
	[mv_engin_no] [char](20) NULL,
	[mv_chas_no] [char](20) NULL,
	[mv_combine] [char](1) NOT NULL,
	[mv_no_claim] [char](1) NULL,
	[mv_no_claim_perc] [numeric](5, 2) NULL,
 CONSTRAINT [PK_mtveh] PRIMARY KEY CLUSTERED 
(
	[mv_regis_no] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mtdesc_comp_veh_all]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mtdesc_comp_veh_all](
	[mdcv_veh_comp] [char](2) NOT NULL,
	[mdcv_veh_cd] [char](7) NOT NULL,
	[mdcv_veh_use] [char](1) NOT NULL,
	[mdcv_desc_t] [varchar](100) NULL,
	[mdcv_desc_e] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mtdesc_comp_veh]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mtdesc_comp_veh](
	[mdcv_veh_comp] [char](2) NOT NULL,
	[mdcv_veh_cd] [char](7) NOT NULL,
	[mdcv_veh_use] [char](1) NOT NULL,
	[mdcv_desc_t] [varchar](100) NULL,
	[mdcv_desc_e] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mtdesc_comp]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mtdesc_comp](
	[mdc_major_cd] [char](4) NOT NULL,
	[mdc_minor_cd] [char](4) NOT NULL,
	[mdc_desc_t] [char](100) NULL,
	[mdc_desc_e] [char](100) NULL,
	[mdc_veh_comp] [char](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mst_premiumperiod]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mst_premiumperiod](
	[peri_id] [int] NOT NULL,
	[peri_type] [varchar](30) NULL,
	[peri_value] [int] NULL,
	[peri_text] [varchar](200) NULL,
 CONSTRAINT [PK_mst_premiumperiod] PRIMARY KEY CLUSTERED 
(
	[peri_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mst_motortype]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mst_motortype](
	[mott_id] [varchar](10) NOT NULL,
	[mott_name] [varchar](50) NULL,
	[mott_status] [char](1) NULL,
 CONSTRAINT [PK_mst_motortype] PRIMARY KEY CLUSTERED 
(
	[mott_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mst_insurancecompany]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mst_insurancecompany](
	[insc_id] [varchar](10) NOT NULL,
	[insc_name] [varchar](100) NULL,
	[insc_status] [char](1) NULL,
 CONSTRAINT [PK_mst_insurancecompany] PRIMARY KEY CLUSTERED 
(
	[insc_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mst_giftvoucher]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mst_giftvoucher](
	[gvou_id] [varchar](10) NOT NULL,
	[gvou_type] [char](1) NULL,
	[gvou_premmin] [decimal](18, 2) NULL,
	[gvou_premmax] [decimal](18, 2) NULL,
	[gvou_value] [decimal](18, 2) NULL,
	[gvou_status] [char](1) NULL,
 CONSTRAINT [PK_mst_giftvoucher] PRIMARY KEY CLUSTERED 
(
	[gvou_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mst_drivedistance]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mst_drivedistance](
	[ddis_id] [varchar](10) NOT NULL,
	[ddis_name] [varchar](50) NULL,
	[ddis_status] [char](1) NULL,
 CONSTRAINT [PK_mst_drivedistant] PRIMARY KEY CLUSTERED 
(
	[ddis_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mst_defaultvalue]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mst_defaultvalue](
	[defv_id] [int] NOT NULL,
	[car_cod] [char](4) NULL,
	[defv_type] [varchar](10) NULL,
	[defv_value] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[lastno]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[lastno](
	[lt_main_class] [char](2) NOT NULL,
	[lt_prefix] [varchar](8) NOT NULL,
	[lt_year] [char](4) NOT NULL,
	[lt_month] [char](2) NOT NULL,
	[lt_day] [char](2) NOT NULL,
	[lt_run_no] [int] NOT NULL,
 CONSTRAINT [PK_lastno] PRIMARY KEY CLUSTERED 
(
	[lt_main_class] ASC,
	[lt_prefix] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[discload]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[discload](
	[dc_major_cd] [char](3) NOT NULL,
	[dc_minor_cd] [char](3) NOT NULL,
	[dc_cover] [numeric](10, 2) NOT NULL,
	[dc_amt] [numeric](12, 2) NULL,
	[dc_perc] [numeric](7, 3) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[credit_post]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[credit_post](
	[cp_mid] [char](15) NOT NULL,
	[cp_terminal] [int] NOT NULL,
	[cp_ref_no] [char](20) NOT NULL,
	[cp_ref_date] [char](14) NOT NULL,
	[cp_cur_abbr] [char](3) NOT NULL,
	[cp_amount] [numeric](10, 2) NOT NULL,
	[cp_cust_lname] [char](80) NOT NULL,
	[cp_cust_fname] [char](60) NOT NULL,
	[cp_cust_email] [char](100) NOT NULL,
	[cp_cust_country] [char](2) NOT NULL,
	[cp_cust_addr1] [char](80) NOT NULL,
	[cp_cust_addr2] [char](80) NOT NULL,
	[cp_cust_city] [char](60) NOT NULL,
	[cp_cust_province] [char](60) NOT NULL,
	[cp_cust_zip] [char](10) NOT NULL,
	[cp_telephone] [char](20) NULL,
	[cp_settle_flag] [char](1) NOT NULL,
	[cp_stat] [char](1) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[contact]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[contact](
	[ct_code] [char](15) NOT NULL,
	[ct_name] [varchar](50) NOT NULL,
	[ct_telephone] [varchar](50) NULL,
	[ct_email] [varchar](50) NULL,
	[ct_con_time] [char](5) NULL,
	[ct_motor] [char](1) NULL,
	[ct_fire] [char](1) NULL,
	[ct_marine] [char](1) NULL,
	[ct_pa] [char](1) NULL,
	[ct_health] [char](1) NULL,
	[ct_cancer] [char](1) NULL,
	[ct_other] [char](1) NULL,
	[ct_detail] [char](200) NULL,
	[ct_regis_dt] [datetime] NULL,
	[ct_cont_stat] [char](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[compulsary]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[compulsary](
	[cp_regis_no] [char](20) NOT NULL,
	[cp_veh_cd] [char](5) NOT NULL,
	[cp_comp_prmm] [numeric](10, 2) NOT NULL,
	[cp_comp_tax] [numeric](6, 2) NULL,
	[cp_comp_stamp] [numeric](6, 2) NULL,
 CONSTRAINT [PK_compulsary] PRIMARY KEY CLUSTERED 
(
	[cp_regis_no] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[comprmm]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[comprmm](
	[cp_veh_cd] [char](7) NOT NULL,
	[cp_com_prmm] [numeric](10, 2) NOT NULL,
	[cp_tax] [numeric](6, 2) NOT NULL,
	[cp_stamp] [numeric](6, 2) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[car_import]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[car_import](
	[carmakcod] [varchar](50) NOT NULL,
	[im_flag] [varchar](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[aut_user]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[aut_user](
	[user_login] [varchar](13) NOT NULL,
	[user_pwd] [varchar](30) NOT NULL,
	[user_empcode] [varchar](5) NULL,
	[user_fname] [varchar](100) NOT NULL,
	[user_lname] [varchar](100) NULL,
	[role_id] [char](3) NOT NULL,
	[user_status] [char](1) NOT NULL,
	[user_canceldate] [datetime] NULL,
	[user_cancellogin] [varchar](13) NULL,
	[start_date] [datetime] NOT NULL,
	[stop_date] [datetime] NULL,
	[user_email] [varchar](50) NULL,
	[user_tel] [varchar](50) NULL,
	[user_fax] [varchar](50) NULL,
	[user_flagexpriepwd] [char](1) NULL,
	[user_expirepwddays] [int] NULL,
	[user_changepwddate] [datetime] NULL,
	[user_remark] [varchar](200) NULL,
	[user_lastlogindate] [datetime] NULL,
	[insert_date] [datetime] NOT NULL,
	[insert_login] [varchar](13) NOT NULL,
	[update_date] [datetime] NULL,
	[update_login] [varchar](13) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[aut_role]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[aut_role](
	[role_id] [varchar](3) NOT NULL,
	[role_name] [varchar](50) NOT NULL,
	[role_desc] [varchar](200) NULL,
	[role_status] [char](1) NOT NULL,
	[role_canceldate] [datetime] NULL,
	[role_cancellogin] [varchar](13) NULL,
	[insert_date] [datetime] NOT NULL,
	[insert_login] [varchar](13) NOT NULL,
	[update_date] [datetime] NULL,
	[update_login] [varchar](13) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[add_prmm]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[add_prmm](
	[ad_major_cd] [nvarchar](255) NULL,
	[ad_minor_cd] [nvarchar](255) NULL,
	[ad_rate] [float] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[voluntary]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[voluntary](
	[vl_regis_no] [char](20) NOT NULL,
	[vl_veh_cd] [char](5) NOT NULL,
	[vl_drv_flag] [char](1) NULL,
	[vl_drv1] [char](100) NULL,
	[vl_birth_drv1] [smalldatetime] NULL,
	[vl_drv2] [char](100) NULL,
	[vl_birth_drv2] [smalldatetime] NULL,
	[vl_tpbi_person] [numeric](12, 2) NULL,
	[vl_tpbi_time] [numeric](12, 2) NULL,
	[vl_tppd_time] [numeric](12, 2) NULL,
	[vl_tppd_exc] [numeric](10, 2) NULL,
	[vl_od_time] [numeric](12, 2) NULL,
	[vl_od_exc] [numeric](10, 2) NULL,
	[vl_f_t] [numeric](12, 2) NULL,
	[vl_01_11] [numeric](12, 2) NULL,
	[vl_01_121] [smallint] NULL,
	[vl_01_122] [numeric](12, 2) NULL,
	[vl_01_21] [numeric](12, 2) NULL,
	[vl_01_211] [smallint] NULL,
	[vl_01_212] [numeric](12, 2) NULL,
	[vl_02_person] [smallint] NULL,
	[vl_02] [numeric](12, 2) NULL,
	[vl_03] [numeric](12, 2) NULL,
	[vl_repair] [char](1) NULL,
	[vl_accessory] [char](8) NULL,
	[vl_fleet_perc] [numeric](5, 2) NULL,
	[vl_prmm] [numeric](10, 2) NULL,
	[vl_campaign] [varchar](3) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tmpvolun]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tmpvolun](
	[tvl_tmp_cd] [char](10) NOT NULL,
	[tvl_veh_cd] [char](5) NOT NULL,
	[tvl_drv_flag] [char](1) NULL,
	[tvl_drv1] [char](100) NULL,
	[tvl_birth_drv1] [datetime] NULL,
	[tvl_drv2] [char](100) NULL,
	[tvl_birth_drv2] [datetime] NULL,
	[tvl_tpbi_person] [numeric](12, 2) NULL,
	[tvl_tpbi_time] [numeric](12, 2) NULL,
	[tvl_tppd_time] [numeric](12, 2) NULL,
	[tvl_tppd_exc] [numeric](10, 2) NULL,
	[tvl_od_time] [numeric](12, 2) NULL,
	[tvl_od_exc] [numeric](10, 2) NULL,
	[tvl_f_t] [numeric](12, 2) NULL,
	[tvl_01_11] [numeric](12, 2) NULL,
	[tvl_01_121] [smallint] NULL,
	[tvl_01_122] [numeric](12, 2) NULL,
	[tvl_01_21] [numeric](12, 2) NULL,
	[tvl_01_211] [smallint] NULL,
	[tvl_01_212] [numeric](12, 2) NULL,
	[tvl_02_person] [smallint] NULL,
	[tvl_02] [numeric](12, 2) NULL,
	[tvl_03] [numeric](12, 2) NULL,
	[tvl_repair] [char](1) NULL,
	[tvl_accessory] [char](8) NULL,
	[tvl_fleet_perc] [numeric](5, 2) NULL,
	[tvl_prmm] [numeric](10, 2) NULL,
	[tvl_campaign] [varchar](3) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tmpta_cover]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tmpta_cover](
	[tta_tmp_no] [char](10) NOT NULL,
	[tta_tot_ins] [int] NOT NULL,
	[tta_sum_ins] [numeric](12, 2) NOT NULL,
	[tta_medic_exp] [numeric](12, 2) NOT NULL,
	[tta_prmm] [numeric](10, 2) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tmpreg]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tmpreg](
	[tm_tmp_cd] [varchar](10) NOT NULL,
	[tm_regis_no] [varchar](20) NULL,
	[tm_member_cd] [varchar](10) NULL,
	[tm_member_ref] [varchar](10) NULL,
	[tm_main_class] [varchar](1) NOT NULL,
	[tm_pol_type] [varchar](1) NOT NULL,
	[tm_effect_dt] [datetime] NULL,
	[tm_expiry_dt] [datetime] NULL,
	[tm_regis_dt] [datetime] NOT NULL,
	[tm_regis_time] [char](5) NOT NULL,
	[tm_regis_type] [varchar](1) NOT NULL,
	[tm_ins_fname] [varchar](50) NOT NULL,
	[tm_ins_lname] [varchar](50) NOT NULL,
	[tm_ins_addr1] [varchar](50) NULL,
	[tm_ins_addr2] [varchar](50) NULL,
	[tm_ins_amphor] [varchar](30) NULL,
	[tm_ins_changwat] [varchar](2) NULL,
	[tm_ins_postcode] [varchar](5) NULL,
	[tm_ins_tel] [varchar](30) NULL,
	[tm_ins_email] [varchar](50) NULL,
	[tm_old_policy] [varchar](20) NULL,
	[tm_sum_ins] [numeric](12, 2) NOT NULL,
	[tm_prmm] [numeric](10, 2) NOT NULL,
	[tm_tax] [numeric](10, 2) NOT NULL,
	[tm_stamp] [numeric](10, 2) NOT NULL,
	[tm_fleet_perc] [numeric](5, 2) NOT NULL,
	[tm_stat] [varchar](1) NULL,
	[tm_ins_mobile] [varchar](30) NULL,
	[tm_ins_idcard] [varchar](13) NULL,
	[tm_oth_distance] [varchar](10) NULL,
	[tm_oth_region] [char](1) NULL,
	[tm_oth_flagdeduct] [char](1) NULL,
	[tm_oth_oldpolicy] [char](1) NULL,
	[tm_mott_id] [varchar](10) NULL,
	[tm_insc_id] [varchar](10) NULL,
	[tm_prmmgross] [numeric](18, 2) NULL,
 CONSTRAINT [PK_tmpreg] PRIMARY KEY CLUSTERED 
(
	[tm_tmp_cd] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tmppersonal]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tmppersonal](
	[tpe_tmp_no] [char](10) NOT NULL,
	[tpe_eff_time] [char](5) NOT NULL,
	[tpe_card_type] [char](1) NULL,
	[tpe_card_no] [varchar](20) NULL,
	[tpe_birth_dt] [datetime] NULL,
	[tpe_occup] [char](2) NULL,
	[tpe_ben_fname] [varchar](50) NULL,
	[tpe_ben_lname] [varchar](50) NULL,
	[tpe_ben_addr1] [varchar](50) NULL,
	[tpe_ben_addr2] [varchar](50) NULL,
	[tpe_ben_amphor] [varchar](30) NULL,
	[tpe_ben_changwat] [char](2) NULL,
	[tpe_ben_postcode] [char](5) NULL,
	[tpe_ben_tel] [varchar](30) NULL,
	[tpe_ben_email] [varchar](50) NULL,
	[tpe_relation] [char](8) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tmppa_cover]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tmppa_cover](
	[tpa_tmp_no] [char](10) NOT NULL,
	[tpa_type] [char](3) NOT NULL,
	[tpa_death_si] [numeric](12, 2) NOT NULL,
	[tpa_death_exc] [numeric](10, 2) NULL,
	[tpa_death_prmm] [numeric](10, 2) NULL,
	[tpa_ttd_si] [numeric](12, 2) NULL,
	[tpa_ttd_week] [int] NULL,
	[tpa_ttd_exc] [numeric](10, 2) NULL,
	[tpa_ttd_prmm] [numeric](10, 2) NULL,
	[tpa_ptd_si] [numeric](12, 2) NULL,
	[tpa_ptd_week] [int] NULL,
	[tpa_ptd_exc] [numeric](10, 2) NULL,
	[tpa_ptd_prmm] [numeric](10, 2) NULL,
	[tpa_med_si] [numeric](12, 2) NULL,
	[tpa_med_exc] [numeric](10, 2) NULL,
	[tpa_med_prmm] [numeric](10, 2) NULL,
	[tpa_war] [char](1) NOT NULL,
	[tpa_str] [char](1) NOT NULL,
	[tpa_spt] [char](1) NOT NULL,
	[tpa_mtr] [char](1) NOT NULL,
	[tpa_air] [char](1) NOT NULL,
	[tpa_murder] [char](1) NOT NULL,
	[tpa_add_prmm] [numeric](10, 2) NOT NULL,
	[tpa_disc_prmm] [numeric](10, 2) NOT NULL,
	[tpa_net_prmm] [numeric](10, 2) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tmpmtveh]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tmpmtveh](
	[tv_tmp_cd] [char](10) NOT NULL,
	[tv_major_cd] [char](4) NOT NULL,
	[tv_minor_cd] [char](8) NOT NULL,
	[tv_veh_year] [smallint] NOT NULL,
	[tv_veh_cc] [smallint] NULL,
	[tv_veh_seat] [smallint] NULL,
	[tv_veh_weight] [smallint] NULL,
	[tv_license_no] [char](20) NULL,
	[tv_license_area] [char](30) NULL,
	[tv_engin_no] [char](20) NULL,
	[tv_chas_no] [char](20) NULL,
	[tv_combine] [char](1) NULL,
	[tv_no_claim] [char](1) NULL,
	[tv_no_claim_perc] [numeric](5, 2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tmpcompul]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tmpcompul](
	[tcp_tmp_cd] [char](10) NOT NULL,
	[tcp_veh_cd] [char](5) NOT NULL,
	[tcp_comp_prmm] [numeric](10, 2) NOT NULL,
	[tcp_comp_tax] [numeric](6, 2) NULL,
	[tcp_comp_stamp] [numeric](6, 2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tmp1]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tmp1](
	[campaign] [char](3) NOT NULL,
	[car_cod] [char](4) NOT NULL,
	[pol_typ] [char](1) NOT NULL,
	[car_age] [int] NOT NULL,
	[car_group] [char](1) NOT NULL,
	[car_mak] [char](8) NOT NULL,
	[car_size] [int] NOT NULL,
	[car_body] [char](1) NOT NULL,
	[agt_cod] [char](7) NOT NULL,
	[car_od] [decimal](10, 0) NULL,
	[car_driver] [int] NULL,
	[start_dte] [datetime] NOT NULL,
	[end_dte] [datetime] NULL,
	[pre_pregar] [decimal](12, 2) NULL,
	[pre_extgar] [decimal](12, 2) NULL,
	[pre_netgar] [decimal](12, 2) NULL,
	[pre_grsgar] [decimal](12, 2) NULL,
	[pre_preservice] [decimal](12, 2) NULL,
	[pre_extservice] [decimal](12, 2) NULL,
	[pre_netservice] [decimal](12, 2) NULL,
	[pre_grsservice] [decimal](12, 2) NULL,
	[sys_dte] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tmp_prov]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tmp_prov](
	[ds_major_cd] [char](2) NOT NULL,
	[ds_minor_cd] [char](8) NOT NULL,
	[ds_desc_t] [char](100) NOT NULL,
	[ds_desc_e] [char](100) NULL,
	[ds_desc] [varchar](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tmp_pol1Mage99]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tmp_pol1Mage99](
	[web_id] [int] IDENTITY(1,1) NOT NULL,
	[campaign] [char](3) NOT NULL,
	[car_cod] [char](4) NOT NULL,
	[pol_typ] [char](1) NOT NULL,
	[car_age] [int] NOT NULL,
	[car_group] [char](1) NOT NULL,
	[car_mak] [char](8) NOT NULL,
	[car_size] [int] NOT NULL,
	[car_body] [char](2) NOT NULL,
	[agt_cod] [char](7) NOT NULL,
	[car_od] [decimal](10, 0) NULL,
	[car_driver] [int] NULL,
	[od_dd] [decimal](8, 0) NULL,
	[start_dte] [datetime] NOT NULL,
	[end_dte] [datetime] NULL,
	[pre_pregar] [decimal](12, 2) NULL,
	[pre_extgar] [decimal](12, 2) NULL,
	[pre_netgar] [decimal](12, 2) NULL,
	[pre_stmgar] [decimal](6, 2) NULL,
	[pre_taxgar] [decimal](10, 2) NULL,
	[pre_grsgar] [decimal](12, 2) NULL,
	[pre_preservice] [decimal](12, 2) NULL,
	[pre_extservice] [decimal](12, 2) NULL,
	[pre_netservice] [decimal](12, 2) NULL,
	[pre_stmservice] [decimal](6, 2) NULL,
	[pre_taxservice] [decimal](10, 2) NULL,
	[pre_grsservice] [decimal](12, 2) NULL,
	[sys_dte] [datetime] NULL,
	[pre_netgar20] [decimal](20, 6) NULL,
	[pre_netgar30] [decimal](20, 6) NULL,
	[pre_netgar40] [decimal](20, 6) NULL,
	[pre_netgar50] [decimal](20, 6) NULL,
	[pre_netservice20] [decimal](20, 6) NULL,
	[pre_netservice30] [decimal](20, 6) NULL,
	[pre_netservice40] [decimal](20, 6) NULL,
	[pre_netservice50] [decimal](20, 6) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tmp_pol12age99]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tmp_pol12age99](
	[web_id] [int] IDENTITY(1,1) NOT NULL,
	[campaign] [char](3) NOT NULL,
	[car_cod] [char](4) NOT NULL,
	[pol_typ] [char](1) NOT NULL,
	[car_age] [int] NOT NULL,
	[car_group] [char](1) NOT NULL,
	[car_mak] [char](8) NOT NULL,
	[car_size] [int] NOT NULL,
	[car_body] [char](2) NOT NULL,
	[agt_cod] [char](7) NOT NULL,
	[car_od] [decimal](10, 0) NULL,
	[car_driver] [int] NULL,
	[od_dd] [decimal](8, 0) NULL,
	[start_dte] [datetime] NOT NULL,
	[end_dte] [datetime] NULL,
	[pre_pregar] [decimal](12, 2) NULL,
	[pre_extgar] [decimal](12, 2) NULL,
	[pre_netgar] [decimal](12, 2) NULL,
	[pre_stmgar] [decimal](6, 2) NULL,
	[pre_taxgar] [decimal](10, 2) NULL,
	[pre_grsgar] [decimal](12, 2) NULL,
	[pre_preservice] [decimal](12, 2) NULL,
	[pre_extservice] [decimal](12, 2) NULL,
	[pre_netservice] [decimal](12, 2) NULL,
	[pre_stmservice] [decimal](6, 2) NULL,
	[pre_taxservice] [decimal](10, 2) NULL,
	[pre_grsservice] [decimal](12, 2) NULL,
	[sys_dte] [datetime] NULL,
	[pre_netgar20] [decimal](20, 6) NULL,
	[pre_netgar30] [decimal](20, 6) NULL,
	[pre_netgar40] [decimal](20, 6) NULL,
	[pre_netgar50] [decimal](20, 6) NULL,
	[pre_netservice20] [decimal](20, 6) NULL,
	[pre_netservice30] [decimal](20, 6) NULL,
	[pre_netservice40] [decimal](20, 6) NULL,
	[pre_netservice50] [decimal](20, 6) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tmp_carprice2012]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tmp_carprice2012](
	[carmakcod] [char](8) NOT NULL,
	[carnamcod] [char](6) NULL,
	[group_code] [char](2) NULL,
	[desc_t] [varchar](40) NULL,
	[desc_e] [varchar](40) NULL,
	[car_type] [char](1) NULL,
	[car_price] [decimal](12, 2) NULL,
	[od_min] [int] NOT NULL,
	[od_max] [int] NOT NULL,
	[car_cc] [smallint] NULL,
	[od_avg] [decimal](12, 2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tmp_carprice2]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tmp_carprice2](
	[carmakcod] [char](8) NOT NULL,
	[desc_t] [varchar](40) NULL,
	[car_price] [decimal](12, 2) NULL,
	[od_min] [int] NOT NULL,
	[od_max] [int] NOT NULL,
	[od_avg] [decimal](12, 2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tmp_carprice1]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tmp_carprice1](
	[carmakcod] [char](8) NOT NULL,
	[carnamcod] [char](6) NULL,
	[group_code] [char](2) NULL,
	[desc_t] [varchar](40) NULL,
	[desc_e] [varchar](40) NULL,
	[car_type] [char](1) NULL,
	[od_min] [int] NOT NULL,
	[od_max] [int] NOT NULL,
	[car_cc] [smallint] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tmp_carimport]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tmp_carimport](
	[carmakcod] [varchar](50) NOT NULL,
	[im_flag] [varchar](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbPrdCon]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbPrdCon](
	[PrdConId] [int] IDENTITY(1,1) NOT NULL,
	[PrdCatId] [int] NULL,
	[PrdConTopic] [nvarchar](max) NULL,
	[PrdConDetailShort] [nvarchar](max) NULL,
	[PrdConDetailLong] [text] NULL,
	[PrdConDtCreate] [datetime] NULL,
	[PrdConDtShow] [datetime] NULL,
	[PrdConDtHide] [datetime] NULL,
	[PrdConStatus] [int] NULL,
	[PrdConSort] [decimal](8, 3) NULL,
	[PrdConIp] [nvarchar](max) NULL,
	[PrdPicSmall] [nvarchar](max) NULL,
	[PrdPicBig] [nvarchar](max) NULL,
	[PrdConType] [int] NULL,
	[lang] [int] NULL,
	[input_date] [datetime] NULL,
	[input_user] [varchar](13) NULL,
 CONSTRAINT [PK_tbPrdCon] PRIMARY KEY CLUSTERED 
(
	[PrdConId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbPrdCat]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbPrdCat](
	[PrdCatId] [int] IDENTITY(1,1) NOT NULL,
	[PrdCatName] [varchar](100) NULL,
	[PrdCatDes] [varchar](100) NULL,
	[Spare1] [varchar](100) NULL,
	[Spare2] [varchar](100) NULL,
 CONSTRAINT [PK_tbPrdCat] PRIMARY KEY CLUSTERED 
(
	[PrdCatId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbNtwZone]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbNtwZone](
	[NtwZoneId] [int] IDENTITY(1,1) NOT NULL,
	[NtwZoneName] [varchar](50) NULL,
	[Spare1] [varchar](50) NULL,
	[Spare2] [varchar](50) NULL,
	[Spare3] [varchar](50) NULL,
 CONSTRAINT [PK_tbNtwZone] PRIMARY KEY CLUSTERED 
(
	[NtwZoneId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbNtwType]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbNtwType](
	[NtwTypeId] [int] IDENTITY(1,1) NOT NULL,
	[NtwTypeName] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbNtwCat]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbNtwCat](
	[NtwCatId] [int] IDENTITY(1,1) NOT NULL,
	[NtwCatName] [varchar](50) NULL,
	[spare1] [varchar](50) NULL,
	[spare2] [varchar](50) NULL,
 CONSTRAINT [PK_tbNtwCat] PRIMARY KEY CLUSTERED 
(
	[NtwCatId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbNtw]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbNtw](
	[NtwId] [int] IDENTITY(1,1) NOT NULL,
	[NtwZoneId] [int] NULL,
	[NtwCatId] [int] NULL,
	[NtwName] [varchar](200) NULL,
	[NtwAddress] [text] NULL,
	[NtwTelPhone] [varchar](50) NULL,
	[NtwTelMobile] [varchar](50) NULL,
	[NtwProvince] [varchar](50) NULL,
	[NtwEmail] [varchar](50) NULL,
	[NtwNotation] [varchar](max) NULL,
	[NtwMap] [text] NULL,
	[NtwPic1] [varchar](200) NULL,
	[NtwPic2] [varchar](200) NULL,
	[NtwStatus] [int] NULL,
	[NtwSort] [decimal](8, 3) NULL,
	[NtwDateTime] [datetime] NULL,
	[NtwNumCode] [varchar](50) NULL,
	[NtwTypeId] [int] NULL,
	[NtwAmphoe] [varchar](100) NULL,
	[NtwService] [text] NULL,
	[lang] [int] NULL,
	[input_date] [datetime] NULL,
	[input_user] [varchar](13) NULL,
 CONSTRAINT [PK_tbNtw] PRIMARY KEY CLUSTERED 
(
	[NtwId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbNewsCon]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbNewsCon](
	[NewsConId] [int] IDENTITY(1,1) NOT NULL,
	[NewsCatId] [int] NULL,
	[NewsConTopic] [nvarchar](max) NULL,
	[NewsConDetailShort] [nvarchar](max) NULL,
	[NewsConDetailLong] [nvarchar](max) NULL,
	[NewsConDtCreate] [datetime] NULL,
	[NewsConDtShow] [datetime] NULL,
	[NewsConDtHide] [datetime] NULL,
	[NewsConStatus] [int] NULL,
	[NewsConSort] [decimal](8, 3) NULL,
	[NewsConIp] [nvarchar](max) NULL,
	[NewsPicSmall] [nvarchar](max) NULL,
	[NewsPicBig] [nvarchar](max) NULL,
	[NewsVdo] [nvarchar](max) NULL,
	[lang] [int] NULL,
	[input_date] [datetime] NULL,
	[input_user] [varchar](13) NULL,
 CONSTRAINT [PK_tbNewsCon] PRIMARY KEY CLUSTERED 
(
	[NewsConId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbNewsCat]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbNewsCat](
	[NewsCatId] [int] IDENTITY(1,1) NOT NULL,
	[NewsCatName] [varchar](100) NULL,
	[NewsCatDes] [varchar](100) NULL,
	[Spare1] [varchar](100) NULL,
	[Spare2] [varchar](100) NULL,
 CONSTRAINT [PK_tbNewsCat] PRIMARY KEY CLUSTERED 
(
	[NewsCatId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbJobPrf]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbJobPrf](
	[JobPrfId] [int] IDENTITY(1,1) NOT NULL,
	[JobCatId] [int] NULL,
	[JobPrfPosition1] [varchar](200) NULL,
	[JobPrfPosition2] [varchar](200) NULL,
	[JobPrfPosition3] [varchar](200) NULL,
	[JobPrfSalary] [varchar](50) NULL,
	[JobPrfSex] [varchar](50) NULL,
	[JobPrfName1] [varchar](50) NULL,
	[JobPrfName2] [varchar](50) NULL,
	[JobPrfBirth] [datetime] NULL,
	[JobPrfAge] [int] NULL,
	[JobPrfIdCard] [varchar](50) NULL,
	[JobPrfNationality] [varchar](100) NULL,
	[JobPrfReligion] [varchar](100) NULL,
	[JobPrfWeight] [varchar](50) NULL,
	[JobPrfHeight] [varchar](50) NULL,
	[JobPrfMilitary] [int] NULL,
	[JobPrfAddress1] [varchar](max) NULL,
	[JobPrfAddress2] [varchar](max) NULL,
	[JobPrfTel1] [varchar](50) NULL,
	[JobPrfTel2] [varchar](50) NULL,
	[JobPrfEmail] [varchar](50) NULL,
	[JobPrfEdu1] [varchar](max) NULL,
	[JobPrfEdu2] [varchar](max) NULL,
	[JobPrfEdu3] [varchar](max) NULL,
	[JobPrfEduActivity] [varchar](500) NULL,
	[JobPrfSkLang1] [varchar](500) NULL,
	[JobPrfSkLang2] [varchar](500) NULL,
	[JobPrfSkCom] [varchar](500) NULL,
	[JobPrfSkOther] [varchar](500) NULL,
	[JobPrfPastWork1] [varchar](500) NULL,
	[JobPrfPastWork2] [varchar](500) NULL,
	[JobPrfMoreInfo] [varchar](max) NULL,
	[JobPrfDateReg] [datetime] NULL,
	[JobPrfStatus] [int] NULL,
 CONSTRAINT [PK_tbJobPrf] PRIMARY KEY CLUSTERED 
(
	[JobPrfId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbJobPosi]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbJobPosi](
	[JobPosiId] [int] IDENTITY(1,1) NOT NULL,
	[JobCatId] [int] NULL,
	[JobPosiName] [varchar](200) NULL,
	[JobPosiBranch] [varchar](200) NULL,
	[JobPosiDetail] [varchar](max) NULL,
	[JobPosiQnt] [varchar](200) NULL,
	[JobPosiStatus] [int] NULL,
	[JobPosiSort] [decimal](8, 3) NULL,
	[JobPosiDtStart] [datetime] NULL,
	[JobPosiDtStop] [datetime] NULL,
	[JobPiority] [int] NULL,
	[JobPosiDateReg] [datetime] NULL,
	[lang] [int] NULL,
	[input_date] [datetime] NULL,
	[input_user] [varchar](13) NULL,
 CONSTRAINT [PK_tbJobPosi] PRIMARY KEY CLUSTERED 
(
	[JobPosiId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbInv]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbInv](
	[InvId] [int] IDENTITY(1,1) NOT NULL,
	[InvTopic] [varchar](200) NULL,
	[InvStatus] [int] NULL,
	[InvDetailLong] [varchar](max) NULL,
	[InvDtReg] [datetime] NULL,
	[lang] [int] NULL,
	[input_date] [datetime] NULL,
	[input_user] [varchar](13) NULL,
 CONSTRAINT [PK_tbIvm] PRIMARY KEY CLUSTERED 
(
	[InvId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbFaq]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbFaq](
	[FaqId] [int] IDENTITY(1,1) NOT NULL,
	[FaqQuiz] [varchar](200) NULL,
	[FaqAnswer] [varchar](max) NULL,
	[FaqSort] [decimal](8, 3) NULL,
	[FaqStatus] [int] NULL,
	[FaqDateReg] [datetime] NULL,
	[lang] [int] NULL,
	[input_date] [datetime] NULL,
	[input_user] [varchar](13) NULL,
 CONSTRAINT [PK_tbFaq] PRIMARY KEY CLUSTERED 
(
	[FaqId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbCnt]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbCnt](
	[CntId] [int] IDENTITY(1,1) NOT NULL,
	[CntName] [varchar](200) NULL,
	[CntTel] [varchar](200) NULL,
	[CntEmail] [varchar](200) NULL,
	[CntDetail] [varchar](max) NULL,
	[CntDateTimeGet] [datetime] NULL,
	[CntDateSet] [datetime] NULL,
	[CntTimeSet] [varchar](100) NULL,
	[CntStatus] [int] NULL,
 CONSTRAINT [PK_tbCnt] PRIMARY KEY CLUSTERED 
(
	[CntId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbBanner]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbBanner](
	[BannerId] [int] IDENTITY(1,1) NOT NULL,
	[BannerCat] [int] NULL,
	[BannerTitle] [varchar](200) NULL,
	[BannerPic] [varchar](500) NULL,
	[BannerStatus] [int] NULL,
	[BannerStart] [datetime] NULL,
	[BannerStop] [datetime] NULL,
	[BannerSort] [decimal](8, 3) NULL,
	[BannerLink] [varchar](500) NULL,
	[lang] [int] NULL,
	[input_date] [datetime] NULL,
	[input_user] [varchar](13) NULL,
 CONSTRAINT [PK_BannerId] PRIMARY KEY CLUSTERED 
(
	[BannerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ta_rate]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ta_rate](
	[tr_day] [int] NOT NULL,
	[tr_sum_ins] [numeric](12, 2) NOT NULL,
	[tr_medic] [char](1) NOT NULL,
	[tr_prmm] [numeric](10, 2) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ta_cover]    Script Date: 11/21/2013 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ta_cover](
	[ta_regis_no] [char](20) NOT NULL,
	[ta_tot_ins] [int] NOT NULL,
	[ta_sum_ins] [numeric](12, 2) NOT NULL,
	[ta_medic_exp] [numeric](12, 2) NOT NULL,
	[ta_prmm] [numeric](10, 2) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[sp_calonlineweb]    Script Date: 11/21/2013 17:39:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_calonlineweb]
	-- Add the parameters for the stored procedure here
	 @strFlagCompul varchar(1),
	 @strCarCod VARCHAR(16),
	 @strPolType VARCHAR(10),
     @strCarAge  VARCHAR(4),
     @strCarGroup VARCHAR(16),
     @strCarMark VARCHAR(16),
     @strCarSize  VARCHAR(4),
     @strCarBody VARCHAR(16),
     @strAgtCod VARCHAR(16),
     @pv_strOdMin Decimal(12,2),
     @pv_strOdMax Decimal(12,2),
	 @pv_strDriver1Age Decimal(12,2),
	 @strExpPercent Decimal(12,2)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    DECLARE @pv_comnet Decimal(12,2);
    DECLARE @pv_comstm Decimal(12,2);
    DECLARE @pv_comtax Decimal(12,2);
    DECLARE @pv_comgrs Decimal(12,2);
    DECLARE @pv_stmrate Decimal(5,2);
    DECLARE @pv_taxrate Decimal(5,2);
	DECLARE @pv_PolType1 VARCHAR(1);
	DECLARE @pv_PolType2 VARCHAR(1);
	
	set @pv_comnet = 0;
	set @pv_comstm = 0;
	set @pv_comtax = 0;
	set @pv_comgrs = 0;
	set @pv_PolType1 = @strPolType;
	set @pv_PolType2 = @strPolType;
	if (@strPolType = '3')
	begin
		set @pv_strOdMin = 0;
		set @pv_strOdMax = 9999999999;
	end
	if (@strPolType = '3' or @strPolType = '3' )
	begin
		set @pv_strDriver1Age = 0;
	end
	if (@strPolType = '2')
	begin
	     set @pv_PolType1 = '2';
	     set @pv_PolType2 = '5';
	end
	if (@strCarCod = '320')
	begin
		set @strCarSize = '4';
	end
	if(convert(int,@strCarAge) > 10)
	begin
		set @strCarAge = '99';
	end
	if (@strFlagCompul = 'Y')
	BEGIN
	   select @pv_comnet = C.pre_net ,
			@pv_comgrs = C.pre_grs,
			@pv_comstm = C.pre_stm,
			@pv_comtax = C.pre_tax	   
	   From online_compprem C
	   Where C.car_cod = @strCarCod
	   AND C.car_body = case when (@strCarBody in ('A','N')) then '0' else @strCarBody end;
	END
	set @pv_stmrate = 0.4;	
	set @pv_taxrate = 7;
		
	With CteMain
	As
	(
		SELECT B.name, A.web_id, A.campaign, A.car_cod, A.pol_typ, car_age, car_group,
		car_mak, car_size, A.car_body, agt_cod, car_od, car_driver, start_dte, end_dte,
		pre_pregar, pre_extgar, pre_netgar,pre_stmgar,pre_taxgar,pre_grsgar,
		pre_preservice,pre_extservice,pre_netservice,pre_stmservice,pre_taxservice,pre_grsservice,  
		pre_netgar20, pre_netgar30, pre_netgar40, pre_netgar50,
		pre_netservice20, pre_netservice30, pre_netservice40, pre_netservice50
		FROM online_web A  
		INNER JOIN online_campaign B ON A.campaign = B.campaign AND 
					(A.pol_typ = B.pol_typ   OR case when  A.pol_typ in ('1','3') then 'A' else A.pol_typ end = B.pol_typ )   
		 WHERE     (A.car_cod =  @strCarCod ) AND  
					(A.pol_typ = @pv_PolType1 OR A.pol_typ = @pv_PolType2) AND
					(car_age = @strCarAge) AND 
					 (car_group = @strCarGroup) AND 
					(car_mak = @strCarMark OR         
						(car_mak = 'A'  AND (SELECT count(*) FROM online_web T 
											WHERE T.campaign = A.campaign AND T.car_cod = A.car_cod AND 
												T.pol_typ = A.pol_typ  AND  
												T.car_age = A.car_age AND T.car_group = A.car_group AND  
												 T.car_mak = @strCarMark  AND T.car_size = A.car_size AND 
												T.car_body = A.car_body AND T.agt_cod = A.agt_cod AND  
												T.car_od = A.car_od AND T.car_driver = A.car_driver) = 0 )) AND 
					 (car_size = @strCarSize) AND (A.car_body = @strCarBody) AND (agt_cod = @strAgtCod)AND
					 (car_od between @pv_strOdMin AND @pv_strOdMax) AND  (car_driver = @pv_strDriver1Age)   
		),    
	CteDiscount
	    AS
    (
		select *,
		case 
		     when pol_typ in ('3','5') then pre_netgar
		     when  campaign = '500' then
		           case when @strExpPercent = 0  then pre_netgar else pre_netgar20 end
			 when  @strExpPercent = 20 then pre_netgar20 
			 when  @strExpPercent = 30 then pre_netgar30 
		     when  @strExpPercent = 40 then pre_netgar40 
		     when  @strExpPercent = 50 then pre_netgar50 
		     when  @strExpPercent = 0 then pre_netgar 
		     else  pre_netgar end  as pre_netgar1,
		case  
		     when  campaign = '500' then
		       case when @strExpPercent = 0  then pre_netservice else pre_netservice20 end
			 when  @strExpPercent = 20 then pre_netservice20 
		     when  @strExpPercent = 30 then pre_netservice30 
		     when  @strExpPercent = 40 then pre_netservice40 
		     when  @strExpPercent = 50 then pre_netservice50 
		     when  @strExpPercent = 0 then pre_netservice 
		     else  pre_netservice end  as pre_netservice1
		from CteMain
    ),
    CteAA
    AS
    (
		select name, web_id, campaign, car_cod, pol_typ, car_age, car_group,
				car_mak, car_size, car_body, agt_cod, car_od, car_driver, start_dte, end_dte,
				round(pre_netgar1,0) as pre_netgar, 
				pre_pregar,
				pre_extgar,
				round(pre_netservice1,0) as pre_netservice, 
				pre_preservice, 
				pre_extservice,
				pre_netgar1,pre_netservice1,
				pre_grsgar,pre_grsservice, -- 013 ต้องใช้ grs   net เป็น net ของสมัครใจ
				ceiling(round(pre_netgar1,0) * @pv_stmrate/100) as pre_stmgar,
				round((round(pre_netgar1,0) + ceiling(round(pre_netgar1,0) * @pv_stmrate/100)) * @pv_taxrate/100,2) as pre_taxgar, 
				round(pre_netgar1,0) + ceiling(round(pre_netgar1,0) * @pv_stmrate/100) + round((round(pre_netgar1,0) + ceiling(round(pre_netgar1,0) * @pv_stmrate/100)) * @pv_taxrate/100,2) as pre_grsgar1,
				ceiling(round(pre_netservice1,0) * @pv_stmrate/100) as pre_stmservice,
				round((round(pre_netservice1,0) + ceiling(round(pre_netservice1,0) * @pv_stmrate/100)) * @pv_taxrate/100,2) as pre_taxservice, 
				round(pre_netservice1,0) + ceiling(round(pre_netservice1,0) * @pv_stmrate/100) + round((round(pre_netservice1,0) + ceiling(pre_netservice1 * @pv_stmrate/100)) * @pv_taxrate/100,2) as pre_grsservice1
    From CteDiscount 
    )
    
    
    Select name, web_id, campaign, car_cod, pol_typ, car_age, car_group,
    car_mak, car_size, car_body, agt_cod, car_od, car_driver, start_dte, end_dte,
             pre_netgar + @pv_comnet as pre_netgar, 
             pre_stmgar + @pv_comstm as pre_stmgar, 
			 pre_taxgar + @pv_comtax as pre_taxgar,  
			 --case when pre_grsgar  > 0 then pre_grsgar + @pv_comgrs else pre_grsgar  end as pre_grsgar, 
			 case when pre_grsgar  > 0 then
			      case when campaign != '013' then pre_grsgar1 + @pv_comgrs else pre_grsgar  end   
			 else 0
			 end as pre_grsgar,
			 pre_preservice, 
			 pre_extservice,  
			 pre_netservice + @pv_comnet as pre_netservice, 
			 pre_stmservice + @pv_comstm as pre_stmservice,  
			 pre_taxservice + @pv_comtax as pre_taxservice,  
			--case when pre_grsservice  > 0 then pre_grsservice + @pv_comgrs else pre_grsservice end as pre_grsservice 
			--case when pre_grsservice  > 0 then
			--      case when campaign != '013' then pre_grsservice1 + @pv_comgrs else pre_grsgar  end   
			-- else 0
			case when campaign = '500' then pre_grsservice1 + @pv_comgrs else 0 end
			 as pre_grsservice,
			 pre_netgar + pre_stmgar +pre_taxgar as pre_vouchergar,
			 pre_netservice + pre_stmservice +pre_taxservice as pre_voucherservice
			--,*
    From CteAA 
    ORDER BY case when campaign = '000' then 'ZZZ' else campaign end , name, car_od     
END
GO
/****** Object:  Default [DF_tbBanner_lang]    Script Date: 11/21/2013 17:39:14 ******/
ALTER TABLE [dbo].[tbBanner] ADD  CONSTRAINT [DF_tbBanner_lang]  DEFAULT ((0)) FOR [lang]
GO
/****** Object:  Default [DF_tbPrdCon_lang]    Script Date: 11/21/2013 17:39:14 ******/
ALTER TABLE [dbo].[tbPrdCon] ADD  CONSTRAINT [DF_tbPrdCon_lang]  DEFAULT ((0)) FOR [lang]
GO
